/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkio_i.h
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#include <cstdint>
#ifndef KNERONSDK_I_H
#define KNERONSDK_I_H
#define USB_MAX_BYTES 1024 * 8
#define VENDOR_ID 0x0d7d
#define PRODUCT_ID 0x0100
#define EP_IN 0x81
#define EP_OUT 0x01
#define ACK_PACKET_SIZE 8
#define SYNC_PACKET_SIZE 4
#define UART_MAX_BYTES 64

#define CNN_COMMAND_SIZE 2


#define HAPS_ID  1  // 2 3

#define DATA_BLOCK_SIZE  8192;


#define INTF_USB  0
#define INTF_UART  1
#define INTF_I2C  2
#define INTF_SPI 3

#if (HAPS_ID == 1) || (HAPS_ID == 3)
#define COM_ID  5
#define UART_BLOCK  0x800
#else
#if (HAPS_ID == 2)
#define COM_ID  7
#define UART_BLOCK 0x890
#define  act_intf  INTF_UART
#define  BAUDRATE  115200
#define  dut_intf  INTF_I2C
#endif
#endif

#define CMD_MEM_READ  0x0001
#define CMD_MEM_WRITE 0x0002
#define CMD_MEM_CLR   0x0006
#define CMD_TEST_ECHO 0x0008
#define CMD_USB_WRITE 0x0009

#define RESP_MEM_READ  0x8001
#define RESP_MEM_WRITE 0x8002
#define RESP_MEM_CLR   0x8006
#define RESP_TEST_ECHO 0x8008
#define RESP_USB_WRITE 0x8009

#define CMD_DEMO_SET_MODELS	0x0010
#define CMD_DEMO_SET_IMAGES	0x0011
#define CMD_DEMO_RUN_ONCE	0x0012
#define CMD_DEMO_RUN	0x0013
#define CMD_DEMO_STOP	0x0014

#define RESP_DEMO_SET_MODELS  0x8010
#define RESP_DEMO_SET_IMAGES  0x8011
#define RESP_DEMO_RUN_ONCE  0x8012
#define RESP_DEMO_RUN  0x8013
#define RESP_DEMO_STOP  0x8014

#define CMD_RESET	0x0020
#define CMD_SYSTEM_STATUS	0x0021
#define CMD_QUERY_APPS	0x0022
#define CMD_SELECT_APP	0x0023
#define CMD_SET_PROCESSING_MODE	0x0024
#define CMD_SET_UPDATE_EVENT	0x0025
#define CMD_STATUS_UPDATE	0x0026
#define CMD_ABORT_IMAGE	0x0028

#define RESP_RESET	0x8020
#define RESP_SYSTEM_STATUS	0x8021
#define RESP_QUERY_APPS	0x8022
#define RESP_SELECT_APP	0x8023
#define RESP_SET_PROCESSING_MODE	0x8024
#define RESP_SET_UPDATE_EVENT	0x8025
#define RESP_STATUS_UPDATE	0x8026
#define RESP_IMAGE_RESULT	0x8027
#define RESP_ABORT_IMAGE	0x8028

#define CMD_SET_FID_MODE	0x0100
#define CMD_VERIFY_FID_USER	0x0101
#define CMD_NEW_FID_USER	0x0102
#define CMD_EDIT_FID_USER	0x0103

#define RESP_SET_FID_MODE	0x8100
#define RESP_VERIFY_FID_USER	0x8101
#define RESP_NEW_FID_USER	0x8102
#define RESP_EDIT_FID_USER	0x8103

#define CMD_SFID_START	0x0108
#define CMD_SFID_NEW_USER	0x0109
#define CMD_SFID_ADD_DB	0x010A
#define CMD_SFID_DELETE_DB	0x010B
#define CMD_SFID_SEND_IMAGE	0x010C

#define RESP_SFID_START	0x8108
#define RESP_SFID_NEW_USER	0x8109
#define RESP_SFID_ADD_DB	0x810A
#define RESP_SFID_DELETE_DB	0x810B
#define RESP_SFID_SEND_IMAGE 0x810C

#define RESP_ACK 0x8004

#define PKT_W_FLAG  0x01
#define PKT_CRC_FLAG  0x1



// model type, for development of FD/FR, remove later
#define MODEL_TYPE_FD  1
#define MODEL_TYPE_LM  2
#define MODEL_TYPE_LD  3
#define MODEL_TYPE_FR  4

// Messge HDR formart
//#define MSG_HDR_FMT  "<HHIII" //# Header(2b), checksum(2b), Cmd(4b), Addr(4b), Len(4b)
#define  MSG_HDR_FMT = "<HHHHII"// # Preamble(2b), Psize(2b), Cmd(2b), Csize(2b), Addr(4b), Len(4b)
#define PKTX_PAMB 0xA583    //  # Packet TX Preamble
#define PKRX_PAMB 0x8A35     // # Packet RX Preamble
#define MSG_DAT_IDX  16      //  # message body will be stored from offset 16


// SET_MODEL and SET_IMAGES format
//#define CMD_DEMO_SET_TEN_FMT = "<IIIIIIIIII"  // # arg1(4b), arg2(4b), arg3(4b), arg4(4b), arg5(4b), arg6(4b), arg7(4b), arg8(4b), arg9(4b), arg10(4b)
//#define CMD_DEMO_SET_EIGHT_FMT = "<IIIIIIII" //  # arg1(4b), arg2(4b), arg3(4b), arg4(4b), arg5(4b), arg6(4b), arg7(4b), arg8(4b)
//#define CMD_DEMO_SET_SIX_FMT = "<IIIIII"   //# arg1(4b), arg2(4b), arg3(4b), arg4(4b), arg5(4b), arg6(4b)
//#define CMD_DEMO_SET_TWO_FMT = "<II"  // # arg1(4b), arg2(4b)

//#define MSG_ACK  "UsbAck\0"

/*
# FID Commands
*/
#define SET_FID_MODE   0x100
#define VERIFY_USER    0x101
#define NEW_FID_USER   0x102
#define EDIT_FID_USER  0x103

#define FID_MSG_HDR  8 // 2+2+2+2 # only packet and msg header
// #define FID_RSP_FMT  "<HHHHIH" // Packet & Msg HDR, error(4b), usr_id(2b)
#define MSG_RESP_HDR_SIZE 4
#define MSG_HDR_SIZE 4
#define MSG_PARA_HDR_SIZE 4
//typedef struct test_input {
//  bool crc;
//  uint32_t addr;
//  uint32_t len;
//  void *data;

//} __attribute__ ((packed,aligned(32))) test_input_t;

//typedef struct test_output {
//  uint16_t rsp_type;
//  uint32_t *act_out_len;
//  uint32_t *err;
//  void *data;
//} __attribute__ ((packed,aligned(32))) test_output_t;


typedef struct packet {
    uint16_t premble;
    struct  {
     uint16_t payload_size:12;
     uint16_t rw:1;
     uint16_t crc:1;
     uint16_t rsvd:1;
     uint16_t nack:1;
    } pack_payload_attr;
    struct  {
        uint16_t type;
        uint16_t length;
        struct  {
          uint32_t addr;
          uint32_t num;
        } para;
    } message;
    uint32_t crc;
}  __attribute__ ((packed,aligned(32))) packet_t;

typedef struct packet_r {
    uint16_t premble;
    struct  {
     uint16_t payload_size:12;
     uint16_t rw:1;
     uint16_t crc:1;
     uint16_t rsvd:1;
     uint16_t nack:1;
    } pack_payload_attr;
    struct  {
        uint16_t type;
        uint16_t length;
        struct  {
          uint32_t ret;
          uint32_t num;
        } para;
    } message;
    uint32_t crc;
}  __attribute__ ((packed,aligned(32))) packet_r_t;


typedef struct test_mem_read {
  uint16_t type;
  uint16_t length;
  struct  {
    uint32_t addr;
    uint32_t num;
  } para_mem;
}  __attribute__((packed,aligned(32))) test_mem_read_t;

typedef struct test_mem_read_rsp {
  uint16_t type; //	0x8001
  uint16_t length; //	8+n
  struct {
    uint32_t	ret; // Memory read result
    uint32_t	num; //number of bytes read
    uint8_t data[0];
   } para_mem;//n bytes memory data
}  __attribute__ ((packed,aligned(32))) test_mem_read_rsp_t;

typedef struct test_mem_write {
  uint16_t type;
  uint16_t length;
  struct {
    uint32_t addr;
    uint32_t num;
    uint8_t data[0];
  } para_mem;
} __attribute__ ((packed,aligned(32))) test_mem_write_t ;

typedef struct test_mem_write_rsp {
uint16_t type;
uint16_t length;
struct {
  uint32_t ret;
  uint32_t num;
}para;
}  __attribute__ ((packed,aligned(32))) test_mem_write_rsp_t;

typedef struct test_mem_clr {
    uint16_t type;
    uint16_t length;
    struct {
      uint32_t	addr;
      uint32_t	num;
    } para_mem;
}  __attribute__ ((packed,aligned(32))) test_mem_clr_t;

typedef struct test_mem_clr_rsp {
      uint16_t type;
      uint16_t length;
      struct {
        uint32_t ret;
        uint32_t num;
      } para_mem;
}  __attribute__ ((packed,aligned(32)))  test_mem_clr_rsp_t;

typedef struct test_echo {
  uint16_t type;
  uint16_t length;
  struct  {
   uint32_t reserved;
    uint32_t num;
    uint8_t data[0];
  }para_mem;
}  __attribute__ ((packed,aligned(32))) test_echo_t;

typedef struct test_echo_rsp {
  uint16_t type;
  uint16_t length;
  struct  {
    uint32_t ret;
    uint32_t num;
    uint8_t data[0];
   }para_mem;
}  __attribute__ ((packed,aligned(32))) test_echo_rsp_t;

typedef struct test_usb_write {
  uint16_t type;
  uint16_t length;
  struct  {
   uint32_t addr;
    uint32_t num;
  }para_mem;
}  __attribute__ ((packed,aligned(32))) test_usb_write_t;

typedef struct test_usb_write_rsp {
  uint16_t type;
  uint16_t length;
  struct  {
    uint32_t ret;
    uint32_t num;
   }para_mem;
}  __attribute__ ((packed,aligned(32))) test_usb_write_rsp_t;

typedef struct demo_set_model {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t reserved;
      uint32_t reserved1;
      uint32_t setup_addr;
      uint32_t setup_len;
      uint32_t cmd_addr;
      uint32_t cmd_len;
      uint32_t weight_addr;
      int32_t weight_len;
    }para_mode;
}  __attribute__ ((packed,aligned(32))) demo_set_model_t;

typedef struct demo_set_models_rsp {
  uint16_t type;
  uint16_t length;
  struct  {
  uint32_t ret;
  uint32_t reserved;
    }para_mode;
}  __attribute__ ((packed,aligned(32))) demo_set_models_rsp_t;

typedef struct demo_set_image {
      uint16_t type;
      uint16_t length;
      struct  {
        uint32_t reserved;
        uint32_t reserved1;
        uint32_t input_row;
        uint32_t input_col;
        uint32_t input_chann;
        uint32_t addr;
        int32_t len;
        int32_t img_no;
      } para_img;
}  __attribute__ ((packed,aligned(32)))demo_set_image_t ;

typedef struct demo_set_image_rsp {
  uint16_t type;
  uint16_t length;
  struct  {
  uint32_t ret;
  uint32_t reserved;
    }para;
}  __attribute__ ((packed,aligned(32))) demo_set_image_rsp_t;

typedef struct demo_run_once {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t ret;
      uint32_t reserved;
    } para;
}  __attribute__ ((packed,aligned(32)))demo_run_once_t;

typedef struct demo_run_once_rsp {
  uint16_t type;
  uint16_t length;
  struct  {
  uint32_t ret;
  uint32_t reserved;
    }para;
}  __attribute__ ((packed,aligned(32))) demo_run_once_rsp_t;


typedef struct demo_run {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t mod_addr;
      uint32_t img_addr;
      uint32_t mode;
    } para_mod;
}  __attribute__ ((packed,aligned(32))) demo_run_t;

typedef struct demo_run_rsp {
  uint16_t type;
  uint16_t length;
  struct  {
  uint32_t ret;
  uint32_t reserved;
    }para;
}  __attribute__ ((packed,aligned(32))) demo_run_rsp_t;


typedef struct demo_stop {
  uint16_t type;
  uint16_t length;
  struct  {
  uint32_t ret;
  uint32_t reserved;
    }para;
}  __attribute__ ((packed,aligned(32))) demo_stop_t;

typedef struct demo_stop_rsp {
  uint16_t type;
  uint16_t length;
  struct  {
  uint32_t ret;
  uint32_t reserved;
    }para;
}  __attribute__ ((packed,aligned(32))) demo_stop_rsp_t;

typedef struct demo_report {
  uint16_t type;
  uint16_t length;
  uint32_t flag;
}  __attribute__ ((packed,aligned(32))) demo_report_t;

typedef struct demo_report_rsp {
  uint16_t type;
  uint16_t length;
  struct {
    uint32_t  ret;
    uint32_t reserved;
    uint32_t  size;
  } para_report;
}  __attribute__ ((packed,aligned(32))) demo_report_rsp_t;

typedef struct demo_result_rsp {
  uint16_t type;
  uint16_t length;
  struct {
    uint32_t  ret;
    uint32_t reserved;
    uint32_t  size;
  } para_report;
}  __attribute__ ((packed,aligned(32))) demo_result_rsp_t;


typedef struct oper_reset {
  uint16_t type;
  uint16_t length;
  struct  {
    uint32_t mode;
    uint32_t c_flag;
  }para_reset;
}  __attribute__ ((packed,aligned(32))) oper_reset_t;

typedef struct oper_reset_rsp {
  uint16_t type;
  uint16_t length;
  struct  {
  uint32_t ret;
  uint32_t reserved;
} para;
}  __attribute__ ((packed,aligned(32))) oper_reset_rsp_t;

typedef struct oper_sys_status {
  uint16_t type;
  uint16_t length;
}  __attribute__ ((packed,aligned(32))) oper_sys_status_t;

typedef struct oper_sys_status_rsp {
    uint16_t type;
    uint16_t length;
    struct {
      uint32_t dev_id;
      uint32_t firmware_id;
    } para_sys;
}  __attribute__ ((packed,aligned(32))) oper_sys_status_rsp_t;



typedef struct oper_query_apps {
   uint16_t type;
   uint16_t length;
}  __attribute__ ((packed,aligned(32))) oper_query_apps_t;

typedef struct oper_query_apps_rsp {
      uint16_t type;
      uint16_t length;
      struct  {
        uint32_t dev_id;
        uint32_t reserved;
       struct  {
          uint32_t app_id;
        }  app_ino[0];
      } para;
}  __attribute__ ((packed,aligned(32))) oper_query_apps_rsp_t;

typedef struct oper_select_app {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t appid;
    } para_mode;
}  __attribute__ ((packed,aligned(32))) oper_select_app_t;

typedef struct oper_select_app_rsp {
    uint16_t type;
    uint16_t length;
    struct {
      uint32_t  ret;
      uint32_t  size;
    } para;

}  __attribute__ ((packed,aligned(32))) oper_select_app_rsp_t;

typedef struct oper_set_proc_mode {
  uint16_t type;
  uint16_t length;
  struct  {
    uint32_t seqno;
    uint32_t mode;
  }para;
}  __attribute__ ((packed,aligned(32))) oper_set_proc_mode_t;

typedef struct oper_set_proc_mode_rsp {
    uint16_t type;
    uint16_t length;
    struct  {
     uint32_t ret;
      uint32_t reserved;
    }para;
}  __attribute__ ((packed,aligned(32))) oper_set_proc_mode_rsp_t;

typedef struct oper_set_update_event {
  uint16_t type;
  uint16_t length;
  struct  {
    uint32_t flag;
   }para;
}  __attribute__ ((packed,aligned(32))) oper_set_update_event_t;

typedef struct oper_set_update_event_rsp {
    uint16_t type;
    uint16_t length;
    struct  {
     uint32_t mask;
      uint32_t reserved;
    }para;
}  __attribute__ ((packed,aligned(32))) oper_set_update_event_rsp_t;

typedef struct oper_status_update {
  uint16_t type;
  uint16_t length;
}  __attribute__ ((packed,aligned(32))) oper_status_update_t;

typedef struct oper_status_update_rsp {
    uint16_t type;
    uint16_t length;
    struct  {
     uint32_t event;
      uint32_t event_detail;
    }para;
}  __attribute__ ((packed,aligned(32))) oper_status_update_rsp_t;

typedef struct oper_img_result_rsp_t{
  uint16_t type;
  uint16_t length;
  struct {
    uint32_t seq_no;
    uint32_t ret;
    uint8_t data[0];
  }para;
}  __attribute__ ((packed,aligned(32))) oper_img_result_rsp_t;

typedef struct oper_abort_img {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t img_seqno;
    } para;
}  __attribute__ ((packed,aligned(32))) oper_abort_img_t;

typedef struct oper_abort_img_rsp_t {
    uint16_t type;
    uint16_t length;
    struct {
      uint32_t ret;
      uint32_t seq_no;
    }para;
}  __attribute__ ((packed,aligned(32))) oper_abort_img_rsp_t;

typedef struct fid_set_fid_mode {
    uint16_t type;
    uint16_t length;
    struct  {
        uint16_t mode;
        uint16_t verif;
        uint32_t password;
        uint32_t conf_para;
    } para;
}  __attribute__ ((packed,aligned(32))) fid_set_fid_mode_t;

typedef struct fid_set_fid_mode_rsp_t {
    uint16_t type;
    uint16_t length;
    struct {
      uint32_t ret;
      uint16_t new_fid;
      uint16_t reserved;
      uint8_t unit_info[12];
    }para;
}  __attribute__ ((packed,aligned(32))) fid_set_fid_mode_rsp_t;

typedef struct fid_verify_fid_user {
    uint16_t type;
    uint16_t length;
    struct  {
        uint16_t subcommand;
        uint16_t reserved;
        uint16_t user_id;
        uint16_t img_idx;
    } para;
}  __attribute__ ((packed,aligned(32))) fid_verify_fid_user_t;

typedef struct fid_verify_fid_user_rsp_t {
    uint16_t type;
    uint16_t length;
    struct {
      uint32_t ret;
      uint16_t user_id;
      uint16_t reserved;
    }para;
}  __attribute__ ((packed,aligned(32))) fid_verify_fid_user_rsp_t;

typedef struct fid_cmd_new_fid_user {
    uint16_t type;
    uint16_t length;
    struct  {
        uint16_t subcommand;
        uint16_t img_idx;
        uint16_t reserved;
        uint16_t reserved1;
    } para;
}  __attribute__ ((packed,aligned(32))) fid_cmd_new_fid_user_t;

typedef struct fid_cmd_new_fid_user_rsp_t {
    uint16_t type;
    uint16_t length;
    struct {
      uint32_t ret;
      uint16_t user_id;
      uint16_t img_idx;
    }para;
}  __attribute__ ((packed,aligned(32))) fid_cmd_new_fid_user_rsp_t;

typedef struct fid_edit_fid_user {
    uint16_t type;
    uint16_t length;
    struct  {
        uint16_t subcommand;
        uint16_t user_id;
        uint16_t img_idx;
        uint16_t reserved;
    } para;
}  __attribute__ ((packed,aligned(32))) fid_edit_fid_user_t;

typedef struct fid_edit_fid_user_rsp_t {
    uint16_t type;
    uint16_t length;
    struct {
      uint32_t ret;
      uint16_t subcommand;
      uint16_t user_id;
      uint8_t num[0];
    }para;
}  __attribute__ ((packed,aligned(32))) fid_edit_fid_user_rsp_t;

typedef struct sfid_start {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t reserved;
    } para;
}  __attribute__ ((packed,aligned(32))) sfid_start_t;


typedef struct sfid_start_rsp_t{
  uint16_t type;
  uint16_t length;
  struct {
    uint32_t ret;
    uint32_t img_size;
  }para;
}  __attribute__ ((packed,aligned(32))) sfid_start_rsp_t;

typedef struct sfid_new_user {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t user_id;
      uint32_t img_idx;
    } para;
} __attribute__ ((packed,aligned(32))) sfid_new_user_t;

typedef struct sfid_new_user_rsp_t{
  uint16_t type;
  uint16_t length;
  struct {
    uint32_t ret;
    uint16_t user_id;
    uint16_t img_idx;
  }para;
}  __attribute__ ((packed,aligned(32))) sfid_new_user_rsp_t;

typedef struct sfid_add_new_user_db {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t user_id;
    } para;
}  __attribute__ ((packed,aligned(32))) sfid_add_new_user_db_t;

typedef struct sfid_add_new_user_db_rsp_t{
  uint16_t type;
  uint16_t length;
  struct {
    uint32_t ret;
    uint32_t reserved;
  }para;
}  __attribute__ ((packed,aligned(32))) sfid_add_new_user_db_rsp_t;

typedef struct sfid_del_userDB {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t user_id;
    } para;
}  __attribute__ ((packed,aligned(32))) sfid_del_userDB_t;

typedef struct sfid_del_userDB_rsp_t{
  uint16_t type;
  uint16_t length;
  struct {
    uint32_t ret;
    uint32_t reserved;
  }para;
}  __attribute__ ((packed,aligned(32))) sfid_del_userDB_rsp_t;

typedef struct sfid_send_img {
    uint16_t type;
    uint16_t length;
    struct  {
      uint32_t img_size;
    } para;
}  __attribute__ ((packed,aligned(32))) sfid_send_img_t;

typedef struct sfid_send_img_rsp_t{
  uint16_t type;
  uint16_t length;
  struct {
    uint32_t ret;
    uint32_t id_idx;
  }para;
}  __attribute__ ((packed,aligned(32))) sfid_send_img_rsp_t;
#endif // KNERONSDK_I_H
