#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include "knorenimage.h"
#include "kneronsdkuart_e.h"
#include "kneronsdkusb_e.h"
#include "kneronsdkcommand_e.h"
int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
//    qmlRegisterType<KneronPKG>("io.qt.kneronpkg", 1, 0, "KneronPKG");
//    qmlRegisterType<UARTPort>("io.qt.uartport", 1, 0, "UARTPort");
//    qmlRegisterType<USBPort>("io.qt.usbport", 1, 0, "USBPort");

//    qmlRegisterType<Command>("io.qt.command", 1, 0, "Command");
    QQmlApplicationEngine engine;
  //  UARTPort uartport;
  //  printf("uart number is %d\r\n", uartport.m_port_num);
 //   printf("uart name is %d\n\r", uartport.m_infos.size());

//    if (false == uartport.init_io_port("COM3"))
//    {
//          printf("uartport  init_io_port is COM3 error\r\n");
//          return -1;

//    }
//    uartport.open();
//    if (false == uartport.send_sync_to_io())
//    {
//         printf("uartport  send_sync_to_io error\r\n");
//         return -1;
//    }
     USBPort usbport;
     if (nullptr == usbport.p_usbport_i) {
         printf("open usb error\n\r");
         return -1;
     }
     if (false == usbport.init_io_port()) {
         printf("port initialization failed\r\n");
         return -1;
     }
     if (true != usbport.open()) {
         printf("usbport open failed\r\n");
         return -1;

     }
//     if (false == usbport.send_sync_to_io(uartport)) {
//         printf("usb port send_sync_to_io failed\r\n");
//         return -1;
//     }
//     else
//         printf("sync USB is OK\n\r");
//     uartport.close();
//     usbport.close();
//     if (false == uartport.init_io_port("COM3"))
//     {
//           printf("uartport  init_io_port is COM3 error\r\n");
//           return -1;

//     }
//     uartport.open();
//     if (false == uartport.send_sync_to_io())
//     {
//          printf("uartport  send_sync_to_io error\r\n");
//          return -1;
//     }
//     if (nullptr == usbport.p_usbport_i) {
//         printf("open usb error\n\r");
//         return -1;
//     }
//     if (false == usbport.init_io_port()) {
//         printf("port initialization failed\r\n");
//         return -1;
//     }
//     if (true != usbport.open()) {
//         printf("usbport open failed\r\n");
//         return -1;

//     }
//     if (false == usbport.send_sync_to_io(uartport)) {
//         printf("usb port send_sync_to_io failed\r\n");
//         return -1;
//     }
//     else
//         printf("sync USB is OK\n\r");
     KneronPKG KneronImage;
  //   Command command;
     uint8_t temp[256] = {'b'};
     uint8_t *test_p = new uint8_t[32*1024*1024];
//while (1) {}
     for (int i = 0; i < 32*1024*1024 -1; i++)
         test_p[i] = '9';
//     QString filename("C:\\Users\\feng zhou\\Pictures\\cat\\cat03.jpg");
//     QStringList file_name_list;
//    file_name_list.append(filename);
     // command.send_image(uartport, usbport, image,file_name_list,  0, 0);
     bool status = false;
  //   status = image.send_model(uartport, usbport, image,test_p, 33554435,0);
   status =   KneronImage.send_image_buffer(test_p,  32*1024*1024,  usbport);
  //   status =   KneronImage.send_image_buffer(temp,  256,  usbport);
     if (status == false)
     printf("test command status is %d false ", status);
     delete[] test_p;
//     uartport.close();
       usbport.close();

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
