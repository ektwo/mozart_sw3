import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import io.qt.uartport 1.0
Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")
    UARTPort {
        id: uartport
    }

    Button {
         id: button
         text: "Submit"
         anchors.verticalCenter: parent.verticalCenter
         MouseArea {
                 id: mouseArea1
                anchors.fill: parent
            //    hoverEnabled: true;
             //    onEntered: { rectangle1.border.width = 2 }
              //   onExited: { rectangle1.border.width = 1 }
                onClicked: { imgpack.buttonClicked();
                                 uartport.init_io_port()      }
           }
}
}
