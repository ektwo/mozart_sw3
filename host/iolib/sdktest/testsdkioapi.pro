QT += quick serialport
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target




#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-kneronSDKIO-Desktop_Qt_5_11_1_MSVC2017_64bit-Release/release/ -lkneronSDKIO
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-kneronSDKIO-Desktop_Qt_5_11_1_MSVC2017_64bit-Release/release/ -lkneronSDKIOd

INCLUDEPATH += D:/kneron_io_mozart
DEPENDPATH += D:/kneron_io_mozart



INCLUDEPATH += D:/Download/libusb-win32-bin-1.2.6.0/include
DEPENDPATH += D:/Download/libusb-win32-bin-1.2.6.0/include


INCLUDEPATH += $$PWD/../../Download/libusb-win32-bin-1.2.6.0/include
DEPENDPATH += $$PWD/../../Download/libusb-win32-bin-1.2.6.0/include

DISTFILES += \
    ../../build-kneronSDKIO-Desktop_Qt_5_12_1_MinGW_64_bit-Debug/debug/libkneronSDKIO.a


win32: LIBS += -L$$PWD/../../build-kneronSDKIO-Desktop_Qt_5_12_1_MinGW_64_bit-Debug/debug/ -lkneronSDKIO

INCLUDEPATH += $$PWD/../
DEPENDPATH += $$PWD/../

win32:!win32-g++: PRE_TARGETDEPS += $$PWD/../../build-kneronSDKIO-Desktop_Qt_5_12_1_MinGW_64_bit-Debug/debug/kneronSDKIO.lib
else:win32-g++: PRE_TARGETDEPS += $$PWD/../../build-kneronSDKIO-Desktop_Qt_5_12_1_MinGW_64_bit-Debug/debug/libkneronSDKIO.a

win32: LIBS += -L$$PWD/../../Download/libusb-win32-bin-1.2.6.0/lib/msvc_x64/ -llibusb

INCLUDEPATH += $$PWD/../../Download/libusb-win32-bin-1.2.6.0/include
DEPENDPATH += $$PWD/../../Download/libusb-win32-bin-1.2.6.0/include
