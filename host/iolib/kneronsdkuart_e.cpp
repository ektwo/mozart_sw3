/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkuart_e.cpp
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */

#include <QSerialPortInfo>
#include <QSerialPort>
#include <QString>
#include "kneronsdkuart_e.h"



UARTPort::UARTPort(QObject *parent):
  QObject(parent)
{
  p_uartport_i = new (UARTPort_i);
  m_infos = p_uartport_i->m_infos;
  m_port_num = p_uartport_i->m_port_num;
}

bool UARTPort::init_io_port(const QString portName)
{
  if (nullptr == p_uartport_i)
      return false;
  return p_uartport_i->init_io_port(portName);
}
bool UARTPort::open()
{
   bool status;
   status = p_uartport_i->open();
   return status;
}
void UARTPort::close()
{
    p_uartport_i->close();

}

bool UARTPort::send_sync_to_io()
{
  bool status = false;

  status = p_uartport_i->send_sync_to_io();
  return status;
}


