/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkusb.cpp
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#include <lusb0_usb.h>
#include <QString>
#include "kneronsdkio_i.h"
#include "kneronsdkio.h"

static  usb_dev_handle *m_pkneron_device_h = nullptr;
static bool send_data_to_io(char *p_wBuffer, long length);


USBPort_i::USBPort_i()
{

}


USBPort_i::~USBPort_i()
{
  if (nullptr != m_pkneron_device_h) {
    usb_release_interface(m_pkneron_device_h, 0);
    usb_close(m_pkneron_device_h);
  }
}

//bool USBPort_i::send_sync_to_io(UARTPort_i &uart_port_i) const
//{
//  char command[2] =  {SYNC_USB, 0};
//  bool status = false;
//  char ReadSyncBuffer[5] = { 0, 0, 0, 0, 0};

//  QString tag = "SYNC";

//  if (false == (status = uart_port_i.send_command_to_io(command)))
//      return status;

//  char challenge[8] = "KNERON";
//  if (false == (status =recv_data_ack()))
//      return status;
//  if (false == (status = send_data_to_io(challenge, sizeof challenge)))
//      return status;

//  if (false == (status = read_result_or_status_from_io(ReadSyncBuffer, sizeof ReadSyncBuffer)))
//    return status;

//  QString tag_r(ReadSyncBuffer);
//  if (0 != QString::compare(tag, tag_r, Qt::CaseInsensitive)) {
//        status = false;
//  }
//  return status;
//}

bool USBPort_i::init_io_port()
{
  int status;

  usb_init();
  if (0 > (status = usb_find_busses())) {
    printf("no usb bus is found, error %d\r\n", status);
    return false;
  }
  //  printf("usb_find_busses, error %d", status);
  if (0 > (status = usb_find_devices())) {
      printf("no usb device is found, error %d", status);
      return false;
  }
   // printf("usb_find_devices, error %d", status);
  struct usb_bus *bus;
  struct usb_device *dev;

  for (bus = usb_get_busses(); bus; bus = bus->next)
  {
    for (dev = bus->devices; dev; dev = dev->next)
    {
      if (dev->descriptor.idVendor == VENDOR_ID
        && dev->descriptor.idProduct == PRODUCT_ID)
        {
          p_kneron_dev = dev;
          return true;
        }
     }
  }
  printf("no kenron usb device is found, error %d", status);
  return false;
}
bool USBPort_i::open() const
{
  int status;
  if (nullptr == p_kneron_dev)
    return false;

  if (nullptr == (m_pkneron_device_h = usb_open(p_kneron_dev)))
  {
      printf("usb device VID %d, PID %d open failed\n\r ", VENDOR_ID, PRODUCT_ID);
      return false;
  }
  if (0 != (status =  usb_set_configuration(m_pkneron_device_h, 1)))
  {
      printf("error setting config %d:\r\n",  status);
      usb_close(m_pkneron_device_h);
     return false;
  }


 if (0 != (status = usb_claim_interface(m_pkneron_device_h, 0))) {
      printf ("claim inserface, error = %d\r\n", status);
      usb_close(m_pkneron_device_h);
      return false;
  }
  return true;
}

void USBPort_i::close() const
{
  if (nullptr != m_pkneron_device_h) {
    usb_release_interface(m_pkneron_device_h, 0);
    usb_close(m_pkneron_device_h);
    m_pkneron_device_h = nullptr;

  }
}
static bool send_data_to_io(char *p_wBuffer, long length)
{
  int status = 0;

  if (nullptr == m_pkneron_device_h)
      return false;

  if (0 >= (status = usb_bulk_write(m_pkneron_device_h, EP_OUT, p_wBuffer, length, 1000))) {
      printf("write usb error %d\n\r", status);
      return false;
   }

  return true;
}

bool USBPort_i::recv_data_ack() const
{
  bool status = false;
  char ReadBuffer[ACK_PACKET_SIZE] = { 0, 0, 0, 0,0,0,0,0};
  uint8_t ACK[8] = {0x35, 0x8A, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00};


  if (false == (status = read_result_or_status_from_io(ReadBuffer, ACK_PACKET_SIZE)))
    return status;
  for(int i = 0; i < ACK_PACKET_SIZE; i++) {
      if (ACK[i] != ReadBuffer[i]) {
          status =  false;
          break;
      }
     }
  return status;
}

bool USBPort_i::send_image_data_block_to_io(char *buf, long length) const
{
  bool status = false;


    if (false == (status = send_data_to_io(buf, length))) {
        //  printf("status is false from send_data_to_io");
          return false;
    }

  return status;
}

bool USBPort_i::read_result_or_status_from_io(char *p_rBuffer, int length) const
{
  bool status = false;
  int32_t ret_val;
  if (nullptr == m_pkneron_device_h)
      return status;
  if ( 0 >=  (ret_val = usb_bulk_read(m_pkneron_device_h, EP_IN , p_rBuffer, length, 2000))) {
      printf("usb er read retun error code %d\n\r", ret_val);
      return status;
  }
 // printf("usb read retun  code %x\n\r", ret_val);
  return true;
}


