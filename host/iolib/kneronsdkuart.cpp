/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkuart.cpp
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#include <QSerialPortInfo>
#include <QString>
#include <QSerialPort>
#include <QTime>
#include <QObject>
#include "kneronsdkio_i.h"
#include "kneronsdkio.h"
static QSerialPort *m_serialPort;
static QString m_portName;

static int m_waitMsecs = 1000;

static bool send_data_to_io(const char *p_wBuffer, qint64 num);
UARTPort_i::UARTPort_i()
{
  m_infos = QSerialPortInfo::availablePorts();
  m_port_num = m_infos.size();

}

UARTPort_i::~UARTPort_i()
{
  if (nullptr != m_serialPort) {
    m_serialPort->close();
    delete m_serialPort;
  }
}

bool UARTPort_i::open() const
{
  bool status = false;
  status = m_serialPort->open(QIODevice::ReadWrite);
  return status;
}

void UARTPort_i::close() const
{
  m_serialPort->close();
}
bool UARTPort_i::port_is_busy(const QString portname) const
{
  bool status = false;

  for (const QSerialPortInfo &serialPortInfo : m_infos) {
    if (0 == QString::compare(serialPortInfo.portName(), portname, Qt::CaseInsensitive)) {
      if (true == serialPortInfo.isBusy()) {
        status = true;
      }
      return status;
    }
  }
  return status;
}

bool UARTPort_i::init_io_port(const QString portName)
{
  bool status = false;
  if (true ==  port_is_busy(portName)) {
      printf("port is busy\n\r");
      return status;
  }
  m_portName  = portName;
  m_serialPort = new QSerialPort(portName, nullptr);
  m_serialPort->setBaudRate(QSerialPort::Baud115200);
  m_serialPort->setFlowControl(QSerialPort::NoFlowControl);
  m_serialPort->setParity(QSerialPort::NoParity);
  m_serialPort->setDataBits(QSerialPort::Data8);
  m_serialPort->setStopBits(QSerialPort::OneStop);
  status = true;
  return status;
}

bool UARTPort_i::send_command_to_io(void *buffer, qint64 len) const
{
  bool status = false;

  if (nullptr == m_serialPort) {

      printf("uart object is null \n\r");
  }

  status = send_data_to_io((const char *) buffer, len);


  return status;
}

//bool UARTPort_i::send_sync_to_io() const
//{
//  bool status = false;
//  char command[2] =  {SYNC_UART, 0};
//  char ReadSyncBuffer[5] = { 0, 0, 0, 0, 0};

//  if (nullptr == m_serialPort) {

//      printf("uart object is null \n\r");
//  }

//  QString tag = "SYNC\0";

//  if (false == (status = send_data_to_io(command, sizeof command[2])))
//  {
//      printf("send uart sync command error\r\n");
//      return status;
//  }
//  if (false == (status =read_result_or_status_from_io(ReadSyncBuffer, SYNC_PACKET_SIZE))) {
//      printf("read result error %s\n\r", ReadSyncBuffer);
//      return status;
//  }
//  QString tag_r(ReadSyncBuffer);

//  if (0 == QString::compare(tag, tag_r, Qt::CaseInsensitive)) {
//      status = true;
//  }
//  else
//  {
//      status = false;
//      printf("sync message error\r\n");
//  }
//  return status;
//}

static bool send_data_to_io(const char *p_wBuffer, qint64 num)
{
  qint64 ret_num;
  bool status = true;

  if (nullptr == m_serialPort) {
      printf("uart port is empty\r\n");
      return status;
  }

  ret_num = m_serialPort->write(p_wBuffer, num);

  if (false == m_serialPort->waitForBytesWritten(m_waitMsecs)) {
     //  emit timeout(tr("Wait write request timeout %1")
          //     .arg(QTime::currentTime().toString()));
       status = false;
  }
  if (ret_num != num)
      status = false;
   return status;
}

bool UARTPort_i::send_image_data_block_to_io(const char *buf, qint64 length) const
{
  bool status = true;
  off_t off = 0;
  qint64 num = length / static_cast<int>(UART_MAX_BYTES);
  int leftover = length % static_cast<int>(UART_MAX_BYTES);

  for (int i = 0; i < num; i++)
  {
    if (false == (status = send_data_to_io(buf + off, 64)))
        return status;

    if (false == (status = recv_data_ack()))
        return status;
       off += UART_MAX_BYTES;
  }
  if (leftover != 0)
  {
    if (false == (status = send_data_to_io(buf + off, leftover)))
        return status;
    if (false == (status = recv_data_ack()))
        return status;
  }
  return status;
}

bool UARTPort_i::read_result_or_status_from_io(char* p_rBuffer, qint64 len) const
{
  bool status = false;
  qint64 num_read = 0, num_read_total = 0;
  qint64 leng_tmp = len;
  if (nullptr == m_serialPort)
      return status;

  m_serialPort->waitForReadyRead(m_waitMsecs);
  for (;;) {
    num_read = m_serialPort->read(p_rBuffer, leng_tmp);

    if (num_read < 0) {
        status = false;
        return status;
    }
    num_read_total += num_read;
    leng_tmp -= num_read;
    if (num_read_total == len) {
      return true;
    }
    if (leng_tmp == 0 && !m_serialPort->waitForReadyRead(m_waitMsecs))
      break;
  }
   return status;
}

bool UARTPort_i::read_result_or_status_from_io_line(char* p_rBuffer) const
{
  bool status = false;
  qint64 num_read;
  if (nullptr == m_serialPort)
    return status;

  m_serialPort->waitForReadyRead(m_waitMsecs);
  num_read = m_serialPort->readLine(p_rBuffer, 1024);;

  if (num_read < 0) {
     status = false;
     return status;
  }
  return status;
}

bool UARTPort_i::recv_data_ack() const
{
  bool status = true;
  char ReadBuffer[ACK_PACKET_SIZE] = { 0, 0, 0, 0,0,0,0,0};
  uint8_t ACK[8] = {0x35, 0x8A, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00};

  if (false == (status = read_result_or_status_from_io(ReadBuffer, ACK_PACKET_SIZE)))
    return status;
  for(int i = 0; i < ACK_PACKET_SIZE; i++) {
      if (ACK[i] != ReadBuffer[i]) {
          status =  false;
          break;
     }
   }


  return status;
}


