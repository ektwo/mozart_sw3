/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkusb_e.cpp
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */

#include "kneronsdkusb_e.h"
#include <QString>

USBPort::USBPort(QObject *parent):
    QObject(parent)
{
  p_usbport_i = new (USBPort_i);

}

USBPort::~USBPort ()
{
  delete p_usbport_i;
}

bool USBPort::init_io_port()
{
  bool status = false;
  status = p_usbport_i->init_io_port();

  return status;
}
bool USBPort::open()
{
  return p_usbport_i->open();
}

void USBPort::close()
{
  return p_usbport_i->close();
}
bool USBPort::send_sync_to_io(UARTPort &uart_port)
{
   bool status = false;
   if (nullptr == uart_port.p_uartport_i)
       return status;
   status = p_usbport_i->send_sync_to_io(*uart_port.p_uartport_i);
   return status;
}




