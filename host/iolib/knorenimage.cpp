/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkimage_e.cpp
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#include <QFile>
#include <QDataStream>
#include <QImageReader>
#include <iostream>
#include <fstream>
#include "kneronimage_i.h"
#include "knorenimage.h"

KneronImage::KneronImage()
{
}
KneronImage::~KneronImage()
{
}



static bool make_image_package(QStringList &file_name_list,  char **buffer, long *len)
{
  bool status = false;
  uint32_t total_size = 0;

  for (auto it : file_name_list) {
    QFile file(it);
    if (false == (status = file.open(QIODevice::ReadOnly)))
      return  status;
    total_size += file.size();
    file.close();
  }
  *len = static_cast<long>(total_size);


  char *p = new char[total_size];
  *buffer = p;



  for (auto it : file_name_list) {
      QFile file(it);
      if (false == (status = file.open(QIODevice::ReadOnly)))
        return  status;
      QByteArray buf;
      buf = file.readAll();
      memcpy_s(p, static_cast<rsize_t>(file.size()), buf.data(), static_cast<rsize_t>(file.size()));
      p +=file.size();
      file.close();
  }

  return status;
}
static bool send_file(QStringList &file_name_list, USBPort_i &usb_port)
{
    bool status = false;
    char *buffer;

    long length;

    if (false == (status = make_image_package(file_name_list,  &buffer, &length)))
      return status;


    if (false == (status = usb_port.send_image_data_block_to_io(buffer, length)))
        return status;

      delete[](buffer);
    return status;
}
static bool send_file(QStringList &file_name_list,  UARTPort_i &uart_port)
{
    bool status = false;
    char *buffer;

    long length;

    if (false == (status = make_image_package(file_name_list, &buffer, &length)))
      return status;



    if (false == (status = uart_port.send_image_data_block_to_io(buffer, length)))
        return status;

    delete[](buffer);
    return status;
}


static bool send_buffer(uint8_t *buf, long len,  USBPort_i &usb_port)
{
    bool status = false;
  //  char *buffer;



    if ((0 == len) || (nullptr == buf))
        return status;

    if (false ==(status = usb_port.send_image_data_block_to_io((char *)buf, len)))
        return status;

    return status;
}

static bool send_buffer(uint8_t *buf, long len,  UARTPort_i &uart_port)
{
    bool status = false;
  //  char *buffer;



    if ((0 == len) || (nullptr == buf))
        return status;

    if (false ==(status = uart_port.send_image_data_block_to_io((char *)buf, len)))
        return status;

    return status;
}
bool KneronImage::send_image_file(QStringList &file_name_list,  USBPort_i &usb_port) const
{
  bool status;
  status = send_file(file_name_list, usb_port);
  return status;
}

bool KneronImage::send_image_file(QStringList &file_name_list,  UARTPort_i &uart_port) const
{
  bool status;
  status = send_file(file_name_list,  uart_port);
  return status;
}
bool KneronImage::send_image_buffer(uint8_t *buf,  long len,  UARTPort_i &uart_port) const
{
    bool status;
    status = send_buffer(buf, len,  uart_port);
    return status;
}
bool KneronImage::send_image_buffer(uint8_t *buf,  long len,  USBPort_i &usb_port) const
{
    bool status;
    status = send_buffer(buf, len,  usb_port);

    return status;
}

