/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkpkg_e.cpp
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#include <QSerialPortInfo>
#include <QSerialPort>
#include <QString>
#include "kneronsdkpkg_e.h"

KneronPKG::KneronPKG(QObject *parent):
  QObject(parent)
{
  p_img_i = new (KneronImage);

}



bool KneronPKG::send_image_file(QStringList &file_name_list,  USBPort &usb_port) const
{
  bool status = false;
  if (nullptr == usb_port.p_usbport_i)
      return status;
  status = p_img_i->send_image_file(file_name_list, *usb_port.p_usbport_i);
  return status;
}

bool KneronPKG::send_image_file(QStringList &file_name, UARTPort &uart_port) const
{
    bool status = false;
  if (nullptr == uart_port.p_uartport_i)
      return status;
  status = p_img_i->send_image_file(file_name,  *uart_port.p_uartport_i);
  return status;
}

bool KneronPKG::send_image_buffer(uint8_t *buf, long len, UARTPort &uart_port) const
{
    bool status = false;
    if (nullptr == uart_port.p_uartport_i)
      return status;
    status = p_img_i->send_image_buffer(buf, len,  *uart_port.p_uartport_i);
    return status;
}
bool KneronPKG::send_image_buffer(uint8_t *buf, long len,  USBPort &usb_port) const
{
   bool status = false;
   if (nullptr == usb_port.p_usbport_i)
       return status;
   status = p_img_i->send_image_buffer(buf, len,  *usb_port.p_usbport_i);
   return status;
}

