#-------------------------------------------------
#
# Project created by QtCreator 2018-08-07T15:03:45
#
#-------------------------------------------------

QT       += widgets serialport

QT       -= gui
CONFIG += staticlib
TARGET = kneronSDKIO
TEMPLATE = lib

DEFINES += KNERONSDKIO_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
QMAKE_CXXFLAGS += -Wattributes
# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    kneronsdkuart.cpp \
    kneronsdkusb.cpp \
    kneronsdkuart_e.cpp \
    kneronsdkusb_e.cpp \
    kneronsdkcommand.cpp


HEADERS += \
    kneronsdkuart_e.h \
    kneronsdkusb_e.h \
    kneronsdkcommand.h \
    kneronsdkio_i.h \
    kneronsdkio.h \
    lusb0_usb.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}






win32:CONFIG(release, debug|release): LIBS += -LD:/Download/libusb-win32-bin-1.2.6.0/lib/msvc_x64/ -llibusb
else:win32:CONFIG(debug, debug|release): LIBS += -LD:/Download/libusb-win32-bin-1.2.6.0/lib/msvc_x64/ -llibusbd

INCLUDEPATH += D:/Download/libusb-win32-bin-1.2.6.0/lib/msvc_x64
DEPENDPATH += D:/Download/libusb-win32-bin-1.2.6.0/lib/msvc_x64
