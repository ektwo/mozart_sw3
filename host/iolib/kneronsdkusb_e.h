/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkusb_e.h
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#ifndef KNERONSDKUSB_H
#define KNERONSDKUSB_H
#include <QObject>
#include <lusb0_usb.h>
#include <QString>
#include "kneronsdkio.h"
#include "kneronsdkuart_e.h"
class USBPort: public QObject
{

  Q_OBJECT
 // Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)

  public:
    explicit USBPort(QObject *parent = nullptr);
    ~USBPort();
    USBPort_i *p_usbport_i;

  public slots:
    bool open();
    void close();
    bool init_io_port();
    bool send_sync_to_io(UARTPort &uart_port);
};
#endif // KNERONSDKUSB_H
