/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkcommand.cpp
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#include <QSerialPortInfo>
#include <QString>
#include <QSerialPort>
#include "kneronsdkcommand.h"
#include "kneronsdkuart_e.h"
#include "kneronsdkio_i.h"
#include "kneronsdkio.h"
static uint8_t ACK[8] = {0x35, 0x8A, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00};

#include <stdbool.h>
#include <stdlib.h>
//#include "checksum.h"
#define		CRC_POLY_16		0xA001
#define		CRC_START_16	0x0000
static void             init_crc16_tab( void );

static bool             crc_tab16_init          = false;
static uint16_t         crc_tab16[256];

/*
 * uint16_t crc_16( const unsigned char *input_str, size_t num_bytes );
 *
 * The function crc_16() calculates the 16 bits CRC16 in one pass for a byte
 * string of which the beginning has been passed to the function. The number of
 * bytes to check is also a parameter. The number of the bytes in the string is
 * limited by the constant SIZE_MAX.
 */

uint16_t crc_16( const unsigned char *input_str, size_t num_bytes ) {

    uint16_t crc;
    const unsigned char *ptr;
    size_t a;

    if ( ! crc_tab16_init ) init_crc16_tab();

    crc = CRC_START_16;
    ptr = input_str;

    if ( ptr != nullptr ) for (a=0; a<num_bytes; a++) {

        crc = (crc >> 8) ^ crc_tab16[ (crc ^ (uint16_t) *ptr++) & 0x00FF ];
    }

    return crc;

}  /* crc_16 */



/*
 * uint16_t update_crc_16( uint16_t crc, unsigned char c );
 *
 * The function update_crc_16() calculates a new CRC-16 value based on the
 * previous value of the CRC and the next byte of data to be checked.
 */

uint16_t update_crc_16( uint16_t crc, unsigned char c ) {

    if ( ! crc_tab16_init ) init_crc16_tab();

    return (crc >> 8) ^ crc_tab16[ (crc ^ (uint16_t) c) & 0x00FF ];

}  /* update_crc_16 */

/*
 * static void init_crc16_tab( void );
 *
 * For optimal performance uses the CRC16 routine a lookup table with values
 * that can be used directly in the XOR arithmetic in the algorithm. This
 * lookup table is calculated by the init_crc16_tab() routine, the first time
 * the CRC function is called.
 */

static void init_crc16_tab( void ) {

    uint16_t i;
    uint16_t j;
    uint16_t crc;
    uint16_t c;

    for (i=0; i<256; i++) {

        crc = 0;
        c   = i;

        for (j=0; j<8; j++) {

            if ( (crc ^ c) & 0x0001 ) crc = ( crc >> 1 ) ^ CRC_POLY_16;
            else                      crc =   crc >> 1;

            c = c >> 1;
        }

        crc_tab16[i] = crc;
    }

    crc_tab16_init = true;

}  /* init_crc16_tab */



Command_i::Command_i()
{


}

Command_i::~Command_i()
{

}

//typedef struct test_input {
//  uint32_t addr;
//  uint32_t len;
//  bool crc;
//  void *data;

//} test_input_t;

//typedef struct test_output {
//  uint16_t rsp_type;
//  uint32_t *act_out_len;
//  uint32_t *err;
//  void *data;
//} test_output_t;

bool test_send_packet(UARTPort_i &uart_port, packet_t *packet, uint32_t len)
{
  bool status = false;
  packet->premble = PKTX_PAMB;
  if (packet->pack_payload_attr.crc == 1) {
     packet->pack_payload_attr.payload_size += sizeof (packet->crc);
     packet->crc = crc_16((const unsigned char *)&packet, sizeof (packet) - sizeof (packet->crc) + len);
     if (false == (status = uart_port.send_command_to_io((void * )&packet, sizeof(packet) + len)))
         return status;
    }
  else {

     if (false == (status = uart_port.send_command_to_io((void * )&packet, sizeof(packet) - sizeof (packet->crc) + len)))
         return status;
  }

  return status;
}

bool get_respond_data(UARTPort_i &uart_port,  uint8_t *data, uint32_t len)
{
  bool status;
  if (false == (status = uart_port.read_result_or_status_from_io((char *)data, len)))
    return status;
}

bool get_respond_packet(UARTPort_i &uart_port,  packet_r_t *packet_r)
{
  bool status = false;

  if (false == (status = uart_port.read_result_or_status_from_io((char * ) packet_r, sizeof (packet_r_t) - sizeof (packet_r->crc))))
      return status;

   if (packet_r->premble != PKRX_PAMB) {
     printf("this  is not a kneron packet\r\n");
     return false;
   }
  return status;
}

bool Command_i::test_mem_read_cmd(UARTPort_i &uart_port, test_input *p_input, test_output *p_output) const
{
  bool status = false;
  packet_t packet;
  packet.pack_payload_attr.rw = PKT_W_FLAG;
  packet.pack_payload_attr.crc = p_input->crc;
  packet.pack_payload_attr.nack = 0;
  packet.pack_payload_attr.rsvd = 0;
  packet.pack_payload_attr.payload_size = sizeof (packet.message);

  packet.message.type = CMD_MEM_READ;
  packet.message.length = sizeof(packet.message.para);
  packet.message.para.addr = p_input->addr;
  packet.message.para.num = p_input->len;

  if (false == (status = test_send_packet(uart_port, &packet, 0)))
     return status;

  packet_r_t packet_r;
  if (false == (status = get_respond_packet(uart_port,  &packet_r)))
     return status;
  if (packet_r.message.type != RESP_MEM_READ)
  {
    printf("this is not a expected package\r\n");
    return false;
  }
  p_output->err = packet_r.message.para.ret;
  if (packet_r.message.para.ret != 0) {
    printf("error code \r\n");
    return false;
  }
  uint8_t *tmp;

  uint16_t data_len = packet_r.message.para.num;
  if (packet_r.pack_payload_attr.crc == 1) {
     tmp = (uint8_t *) malloc(sizeof (packet_r) + data_len);
     data_len += sizeof packet_r.crc;
  }
  else {
      tmp = (uint8_t *) malloc(sizeof (packet_r) + data_len - sizeof packet_r.crc);

  }
  memcpy(tmp, &packet_r, sizeof (packet_r));
  if (data_len != 0)
  {
      get_respond_data(uart_port, (uint8_t *) tmp + sizeof (packet_r) - sizeof packet_r.crc, data_len);

  }
  if (packet_r.pack_payload_attr.crc == 1) {
    uint16_t crc16;

    crc16 = crc_16(tmp, sizeof (packet_r) + data_len -sizeof packet_r.crc);
    if (crc16 != packet_r.crc) {
      printf("respone packet crc error\n\r");
      return  false;
    }
    data_len -= sizeof packet_r.crc;
  }
  if (data_len != 0){
    memcpy((uint8_t *) p_output->data, tmp + sizeof (packet_r) - sizeof packet_r.crc, data_len);
  }
  p_output->err = packet_r.message.para.ret;
  p_output->act_out_len = packet_r.message.para.num;
  return status;
}

bool Command_i::test_mem_write_cmd(UARTPort_i &uart_port, test_input *p_input, test_output *p_output) const
{
  bool status = false;
  packet_t *p_packet;
  uint32_t w_len;
  if (p_input->crc) {
    p_packet = (packet_t *)calloc(sizeof (packet_t) + p_input->len, sizeof (uint8_t));
    p_packet->pack_payload_attr.payload_size +=sizeof (p_packet->crc);
    p_packet->pack_payload_attr.crc = PKT_CRC_FLAG;
    w_len = sizeof(packet_t) + p_input->len;
  }
  else {
    p_packet = (packet_t *)calloc(sizeof (packet_t) + p_input->len - sizeof (p_packet->crc), sizeof (uint8_t));
    w_len = sizeof(packet_t) + p_input->len - sizeof (p_packet->crc);
  }

  p_packet->pack_payload_attr.rw = 1;

  p_packet->pack_payload_attr.nack = 0;
  p_packet->pack_payload_attr.rsvd = 0;
  p_packet->pack_payload_attr.payload_size += sizeof (p_packet->message)  + p_input->len ;

  p_packet->message.type = CMD_MEM_WRITE;
  p_packet->message.length = sizeof(p_packet->message.para) + p_input->len;
  p_packet->message.para.addr = p_input->addr;
  p_packet->message.para.num = p_input->len;

  memcpy((uint8_t *) &p_packet + sizeof (packet_t) - sizeof (p_packet->crc), p_input->data, p_input->len);
  if (p_input->crc)
  {
     p_packet->pack_payload_attr.crc = 1;
     p_packet->pack_payload_attr.payload_size += sizeof (p_packet->crc);
     uint32_t crc16 = crc_16((const uint8_t *)p_packet, w_len- sizeof(packet_t));
     memcpy((uint8_t *) &p_packet + sizeof (packet_t) - sizeof (p_packet->crc)+ p_input->len, (uint8_t *)&crc16, sizeof (uint32_t));
  }

  if (false == (status = test_send_packet(uart_port, p_packet, w_len)))
     return status;

  packet_r_t packet_r;

  if (false == get_respond_packet(uart_port,  &packet_r))
     return status;

  if (packet_r.message.type != RESP_MEM_WRITE)
  {
    printf("this is not a expected package\r\n");
    return false;
  }
  p_output->err = packet_r.message.para.ret;
  if (packet_r.message.para.ret != 0)
     return false;
  p_output->act_out_len = packet_r.message.para.num;
  return status;
}

bool Command_i::test_mem_clr_cmd(UARTPort_i &uart_port, test_input *p_input, test_output *p_output) const
{
   bool status = false;
   packet_t packet;

   packet.pack_payload_attr.rw = 1;
   packet.pack_payload_attr.crc = 0;
   packet.pack_payload_attr.nack = 0;
   packet.pack_payload_attr.rsvd = 0;
   packet.pack_payload_attr.payload_size = sizeof (packet.message);

   packet.message.type = CMD_MEM_CLR;
   packet.message.length = sizeof(packet.message.para);
   packet.message.para.addr = p_input->addr;
   packet.message.para.num = p_input->len;

   uint32_t w_len = sizeof packet;
   if (p_input->crc)
   {
      packet.pack_payload_attr.crc = 1;
      packet.pack_payload_attr.payload_size += sizeof packet;
      packet.crc = crc_16((uint8_t *)&packet, w_len - sizeof packet);
    //  w_len += sizeof (packet.crc);
   }
   else {
       w_len -= sizeof (packet.crc);
   }
   if (false == (status = test_send_packet(uart_port, &packet, w_len)))
      return status;

   packet_r_t packet_r;
   if (false == get_respond_packet(uart_port,  &packet_r))
      return status;
  if (packet_r.message.type != RESP_MEM_CLR)
  {
    printf("this is not a expected package\r\n");
    return false;
  }
  p_output->err = packet_r.message.para.ret;
  if (packet_r.message.para.ret !=0 )
      return false;
  p_output->act_out_len = packet_r.message.para.num;
  return status;
}

bool Command_i::test_echo_cmd(UARTPort_i &uart_port, test_input *p_input, test_output *p_output) const
{
    bool status = false;
    packet_t *p_packet;
    uint32_t w_len;

    if (p_input->crc) {
      p_packet = (packet_t *)calloc(sizeof (packet_t) + p_input->len, sizeof (uint8_t));
      p_packet->pack_payload_attr.payload_size += sizeof (p_packet->crc);
      p_packet->pack_payload_attr.crc = PKT_CRC_FLAG;
      w_len = sizeof(packet_t) + p_input->len;
    }
    else {
      p_packet = (packet_t *)calloc(sizeof (packet_t) + p_input->len - sizeof (p_packet->crc), sizeof (uint8_t));
      w_len = sizeof(packet_t) + p_input->len - sizeof (p_packet->crc);
    }

    p_packet->pack_payload_attr.rw = 1;

    p_packet->pack_payload_attr.nack = 0;
    p_packet->pack_payload_attr.rsvd = 0;
    p_packet->pack_payload_attr.payload_size += sizeof (p_packet->message)  + p_input->len ;

    p_packet->message.type = CMD_TEST_ECHO;
    p_packet->message.length = sizeof(p_packet->message.para) + p_input->len;
    p_packet->message.para.addr = p_input->addr;
    p_packet->message.para.num = p_input->len;

    memcpy((uint8_t *) &p_packet + sizeof (packet_t) - sizeof (p_packet->crc), p_input->data, p_input->len);
    if (p_input->crc)
    {
       p_packet->pack_payload_attr.crc = 1;
       p_packet->pack_payload_attr.payload_size += sizeof (p_packet->crc);
       uint32_t crc16 = crc_16((const uint8_t *)p_packet, w_len - sizeof(packet_t));
       memcpy((uint8_t *) &p_packet + sizeof (packet_t) - sizeof (p_packet->crc)+ p_input->len, (uint8_t *)&crc16, sizeof (uint32_t));
    }

    if (false == (status = test_send_packet(uart_port, p_packet, w_len)))
       return status;

    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_TEST_ECHO)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    p_output->err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0) {
      printf("error code \r\n");
      return false;
    }
    uint8_t *tmp;

    uint16_t data_len = packet_r.message.para.num;
    if (packet_r.pack_payload_attr.crc == 1) {
       tmp = (uint8_t *) malloc(sizeof (packet_r) + data_len);
       data_len += sizeof packet_r.crc;
    }
    else {
        tmp = (uint8_t *) malloc(sizeof (packet_r) + data_len - sizeof packet_r.crc);

    }
    memcpy(tmp, &packet_r, sizeof (packet_r));
    if (data_len != 0)
    {
        get_respond_data(uart_port, (uint8_t *) tmp + sizeof (packet_r) - sizeof packet_r.crc, data_len);

    }
    if (packet_r.pack_payload_attr.crc == 1) {
      uint16_t crc16;

      crc16 = crc_16(tmp, sizeof (packet_r) + data_len -sizeof packet_r.crc);
      if (crc16 != packet_r.crc) {
        printf("respone packet crc error\n\r");
        return  false;
      }
      data_len -= sizeof packet_r.crc;
    }
    if (data_len != 0){
      memcpy((uint8_t *) p_output->data, tmp + sizeof (packet_r) - sizeof packet_r.crc, data_len);
    }
    p_output->err = packet_r.message.para.ret;
    p_output->act_out_len = packet_r.message.para.num;
    return status;

}
bool test_usb_mem_write(UARTPort_i &uart_port, test_input *p_input, test_output *p_output)
{
    bool status = false;
    packet_t packet;

    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_USB_WRITE;
    packet.message.length = sizeof(packet.message.para);
    packet.message.para.addr = p_input->addr;
    packet.message.para.num = p_input->len;

    uint32_t w_len = sizeof packet;
    if (p_input->crc)
    {
       packet.pack_payload_attr.crc = 1;
       packet.pack_payload_attr.payload_size += sizeof packet;
       packet.crc = crc_16((uint8_t *)&packet, w_len - sizeof packet);
     //  w_len += sizeof (packet.crc);
    }
    else {
        w_len -= sizeof (packet.crc);
    }
    if (false == (status = test_send_packet(uart_port, &packet, w_len)))
       return status;

    packet_r_t packet_r;
    if (false == get_respond_packet(uart_port,  &packet_r))
       return status;
   if (packet_r.message.type != RESP_USB_WRITE)
   {
     printf("this is not a expected package\r\n");
     return false;
   }
   p_output->err = packet_r.message.para.ret;
   if (packet_r.message.para.ret !=0 )
       return false;
   p_output->act_out_len = packet_r.message.para.num;
   return status;

}
bool Command_i::demo_set_models_cmd(UARTPort_i &uart_port,  demo_mode_input_t *p_mode_input, uint32_t *err) const
{
    bool status = false;
    packet_t *p_packet;
    uint32_t w_len;

    if (p_mode_input->crc) {
      p_packet = (packet_t *)calloc(sizeof (packet_t) + sizeof (p_mode_input->mode), sizeof (uint8_t));
      p_packet->pack_payload_attr.payload_size += sizeof (p_packet->crc);
      p_packet->pack_payload_attr.crc = PKT_CRC_FLAG;
      w_len = sizeof(packet_t) + sizeof (p_mode_input->mode);
    }
    else {
      p_packet = (packet_t *)calloc(sizeof (packet_t) + sizeof (p_mode_input->mode) - sizeof (p_packet->crc), sizeof (uint8_t));
      w_len = sizeof(packet_t) + sizeof (p_mode_input->mode) - sizeof (p_packet->crc);
    }

    p_packet->pack_payload_attr.rw = 1;

    p_packet->pack_payload_attr.nack = 0;
    p_packet->pack_payload_attr.rsvd = 0;
    p_packet->pack_payload_attr.payload_size += sizeof (p_packet->message)  + sizeof (p_mode_input->mode) ;

    p_packet->message.type = CMD_DEMO_SET_MODELS;
    p_packet->message.length = sizeof(p_packet->message.para) + sizeof (p_mode_input->mode);
    p_packet->message.para.addr = 0;
    p_packet->message.para.num = 0;

    memcpy((uint8_t *) &p_packet + sizeof (packet_t) - sizeof (p_packet->crc), &p_mode_input->mode, sizeof (p_mode_input->mode));
    if (p_mode_input->crc)
    {
       p_packet->pack_payload_attr.crc = 1;
       p_packet->pack_payload_attr.payload_size += sizeof (p_packet->crc);
       uint32_t crc16 = crc_16((const uint8_t *)p_packet, w_len - sizeof(packet_t));
       memcpy((uint8_t *) &p_packet + sizeof (packet_t) - sizeof (p_packet->crc)+ sizeof (p_mode_input->mode), (uint8_t *)&crc16, sizeof (uint32_t));
    }

    if (false == (status = test_send_packet(uart_port, p_packet, w_len)))
       return status;

     packet_r_t packet_r;
     if (false == (status = get_respond_packet(uart_port,  &packet_r)))
        return status;

     if (packet_r.message.type != RESP_DEMO_SET_MODELS)
     {
       printf("this is not a expected package\r\n");
       return false;
     }
     *err = packet_r.message.para.ret;
     if (packet_r.message.para.ret != 0) {
       printf("error code \r\n");
       return false;
     }

  return status;
}

bool Command_i::demo_set_images_cmd(UARTPort_i &uart_port, demo_img_input_t *p_img_input, uint32_t *err ) const
{
    bool status = false;
    packet_t *p_packet;
    uint32_t w_len;

    if (p_img_input->crc) {
      p_packet = (packet_t *)calloc(sizeof (packet_t) + sizeof (p_img_input->img), sizeof (uint8_t));
      p_packet->pack_payload_attr.payload_size += sizeof (p_packet->crc);
      p_packet->pack_payload_attr.crc = PKT_CRC_FLAG;
      w_len = sizeof(packet_t) + sizeof (p_img_input->img);
    }
    else {
      p_packet = (packet_t *)calloc(sizeof (packet_t) + sizeof (p_img_input->img) - sizeof (p_packet->crc), sizeof (uint8_t));
      w_len = sizeof(packet_t) + sizeof (p_img_input->img) - sizeof (p_packet->crc);
    }

    p_packet->pack_payload_attr.rw = 1;

    p_packet->pack_payload_attr.nack = 0;
    p_packet->pack_payload_attr.rsvd = 0;
    p_packet->pack_payload_attr.payload_size += sizeof (p_packet->message)  + sizeof (p_img_input->img) ;

    p_packet->message.type = CMD_DEMO_SET_IMAGES;
    p_packet->message.length = sizeof(p_packet->message.para) + sizeof (p_img_input->img);
    p_packet->message.para.addr = 0;
    p_packet->message.para.num = 0;

    memcpy((uint8_t *) &p_packet + sizeof (packet_t) - sizeof (p_packet->crc), &p_img_input->img, sizeof (p_img_input->img));
    if (p_img_input->crc)
    {
       p_packet->pack_payload_attr.crc = 1;
       p_packet->pack_payload_attr.payload_size += sizeof (p_packet->crc);
       uint32_t crc16 = crc_16((const uint8_t *)p_packet, w_len - sizeof(packet_t));
       memcpy((uint8_t *) &p_packet + sizeof (packet_t) - sizeof (p_packet->crc)+ sizeof (p_img_input->img), (uint8_t *)&crc16, sizeof (uint32_t));
    }

    if (false == (status = test_send_packet(uart_port, p_packet, w_len)))
       return status;

     packet_r_t packet_r;
     if (false == (status = get_respond_packet(uart_port,  &packet_r)))
        return status;

     if (packet_r.message.type != RESP_DEMO_SET_IMAGES)
     {
       printf("this is not a expected package\r\n");
       return false;
     }
     *err = packet_r.message.para.ret;
     if (packet_r.message.para.ret != 0) {
       printf("error code \r\n");
       return false;
     }

  return status;
}

bool Command_i::demo_run_cmd(UARTPort_i &uart_port, uint32_t *err) const
{
    bool status = false;
    packet_t packet;

    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_DEMO_RUN;
    packet.message.length = sizeof(packet.message.para);
    packet.message.para.num = 0;
    packet.message.para.addr = 0;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_DEMO_RUN)
    {
      printf("this is not a expected package\r\n");
      return false;
    }
    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0) {
      printf("error code \r\n");
      return false;
    }

    return status;
}
bool Command_i::demo_run_once_cmd(UARTPort_i &uart_port, uint32_t *err) const
{
    bool status = false;
    uint32_t w_len;
    packet_t packet;

    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_DEMO_RUN_ONCE;
    packet.message.length = sizeof(packet.message.para);
    packet.message.para.num = 0;
    packet.message.para.addr = 0;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != CMD_DEMO_RUN_ONCE)
    {
      printf("this is not a expected package\r\n");
      return false;
    }
    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0) {
      printf("error code \r\n");
      return false;
    }

    return status;


}
bool Command_i::demo_stop_cmd(UARTPort_i &uart_port, uint32_t *err) const
{

    bool status = false;

    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_DEMO_STOP;
    packet.message.length = sizeof(packet.message.para);
    packet.message.para.num = 0;
    packet.message.para.addr = 0;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_DEMO_STOP)
    {
      printf("this is not a expected package\r\n");
      return false;
    }
    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0) {
      printf("error code \r\n");
      return false;
    }

    return status;
}


bool Command_i::oper_reset_cmd(UARTPort_i &uart_port, uint32_t mod_ctl, uint32_t  confirm, uint32_t *err) const
{
    bool status = false;

    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_RESET;
    packet.message.length = sizeof(packet.message.para);
    packet.message.para.num = mod_ctl;
    packet.message.para.addr = confirm;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_RESET)
    {
      printf("this is not a expected package\r\n");
      return false;
    }
    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0) {
      printf("error code \r\n");
      return false;
    }

    return status;



}
bool Command_i::oper_system_status_cmd(UARTPort_i &uart_port, uint32_t *dev_id, uint32_t *firmware_id) const
{
    bool status = false;

    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_SYSTEM_STATUS;
    packet.message.length = sizeof(packet.message.para);
    packet.message.para.num = 0;
    packet.message.para.addr = 0;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SYSTEM_STATUS)
    {
      printf("this is not a expected package\r\n");
      return false;
    }
    *dev_id = packet_r.message.para.ret;
    *firmware_id = packet_r.message.para.num;


    return status;
}


bool Command_i::oper_query_apps_cmd(UARTPort_i &uart_port, uint8_t *app_id, uint32_t *len) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_QUERY_APPS;
    packet.message.length = 0;
    packet.message.para.num = 0;
    packet.message.para.addr = 0;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_QUERY_APPS)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    uint16_t data_len = packet_r.message.length - sizeof (packet_r.message.para.ret) -  sizeof (packet_r.message.para.num);
    uint8_t *ptmp_appid = (uint8_t *) calloc(data_len, sizeof(uint8_t));
    if (data_len != 0)
    {
        get_respond_data(uart_port,  ptmp_appid, data_len);

    }
   memcpy(app_id, ptmp_appid, data_len);
   *len = data_len;
    return status;
}

bool Command_i::oper_select_app_cmd(UARTPort_i &uart_port,  uint32_t app_id, uint32_t *buf_size, uint32_t *err) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_SELECT_APP;
    packet.message.length = 0;
    packet.message.para.num = 0;
    packet.message.para.addr = app_id;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SELECT_APP)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0)
         return false;
    *buf_size = packet_r.message.para.num;
    return status;
}

bool Command_i::oper_set_processimg_mode_cmd(UARTPort_i &uart_port, uint32_t img_sequ_num,uint32_t mode,  uint32_t *err) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_SET_PROCESSING_MODE;
    packet.message.length = sizeof (packet.message.para);
    packet.message.para.num = mode;
    packet.message.para.addr = img_sequ_num;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SET_PROCESSING_MODE)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0)
         return false;

    return status;
}
bool oper_status_update_cmd(UARTPort_i &uart_port, uint32_t *update_event, uint32_t *event_detail)
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_STATUS_UPDATE;
    packet.message.length = sizeof(packet.message.para.addr);
    packet.message.para.num = 0;
    packet.message.para.addr = 0;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_STATUS_UPDATE)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    *update_event = packet_r.message.para.ret;
    *event_detail = packet_r.message.para.num;
    return status;

}
bool Command_i::oper_set_update_event_cmd(UARTPort_i &uart_port, uint32_t status_flag, uint32_t *flag_mask) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_SET_UPDATE_EVENT;
    packet.message.length = sizeof (packet.message.para);
    packet.message.para.num = 0;
    packet.message.para.addr = status_flag;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SET_UPDATE_EVENT)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    *flag_mask = packet_r.message.para.ret;


  return status;
}

bool Command_i::oper_abort_image_cmd(UARTPort_i &uart_port, uint32_t img_seq_no, uint32_t *err,uint32_t *seq_no_r ) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_ABORT_IMAGE;
    packet.message.length = sizeof (packet.message.para);
    packet.message.para.num = 0;
    packet.message.para.addr = 0;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_ABORT_IMAGE)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0)
         return false;
  *seq_no_r = packet_r.message.para.num;
  return status;
}


bool Command_i::fid_set_fid_mode_cmd(UARTPort_i &uart_port, fid_mode_input_t *p_input, fid_mode_output_t *p_output) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message) + sizeof(p_input->password);

    packet.message.type = CMD_SET_FID_MODE;
    packet.message.length = sizeof (packet.message.para) + sizeof(p_input->password);
    packet.message.para.num = p_input->verif;
    packet.message.para.addr = p_input->mode;
    if ((p_input->mode == 128) || (p_input->mode == 255))
        packet.message.length += sizeof(p_input->conf_para);
    packet.crc = p_input->password;
    if ((p_input->mode != 128) && (p_input->mode != 255))
    {
       // packet.crc = p_input->password;
        test_send_packet(uart_port, &packet, 0);
    }
    else {
         uint8_t *tmp = (uint8_t * )calloc(sizeof packet + sizeof p_input->conf_para, sizeof(uint8_t));
         memcpy(tmp, &packet, sizeof packet);
         memcpy(tmp + sizeof packet,  &p_input->conf_para, sizeof (p_input->conf_para));
         test_send_packet(uart_port, &packet, sizeof (p_input->conf_para) + sizeof (p_input->password));
    }
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SET_FID_MODE)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    p_output->ret = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0)
         return false;
    p_output->new_fid = packet_r.message.para.num & 0xffff;
    if (false == (status = get_respond_data(uart_port,p_output->unit_info,  12)))
       return status;
  return status;
}

bool Command_i::fid_verify_fid_user_cmd(UARTPort_i &uart_port, fid_verify_fid_user_input *p_input, fid_verify_fid_user_output *p_output) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_VERIFY_FID_USER;
    packet.message.length = sizeof (packet.message.para);
    packet.message.para.num = p_input->subcommand;
    packet.message.para.addr = 0;
    packet.crc = p_input->user_id | (p_input->img_idx << 16);

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_VERIFY_FID_USER)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    p_output->error = packet_r.message.para.ret;

    if (packet_r.message.para.ret != 0)
         return false;
    p_output->user_id = packet_r.message.para.num;
      return status;
}
bool Command_i::fid_new_fid_user_cmd(UARTPort_i &uart_port, fid_cmd_new_fid_user_input_t *p_input,fid_cmd_new_fid_user_output_t *p_output) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_NEW_FID_USER;
    packet.message.length = sizeof (packet.message.para);
    packet.message.para.num = p_input->img_idx;
    packet.message.para.addr = p_input->subcommand;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_NEW_FID_USER)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    p_output->ret = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0)
         return false;
    p_output->img_idx = packet_r.message.para.num & 0xffff;
    p_output->user_id = (packet_r.message.para.num >> 16);



    return status;

}
bool Command_i::fid_edit_fid_user_cmd(UARTPort_i &uart_port, fid_edit_fid_user_input_t *p_input,fid_edit_fid_user_output_t *p_output) const
{

    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_EDIT_FID_USER;
    packet.message.length = sizeof (packet.message.para) + sizeof (p_input->img_idx) + sizeof (p_input->reserved);
    packet.message.para.num = p_input->user_id;
    packet.message.para.addr = p_input->subcommand;
    packet.crc = p_input->img_idx;
    packet.pack_payload_attr.payload_size +=  sizeof (p_input->img_idx) + sizeof (p_input->reserved);
    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_EDIT_FID_USER)
    {
      printf("this is not a expected package\r\n");
      return false;
    }
    uint32_t len = packet_r.message.length - sizeof (packet_r.message.para.num) - sizeof (packet_r.message.para.ret);
    uint8_t *p = (uint8_t * )calloc(2048, 1);
    if (false == (status = get_respond_data(uart_port, p, len)))
       return status;
    p_output->ret = packet_r.message.para.ret;
    p_output->subcommand = packet_r.message.para.num & 0xffff;
    p_output->user_id = packet_r.message.para.num >> 16;

    if (p_output->subcommand == 0x22)
        memcpy(p_output->data, p, 1028);
    if (p_output->subcommand == 0x49)
        memcpy(p_output->data, p, p_output->user_id *2);
    if (packet_r.message.para.ret != 0)
         return false;
    free(p);

    return status;

}

bool Command_i::sfid_start_cmd(UARTPort_i &uart_port,  uint32_t *img_size, uint32_t *err) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_SFID_START;
    packet.message.length = sizeof (packet.message.para);
    packet.message.para.num = 0;
    packet.message.para.addr = 0;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SFID_START)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0)
         return false;

    *img_size = packet_r.message.para.num;
    return status;


}
bool Command_i::sfid_new_user_cmd(UARTPort_i &uart_port, uint32_t usr_id, uint32_t img_idx) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message);

    packet.message.type = CMD_SFID_NEW_USER;
    packet.message.length = sizeof (packet.message.para);
    packet.message.para.num = img_idx;
    packet.message.para.addr = usr_id;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SFID_NEW_USER)
    {
      printf("this is not a expected package\r\n");
      return false;
    }




    return status;


}
bool Command_i::sfid_add_db_cmd(UARTPort_i &uart_port, uint32_t user_id, uint32_t *err) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message) - sizeof (packet.message.para.num);

    packet.message.type = CMD_SFID_ADD_DB;
    packet.message.length = sizeof (packet.message.para.addr);
    packet.message.para.num = 0;
    packet.message.para.addr = user_id;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SFID_NEW_USER)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0)
         return false;


    return status;

}

bool Command_i::sfid_delete_db_cmd(UARTPort_i &uart_port,uint32_t user_id, uint32_t *err ) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message) - sizeof (packet.message.para.num);

    packet.message.type = CMD_SFID_DELETE_DB;
    packet.message.length = sizeof (packet.message.para.addr);
    packet.message.para.num = 0;
    packet.message.para.addr = user_id;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SFID_DELETE_DB)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0)
         return false;


    return status;

}

bool Command_i::sfid_send_img_cmd(UARTPort_i &uart_port, bool crc, uint32_t img_size,uint32_t *idx_id, uint32_t *err ) const
{
    bool status = false;
    packet_t packet;
    packet.pack_payload_attr.rw = 1;
    packet.pack_payload_attr.crc = 0;
    packet.pack_payload_attr.nack = 0;
    packet.pack_payload_attr.rsvd = 0;
    packet.pack_payload_attr.payload_size = sizeof (packet.message) - sizeof (packet.message.para.num);

    packet.message.type = CMD_SFID_SEND_IMAGE;
    packet.message.length = sizeof (packet.message.para.addr);
    packet.message.para.num = 0;
    packet.message.para.addr = img_size;

    test_send_packet(uart_port, &packet, 0);
    packet_r_t packet_r;
    if (false == (status = get_respond_packet(uart_port,  &packet_r)))
       return status;

    if (packet_r.message.type != RESP_SFID_SEND_IMAGE)
    {
      printf("this is not a expected package\r\n");
      return false;
    }

    *err = packet_r.message.para.ret;
    if (packet_r.message.para.ret != 0)
         return false;
    *idx_id = packet_r.message.para.num;
    return status;
}




