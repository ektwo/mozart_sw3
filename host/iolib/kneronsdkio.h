/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkio.h
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#ifndef KNERONSDKIO_H
#define KNERONSDKIO_H
#include <QString>
#include <QSerialPortInfo>
#include <lusb0_usb.h>


class UARTPort_i
{
  public:

  UARTPort_i();
  ~UARTPort_i();
  bool init_io_port(const QString portName);
  bool open() const;
  bool send_sync_to_io() const;

  bool send_command_to_io(void *buffer, qint64 length) const;
  bool send_image_data_block_to_io(const char *buf, qint64 length) const;
  bool read_result_or_status_from_io(char* rBuffer, qint64 len) const;
  bool read_result_or_status_from_io_line(char* p_rBuffer) const;
  bool port_is_busy(const QString portname) const;
  void close() const;
  bool recv_data_ack () const;
  QList<QSerialPortInfo> m_infos;
  int m_port_num;

  private:


};

class USBPort_i
{
  public:

  USBPort_i();
  ~USBPort_i();

  bool init_io_port();
  bool open() const;
  void close() const;
  bool send_sync_to_io(UARTPort_i &uart_port_i) const;
  bool send_image_data_block_to_io(char *buf, long length) const;
  bool read_result_or_status_from_io(char* rBuffer, int len) const;
  struct usb_device *p_kneron_dev;

private:
    bool recv_data_ack () const;

};
#endif // KNERONSDKIO_H
