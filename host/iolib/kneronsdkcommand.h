/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkcommand.h
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#ifndef KNERONSDKCOMMAND_H
#define KNERONSDKCOMMAND_H
#include <QString>
#include "kneronsdkio.h"
//input
typedef struct test_input {
  bool crc;
  uint32_t addr;
  uint32_t len;
  void *data;

} __attribute__ ((packed,aligned(32))) test_input_t;

typedef struct test_output {
  uint32_t act_out_len;
  uint32_t err;
  void *data;
} __attribute__ ((packed,aligned(32))) test_output_t;

typedef struct demo_mode_input {
  bool crc;
 struct {
  uint32_t	setup_mem_addr;
  uint32_t	setup_mem_len;
  uint32_t	command_mem_add;
  uint32_t	command_mem_len;
  uint32_t	weight_mem_addr;
  uint32_t	weight_mem_len;
 } mode;
} __attribute__ ((packed,aligned(32))) demo_mode_input_t;

typedef struct demo_img_input {
  bool crc;
 struct {
  uint32_t	input_row;
  uint32_t input_column;
  uint32_t nput_channel;
  uint32_t img_mem_addr;
  uint32_t img_mem_len;
  uint32_t img_num;
 } img;
} __attribute__ ((packed,aligned(32))) demo_img_input_t;

typedef struct fid_mode_input {
uint16_t mode;
uint16_t verif;
uint32_t password;
uint32_t conf_para;

} __attribute__ ((packed,aligned(32))) fid_mode_input_t;

typedef struct fid_mode_output {
     uint32_t ret;
      uint16_t new_fid;
      uint8_t unit_info[12];

}  __attribute__ ((packed,aligned(32))) fid_mode_output_t;

typedef struct fid_verify_fid_user_input {
   uint16_t subcommand;
   uint16_t user_id;
   uint16_t img_idx;
}  __attribute__ ((packed,aligned(32))) fid_verify_fid_user_input_t;

typedef struct fid_verify_fid_user_output {
   uint32_t error;
   uint16_t user_id;
}  __attribute__ ((packed,aligned(32))) fid_verify_fid_user_output_t;

typedef struct fid_cmd_new_fid_user_input {
        uint16_t subcommand;
        uint16_t img_idx;

}  __attribute__ ((packed,aligned(32))) fid_cmd_new_fid_user_input_t;

typedef struct fid_cmd_new_fid_user_output {

      uint32_t ret;
      uint16_t user_id;
      uint16_t img_idx;
}  __attribute__ ((packed,aligned(32))) fid_cmd_new_fid_user_output_t;

typedef struct fid_edit_fid_user_input {
        uint16_t subcommand;
        uint16_t user_id;
        uint16_t img_idx;
        uint16_t reserved;

}  __attribute__ ((packed,aligned(32))) fid_edit_fid_user_input_t;

typedef struct fid_edit_fid_user_output_t {

      uint32_t ret;
      uint16_t subcommand;
      uint16_t user_id;
      uint8_t *data;

}  __attribute__ ((packed,aligned(32))) fid_edit_fid_user_output_t;



typedef struct sfid_start_output_t{

    uint32_t ret;
    uint32_t img_size;

}  __attribute__ ((packed,aligned(32))) sfid_start_output_t;

typedef struct sfid_new_user_input {
  bool crc;
      uint32_t user_id;
      uint32_t img_idx;

} __attribute__ ((packed,aligned(32))) sfid_new_user_input_t;

typedef struct sfid_new_user_output_t{

    uint32_t ret;
    uint16_t user_id;
    uint16_t img_idx;

}  __attribute__ ((packed,aligned(32))) sfid_new_user_output_t;



class Command_i
{

  public:
    Command_i();
    ~Command_i();
    bool test_mem_read_cmd(UARTPort_i &uart_port, test_input_t *p_input, test_output *p_output) const;
    bool test_mem_write_cmd(UARTPort_i &uart_port, test_input *p_input, test_output *p_output)  const;
    bool test_mem_clr_cmd(UARTPort_i &uart_port, test_input *p_input, test_output *p_output) const;
    bool test_echo_cmd(UARTPort_i &uart_port, test_input *p_input, test_output *p_output)  const;
    bool test_usb_mem_write(UARTPort_i &uart_port, test_input *p_input, test_output *p_output);
    bool demo_set_models_cmd(UARTPort_i &uart_port,  demo_mode_input_t *p_mode_input, uint32_t *err) const;
    bool demo_set_images_cmd(UARTPort_i &uart_port, demo_img_input_t *p_img_input, uint32_t *err ) const;
    bool demo_run_cmd(UARTPort_i &uart_port, uint32_t *err)  const;
    bool demo_run_once_cmd(UARTPort_i &uart_port, uint32_t *err) const;
    bool demo_stop_cmd(UARTPort_i &uart_port, uint32_t *err) const;

    bool oper_reset_cmd(UARTPort_i &uart_port, uint32_t mod_ctl, uint32_t  confirm, uint32_t *err)  const;
    bool oper_system_status_cmd(UARTPort_i &uart_port, uint32_t *dev_id, uint32_t *firmware_id) const;
    bool oper_query_apps_cmd(UARTPort_i &uart_port, uint8_t *app_id, uint32_t *len) const;
    bool oper_select_app_cmd(UARTPort_i &uart_port,  uint32_t app_id, uint32_t *buf_size, uint32_t *err) const;
    bool oper_set_processimg_mode_cmd(UARTPort_i &uart_port, uint32_t img_sequ_num,uint32_t mode,  uint32_t *err) const;
    bool oper_status_update_cmd(UARTPort_i &uart_port, uint32_t set_status, uint32_t *mask);
    bool oper_set_update_event_cmd(UARTPort_i &uart_port, uint32_t set_status, uint32_t *mask) const;
    bool oper_abort_image_cmd(UARTPort_i &uart_port, uint32_t seq_no, uint32_t *err,uint32_t *seq_no_r)  const;

    bool fid_set_fid_mode_cmd(UARTPort_i &uart_port, fid_mode_input_t *p_input, fid_mode_output_t *p_output) const;
    bool fid_verify_fid_user_cmd(UARTPort_i &uart_port, fid_verify_fid_user_input *p_input, fid_verify_fid_user_output *p_output) const;
    bool fid_new_fid_user_cmd(UARTPort_i &uart_port, fid_cmd_new_fid_user_input_t *p_input,fid_cmd_new_fid_user_output_t *p_output) const;
    bool fid_edit_fid_user_cmd(UARTPort_i &uart_port, fid_edit_fid_user_input_t *p_input,fid_edit_fid_user_output_t *p_output) const;

    bool sfid_start_cmd(UARTPort_i &uart_port,  uint32_t *img_size, uint32_t *err) const;
    bool sfid_new_user_cmd(UARTPort_i &uart_port, uint32_t usr_id, uint32_t img_idx ) const;
    bool sfid_add_db_cmd(UARTPort_i &uart_port, uint32_t user_id, uint32_t *err) const;
    bool sfid_delete_db_cmd(UARTPort_i &uart_port,uint32_t user_id, uint32_t *err ) const;
    bool sfid_send_img_cmd(UARTPort_i &uart_port, bool crc, uint32_t img_size,uint32_t *idx_id, uint32_t *err ) const;

  private:

};


#endif // KNERONSDKCOMMAND_H
