/******************************************************************************
*
* Copyright (C) 2018 Kneron, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
*
*
******************************************************************************/
/*****************************************************************************/
/*
 *
 * @file kneronsdkpkg_e.h
 *
 * This file demonstrates how to use
 *
 * MODIFICATION HISTORY:
 *
 * Version Who Date			Changes
 * -----------------------------------------------------------------------------
 *	1.0 fz	08/17/18
 * */
#ifndef KNERONPKG_H
#define KNERONPKG_H

#include <QObject>
#include "kneronsdkusb_e.h"
#include "kneronsdkuart_e.h"
#include "knorenimage.h"
#include <QString>

class KneronPKG: public QObject
{

  Q_OBJECT
//  Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)

  public:
    explicit KneronPKG(QObject *parent = nullptr);

  public slots:

    bool send_image_file(QStringList &file_name_list, UARTPort &uart_port) const;
    bool send_image_file(QStringList &file_name_list,  USBPort &usb_port) const;
    bool send_image_buffer(uint8_t *buf,  long len,  UARTPort &uart_port) const;
    bool send_image_buffer(uint8_t *buf,  long len,  USBPort &usb_port) const;

  private:
    KneronImage *p_img_i;

};
#endif // KNERONPKG_H
