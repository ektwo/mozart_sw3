#include "memory.h"
#include <stdint.h>
#include "dbg.h"

static uint32_t ddr_addr_end_s = 0;

void kdp_ddr_init(unsigned int addr)
{
    if(ddr_addr_end_s == 0)
        ddr_addr_end_s = addr; 

    return;
}

uint32_t kdp_ddr_malloc(uint32_t numbyte)
{
    uint32_t ret;

    if(0 == ddr_addr_end_s)
    {
        dbg_msg("[ERR] ddr malloc before init\n");
        return 0;
    }

    ret = ddr_addr_end_s;
    ddr_addr_end_s += numbyte;    

    if(ddr_addr_end_s >= (KDP_DDR_BASE_MOZART+KDP_DDR_BASE_SYSTEM_RESERVED) )
    {
        dbg_msg("[WARN] ddr malloc over SCPU bondary\n" ); 
        return 0; 
    }

    return ret;
}

void kdp_ddr_free(unsigned int addr)
{
    //todo
    return;    
}
