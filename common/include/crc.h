/**
 * @file      crc.h
 * @brief     crc checksum 
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __CRC_H__
#define __CRC_H__
#include "base.h"


u16 gen_crc16(const u8 *data, u16 size);

#endif
