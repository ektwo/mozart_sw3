/*
 * Kneron Header for Mozart memory
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */

#ifndef MOZART_MEMORY_H
#define MOZART_MEMORY_H

/* DDR */
#define KDP_DDR_BASE_MOZART             0x60000000   //reserved 32MB for models
#define KDP_DDR_MODEL_RESERVED_END      0x61FFFFFF

#define KDP_DDR_HEAP_HEAD_FOR_MALLOC    0x62FFDFFF
#define KDP_DDR_BASE_IMAGE_BUF          0x62FFE000   //reserved  : 6buffers (1280 x 720 x 3)
/* camera-0:
 *   duplicated frame  0x62FFE000
 *   cam0 - buf0       0x632A1000
 *   cam0 - buf1       0x63544000
 * camera-1:
 *   duplicated frame  0x637E7000
 *   cam1 - buf0       0x63A8A000
 *   cam1 - buf1       0x63D2D000
*/

#define KDP_DDR_BASE_SYSTEM_RESERVED    0x63FD0000   //reserved 191 KB
#define KDP_DDR_MODEL_INFO_TEMP         0x63FFC000   //1KB
#define KDP_DDR_END                     0x63FFFFFF

#define KDP_IMAGE_BUF_SIZE              0x2A3000     //1280 x 720 x 3


/* N i/d RAM */
#ifdef TARGET_NCPU
#define S_D_RAM_ADDR                0x20200000
#define N_D_RAM_ADDR                0x0FFF0000
#endif
#ifdef TARGET_SCPU
#define S_D_RAM_ADDR                0x10200000
#define N_D_RAM_ADDR                0x2FFF0000
#endif

#define S_D_RAM_SIZE                0x18000          /* 96 KB */
#define N_D_RAM_SIZE                0x10000          /* 64 KB */

/* IPC memory */
#define IPC_RAM_SIZE                0x2000           /* 8K Bytes : split 3 : 1 */
#define IPC_MEM_OFFSET              (S_D_RAM_SIZE - IPC_RAM_SIZE)
#define IPC_MEM_OFFSET2             (S_D_RAM_SIZE - IPC_RAM_SIZE / 4)
#define IPC_MEM_ADDR                (S_D_RAM_ADDR + IPC_MEM_OFFSET)
#define IPC_MEM_ADDR2               (S_D_RAM_ADDR + IPC_MEM_OFFSET2)

/* for ddr memory management */
void kdp_ddr_init(unsigned int addr);
unsigned int kdp_ddr_malloc(unsigned int numbyte);
void kdp_ddr_free(unsigned int addr);

#endif
