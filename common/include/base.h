/**
 * @file      base.h
 * @brief     Basic utils & struct
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __BASE_H__
#define __BASE_H__

#include <stdint.h>

typedef uint8_t    u8;
typedef uint16_t   u16;
typedef uint32_t   u32;
typedef uint64_t   u64;

typedef int8_t     s8;
typedef int16_t    s16;
typedef int32_t    s32;
typedef int64_t    s64;


#define BIT(x)      (0x01 << (x))


#define STS_OK              0
#define STS_ERR_NORMAL      1
#define STS_ERR_CRC         2


#endif


