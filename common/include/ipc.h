/*
 * Kneron IPC Header for Mozart
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */

#ifndef KNERON_IPC_H
#define KNERON_IPC_H

#include <stdint.h>
#include "model_type.h"

#define SCPU2NCPU_ID		('s'<<24 | 'c'<<16 | 'p'<<8 | 'u')
#define NCPU2SCPU_ID		('n'<<24 | 'c'<<16 | 'p'<<8 | 'u')

#define MULTI_MODEL_MAX         16      /* Max active models in memory */
#define IPC_IMAGE_ACTIVE_MAX    2       /* Max active images for NCPU/NPU */
#define IPC_IMAGE_MAX           10      /* Max cycled buffer for images */

/* Image process state */
#define IMAGE_STATE_INACTIVE    0
#define IMAGE_STATE_ACTIVE      1
#define IMAGE_STATE_NPU_DONE    2
#define IMAGE_STATE_DONE        3

/* Image format flags */
#define IMAGE_FORMAT_SUB128                 BIT(31)
#define IMAGE_FORMAT_ROT_MASK               (BIT(30) | BIT(29))
#define IMAGE_FORMAT_ROT_SHIFT              29
#define IMAGE_FORMAT_ROT_CLOCKWISE          0x01
#define IMAGE_FORMAT_ROT_COUNTER_CLOCKWISE  0x02

#define IMAGE_FORMAT_BYPASS_PRE             BIT(19)
#define IMAGE_FORMAT_BYPASS_NPU_OP          BIT(18)
#define IMAGE_FORMAT_BYPASS_CPU_OP          BIT(17)
#define IMAGE_FORMAT_BYPASS_POST            BIT(16)

#define IMAGE_FORMAT_NPU                    0x00FF
#define NPU_FORMAT_RGBA8888         0x00
#define NPU_FORMAT_YUV422           0x10
#define NPU_FORMAT_NIR              0x20
#define NPU_FORMAT_YCBCR422         0x30
#define NPU_FORMAT_YUV444           0x40
#define NPU_FORMAT_YCBCR444         0x50
#define NPU_FORMAT_RGB565           0x60

/* Model structure */
struct kdp_model {
    //todo: 
    //uint32_t    model_type; //defined in model_type.h

    /* Input in memory */
    uint32_t    input_mem_addr;
    int32_t     input_mem_len;
	
    /* Output in memory */
    uint32_t    output_mem_addr;
    int32_t     output_mem_len;

    /* Working buffer */
    uint32_t    buf_addr;
    int32_t     buf_len;

    /* command.bin in memory */
    uint32_t    cmd_mem_addr;
    int32_t     cmd_mem_len;

    /* weight.bin in memory */
    uint32_t    weight_mem_addr;
    int32_t     weight_mem_len;

    /* setup.bin in memory */
    uint32_t    setup_mem_addr;
    int32_t     setup_mem_len;
};
typedef struct kdp_model kdp_model_info_t;

/* Result structure of a model */
struct result_buf {
    int32_t     model_id;
    uint32_t    result_mem_addr;
    int32_t     result_mem_len;
    int32_t     result_ret_len;
};

/* Raw image structure */
struct kdp_img_raw {
    /* Image state: 1 = active, 0 = inactive */
    int         state;

    /* Image sequence number */
    int         seq_num;

    /* Image ref index */
    int         ref_idx;

    /* raw image dimensions */
    uint32_t    input_row;
    uint32_t    input_col;
    uint32_t    input_channel;

    /* Raw image format and pre-process flags
     * bit-31: = 1 : subtract 128
     * bit 30:29 00: no rotation; 01: rotate clockwise; 10: rotate counter clockwise; 11: reserved
     * bit 7:0: format
     */
    uint32_t    format;

    /* Crop parameters */
    int         crop_top;
    int         crop_bottom;
    int         crop_left;
    int         crop_right;

    /* Pad parameters */
    int         pad_top;
    int         pad_bottom;
    int         pad_left;
    int         pad_right;

    /* input image in memory */
    uint32_t    image_mem_addr;
    int32_t     image_mem_len;

    struct result_buf results[MULTI_MODEL_MAX];

    /* Test: SCPU total */
    uint32_t    tick_start;
    uint32_t    tick_end;

    /* Test: NCPU processes */
    uint32_t    tick_start_pre;
    uint32_t    tick_end_pre;
    uint32_t    tick_start_npu;
    uint32_t    tick_end_npu;
    uint32_t    tick_start_post;
    uint32_t    tick_end_post;
};

/* Image result structure */
struct kdp_img_result {
    /* Processing status: 2 = done, 1 = running, 0 = unused */
    int         status;

    /* Image sequence number */
    int         seq_num;
	
    /* result memory addr */
    //dummy information
    uint32_t    result_mem_addr;
};

/* Structure of sCPU->nCPU Message */
struct scpu_to_ncpu {
    uint32_t    id;        /* = 'scpu' */
    uint32_t    version;
    uint32_t    cmd;            // Run / Stop
    uint32_t    input_count;    // # of input image
    uint32_t    input_flags;    // special purposes/reserved

    /*
     * debug control flags (dbg.h):
     *   bits 19-16: scpu debug level
     *   bits 03-00: ncpu debug level
     */
    uint32_t    debug_flags;

    /* single_model_result_int:
     *   = 0, default (result notification after all done); 
     *   = 1, result notification after each single-model is done
     */
    uint32_t    single_model_result_int;    // request to ncpu

    /* Active models in memory and running */
    int32_t             num_models;  //usually, num_models=1 (only one active model)
    struct kdp_model    models[MULTI_MODEL_MAX];            //to save active modelInfo
    int32_t             models_type[MULTI_MODEL_MAX];       //to save model type
    int32_t             model_slot_index;  //usually, model_slot_index = 0 (index for above 2 arrays)

    /* Images being processed via IPC */
    int32_t             active_img_index;
    struct kdp_img_raw  raw_images[IPC_IMAGE_MAX];

    /* Input/Output working buffers for NPU */
    uint32_t    input_mem_addr2;
    int32_t     input_mem_len2;

    uint32_t    output_mem_addr2;
    int32_t     output_mem_len2;
};

/* Structure of nCPU->sCPU Message */
struct ncpu_to_scpu {
    uint32_t    id;        /* = 'ncpu' */
    uint32_t    version;
    int32_t     status;             // status for the cmd from scpu
    uint32_t    output_count;       // # of input images processed/output
    uint32_t    output_flags;       // special purposes/reserved

    /* Images result info corresponding to raw_images[] */
    int32_t                 img_index_done;
    struct kdp_img_result   img_results[IPC_IMAGE_MAX];
};

/* scpu_to_ncpu: cmd */
enum {
    CMD_RUN_NPU = 1,
    CMD_STOP_NPU,
};

/* ncpu_to_scpu: status */
enum {
    STATUS_ERR = -1,
    STATUS_INIT = 0,
    STATUS_OK,
};

/* Post Processing Result Info */

/* Yolo Result */
struct bounding_box {
    float x1;           // top-left corner:  x
    float y1;           // top-left corner:  y
    float x2;           // bottom-right corner:  x
    float y2;           // bottom-right corner:  y

    float score;        // probability score
    int class_num;      // class # (of many) with highest probability
};

struct yolo_result {
    uint32_t    class_count;            // total class count
    uint32_t    box_count;              /* boxes of all classes */
    struct bounding_box boxes[1];       /* [box_count] */
};

/* ImageNet Classification Result */

#define IMAGENET_TOP_MAX    5

struct imagenet_result {
    int     index;          // index of the class
    float   score;          // probability score of the class
};

/* Face Detection Result */

#define FACE_DETECTION_XYWH    4

struct facedet_result {
    int     len;                        // length of detected X,Y,W,H: 0 or 4
    int     xywh[FACE_DETECTION_XYWH];  // 4 values for X, Y, W, H
};

/* Land Mark Result */

#define LAND_MARK_POINTS    5

struct landmark_result {
    struct {
        uint32_t x;
        uint32_t y;
    } marks[LAND_MARK_POINTS];
};

/* fr result */

#define FR_FEATURE_MAP_SIZE 512

struct fr_result {
    float feature_map[FR_FEATURE_MAP_SIZE];
};

#endif

