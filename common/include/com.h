/**
 * @file      com.h
 * @brief     Basic communcation structure
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */
#ifndef __COM_H__
#define __COM_H__

#include "base.h"

#ifdef COM_V1
#define PACKET  0  // 1 -> use packet layer, 0 -> no packet layer
#else
#define PACKET  1  // 1 -> use packet layer, 0 -> no packet layer
#endif

#if PACKET
typedef enum {
    CMD_NONE = 0,
    CMD_MEM_READ,
    CMD_MEM_WRITE,
    CMD_DATA,
    CMD_ACK,
    CMD_STS_CLR,
    CMD_MEM_CLR,
    CMD_CRC_ERR,
    CMD_TEST_ECHO,
    CMD_USB_WRITE,

    CMD_DEMO_SET_MODEL = 0x10,
    CMD_DEMO_SET_IMAGES,
    CMD_DEMO_RUN_ONCE,
    CMD_DEMO_RUN,
    CMD_DEMO_STOP,
    CMD_DEMO_RESULT_LEN,
    CMD_DEMO_RESULT,    // Response/report to host
    CMD_DEMO_FD,
    CMD_DEMO_LM,        // for development (remove after merge LM, LIVESS, FR to CMD_DEMO_RUN)
    CMD_DEMO_LD,        // 
    CMD_DEMO_FR,        //

    CMD_RESET = 0x20,
    CMD_SYSTEM_STATUS,
    CMD_QUERY_APPS,
    CMD_SELECT_APP,
    CMD_SET_MODE,
    CMD_SET_EVENTS,
    CMD_UPDATE,
    CMD_IMG_RESULT,  // only RESP message is used.  No CMD version is implemented
    CMD_ABORT,

    CMD_DEMO_INFERENCE = 100, //= 0x64
    CMD_DEMO_REG1,
    CMD_DEMO_REG2,
    CMD_DEMO_REG3,
    CMD_DEMO_REG4,
    CMD_DEMO_REG5,
    CMD_DEMO_ADDUSER,
    CMD_DEMO_DELUSER,
    CMD_DEMO_ABORT_REG,
    
    CMD_DEMO_TINY_YOLO = 110,  //= 0x6E
    
    CMD_SFID_START = 0x108,
    CMD_SFID_NEW_USER,
    CMD_SFID_ADD_DB,
    CMD_SFID_DELETE_DB,
    CMD_SEND_IMAGE,

    // Flash command
    CMD_FLASH_INFO = 0x1000,
    CMD_FLASH_CHIP_ERASE,
    CMD_FLASH_SECTOR_ERASE,
    CMD_FLASH_READ,
    CMD_FLASH_WRITE,    

    // debug utility command
    CMD_DBG_USB_MEM_WR = 0x2000,
    CMD_DBG_SET_MODEL_TYPE = 0x2001,
} Cmd;
#else
typedef enum {
    CMD_NONE = 0,
    CMD_MEM_READ,
    CMD_MEM_WRITE,
    CMD_DATA,
    CMD_ACK,    
    CMD_STS_CLR,
    CMD_MEM_CLR,
    CMD_CRC_ERR,
    CMD_TEST_ECHO,        

    CMD_DEMO_SET_MODEL = 0x10,
    CMD_DEMO_SET_IMAGES,
    CMD_DEMO_RUN_ONCE,
    CMD_DEMO_RUN,
    CMD_DEMO_STOP,
    CMD_DEMO_RESULT_LEN,
    CMD_DEMO_RESULT,    // Response/report to host
    CMD_DEMO_FD,
    CMD_DEMO_LM,        // for development (remove after merge LM, LIVESS, FR to CMD_DEMO_RUN)
    CMD_DEMO_LD,        // 
    CMD_DEMO_FR,        //

    CMD_DEMO_INFERENCE = 100, //=0x64
    CMD_DEMO_REG1,
    CMD_DEMO_REG2,
    CMD_DEMO_REG3,
    CMD_DEMO_REG4,
    CMD_DEMO_REG5,
    CMD_DEMO_ADDUSER,
    CMD_DEMO_DELUSER,
    CMD_DEMO_ABORT_REG,
    
    CMD_DEMO_TINY_YOLO = 110, //=0x6e

    // Flash command
    CMD_FLASH_INFO = 0x1000,
    CMD_FLASH_CHIP_ERASE,
    CMD_FLASH_SECTOR_ERASE,
    CMD_FLASH_READ,
    CMD_FLASH_WRITE,
} Cmd;
#endif

#if PACKET
typedef struct {
    u16 preamble;
    u16 pSize;
    u16 cmd;
    u16 mSize;
} __attribute__((packed)) MsgHdr;
#else
typedef struct {
    u16 header;
    u16 crc16;
    u32 cmd;
    u32 addr;
    u32 len;
} __attribute__((packed)) MsgHdr;
#endif

typedef struct {
    u32 addr;
    u32 len;
    u8	data[];
} __attribute__((packed)) CmdPram;

typedef struct {
    u32 error;
    u32 bytes;
} __attribute__((packed)) RspPram;

typedef struct {
    u32 op_parm1;
    u32 op_parm2;
    u8  data[];
} __attribute__((packed)) OpPram;
    

#define MSG_HDR_CMD     0xA583
#define	MSG_HDR_RSP     0x8A35
#define MSG_HDR_VAL     0xA553  // this is used by the pre-packet code
#define MSG_HDR_SIZE    16  // includes both MsgHdr and CmdPram addr & len
#define PKT_CRC_FLAG    0x4000

#if (HAPS_ID == 2)
#define MSG_DATA_BUF_MAX    0x890
#else
#define MSG_DATA_BUF_MAX    0x1400
#endif
#define MSG_BUF_MAX         (MSG_DATA_BUF_MAX + MSG_HDR_SIZE)

#endif


