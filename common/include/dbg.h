/**
 * @file      dbg.h
 * @brief     debug macro 
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __DBG_H__
#define __DBG_H__

#include <stdio.h>
#include <fdebug.h>
#include "ipc.h"

#define LOG_NONE        0
#define LOG_CRITICAL    1
#define LOG_ERROR       2
#define LOG_INFO        3
#define LOG_TRACE       4
#define LOG_DBG         5

#ifdef LOG_ENABLE

#ifdef TARGET_NCPU

extern struct scpu_to_ncpu *in_comm_p;

#define MSG(level, format, ...) \
    do { \
        int lvl = in_comm_p->debug_flags & 0x0000000F; \
        if (level > 0 && level <= lvl) \
            fLib_printf(format, ##__VA_ARGS__); \
    } while (0)

#else // TARGET_SCPU

extern struct scpu_to_ncpu *out_comm_p;

#define MSG(level, format, ...) \
    do { \
        int lvl = out_comm_p->debug_flags & 0x000F0000; \
        lvl >>= 16; \
        if (level > 0 && level <= lvl) \
            fLib_printf(format, ##__VA_ARGS__); \
    } while (0)

/* Set log levels on SCPU side */
#define log_set_level_scpu(level) \
    (out_comm_p->debug_flags = (out_comm_p->debug_flags & ~0x000F0000) | (((level) << 16) & 0x000F0000))
#define log_set_level_ncpu(level) \
    (out_comm_p->debug_flags = (out_comm_p->debug_flags & ~0x0000000F) | ((level) & 0x0000000F))

#endif

#define LOG(level, format, ...) \
    do { \
        fLib_printf("[%d|%s@%s,%d] " format, \
            level, __func__, __FILE__, __LINE__, ##__VA_ARGS__ ); \
    } while (0)
     
#define _DSG(__flag__, __format__, ...) { if (__flag__ == LOG_INFO) fLib_printf(__format__"\n", ##__VA_ARGS__); }
#define DSG(__format__, ...) { fLib_printf(__format__"\n", ##__VA_ARGS__); }

#define dbg_msg(fmt, ...) MSG(LOG_DBG, fmt, ##__VA_ARGS__)
#define trace_msg(fmt, ...) MSG(LOG_TRACE, fmt, ##__VA_ARGS__)
#define info_msg(fmt, ...) MSG(LOG_INFO, fmt, ##__VA_ARGS__)
#define err_msg(fmt, ...) MSG(LOG_ERROR, fmt, ##__VA_ARGS__)
#define critical_msg(fmt, ...) MSG(LOG_CRITICAL, fmt, ##__VA_ARGS__)

#else

#define LOG(level, format, ...) 
#define _DSG(__flag__, __format__, ...)
#define DSG(__format__, ...)
#define MSG(level, format, ...)
#define dbg_msg(fmt, ...)
#define trace_msg(fmt, ...)
#define info_msg(fmt, ...)
#define err_msg(fmt, ...)
#define critical_msg(fmt, ...)

#endif // LOG_ENABLE

#endif // __DBG_H__
