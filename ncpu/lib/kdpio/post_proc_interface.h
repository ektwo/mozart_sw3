/*
 * Head for post_proc_main
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */

#ifndef POST_PROC_INTERFACE_H
#define POST_PROC_INTERFACE_H

#define MAX_POSTPROC_OUT_ELEM  20  //max elements in postproc result array
#define ROUND_DELTA  512

typedef enum
{
  RGB,
  BGR,
  MONO
} preproc_out_fmt_t;

typedef enum
{
  YOLO,
  KNERON,
  CAFFE,
  TF,
  TORCH
} preproc_norm_mode_t;

typedef enum
{
  PP_CROP_AFTER_FD,
  PP_CROP_TOTAL
}ppCropType;


typedef struct
{
  ppCropType type;
  int beforeCropW;
  int beforeCropH;
  int crop_x;
  int crop_y;
  int crop_w;
  int crop_h;
} ppCropInfo_t;

typedef struct {
  float fScaleInit;
  int nOrigImgW;
  int nOrigImgH;
  char *rawImgName;
  /* crop info needed for further face recog */
  int total_crops;
  ppCropInfo_t cropInfo[PP_CROP_TOTAL];
/* array output postproc generated */
  int nValidLen;
  int bIsOutFloat;
  int aIntOut[MAX_POSTPROC_OUT_ELEM];
  float aFloatOut[MAX_POSTPROC_OUT_ELEM];
} postProcOut_t;

enum {
  R_CHNL,
  G_CHNL,
  B_CHNL,
  RGB_CHNL_TOTAL
};

typedef struct {
  int row;
  int col;
  uint8_t *pRGB;
} img_rgb8_channels_t;


#endif
