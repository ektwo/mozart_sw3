/**
 * @file      memory.h
 * @brief     Memory functions
 * @version $Id: memory.h 498 2018-07-16 05:43:35Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __MEMORY_H__
#define __MEMORY_H__

#include <stddef.h>

void MemReset(void *ptr);
void* MemGetCur(void);

void* MemAlloc(size_t size);
void* MemCalloc(size_t nmemb, size_t size);
void MemFree(void *ptr);
#endif
