/**
 * @file      matrix.h
 * @brief     2D matrix related functions
 * @version $Id: matrix.h 251 2018-05-03 08:26:41Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __KNERON_PRE_PROC_TRANSFORM_MATRIX
#define __KNERON_PRE_PROC_TRANSFORM_MATRIX

#include "app_flow_base.h"
#include "pre_proc_matrix_macro.h"

#define MATRIX_ELEMENT_ADD         0
#define MATRIX_ELEMENT_MINUS       1
#define MATRIX_ELEMENT_MULTIPLY    2

typedef enum _MAT_TYPE {
    MT_UNKNOWN,
    MT_UCHAR,
    MT_INT,
    MT_FLOAT
} MAT_TYPE;

typedef union _MatData {
    unsigned char *c;
    int *i;
    float *f;
} MatData;

typedef struct _Matrix {
    int height;
    int width;
    MAT_TYPE type;
    MatData data;
} Matrix;

Matrix* MakeMatrix(int width, int height, MAT_TYPE type);
void InitMatrix(Matrix *pMat, int width, int height, int type);
void FreeMatrix(Matrix *pMat);
void FreeMatData(Matrix *pMat);
Matrix* OnesMatrix(int width, int height, MAT_TYPE type);
Matrix* EyeMatrix(int n, MAT_TYPE type);
Matrix* NanMatrix(int width, int height);
Matrix* DiagMatrix(const Matrix *pVec);
Matrix* CopyMatrix(const Matrix *pSrc, MAT_TYPE type);
Matrix* MatAsType(const Matrix *pSrc, MAT_TYPE type);
void AssignMat(Matrix *pDst, const Matrix *pSrc, int hStr, int hEnd, int wStr, int wEnd);

#ifndef RELEASE_BUILD
void PrintMatrix(const Matrix* pMat);
void PrintMatrixThenFree(Matrix *pMat);
Matrix* ReadMatrix(const char *filename, MAT_TYPE type);
void WriteMat(const char *filename, const Matrix *pSrc);
#endif

int IsVector(const Matrix *pMat);
float MaxVal(const Matrix *pMat);
float SumMat(const Matrix *pMat);
Matrix* SubMat(const Matrix *pSrc, int hStr, int hEnd, int wStr, int wEnd);
void SubMatAssign(Matrix *pDst, const Matrix *pSrc, int hStr, int hEnd, int wStr, int wEnd);
Matrix* MatGetCol(const Matrix *pSrc, int col);
Matrix* MatGetRow(const Matrix *pSrc, int row);
Matrix* TransposeMat(const Matrix* pSrc);
Matrix* MeanMat(const Matrix *pSrc);
Matrix* VarMat(const Matrix *pSrc);
Matrix* SubtractVec(const Matrix *pSrc, const Matrix *pVec);
void ScaleMat(Matrix* pMat, float value, int scaleUp);
void ScaleSubMat(Matrix *pMat, float value, int scaleUp, int hStr, int hEnd, int wStr, int wEnd);
float InnerProduct(const Matrix* pVec1, const Matrix* pVec2);
Matrix* DotProduct(const Matrix* pMat1, const Matrix* pMat2);
float DetMatrix(const Matrix *pMat);

Matrix* TruncMatDecimalVal(const Matrix *pSrc);
Matrix* MergeMatrixCol(const Matrix* m1, const Matrix* m2);
Matrix* MatElementOp(const Matrix* m1, const Matrix* m2,int op);

#endif /* __KNERON_PRE_PROC_TRANSFORM_MATRIX */
