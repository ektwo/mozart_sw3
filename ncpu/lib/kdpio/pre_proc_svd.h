/**
 * @file      svd.h
 * @brief     Single Value Decomposition
 * @version $Id: svd.h 189 2018-04-25 01:55:00Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */


#ifndef __KNERON_PRE_PROC_SVD
#define __KNERON_PRE_PROC_SVD

typedef struct _Matrix Matrix;

void SVD(Matrix *pA, Matrix **ppU, Matrix **ppS, Matrix **ppV, int *pRank);

#endif /* __KNERON_PRE_PROC_SVD */
