/*
 * Kneron NPU driver for KDP520
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
 
#include "base.h"
#include "kdpio.h"
#include "npu.h"
#include "ipc.h"
#include "dbg.h"

/* NPU Bits Definitions */
#define NPU_RUN_WFC               BIT(17)
#define NPU_RUN_BUSY              BIT(16)
#define NPU_RUN_CONTI             BIT(1)
#define NPU_RUN_GO                BIT(0)

#define NPU_INT_ERR_INST          BIT(11)
#define NPU_INT_ERR_WT            BIT(10)
#define NPU_INT_ERR_CONFIG        BIT(9)
#define NPU_INT_ERR_AHB           BIT(8)
#define NPU_INT_ERRORS           (NPU_INT_ERR_INST | NPU_INT_ERR_WT | NPU_INT_ERR_CONFIG | NPU_INT_ERR_AHB)
#define NPU_INT_ALL              (NPU_INT_ERRORS | 0xFF)

/* NPU data format */
#define NPU_NEXT1_FORMAT          0x0F

/* NPU FORMAT mode[7:4] */
#define NPU_FORMAT_MODE_7_4_MASK    0x00F0

#define NPU_FORMAT_MODE_3_MASK      0x0008
#define NPU_FORMAT_MODE_2_0_MASK    0x0007

void kdp_init_npu()
{
    outw(ADDR_NPU_INTEN, NPU_INT_ALL);
}

void kdp_exit_npu()
{
    outw(ADDR_NPU_INTEN, 0);
}

void kdp_enable_npu_int()
{
    outw(ADDR_NPU_INTEN, NPU_INT_ALL);
}

void kdp_disable_npu_int()
{
    outw(ADDR_NPU_INTEN, 0);
}

void kdp_enable_npu(struct kdp_image *image_p)
{
    outw(ADDR_NPU_CODE, (unsigned int)image_p->model_p->cmd_mem_addr);
    outw(ADDR_NPU_CLEN, image_p->model_p->cmd_mem_len);
    outw(ADDR_NPU_GETW0, (unsigned int)image_p->model_p->weight_mem_addr);

    outw(ADDR_NPU_RDMA2_SRC3, (unsigned int)image_p->input_mem_addr);
    outw(ADDR_NPU_WDMA0_DST0, (unsigned int)image_p->model_p->buf_addr);

    dbg_msg("kdp_enable_npu: input_mem_addr = 0x%x, cmd_mem_addr=0x%x, cmd_len=0x%x, wt_addr=0x%x, buf_addr=0x%x\n",
	   (unsigned int)image_p->input_mem_addr, 
	   (unsigned int)image_p->model_p->cmd_mem_addr,
	   image_p->model_p->cmd_mem_len,
	   (unsigned int)image_p->model_p->weight_mem_addr,
	   (unsigned int)image_p->model_p->buf_addr);

    outw(ADDR_NPU_RUN, NPU_RUN_GO);
}

int kdp_read_data_size(void)
{
	return (inw(ADDR_NPU_NEXT1) & NPU_NEXT1_FORMAT);
}

int kdp_read_format_size(uint32_t format)
{
    int pixel_size;

    switch(format & 0x00FF){
        case NPU_FORMAT_RGBA8888:
            pixel_size = 4;
            break;
        case NPU_FORMAT_RGB565:
            pixel_size = 2;
            break;
        case NPU_FORMAT_NIR | 0x01:
            pixel_size = 2;
            break;
        case NPU_FORMAT_NIR:
        default:
            pixel_size = 1;
            break;
    }
    return pixel_size;
}

void kdp_enable_npu_preproc(uint32_t cmd_addr, int len)
{
    outw(ADDR_NPU_CODE, cmd_addr);
    outw(ADDR_NPU_CLEN, len);
    outw(ADDR_NPU_RUN, NPU_RUN_GO);
}

int kdp_wait_for_npu_done(int timeout_count)
{
    uint32_t value;
    int rc = -1;

    do {
        value = inw(ADDR_NPU_INT);
        if (value & 0x80) {
            rc = 0;
            break;
        }
        osDelay(1);
    } while (timeout_count--);

    /* Clear */
    outw(ADDR_NPU_INT, value);
    
    if (timeout_count <= 0)
        err_msg("NPU_INT: 0x%x (rem ticks %d, RUN=0x%x)!\n", value, timeout_count, inw(ADDR_NPU_RUN));

    return rc;
}

int kdp_handle_int(void)
{
    uint32_t value;
    int error = 0;

    value = inw(ADDR_NPU_INT);

    if (value & NPU_INT_ERRORS) {
        err_msg("ncpu: NPU_INT: 0x%x (RUN=0x%x)\n", value, inw(ADDR_NPU_RUN));
        error = -1;
    }

    /* Clear */
    outw(ADDR_NPU_INT, value);
    return error;
}
