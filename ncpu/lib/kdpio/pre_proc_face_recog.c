
#include <stdint.h>
#include <string.h>
#include "pre_proc_matrix_macro.h"
#include "kdpio.h"
#include "pre_proc_transform_matrix.h"
#include "pre_proc_umeyama.h"
#include "ipc.h"
#include "post_proc_interface.h"
#include "app_flow_base.h"
#include "dbg.h"

typedef int bool;
#define true 1
#define false 0

static Matrix* CreateLandMarkMatrix(float* landmark, bool source)
{
    int k = 0;
    int size = 10;
    Matrix *pMat = MakeMatrix(2, 5, MT_FLOAT);
    MatData data = pMat->data;
    MAT_TYPE type = pMat->type;
    float fTmp;

    while (size--) {
        fTmp = (source && ((k%2) == 0))? (landmark[k] + 8): landmark[k];
        M_SET_VAL_1D(data, type, k, fTmp);
        k++;
    }

    return pMat;
}

static Matrix* SimilarityTransform(const Matrix *pSrc, const Matrix *pDst)
{
    /* https://github.com/scikit-image/scikit-image/blob/master/skimage/transform/_geometric.py#L1041 */
    Matrix *pTmp = umeyama(pSrc, pDst, 1);
    Matrix *pTform = SubMat(pTmp, 0, 2, 0, pTmp->width);
    FreeMatrix(pTmp);

    return pTform;
}

void cvWarpAffine(img_rgb8_channels_t *pSrcImg, IplImage *pDst, Matrix *pM, uint32_t format)
{
    int i, newWidth, newHeight, adelta, bdelta, height, width, X0, Y0, x, y, X, Y, offset;
    float D, A11, A22, b1, b2;
    u8 *pValSrc, *pValDst;
    float m[6];
    float M[6];

    u8 tmp1, tmp2;
    u8 r, g, b;

    newWidth = pDst->width;
    newHeight = pDst->height;
	dbg_msg("cvWarpAffine: newHeight=%d, newWidth=%d\n", newHeight, newWidth);

    for (i=0; i<6; i++) {
        m[i] = pM->data.f[i];
        dbg_msg("Pm %d: %f\n", i, m[i]);        
    } 	

    D = m[0]*m[4] - m[1]*m[3];
    D = D != 0 ? (float)1./D : 0;
    A11 = m[4]*D;
    A22 = m[0]*D;
    m[0] = A11; m[1] *= -D;
    m[3] *= -D; m[4] = A22;
    b1 = -m[0]*m[2] - m[1]*m[5];
    b2 = -m[3]*m[2] - m[4]*m[5];
    m[2] = b1;
    m[5] = b2;

    M[0] = m[0];
    M[1] = m[1];
    M[2] = m[2];
    M[3] = m[3];
    M[4] = m[4];
    M[5] = m[5];

    width = pSrcImg->col;
    height = pSrcImg->row;

    dbg_msg("cvWarpAffine: height=%d, width=%d\n", height, width);

    pValSrc = pSrcImg->pRGB;
    pValDst = (u8 *)pDst->imageData;

    /* To prevent from floating point operations,
     * convert to int by scaling up then scaling down (may overflow?) */
    offset = 0;
    for (y = 0; y < newHeight; ++y) {
        X0 = (int) ((M[1]*y + M[2]) * 1024);
        Y0 = (int) ((M[4]*y + M[5]) * 1024);

        for (x = 0; x < newWidth; x++) {
            adelta = (int) (M[0]*x*1024);
            bdelta = (int) (M[3]*x*1024);
            X = (X0 + adelta + ROUND_DELTA) >> 10;
            Y = (Y0 + bdelta + ROUND_DELTA) >> 10;
            r = g = b = 0;

            if (X < width && Y < height) {
                if (format == NPU_FORMAT_RGB565) {
                    tmp1 = pValSrc[2*(Y*width+X)];
                    tmp2 = pValSrc[2*(Y*width+X)+1];
                    r = (tmp2 >>3)<<3;
                    g = ((tmp2 & 0x07) << 5) + ((tmp1 & 0xe0) >> 3);
                    b = (tmp1 & 0x1f) << 3;
                } else if (format == NPU_FORMAT_NIR) {
                    r = g = b = pValSrc[Y*width+X];
                }
            }

            pValDst[offset] = r;
            pValDst[offset+1] = g;
            pValDst[offset+2] = b;
            pValDst[offset+3] = 0;
            offset += 4;
        }
    }
}

/************ global function **********/
/*
*  landmarks: point to the landmark array
*  img: original RGBA image which has all channels are 
*       laid sequentially RRRRRR...GGGGGG....BBBB
*  rc:  the up left coordiates of the croped rect box 
*  return: pre-processed image, will be pass to emulator for calcualtion
*/
static float vals[] = {
    30.2946, 51.6963, 65.5318, 51.5014, 48.0252, 71.7366, 33.5493, 92.3655, 62.7299, 92.2041
};

int kdp_preproc_fr(int model_id, struct kdp_image *image_p, void *params_p)
{
    struct landmark_result *lm_p;

    img_rgb8_channels_t img_s;
    img_rgb8_channels_t *img = &img_s;
    IplImage dstImg;
    uint8_t * pDst;
    float *landmarks;
    uint32_t src_format;
    Matrix pM;
    Matrix *srcMat, *dstMat, *tMat;
    int i, len;

    img->pRGB = (uint8_t *)(image_p->raw_img_p->image_mem_addr);
    img->row = image_p->raw_img_p->input_row;
    img->col = image_p->raw_img_p->input_col;
    lm_p = (struct landmark_result *)(image_p->raw_img_p->results[1].result_mem_addr);
    landmarks = (float *)lm_p->marks;

    dbg_msg("kdp_preproc_FR: raw_row=%d, raw_col=%d, raw_img_addr=%x\n", img->row, img->col, (uint32_t)img->pRGB);

    // get landmark in original image, +crop x, y
    for(i = 0; i < LAND_MARK_POINTS; i++)
    {
        landmarks[2*i] =  lm_p->marks[i].x;
        landmarks[2*i + 1] =  lm_p->marks[i].y;
    }

    dbg_msg("Pre_recog, using landmarks:\n");
    for(int i = 0; i < LAND_MARK_POINTS*2; i++)
    {
       dbg_msg("landmarks[%d]=%f\n", i, landmarks[i]);
    }

    srcMat = CreateLandMarkMatrix(vals, true);
    dstMat = CreateLandMarkMatrix(landmarks, false);
    tMat = SimilarityTransform(dstMat, srcMat);

    FreeMatrix(srcMat);
    FreeMatrix(dstMat);

    if (tMat == NULL) {
        err_msg("kdp_preproc_fr: tMat=NULL\n");
        return -1;
    }

    pM.data.f = tMat->data.f;
    pM.height = 2;
    pM.width = 3;
    pM.type = MT_FLOAT;

    // Create dst IplImage for alignment result.
    pDst = (uint8_t *)image_p->input_mem_addr;
    memset(pDst, 0, image_p->input_col*image_p->input_row*4);
    dstImg.depth = IPL_DEPTH_8U;
    dstImg.width = image_p->input_col;
    dstImg.height = image_p->input_row;
    dstImg.imageData = (char *)pDst;

    // Do alignment
    src_format = image_p->raw_img_p->format;
    cvWarpAffine(img, &dstImg, &pM, src_format & IMAGE_FORMAT_NPU);

    FreeMatrix(tMat);

    if (src_format & (uint32_t)IMAGE_FORMAT_SUB128) {
        len = image_p->input_col * image_p->input_row * 4;
        for (i = 0; i < len; i++) {
            *pDst -= 128;
            pDst++;
        }
    }

    return 0;
}
