/**
 * @file      memory.h
 * @brief     Memory functions
 * @version $Id: memory.c 555 2018-08-14 03:13:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

//#include "base.h"

#define LINUX_C_SYSTEM

#include "memory.h"

#ifdef LINUX_C_SYSTEM

#include <stdlib.h>

void MemReset(void *ptr)
{
}

void* MemGetCur(void)
{
    return NULL;
}

void* MemAlloc(size_t size)
{
    return malloc(size);
}

void* MemCalloc(size_t nmemb, size_t size)
{
    return calloc(nmemb, size);
}

void MemFree(void *ptr)
{
    free(ptr);
}

#else

#include <string.h>
//#include <stdio.h>

#define MEM_BULK_SIZE          (1 << 20)
#define MEM_ADDR_ALIGN_MASK    (0x1f)

static char _sMemBulk[MEM_BULK_SIZE] __attribute__((at(0x68000000)));
//static char _sMemBulk[16];

static char *_sCur = NULL;
static char *_sPeak = NULL;

static void MemInit()
{
    _sCur = _sMemBulk;
    _sPeak = _sMemBulk + MEM_BULK_SIZE;
}

void MemReset(void *ptr)
{
    _sCur = (char*) ptr;
//	  printf("Resetting memory to:%p...\n", ptr);
}

void* MemGetCur(void)
{
    return _sCur;
}

void* MemAlloc(size_t size)
{
    if (_sCur == NULL) {
        MemInit();
    }

    if(size == 0) return NULL;

    if(size & MEM_ADDR_ALIGN_MASK) {
        size_t s = size >> 5;
        s++;
        size = s << 5;
    }

    char* addr = _sCur;

//		printf("Allocating memory size:%d, add:%p...\n", size, addr);
    if (addr + size < _sPeak) {
        _sCur = (void*) (addr + size);
        return (void*) addr;
    } else {
        return NULL;
    }
}

void* MemCalloc(size_t nmemb, size_t size)
{
    size_t totalSize = nmemb*size;
    void *pData = MemAlloc(totalSize);

    if (pData) {
        memset(pData, 0, totalSize);
    }

    return pData;
}

void MemFree(void *ptr)
{
}

#endif
