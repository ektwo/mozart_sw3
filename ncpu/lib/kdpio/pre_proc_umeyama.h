/**
 * @file      umeyama.h
 * @brief     umeyama header
 * @version $Id: umeyama.h 192 2018-04-25 03:07:17Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */


#ifndef __KNERON_PRE_PROC_UMEYAMA
#define __KNERON_PRE_PROC_UMEYAMA

#include "pre_proc_transform_matrix.h"

typedef struct _Matrix Matrix;

Matrix* umeyama(const Matrix *src, const Matrix *dst, int estimate_scale);

#endif /* __KNERON_PRE_PROC_UMEYAMA */
