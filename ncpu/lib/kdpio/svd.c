/**
 * @file      svd.c
 * @brief     Single Value Decomposition
 * @version $Id: svd.c 245 2018-05-03 04:01:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include "pre_proc_transform_matrix.h"
#include <stdlib.h>
#include "utils.h"
//#include "memory.h"
#include "dbg.h"

/* numpy.finfo(np.float32).eps */
#define RANK_EPSILON     1.1920929e-07f

#define SIGN(a,b) ((b) >= 0.0f ? ABS(a) : -ABS(a))
#define V(a,i,j)  (a[i*_MTX_WIDTH+j])

/**
 * Sqrt implementation
 * @ref https://www.codeproject.com/Articles/69941/Best-Square-Root-Method-Algorithm-Function-Precisi
 */
static float _sqrt(const float x)
{
    union {
        int i;
        float x;
    } u;
    u.x = x;
    u.i = (1<<29) + (u.i >> 1) - (1<<22);

    // Two Babylonian Steps (simplified from:)
    // u.x = 0.5f * (u.x + x/u.x);
    // u.x = 0.5f * (u.x + x/u.x);
    u.x =       u.x + x/u.x;
    u.x = 0.25f*u.x + x/u.x;

    return u.x;
}

static float _Pythag(float a, float b)
{
    float at = ABS(a), bt = ABS(b), ct, result;

    if (at > bt)       { ct = bt / at; result = at * _sqrt(1.0f + ct * ct); }
    else if (bt > 0.0f) { ct = at / bt; result = bt * _sqrt(1.0f + ct * ct); }
    else result = 0.0f;
    return(result);
}

/*
 * svdcomp - SVD decomposition routine.
 * (Copied & modified from http://www.public.iastate.edu/~dicook/JSS/paper/code/svd.c)
 *
 * Takes an mxn matrix a and decomposes it into udv, where u,v are
 * left and right orthogonal transformation matrices, and d is a
 * diagonal matrix of singular values.
 *
 * This routine is adapted from svdecomp.c in XLISP-STAT 2.1 which is
 * code from Numerical Recipes adapted by Luke Tierney and David Betz.
 *
 * Input to dsvd is as follows:
 *   a = mxn matrix to be decomposed, gets overwritten with u
 *   m = row dimension of a
 *   n = column dimension of a
 *   w = returns the vector of singular values of a
 *   v = returns the right orthogonal transformation matrix
 */
static int _SVD(float *a, int m, int n, float *w, float *v)
{
    int _MTX_WIDTH = n;
    int flag, i, its, j, jj, k, l = 0, nm = 0;
    float c, f, h, s, x, y, z;
    float anorm = 0.0f, g = 0.0f, scale = 0.0f;
    float *rv1;

    if (m < n) {
        err_msg("#rows must be > #cols \n");
        return(0);
    }

    rv1 = (float *)malloc((unsigned int) n*sizeof(float));

/* Householder reduction to bidiagonal form */
    for (i = 0; i < n; i++)
    {
        /* left-hand reduction */
        l = i + 1;
        rv1[i] = scale * g;
        g = s = scale = 0.0f;
        if (i < m)
        {
            for (k = i; k < m; k++)
                scale += ABS((float)V(a,k,i));
            if (scale)
            {
                for (k = i; k < m; k++)
                {
                    V(a,k,i) = (float)((float)V(a,k,i)/scale);
                    s += ((float)V(a,k,i) * (float)V(a,k,i));
                }
                f = (float)V(a,i,i);
                g = -SIGN(_sqrt(s), f);
                h = f * g - s;
                V(a,i,i) = (float)(f - g);
                if (i != n - 1)
                {
                    for (j = l; j < n; j++)
                    {
                        for (s = 0.0, k = i; k < m; k++)
                            s += ((float)V(a,k,i) * (float)V(a,k,j));
                        f = s / h;
                        for (k = i; k < m; k++)
                            V(a,k,j) += (float)(f * (float)V(a,k,i));
                    }
                }
                for (k = i; k < m; k++)
                    V(a,k,i) = (float)((float)V(a,k,i)*scale);
            }
        }
        w[i] = (float)(scale * g);

        /* right-hand reduction */
        g = s = scale = 0.0;
        if (i < m && i != n - 1)
        {
            for (k = l; k < n; k++)
                scale += ABS((float)V(a,i,k));
            if (scale)
            {
                for (k = l; k < n; k++)
                {
                    V(a,i,k) = (float)((float)V(a,i,k)/scale);
                    s += ((float)V(a,i,k) * (float)V(a,i,k));
                }
                f = (float)V(a,i,l);
                g = -SIGN(_sqrt(s), f);
                h = f * g - s;
                V(a,i,l) = (float)(f - g);
                for (k = l; k < n; k++)
                    rv1[k] = (float)V(a,i,k) / h;
                if (i != m - 1)
                {
                    for (j = l; j < m; j++)
                    {
                        for (s = 0.0, k = l; k < n; k++)
                            s += ((float)V(a,j,k) * (float)V(a,i,k));
                        for (k = l; k < n; k++)
                            V(a,j,k) += (float)(s * rv1[k]);
                    }
                }
                for (k = l; k < n; k++)
                    V(a,i,k) = (float)((float)V(a,i,k)*scale);
            }
        }
        anorm = MAX(anorm, (ABS((float)w[i]) + ABS(rv1[i])));
    }

    /* accumulate the right-hand transformation */
    for (i = n - 1; i >= 0; i--)
    {
        if (i < n - 1)
        {
            if (g)
            {
                for (j = l; j < n; j++)
                    V(v,j,i) = (float)(((float)V(a,i,j) / (float)V(a,i,l)) / g);
                /* float division to avoid underflow */
                for (j = l; j < n; j++)
                {
                    for (s = 0.0, k = l; k < n; k++)
                        s += ((float)V(a,i,k) * (float)V(v,k,j));
                    for (k = l; k < n; k++)
                        V(v,k,j) += (float)(s * (float)V(v,k,i));
                }
            }
            for (j = l; j < n; j++)
                V(v,i,j) = V(v,j,i) = 0.0;
        }
        V(v,i,i) = 1.0;
        g = rv1[i];
        l = i;
    }

    /* accumulate the left-hand transformation */
    for (i = n - 1; i >= 0; i--)
    {
        l = i + 1;
        g = (float)w[i];
        if (i < n - 1)
            for (j = l; j < n; j++)
                V(a,i,j) = 0.0;
        if (g)
        {
            g = 1.0f / g;
            if (i != n - 1)
            {
                for (j = l; j < n; j++)
                {
                    for (s = 0.0, k = l; k < m; k++)
                        s += ((float)V(a,k,i) * (float)V(a,k,j));
                    f = (s / (float)V(a,i,i)) * g;
                    for (k = i; k < m; k++)
                        V(a,k,j) += (float)(f * (float)V(a,k,i));
                }
            }
            for (j = i; j < m; j++)
                V(a,j,i) = (float)((float)V(a,j,i)*g);
        }
        else
        {
            for (j = i; j < m; j++)
                V(a,j,i) = 0.0;
        }
        V(a,i,i) = V(a,i,i)+1;
    }

    /* diagonalize the bidiagonal form */
    for (k = n - 1; k >= 0; k--)
    {                             /* loop over singular values */
        for (its = 0; its < 30; its++)
        {                         /* loop over allowed iterations */
            flag = 1;
            for (l = k; l >= 0; l--)
            {                     /* test for splitting */
                nm = l - 1;
                if (ABS(rv1[l]) + anorm == anorm)
                {
                    flag = 0;
                    break;
                }
                if (ABS((float)w[nm]) + anorm == anorm)
                    break;
            }
            if (flag)
            {
                c = 0.0;
                s = 1.0;
                for (i = l; i <= k; i++)
                {
                    f = s * rv1[i];
                    if (ABS(f) + anorm != anorm)
                    {
                        g = (float)w[i];
                        h = _Pythag(f, g);
                        w[i] = (float)h;
                        h = 1.0f / h;
                        c = g * h;
                        s = (- f * h);
                        for (j = 0; j < m; j++)
                        {
                            y = (float)V(a,j,nm);
                            z = (float)V(a,j,i);
                            V(a,j,nm) = (float)(y * c + z * s);
                            V(a,j,i) = (float)(z * c - y * s);
                        }
                    }
                }
            }
            z = (float)w[k];
            if (l == k)
            {                  /* convergence */
                if (z < 0.0f)
                {              /* make singular value nonnegative */
                    w[k] = (float)(-z);
                    for (j = 0; j < n; j++)
                        V(v,j,k) = (-V(v,j,k));
                }
                break;
            }
            if (its >= 30) {
                free((void*) rv1);
                err_msg("No convergence after 30,000! iterations \n");
                return(0);
            }

            /* shift from bottom 2 x 2 minor */
            x = (float)w[l];
            nm = k - 1;
            y = (float)w[nm];
            g = rv1[nm];
            h = rv1[k];
            f = ((y - z) * (y + z) + (g - h) * (g + h)) / (2.0f * h * y);
            g = _Pythag(f, 1.0);
            f = ((x - z) * (x + z) + h * ((y / (f + SIGN(g, f))) - h)) / x;

            /* next QR transformation */
            c = s = 1.0;
            for (j = l; j <= nm; j++)
            {
                i = j + 1;
                g = rv1[i];
                y = (float)w[i];
                h = s * g;
                g = c * g;
                z = _Pythag(f, h);
                rv1[j] = z;
                c = f / z;
                s = h / z;
                f = x * c + g * s;
                g = g * c - x * s;
                h = y * s;
                y = y * c;
                for (jj = 0; jj < n; jj++)
                {
                    x = (float)V(v,jj,j);
                    z = (float)V(v,jj,i);
                    V(v,jj,j) = (float)(x * c + z * s);
                    V(v,jj,i) = (float)(z * c - x * s);
                }
                z = _Pythag(f, h);
                w[j] = (float)z;
                if (z)
                {
                    z = 1.0f / z;
                    c = f * z;
                    s = h * z;
                }
                f = (c * g) + (s * y);
                x = (c * y) - (s * g);
                for (jj = 0; jj < m; jj++)
                {
                    y = (float)V(a,jj,j);
                    z = (float)V(a,jj,i);
                    V(a,jj,j) = (float)(y * c + z * s);
                    V(a,jj,i) = (float)(z * c - y * s);
                }
            }
            rv1[l] = 0.0;
            rv1[k] = f;
            w[k] = (float)x;
        }
    }
    free((void*) rv1);
    return(1);
}

/**
 * Get rank of matrix using SVD output
 * Check numpy.linalg.matrix_rank()
 */
static int GetRank(Matrix *pS, int m, int n)
{
    /* Based on pydoc of numpy.linalg.matrix_rank() */
    float thresh = MaxVal(pS) * MAX(m, n) * RANK_EPSILON;
    int rank = 0;
    int size = pS->height*pS->width;
    float *pVal = pS->data.f;

    while (size--) {
        if (*pVal++ > thresh) {
            rank++;
        }
    }

    return rank;
}

/* #include "svd.original.h" */

/**
 * Single Value Decomposition
 * Note that the result may not match numpy.linalg.svd(A)
 * since SVD result is not unique
 */
void SVD(Matrix *pA, Matrix **ppU, Matrix **ppS, Matrix **ppV, int *pRank)
{
    /* return TestOriginalSvd(); */

    MAT_TYPE type = MT_FLOAT;
    int m = pA->height;
    int n = pA->width;
    Matrix *pVT = MakeMatrix(n, n, type);

    *ppU = CopyMatrix(pA, type);
    *ppS = MakeMatrix(n, 1, type);
    _SVD((*ppU)->data.f, m, n, (*ppS)->data.f, pVT->data.f);
    *pRank = GetRank(*ppS, m, n);
    *ppV = TransposeMat(pVT);
    FreeMatrix(pVT);
}
