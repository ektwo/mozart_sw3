/**
 * @file      base.h
 * @brief     Basic utils & struct
 * @version $Id: base.h 255 2018-05-03 12:17:23Z asta.lin $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __KNERON_APP_FLOW_BASE_H__
#define __KNERON_APP_FLOW_BASE_H__

#ifdef RELEASE_BUILD
#define assert(...)
#define printf(...)
#define fprintf(...)
#define sprintf(...)
#define puts(...)
#define WriteImage(...)
#else
#include <stdio.h>
#include <assert.h>
#endif

/******************************************************************************
 * System control definition
 *****************************************************************************/
/* Code system control definition
 * Use makefile to define SYSMOD (e.g. -DSYSMOD=SYS_PC) */
#define BIT(n)                     (0x01 << n)
#define SYS_PC                     BIT(0)      // PC with file I/O pattern
#define SYS_XILINX                 BIT(1)      // Xilinx
#define SYS_XILINX_EMU             BIT(2)      // Xilinx with test pattern
#define SYS_ANDES                  BIT(3)      // Andes
#define SYS_ANDES_EMU              BIT(4)      // Andes with test pattern
#define SYS_EMU                    (SYS_PC | SYS_XILINX_EMU | SYS_ANDES_EMU)

/* Empty macro for system emulation */
#if (SYSMOD & SYS_EMU)
#define Xil_Out32(m, n)            asm ("nop")
#define NewBdRing(m, n, p)         0
#define Xil_In32(n)                (n+1)
#endif

/******************************************************************************
 * GENERAL VALUE
 *****************************************************************************/
#define MAX_BOX_COUNT 128

#define MIN(a,b)      (((a)<(b))?(a):(b))
#define MAX(a,b)      (((a)>(b))?(a):(b))
//#define ABS(a)        (((a)>=0)?(a):(-a))
#define FLOOR(val)    ((int)(val) - ((int)(val) > val))

/* Type defintion */
#define U32  unsigned int
#define U16  unsigned short
#define S32  int
#define S16  short int
#define U8   unsigned char
#define S8   char

typedef unsigned char   u8;
typedef unsigned char   uchar;
typedef unsigned short  u16;
typedef unsigned int    u32;
typedef signed char     s8;
typedef signed short    s16;
typedef signed int      s32;

/* (x1,y1), (x2, y2) are left-bottom & right-top points of box */
typedef struct _Box {
    int x1;
    int y1;
    int x2;
    int y2;
} Box;

//typedef struct _Size {
//    int width;
//    int height;
//} Size;

/* Return status control */
#define XST_SUCCESS         0
#define XST_FAILURE         1

/* Image control and size definition */
#define IMAGE_WIDTH         360
#define IMAGE_HEIGHT        640
#define FD_IMAGE_WIDTH      200
#define FD_IMAGE_HEIGHT     200
#define DET_WINDOWS_W       48
#define DET_WINDOWS_H       48
#define FD_ADJ_THRD         0

#define CV_MAX_DIM            32

typedef struct _IplROI
{
    int  coi; /**< 0 - no COI (all channels are selected), 1 - 0th channel is selected ...*/
    int  xOffset;
    int  yOffset;
    int  width;
    int  height;
}
IplROI;


#define IPL_DEPTH_SIGN 0x80000000

#define IPL_DEPTH_1U     1
#define IPL_DEPTH_8U     8
#define IPL_DEPTH_16U   16
#define IPL_DEPTH_32F   32

#define IPL_DEPTH_8S  (IPL_DEPTH_SIGN| 8)
#define IPL_DEPTH_16S (IPL_DEPTH_SIGN|16)
#define IPL_DEPTH_32S (IPL_DEPTH_SIGN|32)

#define IPL_DATA_ORDER_PIXEL  0
#define IPL_DATA_ORDER_PLANE  1

#define IPL_ORIGIN_TL 0
#define IPL_ORIGIN_BL 1

#define IPL_ALIGN_4BYTES   4
#define IPL_ALIGN_8BYTES   8
#define IPL_ALIGN_16BYTES 16
#define IPL_ALIGN_32BYTES 32

#define IPL_ALIGN_DWORD   IPL_ALIGN_4BYTES
#define IPL_ALIGN_QWORD   IPL_ALIGN_8BYTES

#define IPL_BORDER_CONSTANT   0
#define IPL_BORDER_REPLICATE  1
#define IPL_BORDER_REFLECT    2
#define IPL_BORDER_WRAP       3

typedef struct
_IplImage
{
    int  nSize;             /**< sizeof(IplImage) */
    int  ID;                /**< version (=0)*/
    int  nChannels;         /**< Most of OpenCV functions support 1,2,3 or 4 channels */
    int  alphaChannel;      /**< Ignored by OpenCV */
    int  depth;             /**< Pixel depth in bits: IPL_DEPTH_8U, IPL_DEPTH_8S, IPL_DEPTH_16S,
                               IPL_DEPTH_32S, IPL_DEPTH_32F and IPL_DEPTH_64F are supported.  */
    char colorModel[4];     /**< Ignored by OpenCV */
    char channelSeq[4];     /**< ditto */
    int  dataOrder;         /**< 0 - interleaved color channels, 1 - separate color channels.
                               cvCreateImage can only create interleaved images */
    int  origin;            /**< 0 - top-left origin,
                               1 - bottom-left origin (Windows bitmaps style).  */
    int  align;             /**< Alignment of image rows (4 or 8).
                               OpenCV ignores it and uses widthStep instead.    */
    int  width;             /**< Image width in pixels.                           */
    int  height;            /**< Image height in pixels.                          */
    struct _IplROI *roi;    /**< Image ROI. If NULL, the whole image is selected. */
    struct _IplImage *maskROI;      /**< Must be NULL. */
    void  *imageId;                 /**< "           " */
//    struct _IplTileInfo *tileInfo;  /**< "           " */
    int  imageSize;         /**< Image data size in bytes
                               (==image->height*image->widthStep
                               in case of interleaved data)*/
    char *imageData;        /**< Pointer to aligned image data.         */
    int  widthStep;         /**< Size of aligned image row in bytes.    */
    int  BorderMode[4];     /**< Ignored by OpenCV.                     */
    int  BorderConst[4];    /**< Ditto.                                 */
    char *imageDataOrigin;  /**< Pointer to very origin of image data
                               (not necessarily aligned) -
                               needed for correct deallocation */

} IplImage;


#endif /* __KNERON_APP_FLOW_BASE_H__ */
