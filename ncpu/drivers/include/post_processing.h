/*
 * Kneron Header for KDP on Mozart
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */

#ifndef POST_PROCESSING_H
#define POST_PROCESSING_H

void post_processing_init(void);
int post_processing(struct kdp_image *image_p, int model_id, void *params_p);

#endif
