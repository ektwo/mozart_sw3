/*
 * Kneron Header for KDP on Mozart
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */

#ifndef KDPIO_H
#define KDPIO_H

#define MAX_MODEL_REGISTRATIONS          32

/* Type of Operations */
enum {
    NODE_TYPE_IN,
    NODE_TYPE_CPU,
    NODE_TYPE_OUT,
};

/* Type of CPU Operations */
enum {
    CPU_OP_RELU,
    CPU_OP_MAXPOOL,
    CPU_OP_BN,
    CPU_OP_AVGPOOL,
};

/* Structures of Data Nodes */
struct super_node {
	uint32_t node_id;
	uint32_t addr;
	uint32_t row_start;
	uint32_t col_start;
	uint32_t ch_start;
	uint32_t row_length;
	uint32_t col_length;
	uint32_t ch_length;
};

struct data_node {
	uint32_t node_id;
	uint32_t supernum;
	uint32_t data_format;
	uint32_t row_start;
	uint32_t col_start;
	uint32_t ch_start;
	uint32_t row_length;
	uint32_t col_length;
	uint32_t ch_length;
	struct super_node node_list[1];
};

/* Structure of Input Operation */
struct in_node {
	uint32_t node_id;
	uint32_t next_npu;
};

/* Structure of Output Operation */
struct out_node {
	uint32_t node_id;
	uint32_t supernum;
	uint32_t data_format;
	uint32_t row_start;
	uint32_t col_start;
	uint32_t ch_start;
	uint32_t row_length;
	uint32_t col_length;
	uint32_t ch_length;
	uint32_t output_index;
	uint32_t output_radix;
	uint32_t output_scale;
	struct super_node node_list[1];
};

/* Structure of CPU Operation */
struct cpu_node {
	uint32_t node_id;
	uint32_t input_datanode_num;
	uint32_t op_type;
	/* There will be more parameter here for cpu operation  */
	uint32_t in_num_row;
	uint32_t in_num_col;
	uint32_t in_num_ch;
	uint32_t out_num_row;
	uint32_t out_num_col;
	uint32_t out_num_ch;
	uint32_t h_pad;
	uint32_t w_pad;
	uint32_t kernel_h;
	uint32_t kernel_w;
	uint32_t stride_h;
	uint32_t stride_w;
	uint32_t input_radix;
	uint32_t output_radix;
	struct data_node output_datanode;
	struct data_node input_datanode[1];
};

/* Structure of CNN Header in setup.bin */
struct cnn_header {
	uint32_t crc;
	uint32_t version;
	uint32_t reamaining_models;
	uint32_t model_type;
	uint32_t app_type;
	uint32_t dram_start;
	uint32_t dram_size;
	uint32_t input_row;
	uint32_t input_col;
	uint32_t input_channel;
	uint32_t cmd_start;
	uint32_t cmd_size;
	uint32_t weight_start;
	uint32_t weight_size;
	uint32_t input_start;
	uint32_t input_size;
	uint32_t input_radix;
	uint32_t output_nums;
};

/* Structure of setup.bin file */
struct setup_struct {
    struct cnn_header       header;

    union {
        struct in_node      in_nd;
        struct out_node     out_nd;
        struct cpu_node     cpu_nd;
    } nodes[1];
};

/* KDP image structure */
struct kdp_image {
    /* Original image and model */
    struct kdp_img_raw          *raw_img_p;
    struct kdp_model            *model_p;

    int         model_id;
    char        *setup_mem_p;

    /* CNN input dimensions */
    uint32_t    input_row;
    uint32_t    input_col;
    uint32_t    input_channel;

    /* number of bits for input fraction */
    uint32_t    input_radix;

    /* input image in memory for NPU */
    uint32_t    input_mem_addr;
    int32_t     input_mem_len;

    /* output data memory from NPU */
    uint32_t    output_mem_addr;
    int32_t     output_mem_len;

    /* result data memory from post processing */
    uint32_t    result_mem_addr;
    int32_t     result_mem_len;

    /* output data format from NPU
     *     BIT(0): =0, 8-bits
     *             =1, 16-bits
     */
    uint32_t    output_format;      

    /* number of bits for output fraction */
    uint32_t    output_radix;

    /* output scale: a float */
    uint32_t    output_scale;

    /* output data dimensions */
	uint32_t    output_row;
	uint32_t    output_col;
	uint32_t    output_channel;

    /* Input/Output working buffers for NPU */
    uint32_t    input_mem_addr2;
    int32_t     input_mem_len2;

    uint32_t    output_mem_addr2;
    int32_t     output_mem_len2;
};

/* Prototypes for callback functions */
typedef int (* cpu_op_fn)(int cpu_op, struct kdp_image *image_p, void *params_p);
typedef int (* pre_proc_fn)(int model_id, struct kdp_image *image_p, void *params_p);
typedef int (* post_proc_fn)(int model_id, struct kdp_image *image_p, void *params_p);
typedef void (* pre_post_fn)(void *dst_p, void *src_p, int size);

/* API */

/* Return code */
#define RET_ERROR               -1
#define RET_NO_ERROR            0
#define RET_NEXT_PRE_PROC       1
#define RET_NEXT_NPU            2
#define RET_NEXT_CPU            3
#define RET_NEXT_POST_PROC      4

/**
 * kdpio_init() - initialize kdpio
 *
 * This function tells kdpio to initialize the platform and
 * resources for NPU support.
 *
 */
void kdpio_init(void);

/* Registration APIs */
int kdpio_cpu_op_register(int cpu_op, cpu_op_fn cof);
void kdpio_cpu_op_unregister(int cpu_op, cpu_op_fn cof);
int kdpio_pre_processing_register(int model_id, pre_proc_fn ppf);
void kdpio_pre_processing_unregister(int model_id, pre_proc_fn ppf);
int kdpio_post_processing_register(int model_id, post_proc_fn ppf);
void kdpio_post_processing_unregister(int model_id, post_proc_fn ppf);

/**
 * kdpio_set_model() - set the model for NPU processing
 *
 * @model_p: pointer to struct kdp_model with buffers of setup,
 *          command and weights.
 *
 * This function tells kdpio the CNN model to use for processing next
 * images.
 *
 * Return value:
 *  none
 */
void kdpio_set_model(struct kdp_image *image_p, struct kdp_model *model_p);

/**
 * kdpio_run_image() - run the image using the set model
 *
 * @image_p: pointer to struct kdp_image with buffers of input image
 *          and output.
 *
 * This function tells kdpio to start run an image using the current set model,
 * and put the result in result buffer after post processing.
 *
 * Return value:
 * 0 : success
 * <0 : error
 */
int kdpio_run_preprocess(struct kdp_image *image_p);
int kdpio_run_npu_op(struct kdp_image *image_p);

/**
 * kdpio_run_image_cont() - continue run the image
 *
 * @image_p: pointer to struct kdp_image with buffers of input image
 *          and output.
 *
 * This function tells kdpio to continue run the same image as used early
 * with kdpio_run_image(), and should be called when NPU comes back with
 * finished computation.
 * 
 * Post processing will happen in this call to convert NPU output to needed
 * result.
 *
 * Return value:
 * 0 : success
 * <0 : error
 */
int kdpio_run_cpu_op(struct kdp_image *image_p);
int kdpio_run_postprocess(struct kdp_image *image_p, pre_post_fn perf_improv_fn);

/**
 * kdpio_exit() - exit kdpio
 *
 * This function tells kdpio to free allocated resources and
 * quit from NPU support.
 *
 */
void kdpio_exit(void);

#endif
