/*
 * Kneron Header for KDP on Mozart
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */

#ifndef PRE_PROCESSING_H
#define PRE_PROCESSING_H

void pre_processing_init(void);
int pre_processing(struct kdp_image *image_p, int model_id, void *params_p);

#endif
