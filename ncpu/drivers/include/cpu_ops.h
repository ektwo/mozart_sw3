/*
 * Kneron Header for KDP on Mozart
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#ifndef CPU_OPS_H
#define CPU_OPS_H

void kdp_do_cpu_ops_init(void);
int kdp_do_cpu_ops(struct kdp_image *image_p, struct cpu_node *cpu_p);

#endif
