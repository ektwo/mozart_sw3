/***************************************************************************
* Copyright  Faraday Technology Corp 2002-2003.  All rights reserved.      *
*--------------------------------------------------------------------------*
* Name:timer.c                                                             *
* Description: Timer library routine                                       *
* Author:                                                        *
****************************************************************************/
//#include "flib.h"
//#include "common_include.h"
#include "io.h"
#include "Driver_Common.h"
#include "kneron_mozart.h"
#include "DrvPWMTMR010.h"

//#define IPMODULE 	TIMER
//#define IPNAME   	FTPWMTMR010

//static UINT32 fLib_Timer_Vectors[MAX_TIMER + 1] = {0,TIMER_FTPWMTMR010_1_IRQ,TIMER_FTPWMTMR010_2_IRQ,TIMER_FTPWMTMR010_3_IRQ,TIMER_FTPWMTMR010_3_IRQ};
static UINT32 TimerBase[MAX_TIMER+1] ={0, PWM_FTPWMTMR010_PA_BASE+0x10 };
//#define timer_base  TIMER_FTPWMTMR010_PA_BASE
fLib_TimerControl *TimerControl[MAX_TIMER+1];


static fLib_TimerStruct ftimer[MAX_TIMER+1];
//funtion prototype
void fLib_Timer_AutoReloadValue(DRVTIMER timer, UINT32 value);
void  Timer_ResetAll(void);
INT32 GetTimerTick(DRVTIMER timer);
INT32 SetTimerTick(UINT32 timer,UINT32 clk_tick);
INT32 SetTimerClkSource(DRVTIMER timer,UINT32 clk);
//INT32 Timer_ConnectIsr(UINT32 timer,PrHandler handler);
UINT32 T1_Tick = 0,T2_Tick = 0, T3_Tick = 0,T4_Tick = 0;
INT32 fLib_Timer_IntClear(DRVTIMER timer);
INT32 fLib_Timer_AutoReloadEnable(DRVTIMER timer);
INT32 fLib_Timer_Counter(DRVTIMER timer);

typedef void (*FNTimerISR)(void);

static void PWMTimer1_IRQHandler(void)
{	
 	fLib_Timer_IntClear(DRVPWMTMR1);
    ++T1_Tick;
}

static void PWMTimer2_IRQHandler(void)
{	
 	fLib_Timer_IntClear(DRVPWMTMR2);
    ++T2_Tick;
}

static void PWMTimer3_IRQHandler(void)
{	
 	fLib_Timer_IntClear(DRVPWMTMR3);
    ++T3_Tick;
}

static void PWMTimer4_IRQHandler(void)
{	
 	fLib_Timer_IntClear(DRVPWMTMR4);
    ++T4_Tick;
}

/* Routine to disable a timer and free-up the associated IRQ */
INT32 fLib_Timer_Close(DRVTIMER timer)
{
	UINT32 timer_irq;


    if (timer == 0 || timer > MAX_TIMER)
        return FALSE;

    if(!fLib_Timer_Disable(timer))
        return FALSE;   /* Stop the timer first */

	switch(timer)
	{
		case DRVPWMTMR1:
			timer_irq = PWM_FTPWMTMR010_0_IRQ;
		break;
		case DRVPWMTMR2:
			timer_irq = PWM_FTPWMTMR010_1_IRQ;
		break;
		case DRVPWMTMR3:
			timer_irq = PWM_FTPWMTMR010_2_IRQ;
		break;
		case DRVPWMTMR4:
			timer_irq = PWM_FTPWMTMR010_3_IRQ;
		break;
		default:
			return FALSE;
	}	
		
	NVIC_DisableIRQ((IRQn_Type)timer_irq);
		
    return TRUE;
}


INT32 fLib_Timer_IOCtrl(fLib_Timer_IoType IoType,DRVTIMER timer,UINT32 tick)
{

    switch(IoType)
    {
    case IO_TIMER_RESETALL:
        Timer_ResetAll();
        break;
    case IO_TIMER_GETTICK:
        return GetTimerTick(timer);
       // break;
    case IO_TIMER_SETTICK:
        return SetTimerTick(timer,tick);
       // break;
    case IO_TIMER_SETCLKSRC:
        return SetTimerClkSource(timer,tick);
       // break;
    default:
        return FALSE;
    }

    return TRUE;
}

/* Routine to start the specified timer & enable the interrupt */
//BOOL fLib_Timer_Init(DRVTIMER timer,UINT32 tick)
BOOL fLib_Timer_Init(DRVTIMER timer, UINT32 tick)
{
    fLib_TimerStruct *ctimer=&ftimer[timer];
	UINT32 timer_irq;
	FNTimerISR isr;

    if (timer == 0 || timer > MAX_TIMER)
        return FALSE;

		switch(timer)
		{
			case DRVPWMTMR1:
				T1_Tick = 0;
				timer_irq = PWM_FTPWMTMR010_0_IRQ;		
                isr = PWMTimer1_IRQHandler;
			break;
			case DRVPWMTMR2:
				T2_Tick = 0;
				timer_irq = PWM_FTPWMTMR010_1_IRQ;		
                isr = PWMTimer2_IRQHandler;
			break;
			case DRVPWMTMR3:
				T3_Tick = 0;
				timer_irq = PWM_FTPWMTMR010_2_IRQ;		
                isr = PWMTimer3_IRQHandler;
			break;
			case DRVPWMTMR4:
				T4_Tick = 0;
				timer_irq = PWM_FTPWMTMR010_3_IRQ;		
                isr = PWMTimer4_IRQHandler;
			break;
			default:
				return FALSE;
		}

    TimerControl[timer]=(fLib_TimerControl *)(TimerBase[timer]);
	fLib_Timer_Close(timer);

    /* Set the timer tick */
    if(!fLib_Timer_IOCtrl(IO_TIMER_SETTICK,timer,tick))
        return FALSE;

    fLib_Timer_AutoReloadValue(timer,ctimer->Tick);
    /*enable auto and int bit */
    fLib_Timer_AutoReloadEnable(timer);

    if (!fLib_Timer_IntEnable(timer))
        return FALSE;

    NVIC_EnableIRQ((IRQn_Type)timer_irq);
    NVIC_SetVector((IRQn_Type)timer_irq, (uint32_t)isr);

    /* Start the timer ticking */
    if(!fLib_Timer_Enable(timer))
        return FALSE;

    return TRUE;
}

INT32 fLib_Timer_AutoReloadEnable(DRVTIMER timer)
{
    //fLib_TimerStruct *ctimer=&ftimer[timer];

    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;

    TimerControl[timer]->TmAutoLoad=1;
    return TRUE;
}


INT32 fLib_Timer_Counter(DRVTIMER timer)
{
    return inw(TimerBase[timer] + TIMER_CNTO);
}


INT32 fLib_Timer_IntEnable(DRVTIMER timer)
{
    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;

    TimerControl[timer]->TmIntEn=1;
    return TRUE;
}

INT32 fLib_Timer_IntDisable(UINT32 timer)
{
    //fLib_TimerStruct *ctimer=&ftimer[timer];

    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;


      TimerControl[timer]->TmIntEn=0;


    return TRUE;
}


INT32 fLib_Timer_IntModeEnable(UINT32 timer,UINT32 mode)
{
    //fLib_TimerStruct *ctimer=&ftimer[timer];

    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;


      TimerControl[timer]->TmIntMode=mode;


    return TRUE;
}

INT32 fLib_Timer_DmaEnable(UINT32 timer)
{

    //fLib_TimerStruct *ctimer=&ftimer[timer];

    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;


      TimerControl[timer]->TmDmaEn=1;


    return TRUE;
}


INT32 fLib_Timer_DeadZoneEnable(UINT32 timer,UINT32 offset)
{
    //fLib_TimerStruct *ctimer=&ftimer[timer];

    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;


      TimerControl[timer]->TmDeadZone=offset;


    return TRUE;
}

/* This routine starts the specified timer hardware. */
INT32 fLib_Timer_Enable(DRVTIMER timer)
{
    fLib_TimerStruct *ctimer=&ftimer[timer];

    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;

    if(ctimer->Running==TRUE)
        return FALSE;
    TimerControl[timer]->TmUpdate=1;
	TimerControl[timer]->TmStart=1;

    //set the timer status =true
    ctimer->Running=TRUE;

    return TRUE;
}


/* This routine stops the specified timer hardware. */
INT32 fLib_Timer_Disable(DRVTIMER timer)
{
    fLib_TimerStruct *ctimer=&ftimer[timer];

    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;

    /* Disable the Control register bit */
    TimerControl[timer]->TmStart=0;
    TimerControl[timer]->TmUpdate=0;
    TimerControl[timer]->TmOutInv=0;
    TimerControl[timer]->TmDmaEn=0;
    TimerControl[timer]->TmIntEn=0;
    TimerControl[timer]->TmDeadZone=0;
    TimerControl[timer]->TmSrc=0;
    TimerControl[timer]->TmAutoLoad=0;

     //set the timer status=false
    ctimer->Running=FALSE;

    return TRUE;
}

/* This routine starts the specified timer hardware. */
INT32 fLib_Timer_IntClear(DRVTIMER timer)
{
    int value;

   if ((timer == 0) || (timer > MAX_TIMER))
       return FALSE;

    value=1<<(timer-1);
    outw(PWM_FTPWMTMR010_PA_BASE + TIMER_INTSTAT, value);

    return TRUE;
}

void fLib_Timer_CmpValue(DRVTIMER timer, UINT32 value)
{
    outw(TimerBase[timer] + TIMER_COMPARE, value);
}

INT32 SetTimerClkSource(DRVTIMER timer,UINT32 clk)
{
	if ((timer == 0) || (timer > MAX_TIMER))
		return FALSE;

	TimerControl[timer]->TmSrc=clk;

   	return TRUE;
}

#if 0
/* Routine to initialise install requested timer. Stops the timer. */
INT32 Timer_ConnectIsr(UINT32 timer,PrHandler handler)
{
    fLib_TimerStruct *ctimer=&ftimer[timer];
    UINT32 i;

    i = fLib_Timer_Vectors[timer];

	if (request_irq(i, handler, IRQF_SHARED | IRQF_DISABLED, "timer", 0) < 0)
	{
		return FALSE;
	}

    ctimer->Handler = handler;
    ctimer->IntNum = i;     /* INT number */

    return timer;
}
#endif

UINT32 fLib_CurrentT1Tick(void)
{
    return T1_Tick;
}

UINT32 fLib_CurrentT2Tick(void)
{
    return T2_Tick;
}


UINT32 fLib_CurrentT3Tick(void)
{
    return T3_Tick;
}

UINT32 fLib_CurrentT4Tick(void)
{
    return T4_Tick;
}

/////////////////////////////////////////////////////
//
//  Only for detail function call subroutine
//
/////////////////////////////////////////////////////


/* Start-up routine to initialise the timers to a known state */
void Timer_ResetAll(void)
{
    UINT32 i;

    //reset all timer to default value
    for (i = 1; i <= MAX_TIMER; i++)
        fLib_Timer_Disable((DRVTIMER)i);

}

INT32 GetTimerTick(DRVTIMER timer)
{
    UINT32 cur_tick;

    volatile fLib_TimerStruct *ctimer = &ftimer[timer];

    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;

    cur_tick=ctimer->Tick;

    return cur_tick;
}

INT32 SetTimerTick(UINT32 timer,UINT32 clk_tick)
{
    volatile fLib_TimerStruct *ctimer = &ftimer[timer];

    if ((timer == 0) || (timer > MAX_TIMER))
        return FALSE;

    ctimer->Tick=clk_tick;

    return TRUE;
}

void fLib_Timer_AutoReloadValue(DRVTIMER timer, UINT32 value)
{
    outw(TimerBase[timer] + TIMER_LOAD, value);
}

// End of file - timer.c
