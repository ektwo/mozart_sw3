#include <string.h>
#include <stdlib.h>

#include "kneron_mozart.h"
#include "io.h"
#include "types.h"

#include "DrvDMAC020.h"
#include "DrvUART010.h"


#define DMA_CHANNEL_COUNT 8 
#define min_t(x,y) ( x < y ? x: y )

#define DATA_BUF_SIZE		0x100//0x2000  

extern UINT32 rd_bl_len, wr_bl_len;

int DMA_MemToMemTest0(UINT32 *SrcAddr, UINT32 *DstAddr, UINT32 Size);
//int DMA_MemToMemTest1(UINT32 *SrcAddr, UINT32 *DstAddr, UINT32 Size, UINT32 LLPCount);
//int DMA_MemToMemTest2(UINT32 *Addr0, UINT32 *Addr1, UINT32 *Addr2, UINT32 *Addr3, UINT32 Size, UINT32 LLPCount);

UINT32 AHBDMA_Channel = 0;

//void spin_lock_cachesave(char);
//void spin_unlock_cache(void);

//extern void fLib_DisableCache(void);

static char dma_buf1[DATA_BUF_SIZE/*+0x3*/];
static char dma_buf2[DATA_BUF_SIZE/*+0x3*/];
static char dma_buf3[DATA_BUF_SIZE/*+0x3*/];
static char dma_buf4[DATA_BUF_SIZE/*+0x3*/];

uint32_t volatile msTicks;                       // Counter for millisecond Interval


#if 0
void SysTick_Handler (void) {                    // SysTick Interrupt Handler
	  msTicks++;                                     // Increment Counter
}


void WaitForTick (void)  {
  uint32_t curTicks;

  curTicks = msTicks;                            // Save Current SysTick Value
  while (msTicks == curTicks)  {                 // Wait for next SysTick Interrupt
	  __WFE (); 								   // Power-Down until next Event/Interrupt
  }

}

void delay_ms(int msec)
{
    for (;msec != 0; --msec)
    {
        WaitForTick();
    }
}
#endif

#if 0
void DMA_Interrupt_Test(void)
{
	unsigned int *ptr1 = (unsigned int *)dma_buf1;
	unsigned int *ptr2 = (unsigned int *)dma_buf2;
    
    
	
	
	fLib_InitDMA(FALSE, FALSE, 0x0);			
	fLib_DMA_ClearAllInterrupt();  
	
	AHBDMA_Channel = 2;	
	fLib_DMA_ResetChannel(AHBDMA_Channel);	
	fLib_DMA_EnableDMAInt(); //Enable DMA Interrupt			
	fLib_DMA_NormalMode(AHBDMA_Channel, (UINT32)ptr1, (UINT32)ptr2, 0x100, 0, 0, 0, 0, 0, 0, 0, 0);
	fLib_EnableDMAChannel(AHBDMA_Channel);  
	fLib_DMA_WaitDMAInt(AHBDMA_Channel);
	fLib_printf("DMA Intr occurred!\n");	
  fLib_DisableDMAChannel(AHBDMA_Channel);  
	fLib_DMA_DisableDMAInt(); //Disable DMA Interrupt

	AHBDMA_Channel = 1;		
	fLib_DMA_ResetChannel(AHBDMA_Channel);	
	fLib_DMA_EnableDMATCInt(); //Enable DMA Terminal Count Interrupt	
	fLib_DMA_NormalMode(AHBDMA_Channel, (UINT32)ptr1, (UINT32)ptr2, 0x100, 0, 0, 0, 0, 0, 0, 0, 0);
	fLib_EnableDMAChannel(AHBDMA_Channel);  
	fLib_DMA_WaitDMATCInt(AHBDMA_Channel);	
  fLib_printf("DMA Intr TC occurred!\n");			
  fLib_DisableDMAChannel(AHBDMA_Channel);  
	fLib_DMA_DisableDMATCInt(); //Disable DMA Terminal Count Interrupt
}
#endif
void kdp_ddr_dma_copy(unsigned int *src_ptr, unsigned int *dst_ptr, int size)
{
	int i;
	
	//unsigned int *ptr3 = (unsigned int *)dma_buf3;
	//unsigned int *ptr4 = (unsigned int *)dma_buf4; 	

  /*
	char* dma_buf1 = (char*)0x80000000;
	char* dma_buf2 = (char*)0x80001000;
	int index = 0;
	for(index = 0; index < 100; index++){
			dma_buf1[index] = '$';
	}
    
	unsigned int *ptr1 = (unsigned int *)dma_buf1;
	unsigned int *ptr2 = (unsigned int *)dma_buf2;
	
	*/
	int channel_num;
	
  fLib_printf("\rMemory <-> Memory Test\n");
  fLib_InitDMA(FALSE, FALSE, 0x0);	
	fLib_DMA_ClearAllInterrupt(); 
	fLib_DMA_EnableDMAInt(); //Enable DMA Interrupt					
	channel_num = fLib_DMA_ChannelNum();
	fLib_printf("channel num: %d\n", channel_num);
	
  for (i = 0; i < channel_num; i++)
  {
     AHBDMA_Channel = i;
     fLib_DMA_ResetChannel(AHBDMA_Channel);
		 if(!DMA_MemToMemTest0(src_ptr, dst_ptr, DATA_BUF_SIZE))
     {
        //fail("\rFail!\n");
			  fLib_printf("\r!!!!Fail!\n"); //legend
        return;
     }else{
         fLib_printf("!!!!!!!!!!DMA_MemToMemTest0 pass for channel%d\n", i); //legend
     }
	}

}


 int DMA_MemToMemTest0(UINT32 *SrcAddr, UINT32 *DstAddr, UINT32 Size)
{
    UINT32 i, SrcWidth, DstWidth, SrcSize;
    UINT32 *TempAddr;

		//fLib_printf("1\n");
		fLib_printf("DMA_MemToMemTest0 for DMA Channel %d\n", AHBDMA_Channel);  
    /* init source */
    for(i = 0; i < (Size/4); i++)
        *(SrcAddr+i) =(UINT32)( SrcAddr+i);   
#if 1
    for(SrcWidth = 0; SrcWidth <= 2; SrcWidth++) 
    {

        for(DstWidth = 0; DstWidth <= 2; DstWidth++)
        {
    
            for(SrcSize = 0; SrcSize <= 7; SrcSize++)
            {
    
            if((SrcWidth < DstWidth) && (SrcSize == 0))
                {
#if 0                   
                    fLib_printf("Can not use this configure!!! SrcWidth=%d. DstWidth=%d, SrcSize=%d\n", 
                        1 << (SrcWidth + 3), 1 << (DstWidth + 3), ((SrcSize == 0) ? 1 : 1 << (SrcSize+1)));
#endif                      
                    break;
                }
                
                memset(DstAddr, 0, Size);

                fLib_DMA_NormalMode(AHBDMA_Channel, (UINT32)SrcAddr, (UINT32)DstAddr, Size, SrcWidth, DstWidth, SrcSize, 0, 0, 0, 0, 0);             
								fLib_EnableDMAChannel(AHBDMA_Channel);
                fLib_DMA_WaitDMAInt(AHBDMA_Channel);
                fLib_DisableDMAChannel(AHBDMA_Channel);  

                if(memcmp(SrcAddr, DstAddr, Size) != 0)
                {
                    for(i = 0; i < (Size/4); i++)
                    {
                      
                        if(DstAddr[i] != SrcAddr[i])
                        {
							              fLib_printf("======!!!!!!==============>DstAddr[%0.8X] = %0.8X, should be %0.8X\n", i, DstAddr[i], SrcAddr[i]);
                            return FALSE;
                        }
                    }
                }
                

                // clear destination, SRC+size(descrement) <== DST+size(descrement)
                TempAddr = DstAddr; DstAddr = SrcAddr; SrcAddr = TempAddr;
                memset(DstAddr, 0, Size);

                fLib_DMA_NormalMode(AHBDMA_Channel, (UINT32)(SrcAddr)+Size-1, (UINT32)(DstAddr)+Size-1, Size, SrcWidth, DstWidth, SrcSize, 1, 1, 0, 0, 0);
                fLib_EnableDMAChannel(AHBDMA_Channel);
                fLib_DMA_WaitDMAInt(AHBDMA_Channel);
                fLib_DisableDMAChannel(AHBDMA_Channel);  

                if(memcmp(SrcAddr, DstAddr, Size) != 0)
                {
                    for(i = 0; i < (Size/4); i++)
                    {
                        if(DstAddr[i] != SrcAddr[i])
                        {
                            //fail("========>DstAddr[%0.8X] = %0.8X, should be %0.8X\n", i, DstAddr[i], SrcAddr[i]);
													fLib_printf("========>DstAddr[%0.8X] = %0.8X, should be %0.8X\n", i, DstAddr[i], SrcAddr[i]);
                            return FALSE;
                        }
                    }
                }
//              fLib_printf("--------------------\n");
                /////////////////////////////////////////////////////////////// 
                // clear destination, SRC+size(descrement) ==> DST(increment)
                TempAddr = DstAddr; DstAddr = SrcAddr; SrcAddr = TempAddr;
                memset(DstAddr, 0, Size);

                fLib_DMA_NormalMode(AHBDMA_Channel, (UINT32)(SrcAddr)+Size-1, (UINT32)DstAddr, Size, SrcWidth, DstWidth, SrcSize, 1, 0, 0, 0, 0);
        //      fLib_printf("Phase 3:Src=0x%x, Dst=0x%x\r\n",(SrcAddr+Size-1),DstAddr);
                fLib_EnableDMAChannel(AHBDMA_Channel);
                fLib_DMA_WaitDMAInt(AHBDMA_Channel);
                fLib_DisableDMAChannel(AHBDMA_Channel);  

                /////////////////////////////////////////////////////////////// 
                // clear destination, SRC+size(increment) ==> DST(descrement)
                TempAddr = DstAddr; DstAddr = SrcAddr; SrcAddr = TempAddr;
                memset(DstAddr, 0, Size);

                fLib_DMA_NormalMode(AHBDMA_Channel, (UINT32)SrcAddr, (UINT32)(DstAddr)+Size-1, Size, SrcWidth, DstWidth, SrcSize, 0, 1, 0, 0, 0);
        //      fLib_printf("Phase 4:Src=0x%x, Dst=0x%x\r\n",SrcAddr,(DstAddr+Size-1));
                fLib_EnableDMAChannel(AHBDMA_Channel);
                fLib_DMA_WaitDMAInt(AHBDMA_Channel);
                fLib_DisableDMAChannel(AHBDMA_Channel);  

                /////////////////////////////////////////////////////////////// 
                // clear destination, SRC+size(descrement) ==> DST(increment)
                TempAddr = DstAddr; DstAddr = SrcAddr; SrcAddr = TempAddr;
                memset(DstAddr, 0, Size);

                fLib_DMA_NormalMode(AHBDMA_Channel, (UINT32)(SrcAddr)+Size-1, (UINT32)DstAddr, Size, SrcWidth, DstWidth, SrcSize, 1, 0, 0, 0, 0);
        //      fLib_printf("Phase 5:Src=0x%x, Dst=0x%x\r\n",(SrcAddr+Size-1),DstAddr);
                fLib_EnableDMAChannel(AHBDMA_Channel);
                fLib_DMA_WaitDMAInt(AHBDMA_Channel);
                fLib_DisableDMAChannel(AHBDMA_Channel);  

                /////////////////////////////////////////////////////////////// 
                // clear destination, SRC+size(increment) ==> DST(decrement)
                TempAddr = DstAddr; DstAddr = SrcAddr; SrcAddr = TempAddr;
                memset(DstAddr, 0, Size);

                fLib_DMA_NormalMode(AHBDMA_Channel, (UINT32)SrcAddr, (UINT32)(DstAddr)+Size-1, Size, SrcWidth, DstWidth, SrcSize, 0, 1, 0, 0, 0);
        //      fLib_printf("Phase 6:Src=0x%x, Dst=0x%x\r\n",SrcAddr,(DstAddr+Size-1));
                fLib_EnableDMAChannel(AHBDMA_Channel);
                fLib_DMA_WaitDMAInt(AHBDMA_Channel);
                fLib_DisableDMAChannel(AHBDMA_Channel);  
                
                
                    for(i = 0; i < (Size/4); i++)
                    {   
                                    
                        if(DstAddr[i] != (UINT32)(DstAddr+i))
                        {
                            //fail("========>DstAddr[%0.8X] = %0.8X, should be %0.8X\n", i, (UINT32)DstAddr[i], (UINT32)DstAddr+i);
														fLib_printf("========>DstAddr[%0.8X] = %0.8X, should be %0.8X\n", i, (UINT32)DstAddr[i], (UINT32)DstAddr+i);
                            return FALSE;
                        }
                    }
            
                
                TempAddr = DstAddr; DstAddr = SrcAddr; SrcAddr = TempAddr;
                
            //  fLib_printf("DMA configure: SrcWidth=%d. DstWidth=%d, SrcSize=%d test pass...\n", 
            //      1 << (SrcWidth + 3), 1 << (DstWidth + 3), ((SrcSize == 0) ? 1 : 1 << (SrcSize+1)));             
            //  fLib_printf("OKOKOKOK!!!\n");
            }
        }
    }
    #endif
    return TRUE;
}
