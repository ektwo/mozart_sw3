/*
 * Kneron Post-Processing driver for KDP520
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */
 
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "base.h"
#include "kdpio.h"
#include "ipc.h"
#include "post_processing.h"
#include "dbg.h"

#define MAX(a, b)     (((a) > (b))? a : b)
#define MIN(a, b)     (((a) < (b))? a : b)

struct post_proc_func_s {
    int             model_id;
    post_proc_fn    ppf;
} post_proc_fns[MAX_MODEL_REGISTRATIONS];

#define YOLO_GRID_W             13
#define YOLO_GRID_H             13
#define YOLO_GRID_MAX           (YOLO_GRID_W * YOLO_GRID_H)
#define YOLO_CELL_BOX_NUM       5
#define YOLO_CLASS_MAX          80
#define YOLO_GOOD_BOX_MAX       180
#define YOLO_BOX_FIX_CH         5   /* x, y, w, h, confidence score */

#define IMAGENET_CLASSES_MAX    1000

#define KDP_COL_MIN             16 /* Bytes, i.e. 128 bits */

const float anchers[YOLO_CELL_BOX_NUM][2] = {{1.08, 1.19}, {3.42, 4.41}, {6.63, 11.38}, {9.42, 5.11}, {16.62, 10.52}};
const float prob_thresh = 0.5;     // probability threshold
const float nms_thresh = 0.25;       // non max suppression threshold

/* Shared global variable area among models */
struct yolo_post_globals {
    float box_class_probs[YOLO_CLASS_MAX];
    struct bounding_box bboxes[YOLO_GRID_MAX * YOLO_CELL_BOX_NUM];
    struct bounding_box result_tmp_s[YOLO_GOOD_BOX_MAX];
};

struct imagenet_post_globals {
    struct imagenet_result  temp[IMAGENET_CLASSES_MAX];
};

struct fd_post_globals {
    int feature_mat[20*20];
    float feature_pooled_mid[20*20];
    float feature_pooled[20*20];
    int feature[20*20];
};

struct lm_post_globals {
    int placeholder;
};

struct fr_post_globals {
    int placeholder;
};

union post_globals_u {
    struct yolo_post_globals        yolo;
    struct imagenet_post_globals    imgnet;
    struct fd_post_globals          fd;
    struct lm_post_globals          lm;
    struct fr_post_globals          fr;
} globals;

static float do_div_scale(float v, int div, float scale)
{
    return ((v / div) / scale);
}

static uint32_t round_up(uint32_t num){
  return ((num + (KDP_COL_MIN - 1)) & ~(KDP_COL_MIN - 1));
}

static float sigmoid(float x)
{
     float exp_value;
     float return_value;

     exp_value = expf(-x);

     return_value = 1 / (1 + exp_value);

     return return_value;
}

static void softmax(struct imagenet_result input[], int input_len)
{
    int i;
    float m;
    
    m = input[0].score;
    for (i = 1; i < input_len; i++) {
        if (input[i].score > m) {
            m = input[i].score;
        }
    }

    float sum = 0;
    for (i = 0; i < input_len; i++) {
        sum += expf(input[i].score - m);
    }

    for (i = 0; i < input_len; i++) {
        input[i].score = expf(input[i].score - m - log(sum));
    }    
}

static int float_comparator(float a, float b)
{
    float diff = a - b;

    if (diff < 0)
        return 1;
    else if (diff > 0)
        return -1;
    return
        0;
}

static int box_score_comparator(const void *pa, const void *pb)
{
    float a, b;

    a = ((struct bounding_box *)pa)->score;
    b = ((struct bounding_box *)pb)->score;

    return float_comparator(a, b);
}

static float overlap(float l1, float r1, float l2, float r2)
{
    float left = l1 > l2 ? l1 : l2;
    float right = r1 < r2 ? r1 : r2;
    return right - left;
}

static float box_intersection(struct bounding_box *a, struct bounding_box *b)
{
    float w, h, area;

    w = overlap(a->x1, a->x2, b->x1, b->x2);
    h = overlap(a->y1, a->y2, b->y1, b->y2);

    if (w < 0 || h < 0)
        return 0;

    area = w * h;
    return area;
}

static float box_union(struct bounding_box *a, struct bounding_box *b)
{
    float i, u;

    i = box_intersection(a, b);
    u = (a->y2 - a->y1) * (a->x2 - a->x1) + (b->y2 - b->y1) * (b->x2 - b->x1) - i;

    return u;
}

static float box_iou(struct bounding_box *a, struct bounding_box *b)
{
    float c;

    if (box_intersection(a, b) / box_intersection(a, a) > box_intersection(a, b) / box_intersection(b, b))
        c = box_intersection(a, b) / box_intersection(a, a);
    else
        c = box_intersection(a, b) / box_intersection(b, b);

    if (c < box_intersection(a, b) / box_union(a, b))
        c = box_intersection(a, b) / box_union(a, b);

    return c;
}

static int post_yolo(int model_id, struct kdp_image *image_p, void *params_p)
{
    struct yolo_post_globals *gp = &globals.yolo;
    int i, j, k, div, ch, row, col, max_score_class, good_box_count, class_good_box_count, good_result_count, len;
    float box_x, box_y, box_w, box_h, box_confidence, max_score, sum, scale;
    int8_t *src_p, *x_p, *y_p, *width_p, *height_p, *score_p, *class_p;
    struct bounding_box *bbox;
    struct yolo_result *result;
    struct bounding_box *result_box_p, *result_tmp_p, *r_tmp_p;
    int data_size, grid_w, grid_h, class_num, grid_w_len, cell_box_len;

    data_size = (image_p->output_format & BIT(0)) + 1;     /* 1 or 2 in bytes */

    src_p = (int8_t *)image_p->output_mem_addr;
    result = (struct yolo_result *)(image_p->result_mem_addr);
    result_box_p = result->boxes;

    result_tmp_p = gp->result_tmp_s;
    
    grid_w = (image_p->output_col <= YOLO_GRID_H) ? (image_p->output_col) : (YOLO_GRID_W);    
    grid_h = (image_p->output_row <= YOLO_GRID_H) ? (image_p->output_row) : (YOLO_GRID_H);
    class_num = image_p->output_channel / YOLO_CELL_BOX_NUM - YOLO_BOX_FIX_CH;

    grid_w_len = grid_w * data_size;
    len = round_up(grid_w_len);
    cell_box_len = (class_num + YOLO_BOX_FIX_CH) * len;

    result->class_count = class_num;
    bbox = gp->bboxes;
    good_box_count = 0;
    div = 1 << image_p->output_radix;
    scale = *(float *)&image_p->output_scale;

    for (row = 0; row < grid_h; row++) {
        for (col = 0; col < grid_w; col++) {
            x_p = src_p;
            y_p = x_p + len;
            width_p = y_p + len;
            height_p = width_p + len;
            score_p = height_p + len;
            class_p = score_p + len;

            for (ch = 0; ch < YOLO_CELL_BOX_NUM; ch++) {

                if (data_size == 1) {
                    box_x = (float)*x_p;
                    box_y = (float)*y_p;
                    box_w = (float)*width_p;
                    box_h = (float)*height_p;
                    box_confidence = (float)*score_p;
                } else {
                    box_x = (float)*(int16_t *)x_p;
                    box_y = (float)*(int16_t *)y_p;
                    box_w = (float)*(int16_t *)width_p;
                    box_h = (float)*(int16_t *)height_p;
                    box_confidence = (float)*(int16_t *)score_p;
                }

                x_p += cell_box_len;
                y_p += cell_box_len;
                width_p += cell_box_len;
                height_p += cell_box_len;
                score_p += cell_box_len;
                
                box_confidence = sigmoid(do_div_scale(box_confidence, div, scale));

                /* Get scores of all class */
                sum = 0;
                for (i = 0; i < class_num; i++) {
                    if (data_size == 1)
                        gp->box_class_probs[i] = (float)*(class_p + i * len);
                    else
                        gp->box_class_probs[i] = (float)*(uint16_t *)(class_p + i * len );

                    gp->box_class_probs[i] = expf(do_div_scale(gp->box_class_probs[i], div, scale));
                    sum += gp->box_class_probs[i];
                }
                class_p += cell_box_len;

                /* Find highest score class */
                max_score = 0;
                max_score_class = -1;
                for (i = 0; i < class_num; i++) {
                    gp->box_class_probs[i] = gp->box_class_probs[i] / sum * box_confidence;
                    if (gp->box_class_probs[i] >= prob_thresh && gp->box_class_probs[i] > max_score) {
                        max_score = gp->box_class_probs[i];
                        max_score_class = i;
                    }
                }
                
                if (max_score_class != -1) {
                    box_x = do_div_scale(box_x, div, scale);
                    box_y = do_div_scale(box_y, div, scale);
                    box_w = do_div_scale(box_w, div, scale);
                    box_h = do_div_scale(box_h, div, scale);

                    box_x = (sigmoid(box_x) + col) / grid_w;
                    box_y = (sigmoid(box_y) + row) / grid_h;
                    box_w = expf(box_w) * anchers[ch][0] / grid_w;
                    box_h = expf(box_h) * anchers[ch][1] / grid_h; 

                    bbox->x1 = (box_x - (box_w / 2)) * image_p->input_col;
                    bbox->y1 = (box_y - (box_h / 2)) * image_p->input_row;
                    bbox->x2 = (box_x + (box_w / 2)) * image_p->input_col;
                    bbox->y2 = (box_y + (box_h / 2)) * image_p->input_row;
                    bbox->score = box_confidence;
                    bbox->class_num = max_score_class;

                    bbox++;
                    good_box_count++;              
                }
            }

            /* Go to next col */
            src_p += data_size;
        }

        /* Go to begining of next row */
        src_p += cell_box_len * YOLO_CELL_BOX_NUM - grid_w_len;
    }

    good_result_count = 0;

    for (i = 0; i < class_num; i++) {
        bbox = gp->bboxes;
        class_good_box_count = 0;
        r_tmp_p = result_tmp_p;

        for (j = 0; j < good_box_count; j++) {
            if (bbox->class_num == i) {
                memcpy(r_tmp_p, bbox, sizeof(struct bounding_box));
                r_tmp_p++;
                class_good_box_count++;
            }
            bbox++;
        }
        
        if (class_good_box_count == 1) {
            memcpy(result_box_p, &result_tmp_p[0], sizeof(struct bounding_box));
            result_box_p++;
            good_result_count++;
        } else if (class_good_box_count >= 2) {
            qsort(result_tmp_p, class_good_box_count, sizeof(struct bounding_box), box_score_comparator);
            for (j = 0; j < class_good_box_count; j++) {
                if (result_tmp_p[j].score == 0)
                    continue;
                for (k = j + 1; k < class_good_box_count; k++) {
                    if (box_iou(&result_tmp_p[j], &result_tmp_p[k]) > nms_thresh) {
                        result_tmp_p[k].score = 0;
                    }
                }
            }

            for (j = 0; j < class_good_box_count; j++) {
                if (result_tmp_p[j].score > 0) {
                    memcpy(result_box_p, &result_tmp_p[j], sizeof(struct bounding_box));
                    result_box_p++;
                    good_result_count++;
                }
            }
        }
    }
    
    dbg_msg("[class %d]: good_box_count: %d, good_result_count: %d (radix %d, scale %f)\n", 
        class_num, good_box_count, good_result_count, image_p->output_radix, scale);

    result->box_count = good_result_count;
    len = good_result_count * sizeof(struct bounding_box);

    return len;
}

static int inet_comparator(const void *pa, const void *pb)
{
    float a, b;

    a = ((struct imagenet_result *)pa)->score;
    b = ((struct imagenet_result *)pb)->score;

    return float_comparator(a, b);
}

static int post_imagenet_classification(int model_id, struct kdp_image *image_p, void *params_p)
{
    struct imagenet_post_globals *gp = &globals.imgnet;
    int8_t *cnn_out;
    int16_t *cnn_out2;
    uint8_t *result_p, *tmp;
    int i, len, data_size, div;
    float scale;
    
    data_size = (image_p->output_format & BIT(0)) + 1;     /* 1 or 2 in bytes */

    cnn_out = (int8_t *)image_p->output_mem_addr;
    int8_t *src_p = (int8_t *)image_p->output_mem_addr;    
    int grid_w = image_p->output_col;
    int grid_h = image_p->output_row;
    len = grid_w * data_size;
    int grid_w_bytes_aligned = round_up(len);    
    int w_bytes_to_skip = grid_w_bytes_aligned - len;
    len = grid_w_bytes_aligned;

    int ch = image_p->output_channel;
    
    /* Convert to float */
    tmp = src_p;
    scale = *(float *)&image_p->output_scale;
    div = 1 << image_p->output_radix;
    for (i = 0; i < ch; i++) {
        gp->temp[i].score = (float)*tmp;
        gp->temp[i].score = do_div_scale(gp->temp[i].score, div, scale);
        tmp += data_size + w_bytes_to_skip;
    }
    
    softmax(gp->temp, len);
    qsort(gp->temp, len, sizeof(struct imagenet_result), inet_comparator);

    result_p = (uint8_t *)(image_p->result_mem_addr);
    len = sizeof(struct imagenet_result) * IMAGENET_TOP_MAX;
    memcpy(result_p, gp->temp, len);
    
    return len;
}

static int post_face_detection(int model_id, struct kdp_image *image_p, void *params_p)
{
    struct fd_post_globals *gp = &globals.fd;
    struct facedet_result *result_p = (struct facedet_result *)(image_p->result_mem_addr);

    int scale = 8; // hard coded scale
    float feature_poll_thresh = 45.;
    int data_size = (image_p->output_format & BIT(0)) + 1;     /* 1 or 2 in bytes */

    int8_t *src_p = (int8_t *)image_p->output_mem_addr;    
    int grid_w = image_p->output_col;
    int grid_h = image_p->output_row;
    int len = grid_w * data_size;
    int grid_w_bytes_aligned = round_up(len);    
    int w_bytes_to_skip = grid_w_bytes_aligned - len;
    len = grid_w_bytes_aligned;

    //get feature map from model
    float value;
    int8_t *tmp = src_p;
    int count = 0;
    memset(gp->feature_mat, 0, sizeof(gp->feature_mat));
    for (int row=0; row < grid_h; row++) {
        for (int col=0; col < grid_w; col++) {
            //tmp_value = (float)*tmp;
            value=(float)*tmp;
            gp->feature_mat[row*grid_w+col] = (value>0) ? 1 : 0;
            tmp+=data_size;
        }
        tmp+=w_bytes_to_skip;
    } 

    //average pool
    memset(gp->feature_pooled_mid, 0, sizeof(gp->feature_pooled_mid));
    for (int row=0; row < grid_h; row++) {
        for (int col=0; col < grid_w; col++) {
            gp->feature_pooled_mid[row*grid_w+col] = 0;
            for (int i=-1; i<2; i++) {
                for (int j=-1; j<2; j++) {
                    if (((row+i)>0 && (row+i)<grid_h) && ((col+j)>0 && (col+j)<grid_w)) {
                        gp->feature_pooled_mid[row*grid_w+col] += gp->feature_mat[(row+i)*grid_w+col+j];
                    }
                }
            }
        }
    }

    memset(gp->feature_pooled, 0, sizeof(gp->feature_pooled));
    for (int row=0; row < grid_h; row++) {
        for (int col=0; col < grid_w; col++) {
            gp->feature_pooled[row*grid_w+col] = 0;
            for (int i=-1; i<2; i++) {
                for (int j=-1; j<2; j++) {
                    if (((row+i)>0 && (row+i)<grid_h) && ((col+j)>0 && (col+j)<grid_w)) {
                        gp->feature_pooled[row*grid_w+col] += gp->feature_pooled_mid[(row+i)*grid_w+col+j];
                    }
                }
            }
        }
    }

    float max = gp->feature_pooled[0];
    int tmp0 = 0;
    int tmp1 = 0;
    for (int row=0; row < grid_h; row++) {
        for (int col=0; col < grid_w; col++) {
            if (max < gp->feature_pooled[row*grid_w+col]) {
                max = gp->feature_pooled[row*grid_w+col];
                tmp0=col;
                tmp1=row;
            }
        }
    }

    int rc[4] = {-1, -1, -1, -1};

    if (max < feature_poll_thresh) {
        result_p->len = 0;
        for(int j=0; j<4; j++)
            result_p->xywh[j] = rc[j];
        result_p->len =4;
        return 0;
    }

    count = 0;
    memset(gp->feature, 0, sizeof(gp->feature));
    for (int row=0; row < grid_h; row++) {
        for (int col=0; col < grid_w; col++) {
            gp->feature[count]= gp->feature_pooled[row*grid_w+col] > feature_poll_thresh ? 1: 0;
            count++;
        }
    }   

    int left = grid_w, right = 0;
    int top = grid_h, bot = 0;
    
    int num = 0;
    for (int j = 0; j < grid_h; j++) {
        for (int i = 0; i < grid_w; i++) {
            if (gp->feature[j * grid_w + i] > 0.0) {
                num++;
                left = MIN(left, i);    
                top = MIN(top, j);
                right = MAX(right, i);
                bot = MAX(bot, j);
            } 
        }
    }

    if (num <= grid_w * grid_h / 3) {
        left = tmp0;
        top = tmp1;
        right = tmp0;
        bot = tmp1;

        int c_y = (bot + top) / 2;
        int c_x = (right + left) / 2;

        //// note: change to C style
        while (left > 0) {
            if (gp->feature[c_y * grid_w + left-1] > 0) {
                left--;
                continue;
            }
            if (gp->feature[(c_y-1) * grid_w + left-1] > 0 && c_y > 0) {
                c_y--;
                left--;
                continue;
            }
            if (gp->feature[(c_y+1) * grid_w + left-1] > 0 && c_y < grid_h-1) {
                c_y++;
                left--;
                continue;
            }
            break;
        }

        while(right < grid_w-1) {
            if(gp->feature[c_y * grid_w + right+1] > 0) {
                right++;
                continue;
            }
            if(gp->feature[(c_y+1) * grid_w + right+1] > 0 && c_y < grid_h-1) {
                right++; 
                c_y++;
                continue;
            }
            if(gp->feature[(c_y-1) * grid_w + right+1] > 0 && c_y > 0) {
                right++;
                c_y--;
                continue;
            }
            break;
        }

        while(top > 0){
            if(gp->feature[(top-1) * grid_w + c_x] > 0){
                top--;
                continue;
            }

            if(gp->feature[(top-1) * grid_w + c_x-1] > 0 && c_x > 0){
                top--; 
                c_x--;
                continue;
            }

            if(gp->feature[(top-1) * grid_w + c_x+1] > 0 && c_x < grid_w-1){
                top--;
                c_x++;
                continue;
            }
            break;
        }

        while(bot < grid_h-1){
            if(gp->feature[(bot+1) * grid_w + c_x] > 0){
                bot++;
                continue;
            }

            if(gp->feature[(bot+1) * grid_w + c_x+1] > 0 && c_x< grid_w-1){
                c_x++;
                bot++;
                continue;
            }

            if(gp->feature[(bot+1) * grid_w + c_x-1] > 0 && c_x>0){
                c_x--;
                bot++;
                continue;
            }
            break;
        }
    }
   
    // center of face on feature map;
    float c_y = (float)(bot + top) / 2 + (float)0.5;
    float c_x = (float)(right + left) / 2 + (float)0.5;

    // center of face on input map;
    c_y = c_y * scale;
    c_x = c_x * scale;
    
    // adjust rectangle size
    float length = (float)(bot - top + 1);
    float flength = (float)MAX(length, (float)3.5);
    float fwidth = (float)flength * (float)0.8 * (float)scale;
    float fheight = (float)flength * (float)scale;
    float scale_init;

    if (image_p->raw_img_p->input_col > image_p->raw_img_p->input_row) {
        scale_init = (float)image_p->input_col / (float)image_p->raw_img_p->input_col;
    } else {
        scale_init = (float)image_p->input_row / (float)image_p->raw_img_p->input_row;
    }

    rc[0] = (int) (round((c_x - fwidth / (float)2.) / scale_init));
    rc[1] = (int) (round((c_y - fheight / (float)2.) / scale_init));
    rc[2] = (int) (round((c_x + fwidth / (float)2.) / scale_init)) - rc[0];
    rc[3] = (int) (round((c_y + fheight / (float)2.) / scale_init)) - rc[1];

    int rotation = (image_p->raw_img_p->format >> IMAGE_FORMAT_ROT_SHIFT) & 0x03;
    if (rotation) {
        /* support padding mode 1 only. TODO: support padding mode 0 */

        // Rotate bounding box if the image was rotated
        if (rotation == IMAGE_FORMAT_ROT_CLOCKWISE) {
            //swap width and height
            int temp_width = rc[2];
            rc[2] = rc[3];
            rc[3] = temp_width;
            //swap x and y
            int temp_x = rc[0];
            rc[0] = rc[1];
            rc[1] = image_p->raw_img_p->input_row - image_p->raw_img_p->pad_bottom - temp_x - rc[3];
        } else if (rotation == IMAGE_FORMAT_ROT_COUNTER_CLOCKWISE) {
            //swap width and height
            int temp_width = rc[2];
            rc[2] = rc[3];
            rc[3] = temp_width;
            //swap x and y
            int temp_x = rc[0];
            rc[0] = image_p->raw_img_p->input_col - image_p->raw_img_p->pad_right - rc[1] - rc[2];
            rc[1] = temp_x;
        }
    }

    for(int j=0; j<4; j++)
         result_p->xywh[j] = rc[j];    
    result_p->len =4;

    dbg_msg("post-FD: rc[0]=%d, rc[1]=%d, rc[2]=%d, rc[3]=%d\n", rc[0], rc[1], rc[2], rc[3]);
    return 0;
}

static int post_face_landmark_onet_5p(int model_id, struct kdp_image *image_p, void *params_p)
{
    struct landmark_result *result_p = (struct landmark_result *)(image_p->result_mem_addr);
    int origin_row, origin_col;
    float scale;
    int crop_top, crop_bottom, crop_left, crop_right;
    int i, data_size, div, grid_w, row, col, len, grid_w_bytes_aligned, w_bytes_to_skip, temp_x, temp_y;
    float landmarks[10];

    int8_t *src_p = (int8_t *)image_p->output_mem_addr;

    //setup output buffer and data format setting
    data_size = (image_p->output_format & BIT(0)) + 1;     /* 1 or 2 in bytes */
    div = 1 << image_p->output_radix;
    grid_w = image_p->output_col;  //col = 1
    row = image_p->input_row;
    col = image_p->input_col;
    len = grid_w * data_size;
    grid_w_bytes_aligned = round_up(len);
    w_bytes_to_skip = grid_w_bytes_aligned - len; //waste = 15
    len = grid_w_bytes_aligned;

    crop_top = image_p->raw_img_p->crop_top;
    crop_bottom = image_p->raw_img_p->crop_bottom;
    crop_left = image_p->raw_img_p->crop_left;
    crop_right = image_p->raw_img_p->crop_right;
    origin_row = image_p->raw_img_p->input_row - (crop_top + crop_bottom);
    origin_col = image_p->raw_img_p->input_col - (crop_left + crop_right);

    scale = *(float *)&image_p->output_scale;

    int8_t *tmp_p = src_p;
    for(i = 0; i < 10; i++) {
        landmarks[i] = (float)*tmp_p;
        landmarks[i] = do_div_scale(landmarks[i], div, scale);
        tmp_p += data_size + w_bytes_to_skip;
    }

    // onet 48 post output processing.
    float scale_w = (float)MAX(origin_row, origin_col) / col;
    float scale_h = (float)MAX(origin_row, origin_col) / row;

    for(i = 0; i < 10; i++) {
        landmarks[i] = landmarks[i] + (float)col / 2;
    }

    for(i = 0; i < 10; i += 2) {
        landmarks[i] = (int)(round(landmarks[i] * scale_w));
        landmarks[i+1] = (int)(round(landmarks[i+1] * scale_h));
    }

    int rotation = (image_p->raw_img_p->format >> IMAGE_FORMAT_ROT_SHIFT) & 0x03;
    if (rotation) {
        /* support padding mode 1 only. TODO: support padding mode 0 */

        if (rotation == IMAGE_FORMAT_ROT_CLOCKWISE) {
            for (i=0; i<LAND_MARK_POINTS*2; i=i+2) {
                //swap x and y
                temp_x = (int)landmarks[i];
                landmarks[i] = landmarks[i+1];
                landmarks[i+1] = image_p->raw_img_p->input_row - image_p->raw_img_p->crop_bottom - image_p->raw_img_p->pad_bottom - temp_x;
            }
        } else if (rotation == IMAGE_FORMAT_ROT_COUNTER_CLOCKWISE) {
            for (i=0; i<LAND_MARK_POINTS*2; i=i+2) {
                //swap x and y
                temp_y = (int)landmarks[i];
                temp_x = image_p->raw_img_p->input_col - image_p->raw_img_p->crop_right - image_p->raw_img_p->pad_right - (int)landmarks[i+1];
                landmarks[i] = temp_x;
                landmarks[i+1] = temp_y + image_p->raw_img_p->crop_top;
            }
        }
    } else {
        for (i=0; i<LAND_MARK_POINTS*2; i=i+2) {
            landmarks[i] += image_p->raw_img_p->crop_left;
            landmarks[i+1] += image_p->raw_img_p->crop_top;
        }
    }
        
    dbg_msg("Post landmarks:\n");
    for(i = 0; i< 10; i++)
    {
        dbg_msg("landmarks[%d]=%f\n",i,landmarks[i]);
    }

    for(i=0; i<LAND_MARK_POINTS; i++) {
        result_p->marks[i].x = (int32_t)landmarks[i*2];
        result_p->marks[i].y = (int32_t)landmarks[i*2+1];
    }
    return 0;
}

 //Normalize embedding
 static void NormalizeEmbedding(float *x, int emb_size)
 {
     float sum_x = 0.;

     int i = 0;
     for(i = 0; i < emb_size; i++) {
         sum_x += powf(x[i], 2);
     }
     sum_x = sqrtf(sum_x);

     for(i = 0; i < emb_size; i++) {
         x[i] /= sum_x;
     }
 }

static int post_face_recognition(int model_id, struct kdp_image *image_p, void *params_p)
{
    struct fr_result *result_p = (struct fr_result *)(image_p->result_mem_addr);

    int8_t *src_p = (int8_t *)image_p->output_mem_addr;

    int data_size = (image_p->output_format & BIT(0)) + 1;     /* 1 or 2 in bytes */
    int col = image_p->output_col;
    int row = image_p->output_row;
    int ch = image_p->output_channel;
    int len = col * row * ch;
    int div = 1 << image_p->output_radix;
    float scale = *(float *)&image_p->output_scale;
    int i;

    data_size = round_up(data_size);    // aligned
    for(i=0; i < len; i++) {
        result_p->feature_map[i] = do_div_scale((float)*src_p, div, scale);
        src_p += data_size;
    }

    NormalizeEmbedding(result_p->feature_map, len);
    dbg_msg("FR feature map:\n");
    for(i=0; i<len; i++)
    {
      dbg_msg("%f\n",result_p->feature_map[i]);
    }

    return 0;
}

int post_processing(struct kdp_image *image_p, int model_id, void *params_p)
{
    int i, len;
    post_proc_fn ppf = NULL;

    if (image_p->raw_img_p->format & IMAGE_FORMAT_BYPASS_POST) {
        dbg_msg("post_processing: Bypass\n");
        return RET_NO_ERROR;
    }

    for (i = 0; i < MAX_MODEL_REGISTRATIONS; i++) {
        if (post_proc_fns[i].model_id == model_id) {
            ppf = post_proc_fns[i].ppf;
            break;
        }
    }
    
    if (ppf) {
        len = ppf(model_id, image_p, params_p);
    } else {
        dbg_msg("TODO: add support for model_id %d\n", model_id);
        len = 0;
    }
    
    return len;
}

void post_processing_init()
{
#if !defined(CODE_SIZE_LIMITED)
    /* Move any line out to test it */
    kdpio_post_processing_register(TINY_YOLO_VOC, post_yolo);
    kdpio_post_processing_register(KNERON_TINY_YOLO_PERSON, post_yolo);
    kdpio_post_processing_register(IMAGENET_CLASSIFICATION_RES50, post_imagenet_classification);
    kdpio_post_processing_register(IMAGENET_CLASSIFICATION_MOBILENET_V2, post_imagenet_classification);
#endif
    kdpio_post_processing_register(KNERON_FDSMALLBOX, post_face_detection);
    kdpio_post_processing_register(KNERON_LM_5PTS, post_face_landmark_onet_5p);
    kdpio_post_processing_register(KNERON_FR_VGG10, post_face_recognition);
}

/* Registration APIs */
int kdpio_post_processing_register(int model_id, post_proc_fn ppf)
{
    int i, rc;

    rc = -1;
    if (ppf == NULL)
        return rc;

    for (i = 0; i < MAX_MODEL_REGISTRATIONS; i++) {
        if (post_proc_fns[i].ppf == NULL) {
            post_proc_fns[i].ppf = ppf;
            post_proc_fns[i].model_id = model_id;
            rc = 0;
            break;
        }
    }

    return rc;
}

void kdpio_post_processing_unregister(int model_id, post_proc_fn ppf)
{
    int i;

    if (model_id == 0 || ppf == NULL)
        return;

    for (i = 0; i < MAX_MODEL_REGISTRATIONS; i++) {
        if (post_proc_fns[i].model_id == model_id && post_proc_fns[i].ppf == ppf) {
            post_proc_fns[i].ppf = NULL;
            post_proc_fns[i].model_id = 0;
            return;
        }
    }
}
