/*
 * Kneron Pre-Processing driver for KDP520
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
 
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "base.h"
#include "kdpio.h"
#include "ipc.h"
#include "npu.h"
#include "pre_processing.h"
#include "dbg.h"

struct pre_proc_func_s {
    int             model_id;
    pre_proc_fn     ppf;
} pre_proc_fns[MAX_MODEL_REGISTRATIONS];

extern uint32_t inproc_sm[];
extern uint32_t inproc_lg[];

int inproc_size_sm(void);
int inproc_size_lg(void);

#define SCU_EXTREG_PA_BASE          0xC2380000
#define NPU_RESET_REG_ADDR(base)    (base + 0x004C)
#define NPU_RESET_BIT               BIT(2)

int kdp_preproc_fr(int model_id, struct kdp_image *image_p, void *params_p);

static void npu_reset(void)
{
    uint32_t reg, value;

    reg = NPU_RESET_REG_ADDR(SCU_EXTREG_PA_BASE);
    value = inw(reg);
    outw(reg, value & ~NPU_RESET_BIT);
    outw(reg, value | NPU_RESET_BIT);
}

static void alter_inproc_data(int addr, int value)
{
    int data = inw(addr);
    data &= 0xFFFF0000;
    data |= (value & 0x0000FFFF);
    outw(addr, data);
}

static inline int pad_to_4(int value)
{
    return (value % 4) ? (((value / 4) + 1) * 4): value;
}

static void calculate_padding(int in_row, int in_col, int out_row, int out_col, int pad_mode,
                              int* pad_top, int* pad_bottom, int* pad_left, int* pad_right)
{
    if (pad_mode == 0) {
        // pad on both side
        if (in_row >= in_col) {
            *pad_top = 0;
            *pad_bottom = 0;
            *pad_left = (out_row - ((int)(((float)out_row / in_row) * in_col) / 2) * 2) / 2;
            *pad_right = *pad_left;
        } else {
            *pad_top = (out_col - ((int)(((float)out_col / in_col) * in_row) / 2) * 2) / 2;
            *pad_bottom = *pad_top;
            *pad_left = 0;
            *pad_right = 0;
        }
    } else if (pad_mode == 1) {
        // only pad right and bottom
        if (in_row >= in_col) {
            *pad_top = 0;
            *pad_bottom = 0;
            *pad_left = 0;
            *pad_right = (int)(out_row -  (((float)out_row / in_row)* in_col));
        } else {
            *pad_top = 0;
            *pad_bottom = (int)(out_col - (((float)out_col / in_col) * in_row));
            *pad_left = 0;
            *pad_right = 0;
        }
    }
}

uint32_t inproc_base_addr;

static int kdp_preproc_sm(int model_id, struct kdp_image *image_p, void *params_p)
{
    int in_row, in_col, out_row, out_col;
    int crop_top, crop_bottom, crop_left, crop_right;
    int start_row;
    uint32_t src_img_mode;
    int16_t sub;

    int pixel_size;
    int pad_top, pad_bottom, pad_left, pad_right;
    int valid_in_row;
    int valid_in_col;
    int valid_out_row;
    int valid_out_col;
    int img_str_row;
    int offset, sa, pitch, len, line, rdma_hw, ceil_l, pad, resize_hw, ratio_w, ratio_h, da, wpitch, wlen, wline, wdma_hw;
    int resize_offset;

    in_row = image_p->raw_img_p->input_row;
    in_col = image_p->raw_img_p->input_col;
    out_row = image_p->input_row;
    out_col = image_p->input_col;

    src_img_mode = image_p->raw_img_p->format;
    pixel_size = kdp_read_format_size(src_img_mode);

    if (src_img_mode & (uint32_t)IMAGE_FORMAT_SUB128)
        sub = 128;
    else
        sub = 0;

    crop_top = image_p->raw_img_p->crop_top;
    crop_bottom = image_p->raw_img_p->crop_bottom;
    crop_left = image_p->raw_img_p->crop_left;
    crop_right = image_p->raw_img_p->crop_right;
    start_row = 0;

    dbg_msg("inproc: crop: top/bottom/left/right: %d %d %d %d\n",
            crop_top, crop_bottom, crop_left, crop_right);

    if (model_id != KNERON_FDSMALLBOX) {
        crop_left &= ~0x03;  // align by 4-
        crop_right &= ~0x03;  // align by 4-

        dbg_msg("inproc: crop: aligned left/right: %d %d (pad=sub=%d)\n", crop_left, crop_right, sub);

        image_p->raw_img_p->crop_left = crop_left;
        image_p->raw_img_p->crop_right = crop_right;
    }

    // calculate padding
    valid_in_row = in_row - (crop_top + crop_bottom);
    valid_in_col = in_col - (crop_left + crop_right);

    calculate_padding(valid_in_row, valid_in_col, out_row, out_col, 1, &pad_top, &pad_bottom, &pad_left, &pad_right);

    dbg_msg("inproc: pad: top/bottom/left/right: %d %d %d %d\n",
            pad_top, pad_bottom, pad_left, pad_right);

    valid_out_row = out_row - (pad_top + pad_bottom);
    valid_out_col = out_col - (pad_left + pad_right);

    // set NPU_RDMA0_SRC0_sa (start address of handle cropped image)
    img_str_row = (start_row - pad_top) > 0 ? (start_row - pad_top) : 0;
    img_str_row = valid_in_row * img_str_row / valid_out_row;
    offset = ((img_str_row + crop_top) * in_col + crop_left) * pixel_size;
    sa = image_p->raw_img_p->image_mem_addr + (offset & ~0x03);     // 4 bytes align
    da = image_p->input_mem_addr;

    dbg_msg("inproc: sa=0x%x, da=0x%x, offset %d\n", sa, da, offset);

    alter_inproc_data(inproc_base_addr + 0x00, sa);
    alter_inproc_data(inproc_base_addr + 0x04, (sa >> 16));

    // set NPU_RDMA0_SRC1_pitch
    pitch = in_col * pixel_size;
    alter_inproc_data(inproc_base_addr + 0x08, pitch);

    // set NPU_RDMA0_SRC2_len
    len = valid_in_col * pixel_size;
    alter_inproc_data(inproc_base_addr + 0x10, len);

    // set NPU_RDMA0_BLK_line
    line = valid_in_row - 1;
    alter_inproc_data(inproc_base_addr + 0x18, line);

    // set NPU_NMEM_RDMA1_h / NPU_NMEM_RDMA1_w
    rdma_hw = ((out_row << 16) & 0xFFFF0000) + 4;  // h=b31:16, w=b11:0
    alter_inproc_data(inproc_base_addr + 0x24, rdma_hw);
    alter_inproc_data(inproc_base_addr + 0x28, (rdma_hw >> 16));

    // set NPU_NMEM_RDMA2_l
    ceil_l = out_col / (4 * 4);
    if (out_col % (4 * 4))
        ceil_l++;
    alter_inproc_data(inproc_base_addr + 0x2C, ceil_l);

    // set NPU_FORMAT_mode
    alter_inproc_data(inproc_base_addr + 0x30, src_img_mode & IMAGE_FORMAT_NPU);

    // set NPU_PREP0_pad (pad=b15:0): pad same value as sub
    alter_inproc_data(inproc_base_addr + 0x38, (int16_t)(sub<<8|sub));
    // set NPU_PREP0_sub (sub=b31:16)
    alter_inproc_data(inproc_base_addr + 0x3C, sub);

    // set NPU_PREP1_pad_r / NPU_PREP1_pad_l / NPU_PREP1_pad_b / NPU_PREP1_pad_t
    pad = ((pad_right << 24) & 0xFF000000) + ((pad_left << 16) & 0x00FF0000) +
              ((pad_bottom << 8) & 0x0000FF00) + (pad_top & 0x000000FF); // r=b31:24, l=b23:16, b=b15:8, t=b7:0
    alter_inproc_data(inproc_base_addr + 0x40, pad);
    alter_inproc_data(inproc_base_addr + 0x44, (pad >> 16));

    // set NPU_RESIZE_SRC_offset
    resize_offset = offset & 0x03;
    alter_inproc_data(inproc_base_addr + 0x54, resize_offset);

    // set NPU_RESIZE_DST_h / NPU_RESIZE_DST_w
    resize_hw = ((valid_out_row << 16) & 0x01FF0000) + (valid_out_col & 0x000001FF);  // h=b24:16, w=b8:0
    alter_inproc_data(inproc_base_addr + 0x58, resize_hw);
    alter_inproc_data(inproc_base_addr + 0x5C, (resize_hw >> 16));

    // set NPU_RESIZE_RATIO0_w / NPU_RESIZE_RATIO0_h (scaling ratio)
    ratio_w = 4096 * (pad_to_4(valid_in_col) - 1) / (valid_out_col - 1);
    ratio_h = 4096 * (valid_in_row - 1) / (valid_out_row - 1);
    alter_inproc_data(inproc_base_addr + 0x60, ratio_w);
    alter_inproc_data(inproc_base_addr + 0x64, (ratio_w >> 16));
    alter_inproc_data(inproc_base_addr + 0x68, ratio_h);
    alter_inproc_data(inproc_base_addr + 0x6C, (ratio_h >> 16));

    // set NPU_WDMA0_DST0_da
    alter_inproc_data(inproc_base_addr + 0x7C, da);
    alter_inproc_data(inproc_base_addr + 0x80, (da >> 16));

    // set NPU_WDMA0_DST1_pitch
    wpitch = ceil_l * 4 * 16;
    alter_inproc_data(inproc_base_addr + 0x84, wpitch);
    alter_inproc_data(inproc_base_addr + 0x88, (wpitch >> 16));

    // set NPU_WDMA0_DST2_len
    wlen = wpitch;
    alter_inproc_data(inproc_base_addr + 0x8C, wlen);

    // set NPU_WDMA0_BLK_line
    wline = out_row - 1;
    alter_inproc_data(inproc_base_addr + 0x90, wline);

    // set NPU_NMEM_WDMA1_h / NPU_NMEM_WDMA1_w
    wdma_hw = ((out_row << 16) & 0x0FFF0000) + 4;   // h=b27:16, w=b11:0
    alter_inproc_data(inproc_base_addr + 0x9C, wdma_hw);
    alter_inproc_data(inproc_base_addr + 0xA0, (wdma_hw >> 16));

    // set NPU_NMEM_WDMA2_l
    alter_inproc_data(inproc_base_addr + 0xA4, ceil_l);

    /* Pass pad parameters to others */
    image_p->raw_img_p->pad_top = pad_top;
    image_p->raw_img_p->pad_bottom = pad_bottom;
    image_p->raw_img_p->pad_left = pad_left;
    image_p->raw_img_p->pad_right = pad_right;

    return 0;
}

static void pre_proc_rotate(int8_t *src_p, int8_t *dst_p, int row, int col, int rot_dir)
{
    int unit = 4;
    unsigned int r, c;

    for (r = 0; r < row; r++) {
        for (c = 0; c < col; c++) {
            for(int u = 0; u < unit; u++) {
                if (rot_dir == IMAGE_FORMAT_ROT_CLOCKWISE) {
                    *(dst_p + unit * (c * row + (row - r - 1))+u) =
                                *(src_p + unit * (r * col + c)+u);
                } else if (rot_dir == IMAGE_FORMAT_ROT_COUNTER_CLOCKWISE) {
                    *(dst_p + unit * (row * (col - c - 1) + r)+u) =
                                *(src_p + unit * (r * col + c)+u);
                }
            }
        }
    }

    return;
}

static int kdp_preproc_inproc(int model_id, struct kdp_image *image_p, void *params_p)
{
    int in_row, in_col, out_row, out_col, rc;
    pre_proc_fn ppf;
    int inproc_len, src_format, rotation;
    int8_t *src_p, *dst_p;

    out_row = image_p->input_row;
    out_col = image_p->input_col;

    src_format = image_p->raw_img_p->format;
    rotation = (src_format >> IMAGE_FORMAT_ROT_SHIFT) & 0x03;
    if (rotation) {
        // need rotation: use secondary buffer first, and save regular buffer for npu
        src_p = (int8_t *)image_p->input_mem_addr2;
        dst_p = (int8_t *)image_p->input_mem_addr;
        image_p->input_mem_addr = image_p->input_mem_addr2;
    }

    in_row = image_p->raw_img_p->input_row;
    in_col = image_p->raw_img_p->input_col;

    if (out_row < 256 && out_col < 256) {
        inproc_len = inproc_size_sm();
        inproc_base_addr = (uint32_t)inproc_sm;
        ppf = kdp_preproc_sm;
    } else {
        inproc_len = inproc_size_lg();
        inproc_base_addr = (uint32_t)inproc_lg;
        ppf = kdp_preproc_sm;
    }

    dbg_msg("[Model %d] pre-proc: in[%d, %d], out[%d, %d], inproc[0x%.8x, %d]\n", model_id, in_row, in_col, out_row, out_col, inproc_base_addr, inproc_len);

    if (ppf)
        /* Prepare inproc code */
        ppf(model_id, image_p, params_p);

    /* Use polling mode and wait for npu done */
    kdp_disable_npu_int();
    kdp_enable_npu_preproc(inproc_base_addr, inproc_len);
    rc = kdp_wait_for_npu_done(1000);
    npu_reset();        // HACK until root cause found
    if (rotation) {
        pre_proc_rotate(src_p, dst_p, out_row, out_col, rotation);
        /* Restore the buffer/date for next step */
        image_p->input_mem_addr = (uint32_t)dst_p;
    }
    kdp_enable_npu_int();

    return rc;
}

int pre_processing(struct kdp_image *image_p, int model_id, void *params_p)
{
    int i, rc;
    pre_proc_fn ppf = NULL;

    if (image_p->raw_img_p->format & IMAGE_FORMAT_BYPASS_PRE) {
        /* No pre-process. Pass the address */
//        image_p->input_mem_addr = image_p->raw_img_p->image_mem_addr;
//        image_p->input_mem_len = image_p->raw_img_p->image_mem_len;

        dbg_msg("\npre_processing: Bypass: use image 0x%x\n", image_p->raw_img_p->image_mem_addr);

		npu_reset();
		kdp_enable_npu_int();

        return RET_NO_ERROR;
    }

    for (i = 0; i < MAX_MODEL_REGISTRATIONS; i++) {
        if (pre_proc_fns[i].model_id == model_id) {
            ppf = pre_proc_fns[i].ppf;
            break;
        }
    }

    if (ppf) {
        rc = ppf(model_id, image_p, params_p);
    }

    if (rc)
        return RET_ERROR;
    else
        return RET_NO_ERROR;
}

void pre_processing_init(void)
{
    kdpio_pre_processing_register(KNERON_FDSMALLBOX, kdp_preproc_inproc);
    kdpio_pre_processing_register(KNERON_FR_VGG10, kdp_preproc_fr);
    kdpio_pre_processing_register(KNERON_LM_5PTS, kdp_preproc_inproc);
    kdpio_pre_processing_register(KNERON_TINY_YOLO_PERSON, kdp_preproc_inproc);
    kdpio_pre_processing_register(TINY_YOLO_VOC, kdp_preproc_inproc);
}

/* Registration APIs */
int kdpio_pre_processing_register(int model_id, pre_proc_fn ppf)
{
    int i, rc;

    rc = -1;
    if (ppf == NULL)
        return rc;

    for (i = 0; i < MAX_MODEL_REGISTRATIONS; i++) {
        if (pre_proc_fns[i].ppf == NULL) {
            pre_proc_fns[i].ppf = ppf;
            pre_proc_fns[i].model_id = model_id;
            rc = 0;
            break;
        }
    }

    return rc;
}

void kdpio_pre_processing_unregister(int model_id, pre_proc_fn ppf)
{
    int i;

    if (model_id == 0 || ppf == NULL)
        return;

    for (i = 0; i < MAX_MODEL_REGISTRATIONS; i++) {
        if (pre_proc_fns[i].model_id == model_id && pre_proc_fns[i].ppf == ppf) {
            pre_proc_fns[i].ppf = NULL;
            pre_proc_fns[i].model_id = 0;
            return;
        }
    }
}
