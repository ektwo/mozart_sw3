/*
 * Kneron CPU Operation driver for KDP520
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
 
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "base.h"
#include "kdpio.h"
#include "ipc.h"
#include "cpu_ops.h"
#include "dbg.h"

struct cpu_op_func_s {
    int         cpu_op_id;
    cpu_op_fn   cof;
} cpu_op_fns[MAX_MODEL_REGISTRATIONS];

int kdp_do_cpu_ops(struct kdp_image *image_p, struct cpu_node *cpu_p)
{
    int i, op_id, len;
    cpu_op_fn cof = NULL;

    op_id = cpu_p->op_type;

    if (image_p->raw_img_p->format & IMAGE_FORMAT_BYPASS_CPU_OP) {
        dbg_msg("kdp_do_cpu_ops: Bypass: cpu_op_id: %d\n", op_id);
        return 0;
    }

    dbg_msg("    cpu_op_id: %d\n", op_id);

    for (i = 0; i < MAX_MODEL_REGISTRATIONS; i++) {
        if (cpu_op_fns[i].cpu_op_id == op_id) {
            cof = cpu_op_fns[i].cof;
            break;
        }
    }

    if (cof) {
        len = cof(op_id, image_p, cpu_p);
    } else {
        dbg_msg("TODO: add support for cpu_op_id %d\n", op_id);
        len = -1;
    }

    return len;
}

void kdp_do_cpu_ops_init(void)
{
}

/* Registration APIs */
int kdpio_cpu_op_register(int cpu_op, cpu_op_fn cof)
{
    int i, rc;

    rc = -1;
    if (cof == NULL)
        return rc;

    for (i = 0; i < MAX_MODEL_REGISTRATIONS; i++) {
        if (cpu_op_fns[i].cof == NULL) {
            cpu_op_fns[i].cof = cof;
            cpu_op_fns[i].cpu_op_id = cpu_op;
            rc = 0;
            break;
        }
    }

    return rc;
}

void kdpio_cpu_op_unregister(int cpu_op, cpu_op_fn cof)
{
    int i;

    if (cpu_op == 0 || cof == NULL)
        return;

    for (i = 0; i < MAX_MODEL_REGISTRATIONS; i++) {
        if (cpu_op_fns[i].cpu_op_id == cpu_op && cpu_op_fns[i].cof == cof) {
            cpu_op_fns[i].cof = NULL;
            cpu_op_fns[i].cpu_op_id = 0;
            return;
        }
    }
}

