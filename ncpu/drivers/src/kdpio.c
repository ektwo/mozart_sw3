/*
 * Kneron NPU driver for KDP520
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */
 
#include "base.h"
#include "kdpio.h"
#include "npu.h"
#include "cpu_ops.h"
#include "pre_processing.h"
#include "post_processing.h"
#include "ipc.h"
#include "dbg.h"

/* Functions */

static int kdp_get_output(struct kdp_image *image_p, struct out_node *out_p)
{
    uint32_t    data;
    int len;

    data = kdp_read_data_size();
    if (data == 8)
        image_p->output_format = 0;
    else if (data == 0)
        image_p->output_format = 1;

    image_p->output_row = out_p->row_length;
    image_p->output_col = out_p->col_length;
    image_p->output_channel = out_p->ch_length;
    image_p->output_radix = out_p->output_radix;
    image_p->output_scale = out_p->output_scale;

    len = sizeof(struct out_node);
    return len;
}

/*
 * API
 *
 */
void kdpio_init(void)
{
    kdp_do_cpu_ops_init();
    pre_processing_init();
    post_processing_init();

    kdp_init_npu();
}

void kdpio_set_model(struct kdp_image *image_p, struct kdp_model *model_p)
{
    struct setup_struct *setup_p;

    image_p->model_p = model_p;

    setup_p = (struct setup_struct *)model_p->setup_mem_addr;

    /* Prepare for pre-process */
    //removed by Vincent, already set in scpu_comm_thread()
    //image_p->model_id = setup_p->header.model_type;
    image_p->setup_mem_p = (char *)&setup_p->nodes[0].in_nd;

    image_p->input_row = setup_p->header.input_row;
    image_p->input_col = setup_p->header.input_col;
    image_p->input_channel = setup_p->header.input_channel;

    image_p->input_radix = setup_p->header.input_radix;

    /* This should be set by scpu */
    //removed by Vincent
    //image_p->raw_img_p->results[0].model_id = image_p->model_id;
}

int kdpio_run_preprocess(struct kdp_image *image_p)
{
    char *setup_mem_p;
    struct in_node *in_p;
    int model_id, node_id;

    setup_mem_p = image_p->setup_mem_p;
    model_id = image_p->model_id;
    in_p = (struct in_node *)setup_mem_p;
    node_id = in_p->node_id;

    if (node_id != NODE_TYPE_IN) {
        err_msg("kdpio_run_preprocess: wrong node_id %d @ %p\n", node_id, in_p);
        return RET_ERROR;
    }

    image_p->raw_img_p->tick_start_pre = osKernelGetTickCount();
    pre_processing(image_p, model_id, NULL);
    image_p->raw_img_p->tick_end_pre = osKernelGetTickCount();

    setup_mem_p += sizeof(struct in_node);

    image_p->setup_mem_p = setup_mem_p;

    return RET_NO_ERROR;
}

int kdpio_run_npu_op(struct kdp_image *image_p)
{
    if (image_p->raw_img_p->format & IMAGE_FORMAT_BYPASS_NPU_OP) {
        dbg_msg("kdpio_run_npu_op: Bypass\n");
        return RET_NO_ERROR;
    }

    image_p->raw_img_p->tick_start_npu = osKernelGetTickCount();
    kdp_enable_npu(image_p);
    // npu-op end time is in kdpio_run_cpu_op
    //image_p->raw_img_p->tick_end_npu = osKernelGetTickCount();

    return RET_NO_ERROR;
}

int kdpio_run_cpu_op(struct kdp_image *image_p)
{
    char *setup_mem_p;
    struct cpu_node *cpu_p;
    int node_id, rc;

    // npu op start time is in kdpio_run_npu_op
    image_p->raw_img_p->tick_end_npu = osKernelGetTickCount();

    /* Check NPU interrupt first */
    rc = kdp_handle_int();
    if (rc < 0)
        return RET_ERROR;

    setup_mem_p = image_p->setup_mem_p;
    cpu_p = (struct cpu_node *)setup_mem_p;
    node_id = cpu_p->node_id;

    if (node_id == NODE_TYPE_OUT)
        return RET_NO_ERROR;

    if (node_id != NODE_TYPE_CPU) {
        err_msg("kdpio_run_cpu_op: wrong node_id %d @ %p\n", node_id, cpu_p);
        return RET_ERROR;
    }

    rc = kdp_do_cpu_ops(image_p, cpu_p);
    if (rc < 0)
        return RET_ERROR;

    image_p->setup_mem_p += rc;
    return RET_NO_ERROR;
}

int kdpio_run_postprocess(struct kdp_image *image_p, pre_post_fn perf_improv_fn)
{
    char *setup_mem_p;
    struct out_node *out_p;
    int node_id, model_id, rc, size;

    setup_mem_p = image_p->setup_mem_p;
    model_id = image_p->model_id;
    out_p = (struct out_node *)setup_mem_p;
    node_id = out_p->node_id;

    if (node_id != NODE_TYPE_OUT) {
        err_msg("kdpio_run_postprocess: bad node_id: %d @ %p\n", node_id, out_p);
        rc = RET_ERROR;
    }

    rc = kdp_get_output(image_p, out_p);
    if (rc < 0) {
        return RET_ERROR;
    }

    image_p->raw_img_p->tick_start_post = osKernelGetTickCount();

    size = image_p->output_channel * image_p->output_row * image_p->output_col * 2;
    if (perf_improv_fn && image_p->output_mem_addr) {
        /* Let the callback handle data move */
        perf_improv_fn((void *)image_p->output_mem_addr, (void *)out_p->node_list[0].addr, size);
    } else {
        /* Keep using the same */
        image_p->output_mem_addr = out_p->node_list[0].addr;
    }

    image_p->setup_mem_p += rc;

    post_processing(image_p, model_id, 0);

    image_p->raw_img_p->tick_end_post = osKernelGetTickCount();

    return RET_NO_ERROR;
}

void kdpio_exit(void)
{
    kdp_exit_npu();
}
