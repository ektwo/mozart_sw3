#include "bootloader.h"
#include "kneron_mozart.h" 
#include "memory.h"

unsigned int bootloader_handler(void) {
#define WHOAMI_ADDR (S_D_RAM_ADDR + SdRAM_MEM_SIZE - 16)
    unsigned int flag = (*(volatile unsigned int *)(WHOAMI_ADDR));

	return flag;
}