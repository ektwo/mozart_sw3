/* -------------------------------------------------------------------------- 
 * Copyright (c) 2018 Kneron Inc. All rights reserved.
 *
 *      Name:    main.c
 *      Purpose: Kneron NCPU
 *
 *---------------------------------------------------------------------------*/


#include <string.h>
#include "kneron_mozart.h"
#include "kneron_mozart_ext.h"
#include "cmsis_os2.h"
#include "DrvUART010.h"
#include "memory.h"
#include "ipc.h"
#include "io.h"
#include "base.h"
#include "kdpio.h"
#include "scu_ipc.h"
#include "version.h"
#include "bootloader.h"
#include "dbg.h"

#define FLAG_SCPU_COMM_START			0x0001
#define FLAG_SCPU_COMM_ISR				0x0002

#define FLAG_NPU_COMM_START				0x0001
#define FLAG_NPU_COMM_ISR               0x0002

#define SCPU_IRQ                SYSC_SGI_S_STATUS_IRQ

osThreadId_t tid_scpu_comm;
osThreadId_t tid_npu_comm;

struct scpu_to_ncpu *in_comm_p;
struct ncpu_to_scpu *out_comm_p;

struct kdp_image image_s[IPC_IMAGE_ACTIVE_MAX];
int npu_img_idx;
struct kdp_image *npu_img_p;
struct kdp_image *pp_img_p;

extern void kdp_ddr_dma_copy(unsigned int *src_ptr, unsigned int *dst_ptr, int size);

/*----------------------------------------------------------------------------
 *      SCPU Communication Thread
 *---------------------------------------------------------------------------*/
void scpu_comm_thread(void *argument)
{
    int32_t flags;
    struct kdp_img_raw *raw_img_p;
    struct kdp_model *model_p;
    int model_slot_index, image_index;

    flags = osThreadFlagsWait(FLAG_SCPU_COMM_START, osFlagsWaitAny ,osWaitForever);

    out_comm_p->status = STATUS_OK;

    for (;;) {
        flags = osThreadFlagsWait(FLAG_SCPU_COMM_ISR | FLAG_NPU_COMM_ISR, 
                            osFlagsWaitAny, osWaitForever);
        osThreadFlagsClear(flags);

        if (flags & FLAG_SCPU_COMM_ISR) {
            dbg_msg("ncpu: got IPC interrupt\n");

            if (in_comm_p->cmd == CMD_RUN_NPU) {
                model_slot_index = in_comm_p->model_slot_index;
                model_p = &in_comm_p->models[model_slot_index];

                dbg_msg("NPU: model_slot_index=%d, NPU output mem addr=0x%x, len=%d\n", model_slot_index, (uint32_t)model_p->output_mem_addr, model_p->output_mem_len);
                dbg_msg("NPU: input_mem_addr = 0x%x, len=0x%x, cmd_mem_addr=0x%x, len=0x%x, wt_addr=0x%x, wt_len=0x%x, setup_addr=0x%x, setup_len=0x%x\n",
                   (uint32_t)model_p->input_mem_addr, (uint32_t)model_p->input_mem_len,
                   (uint32_t)model_p->cmd_mem_addr, (uint32_t)model_p->cmd_mem_len,
                   (uint32_t)model_p->weight_mem_addr, (uint32_t)model_p->weight_mem_len,
                   (uint32_t)model_p->setup_mem_addr, (uint32_t)model_p->setup_mem_len);

                image_index = in_comm_p->active_img_index;
                raw_img_p = &in_comm_p->raw_images[image_index];
                npu_img_p = &image_s[npu_img_idx];
                npu_img_idx = !npu_img_idx;

                npu_img_p->raw_img_p = raw_img_p;
                npu_img_p->model_id = in_comm_p->models_type[model_slot_index];
                npu_img_p->input_mem_addr = model_p->input_mem_addr;
                npu_img_p->input_mem_len = model_p->input_mem_len;
                npu_img_p->result_mem_addr = raw_img_p->results[model_slot_index].result_mem_addr;
                npu_img_p->result_mem_len = raw_img_p->results[model_slot_index].result_mem_len;

                dbg_msg("NPU: model_type = %d\n",npu_img_p->model_id);

                out_comm_p->img_results[image_index].seq_num = raw_img_p->seq_num;
                out_comm_p->img_results[image_index].status = IMAGE_STATE_ACTIVE;

                npu_img_p->input_mem_addr2 = in_comm_p->input_mem_addr2;
                npu_img_p->input_mem_len2 = in_comm_p->input_mem_len2;

                kdpio_set_model(npu_img_p, model_p);

                kdpio_run_preprocess(npu_img_p);
                kdpio_run_npu_op(npu_img_p);
            } else {
                /* TODO */
            }

            scu_ipc_clear_from_scpu_int();
            NVIC_ClearPendingIRQ((IRQn_Type)SCPU_IRQ);
            NVIC_EnableIRQ((IRQn_Type)SCPU_IRQ);
        }
    }
}

void SCPU_IRQHandler(void)
{
    NVIC_DisableIRQ((IRQn_Type)SCPU_IRQ);
    dbg_msg("NPU: received NPU interrupt\n");
    osThreadFlagsSet(tid_scpu_comm, FLAG_SCPU_COMM_ISR);
}

static void npu_out_data_move(void *dst_p, void *src_p, int size)
{
    /* Move data out from npu output buffer for parallel handling */
    memcpy(dst_p, src_p, size);

    /* Notify scpu now */
    scu_ipc_trigger_to_scpu_int();
}

/*----------------------------------------------------------------------------
 *      NPU Communication Thread
 *---------------------------------------------------------------------------*/
void npu_comm_thread(void *argument)
{
    int32_t flags;
    int rc, i;

    flags = osThreadFlagsWait(FLAG_NPU_COMM_START, osFlagsWaitAny ,osWaitForever);
    if (flags < 0) {
    }

    for (;;) {
        flags = osThreadFlagsWait(FLAG_NPU_COMM_ISR, osFlagsWaitAny, osWaitForever);
        osThreadFlagsClear(flags);

        if (flags & FLAG_NPU_COMM_ISR) {

            rc = kdpio_run_cpu_op(npu_img_p);
            if (rc == RET_NO_ERROR) {
                i = npu_img_p->raw_img_p->ref_idx;

                dbg_msg("npu out: img[%d] state: %d -> %d\n", i, out_comm_p->img_results[i].status, IMAGE_STATE_NPU_DONE);

                out_comm_p->img_index_done = i;
                out_comm_p->img_results[i].status = IMAGE_STATE_NPU_DONE;
                npu_img_p->output_mem_addr = in_comm_p->output_mem_addr2;
                pp_img_p = npu_img_p;

                rc = kdpio_run_postprocess(pp_img_p, npu_out_data_move);

                if (rc == RET_NO_ERROR) {
                    out_comm_p->output_count++;
                    out_comm_p->status = STATUS_OK;

                    i = pp_img_p->raw_img_p->ref_idx;

                    dbg_msg("npu out: img[%d] state: %d -> %d\n", i, out_comm_p->img_results[i].status, IMAGE_STATE_DONE);

                    out_comm_p->img_index_done = i;
                    out_comm_p->img_results[i].status = IMAGE_STATE_DONE;

                    scu_ipc_trigger_to_scpu_int();
                }
            }

            NVIC_ClearPendingIRQ((IRQn_Type)NPU_NPU_IRQ);
            NVIC_EnableIRQ((IRQn_Type)NPU_NPU_IRQ);
        }
    }
}

void NPU_IRQHandler(void)
{
    NVIC_DisableIRQ((IRQn_Type)NPU_NPU_IRQ);
    osThreadFlagsSet(tid_npu_comm, FLAG_NPU_COMM_ISR);
}

/*----------------------------------------------------------------------------
 *      Main and Idle Thread
 *---------------------------------------------------------------------------*/
void ncpu_main(void *argument)
{
    tid_scpu_comm = osThreadNew(scpu_comm_thread, NULL, NULL);
    tid_npu_comm = osThreadNew(npu_comm_thread, NULL, NULL);

    NVIC_SetVector((IRQn_Type)SCPU_IRQ, (uint32_t)SCPU_IRQHandler);
    NVIC_SetVector((IRQn_Type)NPU_NPU_IRQ, (uint32_t)NPU_IRQHandler);

    osThreadFlagsSet(tid_scpu_comm, FLAG_SCPU_COMM_START);	
    osThreadFlagsSet(tid_npu_comm, FLAG_NPU_COMM_START);
    
    in_comm_p = (struct scpu_to_ncpu *)IPC_MEM_ADDR;
    out_comm_p = (struct ncpu_to_scpu *)IPC_MEM_ADDR2;

    out_comm_p->id = NCPU2SCPU_ID;
    //kdpio_test_post_processing();
    kdpio_init();
    scu_ipc_enable_to_scpu_int();

    NVIC_EnableIRQ((IRQn_Type)SCPU_IRQ);    
    NVIC_EnableIRQ((IRQn_Type)NPU_NPU_IRQ);

    osDelay(osWaitForever);
    /* Idle */
    while(1);
}

int cpu_type = 0;
void info(uint32_t boot_loader_flag) {
#define WHOAMI_KEY 0x12345678
    char infobuf[100];
    osVersion_t osv;
    osStatus_t status;
    uint32_t rom_table;
	
    if (WHOAMI_KEY == boot_loader_flag) {
        dbg_msg("\n=== From Bootloader ===\n");
    }
    else {
        dbg_msg("\n=== From JTag debug ===\n");
    }	
    
    rom_table = inw(CPU_ID_STS_ADDR);
    if (CPU_ID_SCPU == rom_table) {
        dbg_msg("=== [I am SCPU] firmware version: %x ===\n", g_fw_misc.version);
        cpu_type = 0;
    }
    else if (CPU_ID_NCPU == rom_table) {
        dbg_msg("=== [I am NCPU] firmware version: %x ===\n", g_fw_misc.version);
        cpu_type = 1;
    }  
    
    status = osKernelGetInfo(&osv, infobuf, sizeof(infobuf));
    if(status == osOK) {	
        dbg_msg("+-------------------Keil RTX5----------------------+\n");
        dbg_msg("Kernel Information: %s\r\n", infobuf);
        dbg_msg("Kernel Version    : %d\r\n", osv.kernel);
        dbg_msg("Kernel API Version: %d\r\n", osv.api);      
    }
}

/*----------------------------------------------------------------------------
 *      Main: Initialize and start RTX Kernel
 *---------------------------------------------------------------------------*/
int main(void)
{
	unsigned int boot_loader_flag = 0x0;     
	boot_loader_flag = bootloader_handler();    
    fLib_SerialInit(DEBUG_CONSOLE, DEFAULT_CONSOLE_BAUD, PARITY_NONE, 0, 8, 0);
    info(boot_loader_flag);

    // System Initialization
    SystemCoreClockUpdate();

    osKernelInitialize();

#if 0 // Test only
    //ncpu dma ddr2ddr
	  char* dma_buf1 = (char*)0x80000000;
		char* dma_buf2 = (char*)0x80001000;
		int index = 0;
		for(index = 0; index < 100; index++){
				dma_buf1[index] = '$';
		}
		unsigned int *ptr1 = (unsigned int *)dma_buf1;
		unsigned int *ptr2 = (unsigned int *)dma_buf2;
    kdp_ddr_dma_copy(ptr1, ptr2, 100);
#endif

    osThreadNew(ncpu_main, NULL, NULL);

    if (osKernelGetState() == osKernelReady) {
        osKernelStart();                    // Start thread execution
    }

    while(1);
}

