;/**************************************************************************//**
; * @file     startup_ARMCM4.s
; * @brief    CMSIS Core Device Startup File for
; *           ARMCM4 Device
; * @version  V5.3.1
; * @date     09. July 2018
; ******************************************************************************/
;/*
; * Copyright (c) 2009-2018 Arm Limited. All rights reserved.
; *
; * SPDX-License-Identifier: Apache-2.0
; *
; * Licensed under the Apache License, Version 2.0 (the License); you may
; * not use this file except in compliance with the License.
; * You may obtain a copy of the License at
; *
; * www.apache.org/licenses/LICENSE-2.0
; *
; * Unless required by applicable law or agreed to in writing, software
; * distributed under the License is distributed on an AS IS BASIS, WITHOUT
; * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; * See the License for the specific language governing permissions and
; * limitations under the License.
; */

;//-------- <<< Use Configuration Wizard in Context Menu >>> ------------------


;<h> Stack Configuration
;  <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
;</h>

Stack_Size      EQU      0x00002000;0x00000400

                AREA     STACK, NOINIT, READWRITE, ALIGN=3
__stack_limit
Stack_Mem       SPACE    Stack_Size
__initial_sp


;<h> Heap Configuration
;  <o> Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
;</h>

Heap_Size       EQU      0x00000C00

                IF       Heap_Size != 0                      ; Heap is provided
                AREA     HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE    Heap_Size
__heap_limit
                ENDIF


                PRESERVE8
                THUMB


; Vector Table Mapped to Address 0 at Reset

                AREA     RESET, DATA, READONLY
                EXPORT   __Vectors
                EXPORT   __Vectors_End
                EXPORT   __Vectors_Size

__Vectors       DCD      __initial_sp                        ;     Top of Stack
                DCD      Reset_Handler                       ;     Reset Handler
                DCD      NMI_Handler                         ; -14 NMI Handler
                DCD      HardFault_Handler                   ; -13 Hard Fault Handler
                DCD      MemManage_Handler                   ; -12 MPU Fault Handler
                DCD      BusFault_Handler                    ; -11 Bus Fault Handler
                DCD      UsageFault_Handler                  ; -10 Usage Fault Handler
                DCD      0                                   ;     Reserved
                DCD      0                                   ;     Reserved
                DCD      0                                   ;     Reserved
                DCD      0                                   ;     Reserved
                DCD      SVC_Handler                         ;  -5 SVCall Handler
                DCD      DebugMon_Handler                    ;  -4 Debug Monitor Handler
                DCD      0                                   ;     Reserved
                DCD      PendSV_Handler                      ;  -2 PendSV Handler
                DCD      SysTick_Handler                     ;  -1 SysTick Handler

                ; Interrupts
                DCD      PINMUX_IRQHandler						;   0 Interrupt 0
                DCD      DDR3_IRQHandler                  ;   1 Interrupt 1
                DCD      ADC_IRQHandler                  ;   2 Interrupt 2
                DCD      AHB_DMA_IRQHandler                  ;   3 Interrupt 3
                DCD      DMA_TC_IRQHandler                  ;   4 Interrupt 4
                DCD      DMA_ERR_IRQHandler                  ;   5 Interrupt 5
                DCD      AHB_DMA1_IRQHandler                  ;   6 Interrupt 6
                DCD      DMA1_TC_IRQHandler                  ;   7 Interrupt 7
                DCD      DMA2_ERR_IRQHandler                  ;   8 Interrupt 8
                DCD      DPI2AHB_IRQHandler                  ;   9 Interrupt 9
				DCD      GPIO010_IRQHandler                 ;   10 Interrupt 10
                DCD      PWMTMR1_IRQHandler                  ;   11 Interrupt 11
                DCD      PWMTMR2_IRQHandler                  ;   12 Interrupt 12
                DCD      PWMTMR3_IRQHandler                  ;   13 Interrupt 13
				DCD      PWMTMR4_IRQHandler                 ;   14 Interrupt 14
                DCD      PWMTMR5_IRQHandler                 ;   15 Interrupt 15
				DCD      PWMTMR6_IRQHandler                 ;   16 Interrupt 16
                DCD      PWMTMR7_IRQHandler                 ;   17 Interrupt 17
                DCD      SDC_IRQHandler                 ;   18 Interrupt 18
                DCD      SPI_IRQHandler                  ;   19 Interrupt 19
                DCD      SPI2AHB_RD_IRQHandler                 ;   20 Interrupt 20
                DCD      SPI2AHB_WR_IRQHandler                 ;   21 Interrupt 21
                DCD      SPI2AHB_IRQHandler                 ;   22 Interrupt 22
                DCD      SSP0_IRQHandler                 ;   23 Interrupt 23
                DCD      SSP1_IRQHandler                 ;   24 Interrupt 24
                DCD      TMR0_0_IRQHandler                 ;   25 Interrupt 25
				DCD      TMR0_1_IRQHandler                 ;   26 Interrupt 26
                DCD      TMR0_2_IRQHandler                 ;   27 Interrupt 27
                DCD      TMR0_3_IRQHandler                 ;   28 Interrupt 28
                DCD      UART0_IRQHandler                 ;   29 Interrupt 29
				DCD      UART1_IRQHandler                 ;   30 Interrupt 30
                DCD      UART2_IRQHandler                 ;   31 Interrupt 31
				DCD      WDT_IRQHandler                 ;   32 Interrupt 32
                DCD      NPU_IRQHandler                 ;   33 Interrupt 33
                DCD      CSIRX_IRQHandler                 ;   34 Interrupt 34
                DCD      OTG_IRQHandler                 ;   35 Interrupt 35
                DCD      TMR1_0_IRQHandler                 ;   36 Interrupt 36
                DCD      TMR1_1_IRQHandler                 ;   37 Interrupt 37
				DCD      TMR1_2_IRQHandler                 ;   38 Interrupt 38
                DCD      TMR1_3_IRQHandler                 ;   39 Interrupt 39
                DCD      SYSTEM_IRQHandler                 ;   40 Interrupt 40
                DCD      MIPITX_IRQHandler                 ;   41 Interrupt 41
				DCD      IIC0_IRQHandler                 ;   42 Interrupt 42
                DCD      IIC1_IRQHandler                 ;   43 Interrupt 43
				DCD      IIC2_IRQHandler                 ;   44 Interrupt 44
				DCD      IIC3_IRQHandler                 ;   45 Interrupt 45
                DCD      SSP0_1_IRQHandler                 ;   46 Interrupt 46
				DCD      SSP1_1_IRQHandler                 ;   47 Interrupt 47
                DCD      UART1_1_IRQHandler                 ;   48 Interrupt 48
                DCD      UART1_2_IRQHandler                 ;   49 Interrupt 49
                DCD      UART1_3_IRQHandler                 ;   50 Interrupt 50
                DCD      SYSC_SGI_IRQHandler                 ;   51 Interrupt 51
                DCD      CSI_RX_IRQHandler                 ;   52 Interrupt 52
				DCD      LCDC_VSTATUS_IRQHandler                 ;   53 Interrupt 53
                DCD      LCDC_BAUPD_IRQHandler                 ;   54 Interrupt 54
                DCD      LCDC_FUR_IRQHandler                 ;   55 Interrupt 55
                DCD      LCDC_MERR_IRQHandler                 ;   56 Interrupt 56
				DCD      LCDC_IRQHandler                 ;   57 Interrupt 57


                SPACE    (214 * 4)                           ; Interrupts 10 .. 224 are left out
__Vectors_End
__Vectors_Size  EQU      __Vectors_End - __Vectors


                AREA     |.text|, CODE, READONLY

; Reset Handler

Reset_Handler   PROC
                EXPORT   Reset_Handler             [WEAK]
                IMPORT   SystemInit
                IMPORT   __main


                LDR      R0, =SystemInit
                BLX      R0
				LDR      R0, =__main
                BX       R0
                ENDP


; Macro to define default exception/interrupt handlers.
; Default handler are weak symbols with an endless loop.
; They can be overwritten by real handlers.
                MACRO
                Set_Default_Handler  $Handler_Name
$Handler_Name   PROC
                EXPORT   $Handler_Name             [WEAK]
                B        .
                ENDP
                MEND


; Default exception/interrupt handler

                Set_Default_Handler  NMI_Handler
                Set_Default_Handler  HardFault_Handler
                Set_Default_Handler  MemManage_Handler
                Set_Default_Handler  BusFault_Handler
                Set_Default_Handler  UsageFault_Handler
                Set_Default_Handler  SVC_Handler
                Set_Default_Handler  DebugMon_Handler
                Set_Default_Handler  PendSV_Handler
                Set_Default_Handler  SysTick_Handler

                Set_Default_Handler  PINMUX_IRQHandler
                Set_Default_Handler  DDR3_IRQHandler
                Set_Default_Handler  ADC_IRQHandler
                Set_Default_Handler  AHB_DMA_IRQHandler
                Set_Default_Handler  DMA_TC_IRQHandler
                Set_Default_Handler  DMA_ERR_IRQHandler
                Set_Default_Handler  AHB_DMA1_IRQHandler
                Set_Default_Handler  DMA1_TC_IRQHandler
                Set_Default_Handler  DMA2_ERR_IRQHandler
                Set_Default_Handler  DPI2AHB_IRQHandler
				Set_Default_Handler  GPIO010_IRQHandler
				Set_Default_Handler  PWMTMR1_IRQHandler
				Set_Default_Handler  PWMTMR2_IRQHandler
				Set_Default_Handler  PWMTMR3_IRQHandler
				Set_Default_Handler  PWMTMR4_IRQHandler
				Set_Default_Handler  PWMTMR5_IRQHandler
				Set_Default_Handler  PWMTMR6_IRQHandler
                Set_Default_Handler  PWMTMR7_IRQHandler
                Set_Default_Handler  SDC_IRQHandler
                Set_Default_Handler  SPI_IRQHandler
                Set_Default_Handler  SPI2AHB_RD_IRQHandler
                Set_Default_Handler  SPI2AHB_WR_IRQHandler
                Set_Default_Handler  SPI2AHB_IRQHandler
                Set_Default_Handler  SSP0_IRQHandler
                Set_Default_Handler  SSP1_IRQHandler
                Set_Default_Handler  TMR0_0_IRQHandler
				Set_Default_Handler  TMR0_1_IRQHandler
				Set_Default_Handler  TMR0_2_IRQHandler
				Set_Default_Handler  TMR0_3_IRQHandler
				Set_Default_Handler  UART0_IRQHandler
				Set_Default_Handler  UART1_IRQHandler
                Set_Default_Handler  UART2_IRQHandler
				Set_Default_Handler  WDT_IRQHandler
                Set_Default_Handler  NPU_IRQHandler
                Set_Default_Handler  CSIRX_IRQHandler
                Set_Default_Handler  OTG_IRQHandler
                Set_Default_Handler  TMR1_0_IRQHandler
                Set_Default_Handler  TMR1_1_IRQHandler
                Set_Default_Handler  TMR1_2_IRQHandler
				Set_Default_Handler  TMR1_3_IRQHandler
				Set_Default_Handler  SYSTEM_IRQHandler
				Set_Default_Handler  MIPITX_IRQHandler
				Set_Default_Handler  IIC0_IRQHandler
				Set_Default_Handler  IIC1_IRQHandler
				Set_Default_Handler  IIC2_IRQHandler
				Set_Default_Handler  IIC3_IRQHandler
				Set_Default_Handler  SSP0_1_IRQHandler
				Set_Default_Handler  SSP1_1_IRQHandler
				Set_Default_Handler  UART1_1_IRQHandler
				Set_Default_Handler  UART1_2_IRQHandler
                Set_Default_Handler  UART1_3_IRQHandler
                Set_Default_Handler  SYSC_SGI_IRQHandler
                Set_Default_Handler  CSI_RX_IRQHandler
                Set_Default_Handler  LCDC_VSTATUS_IRQHandler
                Set_Default_Handler  LCDC_BAUPD_IRQHandler
                Set_Default_Handler  LCDC_FUR_IRQHandler
				Set_Default_Handler  LCDC_MERR_IRQHandler
				Set_Default_Handler  LCDC_IRQHandler

                ALIGN


; User setup Stack & Heap

                EXPORT   __stack_limit
                EXPORT   __initial_sp
                IF       Heap_Size != 0                      ; Heap is provided
                EXPORT   __heap_base
                EXPORT   __heap_limit
                ENDIF

                END
