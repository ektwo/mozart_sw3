#ifndef __VERSION_H__
#define __VERSION_H__


struct fw_misc_data {
    unsigned char reserved[4];    
    unsigned int version;
} __attribute__((packed));

static struct fw_misc_data g_fw_misc __attribute__((section("misc_data"))) = 
{
    .version = 0x00190531
};

#endif
