# generated by ch2py.py @17:10:51 -- December 05, 2018 (Wednesday)
# source h file: /home/corey.chien/studio/project/kneron/mozart/software/common/npu.h

ADDR_NPU_CODE            = 0x30ff0000
ADDR_NPU_CLEN            = 0x30ff0004
ADDR_NPU_RUN             = 0x30ff0008
ADDR_NPU_ELEN            = 0x30ff000c
ADDR_NPU_VER             = 0x30ff0010
ADDR_NPU_MODE            = 0x30ff0014
ADDR_NPU_DMA             = 0x30ff0018
ADDR_NPU_RDMA0_SRC0      = 0x30ff001c
ADDR_NPU_RDMA0_SRC1      = 0x30ff0020
ADDR_NPU_RDMA0_SRC2      = 0x30ff0024
ADDR_NPU_RDMA0_DST       = 0x30ff0028
ADDR_NPU_RDMA0_BLK       = 0x30ff002c
ADDR_NPU_RDMA1_SRC0      = 0x30ff0030
ADDR_NPU_RDMA1_SRC1      = 0x30ff0034
ADDR_NPU_RDMA1_SRC2      = 0x30ff0038
ADDR_NPU_RDMA1_DST       = 0x30ff003c
ADDR_NPU_RDMA1_BLK       = 0x30ff0040
ADDR_NPU_RDMA2_SRC0      = 0x30ff0044
ADDR_NPU_RDMA2_SRC1      = 0x30ff0048
ADDR_NPU_RDMA2_SRC2      = 0x30ff004c
ADDR_NPU_RDMA2_DST       = 0x30ff0050
ADDR_NPU_RDMA2_BLK       = 0x30ff0054
ADDR_NPU_GETW0           = 0x30ff0058
ADDR_NPU_GETW1           = 0x30ff005c
ADDR_NPU_GETW2           = 0x30ff0060
ADDR_NPU_WDMA0_SRC       = 0x30ff0064
ADDR_NPU_WDMA0_DST0      = 0x30ff0068
ADDR_NPU_WDMA0_DST1      = 0x30ff006c
ADDR_NPU_WDMA0_DST2      = 0x30ff0070
ADDR_NPU_WDMA0_BLK       = 0x30ff0074
ADDR_NPU_WDMA1_SRC       = 0x30ff0078
ADDR_NPU_WDMA1_DST0      = 0x30ff007c
ADDR_NPU_WDMA1_DST1      = 0x30ff0080
ADDR_NPU_WDMA1_DST2      = 0x30ff0084
ADDR_NPU_WDMA1_BLK       = 0x30ff0088
ADDR_NPU_NMEM_FM0        = 0x30ff008c
ADDR_NPU_NMEM_FM1        = 0x30ff0090
ADDR_NPU_NMEM_FM2        = 0x30ff0094
ADDR_NPU_NMEM_PS0        = 0x30ff0098
ADDR_NPU_NMEM_PS1        = 0x30ff009c
ADDR_NPU_NMEM_PS2        = 0x30ff00a0
ADDR_NPU_NMEM_ST0        = 0x30ff00a4
ADDR_NPU_NMEM_ST1        = 0x30ff00a8
ADDR_NPU_NMEM_ST2        = 0x30ff00ac
ADDR_NPU_NMEM_RDMA0      = 0x30ff00b0
ADDR_NPU_NMEM_RDMA1      = 0x30ff00b4
ADDR_NPU_NMEM_RDMA2      = 0x30ff00b8
ADDR_NPU_NMEM_WDMA0      = 0x30ff00bc
ADDR_NPU_NMEM_WDMA1      = 0x30ff00c0
ADDR_NPU_NMEM_WDMA2      = 0x30ff00c4
ADDR_NPU_NMEM_WT         = 0x30ff00c8
ADDR_NPU_NMEM_LB         = 0x30ff00cc
ADDR_NPU_CONV            = 0x30ff00d0
ADDR_NPU_FMAP0           = 0x30ff00d4
ADDR_NPU_FMAP1           = 0x30ff00d8
ADDR_NPU_ZPAD            = 0x30ff00dc
ADDR_NPU_NEXT0           = 0x30ff00e0
ADDR_NPU_NEXT1           = 0x30ff00e4
ADDR_NPU_NEXT2           = 0x30ff00e8
ADDR_NPU_LWT             = 0x30ff00ec
ADDR_NPU_STORE           = 0x30ff00f0
ADDR_NPU_TRIM            = 0x30ff00f4
ADDR_NPU_RESHAPE         = 0x30ff00f8
ADDR_NPU_PCONV           = 0x30ff00fc
ADDR_NPU_BN              = 0x30ff0100
ADDR_NPU_RELU            = 0x30ff0104
ADDR_NPU_RELU6           = 0x30ff0108
ADDR_NPU_POOL            = 0x30ff010c
ADDR_NPU_GAP             = 0x30ff0110
ADDR_NPU_FORMAT          = 0x30ff0114
ADDR_NPU_RESIZE          = 0x30ff0118
ADDR_NPU_RESIZE_SRC      = 0x30ff011c
ADDR_NPU_RESIZE_DST      = 0x30ff0120
ADDR_NPU_RESIZE_RATIO    = 0x30ff0124
ADDR_NPU_CHAN            = 0x30ff0128
ADDR_NPU_INTEN           = 0x30ff012c
ADDR_NPU_INT             = 0x30ff0130
ADDR_NPU_DBG0            = 0x30ff0134
ADDR_NPU_DBG1            = 0x30ff0138
ADDR_NPU_REGM            = 0x30ff013c
ADDR_NPU_DUMMY_0         = 0x30ff0140
ADDR_NPU_DUMMY_1         = 0x30ff0144
ADDR_NPU_DUMMY_2         = 0x30ff0148


def NPU_CODE_a(x):             return (((x)&0xffffffff)<<0)
def NPU_CLEN_l(x):             return (((x)&0xfffff)<<0)
def NPU_RUN_conv(x):           return (((x)&0x1)<<23)
def NPU_RUN_getw(x):           return (((x)&0x1)<<22)
def NPU_RUN_wdma(x):           return (((x)&0x1)<<21)
def NPU_RUN_rdma(x):           return (((x)&0x1)<<20)
def NPU_RUN_wfc(x):            return (((x)&0x1)<<17)
def NPU_RUN_busy(x):           return (((x)&0x1)<<16)
def NPU_RUN_slp(x):            return (((x)&0x1)<<9)
def NPU_RUN_ret(x):            return (((x)&0x1)<<8)
def NPU_RUN_conti(x):          return (((x)&0x1)<<1)
def NPU_RUN_go(x):             return (((x)&0x1)<<0)
def NPU_ELEN_l(x):             return (((x)&0xfffff)<<0)
def NPU_VER_num(x):            return (((x)&0xffff)<<0)
def NPU_MODE_8b_3x3_conv(x):   return (((x)&0x1)<<0)
def NPU_DMA_bl(x):             return (((x)&0x1fff)<<0)
def NPU_RDMA0_SRC0_sa(x):      return (((x)&0xffffffff)<<0)
def NPU_RDMA0_SRC1_pitch(x):   return (((x)&0xffffff)<<0)
def NPU_RDMA0_SRC2_len(x):     return (((x)&0xffff)<<0)
def NPU_RDMA0_DST_dl(x):       return (((x)&0xfff)<<0)
def NPU_RDMA0_BLK_line(x):     return (((x)&0x1fff)<<0)
def NPU_RDMA1_SRC0_sa(x):      return (((x)&0xffffffff)<<0)
def NPU_RDMA1_SRC1_pitch(x):   return (((x)&0xffffff)<<0)
def NPU_RDMA1_SRC2_len(x):     return (((x)&0xffff)<<0)
def NPU_RDMA1_DST_dl(x):       return (((x)&0xfff)<<0)
def NPU_RDMA1_BLK_line(x):     return (((x)&0x1fff)<<0)
def NPU_RDMA2_SRC0_sa(x):      return (((x)&0xffffffff)<<0)
def NPU_RDMA2_SRC1_pitch(x):   return (((x)&0xffffff)<<0)
def NPU_RDMA2_SRC2_len(x):     return (((x)&0xffff)<<0)
def NPU_RDMA2_DST_dl(x):       return (((x)&0xfff)<<0)
def NPU_RDMA2_BLK_line(x):     return (((x)&0x1fff)<<0)
def NPU_GETW0_sa(x):           return (((x)&0xffffffff)<<0)
def NPU_GETW1_len(x):          return (((x)&0xfffffff)<<0)
def NPU_GETW2_back(x):         return (((x)&0x7fff)<<0)
def NPU_WDMA0_SRC_sl(x):       return (((x)&0xfff)<<0)
def NPU_WDMA0_DST0_da(x):      return (((x)&0xffffffff)<<0)
def NPU_WDMA0_DST1_pitch(x):   return (((x)&0xffffff)<<0)
def NPU_WDMA0_DST2_len(x):     return (((x)&0xffff)<<0)
def NPU_WDMA0_BLK_line(x):     return (((x)&0x1fff)<<0)
def NPU_WDMA1_SRC_sl(x):       return (((x)&0xfff)<<0)
def NPU_WDMA1_DST0_da(x):      return (((x)&0xffffffff)<<0)
def NPU_WDMA1_DST1_pitch(x):   return (((x)&0xffffff)<<0)
def NPU_WDMA1_DST2_len(x):     return (((x)&0xffff)<<0)
def NPU_WDMA1_BLK_line(x):     return (((x)&0x1fff)<<0)
def NPU_NMEM_FM0_offset_x(x):  return (((x)&0xfff)<<16)
def NPU_NMEM_FM0_offset_y(x):  return (((x)&0x3f)<<8)
def NPU_NMEM_FM0_config(x):    return (((x)&0xf)<<0)
def NPU_NMEM_FM1_h(x):         return (((x)&0xfff)<<16)
def NPU_NMEM_FM1_w(x):         return (((x)&0xfff)<<0)
def NPU_NMEM_FM2_l(x):         return (((x)&0xfff)<<0)
def NPU_NMEM_PS0_offset_x(x):  return (((x)&0xfff)<<16)
def NPU_NMEM_PS0_offset_y(x):  return (((x)&0x3f)<<8)
def NPU_NMEM_PS0_config(x):    return (((x)&0xf)<<0)
def NPU_NMEM_PS1_h(x):         return (((x)&0xfff)<<16)
def NPU_NMEM_PS1_w(x):         return (((x)&0xfff)<<0)
def NPU_NMEM_PS2_l(x):         return (((x)&0xfff)<<0)
def NPU_NMEM_ST0_offset_x(x):  return (((x)&0xfff)<<16)
def NPU_NMEM_ST0_offset_y(x):  return (((x)&0x3f)<<8)
def NPU_NMEM_ST0_config(x):    return (((x)&0xf)<<0)
def NPU_NMEM_ST1_h(x):         return (((x)&0xfff)<<16)
def NPU_NMEM_ST1_w(x):         return (((x)&0xfff)<<0)
def NPU_NMEM_ST2_l(x):         return (((x)&0xfff)<<0)
def NPU_NMEM_RDMA0_offset_x(x): return (((x)&0xfff)<<16)
def NPU_NMEM_RDMA0_offset_y(x): return (((x)&0x3f)<<8)
def NPU_NMEM_RDMA0_config(x):  return (((x)&0xf)<<0)
def NPU_NMEM_RDMA1_h(x):       return (((x)&0xfff)<<16)
def NPU_NMEM_RDMA1_w(x):       return (((x)&0xfff)<<0)
def NPU_NMEM_RDMA2_l(x):       return (((x)&0xfff)<<0)
def NPU_NMEM_WDMA0_offset_x(x): return (((x)&0xfff)<<16)
def NPU_NMEM_WDMA0_offset_y(x): return (((x)&0x3f)<<8)
def NPU_NMEM_WDMA0_config(x):  return (((x)&0xf)<<0)
def NPU_NMEM_WDMA1_h(x):       return (((x)&0xfff)<<16)
def NPU_NMEM_WDMA1_w(x):       return (((x)&0xfff)<<0)
def NPU_NMEM_WDMA2_l(x):       return (((x)&0xfff)<<0)
def NPU_NMEM_WT_restart(x):    return (((x)&0x1)<<31)
def NPU_NMEM_WT_sa(x):         return (((x)&0xfff)<<16)
def NPU_NMEM_WT_ea(x):         return (((x)&0xfff)<<0)
def NPU_NMEM_LB_restart(x):    return (((x)&0x1)<<31)
def NPU_NMEM_LB_sa(x):         return (((x)&0xfff)<<16)
def NPU_NMEM_LB_ea(x):         return (((x)&0xfff)<<0)
def NPU_CONV_en(x):            return (((x)&0x1)<<31)
def NPU_CONV_ch_resume(x):     return (((x)&0x1)<<30)
def NPU_CONV_full_ch(x):       return (((x)&0x1)<<29)
def NPU_CONV_add_shift(x):     return (((x)&0x1)<<20)
def NPU_CONV_pformat(x):       return (((x)&0x3)<<16)
def NPU_CONV_step(x):          return (((x)&0x1)<<9)
def NPU_CONV_hstride2(x):      return (((x)&0x1)<<8)
def NPU_CONV_mode(x):          return (((x)&0xff)<<0)
def NPU_FMAP0_row(x):          return (((x)&0x1fff)<<16)
def NPU_FMAP0_col(x):          return (((x)&0x1fff)<<0)
def NPU_FMAP1_ch(x):           return (((x)&0x1fff)<<0)
def NPU_ZPAD_b(x):             return (((x)&0xf)<<12)
def NPU_ZPAD_r(x):             return (((x)&0xf)<<8)
def NPU_ZPAD_l(x):             return (((x)&0xf)<<4)
def NPU_ZPAD_t(x):             return (((x)&0xf)<<0)
def NPU_NEXT0_ch_end(x):       return (((x)&0x1fff)<<16)
def NPU_NEXT0_ch_start(x):     return (((x)&0x1fff)<<0)
def NPU_NEXT1_ch_total(x):     return (((x)&0x1fff)<<16)
def NPU_NEXT1_format(x):       return (((x)&0xf)<<0)
def NPU_NEXT2_line(x):         return (((x)&0xfff)<<0)
def NPU_LWT_prefetch(x):       return (((x)&0x1)<<2)
def NPU_LWT_w16b(x):           return (((x)&0x1)<<1)
def NPU_LWT_decomp(x):         return (((x)&0x1)<<0)
def NPU_STORE_en(x):           return (((x)&0x1)<<31)
def NPU_TRIM_wo(x):            return (((x)&0xf)<<24)
def NPU_TRIM_wv(x):            return (((x)&0xf)<<20)
def NPU_TRIM_w(x):             return (((x)&0xf)<<16)
def NPU_TRIM_ho(x):            return (((x)&0xf)<<4)
def NPU_TRIM_hm(x):            return (((x)&0xf)<<0)
def NPU_RESHAPE_ch(x):         return (((x)&0xffff)<<16)
def NPU_RESHAPE_ch_max(x):     return (((x)&0xffff)<<0)
def NPU_PCONV_en(x):           return (((x)&0x1)<<31)
def NPU_PCONV_shift(x):        return (((x)&0x1f)<<8)
def NPU_PCONV_pool(x):         return (((x)&0x1)<<6)
def NPU_PCONV_relu(x):         return (((x)&0x1)<<5)
def NPU_PCONV_bn(x):           return (((x)&0x1)<<4)
def NPU_PCONV_order(x):        return (((x)&0x7)<<0)
def NPU_BN_mode(x):            return (((x)&0xff)<<0)
def NPU_RELU_mode(x):          return (((x)&0x7)<<0)
def NPU_RELU6_clamp(x):        return (((x)&0xffffffff)<<0)
def NPU_POOL_zpad_r(x):        return (((x)&0x3)<<22)
def NPU_POOL_zpad_l(x):        return (((x)&0x3)<<20)
def NPU_POOL_zpad_b(x):        return (((x)&0x3)<<18)
def NPU_POOL_zpad_t(x):        return (((x)&0x3)<<16)
def NPU_POOL_stride(x):        return (((x)&0x3)<<8)
def NPU_POOL_size(x):          return (((x)&0x1)<<3)
def NPU_POOL_mode(x):          return (((x)&0x3)<<0)
def NPU_GAP_shift(x):          return (((x)&0x1f)<<16)
def NPU_GAP_mul(x):            return (((x)&0xffff)<<0)
def NPU_FORMAT_en(x):          return (((x)&0x1)<<31)
def NPU_FORMAT_uv_sub_128(x):  return (((x)&0x1)<<9)
def NPU_FORMAT_y_sub_128(x):   return (((x)&0x1)<<8)
def NPU_FORMAT_mode(x):        return (((x)&0xff)<<0)
def NPU_RESIZE_en(x):          return (((x)&0x1)<<31)
def NPU_RESIZE_restart(x):     return (((x)&0x1)<<30)
def NPU_RESIZE_SRC_offset(x):  return (((x)&0x3)<<0)
def NPU_RESIZE_DST_h(x):       return (((x)&0x1ff)<<16)
def NPU_RESIZE_DST_w(x):       return (((x)&0x1ff)<<0)
def NPU_RESIZE_RATIO_h(x):     return (((x)&0xffff)<<16)
def NPU_RESIZE_RATIO_w(x):     return (((x)&0xffff)<<0)
def NPU_CHAN_en(x):            return (((x)&0x1)<<31)
def NPU_CHAN_16b(x):           return (((x)&0x1)<<0)
def NPU_INTEN_err_inst(x):     return (((x)&0x1)<<11)
def NPU_INTEN_err_wt(x):       return (((x)&0x1)<<10)
def NPU_INTEN_err_config(x):   return (((x)&0x1)<<9)
def NPU_INTEN_err_ahb(x):      return (((x)&0x1)<<8)
def NPU_INTEN_int(x):          return (((x)&0xff)<<0)
def NPU_INT_err_inst(x):       return (((x)&0x1)<<11)
def NPU_INT_err_wt(x):         return (((x)&0x1)<<10)
def NPU_INT_err_config(x):     return (((x)&0x1)<<9)
def NPU_INT_err_ahb(x):        return (((x)&0x1)<<8)
def NPU_INT_int(x):            return (((x)&0xff)<<0)
def NPU_DBG0_m(x):             return (((x)&0xff)<<0)
def NPU_DBG1_step(x):          return (((x)&0x1)<<0)
def NPU_REGM_c(x):             return (((x)&0xffffffff)<<0)
def NPU_DUMMY_0_b(x):          return (((x)&0xffffffff)<<0)
def NPU_DUMMY_1_b(x):          return (((x)&0xffffffff)<<0)
def NPU_DUMMY_2_b(x):          return (((x)&0xffffffff)<<0)
