Bitstream/NPU mapping

** 2019/01/15 (NPU svn_r3415)
1. With npu_01071647_R3415.edf.gz
2. Bus clock change from 25MHz to 50MHz.
3. Camera MIPI RX/LCDC can work

** 2019/01/09 (NPU svn_r3415)
1.	Change NPU’s version to npu_01071647_R3415.edf.gz

** 2019/01/02 (NPU svn_r3324)
1.	Change NPU’s version to npu_01020924_R3324.edif.gz


** 2018/12/28 (NPU svn_r3173)
1.	Change NPU’s version to npu_12281022_R3173.edf.gz
2.	LCDC can work properly


** 2018/12/26 (NPU svn_r3017)
1.	Change NPU’s version to npu_12241328_R3017.edf.gz
2.	LCDC cannot work properly


** 2018/12/22 (NPU svn_r2581)
1.	Updated bus design from V013 to V015. 
	SCPU’s IMEM : 96KB , DMEM: 96KB.
	NCPU’s IMEM: 64KB , DMEM: 64K
2.	Change NPU’s version to npu_12201918_R2581.edf.gz
3.	DDR’s space increase to 1GB.
4.	SCPU only mode’s breakpoint cannot work properly, please change to daisy-chain mode and use SCPU to debug.


** 2018/12/12 (NPU svn_r1905)
Fixed the DDR3 fail on 12/7


** 2018/12/5 (NPU svn_r1828)
1. Change NPU’s version to npu_1205.edf.gz
2. Speed down for DDR’s clock
3. Please usb new DDR_init()

