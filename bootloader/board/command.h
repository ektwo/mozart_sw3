#ifndef __COMMAND_H__
#define __COMMAND_H__
#include "scu.h"

struct boot_ip_info {
	unsigned char cpu_type;
	char *ip_name[BOOT_IP_END];
	//short ip_strap[CONFIG_SCU_BOOTSTRAP_ENABLE_IP];
	short ip_strap[BOOT_IP_END];
	unsigned int total_enable_ip;
};
extern struct boot_ip_info boot_ip;

enum {
	CPU_FA_V5TE = 0,
	CPU_ARM_V7A,
	CPU_ARM_V8A,
};

int cmd_menu_get_selection(int list);
unsigned int spl_menu(unsigned int *img_addr);
void boot_ip_init(void);
unsigned int spl_ncpu(int mode, unsigned int *img_addr);
unsigned int cmd_max_transfer_size(unsigned int addr);
void cmd_show_boot_mode(int mode);
#endif
