#include <common.h>
#include <core/serial.h>
#include <drivers/drivers_common.h>
#include "command.h"

struct boot_ip_info boot_ip = {0,{0},{-1},0};

void boot_ip_init(void)
{
    boot_ip.total_enable_ip = CONFIG_SCU_BOOTSTRAP_ENABLE_IP;

#ifdef CONFIG_FTSPI020
    boot_ip.ip_name[BOOT_IP_SPI] = "SPI";
#endif
    ftscu_strap_assign();

#if 0
    int i;
    for(i=0;i<CONFIG_SCU_BOOTSTRAP_ENABLE_IP;i++)
            printf("scu_real_strap_idx[%d]=%d\n",i,boot_ip.ip_strap[i]);
#endif
}

#if 1
unsigned int spl_ncpu(int mode, unsigned int *img_addr)
{
    unsigned int img_size = 0;
    mem_booting_t bsp_img_info = {0};
    *img_addr = BSP_DL_BASE;
    
    bsp_img_info.io_type = BSP_IO_READ;
    bsp_img_info.mem_addr = BSP_DL_BASE; //0x28000000
    bsp_img_info.img_size = 64*1024;//BSP_SIZE; //4K
    _DSG(LOG_INFO, "%s: boot mode=%d BSP_DL_BASE=0x%x BSP_SIZE=0x%x",__func__, mode, BSP_DL_BASE, BSP_SIZE);
    switch(mode) {
#if 0//def CONFIG_FTSPI020
        case BOOT_IP_SPI: 
        case BOOT_IP_SPI_NAND: {
            ftscu_io_mode_pinmux(BOOT_IP_SPI);
//			if(mode == BOOT_IP_SPI_NAND)
//				bsp_img_info.dev_offset = NAND_BSP_BLOCK;	//0: block 0(SPL)
//			else
                bsp_img_info.dev_offset = 0x18000;//(8 + 88) * 1024;//1;	//0: block 0(SPL)
            //bsp_img_info.dev_offset = 0;
            img_size = spi020_booting(&bsp_img_info, mode);
            *img_addr = bsp_img_info.mem_addr;
            printf("%s: boot img_size=%d *img_addr=%d\n",__func__, img_size, *img_addr);
            }
            break;
#endif
        default:
            _DSG(LOG_INFO, "%s: Unknown BOOT mode selection(%d)",__FUNCTION__, mode);
    }

    return img_size;
}
#endif


int cmd_menu_get_selection(int list)
{
    char msg_char[3];
    int select;
    fLib_console_msg("Please select boot type[1-%d]: ",list);
    fLib_scanf(msg_char);
    select = atoi(msg_char);
    fLib_console_msg("\n");
    return select;
}

/**********************************/
unsigned int cmd_max_transfer_size(unsigned int addr)
{
    unsigned int bsp_dl_size = BSP_SIZE;
    bsp_dl_size -= (addr - BSP_DL_BASE);

    if(bsp_dl_size > BSP_SIZE)
        return 0;

    return bsp_dl_size;
}

void cmd_show_boot_mode(int mode)
{
    serial_puts("\nSPL MODE: ");
    if(mode == BOOT_IP_MANUAL_DEBUG)
        fLib_console_msg("Manual\n");
    else if(mode>=BOOT_IP_END)
        fLib_console_msg("Unknown\n");
    else
        fLib_console_msg("%s\n",boot_ip.ip_name[mode]);
}

// /**********************************/
unsigned int spl_menu(unsigned int *img_addr)
{
    unsigned char spl_write_fun[3] = {0};
    short i;
    unsigned int val;
    do {

        //for(i=0;i<boot_ip.total_enable_ip;i++) {
        for(i=1;i<boot_ip.total_enable_ip;i++) {
    
            val = boot_ip.ip_strap[i];
            //fLib_console_msg("%d. %s\n", i+1, boot_ip.ip_name[val]);
            fLib_console_msg("%d. %s\n", i, boot_ip.ip_name[val]);
        }

        val = cmd_menu_get_selection(boot_ip.total_enable_ip-1);
        _DSG(LOG_INFO, " val=%x", val);
    }while((val<1) || (val>i));
    
    {
        //val--;
        if((boot_ip.ip_strap[val]>=0) && (boot_ip.ip_strap[val]<BOOT_IP_END)) {
            _DSG(LOG_INFO, "%s: spl bootting selection %d : %d",__func__, val, boot_ip.ip_strap[val]);
            i = boot_ip.ip_strap[val];
        }
        else
            i = -1;
    }
    //printf(" i=%x \n", i);
    if(i>=0) {
        //val = spl_ncpu(i, img_addr);
    }
    else {
        val = 0;
    }
    return val;
}

