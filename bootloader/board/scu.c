#include <common.h>
#include "scu.h"
#include "command.h"
#ifdef CONFIG_FTTMR010
#include <drivers/FTTMR010/fttmr010.h>
#endif
#ifdef CONFIG_SYS_TIMER
#include <core/sys_timer.h>
#endif

extern struct boot_ip_info boot_ip;

/**********************************/
int ftscu_bootstrap_boot_mode(void)
{
	int val;
	val = (SCU_REG32(SCU_BOOTSTRAP_REG)>>SCU_BOOTSTRAP_SHIFT) & SCU_BOOTSTRAP_BIT;
	_DSG(LOG_INFO, "%s: strap pin val = %d boot_ip.ip_strap[val]=%d",__FUNCTION__, val, boot_ip.ip_strap[val]);
	if((val <= boot_ip.total_enable_ip) && (val>0))
		return boot_ip.ip_strap[val];

	return BOOT_IP_MANUAL_DEBUG;
}

/*	We default set the boot stap mapping: 0: Manual, 1~: SCU defined
*	So I need boot_ip.ip_strap[BOOT_IP_END] to convert between strap & code defined IP.
*
*		Real Strap		Code Defined
*	0	Manual		0	UART(Xmodem)
*	1~	real strap		1	USB
*					2	SD
*					3	SPI
*					4	NAND
*					5	NOR write
*/

extern struct boot_ip_info boot_ip;

void ftscu_strap_assign(void)
{
	boot_ip.ip_strap[0] = BOOT_IP_MANUAL_DEBUG;//BOOT_IP_SPI;//BOOT_IP_UART;//BOOT_IP_MANUAL_DEBUG;
#ifdef CONFIG_SCU_BOOT_UART
	boot_ip.ip_strap[CONFIG_SCU_BOOT_UART] = BOOT_IP_UART;	//BOOT_IP_UART
#endif

#ifdef CONFIG_SCU_BOOT_SPI
	boot_ip.ip_strap[CONFIG_SCU_BOOT_SPI] = BOOT_IP_SPI;	//BOOT_IP_SPI
#endif

#ifdef CONFIG_SPI020_NAND_FLASH
	boot_ip.ip_strap[CONFIG_SCU_BOOT_SPI_NAND] = BOOT_IP_SPI_NAND;	//BOOT_IP_SPI_NAND
#endif

#ifdef CONFIG_SCU_BOOT_NAND
	boot_ip.ip_strap[CONFIG_SCU_BOOT_NAND] = BOOT_IP_NAND;	//BOOT_IP_NAND
#endif
}
/**********************************/
void ftscu_io_mode_pinmux(int type)
{
	unsigned val = 0;
	switch(type) {
#ifdef CONFIG_NANDC024_PINMUX_ENABLE
		case BOOT_IP_NAND:
			val = (1<<CONFIG_SCU_NAND_PINMUX_BITS1+1)-1;
			SCU_REG32(SCU_NAND_PINMUX_REG1)  &=~val;
			SCU_REG32(SCU_NAND_PINMUX_REG1) |= SCU_NAND_PINMUX_VAL1;
		#if (CONFIG_SCU_NAND_PINMUX_REG_NUM == 2)
			val = (1<<CONFIG_SCU_NAND_PINMUX_BITS2+1)-1;
			SCU_REG32(SCU_NAND_PINMUX_REG2)  &=~val;
			SCU_REG32(SCU_NAND_PINMUX_REG2) |= SCU_NAND_PINMUX_VAL2;
		#endif
			break;
#endif
#ifdef CONFIG_SPI020_PINMUX_ENABLE
		case BOOT_IP_SPI:
		case BOOT_IP_SPI_NAND:
			val = (1<<CONFIG_SCU_SPI_PINMUX_BITS1+1)-1;
			SCU_REG32(SCU_SPI_PINMUX_REG1)  &=~val;
			SCU_REG32(SCU_SPI_PINMUX_REG1) |= SCU_SPI_PINMUX_VAL1;
		#if (CONFIG_SCU_SPI_PINMUX_REG_NUM == 2)
			val = (1<<CONFIG_SCU_SPI_PINMUX_BITS2+1)-1;
			SCU_REG32(SCU_SPI_PINMUX_REG2)  &=~val;
			SCU_REG32(SCU_SPI_PINMUX_REG2) |= SCU_SPI_PINMUX_VAL2;
		#endif
			break;
#endif
		default:
			_DSG(LOG_INFO, "%s: We don't support this IP(%d) in pin mux mode",__FUNCTION__, type);
	}
}

#if (CONFIG_NANDC_INFO_STRAP_PAGE  || CONFIG_SPI020_NAND_FLASH)
unsigned int ftscu_get_nand_page_size(void)
{
	unsigned int val;
	val = (SCU_REG32(SCU_NAND_PAGE_REG)>>SCU_NAND_PAGE_SHIFT) & SCU_NAND_PAGE_MASK_VAL;
	return val;
}
#endif

#if (CONFIG_NANDC_INFO_STRAP_BLOCK || CONFIG_SPI020_NAND_FLASH)
int ftscu_get_nand_block_size(void)
{
	int val;
	val = (SCU_REG32(SCU_NAND_BLOCK_REG)>>SCU_NAND_BLOCK_SHIFT) & SCU_NAND_BLOCK_MASK_VAL;
	return val;
}
#endif

#ifdef CONFIG_NANDC_INFO_STRAP_ADDR_CYCLE
int ftscu_get_nand_address_cycle(void)
{
	int val, bits_shift = 0x3;
#if CONFIG_NANDC_INFO_STRAP_ADDR_CYCLE_FIX_COL
	bits_shift = 0x1;
#endif
	val = (SCU_REG32(SCU_NAND_ADDR_CYCLE_REG)>>SCU_NAND_ADDR_CYCLE_SHIFT) & bits_shift;
	return val;
}
#endif


#ifdef CONFIG_NANDC_BI_INFO_STRAP
int ftscu_get_nand_bi_info(void)
{
	int val;
	val = (SCU_REG32(SCU_NANDC_BI_STRAP_REG)>>SCU_NANDC_BI_STRAP_SHIFT) & SCU_NANDC_BI_STRAP_MASK_VAL;
	return val;
}
#endif

#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
int ftscu_get_spi_addr_len(void)
{
	int val;
	val = (SCU_REG32(SCU_SPI_ADDR_LEN_REG)>>SCU_SPI_ADDR_LEN_SHIFT) & 0x1;
	return val;
}
#endif

