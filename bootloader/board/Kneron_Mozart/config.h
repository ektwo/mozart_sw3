#ifndef _CONFIG_H_
#define _CONFIG_H_
#include <autoconf.h>
#include "spec.h"
#include "fwimage.h"

#define BSP_DL_BASE			CONFIG_TEXT_BASE_BSP
#define BSP_SIZE			CONFIG_BSP_SIZE
#define BSP_CONFIG_BASE		CONFIG_BSP_CONFIG_BASE
#define SMP_HODING_PEN_ADDR	CONFIG_DATA_BASE

/* Serial */ 	 
#define CONFIG_SERIAL_BASE          	FTUART010_REG_BASE
#define CONFIG_SERIAL_CLOCK         	CONFIG_UART_CLK


#endif
