#include <common.h>
#include <core/serial.h>
#include <drivers/drivers_common.h>
#include "scu.h"
#include "command.h"
#include <stdio.h>
#include "kneron_mozart.h"
#include "pwmtimer.h"

#define DDR_BASE                    0xC3200000
#define SCU_EXTREG_PA_BASE          0xC2380000
#define SCU_FTSCU100_PA_BASE        0xC2300000

#define SCPU_BIN_OFFSET_IN_FLASH    0x2000
#define NCPU_BIN_OFFSET_IN_FLASH    0x18000

#define SCPU_START_ADDRESS          0x10102000
#define NCPU_START_ADDRESS          0x28000000

#define SCPU_IMAGE_SIZE             0x16000
#define NCPU_IMAGE_SIZE             0x10000



#ifdef TEST_TIME   
extern UINT32 T1_Tick;
extern void gpio_test_start(void);
extern void gpio_test_end(void);
#endif
extern struct boot_ip_info boot_ip;

typedef struct
{
  volatile  UINT32 CPUID;                   /*!< Offset: 0x000 (R/ )  */
  volatile  UINT32 ICSR;                    /*!< Offset: 0x004 (R/W)  */
  volatile  UINT32 VTOR;                    /*!< Offset: 0x008 (R/W)  */
  volatile  UINT32 AIRCR;                   /*!< Offset: 0x00C (R/W)  */
} SCB_Type;

#define SCS_BASE                    (0xE000E000UL)
#define SCB_BASE                    (SCS_BASE +  0x0D00UL)
#define SCB                         ((SCB_Type *) SCB_BASE)
#define NVIC_BASE           		(SCS_BASE +  0x0100UL)                    /*!< NVIC Base Address */
#define NVIC                		((NVIC_Type      *)     NVIC_BASE     )   /*!< NVIC configuration struct */

#define __OM    volatile            /*! Defines 'write only' structure member permissions */
#define  __IOM  volatile            /*! Defines 'read / write' structure member permissions */

typedef int int32_t;
typedef unsigned char uint8_t;
typedef unsigned int uint32_t;

#define NVIC_USER_IRQ_OFFSET          16
typedef struct
{
  __IOM uint32_t ISER[8U];               /*!< Offset: 0x000 (R/W)  Interrupt Set Enable Register */
        uint32_t RESERVED0[24U];
  __IOM uint32_t ICER[8U];               /*!< Offset: 0x080 (R/W)  Interrupt Clear Enable Register */
        uint32_t RESERVED1[24U];
  __IOM uint32_t ISPR[8U];               /*!< Offset: 0x100 (R/W)  Interrupt Set Pending Register */
        uint32_t RESERVED2[24U];
  __IOM uint32_t ICPR[8U];               /*!< Offset: 0x180 (R/W)  Interrupt Clear Pending Register */
        uint32_t RESERVED3[24U];
  __IOM uint32_t IABR[8U];               /*!< Offset: 0x200 (R/W)  Interrupt Active bit Register */
        uint32_t RESERVED4[56U];
  __IOM uint8_t  IP[240U];               /*!< Offset: 0x300 (R/W)  Interrupt Priority Register (8Bit wide) */
        uint32_t RESERVED5[644U];
  __OM  uint32_t STIR;                   /*!< Offset: 0xE00 ( /W)  Software Trigger Interrupt Register */
}  NVIC_Type;

inline void NVIC_SetVector(IRQn_Type IRQn, uint32_t vector)
{
    uint32_t vectors = (uint32_t )SCB->VTOR;
    (* (int *) (vectors + ((int32_t)IRQn + NVIC_USER_IRQ_OFFSET) * 4)) = vector;
}

inline void NVIC_EnableIRQ(IRQn_Type IRQn)
{
    if ((int32_t)(IRQn) >= 0)
    {
        NVIC->ISER[(((uint32_t)IRQn) >> 5UL)] = (uint32_t)(1UL << (((uint32_t)IRQn) & 0x1FUL));
    }
}

void NVIC_SetVectorTable(unsigned long NVIC_VectTab, unsigned long Offset)
{
    SCB->VTOR = NVIC_VectTab | (Offset & (unsigned int)0x1FFFFF80);
}

inline int board_jumpstart(UINT32 addr)
{
    printf("jump to 0x%08X, [0x%08X]\n", (UINT32)addr, *(UINT32 *)addr);

    __asm__ __volatile__ (
        "bx %0\n"
        :                       /* output */
        : "r"(addr)             /* input */
        : "memory"              /* clobber list */
    );

    return 0;
}

void spl_ip_init(void)
{
    
    boot_ip_init();
}

void sys_init(void)
{
    int val;
    UINT32    data;

    outw(SCU_FTSCU100_PA_BASE + 0x00,0x00010000);
    outw(SCU_FTSCU100_PA_BASE + 0x04,0x00030001);

    //power_mgr_set_domain(power_domain_all, TRUE);  
    //power_mgr_wait_domain_ready(power_domain_all);
        outw(SCU_FTSCU100_PA_BASE + 0x08,(inw(SCU_FTSCU100_PA_BASE + 0x08)|0x01000700));
        do{
            val = inw(SCU_FTSCU100_PA_BASE + 0x48);
        }while((val & 0xFF) != 0x7);
        do{
            val = inw(SCU_EXTREG_PA_BASE + 0xB0);
        }while((val & 0xF0) != 0x70);

   //pll enable on ASIC ++++++++++++++
   // Pll1 en setting
   data = inw(SCU_FTSCU100_PA_BASE+0x00080000+0x004);
   outw( (SCU_FTSCU100_PA_BASE+0x00080000+0x004) , data | 0x00000001 );

   idlen(100, 3000);
   // Pll1 out_en setting
   data = inw(SCU_FTSCU100_PA_BASE+0x00080000+0x014);
   outw( (SCU_FTSCU100_PA_BASE+0x00080000+0x014) , data | 0x00000002 );


   // pll2, Pll3, pll5 fref_en setting
   //HAL_READ_UINT32(SCU_FTSCU100_PA_BASE+0x00080000+0x014, data);
   //WRITE( (SCU_FTSCU100_PA_BASE+0x00080000+0x014) , data | 0x00001800 );
   idlen(100, 3000);
   // Pll2, pll3, pll5 en setting
   data = inw(SCU_FTSCU100_PA_BASE+0x00080000+0x00C);
   outw( (SCU_FTSCU100_PA_BASE+0x00080000+0x00C) , data | 0x00000001 );
   data = inw(SCU_FTSCU100_PA_BASE+0x00080000+0x008);
   outw( (SCU_FTSCU100_PA_BASE+0x00080000+0x008) , data | 0x00000001 );
   data = inw(SCU_FTSCU100_PA_BASE+0x00080000+0x03C);
   outw( (SCU_FTSCU100_PA_BASE+0x00080000+0x03C) , data | 0x00000001 );

   idlen(100, 3000);
   // Pll2, pll3, pll5 out_en setting
   data = inw(SCU_FTSCU100_PA_BASE+0x00080000+0x014 );
   outw( (SCU_FTSCU100_PA_BASE+0x00080000+0x014) , data | 0x00000334 );



   // For fcs function to gating pll4(from pll0)
   idlen(100, 200);
   //pll4 out_en setting
   data = inw(SCU_FTSCU100_PA_BASE+0x00080000+0x014);
   outw( (SCU_FTSCU100_PA_BASE+0x00080000+0x014) , data & 0xffffffbf );
   
   idlen(100, 200);
   //pll4 en setting
   data = inw(SCU_FTSCU100_PA_BASE+0x00080000+0x010);
   outw( (SCU_FTSCU100_PA_BASE+0x00080000+0x010) , data & 0xfffffffe );     
     
   idlen(100, 200);
   //pll4 fref_en setting
   data = inw(SCU_FTSCU100_PA_BASE+0x00080000+0x014);
   outw( (SCU_FTSCU100_PA_BASE+0x00080000+0x014) , data & 0xffffe7ff );

   //pll enable-----------------------

#if 1 //ddr
    outw(SCU_EXTREG_PA_BASE + 0x80, (inw(SCU_EXTREG_PA_BASE + 0x80) & ~0x10000000)); //DDRC 
    int i= 50000;
    while(i--);
#endif
    outw(SCU_EXTREG_PA_BASE + 0x14, (inw(SCU_EXTREG_PA_BASE + 0x14) | 0x00400000)); // NCPU fclk src en setting
    outw(SCU_EXTREG_PA_BASE + 0x18, (inw(SCU_EXTREG_PA_BASE + 0x18) | 0x00800000)); // Enable npu clk_en
    outw(SCU_EXTREG_PA_BASE + 0x4c, (inw(SCU_EXTREG_PA_BASE + 0x4c) | 0x00800002)); // Enable PD_NPU_reset_n
    outw(SCU_EXTREG_PA_BASE + 0x4c, (inw(SCU_EXTREG_PA_BASE + 0x4c) | 0x00800004)); // Enable NPU_reset_n

    outw(SCU_EXTREG_PA_BASE + 0x80, (inw(SCU_EXTREG_PA_BASE + 0x80) | 0x10000000)); // wakeup DDRC 

}

void load_ncpu(int mode, unsigned int flash_offset, unsigned int img_addr, unsigned int img_size)
{
#if 1
    mem_booting_t bsp_img_info = {0};

    bsp_img_info.io_type = BSP_IO_READ;
    bsp_img_info.mem_addr = img_addr;
    bsp_img_info.img_size = img_size;
    //printf("%s: boot mode=%d img_addr=0x%x img_size=%d\n",__func__, mode, img_addr, img_size);
    ftscu_io_mode_pinmux(BOOT_IP_SPI);
    bsp_img_info.dev_offset = flash_offset;
    img_size = spi020_booting(&bsp_img_info, mode);

    //outw(SCU_EXTREG_PA_BASE + 0x68, (inw(SCU_EXTREG_PA_BASE + 0x68) | 0x00001000)); // wakeup NCPU
#endif
}

void load_scpu(int mode, unsigned int flash_offset, unsigned int img_addr, unsigned int img_size)
{
    mem_booting_t bsp_img_info = {0};

    bsp_img_info.io_type = BSP_IO_READ;
    bsp_img_info.mem_addr = img_addr;
    bsp_img_info.img_size = img_size;
    //printf("%s: boot mode=%d img_addr=0x%x img_size=%d\n",__func__, mode, img_addr, img_size);
    ftscu_io_mode_pinmux(BOOT_IP_SPI);
    bsp_img_info.dev_offset = flash_offset;
    img_size = spi020_booting(&bsp_img_info, mode);
}

void post_main(unsigned int scpu_start_addr) {
    int i;
    void (*user_code_entry)(void);
    UINT32 *user_code_addr;
    //printf(" << scpu_start_addr=0x%x >> \n",scpu_start_addr);
    NVIC_SetVectorTable(0x0, scpu_start_addr);
    user_code_addr = *(UINT32 *) (scpu_start_addr + 4);
    user_code_entry=user_code_addr;

    // jumps to new position of vector class
    user_code_entry();

    while (1);
}

void start_main (void)
{
    int i, val;

    sys_init();

    serial_init();
#ifdef TEST_TIME    
    fLib_Timer_Init(DRVPWMTMR1, PWMTMR_1MSEC_PERIOD);    
    
    gpio_test_init();

    gpio_test_start();
#endif
    boot_ip.cpu_type = CPU_ARM_V7A;

    spl_ip_init();

    val = ftscu_bootstrap_boot_mode();
    //printf(" val=%x \n", val);
    cmd_show_boot_mode(val);

    //if (BOOT_IP_MANUAL_DEBUG == val)
    //    spl_menu(NULL);//&img_addr);
    // int i,j=0, count = 5000000000;
    // for (i = 0; i < 100; i++)
    //     printf(" ############%d\n", i);
    // printf(" ############# \n");        
    // printf(" # load ncpu # \n");
    // printf(" ############# \n");

    load_ncpu(BOOT_IP_SPI, NCPU_BIN_OFFSET_IN_FLASH, NCPU_START_ADDRESS, NCPU_IMAGE_SIZE);

    load_scpu(BOOT_IP_SPI, SCPU_BIN_OFFSET_IN_FLASH, SCPU_START_ADDRESS, SCPU_IMAGE_SIZE);

#ifdef TEST_TIME
    gpio_test_end();
#endif
    post_main(SCPU_START_ADDRESS);
}
