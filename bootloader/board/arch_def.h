#include <autoconf.h>

#ifdef CONFIG_CPU_ARMv8
#define PLATFORM_LINKER_FORMAT		"elf64-littleaarch64"
#define PLATFORM_LINKER_ARCH			aarch64
#define PLATFORM_ALIGN					8
#define PLATFORM_DATA_ALIGN			16
#else
#define PLATFORM_LINKER_FORMAT		"elf32-littlearm"
#define PLATFORM_LINKER_ARCH			arm
#define PLATFORM_ALIGN					4
#define PLATFORM_DATA_ALIGN			8
#endif
