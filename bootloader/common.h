#ifndef _COMMON_H
#define _COMMON_H
#include <autoconf.h>
#include <bsp/config.h>
#include <config.h>
#include <core/cpu/sysdep.h>
#include <core/system.h>
#include <stdio.h>
#include <core/serial.h>


#ifdef __GNUC__
#define __packed        __attribute__((__packed__))
//#define __align(x)        __attribute__((__align(x)__))
#define __align(x)
#endif
#define __weak				__attribute__((weak))


int board_jumpstart(UINT32 addr);

/* Platform-Dependent Driver */
#ifdef CONFIG_ENABLE_PDD
#ifdef CONFIG_NANDC_INFO_STRAP_ECC
unsigned char pdd_ftscu_get_nand_ecc(BOOL is_sp);
#endif
int pdd_main(void);
#else
static inline int pdd_main(void) {return 0;};
#endif
unsigned int pdd_menu(unsigned int *err_code);


#if (CONFIG_ENABLE_PDD && CONFIG_SPI_WREN_INFO_STRAP)
unsigned char pdd_ftscu_get_spi_need_wren(void);
#else
static inline unsigned char pdd_ftscu_get_spi_need_wren(void) {return FALSE;}
#endif

extern UINT32 _TEXT_BASE;
extern UINT32 _bss_start;
extern UINT32 _bss_end;
extern UINT32 erodata;
extern UINT32 srwdata;
extern UINT32 erwdata;


/* This is the definition of boot code write into  rom/flash... etc use. */
enum {
	BSP_IO_READ = 1,
	BSP_IO_WRITE,
};
#define LOG_INFO 3
#define LOG_TRACE 4
#define fLib_putchar serial_putc
#ifdef CONFIG_SHOW_DEBUG_MESSAGE
#define fLib_printf_0 printf
#define printf	fLib_printf
#define _DSG(__flag__, __format__, ...) { if (__flag__ == LOG_TRACE) fLib_printf(__format__"\n", ##__VA_ARGS__); }
#else
#define fLib_printf_0(args...)
#define printf(args...)
#endif
#endif
