#ifndef __DRIVERS_COMMON_H__
#define __DRIVERS_COMMON_H__
#include <common.h>

typedef struct{
	UINT8 io_type;
	UINT32 mem_addr;
	UINT32 dev_offset;
	UINT32 img_size;
}mem_booting_t;

unsigned int ftnandc024_boot(mem_booting_t *mem_boot_info);
unsigned int fotg210_init(UINT32 addr, UINT32 max_dl_size);
unsigned int sd_mmc_boot(mem_booting_t *bsp_img_info, int mode);
unsigned int spi020_booting(mem_booting_t *mem_boot_info, int flash_type);
void ftsmc030_main(unsigned int addr, unsigned int size);
unsigned int xmodem_boot(mem_booting_t *img_info, UINT32 max_dl_size);
#endif
