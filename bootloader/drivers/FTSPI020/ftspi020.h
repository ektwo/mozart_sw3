#ifndef __FTSPI020_H__
#define __FTSPI020_H__
#include <common.h>

#define SPI020_REG32(offset)	*((volatile UINT32 *)(FTSPI020_REG_BASE + (offset)))
#define SPI020_REG8(offset)		*((volatile UINT8  *)(FTSPI020_REG_BASE + (offset)))

#define SPI020_SECTOR_SIZE		4096
#define SPI020_BLOCK_64SIZE		65536

#define min_t(x,y) ( x < y ? x: y )

/* Register*/
#define	FTSPI020_REG_CMD0		0x00	/* Flash address */
#define	FTSPI020_REG_CMD1		0x04
#define	FTSPI020_REG_CMD2		0x08
#define	FTSPI020_REG_CMD3		0x0c
#define	FTSPI020_REG_CTRL		0x10	/* Control */
#define	FTSPI020_REG_AC_TIME	0x14
#define	FTSPI020_REG_STS		0x18	/* Status */
#define	FTSPI020_REG_ICR		0x20	/* Interrupt Enable */
#define	FTSPI020_REG_ISR		0x24	/* Interrupt Status */
#define	FTSPI020_REG_READ_STS	0x28
#define FTSPI020_REG_REVISION	0x50
#define FTSPI020_REG_FEATURE	0x54
#define FTSPI020_REG_DATA_PORT	0x100

#define SPI_DATA_READ		BSP_IO_READ
#define SPI_DATA_WRITE		BSP_IO_WRITE

/* Command 1 */ 
/* bit[2:0] */
enum {
	SPI_ADDR_LEN_NONE = 0,
	SPI_ADDR_LEN_1BYTE,
	SPI_ADDR_LEN_2BYTE,
	SPI_ADDR_LEN_3BYTE,
	SPI_ADDR_LEN_4BYTE,
};

/* bit[1:0] divider */
enum {
	SPI_CLK_DIV_2 = 0,
	SPI_CLK_DIV_4,
	SPI_CLK_DIV_6,
	SPI_CLK_DIV_8
};

enum {
	SPI_NOR_FLASH = 0,
	SPI_NAND_FLASH
};

// bit0
#define cmd_complete_intr_disable		0
#define cmd_complete_intr_enable		1
// bit1
#define spi_read						0
#define spi_write						1
// bit2
#define read_status_disable				0
#define read_status_enable				1
// bit3
#define read_status_by_hw				0
#define read_status_by_sw				1
// bit4
#define dtr_disable						0
#define dtr_enable						1
// bit[7:5]
#define spi_operate_serial_mode			0
#define spi_operate_dual_mode			1
#define spi_operate_quad_mode			2
#define spi_operate_dualio_mode			3
#define spi_operate_quadio_mode			4
// bit[9:8]
#define	start_ce0						0
#define	start_ce1						1
#define	start_ce2						2
#define	start_ce3						3
// bit[23:16] op_code info

// bit[31:24] instr index


// bit 4 mode
#define mode0							0
#define mode3							1
// bit 8 abort
// bit 9 wp_en
// bit[18:16] busy_loc
#define BUSY_BIT0						0
#define BUSY_BIT1						1
#define BUSY_BIT2						2
#define BUSY_BIT3						3
#define BUSY_BIT4						4
#define BUSY_BIT5						5
#define BUSY_BIT6						6
#define BUSY_BIT7						7


#define cmd_complete					(1 << 0)
#define rx_fifo_underrun				(1 << 1)
#define tx_fifo_overrun					(1 << 2)
//#define tx_threshold					(1 << 4)
//#define rx_threshold					(1 << 8)


#define SPI_READ_STATUS					0x0028
#define TX_DATA_CNT						0x0030
#define RX_DATA_CNT						0x0034
#define REVISION						0x0050
#define FEATURE							0x0054

#define SPI020_DATA_PORT				0x0100



#define TARGET_CE				start_ce0

#ifdef CONFIG_SPI_ENABLE_BACKUP
#define SPI_SPL_HARDCOPY_NUM	CONFIG_SPI_BACKUP_NUM
#else
#define SPI_SPL_HARDCOPY_NUM	1
#endif

#define SPI_MFID_SPANSION		0x01
#define SPI_MFID_EON			0x1c
#define SPI_MFID_MICRON     	0x20	/* STMicroelectronics */

#define SPI_NOR_EN4B_STS_BIT	0x1
#define SPI_NOR_ENWR_STS_BIT	0x10
struct ftspi020_fop_info {
	unsigned char addr_len;
	unsigned char flash_type;
#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
	unsigned char flash_id; 	/* only vaild in address length: 4-byte mode */
	unsigned char enable_4byte_mode;	/* [0]:En/Disable 4byte cmd, [4]:need wren cmd */
#endif
#ifdef CONFIG_IMG_CHK_SUM
	unsigned int img_chksum;	/* only vaild in CONFIG_IMG_CHK_SUM is been ENABLE */
#endif
	unsigned int img_size;
	//unsigned int block_size;
	volatile UINT32 *dl_addr;
};

struct ftspi020_cmd_info {
	UINT32 spi_addr;
	UINT8 addr_len;
	UINT8 dum_2nd_cyc;
	UINT32 data_counter;
	UINT8 ins_code;
	UINT8 rd_status;
	UINT8 rd_status_en;
	UINT8 write_en;
	UINT8 op_mode;
};

struct ftspi020_nand_flash_info {
	/*UINT8 info_source;	//the flash information comes from...header of strap*/
	UINT16 page_size;
	UINT16 num_page_in_block;
};

void ftspi020_send_command(struct ftspi020_cmd_info *cmd);
void ftspi020_wait_cmd_done(void);
void ftspi020_data_fop(UINT32 *buf, UINT32 len, UINT8 rw);

#endif
