#include <common.h>
#include <drivers/drivers_common.h>
#include "ftspi020.h"
#include "bsp_spi.h"

#define BSP_SPI_CONF_DEF_SIZE	4*1024

unsigned int bsp_spi_io_read(mem_booting_t *boot_info, struct ftspi020_fop_info *fop)
{
	fop->dl_addr = (UINT32 *)boot_info->mem_addr;
	fop->img_size = boot_info->img_size;
	//16384, 65536, 16384
	_DSG(LOG_INFO, "bsp_spi_io_read dev_off=%d", boot_info->dev_offset);
	//ftspi020_read_data(boot_info->dev_offset* fop->block_size, fop);
	ftspi020_read_data(boot_info->dev_offset, fop);
	return fop->img_size;
}

