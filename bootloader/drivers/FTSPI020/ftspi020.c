#include <board/scu.h>
#include <drivers/drivers_common.h>
#include "ftspi020.h"
#include "spi_nor.h"

typedef INT8          INT8S;
typedef UINT8         INT8U;
typedef INT16         INT16S;
typedef UINT16        INT16U;
typedef INT32         INT32S;
typedef UINT32        INT32U;

#define vLib_LeWrite32(x,y)   *(volatile INT32U *)((INT8U * )x)=(y)  //bessel:add  (INT8U * )
#define u32Lib_LeRead32(x)      *((volatile INT32U *)((INT8U * )x)) //bessel:add  (INT8U * )
#define SPI020REG_CMD0      (CONFIG_FTSPI020_REG_BASE+FTSPI020_REG_CMD0)
#define SPI020REG_CMD1      (CONFIG_FTSPI020_REG_BASE+FTSPI020_REG_CMD1)
#define SPI020REG_CMD2      (CONFIG_FTSPI020_REG_BASE+FTSPI020_REG_CMD2)
#define SPI020REG_CMD3      (CONFIG_FTSPI020_REG_BASE+FTSPI020_REG_CMD3)
#define SPI020REG_INTR_ST   (CONFIG_FTSPI020_REG_BASE+FTSPI020_REG_ISR)
#define BIT0                0x00000001
#define SPI020_CMD_CMPLT    BIT0
#define SPI020_CE_0         0x0000
#define SPI020_CE_1         0x0100
#define SPI020_CE_2         0x0200
#define SPI020_CE_3         0x0300
#define SPI020_CE_VALUE     SPI020_CE_0
#define SPI020_INTR_CFG     0x00000000
#define SPI020_B7_CMD0      0x0

/*******************************************
 * set 4Bytes command (0xB7)
 ********************************************/
/* Set command word 0, set SPI flash address to 0 */
#define SPI020_B7_CMD0      0x0
/* Set command word 1, continue read = 0, instruction leng = 1,
   dum_2nd_cyc = 0, dum_1st_cyc = 0, address length = 0 */
#define SPI020_B7_CMD1      0x01000000
/* Set command word 2, set data count to 0 */
#define SPI020_B7_CMD2      0x0
/* Set command word 4, instrction code = 0x06, contiune read = 0,
   start_ce = ??, spi mode = 0, DTR mode = 0, status = 0,
   status_en = 0, write enable = 1, intr_en = ? */
#define SPI020_B7_CMD3      (0xB7000002|SPI020_CE_VALUE|SPI020_INTR_CFG)
/*******************************************
 * Exit 4Bytes command (0xE9)
 ********************************************/
/* Set command word 0, set SPI flash address to 0 */
#define SPI020_E9_CMD0      0x0
/* Set command word 1, continue read = 0, instruction leng = 1,
   dum_2nd_cyc = 0, dum_1st_cyc = 0, address length = 0 */
#define SPI020_E9_CMD1      0x01000000
/* Set command word 2, set data count to 0 */
#define SPI020_E9_CMD2      0x0
/* Set command word 4, instrction code = 0x06, contiune read = 0,
   start_ce = ??, spi mode = 0, DTR mode = 0, status = 0,
   status_en = 0, write enable = 1, intr_en = ? */
#define SPI020_E9_CMD3      (0xE9000002|SPI020_CE_VALUE|SPI020_INTR_CFG)


/**********************************/
#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
int ftspi020_get_addr_len(void)
{
    int addr_len = SPI_ADDR_LEN_3BYTE;

    if(ftscu_get_spi_addr_len())
        addr_len = SPI_ADDR_LEN_4BYTE;
    else
        addr_len = SPI_ADDR_LEN_3BYTE;

    return SPI_ADDR_LEN_4BYTE;//addr_len;
}
#else
static inline int ftspi020_get_addr_len(void) {return SPI_ADDR_LEN_3BYTE;};
#endif

void ftspi020_read_data(unsigned int offset, struct ftspi020_fop_info *fop)
{
    {
        _DSG(LOG_INFO, "offset=%x, size=%d",offset, fop->img_size);
        ftspi020_read(offset, fop->img_size, fop);
    }
}
/**********************************/

/*	In this SPI code, we only use PIO mode */
BOOL ftspi020_data_ready(UINT8 rw)
{
    {
        return ((SPI020_REG32(FTSPI020_REG_STS)&0x2)>>1);
    }
}

UINT32 ftspi020_fifo_depth(UINT8 rw)
{
    {
        return (((SPI020_REG32(FEATURE)>>8) & 0xFF) << 2);
    }
}

void ftspi020_wait_cmd_done(void)
{
    while(1) {
        if(SPI020_REG32(FTSPI020_REG_ISR)&0x1)
            break;
    }
    SPI020_REG32(FTSPI020_REG_ISR) = 0x1;	//clear command complete intr status
}

/*	We waana reduce code size, so only handle word alignment data.
*/
void ftspi020_data_fop(UINT32 *buf, UINT32 len, UINT8 rw)
{
    UINT32 access_byte;

    /*	To reduce code size, we only implement word alignment code. */
    access_byte = len & 0x3;
    if(access_byte)
        len += (0x4 - access_byte);

    while(len > 0) {
        while(!ftspi020_data_ready(rw));
        access_byte = min_t(len, ftspi020_fifo_depth(rw));
        len -= access_byte;
        while(access_byte > 0) {
            {
                *buf= SPI020_REG32(FTSPI020_REG_DATA_PORT);
                //printf("len=%d, addr=%x,data=%x\n",len, buf,*buf);
            }
            buf ++;
            if(access_byte>=4)
                access_byte -= 4;
            else
                access_byte=0;
        }
        //printf("len =%d\n",len);
    }
}

void ftspi020_send_command(struct ftspi020_cmd_info *cmd)
{
    unsigned int cmd1, cmd3;
    cmd1 = 1<<24|(cmd->dum_2nd_cyc<<16) | cmd->addr_len;	//1<<24: 1 byte instruction code
    cmd3 = (cmd->ins_code<<24) | (TARGET_CE<<8)|(cmd->op_mode<<5) | (cmd->rd_status<<3)|
        (cmd->rd_status_en<<2)|(cmd->write_en<<1);
    _DSG(LOG_INFO, "cmd %x, %x, %x, %x",cmd->spi_addr, cmd1,cmd->data_counter,cmd3);
    SPI020_REG32(FTSPI020_REG_CMD0) = cmd->spi_addr;
    SPI020_REG32(FTSPI020_REG_CMD1) = cmd1;
    SPI020_REG32(FTSPI020_REG_CMD2) = cmd->data_counter;
    SPI020_REG32(FTSPI020_REG_CMD3) = cmd3;
}

void spi020_set_commands(UINT32 cmd0, UINT32 cmd1, UINT32 cmd2, UINT32 cmd3)
{
    vLib_LeWrite32((INT8U * )SPI020REG_CMD0, cmd0);
    vLib_LeWrite32((INT8U * )SPI020REG_CMD1, cmd1);
    vLib_LeWrite32((INT8U * )SPI020REG_CMD2, cmd2);
    vLib_LeWrite32((INT8U * )SPI020REG_CMD3, cmd3);
}
/* Wait until command complete */
void spi020_wait_command_complete(void)
{
    UINT32  reg;

    do {
        reg = u32Lib_LeRead32((INT8U * )SPI020REG_INTR_ST);
    } while ((reg & SPI020_CMD_CMPLT)==0x0);
//  vLib_LeWrite32(SPI020REG_INTR_ST, (reg | SPI020_CMD_CMPLT));/* clear command complete status */
    vLib_LeWrite32((INT8U * )SPI020REG_INTR_ST, SPI020_CMD_CMPLT);/* clear command complete status */
}
#define FLASH_4BYTES_CMD_EN
void spi020_4Bytes_ctrl(UINT8 enable)
{
    if (enable) {
        spi020_set_commands(SPI020_B7_CMD0, SPI020_B7_CMD1, SPI020_B7_CMD2, SPI020_B7_CMD3);
    } else {
        spi020_set_commands(SPI020_E9_CMD0, SPI020_E9_CMD1, SPI020_E9_CMD2, SPI020_E9_CMD3);
    }
    /* wait for command complete */
    spi020_wait_command_complete();
}

void ftspi020_init(struct ftspi020_fop_info *fop)
{
    unsigned int reg;
#ifdef CONFIG_SPI_CLK_MODE3
    int mode = mode3;
#else	//SPI_CLK_MODE0
    int mode = mode0;
#endif

    /* reset spi & wait until this reset complete */
    SPI020_REG32(FTSPI020_REG_CTRL) = 1<<8;	//reset hw
    while((SPI020_REG32(FTSPI020_REG_CTRL)&(1<<8)));

    /* Set control reg. */
    reg = SPI020_REG32(FTSPI020_REG_CTRL);
    reg &= ~(BIT4 | BIT1 | BIT0 | BIT20);
    reg |= ((mode<<4) |SPI_CLK_DIV_8);
    SPI020_REG32(FTSPI020_REG_CTRL) = reg;

//    spi020_4Bytes_ctrl(1);

    fop->addr_len = ftspi020_get_addr_len();
    _DSG(LOG_INFO, "%s: address len = %d",__FUNCTION__, fop->addr_len);

#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
    /*	Because SPANSION and EON have different way to implement 4-byte address mode. So we need to 
    *	read manufacturer id to identify it.
    *	SPANSION's ID: 0x01, EON's ID: 0x1c
    */
    if((fop->flash_type == SPI_NOR_FLASH) && (fop->addr_len == SPI_ADDR_LEN_4BYTE)) {
        reg = ftspi020_probe();
        fop->flash_id = reg&0xff;
        if(fop->flash_id != SPI_MFID_SPANSION) {
            fop->enable_4byte_mode = SPI_NOR_EN4B_STS_BIT;
            _DSG(LOG_INFO, "enable_4byte_mode=%x",fop->enable_4byte_mode);
            if((fop->flash_id == SPI_MFID_MICRON) || (pdd_ftscu_get_spi_need_wren()))
                fop->enable_4byte_mode |= SPI_NOR_ENWR_STS_BIT;
            _DSG(LOG_INFO, "enable_4byte_mode=%x, wren=%x",fop->enable_4byte_mode, pdd_ftscu_get_spi_need_wren());
            ftspi020_4byte_enable(fop->enable_4byte_mode);
        }
    }
#endif
}

unsigned int spi020_booting(mem_booting_t *mem_boot_info, int flash_type)
{
    unsigned int offset = 0, block_off = mem_boot_info->dev_offset;
    unsigned char img_cnt = SPI_SPL_HARDCOPY_NUM;
    struct ftspi020_fop_info spi_fop ={0};
    spi_fop.dl_addr = (volatile UINT32 *)mem_boot_info->mem_addr;
    //spi_fop.block_size = SPI020_BLOCK_64SIZE;
    _DSG(LOG_INFO, "spi020_booting flash type=%d", flash_type);

    ftspi020_init(&spi_fop);

#if( CONFIG_SPI_ENABLE_BACKUP )
    do {
#endif
        _DSG(LOG_INFO, "block_off=%d, img_cnt=%d",block_off, img_cnt);

        {

            spi_fop.img_size = bsp_spi_io_read(mem_boot_info, &spi_fop);
            _DSG(LOG_INFO, "img_size=%d",spi_fop.img_size);

#ifdef CONFIG_IMG_CHK_SUM
            unsigned char chk_sum = 0, img_chksum = 0;
            chk_sum = bootimg_sum((unsigned int*)(mem_boot_info->mem_addr),spi_fop.img_size);
            printf("%s: chk_sum=%x, img_chksum=%x\n",__FUNCTION__, chk_sum, img_chksum);
            if(chk_sum != img_chksum)
                spi_fop.img_size = 0;
#endif
        }

#if( CONFIG_SPI_ENABLE_BACKUP )
        _DSG(LOG_INFO, "block_off=%d",block_off);
        if(spi_fop.img_size)
            break;
        block_off++;
        offset += spi_fop.block_size;

    }while(block_off < img_cnt);
#endif



#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
    if(spi_fop.enable_4byte_mode) {
        spi_fop.enable_4byte_mode &= (~SPI_NOR_EN4B_STS_BIT);
        ftspi020_4byte_enable(spi_fop.enable_4byte_mode);
    }
#endif
    return spi_fop.img_size;
}

