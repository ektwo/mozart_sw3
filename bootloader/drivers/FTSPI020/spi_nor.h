#ifndef __SPI_NOR_H__
#define __SPI_NOR_H__


/* Flash opcodes. */
#define OPCODE_WRDI				0x04	/* Write disable */
#define OPCODE_WREN				0x06	/* Write enable */
#define OPCODE_RDSR				0x05	/* Read status register */
#define OPCODE_WRSR				0x01	/* Write status register 1 byte */
#define OPCODE_NORM_WRITE		0x02	/* Write data bytes */
#define OPCODE_NORM_READ		0x03	/* Read data bytes (low frequency) */
#define OPCODE_FAST_READ		0x0b	/* Read data bytes (high frequency) */
#define OPCODE_PP				0x02	/* Page program (up to 256 bytes) */
#define OPCODE_BE_4K			0x20	/* Erase 4KiB block */
#define OPCODE_DUAL_READ		0xbb
#define OPCODE_BE_32K			0x52	/* Erase 32KiB block */
#define OPCODE_CHIP_ERASE		0xc7	/* Erase whole flash chip */
#define OPCODE_SE				0xd8	/* Sector erase (usually 64KiB) */
#define OPCODE_RDID				0x9f	/* Read JEDEC ID */
#define OPCODE_BE_64K			0xd8	/* Erase 64KiB block */
#define OPCODE_RESET			0xff

/* Flash 4-byte mode opcodes */
#define OPCODE_4BYTE_READ		0x13
#define OPCODE_4BYTE_WRITE		0x12
#define OPCODE_4BYTE_BE_64K		0xdc

#define OPCODE_ENABLE_4BYTE		0xb7
#define OPCODE_EXIT_4BYTE		0xe9

#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
void ftspi020_4byte_enable(unsigned char en_4b);
#endif

int ftspi020_read(UINT32 dev_off, UINT32 len, struct ftspi020_fop_info *fop);
#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
unsigned int ftspi020_probe(void);
#endif
#endif

