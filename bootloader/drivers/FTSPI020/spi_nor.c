#include "ftspi020.h"
#include "spi_nor.h"

/*	In ROM code part, we don't need to access address > 16MB.
*	So we can only to use 3-byte address.
*/
void ftspi020_chk_status(void)
{
	struct ftspi020_cmd_info cmd_info = {0};
	cmd_info.ins_code = OPCODE_RDSR;
	cmd_info.rd_status_en = TRUE;
	ftspi020_send_command(&cmd_info);
	ftspi020_wait_cmd_done();
}


#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
void ftspi020_4byte_enable(unsigned char en_4b)
{
	struct ftspi020_cmd_info cmd_info = {0};
	//if(en_4b&SPI_NOR_ENWR_STS_BIT)
	//	ftspi020_write_enable(TRUE, 0);

	_DSG(LOG_INFO, "%s: 4 byte mode %s",__FUNCTION__,en_4b&SPI_NOR_EN4B_STS_BIT?"Enalbe":"Exit");
	if(en_4b&SPI_NOR_EN4B_STS_BIT)
		cmd_info.ins_code = OPCODE_ENABLE_4BYTE;
	else
		cmd_info.ins_code = OPCODE_EXIT_4BYTE;
	cmd_info.write_en = TRUE;
	ftspi020_send_command(&cmd_info);
	ftspi020_wait_cmd_done();

	//if(en_4b&SPI_NOR_ENWR_STS_BIT)
	//	ftspi020_write_enable(FALSE, 0);
}
#endif

int ftspi020_read(UINT32 dev_off, UINT32 len, struct ftspi020_fop_info *fop)
{
	struct ftspi020_cmd_info cmd_info = {0};

	cmd_info.spi_addr = dev_off;
	cmd_info.addr_len = fop->addr_len;

#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
	_DSG(LOG_INFO, "fop->addr_len=%x", fop->addr_len);
	_DSG(LOG_INFO, "fop->enable_4byte_mode=%x", fop->enable_4byte_mode);
	if((fop->addr_len == SPI_ADDR_LEN_4BYTE)&&(fop->enable_4byte_mode))
	//if((fop->addr_len == SPI_ADDR_LEN_4BYTE)&&(!fop->enable_4byte_mode))
		cmd_info.ins_code = OPCODE_4BYTE_READ;
	else
#endif
		cmd_info.ins_code = OPCODE_NORM_READ;
	_DSG(LOG_INFO, "ftspi020_read cmd_info.ins_code=%x", cmd_info.ins_code);
	cmd_info.data_counter = fop->img_size;
	ftspi020_send_command(&cmd_info);
	ftspi020_data_fop((UINT32 *)fop->dl_addr, fop->img_size, SPI_DATA_READ);
	ftspi020_wait_cmd_done();
	return 0;
}

#ifdef CONFIG_SPI_ADDR_LEN_JUMPER
unsigned int ftspi020_probe(void)
{
	UINT32  chip_id=0;
	SPI020_REG32(FTSPI020_REG_CMD0) = 0x0;
	SPI020_REG32(FTSPI020_REG_CMD1) = 0x01000000;
	SPI020_REG32(FTSPI020_REG_CMD2) = 0x3;
	SPI020_REG32(FTSPI020_REG_CMD3) = 0x9F000000;

	ftspi020_data_fop(&chip_id,3,SPI_DATA_READ);
	ftspi020_wait_cmd_done();
	_DSG(LOG_INFO, "chip_id=%x",chip_id);
	return chip_id;
}
#endif

