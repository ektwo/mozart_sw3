#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
    char *fname;
    size_t flimit;
    struct stat st;

    if (argc < 3) {
        printf("usage: %s [file_name] [file_limit]\r\n","bootcode2_chk");
        return -1;
    }

    fname = argv[1];
    flimit = atoi(argv[2]);

    if (!stat(fname, &st)) {
        printf("[bootcode2] Image File: %s, %d bytes\r\n", fname, (int)st.st_size);
        if (st.st_size > flimit) {
            printf("[%s] ERORO!!: %s exceeds file size limit.(%d bytes)\r\n", __FILE__, fname, (int)flimit);
        }
    }

    return 0;
}
