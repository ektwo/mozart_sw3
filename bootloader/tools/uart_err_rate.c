#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "../autoconf.h"

#define MODE_X_DIV 16

/*	UART miss rate:
	<2%: good
	>6%: miss
	for both side of RS232 wire, miss rate should <3%
*/

void uart_miss_range(unsigned int uark_clk)
{
	int i, real_baud, baud_divisor, baud_rate[5]= {19200, 38400, 57600, 115200, 230400};
	float err_rate;
	for(i=0;i<5;i++) {
		baud_divisor =  (uark_clk + (baud_rate[i] * (MODE_X_DIV/2)))
	                       / (MODE_X_DIV * baud_rate[i]);
		real_baud = (uark_clk/16)/baud_divisor;
		if(real_baud > baud_rate[i])
			err_rate = real_baud-baud_rate[i];
		else
			err_rate = baud_rate[i] - real_baud;
		err_rate = (float)err_rate/baud_rate[i];
		printf("UART Clock: %d Hz, Divisor: %d, Baud Rate: %d(%d), Miss rate: %2.3f%\n",uark_clk,baud_divisor, baud_rate[i],real_baud, err_rate*100);
	}
}

int main(int argc, char *argv[])
{
	float err_rate;
	int real_baud, baud_divisor =  (CONFIG_UART_CLK + (CONFIG_SERIAL_BAUDRATE * (MODE_X_DIV/2)))
                       / (MODE_X_DIV * CONFIG_SERIAL_BAUDRATE);

	real_baud = (CONFIG_UART_CLK/16)/baud_divisor;
	if(real_baud > CONFIG_SERIAL_BAUDRATE)
		err_rate = real_baud-CONFIG_SERIAL_BAUDRATE;
	else
		err_rate = CONFIG_SERIAL_BAUDRATE - real_baud;
	err_rate = (float)err_rate/CONFIG_SERIAL_BAUDRATE;
	printf("UART Clock: %d Hz, Divisor: %d, Baud Rate: %d(%d), Miss rate: %2.3f%\n",CONFIG_UART_CLK,baud_divisor, CONFIG_SERIAL_BAUDRATE,real_baud, err_rate*100);

	//uart_miss_range(53571300);
	return 0;
}

