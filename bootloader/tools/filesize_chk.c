#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char *argv[])
{
	char *fname;
	struct stat st;

	if (argc < 2) {
		printf("usage: %s [file_name]\r\n","filesize_chk");
		return -1;
	}

	fname = argv[1];

	if (!stat(fname, &st)) {
		printf("[FILE CHK] Image File: %s, %d bytes\r\n", fname, (int)st.st_size);
	}

	return 0;
}
