#include <common.h>
#ifdef CONFIG_SERIAL_BASE

#define MODE_X_DIV 16

static NS16550_t com_port = (NS16550_t)CONFIG_SERIAL_BASE;

void serial_init(void)
{
    int baud_divisor =  (CONFIG_SERIAL_CLOCK + (CONFIG_SERIAL_BAUDRATE * (MODE_X_DIV/2)))
                       / (MODE_X_DIV * CONFIG_SERIAL_BAUDRATE);
	com_port->ier = 0x00;
	com_port->lcr = LCR_BKSE | LCRVAL;
	com_port->dll = 0;
	com_port->dlm = 0;
	com_port->lcr = LCRVAL;
	com_port->mcr = MCRVAL;
	com_port->fcr = FCRVAL;
	com_port->lcr = LCR_BKSE | LCRVAL;
	com_port->dll = baud_divisor & 0xff;
	com_port->dlm = (baud_divisor >> 8) & 0xff;
	com_port->lcr = LCRVAL;	
}

void serial_putc(char c)
{
	while ((com_port->lsr & LSR_THRE) == 0);
	com_port->thr = c;
}

void serial_puts(char *s)
{
    while (s && *s) {
        if (*s == '\n')
            serial_putc ('\r');
        serial_putc (*s++);
    }
}

int serial_getc(void)
{
    int c = EOF;

	if ((com_port->lsr & LSR_DR) != 0)
		c = (com_port->rbr);

	return c;
}

#include <stdio.h>
#include <stdarg.h> 

#define BACKSP_KEY 0x08
#define RETURN_KEY 0x0D
#define DELETE_KEY 0x7F
#define BELL       0x07

int fLib_scanf(char *buf)
{
	char    *cp;
	char    data;
	UINT32  count;
	count = 0;
	cp = buf;

    do {
        data = (char) serial_getc();
        switch(data) {
        case RETURN_KEY:
            if(count < 256) {
                *cp = '\0';
                serial_putc('\n');
            }
            break;
        case BACKSP_KEY:
        case DELETE_KEY:
            if(count) {
                count--;
                *(--cp) = '\0';
                serial_puts("\b \b");
            }
            break;
        default:
            if( data > 0x1F && data < 0x7F && count < 256) {
                *cp = (char)data;
                cp++;
                count++;
                serial_putc(data);
            }
            break;
        }

    } while(data != RETURN_KEY);

    return (count);
}

INT32 fLib_printf(const char *f, ...)	/* variable arguments */
{
	INT32 cnt;
	va_list arg_ptr;    
	char buffer[256];
	unsigned long flags;

	//put the character to buffer
	va_start(arg_ptr, f);

	cnt = vsprintf(&buffer[0], f, arg_ptr);
	va_end(arg_ptr);   

	//output the buffer

	serial_puts(buffer);

	return 0;
}


void serial_check_tx_fifo_empty(void)
{
	while (1) {
		if((com_port->lsr & LSR_TEMT) != 0)
			break;
	}
}
#endif	/* #ifdef CONFIG_SERIAL_BASE */

