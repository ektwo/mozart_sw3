#include "debug_support.h"

volatile BOOL DEBUG_DUMP_ENABLE	= FALSE; 
#ifdef CONFIG_DEBUG_HW_SIM
void debugport(int code)
{
	*(unsigned long *)(SIM_DEBUG_PORT) = code;

}

void sim_finish(void)
{
	*(unsigned long *)(SIM_DEBUG_BASE) = 0x01234567;
}

void sim_pass(void)
{
	*(unsigned long *)(SIM_DEBUG_BASE) = 0x01234568;
}

void sim_fail(void)
{
	*(unsigned long *)(SIM_DEBUG_BASE) = 0x01234569;
	sim_finish();
}

void sim_warning(void)
{
	*(unsigned long *)(SIM_DEBUG_BASE) = 0x01234570;
}

#endif

