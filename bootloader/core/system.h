#ifndef _SYSTEM_H_
#define _SYSTEM_H_
#include <common.h>
#include <core/debug_support.h>

typedef struct { int gcc_is_buggy;} spinlock_t;

#define spin_lock_init(lock)    do { } while(0)
#define spin_lock(lock)         (void)(lock) /* Not "unused variable". */
#define spin_is_locked(lock)    (0)
#define spin_trylock(lock)      ({1; })
#define spin_unlock_wait(lock)  do { } while(0)
#define spin_unlock(lock)       do { } while(0)

//#define spin_lock_irq(lock)		do { DisableIRQ();	spin_lock(lock); } while (0)
//#define spin_unlock_irq(lock)	do { spin_unlock(lock);	EnableIRQ(); } while (0)

#define REG_IO_32BIT(ip_base, offset)	*((volatile UINT32 *)(ip_base + offset))
#define REG_IO_16BIT(ip_base, offset)	*((volatile UINT16 *)(ip_base + offset))
#define REG_IO_8BIT(ip_base, offset)	*((volatile UINT8 *)(ip_base + offset))

#endif
