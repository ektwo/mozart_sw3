#ifndef _HW_RTL_SIM_H_
#define _HW_RTL_SIM_H_
#include <common.h>

#ifdef CONFIG_DEBUG_HW_SIM
void debugport(int code);
void sim_finish(void);
void sim_pass(void);
void sim_fail(void);
void sim_warning(void);
#else
static inline void debugport(int code) {}
static inline void sim_finish(void) {}
static inline void sim_pass(void) {}
static inline void sim_fail(void) {}
static inline void sim_warning(void) {}
#endif

#endif
