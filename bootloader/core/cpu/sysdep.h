#ifndef _SYSDEP_H
#define _SYSDEP_H
#include "../../autoconf.h"

#ifndef UINT64
#define UINT64			unsigned long long
#endif

#ifndef UINT32
#define UINT32			unsigned int
#endif

#ifndef INT32
#define INT32			int
#endif

#ifndef UINT16
#define UINT16			unsigned short
#endif

#ifndef INT16
#define INT16			short
#endif

#ifndef UINT8
#define UINT8			unsigned char
#endif

#ifndef INT8
#define INT8			char
#endif

#ifndef BOOL
#define BOOL			char
#endif


#ifndef REG32
#define REG32(adr)		*(volatile UINT32 *)(adr)
#endif

#ifndef REG16
#define REG16(adr)		*(volatile UINT16 *)(adr)
#endif

#ifndef REG8
#define REG8(adr)		*(volatile UINT8 *)(adr)
#endif



#ifndef inw
#define inw(adr)		REG32(adr)
#endif

#ifndef outw
#define outw(adr, val)	REG32(adr) = (val)
#endif

#ifndef NULL
#define NULL						((void *)0)
#endif

#ifndef BOOLEAN
#define BOOLEAN						int
#endif

#ifndef FALSE
#define FALSE						0
#endif

#ifndef TRUE
#define TRUE						1
#endif

#ifndef ENABLE
#define ENABLE                                           1
#endif

#ifndef DISABLE
#define DISABLE                                          0
#endif

#define BIT0			                0x00000001
#define BIT1			                0x00000002
#define BIT2			                0x00000004
#define BIT3			                0x00000008
#define BIT4			                0x00000010
#define BIT5			                0x00000020
#define BIT6			                0x00000040
#define BIT7			                0x00000080

#define BIT8			                0x00000100
#define BIT9			                0x00000200
#define BIT10			                0x00000400
#define BIT11			                0x00000800
#define BIT12			                0x00001000
#define BIT13			                0x00002000
#define BIT14			                0x00004000
#define BIT15			                0x00008000

#define BIT16			                0x00010000
#define BIT17			                0x00020000
#define BIT18			                0x00040000
#define BIT19			                0x00080000
#define BIT20			                0x00100000
#define BIT21			                0x00200000
#define BIT22			                0x00400000
#define BIT23			                0x00800000
#define BIT24			                0x01000000
#define BIT25			                0x02000000
#define BIT26			                0x04000000
#define BIT27			                0x08000000
#define BIT28			                0x10000000
#define BIT29			                0x20000000
#define BIT30			                0x40000000
#define BIT31			                0x80000000

#endif
