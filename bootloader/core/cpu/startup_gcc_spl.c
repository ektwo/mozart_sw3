//*****************************************************************************
//
// startup.c - Boot code for Stellaris.
//
// Copyright (c) 2005,2006 Luminary Micro, Inc.  All rights reserved.
//
// Software License Agreement
//
// Luminary Micro, Inc. (LMI) is supplying this software for use solely and
// exclusively on LMI's Stellaris Family of microcontroller products.
//
// The software is owned by LMI and/or its suppliers, and is protected under
// applicable copyright laws.  All rights are reserved.  Any use in violation
// of the foregoing restrictions may subject the user to criminal sanctions
// under applicable laws, as well as to civil liability for the breach of the
// terms and conditions of this license.
//
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// LMI SHALL NOT, IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL, OR
// CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.
//
// This is part of revision 852 of the Stellaris Driver Library.
//
//*****************************************************************************

// modified by Martin Thomas - don't blame Luminary if it does
// not work for you.

#include <common.h>
#include "kneron_mozart.h"
#include "pwmtimer.h"

#define WRITE(addr,data) *( (volatile unsigned int *) ( addr ) ) = data
#define READ( addr ) *( (volatile unsigned int *) ( addr ) )
//#define HAL_WRITE_UINT64( _register_, _value_ ) (*((volatile uint64_t *)(_register_)) = (_value_))
//#define HAL_READ_UINT64( _register_, _value_ )  ((_value_) = *((volatile uint64_t *)(_register_)))

#define WHOAMI_KEY                      0x12345678

#define SCU_FTSCU100_PA_BASE            0xc2300000
#define EXTREG_REG_BASE                 0xc2380000

#define BIT16                           0x00010000
#define SCU_REG_BTUP_STS_PWRBTN_STS     BIT16
#define SCU_REG_BTUP_STS                (SCU_FTSCU100_PA_BASE + 0x0000)
#define SCU_REG_BTUP_CTRL               (SCU_FTSCU100_PA_BASE + 0x0004)
#define writeb(val, addr)               (*(volatile unsigned char *)(addr) = (val))

//#include <core/pwmtimer.h>
#define boardLED_ON                     ( 1 )
#define boardLED_OFF                    ( 0 )
#define board_LED_FIRST_BIT             ( 22 )				
#define board_LEDS_NUM                  ( 6 ) 
#define board_LED_INITIALISER           {(1 << board_LED_FIRST_BIT), (1 << (board_LED_FIRST_BIT + 1)), (1 << (board_LED_FIRST_BIT + 2)),\
 (1 << (board_LED_FIRST_BIT + 3)), (1 << (board_LED_FIRST_BIT + 4)), (1 << (board_LED_FIRST_BIT + 5))}
#define GPIO_DIR_OUTPUT                 1
#define GPIO_PINOUT_OFFSET              0x8
#define GPIO_DATACLR                    0x14
#define GPIO_DATASET                    0x10
UINT32 ulLEDBits[ board_LEDS_NUM ] = board_LED_INITIALISER;

#ifdef TEST_TIME   
extern UINT32 T1_Tick;
#endif

typedef struct {
    //uint32_t    reg[32];
    UINT32    reg[32];
} SCUREG;

typedef struct {
    //uint32_t    reg[32];
    UINT32    reg[32];
} EXTREG;
//*****************************************************************************
//
// Forward declaration of the default fault handlers.
//
//*****************************************************************************
// mthomas: attribute for stack-aligment (see README_mthomas.txt)
#ifdef __GNUC__
void ResetISR(void) __attribute__((__interrupt__));
static void IntDefaultHandler(void) __attribute__((__interrupt__));
#else
void ResetISR(void);
static void IntDefaultHandler(void);
#endif

//*****************************************************************************
//
// The entry point for the application.
//
//*****************************************************************************
extern void start_main(void);

//*****************************************************************************
//
// Reserve space for the system stack.
//
//*****************************************************************************
#ifndef STACK_SIZE
#define STACK_SIZE                              0x600//0x1800
#endif

// mthomas: added section -> alignment thru linker-script
__attribute__ ((section(".stackarea")))
static unsigned long pulStack[STACK_SIZE];
/* We gave 'weak' attribute, so these functions can be aliased into a single
 * function
 */
void __attribute__((weak)) Reset_Handler(void);     /* Reset Handler */
void __attribute__((weak)) NMI_Handler(void);       /* NMI Handler */
void __attribute__((weak)) HardFault_Handler(void); /* Hard Fault Handler */
void __attribute__((weak)) MemManage_Handler(void); /* MPU Fault Handler */
void __attribute__((weak)) BusFault_Handler(void);  /* Bus Fault Handler */
void __attribute__((weak)) UsageFault_Handler(void);/* Usage Fault Handler */
void __attribute__((weak)) SVC_Handler(void);       /* SVCall Handler */
void __attribute__((weak)) DebugMon_Handler(void);  /* Debug Monitor Handler */
void __attribute__((weak)) PendSV_Handler(void);    /* PendSV Handler */
void __attribute__((weak)) SysTick_Handler(void);   /* SysTick Handler */
void __attribute__((weak)) ISR0_Handler(void);      /* ISR0 Handler */
void __attribute__((weak)) ISR1_Handler(void);      /* ISR1 Handler */
void __attribute__((weak)) ISR2_Handler(void);      /* ISR2 Handler */
void __attribute__((weak)) ISR3_Handler(void);      /* ISR3 Handler */
void __attribute__((weak)) ISR4_Handler(void);      /* ISR4 Handler */
void __attribute__((weak)) ISR5_Handler(void);      /* ISR5 Handler */
void __attribute__((weak)) ISR6_Handler(void);      /* ISR6 Handler */
void __attribute__((weak)) ISR7_Handler(void);      /* ISR7 Handler */
void __attribute__((weak)) ISR8_Handler(void);      /* ISR8 Handler */
void __attribute__((weak)) ISR9_Handler(void);      /* ISR9 Handler */
void __attribute__((weak)) ISR10_Handler(void);     /* ISR10 Handler */
void __attribute__((weak)) ISR11_Handler(void);     /* ISR11 Handler */
void __attribute__((weak)) ISR12_Handler(void);     /* ISR12 Handler */
void __attribute__((weak)) ISR13_Handler(void);     /* ISR13 Handler */
void __attribute__((weak)) ISR14_Handler(void);     /* ISR14 Handler */
void __attribute__((weak)) ISR15_Handler(void);     /* ISR15 Handler */
void __attribute__((weak)) ISR16_Handler(void);     /* ISR16 Handler */
void __attribute__((weak)) ISR17_Handler(void);     /* ISR17 Handler */
void __attribute__((weak)) ISR18_Handler(void);     /* ISR18 Handler */
void __attribute__((weak)) ISR19_Handler(void);     /* ISR19 Handler */
void __attribute__((weak)) ISR20_Handler(void);     /* ISR20 Handler */
void __attribute__((weak)) ISR21_Handler(void);     /* ISR21 Handler */
void __attribute__((weak)) ISR22_Handler(void);     /* ISR22 Handler */
void __attribute__((weak)) ISR23_Handler(void);     /* ISR23 Handler */
void __attribute__((weak)) ISR24_Handler(void);     /* ISR24 Handler */
void __attribute__((weak)) ISR25_Handler(void);     /* ISR25 Handler */
void __attribute__((weak)) ISR26_Handler(void);     /* ISR26 Handler */
void __attribute__((weak)) ISR27_Handler(void);     /* ISR27 Handler */
void __attribute__((weak)) ISR28_Handler(void);     /* ISR28 Handler */
void __attribute__((weak)) ISR29_Handler(void);     /* ISR29 Handler */
void __attribute__((weak)) ISR30_Handler(void);     /* ISR30 Handler */
void __attribute__((weak)) ISR31_Handler(void);     /* ISR31 Handler */
void __attribute__((weak)) ISR32_Handler(void);     /* ISR32 Handler */
void __attribute__((weak)) ISR33_Handler(void);     /* ISR33 Handler */
void __attribute__((weak)) ISR34_Handler(void);     /* ISR34 Handler */
void __attribute__((weak)) ISR35_Handler(void);     /* ISR35 Handler */
void __attribute__((weak)) ISR36_Handler(void);     /* ISR36 Handler */
void __attribute__((weak)) ISR37_Handler(void);     /* ISR37 Handler */
void __attribute__((weak)) ISR38_Handler(void);     /* ISR38 Handler */
void __attribute__((weak)) ISR39_Handler(void);     /* ISR39 Handler */
void __attribute__((weak)) ISR40_Handler(void);     /* ISR40 Handler */
void __attribute__((weak)) ISR41_Handler(void);     /* ISR41 Handler */
void __attribute__((weak)) ISR42_Handler(void);     /* ISR42 Handler */
void __attribute__((weak)) ISR43_Handler(void);     /* ISR43 Handler */
void __attribute__((weak)) ISR44_Handler(void);     /* ISR44 Handler */
void __attribute__((weak)) ISR45_Handler(void);     /* ISR45 Handler */
void __attribute__((weak)) ISR46_Handler(void);     /* ISR46 Handler */
void __attribute__((weak)) ISR47_Handler(void);     /* ISR47 Handler */
void __attribute__((weak)) ISR48_Handler(void);     /* ISR48 Handler */
void __attribute__((weak)) ISR49_Handler(void);     /* ISR49 Handler */
void __attribute__((weak)) ISR50_Handler(void);     /* ISR50 Handler */
void __attribute__((weak)) ISR51_Handler(void);     /* ISR51 Handler */
void __attribute__((weak)) ISR52_Handler(void);     /* ISR52 Handler */
void __attribute__((weak)) ISR53_Handler(void);     /* ISR53 Handler */
void __attribute__((weak)) ISR54_Handler(void);     /* ISR54 Handler */
void __attribute__((weak)) ISR55_Handler(void);     /* ISR55 Handler */
void __attribute__((weak)) ISR56_Handler(void);     /* ISR56 Handler */
void __attribute__((weak)) ISR57_Handler(void);     /* ISR57 Handler */
void __attribute__((weak)) ISR58_Handler(void);     /* ISR58 Handler */
void __attribute__((weak)) ISR59_Handler(void);     /* ISR59 Handler */
void __attribute__((weak)) ISR60_Handler(void);     /* ISR60 Handler */
void __attribute__((weak)) ISR61_Handler(void);     /* ISR61 Handler */
void __attribute__((weak)) ISR62_Handler(void);     /* ISR62 Handler */
void __attribute__((weak)) ISR63_Handler(void);     /* ISR63 Handler */



//*****************************************************************************
//
// The minimal vector table for a Cortex M3.  Note that the proper constructs
// must be placed on this to ensure that it ends up at physical address
// 0x0000.0000.
//
//*****************************************************************************
__attribute__ ((section(".isr_vector")))
void (* const g_pfnVectors[])(void) =
{
    (void (*)(void))((unsigned long)pulStack + sizeof(pulStack)),
                                            // The initial stack pointer
    ResetISR,                               // The reset handler
    NMI_Handler,                            // The NMI handler
    HardFault_Handler,                               // The hard fault handler
    MemManage_Handler,                      // The MPU fault handler
    BusFault_Handler,                       // The bus fault handler
    UsageFault_Handler,                     // The usage fault handler
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    0,                                      // Reserved
    SVC_Handler,                            // SVCall handler
    DebugMon_Handler,                       // Debug monitor handler
    0,                                      // Reserved
    PendSV_Handler,                         // The PendSV handler
    SysTick_Handler,                        // The SysTick handler
    ISR0_Handler,                           // ISR0 handler 
    ISR1_Handler,                           // ISR1 handler
    ISR2_Handler,                           // ISR2 handler
    ISR3_Handler,                           // ISR3 handler
    ISR4_Handler,                           // ISR4 handler
    ISR5_Handler,                           // ISR5 handler
    ISR6_Handler,                           // ISR6 handler
    ISR7_Handler,                           // ISR7 handler
    ISR8_Handler,                           // ISR8 handler
    ISR9_Handler,                           // ISR9 handler
    ISR10_Handler,                          // ISR10 handler
    ISR11_Handler,                          // ISR11 handler
    ISR12_Handler,                          // ISR12 handler
    ISR13_Handler,                          // ISR13 handler
    ISR14_Handler,                          // ISR14 handler
    ISR15_Handler,                          // ISR15 handler
    ISR16_Handler,                          // ISR16 handler
    ISR17_Handler,                          // ISR17 handler
    ISR18_Handler,                          // ISR18 handler
    ISR19_Handler,                          // ISR19 handler
    ISR20_Handler,                          // ISR20 handler	
    ISR21_Handler,                          // ISR21 handler
    ISR22_Handler,                          // ISR22 handler
    ISR23_Handler,                          // ISR23 handler
    ISR24_Handler,                          // ISR24 handler
    ISR25_Handler,                          // ISR25 handler
    ISR26_Handler,                          // ISR26 handler
    ISR27_Handler,                          // ISR27 handler
    ISR28_Handler,                          // ISR28 handler
    ISR29_Handler,                          // ISR29 handler
    ISR30_Handler,                          // ISR30 handler
    ISR31_Handler,                          // ISR31 handler
    ISR32_Handler,                          // ISR32 handler
    ISR33_Handler,                          // ISR33 handler
    ISR34_Handler,                          // ISR34 handler
    ISR35_Handler,                          // ISR35 handler
    ISR36_Handler,                          // ISR36 handler
    ISR37_Handler,                          // ISR37 handler
    ISR38_Handler,                          // ISR38 handler
    ISR39_Handler,                          // ISR39 handler
    ISR40_Handler,                          // ISR40 handler
    ISR41_Handler,                          // ISR41 handler
    ISR42_Handler,                          // ISR42 handler
    ISR43_Handler,                          // ISR43 handler
    ISR44_Handler,                          // ISR44 handler
    ISR45_Handler,                          // ISR45 handler
    ISR46_Handler,                          // ISR46 handler
    ISR47_Handler,                          // ISR47 handler
    ISR48_Handler,                          // ISR48 handler
    ISR49_Handler,                          // ISR49 handler
    ISR50_Handler,                          // ISR50 handler
    ISR51_Handler,                          // ISR51 handler
    ISR52_Handler,                          // ISR52 handler
    ISR53_Handler,                          // ISR53 handler
    ISR54_Handler,                          // ISR54 handler
    ISR55_Handler,                          // ISR55 handler
    ISR56_Handler,                          // ISR56 handler
    ISR57_Handler,                          // ISR57 handler
    ISR58_Handler,                          // ISR58 handler
    ISR59_Handler,                          // ISR59 handler
    ISR60_Handler,                          // ISR60 handler
    ISR61_Handler,                          // ISR61 handler
    ISR62_Handler,                          // ISR62 handler
    ISR63_Handler                           // ISR63 handler
};


//*****************************************************************************
//
// The following are constructs created by the linker, indicating where the
// the "data" and "bss" segments reside in memory.  The initializers for the
// for the "data" segment resides immediately following the "text" segment.
//
//*****************************************************************************
//extern unsigned long _etext;
//extern unsigned long _data;
//extern unsigned long _edata;
//extern unsigned long _bss;
//extern unsigned long _ebss;

extern unsigned long spl_rodata_end;
extern unsigned long spl_rwdata_start;
extern unsigned long spl_rwdata_end;
extern unsigned long spl_bss_start;
extern unsigned long spl_bss_end;
extern unsigned long _whoami;

#if 1
volatile unsigned int scu_int_flag;

void ISR40_Handler(void)
{

    WRITE( (SCU_FTSCU100_PA_BASE+0x24) , 0xffffffff ); //clear sleep wakeup interrupt
    //ext_model(40);
    //display("SCU interrupt event!!");
    //debugport(0x0200);

    scu_int_flag = 0x1;

}

void idle( unsigned long addr)
{   
   volatile UINT32 data;
   data=*(UINT32 *)addr;
}

void idlen( unsigned long int addr,  int idle_count)
{
    int i;
    volatile UINT32 data;

   for (i=0;i<=idle_count;i++)
   {
     data=*(volatile UINT32 *)addr;
   }
   /* do nothing */
}
#endif

//*****************************************************************************
//
// This is the code that gets called when the processor first starts execution
// following a reset event.  Only the absolutely necessary set is performed,
// after which the application supplied main() routine is called.  Any fancy
// actions (such as making decisions based on the reset cause register, and
// resetting the bits in that register) are left solely in the hands of the
// application.
//
//*****************************************************************************

void fLib_SetGpioDir(UINT32 io_base, UINT32 pin, UINT32 dir)
{
    UINT32 tmp;
    
     tmp = inw(io_base + GPIO_PINOUT_OFFSET);

     if(dir==GPIO_DIR_OUTPUT)
     {
         tmp |= (1<<pin); 
     }
     else
     {
        tmp &= ~(1<<pin);
    }

    outw(io_base + GPIO_PINOUT_OFFSET, tmp); 
}

void fLib_Gpio_SetData(UINT32 io_base, UINT32 data)
{
    outw(io_base + GPIO_DATASET, data);
}   
void fLib_Gpio_ClearData(UINT32 io_base, UINT32 data)
{
    outw(io_base + GPIO_DATACLR, data);	
}

void gpio_test_init(void)
{
    int data, i;
    data = inw(SCU_EXTREG_PA_BASE + 0x17C);
    outw(SCU_EXTREG_PA_BASE + 0x17C, data | 0x3);
    data = inw(SCU_EXTREG_PA_BASE + 0x180);
    outw(SCU_EXTREG_PA_BASE + 0x180, data | 0x3);
    data = inw(SCU_EXTREG_PA_BASE + 0x184);
    outw(SCU_EXTREG_PA_BASE + 0x184, data | 0x3);
    data = inw(SCU_EXTREG_PA_BASE + 0x188);
    outw(SCU_EXTREG_PA_BASE + 0x188, data | 0x3);
    data = inw(SCU_EXTREG_PA_BASE + 0x18C);
    outw(SCU_EXTREG_PA_BASE + 0x18C, data | 0x3);
    data = inw(SCU_EXTREG_PA_BASE + 0x190);
    outw(SCU_EXTREG_PA_BASE + 0x190, data | 0x3);

    for (i = 0; i < 500000; i++)
    {
        if (i == 490000)
            printf(" gpio_test_init \n");
    }

    //GPIO_OutputTest(GPIO_FTGPIO010_PA_BASE);
    for (i = 0; i < board_LEDS_NUM; i++)	
    {
        fLib_SetGpioDir(GPIO_FTGPIO010_PA_BASE, (i + board_LED_FIRST_BIT), GPIO_DIR_OUTPUT);	
    }    
    
    for (i = 0; i < 50000000; i++)
    {
        if (i == 49000000)
            printf(" SetGpioDir \n");
    }
}

void gpio_test_start(void)
{
#ifdef TEST_TIME    
    printf("gpio_test_start T1_Tick=%d \n", T1_Tick);    
#endif
    fLib_Gpio_SetData(GPIO_FTGPIO010_PA_BASE, ulLEDBits[1]);    
}
void gpio_test_end(void)
{
    fLib_Gpio_ClearData(GPIO_FTGPIO010_PA_BASE, ulLEDBits[1]);
#ifdef TEST_TIME
    printf("gpio_test_end T1_Tick=%d \n", T1_Tick);
#endif
    //for (i = 0; i < 50000000; i++)
    //{
    //    if (i == 49000000) {
    //        printf("done\n");
    //    }
    //}    
}

void
ResetISR(void)
{
    int x, y;    

    unsigned long *pulSrc, *pulDest;

    //memset(0x10200000, 0x10000, 0);
    //memset(0x10210000, 0x08000, 0);
    unsigned long j;
    unsigned long zero_init_start = 0x10200000;
    unsigned long zero_init_end = 0x10218000;
    //for(pulDest = &zero_init_start; pulDest < &zero_init_end; )
    for (j = zero_init_start; j < zero_init_end; ++j)
    {
        writeb(0, j);
    }

    void * ptr=NULL;
    printf(" ResetISR \n");
   __asm__ __volatile__ ("MRS %0, msp\n" : "=r" (ptr)     );        /* output */

    printf("stack read = 0x%x \n", ptr);    

    ptr=((unsigned long)pulStack + sizeof(pulStack));

   __asm__ __volatile__ ( "MSR msp, %0\n" : : "r" (ptr)  );          /* input */	

    printf("stack write\n");
    
  __asm__ __volatile__ ("MRS %0, msp\n" : "=r" (ptr)     );        /* output */

    printf("stack read = 0x%x \n", ptr);    
 
    //
    // Copy the data segment initializers from flash to SRAM.
    //
    //pulSrc = &_etext;
    pulSrc = &spl_rodata_end;
    printf("&spl_rodata_end = %x \n", &spl_rodata_end);//10101244
    printf("&spl_rwdata_start = %x \n", &spl_rwdata_start);//10200000
    printf("&spl_rwdata_end = %x \n", &spl_rwdata_end);//10200038
    
    printf("&spl_bss_start = %x \n", &spl_bss_start);    //10200038
    printf("&spl_bss_end = %x \n", &spl_bss_end);    //10200040
    printf("&_whoami = %x \n", &_whoami);    
    //for(pulDest = &_data; pulDest < &_edata; )
    for(pulDest = &spl_rwdata_start; pulDest < &spl_rwdata_end; )
    {
        *pulDest++ = *pulSrc++;
    }

    //
    // Zero fill the bss segment.
    //
    //for(pulDest = &_bss; pulDest < &_ebss; )
    for(pulDest = &spl_bss_start; pulDest < &spl_bss_end; )
    {
        *pulDest++ = 0;
    }
 
    //
    // Call the application's entry point.
    //

#if 1 //do fcs on ASIC

    int i;
    UINT32    data;
    UINT32    offset_addr;

   debugport(0x0100);

   //======== power on check ========

   // SCU BTUPSTS
   if ( READ( SCU_REG_BTUP_STS ) != SCU_REG_BTUP_STS_PWRBTN_STS ) 
     //display("SCU RTC ALARM enable.");     
     debugport(0x0101);

   WRITE(SCU_REG_BTUP_STS, SCU_REG_BTUP_STS_PWRBTN_STS);
   WRITE(SCU_REG_BTUP_CTRL, 0x00030001);

   // SCU PWRCTL
   //HAL_READ_UINT32(SCU_FTSCU100_PA_BASE + 0x08, data);
   data = READ(SCU_FTSCU100_PA_BASE + 0x08);


   // ==================== Pattern ========================
   // Initial the INTC
     WRITE  (0xE000E284,  0x00000001 << 8);

     WRITE  (0xE000E104,  0x00000001 << 8);
     data = READ   (0xE000E104);

   // SCU Interrupt enable register, clear scu state
   WRITE( (SCU_FTSCU100_PA_BASE+0x24) , 0xffffffff );

   //============== FCS pll en setting ========================

   // SCU PLLCTL, keep PLL NS value/frange and enable PLL
   //HAL_READ_UINT32(SCU_FTSCU100_PA_BASE + 0x30, data);
   data = READ(SCU_FTSCU100_PA_BASE + 0x30);

   WRITE( (SCU_FTSCU100_PA_BASE+0x30) , data & 0xffffff0e | 0x00000021 );

   // SCU PWR_MOD register setting
   //HAL_READ_UINT32(SCU_FTSCU100_PA_BASE + 0x20, data);
   data = READ(SCU_FTSCU100_PA_BASE + 0x20);

   WRITE( (SCU_FTSCU100_PA_BASE+0x20) , data & 0x0f0fff0f | 0x90000040 );

   //display("__wfi function!");
   debugport(0x0102);

   //__wfi();
   __asm volatile ("wfi");

   do{
    idle(100);
   }while((scu_int_flag)!= 0x1);
   //ISR0_Handler;
#endif

    _whoami = WHOAMI_KEY;

    start_main();
}




//*****************************************************************************
//
// This is the code that gets called when the processor receives an unexpected
// interrupt.  This simply enters an infinite loop, preserving the system state
// for examination by a debugger.
//
//*****************************************************************************
static void
IntDefaultHandler(void)
{
    //
    // Go into an infinite loop.
    //
    while(1)
    {
    }
}

//*****************************************************************************
//
//*****************************************************************************

#pragma weak NMI_Handler        = IntDefaultHandler /* NMI handler */
#pragma weak HardFault_Handler  = IntDefaultHandler /* Hard Fault handler */
#pragma weak MemManage_Handler  = IntDefaultHandler /* MPU Fault Handler */
#pragma weak BusFault_Handler   = IntDefaultHandler /* Bus Fault Handler */
#pragma weak UsageFault_Handler = IntDefaultHandler /* Usage Fault Handler */
#pragma weak SVC_Handler        = IntDefaultHandler /* SVCall Handler */
#pragma weak DebugMon_Handler   = IntDefaultHandler /* Debug Monitor Handler */
#pragma weak PendSV_Handler     = IntDefaultHandler /* PendSV Handler */
#pragma weak SysTick_Handler    = IntDefaultHandler /* SysTick Handler */
#pragma weak ISR0_Handler       = IntDefaultHandler /* ISR0 Handler */
#pragma weak ISR1_Handler       = IntDefaultHandler /* ISR1 Handler */
#pragma weak ISR2_Handler       = IntDefaultHandler /* ISR2 Handler */
#pragma weak ISR3_Handler       = IntDefaultHandler /* ISR3 Handler */
#pragma weak ISR4_Handler       = IntDefaultHandler /* ISR4 Handler */
#pragma weak ISR5_Handler       = IntDefaultHandler /* ISR5 Handler */
#pragma weak ISR6_Handler       = IntDefaultHandler /* ISR6 Handler */
#pragma weak ISR7_Handler       = IntDefaultHandler /* ISR7 Handler */
#pragma weak ISR8_Handler       = IntDefaultHandler /* ISR8 Handler */
#pragma weak ISR9_Handler       = IntDefaultHandler /* ISR9 Handler */
#pragma weak ISR10_Handler      = IntDefaultHandler /* ISR10 Handler */
#pragma weak ISR11_Handler      = IntDefaultHandler /* ISR11 Handler */
#pragma weak ISR12_Handler      = IntDefaultHandler /* ISR12 Handler */
#pragma weak ISR13_Handler      = IntDefaultHandler /* ISR13 Handler */
#pragma weak ISR14_Handler      = IntDefaultHandler /* ISR14 Handler */
#pragma weak ISR15_Handler      = IntDefaultHandler /* ISR15 Handler */
#pragma weak ISR16_Handler      = IntDefaultHandler /* ISR16 Handler */
#pragma weak ISR17_Handler      = IntDefaultHandler /* ISR17 Handler */
#pragma weak ISR18_Handler      = IntDefaultHandler /* ISR18 Handler */
#pragma weak ISR19_Handler      = IntDefaultHandler /* ISR19 Handler */
#pragma weak ISR20_Handler      = IntDefaultHandler /* ISR20 Handler */
#pragma weak ISR21_Handler      = IntDefaultHandler /* ISR21 Handler */
#pragma weak ISR22_Handler      = IntDefaultHandler /* ISR22 Handler */
#pragma weak ISR23_Handler      = IntDefaultHandler /* ISR23 Handler */
#pragma weak ISR24_Handler      = IntDefaultHandler /* ISR24 Handler */
#pragma weak ISR25_Handler      = IntDefaultHandler /* ISR25 Handler */
#pragma weak ISR26_Handler      = IntDefaultHandler /* ISR26 Handler */
#pragma weak ISR27_Handler      = IntDefaultHandler /* ISR27 Handler */
#pragma weak ISR28_Handler      = IntDefaultHandler /* ISR28 Handler */
#pragma weak ISR29_Handler      = IntDefaultHandler /* ISR29 Handler */
#pragma weak ISR30_Handler      = IntDefaultHandler /* ISR30 Handler */
#pragma weak ISR31_Handler      = IntDefaultHandler /* ISR31 Handler */
#pragma weak ISR32_Handler      = IntDefaultHandler /* ISR32 Handler */
#pragma weak ISR33_Handler      = IntDefaultHandler /* ISR33 Handler */
#pragma weak ISR34_Handler      = IntDefaultHandler /* ISR34 Handler */
#pragma weak ISR35_Handler      = IntDefaultHandler /* ISR35 Handler */
#pragma weak ISR36_Handler      = IntDefaultHandler /* ISR36 Handler */
#pragma weak ISR37_Handler      = IntDefaultHandler /* ISR37 Handler */
#pragma weak ISR38_Handler      = IntDefaultHandler /* ISR38 Handler */
#pragma weak ISR39_Handler      = IntDefaultHandler /* ISR39 Handler */
//#pragma weak ISR40_Handler      = IntDefaultHandler /* ISR40 Handler */
#pragma weak ISR41_Handler      = IntDefaultHandler /* ISR41 Handler */
#pragma weak ISR42_Handler      = IntDefaultHandler /* ISR42 Handler */
#pragma weak ISR43_Handler      = IntDefaultHandler /* ISR43 Handler */
#pragma weak ISR44_Handler      = IntDefaultHandler /* ISR44 Handler */
#pragma weak ISR45_Handler      = IntDefaultHandler /* ISR45 Handler */
#pragma weak ISR46_Handler      = IntDefaultHandler /* ISR46 Handler */
#pragma weak ISR47_Handler      = IntDefaultHandler /* ISR47 Handler */
#pragma weak ISR48_Handler      = IntDefaultHandler /* ISR48 Handler */
#pragma weak ISR49_Handler      = IntDefaultHandler /* ISR49 Handler */
#pragma weak ISR50_Handler      = IntDefaultHandler /* ISR50 Handler */
#pragma weak ISR51_Handler      = IntDefaultHandler /* ISR51 Handler */
#pragma weak ISR52_Handler      = IntDefaultHandler /* ISR52 Handler */
#pragma weak ISR53_Handler      = IntDefaultHandler /* ISR53 Handler */
#pragma weak ISR54_Handler      = IntDefaultHandler /* ISR54 Handler */
#pragma weak ISR55_Handler      = IntDefaultHandler /* ISR55 Handler */
#pragma weak ISR56_Handler      = IntDefaultHandler /* ISR56 Handler */
#pragma weak ISR57_Handler      = IntDefaultHandler /* ISR57 Handler */
#pragma weak ISR58_Handler      = IntDefaultHandler /* ISR58 Handler */
#pragma weak ISR59_Handler      = IntDefaultHandler /* ISR59 Handler */
#pragma weak ISR60_Handler      = IntDefaultHandler /* ISR60 Handler */
#pragma weak ISR61_Handler      = IntDefaultHandler /* ISR61 Handler */
#pragma weak ISR62_Handler      = IntDefaultHandler /* ISR62 Handler */
#pragma weak ISR63_Handler      = IntDefaultHandler /* ISR63 Handler */

