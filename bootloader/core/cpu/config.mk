arm_cflags-  :=
arm_cflags-n :=
arm_cflags-y := -msoft-float 

arm_cflags-y += \
	-mcpu=cortex-m3 -mtune=cortex-m3

# CPU
ARM_CFLAGS := $(arm_cflags-y)

# Thumb
thumb_cflags-  :=
thumb_cflags-n :=
thumb_cflags-y :=

ifeq ($(CONFIG_THUMB),y)
thumb_cflags-y += \
	-mthumb-interwork \
	-mthumb -Wa,-mimplicit-it=thumb
endif

THUMB_CFLAGS := $(thumb_cflags-y)
