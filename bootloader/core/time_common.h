#ifndef _TIME_COMMON_H_
#define _TIME_COMMON_H_
#define HZ						250		//tick per second

#define JIFF_TO_SEC(x)			((x)/HZ)
#define JIFF_TO_MSEC(x)			(x*1000/HZ)
#define JIFF_TO_MICROSEC(x)		((x)*10000/HZ)

#define SEC_TO_JIFF(x)			((x)*HZ)
#define MSEC_TO_JIFF(x)			(x*HZ/1000)
#define MICROSEC_TO_JIFF(x)		(x*HZ/100000)

#endif
