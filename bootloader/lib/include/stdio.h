/*
 * (C) Copyright 2010 Faraday Technology
 * Dante Su <dantesu@faraday-tech.com>
 *
 * This file is released under the terms of GPL v2 and any later version.
 * See the file COPYING in the root directory of the source tree for details.
 */

#ifndef _STDIO_H
#define _STDIO_H

#include <stddef.h>
//#include <console.h>

#ifndef NULL
#define	NULL	0
#endif

/* End of file character.
   Some things throughout the library rely on this being -1.  */
#ifndef EOF
#define EOF 	(-1)
#endif

#ifndef BUFSIZ
#define	BUFSIZ			1024
#endif

#ifndef FOPEN_MAX
#define	FOPEN_MAX		4
#endif

#ifndef FILENAME_MAX
#define	FILENAME_MAX	1024
#endif

#ifndef SEEK_SET
#define	SEEK_SET		0	/* set file offset to offset */
#endif
#ifndef SEEK_CUR
#define	SEEK_CUR		1	/* set file offset to current plus offset */
#endif
#ifndef SEEK_END
#define	SEEK_END		2	/* set file offset to EOF plus offset */
#endif


#ifdef CONFIG_VFS_FATFS

#include <ff.h>

typedef FIL FILE;

void   ffeconv(FRESULT rs);	/* FatFS error code convert */
FILE  *fopen(const char *filename, const char *mode);
int    fclose(FILE *stream);
size_t fread(void *ptr, size_t size, size_t nitems, FILE *stream);
size_t fwrite(const void *ptr, size_t size, size_t nitems, FILE *stream);
int    fflush(FILE *stream);
int    fseek(FILE *stream, long int offset, int whence);
char   *fgets(char *s, int n, FILE *stream);
#define fputc(c, fs)				f_putc(c, fs)
#define fputs(s, fs)				f_puts(s, fs)
#define fprintf(fs, fmt, args...)	f_printf(fs, fmt, ## args)
#define feof(fs)					f_eof(fs)
#define ferror(fs)					f_error(fs)
#define ftell(fs)					(long int)f_tell(fs)

#endif	/* #ifdef CONFIG_VFS_FATFS */

/* Read/Write characters from/to stdin. */

#ifdef CONFIG_CONSOLE
# define getchar()  console_getc()
# define putchar(c) console_putc(c)
# define gets(s)    console_gets(s)
# define puts(s)    console_puts(s)
#else  /* CONFIG_CONSOLE */
# define getchar()  EOF
# define putchar(c) (0)
# define gets(s)    NULL
# define puts(s)    (0)
#endif /* CONFIG_CONSOLE */

int scanf(const char *format, ...);
int sscanf(const char *str, const char *format, ...);

/*
 * printf(...)
 */
#ifdef CONFIG_LIBC_PRINTF
int printf(const char *format, ...);
int sprintf(char *str, const char *format, ...);
int snprintf(char *str, size_t size, const char *format, ...);
#else  /* CONFIG_LIBC_PRINTF */
# define printf(fmt, args...)            do { } while (0)
# define sprintf(buf, fmt, args...)      do { } while (0)
# define snprintf(buf, sz, fmt, args...) do { } while (0)
#endif /* CONFIG_LIBC_PRINTF */

#endif	/* #ifndef _STDIO_H */
