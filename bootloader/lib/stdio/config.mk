obj-  :=
obj-n :=
obj-y := \
	scanf.o

obj-$(CONFIG_LIBC_PRINTF_MIN) += \
	printf_min.o

obj-$(CONFIG_LIBC_PRINTF_STD) += \
	printf_std.o

obj-$(CONFIG_VFS) += \
	ffeconv.o \
	fopen.o \
	fclose.o \
	fflush.o \
	fgets.o \
	fseek.o \
	fread.o \
	fwrite.o
