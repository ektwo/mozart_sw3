/**
 * @file      kdp_host.h
 * @brief     kdp host lib header file
 * @version   0.1 - 2019-04-19
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __KDP_HOST__H__
#define __KDP_HOST__H__

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"{
#endif

#define KDP_UART_DEV   0

#include <stdint.h>


//-----------------------library init ----------------------

/**
 * init the host library
 * return 0 on succeed, -1 on failure
 */
int kdp_lib_init();

/**
 * start the host library to wait for messages
 * return 0 on succeed, -1 on failure
 */
int kdp_lib_start(); //kdp lib start 

/**
 * free the resources used by host lib
 * return 0 on succeed, -1 on failure
 */
int kdp_lib_de_init(); //kdp lib de init

/**
 * init the host lib internal log
 * dir: the directory name of the log file
 * name: the log file name
 * return 0 on succeed, -1 on failure
 */
int kdp_init_log(const char* dir, const char* name); //init log

/**
 * add com device to the host lib
 * type: the device type, only KDP_UART_DEV supported now
 * name: the UART device name
 * return dev idx on succeed, -1 on failure
 */
int kdp_add_dev(int type, const char* name); //add device to communicate
//---------------------- lib init end------------------------

//-----------------------app level APIs--------------------------
/**
 * start the user verification mode 
 * dev_idx: connected device ID. A host can connect several devices
 * img_size: the required image file size will be returned.
 * return 0 on succeed, error code on failure
 */
int kdp_start_verify_mode(int dev_idx, uint32_t* img_size);

/**
 * extract the face feature from input image, and compare it with DB,
 * return the matched user ID.
 * dev_idx: connected device ID. A host can connect several devices
 * img_name, name_len: the input image file and name length 
 * user_id: matched user ID in DB will be returned.
 * return 0 on succeed, error code on failure
 */
int kdp_verify_user_id(int dev_idx, uint16_t* user_id, char* img_buf, int buf_len);

/**
 * start the user register mode 
 * dev_idx: connected device ID. A host can connect several devices
 * user_id: the user id that will be registered.
 * img_idx: the image idx that will be saved. (a user could have 5 images)
 */
int kdp_start_reg_user_mode(int dev_idx, uint16_t usr_id, uint16_t img_idx);

/**
 * extract face feature from inut image and save it in device 
 * dev_idx: connected device ID. A host can connect several devices
 * img_name, name_len: the input image file and name length 
 * img_idx: the saved index in device
 *          if failed (no face found), img_idx is 0.
 * before calling this API, host must call kdp_start_reg_user_mode to enable register mode,
 * and specifed for which user and which index to be extracted.
 * return 0 on succeed, error code on failure
 */
int kdp_extract_feature(int dev_idx, char* img_buf, int buf_len);

/**
 * register the face features to device DB
 * dev_idx: connected device ID. A host can connect several devices
 * user_id: the user id that be registered. must be same as the kdp_start_reg_user_mode.
 * before calling this API, host must have called kdp_extract_feature
 * at least once successfully, otherwise, no features could be saved.
 * return 0 on succeed, error code on failure
 */
int kdp_register_user(int dev_idx, uint32_t user_id);

/**
 * remove all users from device DB
 * dev_idx: connected device ID. A host can connect several devices
 * return 0 on succeed, error code on failure
 */
int kdp_remove_user(int dev_idx, uint32_t user_id);

//-----------------------app level APIs end-------------------------------------

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif
