/**
 * @file      MsgId.h
 * @brief     Definition for msg ID
 * @version   0.1 - 2019-04-18
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __KN_MSG_ID_H__
#define __KN_MSG_ID_H__

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"{
#endif

#define KN_RSP_OFFSET               0x8000

#define KN_TEST_MEM_READ_REQ        0x0001
#define KN_TEST_MEM_WRITE_REQ       0x0002
#define KN_TEST_MEM_CLEAR_REQ       0x0006
#define KN_TEST_ECHO_REQ            0x0008

#define KN_MSG_GEN_ACK              0x0004

#define KN_DEMO_SET_MODELS_REQ      0x0010
#define KN_DEMO_SET_IMAGES_REQ      0x0011
#define KN_DEMO_RUN_ONCE_REQ        0x0012
#define KN_DEMO_RUN_LOOP_REQ        0x0013
#define KN_DEMO_STOP_REQ            0x0014

#define KN_RSP_IMAGE_PROC_SIZE      0x0015
#define KN_RSP_IMAGE_PROC_DATA      0x0016

#define KN_OPR_CMD_RESET_REQ        0x0020
#define KN_OPR_SYS_STATUS_REQ       0x0021
#define KN_OPR_QUERY_APPS_REQ       0x0022
#define KN_OPR_SELECT_APP_REQ       0x0023
#define KN_OPR_PROC_MODE_REQ        0x0024
#define KN_OPR_SET_UPT_EVENT_REQ    0x0025
#define KN_OPR_STATUS_UPDATE_REQ    0x0026
#define KN_OPR_IMG_PROC_RESULT      0x0027
#define KN_OPR_ABT_IMAGE_REQ        0x0028

#define KN_APP_SET_FID_MODE_REQ     0x0100
#define KN_APP_VERIFY_FID_REQ       0x0101
#define KN_APP_NEW_FID_REQ          0x0102
#define KN_APP_EDIT_FID_REQ         0x0103


#define KN_APP_SFID_START_REQ       0x0108
#define KN_APP_SFID_NEW_USER        0x0109
#define KN_APP_SFID_REGISTER        0x010A
#define KN_APP_SFID_DEL_DB          0x010B
#define KN_APP_SFID_SEND_IMG        0x010C


#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif
