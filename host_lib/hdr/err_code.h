/**
 * @file      err_code.h
 * @brief     Definition for rsp error code
 * @version   0.1 - 2019-05-05
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __ERROR_CODE_H__
#define __ERROR_CODE_H__

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"{
#endif

#define MAX_ERROR_CODE_STR          128

#define GEN_ERROR                   -1

#define ERR_APP_NOT_LOADED          100
#define ERR_INVALID_UID             257
#define ERR_UID_EXIST               258
#define ERR_UID_CHANGED             259
#define ERR_IMG_IDX_START           260
#define ERR_NO_USER_DATA            261
#define ERR_USER_NOT_FOUND          262
#define ERR_DB_DELETE_FAIL          263
#define ERR_NO_MATCHED_USR          10
#define ERR_INFER_FAILURE           256

void error_code_2_str(int code, char* buf, int len);

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif

#endif
