/**
 * @file      SyncMsgHandler.cpp
 * @brief     implementation file for sync message handler
 * @version   0.1 - 2019-04-23
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include "SyncMsgHandler.h"
#include "UARTDev.h"
#include "USBDev.h"
#include "MsgMgmt.h"
#include "KnLog.h"
#include "KnUtil.h"

#include <vector>
using namespace std;

extern MsgMgmt*                _p_mgr; //msg manager

SyncMsgHandler::SyncMsgHandler() {
}

SyncMsgHandler::~SyncMsgHandler() {
    //loop for pending rsp msg
    for(int i = 0; i < MAX_COM_DEV; i++) {
        _map_mtx[i].lock();
        map<uint16_t, KnBaseMsg*>::iterator iter;
        for(iter = _runing_map[i].begin(); iter != _runing_map[i].end(); ++iter) {
            KnBaseMsg* p = iter->second;
            if(p) delete p;
        }
        _map_mtx[i].unlock();
    }
}

KnBaseMsg*  SyncMsgHandler::wait_rsp_msg(int dev_idx, uint16_t msg_type) {
    map<uint16_t, KnBaseMsg*>::iterator iter;
    KnBaseMsg* p_rsp = NULL;
    for(int i = 0; i < KDP_SYNC_WAIT_NUM; i++)  {
        //lock
        _map_mtx[dev_idx].lock();
        //get the iter
        iter = _runing_map[dev_idx].find(msg_type);

        //if could not find
        if(iter == _runing_map[dev_idx].end()) {
            _map_mtx[dev_idx].unlock(); //unlock
            KnTsPrint(KN_LOG_ERR, "no req msg in the local map, no need wait:%d.\n", msg_type);
            return NULL;
        }

        //if no message coming
        if(iter->second == NULL) {
            _map_mtx[dev_idx].unlock(); //unlock

            _cond_mtx.lock();
            //wait for msg
            _cond_wait.wait_for(_cond_mtx, std::chrono::milliseconds(KDP_SYNC_WAIT_TIME));
            _cond_mtx.unlock();
            continue; //wait
        } else {
            p_rsp = iter->second;
            _runing_map[dev_idx].erase(iter);
            _map_mtx[dev_idx].unlock();
            KnTsPrint(KN_LOG_DBG, "rsp msg from device received:0x%x.\n", msg_type);
            return p_rsp; //return msg
        }
    }

    KnTsPrint(KN_LOG_ERR, "waiting for rsp timeout.\n");
    return NULL; //timeout
}

int SyncMsgHandler::kdp_query_app(int dev_idx, uint32_t* app_ids, int* size) {
    uint16_t msg_type = KN_OPR_QUERY_APPS_REQ;
    uint16_t msg_len = 0;

    //create req message
    KnOprQueryAppReq* p_req = new KnOprQueryAppReq(msg_type, msg_len);

    //sending msg to queue
    KnTsPrint(KN_LOG_DBG, "sending query app to dev ........\n");
    int res = _p_mgr->send_cmd_msg_sync(dev_idx, p_req);
    if(res < 0) {
        KnPrint(KN_LOG_ERR, "sending query app request to device failed:%d.\n", res);
        return -1;
    }

    KnTsPrint(KN_LOG_DBG, "saving query app req to local map.\n");
    save_req_to_map(msg_type);

    //wait for rsp
    KnTsPrint(KN_LOG_DBG, "waiting for query app rsp from device ........\n");
    KnBaseMsg* p_rsp = wait_rsp_msg(dev_idx, msg_type);
    if(!p_rsp) {
        KnPrint(KN_LOG_ERR, "query app response message was not received.\n");
        return -1;
    }
    KnTsPrint(KN_LOG_DBG, "query app rsp from device received.\n");

    //extract msg content
    KnOprQueryAppRsp* query_msg = (KnOprQueryAppRsp*)p_rsp;
    *size = query_msg->_len;
    uint32_t* p = query_msg->_p_app_id;

    for(int i = 0; i < *size; i++) {
        KnTsPrint(KN_LOG_DBG, "app id:%d from query app rsp. \n", p[i]);
        app_ids[i] = p[i];
    }

    //free resposne memory
    delete p_rsp;
    return 0;
}

int SyncMsgHandler::kdp_select_app(int dev_idx, uint32_t app_id, uint32_t* rsp_code, uint32_t* buf_size) {
    uint16_t msg_type = KN_OPR_SELECT_APP_REQ;
    uint16_t msg_len = sizeof(uint32_t);

    //create req message
    KnOprSelectAppReq* p_req = new KnOprSelectAppReq(msg_type, msg_len, app_id);

    //sending msg to queue
    KnTsPrint(KN_LOG_DBG, "sending select app to dev ........\n");
    int res = _p_mgr->send_cmd_msg_sync(dev_idx, p_req);
    if(res < 0) {
        KnPrint(KN_LOG_ERR, "sending select app request to device failed:%d.\n", res);
        return -1;
    }

    KnTsPrint(KN_LOG_DBG, "saving select app req to local map.\n");
    save_req_to_map(msg_type);

    //wait for rsp
    KnTsPrint(KN_LOG_DBG, "waiting for select app rsp from device ........\n");
    KnBaseMsg* p_rsp = wait_rsp_msg(dev_idx, msg_type);
    if(!p_rsp) {
        KnPrint(KN_LOG_ERR, "select app response message was not received.\n");
        return -1;
    }
    KnTsPrint(KN_LOG_DBG, "select app rsp from device received.\n");

    //extract msg content
    KnOprSelectAppRsp* sel_app_msg = (KnOprSelectAppRsp*)p_rsp;
    *rsp_code = sel_app_msg->_rsp_code;
    *buf_size = sel_app_msg->_img_size;

    KnTsPrint(KN_LOG_DBG, "rsp_code:%d, buf size:%d from select app rsp.\n", 
            sel_app_msg->_rsp_code, sel_app_msg->_img_size);
    //free resposne memory
    delete p_rsp;
    return 0;
}

int SyncMsgHandler::kdp_sfid_start(int dev_idx, uint32_t* rsp_code, uint32_t* img_size) {
    uint16_t msg_type = KN_APP_SFID_START_REQ;
    uint16_t msg_len = sizeof(uint32_t);

    //create req message
    KnSFIDStartReq* p_req = new KnSFIDStartReq(msg_type, msg_len, 0);
    if(!p_req->is_msg_valid()) {
        KnPrint(KN_LOG_ERR, "create sfid start req failed.\n");
        delete p_req;
        return -1;
    }

    //sending msg to queue
    KnTsPrint(KN_LOG_DBG, "sending sfid start to dev ........\n");
    int res = _p_mgr->send_cmd_msg_sync(dev_idx, p_req);
    if(res < 0) {
        KnPrint(KN_LOG_ERR, "sending sfid start request to device failed:%d.\n", res);
        return -1;
    }

    KnTsPrint(KN_LOG_DBG, "saving sfid start req to local map.\n");
    save_req_to_map(msg_type);

    //wait for rsp
    KnTsPrint(KN_LOG_DBG, "waiting for sfid start rsp from device ........\n");
    KnBaseMsg* p_rsp = wait_rsp_msg(dev_idx, msg_type);
    if(!p_rsp) {
        KnPrint(KN_LOG_ERR, "sfid start response message was not received.\n");
        return -1;
    }
    KnTsPrint(KN_LOG_DBG, "sfid start rsp from device received.\n");

    //extract msg content
    KnSFIDStartRsp* sfid_msg = (KnSFIDStartRsp*)p_rsp;
    *rsp_code = sfid_msg->_rsp_code;
    *img_size = sfid_msg->_img_size;

    KnTsPrint(KN_LOG_DBG, "rsp_code:%d, img size:%d from sfid img rsp.\n", sfid_msg->_rsp_code, sfid_msg->_img_size);
    //free resposne memory
    delete p_rsp;
    return 0;
}

#if 0
int SyncMsgHandler::kdp_sfid_new_user_req(int dev_idx, uint32_t usr_id, uint32_t img_idx) {
    uint16_t msg_type = KN_APP_SFID_NEW_USER;
    uint16_t msg_len = sizeof(uint32_t) + sizeof(uint32_t);

    //create req message
    KnSFIDNewUserReq* p_req = new KnSFIDNewUserReq(msg_type, msg_len, usr_id, img_idx);

    //sending msg to queue
    KnTsPrint(KN_LOG_DBG, "sending sfid new user req to dev ........\n");
    int res = _p_mgr->send_cmd_msg_sync(dev_idx, p_req);
    if(res < 0) {
        KnPrint(KN_LOG_DBG, "sending sfid new user request to device failed:%d.\n", res);
        return -1;
    }
    KnTsPrint(KN_LOG_DBG, "sending sfid new user to dev completed. \n");
    return 0;
}
#endif

int SyncMsgHandler::kdp_sfid_new_user(int dev_idx, uint16_t usr_id, uint16_t img_idx) {
    uint16_t msg_type = KN_APP_SFID_NEW_USER;
    uint16_t msg_len = sizeof(uint32_t) + sizeof(uint32_t);

    //create req message
    KnSFIDNewUserReq* p_req = new KnSFIDNewUserReq(msg_type, msg_len, usr_id, img_idx);
    if(!p_req->is_msg_valid()) {
        KnPrint(KN_LOG_ERR, "create sfid new user req failed.\n");
        delete p_req;
        return -1;
    }

    //sending msg to queue
    KnTsPrint(KN_LOG_DBG, "sending sfid new user to dev ........\n");
    int res = _p_mgr->send_cmd_msg_sync(dev_idx, p_req);
    if(res < 0) {
        KnPrint(KN_LOG_ERR, "sending sfid new user request to device failed:%d.\n", res);
        return -1;
    }

    KnTsPrint(KN_LOG_DBG, "saving sfid new user req to local map.\n");
    save_req_to_map(msg_type);

    //wait for rsp
    KnTsPrint(KN_LOG_DBG, "waiting for sfid new user rsp from device ........\n");
    KnBaseMsg* p_rsp = wait_rsp_msg(dev_idx, msg_type);
    if(!p_rsp) {
        KnPrint(KN_LOG_ERR, "sfid new user response message was not received.\n");
        return -1;
    }
    KnTsPrint(KN_LOG_DBG, "sfid new user rsp from device received.\n");

    //free resposne memory
    delete p_rsp;
    return 0;
}

int SyncMsgHandler::kdp_sfid_register(int dev_idx, uint32_t usr_id, uint32_t* rsp_code) {
    uint16_t msg_type = KN_APP_SFID_REGISTER;
    uint16_t msg_len = sizeof(uint32_t);

    //create req message
    KnSFIDRegisterReq* p_req = new KnSFIDRegisterReq(msg_type, msg_len, usr_id);
    if(!p_req->is_msg_valid()) {
        KnPrint(KN_LOG_ERR, "create sfid register req failed.\n");
        delete p_req;
        return -1;
    }

    //sending msg to queue
    KnTsPrint(KN_LOG_DBG, "sending sfid register to dev ........\n");
    int res = _p_mgr->send_cmd_msg_sync(dev_idx, p_req);
    if(res < 0) {
        KnPrint(KN_LOG_DBG, "sending sfid register request to device failed:%d.\n", res);
        return -1;
    }

    KnTsPrint(KN_LOG_DBG, "saving sfid register req to local map.\n");
    save_req_to_map(msg_type);

    //wait for rsp
    KnTsPrint(KN_LOG_DBG, "waiting for sfid register rsp from device ........\n");
    KnBaseMsg* p_rsp = wait_rsp_msg(dev_idx, msg_type);
    if(!p_rsp) {
        KnPrint(KN_LOG_ERR, "sfid register response message was not received.\n");
        return -1;
    }
    KnTsPrint(KN_LOG_DBG, "sfid register rsp from device received.\n");

    //extract msg content
    KnSFIDRegisterRsp* sfid_msg = (KnSFIDRegisterRsp*)p_rsp;
    *rsp_code = sfid_msg->_rsp_code;

    KnTsPrint(KN_LOG_DBG, "rsp_code:%d, from sfid register rsp.\n", sfid_msg->_rsp_code);

    //free resposne memory
    delete p_rsp;
    return 0;
}

int SyncMsgHandler::kdp_sfid_del_db(int dev_idx, uint32_t usr_id, uint32_t* rsp_code) {
    uint16_t msg_type = KN_APP_SFID_DEL_DB;
    uint16_t msg_len = sizeof(uint32_t);

    //create req message
    KnSFIDDelDBReq* p_req = new KnSFIDDelDBReq(msg_type, msg_len, usr_id);
    if(!p_req->is_msg_valid()) {
        KnPrint(KN_LOG_ERR, "create sfid del db req failed.\n");
        delete p_req;
        return -1;
    }

    //sending msg to queue
    KnTsPrint(KN_LOG_DBG, "sending sfid del db to dev ........\n");
    int res = _p_mgr->send_cmd_msg_sync(dev_idx, p_req);
    if(res < 0) {
        KnPrint(KN_LOG_ERR, "sending sfid del db request to device failed:%d.\n", res);
        return -1;
    }

    KnTsPrint(KN_LOG_DBG, "saving sfid del db req to local map.\n");
    save_req_to_map(msg_type);

    //wait for rsp
    KnTsPrint(KN_LOG_DBG, "waiting for sfid del db rsp from device ........\n");
    KnBaseMsg* p_rsp = wait_rsp_msg(dev_idx, msg_type);
    if(!p_rsp) {
        KnPrint(KN_LOG_ERR, "sfid del db response message was not received.\n");
        return -1;
    }
    KnTsPrint(KN_LOG_DBG, "sfid del db rsp from device received.\n");

    //extract msg content
    KnSFIDDelDBRsp* sfid_msg = (KnSFIDDelDBRsp*)p_rsp;
    *rsp_code = sfid_msg->_rsp_code;

    KnTsPrint(KN_LOG_DBG, "rsp_code:%d from sfid del db rsp.\n", sfid_msg->_rsp_code);
    //free resposne memory
    delete p_rsp;
    return 0;
}

int SyncMsgHandler::kdp_sfid_send_img(int dev_idx, char* img_buf, int buf_len, \
                uint32_t* rsp_code, uint16_t* user_id, uint16_t* mode) {
    uint16_t msg_type = KN_APP_SFID_SEND_IMG;
    uint16_t msg_len = sizeof(uint32_t);

    BaseDev* pDev = _p_mgr->getDevFromIdx(dev_idx);
    if(!pDev) {
        KnPrint(KN_LOG_ERR, "could not get device for idx:%d.\n", dev_idx);
        return -1;
    }
    UARTDev* pUart = (UARTDev*)pDev;
    USBDev* pUsb = pUart->get_helper_dev();
    if(!pUsb) {
        KnPrint(KN_LOG_ERR, "could not get helper device for idx:%d.\n", dev_idx);
        return -1;
    }

    //create req message
    KnSFIDSendImageReq* p_req = new KnSFIDSendImageReq(msg_type, msg_len, buf_len);
    if(!p_req->is_msg_valid()) {
        KnPrint(KN_LOG_ERR, "create sfid send img req failed.\n");
        delete p_req;
        return -1;
    }

    //sending msg to queue
    KnTsPrint(KN_LOG_DBG, "sending sfid img to dev ........\n");
    int res = _p_mgr->send_cmd_msg_sync(dev_idx, p_req);
    if(res < 0) {
        KnPrint(KN_LOG_ERR, "sending sfid send img request to device failed:%d.\n", res);
        return -1;
    }

    KnTsPrint(KN_LOG_DBG, "saving sfid img req to local map.\n");
    save_req_to_map(msg_type, dev_idx);

    //to call usb interface to transfer image file.
    KnTsPrint(KN_LOG_DBG, "calling usb interface to transfer file:........\n");
    int snd_len = pUsb->send_image_data(img_buf, buf_len);

    KnTsPrint(KN_LOG_DBG, "file transfer completed :%d.\n", snd_len);

    //wait for rsp
    KnTsPrint(KN_LOG_DBG, "waiting for sfid img rsp from device ........\n");
    KnBaseMsg* p_rsp = wait_rsp_msg(dev_idx, msg_type);
    if(!p_rsp) {
        KnPrint(KN_LOG_ERR, "sfid send img response message was not received.\n");
        return -1;
    }
    KnTsPrint(KN_LOG_DBG, "sfid img rsp from device received.\n");

    //extract msg content
    KnSFIDSendImageRsp* sfid_msg = (KnSFIDSendImageRsp*)p_rsp;
    *rsp_code = sfid_msg->_rsp_code;
    *user_id = sfid_msg->_uid;
    *mode = sfid_msg->_mode;

    KnTsPrint(KN_LOG_DBG, "rsp_code:%d, user_id:%d, mode:%d from sfid img rsp.\n", 
            sfid_msg->_rsp_code, sfid_msg->_uid, sfid_msg->_mode);
    //free resposne memory
    delete p_rsp;
    return 0;
}

void SyncMsgHandler::save_req_to_map(uint16_t req_type, int idx) {
    _map_mtx[idx].lock();

    map<uint16_t, KnBaseMsg*>::iterator iter = _runing_map[idx].find(req_type);
    if(iter != _runing_map[idx].end()) {
        KnBaseMsg* p = iter->second;
        iter->second = NULL;
        _map_mtx[idx].unlock();

        KnPrint(KN_LOG_DBG, "req msg %d exist in map for dev:%d.\n", req_type, idx);
        if(p) delete p; //free the old one
    } else {
        _runing_map[idx][req_type] = NULL;
        _map_mtx[idx].unlock();
    }
}

int SyncMsgHandler::msg_received_rx_queue(KnBaseMsg* p_rsp, uint16_t req_type, int idx) {
    if(idx < 0 || idx >= MAX_COM_DEV) {
        KnPrint(KN_LOG_ERR, "invalid dev idx received from rx queue:%d.\n", idx);
        return -1;
    }

    KnTsPrint(KN_LOG_DBG, "msg 0x%x received from rx queue for dev:%d.\n", req_type, idx);
    map<uint16_t, KnBaseMsg*>::iterator iter;

    _map_mtx[idx].lock();
    iter = _runing_map[idx].find(req_type);

    if(iter == _runing_map[idx].end()) {
        _map_mtx[idx].unlock();

        KnTsPrint(KN_LOG_DBG, "no req msg is waiting for this rsp:0x%x.\n", req_type);
        delete p_rsp; //ignore it;
        return 0;
    } else {
        KnBaseMsg* p_old = iter->second;
        iter->second = p_rsp;
        _map_mtx[idx].unlock();

        if(p_old) {
            KnPrint(KN_LOG_DBG, "msg %d exist in map for dev:%d.\n", req_type, idx);
            delete p_old; //free the msg
        }
    }

    KnTsPrint(KN_LOG_DBG, "sync msg handler notifying wait thread ........\n");
    //notify
    _cond_mtx.lock();
    _cond_wait.notify_all();
    _cond_mtx.unlock();

    return 0;
}

