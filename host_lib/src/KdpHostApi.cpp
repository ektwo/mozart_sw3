/**
 * @file      KdpHostApi.cpp
 * @brief     kdp host api source file
 * @version   0.1 - 2019-04-19
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include <string.h>
#include <unistd.h>

#include "KdpHostApi.h"
#include "err_code.h"
#include "MsgMgmt.h"
#include "KnLog.h"
#include "KnUtil.h"
#include "UARTDev.h"

#include <thread>
#include <vector>
using namespace std;

MsgMgmt*                _p_mgr = NULL; //msg manager
AsyncMsgHandler*        _async_hdlr = NULL;
SyncMsgHandler*         _sync_hdlr = NULL;

static bool is_valid_dev(int dev_idx) {
    if(!_p_mgr) return false;
    return _p_mgr->is_valid_dev(dev_idx);
}

//init the host lib
int kdp_lib_init() {
    //_async_hdlr = new AsyncMsgHandler();
    _sync_hdlr = new SyncMsgHandler();
    _p_mgr = new MsgMgmt(_async_hdlr, _sync_hdlr);

    KnPrint(KN_LOG_DBG, "msg handler and msg mgr created.\n");
    return 0;
}

//start the host lib
int kdp_lib_start() {
    if(!_p_mgr) return -1;
    //start receiver, sender thread
    _p_mgr->start_receiver();

    //_p_mgr->start_sender();

    //start msg handling thread
    _p_mgr->start_handler();

    KnPrint(KN_LOG_DBG, "handler thread and receiver thread started.\n");
    return 0;
}

//init the lib log
//dir: directory of the log file
//name: file name
int kdp_init_log(const char* dir, const char* name) {
    string dirs(dir);
    string names(name);
    KnLogInit(dirs, names);
    KnSetLogLevel(KN_LOG_ALL);

    KnPrint(KN_LOG_DBG, "\n\n\n************----**********\n");
    KnPrint(KN_LOG_DBG, " -- kdp host lib restarted -- \n");
    KnPrint(KN_LOG_DBG, "************----**********\n\n\n");

    KnPrint(KN_LOG_DBG, "init host lib log to %s%s.\n", dir, name);
    return 0;
}

//add device to lib
//currently only UART is supported.
//name: device name
int kdp_add_dev(int type, const char* name) {
    if(type != KDP_UART_DEV) return -1;
    
    string u_name(name);
    BaseDev* pDev = new UARTDev(u_name);

    if(pDev->get_dev_fd() < 0) {
        KnPrint(KN_LOG_ERR, "dev:%s could not be opened.\n", name);
        return -1;
    }

    pDev->set_baudrate(115200);
    pDev->set_parity();

    KnPrint(KN_LOG_DBG, "adding dev for:%s...\n", name);
    return _p_mgr->add_device(pDev);
}

//free the resources
int kdp_lib_de_init() {
    if(_p_mgr) {
        _p_mgr->stop_receiver();
        //_p_mgr->stop_sender();
        _p_mgr->stop_handler();

        for(int i = 0; i < 1000; i++) {
            usleep(1000);
            if(_p_mgr->is_receiver_running() == false &&
              //_p_mgr->is_sender_running() == false && 
                _p_mgr->is_handler_running() == false) break;
        }
    }

    usleep(1000);
    KnPrint(KN_LOG_DBG, "threads stopped, starting deconstructing objs...\n");
    if(_p_mgr) delete _p_mgr;
    if(_async_hdlr) delete _async_hdlr;
    if(_sync_hdlr) delete _sync_hdlr;

    //close log
    KnLogClose();
    return 0;
}

int kdp_query_app_sync(int dev_idx, uint32_t* app_ids, int* size) {
    if(!_sync_hdlr) return -1;
    return _sync_hdlr->kdp_query_app(dev_idx, app_ids, size);
}

int kdp_select_app_sync(int dev_idx, uint32_t app_id, uint32_t* rsp_code, uint32_t* buf_size) {
    if(!_sync_hdlr) return -1;
    return _sync_hdlr->kdp_select_app(dev_idx, app_id, rsp_code, buf_size);
}

int kdp_sfid_start_sync(int dev_idx, uint32_t* rsp_code, uint32_t* img_size) {
    if(!_sync_hdlr) return -1;
    return _sync_hdlr->kdp_sfid_start(dev_idx, rsp_code, img_size);
}

int kdp_sfid_new_user_sync(int dev_idx, uint16_t usr_id, uint16_t img_idx) {
    if(!_sync_hdlr) return -1;
    return _sync_hdlr->kdp_sfid_new_user(dev_idx, usr_id, img_idx);
}

int kdp_sfid_register_sync(int dev_idx, uint32_t usr_id, uint32_t* rsp_code) {
    if(!_sync_hdlr) return -1;
    return _sync_hdlr->kdp_sfid_register(dev_idx, usr_id, rsp_code);
}

int kdp_sfid_del_db_sync(int dev_idx, uint32_t usr_id, uint32_t* rsp_code) {
    if(!_sync_hdlr) return -1;
    return _sync_hdlr->kdp_sfid_del_db(dev_idx, usr_id, rsp_code);
}

int kdp_sfid_send_img_sync(int dev_idx, char* img_buf, int buf_len, uint32_t* rsp_code, \
                uint16_t* user_id, uint16_t* mode) {
    if(!_sync_hdlr) return -1;
    
    return _sync_hdlr->kdp_sfid_send_img(dev_idx, img_buf, buf_len, rsp_code, user_id, mode);
}

int kdp_start_verify_mode(int dev_idx, uint32_t* img_size) {
    KnPrint(KN_LOG_DBG, "\n\ncalling start verify mode...:%d.\n", dev_idx);
    if(!is_valid_dev(dev_idx)) {
        KnPrint(KN_LOG_ERR, "dev idx is invalid.\n");
        return -1;
    }
    if(!img_size) {
        KnPrint(KN_LOG_ERR, "img size is null.\n");
        return -1;
    }

    uint32_t rsp_code = 0;
    int ret = kdp_sfid_start_sync(dev_idx, &rsp_code, img_size);
    if(ret < 0) {
        KnPrint(KN_LOG_ERR, "start verify mode failed with error:%d.\n", ret);
        return -1;
    }

    if(rsp_code != 0) {
        KnPrint(KN_LOG_ERR, "start verify mode failed with error:%d.\n", rsp_code);
        return rsp_code;
    }
    return 0;
}

int kdp_verify_user_id(int dev_idx, uint16_t* user_id, char* img_buf, int buf_len) {
    KnPrint(KN_LOG_DBG, "\n\ncalling verify user id...:%d,%d.\n", dev_idx, buf_len);
    if(!is_valid_dev(dev_idx)) {
        KnPrint(KN_LOG_ERR, "dev idx is invalid.\n");
        return -1;
    }
    if(!user_id || !img_buf || buf_len <= 0) {
        KnPrint(KN_LOG_ERR, "verify user id input parameter is invalid.\n");
        return -1;
    }

    uint32_t rsp_code = 0;
    uint16_t mode = 1;
    int ret = kdp_sfid_send_img_sync(dev_idx, img_buf, buf_len, &rsp_code, user_id, &mode);
    if(ret < 0) {
        KnPrint(KN_LOG_ERR, "verify user id failed with error:%d.\n", ret);
        return -1;
    }

    if(rsp_code != 0) {
        KnPrint(KN_LOG_ERR, "verify user id failed with error:%d.\n", rsp_code);
        return rsp_code;
    }

    if(mode != 0) {
        KnPrint(KN_LOG_ERR, "device is not in user verify mode.\n");
        return -1;
    }

    KnPrint(KN_LOG_DBG, "verify user id completed, user id:%d.\n", *user_id);
    if(*user_id < 1 || *user_id > 15) {
        KnPrint(KN_LOG_ERR, "invalid user id received:%d.\n", *user_id);
        return -1;
    }
    return 0;
}

int kdp_start_reg_user_mode(int dev_idx, uint16_t usr_id, uint16_t img_idx) {
    KnPrint(KN_LOG_DBG, "\n\ncalling start reg user mode...:%d,%d.\n", usr_id, img_idx);
    if(!is_valid_dev(dev_idx)) {
        KnPrint(KN_LOG_ERR, "dev idx is invalid.\n");
        return -1;
    }
    if(usr_id < 1 || usr_id > 15 || img_idx < 1 || img_idx > 5) {
        KnPrint(KN_LOG_ERR, "new user input parameter is invalid.\n");
        return -1;
    }

    int ret =  kdp_sfid_new_user_sync(dev_idx, usr_id, img_idx);
    if(ret < 0) {
        KnPrint(KN_LOG_ERR, "start reg mode failed with error:%d.\n", ret);
        return -1;
    }

    return 0;
}

int kdp_extract_feature(int dev_idx, char* img_buf, int buf_len) {
    KnPrint(KN_LOG_DBG, "\n\ncalling extract feature...:%d,%d.\n", dev_idx, buf_len);
    if(!is_valid_dev(dev_idx)) {
        KnPrint(KN_LOG_ERR, "dev idx is invalid.\n");
        return -1;
    }
    if(!img_buf || buf_len <= 0) {
        KnPrint(KN_LOG_ERR, "extract feature input parameter is invalid.\n");
        return -1;
    }

    uint32_t rsp_code = 0;
    uint16_t img_idx = 0;
    uint16_t mode = 0;
    int ret = kdp_sfid_send_img_sync(dev_idx, img_buf, buf_len, &rsp_code, &img_idx, &mode);
    if(ret < 0) {
        KnPrint(KN_LOG_ERR, "extract feature failed with error:%d.\n", ret);
        return -1;
    }

    if(rsp_code != 0) {
        KnPrint(KN_LOG_ERR, "extract feature failed with error:%d.\n", rsp_code);
        return rsp_code;
    }

    if(mode == 0) {
        KnPrint(KN_LOG_ERR, "device is not in register mode.\n");
        return -1;
    }

    KnPrint(KN_LOG_DBG, "extract feature completed, img index:%d.\n", img_idx);
    if(img_idx < 1 || img_idx > 5) {
        KnPrint(KN_LOG_ERR, "invalid image received:%d.\n", img_idx);
        return -1;
    }
    return 0;
}

int kdp_register_user(int dev_idx, uint32_t user_id) {
    KnPrint(KN_LOG_DBG, "\n\ncalling register user...:%d,%d.\n", dev_idx, user_id);
    if(!is_valid_dev(dev_idx)) {
        KnPrint(KN_LOG_ERR, "dev idx is invalid.\n");
        return -1;
    }
    if(user_id < 1 || user_id > 15) {
        KnPrint(KN_LOG_ERR, "reg user input parameter is invalid.\n");
        return -1;
    }

    uint32_t rsp_code = 0;
    int ret = kdp_sfid_register_sync(dev_idx, user_id, &rsp_code);
    if(ret < 0) {
        KnPrint(KN_LOG_ERR, "reg user failed with error:%d.\n", ret);
        return -1;
    }

    if(rsp_code != 0) {
        KnPrint(KN_LOG_ERR, "reg user failed with error:%d.\n", rsp_code);
        return rsp_code;
    }

    KnPrint(KN_LOG_DBG, "reg user completed, user id:%d.\n", user_id);
    return 0;
}

int kdp_remove_user(int dev_idx, uint32_t user_id) {
    KnPrint(KN_LOG_DBG, "\n\ncalling remove user...:%d,%d.\n", dev_idx, user_id);
    if(!is_valid_dev(dev_idx)) {
        KnPrint(KN_LOG_ERR, "dev idx is invalid.\n");
        return -1;
    }
    if(user_id < 0 || user_id > 15) {
        KnPrint(KN_LOG_ERR, "del user input parameter is invalid.\n");
        return -1;
    }

    uint32_t rsp_code = 0;
    int ret = kdp_sfid_del_db_sync(dev_idx, user_id, &rsp_code);
    if(ret < 0) {
        KnPrint(KN_LOG_ERR, "remove all user failed for dev:%d, with error:%d.\n", dev_idx, ret);
        return -1;
    }

    if(rsp_code != 0) {
        KnPrint(KN_LOG_ERR, "remove all user failed for dev:%d, with error:%d.\n", dev_idx, rsp_code);
        return rsp_code;
    }
    return 0;
}

void error_code_2_str(int code, char* buf, int len) {
    memset(buf, 0, len);

    switch(code) {
    case GEN_ERROR:
        snprintf(buf, len-1, "General error");
        break;

    case ERR_APP_NOT_LOADED:
        snprintf(buf, len-1, "SFID application not loaded");
        break;

    case ERR_INVALID_UID:
        snprintf(buf, len-1, "Invalid User ID or image index specified");
        break;

    case ERR_UID_EXIST:
        snprintf(buf, len-1, "User ID already exists");
        break;

    case ERR_UID_CHANGED:
        snprintf(buf, len-1, "User ID changed from the New User session");
        break;

    case ERR_IMG_IDX_START:
        snprintf(buf, len-1, "User ID changed from the New User session");
        break;

    case ERR_NO_USER_DATA:
        snprintf(buf, len-1, "No feature maps from New User session found");
        break;

    case ERR_USER_NOT_FOUND:
        snprintf(buf, len-1, "User ID not found in DB");
        break;

    case ERR_DB_DELETE_FAIL:
        snprintf(buf, len-1, "DB delete failure");
        break;

    case ERR_NO_MATCHED_USR:
        snprintf(buf, len-1, "No match in User DB");
        break;

    case ERR_INFER_FAILURE:
        snprintf(buf, len-1, "No successful inference (FD-FR)");
        break;

    default:
        snprintf(buf, len-1, "error code not found");
        break;
    }
    return;
}
