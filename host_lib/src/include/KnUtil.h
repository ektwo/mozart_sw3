/**
 * @file      KnUtil.h
 * @brief     utility header file
 * @version   0.1 - 2019-04-29
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __KN_UTIL__H__
#define __KN_UTIL__H__

#define CRC16_CONSTANT 0x8005

#include <stdint.h>

int copy_file(const char* src, const char* dest);

void kn_dump_msg(int8_t* buf, int len);

uint16_t gen_crc16(int8_t *data, uint16_t size);

#endif

