/**
 * @file      KnMsg.h
 * @brief     Definition for base types
 * @version   0.1 - 2019-04-13
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __KN_MSG_H__
#define __KN_MSG_H__

#include <stdint.h>

#include "MsgId.h"
#include "KdpHostApi.h"

/* 
 * base class for messages
 */
class KnBaseMsg {
public:
    static KnBaseMsg* create_rsp_msg(int8_t* buf, int mlen);
    static int decode_msg_hdr(KnMsgHdr* hdr, int8_t* buf, int mlen);
    static int encode_msg_hdr(KnMsgHdr* hdr, int8_t* buf, int mlen);

    uint16_t get_msg_type() { return hdr.msg_type; }
    uint16_t get_msg_len() { return hdr.msg_len; }
    uint16_t get_req_type(uint16_t msg_type);
    int8_t*  get_msg_payload() { return msg_payload; }

    KnBaseMsg(uint16_t msg_type, uint16_t msg_len);
    KnBaseMsg(int8_t* buf, int mlen);
    virtual ~KnBaseMsg();

    bool is_msg_valid() { return _valid; }

protected:
    int decode_hdr();
    int encode_hdr();

    KnMsgHdr hdr;
    int8_t* msg_payload;
    bool    _valid;
};

//----------------------request message start-----------------------
//base class for req message
//only message type and message length
class KnGenericReq : public KnBaseMsg {
public:
    KnGenericReq(uint16_t msg_type, uint16_t msg_len);
    virtual ~KnGenericReq();
};

//request message for memory read request
class KnTestMemReadReq : public KnGenericReq {
public:
    KnTestMemReadReq(uint16_t msg_type, uint16_t msg_len, uint32_t start_addr, uint32_t num);
    virtual ~KnTestMemReadReq();
    uint32_t _start_addr;
    uint32_t _num_bytes;
};

//request message for memory write request
class KnTestMemWriteReq : public KnTestMemReadReq {
public:
    KnTestMemWriteReq(uint16_t msg_type, uint16_t msg_len, \
                uint32_t start_addr, uint32_t num, int8_t* pdata, int len);
    virtual ~KnTestMemWriteReq();
    int8_t*  _p_data;
    int      _len;
};

//mem clear request is same structure as read req
typedef KnTestMemReadReq KnTestMemClearReq;

//host echo request is same structure as write req
typedef KnTestMemWriteReq KnTestHostEchoReq;

//request message for reset 
class KnCmdResetReq : public KnGenericReq {
public:
    KnCmdResetReq(uint16_t msg_type, uint16_t msg_len, uint32_t rset_mode, uint32_t check_code);
    virtual ~KnCmdResetReq();
    uint32_t _rset_mode;
    uint32_t _check_code;
};

//operation query app requset is same structure as generic req
typedef KnGenericReq KnOprQueryAppReq;

//request message for operation select app req 
class KnOprSelectAppReq : public KnGenericReq {
public:
    KnOprSelectAppReq(uint16_t msg_type, uint16_t msg_len, uint32_t id);
    virtual ~KnOprSelectAppReq();
    uint32_t _app_id;
};

//request message for SFID start req
class KnSFIDStartReq : public KnGenericReq {
public:
    KnSFIDStartReq(uint16_t msg_type, uint16_t msg_len, uint32_t reserved);
    virtual ~KnSFIDStartReq();
    uint32_t _reserved;
};


class KnSFIDNewUserReq : public KnGenericReq {
public:
    KnSFIDNewUserReq(uint16_t msg_type, uint16_t msg_len, uint16_t u_id, uint16_t img_idx);
    virtual ~KnSFIDNewUserReq();
    uint16_t _user_id;
    uint16_t _r0;
    uint16_t _img_idx;
    uint16_t _r1;
};

class KnSFIDRegisterReq : public KnGenericReq {
public:
    KnSFIDRegisterReq(uint16_t msg_type, uint16_t msg_len, uint32_t u_id);
    virtual ~KnSFIDRegisterReq();
    uint32_t _user_id;
};

typedef KnSFIDRegisterReq KnSFIDDelDBReq;
typedef KnSFIDRegisterReq KnSFIDSendImageReq;

//----------------------request message end-----------------------


//----------------------response message start-----------------------
//base class for rsp message
//only message type and message length
class KnGenericAckRsp : public KnBaseMsg {
public:
    KnGenericAckRsp(int8_t* buf, int mlen);
    virtual ~KnGenericAckRsp();
};

//mem request general ack msg
class KnTestMemAck : public KnGenericAckRsp {
public:
    KnTestMemAck(int8_t* buf, int mlen);
    virtual ~KnTestMemAck();

    uint32_t _ack;
    uint32_t _reserved;
};

//response message for memory write
class KnTestMemWriteRsp : public KnGenericAckRsp {
public:
    KnTestMemWriteRsp(int8_t* buf, int mlen);
    virtual ~KnTestMemWriteRsp();

    uint32_t _rsp_result;
    uint32_t _num_bytes;
};

//response message for memory read
class KnTestMemReadRsp : public KnTestMemWriteRsp {
public:
    KnTestMemReadRsp(int8_t* buf, int mlen);
    virtual ~KnTestMemReadRsp();

    int8_t*  _p_data;
    int      _len;
};

//clear response is same structure as write response
typedef KnTestMemWriteRsp KnTestMemClearRsp;

//host echo response is same as mem read response
typedef KnTestMemReadRsp KnTestHostEchoRsp;

//response message for memory write
class KnCmdResetRsp : public KnGenericAckRsp {
public:
    KnCmdResetRsp(int8_t* buf, int mlen);
    virtual ~KnCmdResetRsp();

    uint32_t _err_code;
    uint32_t _reserved;
};

//response message for query app
class KnOprQueryAppRsp : public KnGenericAckRsp {
public:
    KnOprQueryAppRsp(int8_t* buf, int mlen);
    virtual ~KnOprQueryAppRsp();

    uint32_t _reserved;
    uint32_t _reserved2;
    uint32_t*  _p_app_id;
    int      _len;
};

//response message for start sfid
class KnSFIDStartRsp : public KnGenericAckRsp {
public:
    KnSFIDStartRsp(int8_t* buf, int mlen);
    virtual ~KnSFIDStartRsp();

    uint32_t _rsp_code;
    uint32_t _img_size;
};

//response message for send img resp
class KnSFIDSendImageRsp : public KnGenericAckRsp {
public:
    KnSFIDSendImageRsp(int8_t* buf, int mlen);
    virtual ~KnSFIDSendImageRsp();

    uint32_t _rsp_code;
    uint16_t _mode;
    uint16_t _uid;
};

//new user rsp is generic ack
typedef KnGenericAckRsp KnSFIDewUserRsp;

//operation operation select app rsp is same structure as SFID start rsp
typedef KnSFIDStartRsp KnOprSelectAppRsp;

//register rsp, del rsp is same as start sfid rsp
typedef KnSFIDStartRsp KnSFIDRegisterRsp;
typedef KnSFIDStartRsp KnSFIDDelDBRsp;

//----------------------response message end-----------------------

#endif

