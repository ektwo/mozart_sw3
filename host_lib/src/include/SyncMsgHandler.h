/**
 * @file      SyncMsgHandler.h
 * @brief     msg handler header file for sync messages 
 * @version   0.1 - 2019-04-23
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __SYNC_MSG_HANDLER_H__
#define __SYNC_MSG_HANDLER_H__

#define KDP_SYNC_WAIT_TIME      200
//#define KDP_SYNC_WAIT_NUM       25 //5 seconds
#define KDP_SYNC_WAIT_NUM       25 //5 seconds

#include "KdpHostApi.h"
#include "KnMsg.h"

#include <stdint.h>

#include <mutex>
#include <map>
#include <vector>
#include <condition_variable>
using namespace std;

class SyncMsgHandler {
public:
    SyncMsgHandler();
    virtual ~SyncMsgHandler();

    int kdp_query_app(int dev_idx, uint32_t* app_ids, int* size);
    int kdp_select_app(int dev_idx, uint32_t app_id, uint32_t* rsp_code, uint32_t* buf_size);
    int kdp_sfid_start(int dev_idx, uint32_t* rsp_code, uint32_t* img_size);
    int kdp_sfid_new_user(int dev_idx, uint16_t usr_id, uint16_t img_idx);
    int kdp_sfid_register(int dev_idx, uint32_t usr_id, uint32_t* rsp_code);
    int kdp_sfid_del_db(int dev_idx, uint32_t usr_id, uint32_t* rsp_code);
    int kdp_sfid_send_img(int dev_idx, char* img_buf, int buf_len, uint32_t* rsp_code, \
                    uint16_t* user_id, uint16_t* mode);
//    int kdp_sfid_new_user_req(int dev_idx, uint32_t usr_id, uint32_t img_idx);

    int msg_received_rx_queue(KnBaseMsg* p_rsp, uint16_t req_type, int idx);

private:
    //local db for saving the command responses and its mutex.
    //for each device, there is one mutex and one map.
    //the index is same as in _com_devs
    mutex                       _map_mtx[MAX_COM_DEV];
    map<uint16_t, KnBaseMsg*>   _runing_map[MAX_COM_DEV];

    //wait for rsp msg and its mutex
    mutex                        _cond_mtx;
    std::condition_variable_any  _cond_wait;

    KnBaseMsg*  wait_rsp_msg(int dev_idx, uint16_t msg_type);
    void save_req_to_map(uint16_t req_type, int idx = 0);

};

#endif
