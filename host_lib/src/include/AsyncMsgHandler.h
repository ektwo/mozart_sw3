/**
 * @file      AsyncMsgHandler.h
 * @brief     msg handler header file for async messages 
 * @version   0.1 - 2019-04-23
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __ASYNC_MSG_HANDLER_H__
#define __ASYNC_MSG_HANDLER_H__

#define KDP_MSG_WAIT_TIME      200
#define SYS_WAIT_RESET         2

#include "KdpHostApi.h"
#include "KnMsg.h"

#include <stdint.h>

#include <mutex>
#include <map>
#include <vector>
#include <condition_variable>
using namespace std;

//local store for command status.
//in case of msg sent out, sts is ongoing
//in case of rsp got, sts is complt
class KdpCmdStatus {
public:
    KdpCmdStatus();
    virtual ~KdpCmdStatus();
    KnBaseMsg* _p_rsp; //pointer to a rsp msg
    int         _sts;
    KdpCallBack _func; //call back func registered by user
};

class AsyncMsgHandler {
public:
    AsyncMsgHandler();
    virtual ~AsyncMsgHandler();

    int kdp_mem_test_read(int dev_idx, uint32_t start_addr, uint32_t num_bytes, KdpCallBack func);
    int kdp_mem_test_write(int dev_idx, uint32_t start_addr, uint32_t num_bytes, int8_t* p_data, int d_len, KdpCallBack func);
    int kdp_mem_test_clear(int dev_idx, uint32_t start_addr, uint32_t num_bytes, KdpCallBack func);
    int kdp_mem_test_echo(int dev_idx, uint32_t reserved, uint32_t num_bytes, int8_t* p_data, int d_len, KdpCallBack func);
    int kdp_cmd_reset(int dev_idx, uint32_t rset_mode, KdpCallBack func);
    void  handle_notification();
    int msg_received_rx_queue(KnBaseMsg* p_rsp, uint16_t req_type, int idx);

private:
    //local db for saving the commands running status, and its mutex.
    //for each device, there is one mutex and one map.
    //the index is same as in _com_devs
    mutex                   _map_mtx[MAX_COM_DEV];
    map<int, KdpCmdStatus>  _runing_map[MAX_COM_DEV];

    //wait for rsp msg and its mutex
    mutex                        _cond_mtx;
    std::condition_variable_any  _cond_wait;

    int save_to_local_db(int dev_idx, int msg_id, KdpCallBack func);
    int fetch_rsp_msg(int& dev_idx);
    void handle_rsp_msg(int dev_idx, int msgid);
    void wait_for_msgs();
    void init_protocl_msg_stack(int dev_idx);
};

#endif
