/**
 * @file      MsgXBase.h
 * @brief     msg tranceiver base class header file
 * @version   0.1 - 2019-04-15
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __MSG_XBASE_H__
#define __MSG_XBASE_H__

#define MAX_QUEUE_SIZE 128
#define MAX_MSG_LEN      2048

#include "KnMsg.h"
#include "KdpHostApi.h"
#include "BaseDev.h"

#include <queue>
#include <mutex>
#include <condition_variable>

using namespace std;

struct MsgQ {
    queue<KnBaseMsg*> rtx_q;
};


class MsgMgmt;

class MsgXBase {
public:
    MsgMgmt* p_mgr;

    virtual void start();
    virtual void stop(); 
    bool is_running() { return running_sts; }

    virtual void run() = 0; //loop for Rx/Tx msgs
    virtual int send_cmd_msg(KnBaseMsg* p_msg, BaseDev* pDev); 
    virtual int send_cmd_msg_sync(KnBaseMsg* p_msg, BaseDev* pDev);
    virtual KnBaseMsg* rcv_cmd_rsp(int& idx);

    int add_device_rtx(int fd, int idx) { 
        dev_fds[idx] = fd; 
        return idx;
    }

    int get_index_from_fd(int fd);

    MsgXBase(MsgMgmt* p_ref);
    virtual ~MsgXBase();

protected:
    bool running_sts;
    bool stopped;

    static void thrd_func(MsgXBase* p_inst);

    bool is_msg_in_rtx_queue();
    int  add_msg_to_rtx_queue(int idx, KnBaseMsg* p_msg);
    KnBaseMsg* fetch_msg_from_rtx_queue(int& fd);

    MsgQ      msg_q[MAX_COM_DEV];
    int       dev_fds[MAX_COM_DEV];

    std::mutex                      _q_mtx;
    std::condition_variable_any     _q_cond_var;
};

#endif
