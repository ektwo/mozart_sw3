/**
 * @file      USBDev.h
 * @brief     Header file for USB Dev class
 * @version   0.1 - 2019-04-28
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __USB_DEV_H__
#define __USB_DEV_H__

#include <usb.h>

#include "BaseDev.h"

#define VENDOR_ID   0x0d7d
#define PRODUCT_ID  0x0100
#define EP_IN       0x81
#define EP_OUT      0x01
#define ACK_PACKET_SIZE 8
#define USB_MAX_BYTES 1024 * 8

using namespace std;

class USBDev : public BaseDev {
public:
    USBDev(int vid, int pid);
    virtual ~USBDev();

    int u_open();
    int u_close();
    int u_init_port();
    int u_sync_dev();

    virtual int send(int8_t* msgbuf, int msglen);
    virtual int receive(int8_t* msgbuf, int msglen);

    int recv_data_ack();
    int send_image_data(char *buf, int data_len);

private:
    usb_dev_handle* m_pkneron_device_h;
    struct usb_device*    p_kneron_dev;

    int vendor_id;
    int product_id;
    int send(char* msgbuf, int msglen);
    int receive(char* msgbuf, int msglen);
};

#endif
