/**
 * @file      KdpHostApi.h
 * @brief     kdp host api header file
 * @version   0.1 - 2019-04-18
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#ifndef __KDP_HOST_API_H__
#define __KDP_HOST_API_H__

#define MAX_COM_DEV    1

#include "kdp_host.h"
#include "kdp_host_async.h"


//-----------------------basic command API-----------------------
int kdp_query_app_sync(int dev_idx, uint32_t* app_ids, int* size);
int kdp_select_app_sync(int dev_idx, uint32_t app_id, uint32_t* rsp_code, uint32_t* buf_size);
int kdp_sfid_start_sync(int dev_idx, uint32_t* rsp_code, uint32_t* img_size);
int kdp_sfid_new_user_sync(int dev_idx, uint16_t usr_id, uint16_t img_idx);
int kdp_sfid_register_sync(int dev_idx, uint32_t usr_id, uint32_t* rsp_code);
int kdp_sfid_del_db_sync(int dev_idx, uint32_t usr_id, uint32_t* rsp_code);
int kdp_sfid_send_img_sync(int dev_idx, char* img_buf, int buf_len, uint32_t* rsp_code,\
                uint16_t* user_id, uint16_t* mode);
//-----------------------basic command API end-----------------------

#endif

