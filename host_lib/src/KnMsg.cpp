/**
 * @file      KnMsg.cpp
 * @brief     implementation file for msg class
 * @version   0.1 - 2019-04-13
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include "KnMsg.h"
#include "KnLog.h"

#include <arpa/inet.h>
#include <string.h>

/* 
 * Utility API for create an appropriate response message.
 * It is public static function. It can be called outside.
 */

KnBaseMsg* KnBaseMsg::create_rsp_msg(int8_t* buf, int mlen) {
    KnMsgHdr hdr;
    memset(&hdr, 0, sizeof(hdr));
    int hdr_len = sizeof(KnMsgHdr);

    if( !buf || mlen < hdr_len ) {
        KnPrint(KN_LOG_ERR, "invalid buffer given for create msg %d.\n", mlen);
        return NULL;
    }

    if(decode_msg_hdr(&hdr, buf, hdr_len) < 0) {
        KnPrint(KN_LOG_ERR, "decode hdr failed.\n");
        return NULL;
    }

    KnBaseMsg* p_msg = NULL;

    if(hdr.msg_type == 0) {
        KnPrint(KN_LOG_ERR, "invalid msg type received %d.\n", hdr.msg_type);
        return NULL;
    }

    if( ((int)hdr.msg_len + hdr_len) != mlen) {
        KnPrint(KN_LOG_ERR, "msg len specified %d, act:%d.\n", hdr.msg_len, mlen);
        return NULL;
    }

    KnPrint(KN_LOG_DBG, "creating response msg: %d.\n", hdr.msg_type);
    if((hdr.msg_type & KN_RSP_OFFSET) == 0) { //generic rsp msg
        p_msg = new KnGenericAckRsp(buf, mlen);
        return p_msg;
    }
    
    uint16_t msg_type = hdr.msg_type - KN_RSP_OFFSET;

    switch(msg_type) {
    case KN_TEST_MEM_READ_REQ:
        p_msg = new KnTestMemReadRsp(buf, mlen);
        break;

    case KN_TEST_MEM_WRITE_REQ:
        p_msg = new KnTestMemWriteRsp(buf, mlen);
        break;

    case KN_MSG_GEN_ACK:
        p_msg = new KnTestMemAck(buf, mlen);
        break;

    case KN_TEST_MEM_CLEAR_REQ:
        p_msg = new KnTestMemClearRsp(buf, mlen);
        break;

    case KN_TEST_ECHO_REQ:
        p_msg = new KnTestHostEchoRsp(buf, mlen);
        break;

    case KN_OPR_QUERY_APPS_REQ:
        p_msg = new KnOprQueryAppRsp(buf, mlen);
        break;

    case KN_OPR_SELECT_APP_REQ:
        p_msg = new KnOprSelectAppRsp(buf, mlen);
        break;

    case KN_APP_SFID_START_REQ:
        p_msg = new KnSFIDStartRsp(buf, mlen);
        break;

    case KN_APP_SFID_NEW_USER:
        p_msg = new KnSFIDewUserRsp(buf, mlen);
        break;

    case KN_APP_SFID_REGISTER:
        p_msg = new KnSFIDRegisterRsp(buf, mlen);
        break;

    case KN_APP_SFID_DEL_DB:
        p_msg = new KnSFIDDelDBRsp(buf, mlen);
        break;

    case KN_APP_SFID_SEND_IMG:
        p_msg = new KnSFIDSendImageRsp(buf, mlen);
        break;

    default:
        KnPrint(KN_LOG_ERR, "invalid msg type for create kn msg:%d.\n", hdr.msg_type);
    }

    return p_msg;
}

/* 
 * Utility API for decode message header from incoming stream.
 * It is public static function. It can be called outside.
 */
int KnBaseMsg::decode_msg_hdr(KnMsgHdr* hdr, int8_t* buf, int mlen) {
    if(hdr == NULL || buf == NULL) return -1;

    if(mlen < (int)sizeof(KnMsgHdr)) return -1;

    KnMsgHdr* tmp = (KnMsgHdr*)buf;

 //   hdr->msg_type = ntohs(tmp->msg_type);
 //   hdr->msg_len = ntohs(tmp->msg_len);
    hdr->msg_type = (tmp->msg_type);
    hdr->msg_len = (tmp->msg_len);

    return 0;
}

/* 
 * Utility API for encode message header to byte stream for sending 
 * It is public static function. It can be called outside.
 */
int KnBaseMsg::encode_msg_hdr(KnMsgHdr* hdr, int8_t* buf, int mlen) {
    if(hdr == NULL || buf == NULL) return -1;

    if(mlen < (int)sizeof(KnMsgHdr)) return -1;

    uint16_t t = (hdr->msg_type);
    uint16_t l = (hdr->msg_len);

    //uint16_t t = htons(hdr->msg_type);
    //uint16_t l = htons(hdr->msg_len);

    memcpy((void*)buf, (void*)&t, sizeof(uint16_t));
    memcpy((void*)(buf+sizeof(uint16_t)), (void*)&l, sizeof(uint16_t));

    return 0;
}

//map msg type to appropriate request type.
//it could be same as request or request + 0x8000
uint16_t KnBaseMsg::get_req_type(uint16_t msg_type) {
    if(msg_type & KN_RSP_OFFSET) {
        return msg_type - KN_RSP_OFFSET;
    } else {
        return msg_type;
    }
}

//decode header from msg internal payload 
//the incoming byte stream neeed to be copied first
int KnBaseMsg::decode_hdr() {
    if(msg_payload == NULL) return -1;

    return decode_msg_hdr(&hdr, msg_payload, sizeof(KnMsgHdr));
}

//encode header to msg internal payload
//the hdr struct need to be set first. and payload buffer is ready
int KnBaseMsg::encode_hdr() {
    if(msg_payload == NULL) return -1;

    return encode_msg_hdr(&hdr, msg_payload, sizeof(KnMsgHdr));
}

//constructor with msg type and length
KnBaseMsg::KnBaseMsg(uint16_t msg_type, uint16_t msg_len) : _valid(false) {
    hdr.msg_type = msg_type;
    hdr.msg_len = msg_len;

    int tot_len = hdr.msg_len + sizeof(KnMsgHdr);
    msg_payload = new int8_t[tot_len];

    if(encode_hdr() < 0) {
        KnPrint(KN_LOG_ERR, "encode hdr failed: %d, %d.\n", msg_type, msg_len);
        return;
    }
    _valid = true;
}

//constructor with incoming byte stream 
KnBaseMsg::KnBaseMsg(int8_t* buf, int mlen) : _valid(false) {
    msg_payload = NULL;
    if(!buf) return;
    if(mlen < (int)sizeof(KnMsgHdr)) return;

    if(decode_msg_hdr(&hdr, buf, mlen) < 0) {
        KnPrint(KN_LOG_ERR, "decode hdr failed: %d.\n", mlen);
        return;
    }

    int tot_len = hdr.msg_len + sizeof(hdr);
    if(mlen >= tot_len) {
        msg_payload = new int8_t[tot_len];
        memcpy((void*)msg_payload, (void*)buf, tot_len);

        //change msg payload to host format
        KnMsgHdr* p = (KnMsgHdr*)msg_payload;
        p->msg_type = hdr.msg_type;
        p->msg_len = hdr.msg_len;
        _valid = true;
    } else {
        KnPrint(KN_LOG_ERR, "invalid msg len received:(hdr: %d, tot: %d).\n", hdr.msg_len, mlen);
        msg_payload = NULL;
    }
}

//destructor
KnBaseMsg::~KnBaseMsg() {
    if(msg_payload) {
        delete[] msg_payload; //free the payload
        msg_payload = NULL;
    }
}

//----------------------request message start-----------------------

KnGenericReq::KnGenericReq(uint16_t msg_type, uint16_t msg_len) \
        : KnBaseMsg(msg_type, msg_len) {
}

KnGenericReq::~KnGenericReq() {
}

KnTestMemReadReq::KnTestMemReadReq(uint16_t msg_type, uint16_t msg_len, uint32_t start_addr, uint32_t num) \
        : KnGenericReq(msg_type, msg_len), _start_addr(start_addr), _num_bytes(num) {
    //uint32_t s = htonl(_start_addr);
    uint32_t s = (_start_addr);
    //uint32_t n = htonl(_num_bytes);
    uint32_t n = (_num_bytes);

    int off = sizeof(KnMsgHdr);
    memcpy((void*)(msg_payload + off), (void*)&s, sizeof(uint32_t));
    off += sizeof(uint32_t);

    memcpy((void*)(msg_payload + off), (void*)&n, sizeof(uint32_t));
    off += sizeof(uint32_t);

    if(off - sizeof(KnMsgHdr) > msg_len) {
        KnPrint(KN_LOG_ERR, "invalid msg len received:(given: %d, act: %d).\n", msg_len, off);
    }
}

KnTestMemReadReq::~KnTestMemReadReq() {
}

KnTestMemWriteReq::KnTestMemWriteReq(uint16_t msg_type, uint16_t msg_len, uint32_t start_addr, uint32_t num, \
        int8_t* pdata, int len) : KnTestMemReadReq(msg_type, msg_len, start_addr, num) {
    _p_data = msg_payload + 8 + sizeof(KnMsgHdr);
    memcpy((void*)_p_data, pdata, len);
    _len = msg_len - 8;

    if(_len != len) {
        KnPrint(KN_LOG_ERR, "invalid msg len received:(given: %d, act: %d).\n", len, _len);
    }
}

KnTestMemWriteReq::~KnTestMemWriteReq() {
    //_p_data re-use payload mem, no need to free
}

KnCmdResetReq::KnCmdResetReq(uint16_t msg_type, uint16_t msg_len, uint32_t r_mode, uint32_t chk_code) \
        : KnGenericReq(msg_type, msg_len), _rset_mode(r_mode), _check_code(chk_code) {
    uint32_t r = (_rset_mode);
    //uint32_t r = htonl(_rset_mode);
    uint32_t c = (_check_code);
    //uint32_t c = htonl(_check_code);

    int off = sizeof(KnMsgHdr);
    memcpy((void*)(msg_payload + off), (void*)&r, sizeof(uint32_t));
    off += sizeof(uint32_t);

    memcpy((void*)(msg_payload + off), (void*)&c, sizeof(uint32_t));
    off += sizeof(uint32_t);

    if(off - sizeof(KnMsgHdr) > msg_len) {
        KnPrint(KN_LOG_ERR, "invalid msg len received:(given: %d, act: %d).\n", msg_len, off);
    }
}

KnCmdResetReq::~KnCmdResetReq() {
}

KnOprSelectAppReq::KnOprSelectAppReq(uint16_t msg_type, uint16_t msg_len, uint32_t id) \
        : KnGenericReq(msg_type, msg_len), _app_id(id) {
    //uint32_t a = htonl(_app_id);
    uint32_t a = (_app_id);

    int off = sizeof(KnMsgHdr);
    memcpy((void*)(msg_payload + off), (void*)&a, sizeof(uint32_t));
    off += sizeof(uint32_t);

    if(off - sizeof(KnMsgHdr) > msg_len) {
        KnPrint(KN_LOG_ERR, "invalid msg len received:(given: %d, act: %d).\n", msg_len, off);
    }
}

KnOprSelectAppReq::~KnOprSelectAppReq() {
}

KnSFIDStartReq::KnSFIDStartReq(uint16_t msg_type, uint16_t msg_len, uint32_t r) \
        : KnGenericReq(msg_type, msg_len), _reserved(r) {
    if(!_valid) return;
    if(msg_len < sizeof(uint32_t)) {
        KnPrint(KN_LOG_ERR, "invalid msg len for start sfid received: %d.\n", msg_len);
        _valid = false;
        return;
    }

    //uint32_t t = htonl(_reserved);
    uint32_t t = (_reserved);
    int off = sizeof(KnMsgHdr);
    memcpy((void*)(msg_payload + off), (void*)&t, sizeof(uint32_t));
}

KnSFIDStartReq::~KnSFIDStartReq() {
}


KnSFIDNewUserReq::KnSFIDNewUserReq(uint16_t msg_type, uint16_t msg_len, uint16_t u_id, uint16_t img_idx) \
        : KnGenericReq(msg_type, msg_len), _user_id(u_id), _img_idx(img_idx) {
    if(!_valid) return;
    if(msg_len < sizeof(uint32_t) + sizeof(uint32_t)) {
        KnPrint(KN_LOG_ERR, "invalid msg len for new user reg received: %d.\n", msg_len);
        _valid = false;
        return;
    }

    //uint16_t u = htons(_user_id);
    uint16_t u = (_user_id);
    //uint16_t i = htons(_img_idx);
    uint16_t i = (_img_idx);

    int off = sizeof(KnMsgHdr);
    memcpy((void*)(msg_payload + off), (void*)&u, sizeof(uint16_t));
    off += sizeof(uint32_t);

    memcpy((void*)(msg_payload + off), (void*)&i, sizeof(uint16_t));
}

KnSFIDNewUserReq::~KnSFIDNewUserReq() {
}

KnSFIDRegisterReq::KnSFIDRegisterReq(uint16_t msg_type, uint16_t msg_len, uint32_t u_id) \
        : KnGenericReq(msg_type, msg_len), _user_id(u_id) {
    if(!_valid) return;
    if(msg_len < sizeof(uint32_t)) {
        KnPrint(KN_LOG_ERR, "invalid msg len for register req received: %d.\n", msg_len);
        _valid = false;
        return;
    }

    //uint32_t u = htonl(_user_id);
    uint32_t u = (_user_id);

    int off = sizeof(KnMsgHdr);
    memcpy((void*)(msg_payload + off), (void*)&u, sizeof(uint32_t));
}

KnSFIDRegisterReq::~KnSFIDRegisterReq() {
}

//----------------------request message end-----------------------


//----------------------response message start-----------------------

KnGenericAckRsp::KnGenericAckRsp(int8_t* buf, int mlen) \
        : KnBaseMsg(buf, mlen) {
}

KnGenericAckRsp::~KnGenericAckRsp() {
}

KnTestMemAck::KnTestMemAck(int8_t* buf, int mlen) : KnGenericAckRsp(buf, mlen) {
    uint32_t tmp;
    int off = sizeof(KnMsgHdr);
    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    _ack = (tmp);
    //_ack = ntohl(tmp);

    memcpy((void*)(msg_payload+off), (void*)&_ack, sizeof(uint32_t));
    off += sizeof(uint32_t);

    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    _reserved = (tmp);
    //_reserved = ntohl(tmp);
    memcpy((void*)(msg_payload+off), (void*)&_reserved, sizeof(uint32_t));
}

KnTestMemAck::~KnTestMemAck() {
}

KnTestMemWriteRsp::KnTestMemWriteRsp(int8_t* buf, int mlen) : KnGenericAckRsp(buf, mlen) {
    uint32_t tmp;
    int off = sizeof(KnMsgHdr);
    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    //_rsp_result = ntohl(tmp);
    _rsp_result = (tmp);
    memcpy((void*)(msg_payload+off), (void*)&_rsp_result, sizeof(uint32_t));

    off += sizeof(uint32_t);

    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    //_num_bytes = ntohl(tmp);
    _num_bytes = (tmp);
    memcpy((void*)(msg_payload+off), (void*)&_num_bytes, sizeof(uint32_t));
}

KnTestMemWriteRsp::~KnTestMemWriteRsp() {
}

KnTestMemReadRsp::KnTestMemReadRsp(int8_t* buf, int mlen) : KnTestMemWriteRsp(buf, mlen) {
    int off = sizeof(KnMsgHdr) + 8;
    _p_data = msg_payload + off;
    _len = hdr.msg_len - 8;
}

KnTestMemReadRsp::~KnTestMemReadRsp() {
    //_p_data re-use payload mem, no need to free
}

KnCmdResetRsp::KnCmdResetRsp(int8_t* buf, int mlen) : KnGenericAckRsp(buf, mlen) {
    uint32_t tmp;
    int off = sizeof(KnMsgHdr);
    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    //_err_code = ntohl(tmp);
    _err_code = (tmp);

    memcpy((void*)(msg_payload+off), (void*)&_err_code, sizeof(uint32_t));
    off += sizeof(uint32_t);

    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    //_reserved = ntohl(tmp);
    _reserved = (tmp);
    memcpy((void*)(msg_payload+off), (void*)&_reserved, sizeof(uint32_t));
}

KnCmdResetRsp::~KnCmdResetRsp() {
}

KnSFIDStartRsp::KnSFIDStartRsp(int8_t* buf, int mlen) : KnGenericAckRsp(buf, mlen) {
    if(!_valid) return;
    if((uint32_t)mlen < sizeof(uint32_t) + sizeof(uint32_t) + sizeof(KnMsgHdr)) {
        KnPrint(KN_LOG_ERR, "invalid msg len for sfid start rsp received: %d.\n", mlen);
        _valid = false;
        return;
    }

    uint32_t tmp;
    int off = sizeof(KnMsgHdr);
    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    //_rsp_code = ntohl(tmp);
    _rsp_code = (tmp);
    memcpy((void*)(msg_payload+off), (void*)&_rsp_code, sizeof(uint32_t));

    off += sizeof(uint32_t);

    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    _img_size = (tmp);
    //_img_size = ntohl(tmp);
    memcpy((void*)(msg_payload+off), (void*)&_img_size, sizeof(uint32_t));
}

KnSFIDStartRsp::~KnSFIDStartRsp() {
}

KnSFIDSendImageRsp::KnSFIDSendImageRsp(int8_t* buf, int mlen) : KnGenericAckRsp(buf, mlen) {
    if(!_valid) return;
    if((uint32_t)mlen < sizeof(uint32_t) + sizeof(uint32_t) + sizeof(KnMsgHdr)) {
        KnPrint(KN_LOG_ERR, "invalid msg len for send img rsp received: %d.\n", mlen);
        _valid = false;
        return;
    }

    uint32_t tmp;
    int off = sizeof(KnMsgHdr);
    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    _rsp_code = (tmp);
    //_rsp_code = ntohl(tmp);
    memcpy((void*)(msg_payload+off), (void*)&_rsp_code, sizeof(uint32_t));

    off += sizeof(uint32_t);

    uint16_t t16;
    memcpy((void*)&t16, (void*)(msg_payload+off), sizeof(uint16_t));
    _mode = (t16);
    //_mode = ntohs(t16);
    memcpy((void*)(msg_payload+off), (void*)&_mode, sizeof(uint16_t));

    off += sizeof(uint16_t);
    memcpy((void*)&t16, (void*)(msg_payload+off), sizeof(uint16_t));
    //_uid = ntohs(t16);
    _uid = (t16);
    memcpy((void*)(msg_payload+off), (void*)&_uid, sizeof(uint16_t));
}

KnSFIDSendImageRsp::~KnSFIDSendImageRsp() {
}

KnOprQueryAppRsp::KnOprQueryAppRsp(int8_t* buf, int mlen) : KnGenericAckRsp(buf, mlen) {
    uint32_t tmp;
    int off = sizeof(KnMsgHdr);
    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    _reserved = (tmp);
    //_reserved = ntohl(tmp);
    memcpy((void*)(msg_payload+off), (void*)&_reserved, sizeof(uint32_t));
    off += sizeof(uint32_t);

    memcpy((void*)&tmp, (void*)(msg_payload+off), sizeof(uint32_t));
    _reserved2 = (tmp);
    //_reserved2 = ntohl(tmp);
    memcpy((void*)(msg_payload+off), (void*)&_reserved2, sizeof(uint32_t));
    off += sizeof(uint32_t);

    _len = (mlen - off) / sizeof(uint32_t);
    _p_app_id = (uint32_t*)(buf + off);

    for(int i = 0; i < _len; i++) {
        //tmp = ntohl(_p_app_id[i]);
        tmp = (_p_app_id[i]);
        _p_app_id[i] = tmp;
    }

}

KnOprQueryAppRsp::~KnOprQueryAppRsp() {
}


//----------------------response message end-----------------------

