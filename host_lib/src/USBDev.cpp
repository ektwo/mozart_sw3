/**
 * @file      USBDev.cpp
 * @brief     implementation file for USB Dev class
 * @version   0.1 - 2019-04-28
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include "USBDev.h"
#include "KnLog.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include <unistd.h>

USBDev::USBDev(int vid, int pid) {
    m_pkneron_device_h = NULL;
    p_kneron_dev = NULL;
    vendor_id = vid;
    product_id = pid;
}


USBDev::~USBDev() {
    if (NULL != m_pkneron_device_h) {
        usb_release_interface(m_pkneron_device_h, 0);
        usb_close(m_pkneron_device_h);
        m_pkneron_device_h = NULL;
    }
}

int USBDev::u_init_port()
{
    int status;

    usb_init();
    if (0 > (status = usb_find_busses())) {
        KnPrint(KN_LOG_ERR, "no usb bus is found, error code:%d.\n", status);
        return -1;
    }

    if (0 > (status = usb_find_devices())) {
        KnPrint(KN_LOG_ERR, "no usb device is found, error code:%d.\n", status);
        return -1;
    }

    struct usb_bus *bus;
    struct usb_device *dev;

    for (bus = usb_get_busses(); bus; bus = bus->next) {
        for (dev = bus->devices; dev; dev = dev->next) {
            KnPrint(KN_LOG_DBG, "usb dev vid:%d, pid:%d.\n", 
                    dev->descriptor.idVendor, dev->descriptor.idProduct);
            if (dev->descriptor.idVendor == vendor_id
                && dev->descriptor.idProduct == product_id) {
                KnPrint(KN_LOG_DBG, "found correct usb dev vid:%d, pid:%d.\n", 
                    vendor_id, product_id);
                p_kneron_dev = dev;
                return 0;
            }
        }
    }
    KnPrint(KN_LOG_ERR, "no usb device is found, error code:%d.\n", status);
    return -1;
}

int USBDev::u_close() {
    if (NULL != m_pkneron_device_h) {
        usb_release_interface(m_pkneron_device_h, 0);
        usb_close(m_pkneron_device_h);
        m_pkneron_device_h = NULL;
    }
    return 0;
}

int USBDev::u_open() {
    int status;
    if (NULL == p_kneron_dev) return -1;

    if (NULL == (m_pkneron_device_h = usb_open(p_kneron_dev))) {
        KnPrint(KN_LOG_ERR, "usb device vid:%d, pid:%d, open failed.\n", vendor_id, product_id);
        return -1;
    }

    if (0 != (status =  usb_set_configuration(m_pkneron_device_h, 1))) {
        KnPrint(KN_LOG_ERR, "error for set usb config:%d.\n", status);
        usb_close(m_pkneron_device_h);
        return -1;
    }

    if (0 != (status = usb_claim_interface(m_pkneron_device_h, 0))) {
        KnPrint(KN_LOG_ERR, "claim interface failed:%d.\n", status);
        usb_close(m_pkneron_device_h);
        return -1;
    }
    KnPrint(KN_LOG_DBG, "usb dev opened successfully.\n");
    return 0;
}

int USBDev::send(int8_t* msgbuf, int msglen) {
    return send((char*) msgbuf, msglen);
}

int USBDev::send(char* msgbuf, int msglen) {
    int status = 0;

    if (NULL == m_pkneron_device_h)
        return -1;

    KnTsPrint(KN_LOG_DBG, "calling usb write to send data :%d...\n", msglen);
    if (0 >= (status = usb_bulk_write(m_pkneron_device_h, EP_OUT, msgbuf, msglen, 1000))) {
        KnPrint(KN_LOG_ERR, "write usb failed:%d.\n", status);
        return status;
    }

    KnTsPrint(KN_LOG_DBG, "usb write sent data :%d.\n", status);
    return status;
}

int USBDev::receive(int8_t* msgbuf, int msglen) {
    return receive((char*)msgbuf, msglen);
}

int USBDev::receive(char* msgbuf, int msglen) {
    uint32_t ret_val;
    if (NULL == m_pkneron_device_h)
        return -1;

    KnTsPrint(KN_LOG_DBG, "calling usb read to receive data :%d...\n", msglen);
    if ( 0 >=  (ret_val = usb_bulk_read(m_pkneron_device_h, EP_IN , msgbuf, msglen, 2000))) {
        KnPrint(KN_LOG_ERR, "USB EP read failed:%d.\n", ret_val);
        return -1;
    }
    KnTsPrint(KN_LOG_DBG, "usb read received data :%d.\n", ret_val);
    return ret_val;
}


int USBDev::u_sync_dev() {
#if 0
    int status = recv_data_ack();
    if (status < 0) {
        KnPrint(KN_LOG_ERR, "wait ack failed during usb sync:%d.\n", status);
        return status;
    }

    char challenge[8] = "KNERON";
    status = send(challenge, sizeof(challenge));
    if(status < 0) {
        KnPrint(KN_LOG_ERR, "sending hand shake to dev failed:%d.\n", status);
        return status;
    }
            
    char ReadSyncBuffer[5] = { 0, 0, 0, 0, 0};
    status = receive(ReadSyncBuffer, sizeof ReadSyncBuffer);
    if(status < 0) {
        KnPrint(KN_LOG_ERR, "receiving hand shake ack failed:%d.\n", status);
        return status;
    }

    const char* txt = "SYNC";
    int cmp_res = 0;
    for(int i = 0; i < 4; i++) {
        if(toupper(ReadSyncBuffer[i]) != toupper(txt[i])) {
            cmp_res = 1;
            break;
        }
    }

    if(cmp_res != 0) {
        KnPrint(KN_LOG_ERR, "receiving ack msg:%s.\n", ReadSyncBuffer);
        return -1;
    }
#endif

    return 0;
}

int USBDev::recv_data_ack() {
    char ack_buffer[ACK_PACKET_SIZE];
    uint8_t usb_ack[ACK_PACKET_SIZE] = { 0x35, 0x8A, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00 };

    memset(ack_buffer, 0, sizeof(ack_buffer));

    KnTsPrint(KN_LOG_DBG, "usb dev is receiving ack...\n");
    int status = receive(ack_buffer, ACK_PACKET_SIZE);
    if(status < 0) {
        KnPrint(KN_LOG_ERR, "receiving ack failed:%d.\n", status);
        return status;
    }

    for(int i = 0; i < ACK_PACKET_SIZE; i++) {
        if(ack_buffer[i] != usb_ack[i]) {
            KnPrint(KN_LOG_ERR, "receiving ack wrong:%d, %d, %d.\n", i, ack_buffer[i], usb_ack[i]);
            return -1;
        }
    }

    return 0;
}

int USBDev::send_image_data(char *buf, int data_len) {
    if(data_len <= 0 || !buf) return -1;

    KnTsPrint(KN_LOG_DBG, "usb dev is starting to send image :%d...\n", data_len);

    int leftover = data_len;
    int i = 0;
    int off = 0;

    while(leftover > 0) {
        int status = recv_data_ack();
        if(status < 0) {
            KnPrint(KN_LOG_ERR, "ack msg wrong:%d.\n", status);
            return status;
        }

        int len = static_cast<int> (USB_MAX_BYTES);
        if(len > leftover) len = leftover;

        KnTsPrint(KN_LOG_DBG, "usb dev is sending segment :%d, with len:%d...\n", i, len);
        status = send(buf + off, len);
        if(status < 0) {
            KnPrint(KN_LOG_ERR, "sending %d time for img, off:%d.\n", i, off);
            return status;
        }

        KnTsPrint(KN_LOG_DBG, "usb dev sent segment :%d, with len:%d...\n", i, status);
        
        off += status;
        leftover -= status;
        i++;
    }

    KnTsPrint(KN_LOG_DBG, "usb dev finished sending image :%d...\n", off);
    return off;
}

