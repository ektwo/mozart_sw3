/**
 * @file      KdpHostApi_async.cpp
 * @brief     kdp host api source file for async transfer
 * @version   0.1 - 2019-04-25
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include <string.h>
#include <unistd.h>

#include "KdpHostApi.h"
#include "MsgMgmt.h"
#include "KnLog.h"
#include "UARTDev.h"

#include <thread>
#include <vector>
using namespace std;

extern AsyncMsgHandler*        _async_hdlr;

//mem test read request
int kdp_mem_test_read(int dev_idx, uint32_t start_addr, uint32_t num_bytes, KdpCallBack func) {
    if(!_async_hdlr)  return -1;

    return _async_hdlr->kdp_mem_test_read(dev_idx, start_addr, num_bytes, func);
}

//mem test write request
int kdp_mem_test_write(int dev_idx, uint32_t start_addr, uint32_t num_bytes, int8_t* p_data, int d_len, KdpCallBack func) {
    if(!_async_hdlr)  return -1;

    return _async_hdlr->kdp_mem_test_write(dev_idx, start_addr, num_bytes, p_data, d_len, func);
}

//mem test clear request
int kdp_mem_test_clear(int dev_idx, uint32_t start_addr, uint32_t num_bytes, KdpCallBack func) {
    if(!_async_hdlr)  return -1;

    return _async_hdlr->kdp_mem_test_clear(dev_idx, start_addr, num_bytes, func);
}

//mem test echo request
int kdp_mem_test_echo(int dev_idx, uint32_t start_addr, uint32_t num_bytes, int8_t* p_data, int d_len, KdpCallBack func)  {
    if(!_async_hdlr)  return -1;

    return _async_hdlr->kdp_mem_test_echo(dev_idx, start_addr, num_bytes, p_data, d_len, func);
}

//cmd reset requet
int kdp_cmd_reset(int dev_idx, uint32_t rset_mode, KdpCallBack func) {
    if(!_async_hdlr)  return -1;

    return _async_hdlr->kdp_cmd_reset(dev_idx, rset_mode, func);
}

//wait and handle rsp messages.
//it will call the callback funcs in the request api
void wait_n_handle_rsp_msg() {
    if(!_async_hdlr)  return;
    _async_hdlr->handle_notification();
}


