/**
 * @file      AsyncMsgHandler.cpp
 * @brief     implementation file for async message handler
 * @version   0.1 - 2019-04-23
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include "AsyncMsgHandler.h"
#include "MsgId.h"
#include "UARTDev.h"
#include "MsgMgmt.h"
#include "KnLog.h"

#include <string.h>
#include <unistd.h>

#include <vector>
using namespace std;

extern MsgMgmt*                _p_mgr; //msg manager


KdpCmdStatus::KdpCmdStatus() : _p_rsp(NULL), _sts(KDP_MSG_STS_NOT_INIT) {
    _func = NULL;
}

//free the rsp msg object
KdpCmdStatus::~KdpCmdStatus() {
    if(_p_rsp) {
        delete _p_rsp;
        _p_rsp = NULL;
    }
}

AsyncMsgHandler::AsyncMsgHandler() {
}

AsyncMsgHandler::~AsyncMsgHandler() {
}

//save the command status to local map
//user registed call back func
int AsyncMsgHandler::save_to_local_db(int dev_idx, int msg_id, KdpCallBack func) {
    //fill status
    KdpCmdStatus cmd_sts;
    cmd_sts._sts = KDP_MSG_STS_GOING; //request sent out successfully
    cmd_sts._func = func;
    _map_mtx[dev_idx].lock();

    //put to map
    _runing_map[dev_idx][msg_id] = cmd_sts;

    _map_mtx[dev_idx].unlock();
    KnPrint(KN_LOG_DBG, "saved command request to the local store:%d.\n", msg_id);
    return 0;
}


int AsyncMsgHandler::fetch_rsp_msg(int& dev_idx) {
    int msg_id = -1;

    for(int i = 0; i < MAX_COM_DEV; i++) {
        _map_mtx[i].lock();

        if(_runing_map[i].empty()) {
            _map_mtx[i].unlock();
            continue;
        }
    
        map<int, KdpCmdStatus>::iterator iter;
        for(iter = _runing_map[i].begin(); iter != _runing_map[i].end(); ++iter) {
            if(iter->second._sts == KDP_MSG_STS_COMPLT) {
                msg_id = iter->first;
                _map_mtx[i].unlock();

                KnPrint(KN_LOG_DBG, "got rsp message for msg id:%d.\n", msg_id);
                dev_idx = i;
                return msg_id;

            } else if(iter->second._sts == KDP_MSG_STS_GOING) {
                //if any one is waiting
                msg_id = 0;

            } else {
                //not initialized, need erase
                _runing_map[i].erase(iter);
            }
        }
        _map_mtx[i].unlock();
    }

    if(msg_id == -1) {
        KnPrint(KN_LOG_DBG, "no command messages in the local store.\n");
        return -1; //no msg
    }

    //KnPrint(KN_LOG_DBG, "no rsp message received, need wait.\n");
    return 0; //need to wait
}


void AsyncMsgHandler::handle_rsp_msg(int dev_idx, int msgid) {
    map<int, KdpCmdStatus>::iterator iter;

    _map_mtx[dev_idx].lock();
    iter = _runing_map[dev_idx].find(msgid);
    if(iter == _runing_map[dev_idx].end()) {
        _map_mtx[dev_idx].unlock();
        KnPrint(KN_LOG_DBG, "could not find command sts for:%d.\n", msgid);
        return;
    }

    KdpCallBack func = iter->second._func;

    KnBaseMsg* pmsg = iter->second._p_rsp;
    uint16_t len = 0;
    int8_t* p_data = NULL;

    if(pmsg) {
        len = pmsg->get_msg_len() + sizeof(KnMsgHdr);
        p_data = new int8_t[len];
        memcpy((void*)p_data, (void*)(pmsg->get_msg_payload()), len);
    } else {
        KnPrint(KN_LOG_DBG, "could not get rsp msg for req:%d.\n", msgid);
    }

    _runing_map[dev_idx].erase(iter); //base rsp msg and its msg content be freed.

    _map_mtx[dev_idx].unlock();

    KnPrint(KN_LOG_DBG, "calling callback for msg:%d.\n", msgid);
    if(func) {
        //calling callback func to handle msg content
        func(p_data, len);
    }

    delete[] p_data; //free the data

    return;
}

void AsyncMsgHandler::wait_for_msgs() {
    _cond_mtx.lock();

    _cond_wait.wait_for(_cond_mtx, std::chrono::milliseconds(KDP_MSG_WAIT_TIME));

    _cond_mtx.unlock();
    return;
}

void AsyncMsgHandler:: handle_notification() {
    while(1) {
        int dev_idx = -1;
        int msg_id = fetch_rsp_msg(dev_idx);

        if(msg_id == -1) {
            KnPrint(KN_LOG_DBG, "no msg in the local store, returning...\n");
            break; //finish
        } else if(msg_id == 0) {
            //no rsp avail, need wait.
            //KnPrint(KN_LOG_DBG, "waiting for response msgs...\n");
            wait_for_msgs();
        } else {
            handle_rsp_msg(dev_idx, msg_id);
        }
    }
    return;
}


int AsyncMsgHandler::kdp_mem_test_read(int dev_idx, uint32_t start_addr, uint32_t num_bytes, KdpCallBack func) {
    uint16_t msg_type = KN_TEST_MEM_READ_REQ;
    uint16_t msg_len = sizeof(uint32_t) + sizeof(uint32_t);

    //create req message
    KnTestMemReadReq* p_mem_req = new KnTestMemReadReq(msg_type, msg_len, start_addr, num_bytes);

    //sending msg to queue
    int res = _p_mgr->send_cmd_msg(dev_idx, p_mem_req);
    if(res < 0) {
        KnPrint(KN_LOG_DBG, "sending mem read request to send queue failed:%d.\n", res);
        return -1;
    }

    //save callback for rsp handling
    save_to_local_db(dev_idx, KN_TEST_MEM_READ_REQ, func);
    return 0;
}

int AsyncMsgHandler::kdp_mem_test_write(int dev_idx, uint32_t start_addr, uint32_t num_bytes, int8_t* p_data, int d_len, KdpCallBack func) {
    uint16_t msg_type = KN_TEST_MEM_WRITE_REQ;
    uint16_t msg_len = sizeof(uint32_t) + sizeof(uint32_t) + d_len;

    //create req message
    KnTestMemWriteReq* p_mem_req = new KnTestMemWriteReq(msg_type, msg_len, start_addr, num_bytes, p_data, d_len);

    //sending msg to queue
    int res = _p_mgr->send_cmd_msg(dev_idx, p_mem_req);
    if(res < 0) {
        KnPrint(KN_LOG_DBG, "sending mem write request to send queue failed:%d.\n", res);
        return -1;
    }

    //save callback for rsp handling
    save_to_local_db(dev_idx, KN_TEST_MEM_WRITE_REQ, func);
    return 0;
}

int AsyncMsgHandler::kdp_mem_test_clear(int dev_idx, uint32_t start_addr, uint32_t num_bytes, KdpCallBack func) {
    uint16_t msg_type = KN_TEST_MEM_CLEAR_REQ;
    uint16_t msg_len = sizeof(uint32_t) + sizeof(uint32_t);

    //create req message
    KnTestMemClearReq* p_mem_req = new KnTestMemClearReq(msg_type, msg_len, start_addr, num_bytes);

    //sending msg to queue
    int res = _p_mgr->send_cmd_msg(dev_idx, p_mem_req);
    if(res < 0) {
        KnPrint(KN_LOG_DBG, "sending mem clear request to send queue failed:%d.\n", res);
        return -1;
    }

    //save callback for rsp handling
    save_to_local_db(dev_idx, KN_TEST_MEM_CLEAR_REQ, func);
    return 0;
}

int AsyncMsgHandler::kdp_mem_test_echo(int dev_idx, uint32_t reserved, uint32_t num_bytes, int8_t* p_data, int d_len, KdpCallBack func) {
    uint16_t msg_type = KN_TEST_ECHO_REQ;
    uint16_t msg_len = sizeof(uint32_t) + sizeof(uint32_t) + d_len;

    //create req message
    KnTestHostEchoReq* p_mem_req = new KnTestHostEchoReq(msg_type, msg_len, reserved, num_bytes, p_data, d_len);

    //sending msg to queue
    int res = _p_mgr->send_cmd_msg(dev_idx, p_mem_req);
    if(res < 0) {
        KnPrint(KN_LOG_DBG, "sending mem echo request to send queue failed:%d.\n", res);
        return -1;
    }

    //save callback for rsp handling
    save_to_local_db(dev_idx, KN_TEST_ECHO_REQ, func);
    return 0;
}

int AsyncMsgHandler::kdp_cmd_reset(int dev_idx, uint32_t rset_mode, KdpCallBack func) {
    uint16_t msg_type = KN_OPR_CMD_RESET_REQ;
    uint16_t msg_len = sizeof(uint32_t) + sizeof(uint32_t);

    //create req message
    uint32_t chk_code = ~rset_mode;
    KnCmdResetReq* p_req = new KnCmdResetReq(msg_type, msg_len, rset_mode, chk_code);

    //sending msg to queue
    int res = _p_mgr->send_cmd_msg(dev_idx, p_req);
    if(res < 0) {
        KnPrint(KN_LOG_DBG, "sending cmd reset request to send queue failed:%d.\n", res);
        return -1;
    }

    //save callback for rsp handling
    save_to_local_db(dev_idx, KN_OPR_CMD_RESET_REQ, func);

    if(rset_mode == 0xff) { //reset system
        usleep( SYS_WAIT_RESET * 1000000 );
        init_protocl_msg_stack(dev_idx);
    }
    return 0;
}

void AsyncMsgHandler::init_protocl_msg_stack(int dev_idx) {
    return ;
}

int AsyncMsgHandler::msg_received_rx_queue(KnBaseMsg* p_rsp, uint16_t req_type, int idx) {
    map<int, KdpCmdStatus>::iterator iter;
    _map_mtx[idx].lock();

    iter = _runing_map[idx].find(req_type);
    if(iter == _runing_map[idx].end()) {
        KnPrint(KN_LOG_DBG, "rsp msg does not have request:%d.\n", req_type);
        return -1;
    }

    iter->second._sts = KDP_MSG_STS_COMPLT;
    iter->second._p_rsp = p_rsp;

    _map_mtx[idx].unlock();

    //notify
    _cond_mtx.lock();
    _cond_wait.notify_all();
    _cond_mtx.unlock();

    return 0;
}

