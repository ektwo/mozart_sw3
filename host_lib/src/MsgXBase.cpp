/**
 * @file      MsgXBase.cpp
 * @brief     implementation file for msg RX/TX base class
 * @version   0.1 - 2019-04-15
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include "MsgXBase.h"
#include "KnLog.h"

#include <unistd.h>

#include <thread>

MsgXBase::MsgXBase(MsgMgmt* p_ref) {
    stopped = false;
    running_sts = false;
    p_mgr = p_ref;

    for(int i = 0; i < MAX_COM_DEV; i++) {
        dev_fds[i] = -1;
    }
}

MsgXBase:: ~MsgXBase() {
    _q_mtx.lock();

    while(1) {
        int t;
        KnBaseMsg* pmsg = fetch_msg_from_rtx_queue(t);

        if(!pmsg) break;

        delete pmsg;
    }

    _q_mtx.unlock();
}

void MsgXBase::thrd_func(MsgXBase* p_inst) {
    p_inst->run();
}

void MsgXBase::stop() {
    stopped = true;
}

void MsgXBase::start() {
    stopped = false;
    running_sts = false;

    //create thread;
    std::thread th(MsgXBase::thrd_func, this);
    th.detach();

    //yield to let thrd run first
    usleep(1000);

    if(running_sts) {
        KnPrint(KN_LOG_DBG, "rtx thrd started successfully.\n");
    } else {
        KnPrint(KN_LOG_ERR, "rtx thrd could not be started!\n");
    }
    return;
}

int MsgXBase::get_index_from_fd(int fd) {
    for(int i = 0; i < MAX_COM_DEV; i++) {
        if(dev_fds[i] == fd) {
            return i;
        }
    }
    KnPrint(KN_LOG_ERR, "could not find fd:%d.\n", fd);
    return -1;
}

bool MsgXBase::is_msg_in_rtx_queue() {
    for(int i = 0; i < MAX_COM_DEV; i++) {
        if(msg_q[i].rtx_q.empty() == false) {
            return true;
        }
    }
    return false;
}

int MsgXBase::add_msg_to_rtx_queue(int idx, KnBaseMsg* p_msg) {
    //check size
    int size = msg_q[idx].rtx_q.size();
    if(size >= MAX_QUEUE_SIZE) {
        KnPrint(KN_LOG_ERR, "msg queue is full:%d.\n", size);
        return -1; 
    }

    //add it
    msg_q[idx].rtx_q.push(p_msg);

    return size+1;
}

int MsgXBase::send_cmd_msg(KnBaseMsg* p_msg, BaseDev* pDev) {
    return -1;
}

int MsgXBase::send_cmd_msg_sync(KnBaseMsg* p_msg, BaseDev* pDev) {
    return -1;
}

KnBaseMsg* MsgXBase::rcv_cmd_rsp(int& idx) {
    return NULL;
}

KnBaseMsg* MsgXBase::fetch_msg_from_rtx_queue(int& fd) {
    for(int i = 0; i < MAX_COM_DEV; i++) {
        if(msg_q[i].rtx_q.empty() == false) {
            KnBaseMsg* p_msg = msg_q[i].rtx_q.front();
            msg_q[i].rtx_q.pop();
            fd = dev_fds[i];
            return p_msg;
        }
    }
    return NULL;
}

