/**
 * @file      async_test.cpp
 * @brief     kdp host lib async test examples 
 * @version   0.1 - 2019-04-23
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include "stdio.h"
#include "kdp_host.h"
#include "kdp_host_async.h"

//call back for mem test read
static int handle_mem_test_read_rsp(int8_t* rsp_msg, int msglen) {
    if(rsp_msg == NULL) return -1;
    
    if(msglen != sizeof(MemReadRsp)) return -1;

    MemReadRsp* rsp = (MemReadRsp*)rsp_msg;

    printf("mem read resp:%u, %u.\n", rsp->rsp_result, rsp->num_bytes);

    printf("payload....\n");

    for(uint32_t i = 0; i < rsp->num_bytes; i++) {
        printf("%x ", rsp->p_data[i]);
    }

    printf("\n");
    return 0;
}

//call back for mem test write
static int handle_mem_test_write_rsp(int8_t* rsp_msg, int msglen) {
    if(rsp_msg == NULL) return -1;
    
    if(msglen != sizeof(MemWriteRsp)) return -1;

    MemWriteRsp* rsp = (MemWriteRsp*)rsp_msg;

    printf("mem write resp:%u, %u.\n", rsp->rsp_result, rsp->num_bytes);

    return 0;
}

//call back for mem test clear
static int handle_mem_test_clear_rsp(int8_t* rsp_msg, int msglen) {
    if(rsp_msg == NULL) return -1;
    
    if(msglen != sizeof(MemClearRsp)) return -1;

    MemClearRsp* rsp = (MemClearRsp*)rsp_msg;

    printf("mem write resp:%u, %u.\n", rsp->rsp_result, rsp->num_bytes);

    return 0;
}

//call back for mem test echo
static int handle_mem_test_echo_rsp(int8_t* rsp_msg, int msglen) {
    if(rsp_msg == NULL) return -1;
    
    if(msglen != sizeof(MemEchoRsp)) return -1;

    MemEchoRsp* rsp = (MemEchoRsp*)rsp_msg;

    printf("mem read resp:%u, %u.\n", rsp->rsp_result, rsp->num_bytes);

    printf("payload....\n");

    for(uint32_t i = 0; i < rsp->num_bytes; i++) {
        printf("%x ", rsp->p_data[i]);
    }

    printf("\n");
    return 0;
}

int async_test(int dev_idx) 
{
    //mem read
    uint32_t start_addr = 0x68000000;
    uint32_t num = 0x10;
    printf("starting sending mem test read request....\n");
    int n_read = kdp_mem_test_read(dev_idx, start_addr, num, handle_mem_test_read_rsp);
    printf("mem test read req:%d...\n", n_read);

    int8_t data[0x10];
    for(int i = 0; i < 0x10; i++) data[i] = i;

    //mem write
    printf("starting sending mem test write request....\n");
    int n_write = kdp_mem_test_write(dev_idx, start_addr, num, data, 0x10,  handle_mem_test_write_rsp);
    printf("mem test write req:%d...\n", n_write);

    //mem clear
    printf("starting sending mem test clear request....\n");
    int n_clear = kdp_mem_test_clear(dev_idx, start_addr, num, handle_mem_test_clear_rsp);
    printf("mem test clear req:%d...\n", n_clear);

    //mem echo
    printf("starting sending mem test echo request....\n");
    int n_echo = kdp_mem_test_echo(dev_idx, 0, num, data, 0x10, handle_mem_test_echo_rsp);
    printf("mem test echo req:%d...\n", n_echo);

    printf("waiting and handling response messages....\n");
    wait_n_handle_rsp_msg();

    return 0;
}

