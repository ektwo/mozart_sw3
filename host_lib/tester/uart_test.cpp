#include <stdio.h>
#include <fcntl.h> 
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include "uart_test.h"
#include "MsgId.h"
#include "UARTPkt.h"
#include "KnUtil.h"

#include <arpa/inet.h>
 
int uart_int(const char* dev)
{
    int fd;
    fd = open(dev, O_RDWR | O_NOCTTY);

    if(fd == -1) {
        printf("Error! in Opening dev:%s.\n", dev);
        return -1;
    } else {
        printf("uart %s Opened Successfully.\n", dev);
    }

    struct termios SerialPortSettings;

    //speed
    tcgetattr(fd, &SerialPortSettings);    

    cfsetispeed(&SerialPortSettings, B115200);
    cfsetospeed(&SerialPortSettings, B115200);

    if((tcsetattr(fd, TCSANOW, &SerialPortSettings)) != 0)
        printf("ERROR ! in Setting speed.\n");
    else
        printf("Setting baudrate to 115200.\n");
    tcflush (fd, TCIOFLUSH);

    //parity, stop bits, flow control, data bits
    tcgetattr(fd, &SerialPortSettings);    


    //set parity
    SerialPortSettings.c_cflag &= ~PARENB;   /* No Parity   */
    SerialPortSettings.c_cflag &= ~CSTOPB;   /* 1 Stop bit */
    SerialPortSettings.c_cflag &= ~CSIZE;     
    SerialPortSettings.c_cflag |=  CS8;      /* data bits = 8  */

    SerialPortSettings.c_cflag &= ~CRTSCTS;       /* No Hardware flow Control                         */
    SerialPortSettings.c_cflag |= CREAD | CLOCAL; /* Enable receiver,Ignore Modem Control lines       */


    SerialPortSettings.c_iflag &= ~(IXON | IXOFF | IXANY);          /* Disable XON/XOFF flow control both i/p and o/p */
    SerialPortSettings.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);  /* Non Cannonical mode                            */

    SerialPortSettings.c_oflag &= ~OPOST;/*No Output Processing   raw  format  output*/

    if((tcsetattr(fd, TCSANOW, &SerialPortSettings)) != 0)
        printf("ERROR ! in Setting attributes.\n");
    else
        printf("StopBits = 1 \n  Parity   = none\n");


    tcflush(fd, TCIFLUSH); 

    return fd;
}

void send_sfid_rsp_msg(int fd) {
    StartFidRsp rsp;
    rsp.hdr.msg_type = htons(0x8108);
    rsp.hdr.msg_len = htons(8);

    rsp.rsp_code = htonl(0);
    rsp.img_size = htonl(640*480);

    UARTPkt pkt((int8_t*)&rsp, sizeof(rsp), 0, 1);
    printf("sending sfid start rsp to host.\n");
    kn_dump_msg(pkt.get_pkt_buf(), pkt.get_pkt_len());

    write(fd, pkt.get_pkt_buf(), pkt.get_pkt_len());
    return;
}

void send_sfid_user_msg(int fd) {
    MsgHdr rsp;
    rsp.msg_type = htons(0x8109);
    rsp.msg_len = htons(0);

    UARTPkt pkt((int8_t*)&rsp, sizeof(rsp), 0, 1);
    printf("sending sfid user rsp to host.\n");
    kn_dump_msg(pkt.get_pkt_buf(), pkt.get_pkt_len());

    write(fd, pkt.get_pkt_buf(), pkt.get_pkt_len());
    return;
}


void send_sfid_reg_msg(int fd) {
    StartFidRsp rsp;
    rsp.hdr.msg_type = htons(0x810A);
    rsp.hdr.msg_len = htons(8);

    rsp.rsp_code = htonl(0);
    rsp.img_size = htonl(0);

    UARTPkt pkt((int8_t*)&rsp, sizeof(rsp), 0, 1);
    printf("sending sfid reg rsp to host.\n");
    kn_dump_msg(pkt.get_pkt_buf(), pkt.get_pkt_len());

    write(fd, pkt.get_pkt_buf(), pkt.get_pkt_len());
    return;
}

void send_sfid_db_msg(int fd) {
    StartFidRsp rsp;
    rsp.hdr.msg_type = htons(0x810B);
    rsp.hdr.msg_len = htons(8);

    rsp.rsp_code = htonl(0);
    rsp.img_size = htonl(0);

    UARTPkt pkt((int8_t*)&rsp, sizeof(rsp), 0, 1);
    printf("sending sfid db rsp to host.\n");
    kn_dump_msg(pkt.get_pkt_buf(), pkt.get_pkt_len());

    write(fd, pkt.get_pkt_buf(), pkt.get_pkt_len());
    return;
}

void send_sfid_img_msg(int fd) {
    StartFidRsp rsp;
    rsp.hdr.msg_type = htons(0x810C);
    rsp.hdr.msg_len = htons(8);

    rsp.rsp_code = htonl(0);
    rsp.img_size = htonl(1);

    UARTPkt pkt((int8_t*)&rsp, sizeof(rsp), 0, 1);
    printf("sending sfid img rsp to host.\n");
    kn_dump_msg(pkt.get_pkt_buf(), pkt.get_pkt_len());

    write(fd, pkt.get_pkt_buf(), pkt.get_pkt_len());
    return;
}

void handle_rcv_msg(char* buf, int len, int fd) {
    MsgHdr hdr;
    memcpy(&hdr, buf, sizeof(hdr));
    hdr.msg_type = ntohs(hdr.msg_type);
    hdr.msg_len = ntohs(hdr.msg_len);

    if(hdr.msg_len != len-4) {
        printf("msg length not matched:%d,%d.\n", len, hdr.msg_len);
        return;
    }

    switch(hdr.msg_type) {
    case KN_APP_SFID_START_REQ:
        send_sfid_rsp_msg(fd);
        break;

    case KN_APP_SFID_NEW_USER:
        send_sfid_user_msg(fd);
        break;

    case KN_APP_SFID_REGISTER:
        send_sfid_reg_msg(fd);
        break;

    case KN_APP_SFID_DEL_DB:
        send_sfid_db_msg(fd);
        break;

    case KN_APP_SFID_SEND_IMG:
        send_sfid_img_msg(fd);
        break;

    default:
        printf("invalid msg type received:%d", hdr.msg_type);
    }
    return;
}

void handle_uart_msg(char* buf, int len, int fd) {
    UARTPkt pkt((int8_t*)buf, len);
    if(!pkt.is_pkt_valid()) {
        printf("unpacking UART packet falied.\n");
        return;
    }

    memset(buf, 0, len);
    memcpy(buf, pkt.get_payload_buf(), pkt.get_payload_len());
    handle_rcv_msg(buf, pkt.get_payload_len(), fd);
}

int read_uart_msg(int fd, char* msgbuf, int len) {
    int bytes_received = 0;
    char* ptr = msgbuf;
    int left = 4;

    for(int i = 0; i < 100; i++) {
        int n = read(fd, ptr, left);
        if(n < 0) {
            if (errno == EINTR) {
                continue;
            } else if(errno == EAGAIN) {
                usleep(1000);
                continue;
            } else {
                break;
            }
        }

        bytes_received += n;
        ptr += n;
        left -= n;

        if(bytes_received < 4) continue;

        if(bytes_received == 4) {
            unsigned short tmp = 0;
            char* tmp_ptr = msgbuf;
            tmp_ptr += sizeof(unsigned short);
            memcpy((void*)&tmp, tmp_ptr, sizeof(unsigned short));

            tmp = ntohs(tmp);

            unsigned short len = tmp & 0x0fff; //get the len

            bool is_crc = false;
            if(tmp & (0x01 << 14) ) is_crc = true;

            unsigned short tot_len = len + 4;
            if(is_crc) tot_len += 2;

            left = tot_len - bytes_received;
        }

        if(left == 0) break; //read all the msg pkt
    }
    return bytes_received;
}

int main(void)
{
    printf("\n +----------------------------------+");
    printf("\n |        Serial Port Read          |");
    printf("\n +----------------------------------+\n");

    int fd = uart_int("/dev/ttyS4");

    char read_buffer[1024];   
    int  bytes_read = 0;   

    while(1)
    {
        memset(read_buffer, 0, 1024);
        bytes_read = read_uart_msg(fd, read_buffer, 1024);

        if(bytes_read > 0) {

            kn_dump_msg((int8_t*)read_buffer, bytes_read);

            handle_uart_msg(read_buffer, bytes_read, fd);
        }

        usleep(1000);
        printf("waiting for data....\n");
    }
    return 0; 
}

