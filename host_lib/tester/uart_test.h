#ifndef __UART_TEST_H__
#define __UART_TEST_H__

#include <stdint.h>

typedef struct {
    uint16_t msg_type;
    uint16_t msg_len;
} MsgHdr;

typedef struct {
    MsgHdr hdr;
    uint32_t rsp_code;
    uint32_t img_size;
} StartFidRsp; 

#endif
