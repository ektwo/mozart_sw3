#!/bin/bash

function rebuild()
{
	cd $1
	rm -rf build
	mkdir build
	cd build
	cmake ..
	make
	cd ../..
}

#export CXX="/usr/local/bin/g++"
export CXX="/usr/bin/g++-4.8"

rebuild src

cp src/build/libhostkdp.so ./

rebuild example
rebuild tester

