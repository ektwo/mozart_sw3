# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/atom/proj/mozart_host/host_lib-master/example/main.cpp" "/home/atom/proj/mozart_host/host_lib-master/example/build/CMakeFiles/demo.dir/main.cpp.o"
  "/home/atom/proj/mozart_host/host_lib-master/example/user_test.cpp" "/home/atom/proj/mozart_host/host_lib-master/example/build/CMakeFiles/demo.dir/user_test.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../../hdr"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
