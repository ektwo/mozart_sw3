/**
 * @file      main.cpp
 * @brief     kdp host lib demo file 
 * @version   0.1 - 2019-04-19
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include "stdio.h"
#include "kdp_host.h"
#include "user_test.h"

#include <unistd.h>

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"{
#endif

#define MAX_TIMES 1

int main(void) 
{
    printf("init kdp host lib log....\n");
    kdp_init_log("/tmp/", "mzt.log");

    if(kdp_lib_init() < 0) {
        printf("init for kdp host lib failed.\n");
        return -1;
    }

    printf("adding devices....\n");
    int dev_idx = kdp_add_dev(KDP_UART_DEV, "/dev/ttyUSB0");
    if(dev_idx < 0 ) {
        printf("add device failed.\n");
        return -1;
    }

    printf("start kdp host lib....\n");
    if(kdp_lib_start() < 0) {
        printf("start kdp host lib failed.\n");
        return -1;
    }

    for(int i = 0; i < MAX_TIMES; i++) {
        printf("doing test :%d....\n", i);
        user_test(dev_idx);
        usleep(10000);
    }

    printf("de init kdp host lib....\n");
    kdp_lib_de_init();
    return 0;
}

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif
