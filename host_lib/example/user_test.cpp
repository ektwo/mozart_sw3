/**
 * @file      user_test.cpp
 * @brief     kdp host lib user test examples 
 * @version   0.1 - 2019-04-25
 * @copyright (c) 2019 Kneron Inc. All right reserved.
 */

#include "stdio.h"
#include "kdp_host.h"

#include <string.h>
#include <unistd.h>

#if defined (__cplusplus) || defined (c_plusplus)
extern "C"{
#endif

#define IMAGE_SIZE 640*480
#define SLEEP_TIME 1000

int user_test(int dev_idx) 
{
    //verify user id
    uint32_t img_size = 0;
    printf("starting verify mode ...\n");
    int ret = kdp_start_verify_mode(dev_idx, &img_size);
    if(ret != 0 || img_size == 0) {
        printf("start verify mode failed :%d, buf size:%d.\n", ret, img_size);
        return -1;
    }
    printf("starting verify mode successfuly, img size:%d...\n", img_size);

    usleep(SLEEP_TIME);

    char img_buf[IMAGE_SIZE];
    uint16_t u_id = 0;

    for(int i = 0; i < 1; i++) { //do 100 times
        memset(img_buf, 0, sizeof(img_buf));

        //TODO need set image name here 
        printf("verifying for image ...\n");
        int n_len = IMAGE_SIZE;
        u_id = 0;
        ret = kdp_verify_user_id(dev_idx, &u_id, img_buf, n_len);
        if(ret != 0) {
            printf("could not find matched user id.\n");
        } else {
            printf("find matched user id:%d.\n", u_id);
        }
    }

    usleep(SLEEP_TIME);

    //register
    u_id = 5;
    uint16_t img_idx = 1;
    printf("starting register mode ...\n");
    ret = kdp_start_reg_user_mode(dev_idx, u_id, img_idx);
    if(ret != 0) {
        printf("could not set to register user mode..\n");
        return -1;
    }
    printf("register mode succeeded...\n");

    usleep(SLEEP_TIME);

    if(1) {
        //TODO need set image name here 
        memset(img_buf, 0, sizeof(img_buf));

        printf("registering with image...\n");
        int n_len = IMAGE_SIZE;
        ret = kdp_extract_feature(dev_idx, img_buf, n_len);
        if(ret != 0) {
            printf("could not extract feature for.\n");
        }  else {
            printf("feature extracted successfully for img index.\n");
        }
    }

    usleep(SLEEP_TIME);

    uint32_t user_id = 5;
    printf("registering user :%d ...\n", user_id);
    ret = kdp_register_user(dev_idx, user_id);
    if(ret != 0) {
        printf("register user failed.\n");
        return -1;
    }
    printf("registered with user id:%d.\n", user_id);

    usleep(SLEEP_TIME);

    //delete all user
    printf("removing user ...\n");
    ret = kdp_remove_user(dev_idx, 0);
    if(ret != 0) {
        printf("remove all user failed.\n");
    }
    printf("removing user succeeded ...\n");
             
    return 0;
}

#if defined (__cplusplus) || defined (c_plusplus)
}
#endif
