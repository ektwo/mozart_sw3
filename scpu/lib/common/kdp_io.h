/*
 * Kneron System Library
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#ifndef __KDP_IO_H__
#define __KDP_IO_H__

#include "io.h"

static inline void read_set_bit(volatile uint32_t addr, uint32_t bit)
{
    uint32_t tmp;
    tmp = inw(addr);
    tmp |= (1<<bit);
    outw(addr, tmp);
}

static inline void set_mask_bits(volatile uint32_t addr, uint32_t mask)
{
    uint32_t tmp;
    tmp = inw(addr);
    tmp |= mask;
    outw(addr, tmp);
}

static inline void read_clear_bit(volatile uint32_t addr, uint32_t bit)
{
    uint32_t tmp;
    tmp = inw(addr);
    tmp &= ~(1<<bit);
    outw(addr, tmp);
}

static inline void clear_mask_bits(volatile uint32_t addr, uint32_t mask)
{
    uint32_t tmp;
    tmp = inw(addr);
    tmp &= ~mask;
    outw(addr, tmp);
}

static inline int polling_wait_bit_0(volatile uint32_t addr, uint32_t bit, uint32_t try_count)
{
    for (uint32_t tc = 0; tc < try_count; tc++) {
        if(!(inw(addr) & (1<<bit)))
            return 1;
    }
    return 0;
}

static inline int polling_wait_bit_1(volatile uint32_t addr, uint32_t bit, uint32_t try_count)
{
    for (uint32_t tc = 0; tc < try_count; tc++) {
        if(inw(addr) & (1<<bit))
            return 1;
    }
    return 0;
}

#endif
