/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#include <stdlib.h>

#include "cmsis_os2.h"
#include "kneron_mozart.h"

#include "kdp_io.h"
#include "kdp_uart.h"

kdp_status_e kdp_uart_init(kdp_uart_ctrl_e ctrl_id, kdp_uart_config_t *conf)
{
    /*
        TODO: need implemenation
    */
    return KDP_STATUS_ERROR;
}

kdp_status_e kdp_uart_deinit(kdp_uart_ctrl_e ctrl_id)
{
    /*
        TODO: need implemenation
    */
    return KDP_STATUS_ERROR;
}

kdp_status_e kdp_uart_send(kdp_uart_ctrl_e ctrl_id, const uint8_t *data, uint32_t num, uint32_t try_timeout)
{
    /*
        TODO: need implemenation
    */
    return KDP_STATUS_ERROR;
}

kdp_status_e kdp_uart_receive(kdp_uart_ctrl_e ctrl_id, uint8_t *data, uint32_t num, uint32_t try_timeout)
{
    /*
        TODO: need implemenation
    */
    return KDP_STATUS_ERROR;
}

kdp_status_e kdp_gpio_receive_register_notification(kdp_uart_ctrl_e ctrl_id, osThreadId_t tid, uint32_t tflag)
{
    /*
        TODO: need implemenation
    */
    return KDP_STATUS_ERROR;
}
