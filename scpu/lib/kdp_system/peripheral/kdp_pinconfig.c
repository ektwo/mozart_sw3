/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#include <stdarg.h>
#include "kdp_pinconfig.h"
#include "scu_extreg.h"
#include "base.h"

void kdp_pinconfig_set_pin_mode(kdp_pin_name_e pin, kdp_pin_mode_t mode)
{
    PINMUX_SET((SCU_EXTREG_SPI_WP_N + pin*4), mode);
}
