/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#include <stdlib.h>

#include "cmsis_os2.h"
#include "kneron_mozart.h"

#include "kdp_io.h"
#include "kdp_gpio.h"

/* GPIO registers offset */
#define REG_GPIO_DOUT_OFFSET            0x0
#define REG_GPIO_DIN_OFFSET             0x4
#define REG_GPIO_PINOUT_OFFSET          0x8
#define REG_GPIO_PIN_BYPASS             0xC
#define REG_GPIO_DATASET                0x10
#define REG_GPIO_DATACLR                0x14
#define REG_GPIO_PULLENABLE             0x18
#define REG_GPIO_PULLType               0x1C
#define REG_GPIO_INT_ENABLE             0x20
#define REG_GPIO_INT_RAWSTATE           0x24
#define REG_GPIO_INT_MASKSTATE          0x28
#define REG_GPIO_INT_MASK               0x2C
#define REG_GPIO_INT_CLEAR              0x30
#define REG_GPIO_INT_TRIGGER            0x34
#define REG_GPIO_INT_BOTH               0x38
#define REG_GPIO_INT_RISENEG            0x3C
#define REG_GPIO_INT_BOUNCEENABLE       0x40
#define REG_GPIO_INT_PRESCALE           0x44

// internal struct to record user callback information
typedef struct _CALLBACK_t{
    uint32_t pin;
    kdp_user_callback_t user_cb;
    void *user_data;
    struct _CALLBACK_t *next;
}kdp_i_gpio_callback_t;

// internal struct to record user thread flag notification
typedef struct _THREAD_NOTIFY_t{
    uint32_t pin;
    osThreadId_t tid;
    uint32_t tflag;
    struct _THREAD_NOTIFY_t *next;
}kdp_i_gpio_notify_t;

// internal struct to represent GPIO controller settings
typedef struct{
    uint32_t base_addr;
    IRQn_Type irq_num;
    uint32_t intr_status; // updated by ISR, recording current interrupt pins in bit-fields
    kdp_i_gpio_callback_t *cb_list; // user callback function linked list, for saving memory usage
    kdp_i_gpio_notify_t *tn_list; // user thread flag notification linked list, for saving memory usage
}kdp_i_gpio_setting_t;

// global static gpio resources
static kdp_i_gpio_setting_t *gpio_0_settings = NULL;
static osThreadId_t tid_gpio = NULL; // a thread for executing user callback functions

#define FLAG_GPIO_0_INTERRUPT 0x0001
#define FLAG_GPIO_0_THREAD_EXIT 0x0002

// GPIO controller to NVIC ISR
static void gpio_0_isr(void)
{
    if(!gpio_0_settings)
        return;

    uint32_t gpio_base = gpio_0_settings->base_addr;

    // read GPIO interrupt pins with masked status
    uint32_t intr_status = inw(gpio_base + REG_GPIO_INT_MASKSTATE);

    // clear the interrupt source coming from GPIO peripheral
    outw(gpio_base + REG_GPIO_INT_CLEAR, intr_status);

    // check if needed to notify user thread
    kdp_i_gpio_notify_t *tn_item = gpio_0_settings->tn_list;
    while(tn_item)
    {
        if(intr_status & (0x1 << tn_item->pin))
            osThreadFlagsSet(tn_item->tid, tn_item->tflag);

        tn_item = tn_item->next;
    }

    // if user callback exists
    if(tid_gpio){
        // pass interrupt status to user callback delegate thread
        gpio_0_settings->intr_status = intr_status;
        osThreadFlagsSet(tid_gpio, FLAG_GPIO_0_INTERRUPT);
    }
}

// user callback delegate thread
static void gpio_user_thread(void *data)
{
    while(1)
    {
        uint32_t flags = osThreadFlagsWait((FLAG_GPIO_0_INTERRUPT|FLAG_GPIO_0_THREAD_EXIT), osFlagsWaitAny, osWaitForever);

        // terminate the thread immediately
        if(flags & FLAG_GPIO_0_THREAD_EXIT)
            return;

        // should not happen
        if(!gpio_0_settings)
            continue;

        // below start handling FLAG_GPIO_0_INTERRUPT for registered user callback functions
        kdp_i_gpio_callback_t *cb_item = gpio_0_settings->cb_list;
        while(cb_item)
        {
            if(gpio_0_settings->intr_status & (0x1 << cb_item->pin))
                cb_item->user_cb(cb_item->user_data); // execute user callback

            cb_item = cb_item->next;
        }
    }
}

kdp_status_e kdp_gpio_init(kdp_gpio_ctrl_e ctrl_id)
{
    if(ctrl_id != KDP_GPIO_CTRL_0)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    if(gpio_0_settings)
        return KDP_STATUS_ERROR_CTRL_INITIALIZED;

    gpio_0_settings = (kdp_i_gpio_setting_t *)malloc(sizeof(kdp_i_gpio_setting_t));
    if(!gpio_0_settings)
        return KDP_STATUS_ERROR_NO_MEMORY;

    gpio_0_settings->base_addr = GPIO_FTGPIO010_PA_BASE;
    gpio_0_settings->irq_num = GPIO_FTGPIO010_IRQ;
    gpio_0_settings->intr_status = 0;
    gpio_0_settings->cb_list = NULL;
    gpio_0_settings->tn_list = NULL;

    // enable gpio_pclk
    // FIXME: should use internal system clock function (not yet implemented)
    // to manage all peripherals pclk
    read_set_bit(SCU_FTSCU100_PA_BASE + 0x60, 20);

    NVIC_SetVector(gpio_0_settings->irq_num, (uint32_t)gpio_0_isr);

    // Clear and Enable SAI IRQ
    NVIC_ClearPendingIRQ(gpio_0_settings->irq_num);
    NVIC_EnableIRQ(gpio_0_settings->irq_num);

    return KDP_STATUS_OK;
}

kdp_status_e kdp_gpio_deinit(kdp_gpio_ctrl_e ctrl_id)
{
    if(ctrl_id != KDP_GPIO_CTRL_0)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    if(!gpio_0_settings)
        return KDP_STATUS_ERROR_CTRL_UNINITIALIZED;

    NVIC_DisableIRQ(gpio_0_settings->irq_num);

    // disable gpio_pclk
    // FIXME: should use system-clock API (not yet implemented) to manage all peripherals pclk
    read_clear_bit(SCU_FTSCU100_PA_BASE + 0x60, 20);

    // terminate the thread
    if(tid_gpio){
        osThreadFlagsSet(tid_gpio, FLAG_GPIO_0_THREAD_EXIT);
        tid_gpio = NULL;
    }

    // free allocated memory for user callback
    kdp_i_gpio_callback_t *cb_item = gpio_0_settings->cb_list;
    kdp_i_gpio_callback_t *cb_temp;
    while(cb_item)
    {
        cb_temp = cb_item;
        cb_item = cb_item->next;
        free(cb_temp);
    }

    // free allocated memory for user thread notification
    kdp_i_gpio_notify_t *tn_item = gpio_0_settings->tn_list;
    kdp_i_gpio_notify_t *tn_temp;
    while(tn_item)
    {
        tn_temp = tn_item;
        tn_item = tn_item->next;
        free(tn_temp);
    }

    // free GPIO setting itself
    free(gpio_0_settings);

    gpio_0_settings = NULL;

    return KDP_STATUS_OK;
}

kdp_status_e kdp_gpio_set_pin_attribute(kdp_gpio_ctrl_e ctrl_id, kdp_gpio_pin_e pin, uint32_t attributes)
{
    if(ctrl_id != KDP_GPIO_CTRL_0)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    if(!gpio_0_settings)
        return KDP_STATUS_ERROR_CTRL_UNINITIALIZED;

    uint32_t gpio_base = gpio_0_settings->base_addr;

    // disable interrup in case of wrong conditions
    read_clear_bit(gpio_base + REG_GPIO_INT_ENABLE, pin);
    read_set_bit(gpio_base + REG_GPIO_INT_MASK, pin);

    // set pin direction
    if(attributes & KDP_GPIO_DIR_INPUT)
        read_clear_bit(gpio_base + REG_GPIO_PINOUT_OFFSET, pin);
    else // KDP_GPIO_DIR_OUTPUT
        read_set_bit(gpio_base + REG_GPIO_PINOUT_OFFSET, pin);

    uint32_t edge_attributes =
        (KDP_GPIO_INT_EDGE_RISING | KDP_GPIO_INT_EDGE_FALLLING | KDP_GPIO_INT_EDGE_BOTH);

    if(attributes & edge_attributes)
    {
        // edge trigger
        read_clear_bit(gpio_base + REG_GPIO_INT_TRIGGER, pin);

        // set both or single edge
        if(attributes & KDP_GPIO_INT_EDGE_BOTH)
            read_set_bit(gpio_base + REG_GPIO_INT_BOTH, pin);
        else
            read_clear_bit(gpio_base + REG_GPIO_INT_BOTH, pin);

        // set rising or falling edge
        if(attributes & KDP_GPIO_INT_EDGE_RISING)
            read_clear_bit(gpio_base + REG_GPIO_INT_RISENEG, pin);
        else
            read_set_bit(gpio_base + REG_GPIO_INT_RISENEG, pin);
    }

     uint32_t level_attributes =
        (KDP_GPIO_INT_LEVEL_HIGH | KDP_GPIO_INT_LEVEL_LOW);

    if(attributes & level_attributes)
    {
        // level trigger
        read_set_bit(gpio_base + REG_GPIO_INT_TRIGGER, pin);

        // set high or low level
        if(attributes & KDP_GPIO_INT_LEVEL_HIGH)
            read_clear_bit(gpio_base + REG_GPIO_INT_RISENEG, pin);
        else
            read_set_bit(gpio_base + REG_GPIO_INT_RISENEG, pin);
    }

    if(attributes & KDP_GPIO_PAD_PULL_HIGH)
    {
        read_set_bit(gpio_base + REG_GPIO_PULLENABLE, pin);
        read_set_bit(gpio_base + REG_GPIO_PULLType, pin);
    }
    else if (attributes & KDP_GPIO_PAD_PULL_LOW)
    {
        read_set_bit(gpio_base + REG_GPIO_PULLENABLE, pin);
        read_clear_bit(gpio_base + REG_GPIO_PULLType, pin);
    }
    else
        read_clear_bit(gpio_base + REG_GPIO_PULLENABLE, pin);

    return KDP_STATUS_OK;
}

kdp_status_e kdp_gpio_register_callback(kdp_gpio_ctrl_e ctrl_id, kdp_gpio_pin_e pin, kdp_user_callback_t cb_func, void *user_data)
{
    if(ctrl_id != KDP_GPIO_CTRL_0)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    if(!gpio_0_settings)
        return KDP_STATUS_ERROR_CTRL_UNINITIALIZED;

    kdp_i_gpio_callback_t *cb_item = (kdp_i_gpio_callback_t *)malloc(sizeof(kdp_i_gpio_callback_t));

    // insert the new callback item into cb list
    cb_item->pin = pin;
    cb_item->user_cb = cb_func;
    cb_item->user_data = user_data;
    cb_item->next = gpio_0_settings->cb_list;
    gpio_0_settings->cb_list = cb_item;

    // creating one thread while needed (when the first user callback gets registered)
    if(!tid_gpio)
        tid_gpio = osThreadNew(gpio_user_thread, NULL, NULL);

    return KDP_STATUS_OK;
}

kdp_status_e kdp_gpio_register_threadflag(kdp_gpio_ctrl_e ctrl_id, kdp_gpio_pin_e pin, osThreadId_t tid, uint32_t tflag)
{
    if(ctrl_id != KDP_GPIO_CTRL_0)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    if(!gpio_0_settings)
        return KDP_STATUS_ERROR_CTRL_UNINITIALIZED;

    kdp_i_gpio_notify_t *tn_item = (kdp_i_gpio_notify_t *)malloc(sizeof(kdp_i_gpio_notify_t));

    // insert the new callback item into cb list
    tn_item->pin = pin;
    tn_item->tid = tid;
    tn_item->tflag = tflag;
    tn_item->next = gpio_0_settings->tn_list;
    gpio_0_settings->tn_list = tn_item;

    return KDP_STATUS_OK;
}

kdp_status_e kdp_gpio_set_interrupt_enable(kdp_gpio_ctrl_e ctrl_id, kdp_gpio_pin_e pin, kdp_bool_e isEnable)
{
    if(ctrl_id != KDP_GPIO_CTRL_0)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    if(!gpio_0_settings)
        return KDP_STATUS_ERROR_CTRL_UNINITIALIZED;

    uint32_t gpio_base = gpio_0_settings->base_addr;

    if(isEnable)
    {
        outw(gpio_base + REG_GPIO_INT_CLEAR, (1<<pin));
        read_set_bit(gpio_base + REG_GPIO_INT_ENABLE, pin);
        read_clear_bit(gpio_base + REG_GPIO_INT_MASK, pin);
    }
    else
    {
        read_clear_bit(gpio_base + REG_GPIO_INT_ENABLE, pin);
        read_set_bit(gpio_base + REG_GPIO_INT_MASK, pin);
    }

    return KDP_STATUS_OK;
}

kdp_status_e kdp_gpio_set_debounce_enable(kdp_gpio_ctrl_e ctrl_id, kdp_gpio_pin_e pin, kdp_bool_e isEnable, uint32_t debounce_clock)
{
    if(ctrl_id != KDP_GPIO_CTRL_0)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    if(!gpio_0_settings)
        return KDP_STATUS_ERROR_CTRL_UNINITIALIZED;

    uint32_t gpio_base = gpio_0_settings->base_addr;

    if(isEnable){
        read_set_bit(gpio_base + REG_GPIO_INT_BOUNCEENABLE, pin);
        outw(gpio_base + REG_GPIO_INT_PRESCALE, (APB_CLOCK/debounce_clock));
    }
    else
        read_clear_bit(gpio_base + REG_GPIO_INT_BOUNCEENABLE, pin);

    return KDP_STATUS_OK;
}

kdp_status_e kdp_gpio_set_pin_value(kdp_gpio_ctrl_e ctrl_id, kdp_gpio_pin_e pin, kdp_bool_e value)
{
    if(ctrl_id != KDP_GPIO_CTRL_0)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    if(!gpio_0_settings)
        return KDP_STATUS_ERROR_CTRL_UNINITIALIZED;

    uint32_t gpio_base = gpio_0_settings->base_addr;
    uint32_t bit_value = (0x1 << pin);

    if(value == KDP_BOOL_TRUE)
        outw(gpio_base + REG_GPIO_DATASET, bit_value);
    else
        outw(gpio_base + REG_GPIO_DATACLR, bit_value);

    return KDP_STATUS_OK;
}

kdp_status_e kdp_gpio_get_pin_value(kdp_gpio_ctrl_e ctrl_id, kdp_gpio_pin_e pin, kdp_bool_e *pValue)
{
    if(ctrl_id != KDP_GPIO_CTRL_0)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    if(!gpio_0_settings)
        return KDP_STATUS_ERROR_CTRL_UNINITIALIZED;

    uint32_t gpio_base = gpio_0_settings->base_addr;
    uint32_t all_bits = inw(gpio_base + REG_GPIO_DIN_OFFSET);

    *pValue = (all_bits & (0x1 << pin)) ? KDP_BOOL_TRUE : KDP_BOOL_FALSE;

    return KDP_STATUS_OK;
}
