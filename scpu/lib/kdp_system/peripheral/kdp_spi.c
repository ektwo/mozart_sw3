/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#include <stdlib.h>

#include "cmsis_os2.h"
#include "kneron_mozart.h"

#include "kdp_io.h"
#include "kdp_spi.h"

/*

TODO: define related register address here

*/


kdp_status_e kdp_spi_init(kdp_spi_ctrl_e ctrl_id)
{
    /*
        TODO: need implemenation
    */
   return KDP_STATUS_ERROR;
}

kdp_status_e kdp_spi_deinit(kdp_spi_ctrl_e ctrl_id)
{
    /*
        TODO: need implemenation
    */
   return KDP_STATUS_ERROR;
}

kdp_status_e kdp_spi_set_config(kdp_spi_ctrl_e ctrl_id, kdp_spi_config_t *spi_config)
{
    /*
        TODO: need implemenation
    */
   return KDP_STATUS_ERROR;
}

kdp_status_e kdp_spi_set_ss(kdp_spi_ctrl_e ctrl_id, kdp_bool_e ss_high)
{
    /*
        TODO: need implemenation
    */
   return KDP_STATUS_ERROR;
}

kdp_status_e kdp_spi_transmit(kdp_spi_ctrl_e ctrl_id, const uint8_t *data, uint32_t num)
{
    /*
        TODO: need implemenation
    */
   return KDP_STATUS_ERROR;
}

kdp_status_e kdp_spi_receive(kdp_spi_ctrl_e ctrl_id, uint8_t *data, uint32_t num)
{
    /*
        TODO: need implemenation
    */
   return KDP_STATUS_ERROR;
}

kdp_status_e kdp_spi_transmit_receive(kdp_spi_ctrl_e ctrl_id, const uint8_t *out_data, uint8_t *in_data, uint32_t num)
{
    /*
        TODO: need implemenation
    */
   return KDP_STATUS_ERROR;
}
