/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#include <stdlib.h>

#include "cmsis_os2.h"
#include "kneron_mozart.h"

#include "kdp_io.h"
#include "kdp_i2c.h"

// I2C Control Register
#define REG_I2C_CR              0x0
#define CR_RST_BIT              0

#define CR_ALIRQ            0x2000  /* arbitration lost interrupt (master) */
#define CR_SAMIRQ           0x1000  /* slave address match interrupt (slave) */
#define CR_STOPIRQ          0x800   /* stop condition interrupt (slave) */
#define CR_NAKRIRQ          0x400   /* NACK response interrupt (master) */
#define CR_DRIRQ            0x200   /* rx interrupt (both) */
#define CR_DTIRQ            0x100   /* tx interrupt (both) */
#define CR_TBEN             0x80    /* tx enable (both) */
#define CR_NAK              0x40    /* NACK (both) */
#define CR_STOP             0x20    /* stop (master) */
#define CR_START            0x10    /* start (master) */
#define CR_GCEN             0x8     /* general call support (slave) */
#define CR_SCLEN            0x4     /* enable clock out (master) */
#define CR_I2CEN            0x2     /* enable I2C (both) */
#define CR_I2CRST           0x1     /* reset I2C (both) */
#define CR_ENABLE           \
	(CR_ALIRQ | CR_NAKRIRQ | CR_DRIRQ | CR_DTIRQ | CR_SCLEN | CR_I2CEN)

// I2C Status Register
#define REG_I2C_SR          0x04
#define SR_SBS              0x800000 /* start byte */
#define SR_START            0x800    /* start */
#define SR_AL               0x400    /* arbitration lost */
#define SR_GC               0x200    /* general call */
#define SR_SAM              0x100    /* slave address match */
#define SR_STOP             0x80     /* stop received */
#define SR_NACK             0x40     /* NACK received */
#define SR_TD               0x20     /* tx/rx finish */
#define SR_BB               0x8      /* bus busy */
#define SR_I2CB             0x4      /* chip busy */
#define SR_RW               0x1      /* set when master-rx or slave-tx mode */


// I2C Data Register
#define REG_I2C_DR          0x0C

// I2C Set/Hold Time and Glitch Suppresion Setting Register
#define REG_I2C_TGSR        0x14
#define TGSR_GSR            0x4
#define TGSR_TSR            0x4

// I2C Clock Division Register
#define REG_I2C_CDR         0x08
#define CDR_COUNT_100KHZ    ((uint32_t)((APB_CLOCK)/(2*100000) - (TGSR_GSR/2) - 2)) // if APB=100, this will be 496
#define CDR_COUNT_400KHZ    ((uint32_t)((APB_CLOCK)/(2*400000) - (TGSR_GSR/2) - 2)) // if APB=100, this will be 121
#define CDR_COUNT_1MHZ      ((uint32_t)((APB_CLOCK)/(2*1000000) - (TGSR_GSR/2) - 2)) // if APB=100, this will be 46





// internal global variables to represent I2C controller
#define NUM_I2C_CTRL (KDP_I2C_CTRL_3+1)
static uint32_t i2c_ctrl_base[NUM_I2C_CTRL] =
            {IIC_FTIIC010_0_PA_BASE,
            IIC_FTIIC010_1_PA_BASE,
            IIC_FTIIC010_2_PA_BASE,
            IIC_FTIIC010_3_PA_BASE};

static kdp_bool_e reset_i2c_and_check(uint32_t base_addr)
{
    // reset i2c controller
    read_set_bit(base_addr + REG_I2C_CR, CR_RST_BIT);

    // check if reset OK, FIXME: why 50000 ?
    if(polling_wait_bit_0(base_addr + REG_I2C_CR, CR_RST_BIT, 50000))
        return KDP_BOOL_TRUE;
    else
        return KDP_BOOL_FALSE;
}

static void set_i2c_speed(uint32_t base_addr, kdp_i2c_bus_speed_e bus_speed)
{
    // SCL = PCLK / (2 * COUNT  + GSR + 4)

    // set GSR and TSR value
    outw(base_addr + REG_I2C_TGSR, (TGSR_GSR << 10) | TGSR_TSR);

    // select CDR Count value depending on bus speed
    uint32_t count_bits;
    switch(bus_speed)
    {
        case KDP_I2C_SPEED_100K:
            count_bits = CDR_COUNT_100KHZ;
            break;
        case KDP_I2C_SPEED_400K:
            count_bits = CDR_COUNT_400KHZ;
            break;
        case KDP_I2C_SPEED_1M:
            count_bits = CDR_COUNT_1MHZ;
            break;
        default:
            // could be user specified clock in Hz
            count_bits = ((APB_CLOCK)/(2*(uint32_t)bus_speed) - (TGSR_GSR/2) - 2);
            break;
    }

    outw(base_addr + REG_I2C_CDR, count_bits); // not care about COUNTH and DUTY bits
}

kdp_status_e kdp_i2c_init(kdp_i2c_ctrl_e ctrl_id, kdp_i2c_bus_speed_e bus_speed)
{
    if(ctrl_id >= NUM_I2C_CTRL)
        return KDP_STATUS_ERROR_CTRL_NO_EXIST;

    // enable i2c_pclk
    // FIXME: should use internal system clock function (not yet implemented)
    // to manage all peripherals pclk
    read_set_bit(SCU_FTSCU100_PA_BASE + 0x60, (0x2 << ctrl_id));

    if(KDP_BOOL_FALSE == reset_i2c_and_check(i2c_ctrl_base[ctrl_id]))
        return KDP_STATUS_ERROR_I2C_RESET_FAILED;

    set_i2c_speed(i2c_ctrl_base[ctrl_id], bus_speed);

    return KDP_STATUS_OK;
}

kdp_status_e kdp_i2c_deinit(kdp_i2c_ctrl_e ctrl_id)
{
    // disable gpio_pclk
    // FIXME: should use system-clock API (not yet implemented) to manage all peripherals pclk
    read_clear_bit(SCU_FTSCU100_PA_BASE + 0x60, (0x2 << ctrl_id));

    return KDP_STATUS_OK;
}

static kdp_bool_e polling_i2c_complete(uint32_t base)
{
    kdp_bool_e ret = KDP_BOOL_FALSE;
    uint32_t status;

    // polling SR and take actions correspondingly
    // FIXME, why 5000 try count ?
    for(int i=0;i<5000;i++)
    {
        status = inw(base + REG_I2C_SR);
        outw(base + REG_I2C_SR, status);

        // FIXME: should also monitor AL, NACK, RW ..
        if (status & SR_TD) {
            ret = KDP_BOOL_TRUE;
            break;
        }
    }

    return ret;
}

kdp_status_e i2c_tx_rx(kdp_i2c_ctrl_e ctrl_id, uint16_t slave_addr, const uint8_t *data, uint32_t num, kdp_bool_e isRead, kdp_bool_e with_STOP)
{
    uint32_t base = i2c_ctrl_base[ctrl_id];

    // wait until bus and controller are both available
    {
        while(inw(base + REG_I2C_SR) & (SR_BB | SR_I2CB))
            ;

        // clear all status bits
        set_mask_bits(base + REG_I2C_SR, 0xFFFFffff);
    }

    // send slave address and check status
    {
        // write address byte into DR
        // TODO: support 10-bit address
        uint8_t byte_addr = (uint8_t)((slave_addr << 1) & 0xFE);
        if(isRead)
            byte_addr |= 0x1;


        outw(base + REG_I2C_DR, byte_addr);

        // send out slave address with START condition by setting CR
        outw(base + REG_I2C_CR, (CR_ENABLE | CR_TBEN | CR_START));

        // wait for complete status
        if(KDP_BOOL_FALSE == polling_i2c_complete(base))
            return KDP_STATUS_ERROR;
    }

    // send out user data bytes (one by one) and check status
    {
        for(int i=0;i<num;i++)
        {
            uint32_t ctrl_flag = CR_ENABLE | CR_TBEN;
            if(with_STOP && (i==(num-1)))
                ctrl_flag |= CR_STOP;

            if(!isRead)
                // write data to the DR
                outw(base + REG_I2C_DR, data[i]);

            // start transmission
            outw(base + REG_I2C_CR, ctrl_flag);

            // check status
            if(KDP_BOOL_FALSE == polling_i2c_complete(base))
                return KDP_STATUS_ERROR;

            if(isRead)
                // get data
                outw(data[i], base + REG_I2C_DR);
        }
    }

    return KDP_STATUS_OK;
}

kdp_status_e kdp_i2c_transmit(kdp_i2c_ctrl_e ctrl_id, uint16_t slave_addr, const uint8_t *data, uint32_t num, kdp_bool_e with_STOP)
{
    return i2c_tx_rx(ctrl_id, slave_addr, data, num, KDP_BOOL_FALSE, with_STOP);
}

kdp_status_e kdp_i2c_receive(kdp_i2c_ctrl_e ctrl_id, uint16_t slave_addr, uint8_t *data, uint32_t num, kdp_bool_e with_STOP)
{
    return i2c_tx_rx(ctrl_id, slave_addr, data, num, KDP_BOOL_TRUE, with_STOP);
}
