/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#ifndef __KDP_PERIPHERAL_H__
#define __KDP_PERIPHERAL_H__

typedef enum{
    KDP_STATUS_OK = 0,
    KDP_STATUS_ERROR,
    KDP_STATUS_ERROR_CTRL_NO_EXIST,
    KDP_STATUS_ERROR_CTRL_INITIALIZED,
    KDP_STATUS_ERROR_CTRL_UNINITIALIZED,
    KDP_STATUS_ERROR_NO_MEMORY,
    KDP_STATUS_ERROR_I2C_RESET_FAILED
}kdp_status_e;

typedef enum{
    KDP_BOOL_TRUE = 1,
    KDP_BOOL_FALSE = 0
}kdp_bool_e;

typedef void (*kdp_user_callback_t)(void *data);

// including all perihperal related header files here
#include "kdp_pinconfig.h"
#include "kdp_gpio.h"
#include "kdp_i2c.h"
#include "kdp_spi.h"
#include "kdp_uart.h"

#endif
