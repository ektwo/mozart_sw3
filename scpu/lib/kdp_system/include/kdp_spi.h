/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#ifndef __KDP_SPI_H__
#define __KDP_SPI_H__

#include "kdp_peripheral.h"

/* support SPI master only at present */

// clock polarity
typedef enum{
    KDP_SPI_CPOL_0 = 0,
    KDP_SPI_CPOL_1
}kdp_spi_cpol_e;

// clock phase
typedef enum{
    KDP_SPI_CPHA_0 = 0,
    KDP_SPI_CPHA_1
}kdp_spi_cpha_e;

// bit order, send MSB or LSB first
typedef enum{
    KDP_SPI_MSB_FIRST = 0,
    KDP_SPI_LSB_FIRST
}kdp_spi_bitord_e;

// define SPI master configuration
typedef struct {
    kdp_spi_cpol_e cpol;
    kdp_spi_cpha_e cpha;
    kdp_spi_bitord_e bit_order;
    uint8_t frame_bits; // indicate bits per SPI transfer, shoule be 1~32
    uint32_t clock; // bus clock speed in Hz
    /* kdp_bool_e use_dma; */
}kdp_spi_config_t;

// define supported SPI controllers
typedef enum{
    KDP_SPI_CTRL_0 = 0,
    KDP_SPI_CTRL_1,
}kdp_spi_ctrl_e;

// init spi controller with specified controller ID
extern kdp_status_e kdp_spi_init(kdp_spi_ctrl_e ctrl_id);

// de-init spi controller with specified controller ID
extern kdp_status_e kdp_spi_deinit(kdp_spi_ctrl_e ctrl_id);

// set spi configuration, can be changed between transfers
extern kdp_status_e kdp_spi_set_config(kdp_spi_ctrl_e ctrl_id, kdp_spi_config_t *spi_config);

// set the SS line high (TRUE) or low (FALSE), usually active low.
extern kdp_status_e kdp_spi_set_ss(kdp_spi_ctrl_e ctrl_id, kdp_bool_e ss_high);

// transmit data out from DO line (MOSI)
extern kdp_status_e kdp_spi_transmit(kdp_spi_ctrl_e ctrl_id, const uint8_t *data, uint32_t num);

// receive data in from DI line (MISO)
extern kdp_status_e kdp_spi_receive(kdp_spi_ctrl_e ctrl_id, uint8_t *data, uint32_t num);

// do both tranmission and reception at the same time, DO + DI
extern kdp_status_e kdp_spi_transmit_receive(kdp_spi_ctrl_e ctrl_id, const uint8_t *out_data, uint8_t *in_data, uint32_t num);

#endif
