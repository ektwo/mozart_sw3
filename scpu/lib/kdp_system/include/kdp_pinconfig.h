/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#ifndef __KDP_PINCONFIG_H__
#define __KDP_PINCONFIG_H__

/* Define KDP520 all configurable pins here */
typedef enum
{
    KDP_PIN_SPI_WP_N = 0,
    KDP_PIN_SPI_HOLD_N,
    KDP_PIN_JTAG_TRST_N,
    KDP_PIN_JTAG_TDI,
    KDP_PIN_JTAG_SWDITMS,
    KDP_PIN_JTAG_SWCLKTCK,
    KDP_PIN_JTAG_TDO,
    KDP_PIN_LC_PCLK,
    KDP_PIN_LC_VS,
    KDP_PIN_LC_HS,
    KDP_PIN_LC_DE,
    KDP_PIN_LC_DATA_0,
    KDP_PIN_LC_DATA_1,
    KDP_PIN_LC_DATA_2,
    KDP_PIN_LC_DATA_3,
    KDP_PIN_LC_DATA_4,
    KDP_PIN_LC_DATA_5,
    KDP_PIN_LC_DATA_6,
    KDP_PIN_LC_DATA_7,
    KDP_PIN_LC_DATA_8,
    KDP_PIN_LC_DATA_9,
    KDP_PIN_LC_DATA_10,
    KDP_PIN_LC_DATA_11,
    KDP_PIN_LC_DATA_12,
    KDP_PIN_LC_DATA_13,
    KDP_PIN_LC_DATA_14,
    KDP_PIN_LC_DATA_15,
    KDP_PIN_SD_CLK,
    KDP_PIN_SD_CMD,
    KDP_PIN_SD_DAT_0,
    KDP_PIN_SD_DAT_1,
    KDP_PIN_SD_DAT_2,
    KDP_PIN_SD_DAT_3,
    KDP_PIN_UART0_RX,
    KDP_PIN_UART0_TX,
    KDP_PIN_I2C0_SCL,
    KDP_PIN_I2C0_SDA,
    KDP_PIN_PWM0
}kdp_pin_name_e;

/* pin configuration modes */
typedef enum{
    PIN_MODE_0 = 0,
    PIN_MODE_1,
    PIN_MODE_2,
    PIN_MODE_3,
    PIN_MODE_4,
    PIN_MODE_5,
    PIN_MODE_6,
    PIN_MODE_7
}kdp_pin_mode_t;

extern void kdp_pinconfig_set_pin_mode(
    kdp_pin_name_e pin,
    kdp_pin_mode_t mode);

#endif
