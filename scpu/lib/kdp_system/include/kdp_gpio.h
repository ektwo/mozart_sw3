/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#ifndef __KDP_GPIO_H__
#define __KDP_GPIO_H__

#include "kdp_peripheral.h"

typedef enum{
    /* pin direction */
    KDP_GPIO_DIR_INPUT = 0x1,
    KDP_GPIO_DIR_OUTPUT = 0x2,
    /* interrupt setting for input */
    KDP_GPIO_INT_EDGE_RISING = 0x4,
    KDP_GPIO_INT_EDGE_FALLLING = 0x8,
    KDP_GPIO_INT_EDGE_BOTH = 0x10,
    KDP_GPIO_INT_LEVEL_HIGH = 0x20,
    KDP_GPIO_INT_LEVEL_LOW = 0x40,
    /* internal pull-high or pull-low */
    KDP_GPIO_PAD_PULL_HIGH = 0x80,
    KDP_GPIO_PAD_PULL_LOW = 0x100
}kdp_gpio_attribute_e;

typedef enum{
    KDP_GPIO_PIN_0 = 0,
    KDP_GPIO_PIN_1,
    KDP_GPIO_PIN_2,
    KDP_GPIO_PIN_3,
    KDP_GPIO_PIN_4,
    KDP_GPIO_PIN_5,
    KDP_GPIO_PIN_6,
    KDP_GPIO_PIN_7,
    KDP_GPIO_PIN_8,
    KDP_GPIO_PIN_9,
    KDP_GPIO_PIN_10,
    KDP_GPIO_PIN_11,
    KDP_GPIO_PIN_12,
    KDP_GPIO_PIN_13,
    KDP_GPIO_PIN_14,
    KDP_GPIO_PIN_15,
    KDP_GPIO_PIN_16,
    KDP_GPIO_PIN_17,
    KDP_GPIO_PIN_18,
    KDP_GPIO_PIN_19,
    KDP_GPIO_PIN_20,
    KDP_GPIO_PIN_21,
    KDP_GPIO_PIN_22,
    KDP_GPIO_PIN_23,
    KDP_GPIO_PIN_24,
    KDP_GPIO_PIN_25,
    KDP_GPIO_PIN_26,
    KDP_GPIO_PIN_27,
    KDP_GPIO_PIN_28,
    KDP_GPIO_PIN_29,
    KDP_GPIO_PIN_30,
    KDP_GPIO_PIN_31
}kdp_gpio_pin_e;

typedef enum{
    KDP_GPIO_CTRL_0 = 0
}kdp_gpio_ctrl_e;

// init gpio controller with specified controller ID
kdp_status_e kdp_gpio_init(kdp_gpio_ctrl_e ctrl_id);

// de-init gpio controller with specified controller ID
kdp_status_e kdp_gpio_deinit(kdp_gpio_ctrl_e ctrl_id);

// set pin attributes for one GPIO pin
kdp_status_e kdp_gpio_set_pin_attribute(
    kdp_gpio_ctrl_e ctrl_id,
    kdp_gpio_pin_e pin,
    uint32_t attributes /* kdp_gpio_attribute_e */);

// interrupt: register a user callback function for interrupt execution, running in thread contexts
// in this case, there will be an internal thread to execute user's callback functions
kdp_status_e kdp_gpio_register_callback(
    kdp_gpio_ctrl_e ctrl_id,
    kdp_gpio_pin_e pin,
    kdp_user_callback_t cb_func,
    void *user_data);

// interrupt: register a CMSIS-RTOS2 thread flag to be notified when the specified pin is being interrupted.
// in this case, user's thread must wait for a specified thread flag as interrupt notifications
kdp_status_e kdp_gpio_register_threadflag(
    kdp_gpio_ctrl_e ctrl_id,
    kdp_gpio_pin_e pin,
    osThreadId_t tid,
    uint32_t tflag);

// enable / disable interrupt
kdp_status_e kdp_gpio_set_interrupt_enable(
    kdp_gpio_ctrl_e ctrl_id,
    kdp_gpio_pin_e pin,
    kdp_bool_e isEnable);

// enable / disable debounce with clock setting in Hz
kdp_status_e kdp_gpio_set_debounce_enable(
    kdp_gpio_ctrl_e ctrl_id,
    kdp_gpio_pin_e pin,
    kdp_bool_e,
    uint32_t debounce_clock /* in Hz */);

// set pin value
kdp_status_e kdp_gpio_set_pin_value(
    kdp_gpio_ctrl_e ctrl_id,
    kdp_gpio_pin_e pin,
    kdp_bool_e value);

// get pin value
kdp_status_e kdp_gpio_get_pin_value(
    kdp_gpio_ctrl_e ctrl_id,
    kdp_gpio_pin_e pin,
    kdp_bool_e *pValue);

#endif
