/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron,Inc. All rights reserved.
 *
 */

#ifndef __KDP_UART_H__
#define __KDP_UART_H__

#include "kdp_peripheral.h"

typedef enum {
    KDP_UART_BAUDRATE_100 = 100,
    KDP_UART_BAUDRATE_300 = 300,
    KDP_UART_BAUDRATE_600 = 600,
    KDP_UART_BAUDRATE_1200 = 1200,
    KDP_UART_BAUDRATE_2400 = 2400,
    KDP_UART_BAUDRATE_4800 = 4800,
    KDP_UART_BAUDRATE_9600 = 9600,
    KDP_UART_BAUDRATE_10000 = 10000,
    KDP_UART_BAUDRATE_14400 = 14400,
    KDP_UART_BAUDRATE_19200 = 19200,
    KDP_UART_BAUDRATE_38400 = 38400,
    KDP_UART_BAUDRATE_50000 = 50000,
    KDP_UART_BAUDRATE_57600 = 57600,
    KDP_UART_BAUDRATE_75000 = 75000,
    KDP_UART_BAUDRATE_100000 = 100000,
    KDP_UART_BAUDRATE_115200 = 115200,
    KDP_UART_BAUDRATE_153600 = 153600,
    KDP_UART_BAUDRATE_200000 = 200000,
    KDP_UART_BAUDRATE_225000 = 225000,
    KDP_UART_BAUDRATE_230400 = 230400,
    KDP_UART_BAUDRATE_300000 = 300000,
    KDP_UART_BAUDRATE_400000 = 400000,
    KDP_UART_BAUDRATE_460800 = 460800,
    KDP_UART_BAUDRATE_500000 = 500000,
    KDP_UART_BAUDRATE_750000 = 750000,
    KDP_UART_BAUDRATE_921600 = 921600
} kdp_uart_baudrate_e;

typedef enum {
    KDP_UART_PARITY_NONE,
    KDP_UART_PARITY_ODD,
    KDP_UART_PARITY_EVEN
} kdp_uart_parity_e;

typedef enum {
    KDP_UART_STOPBIT_ONE,
    KDP_UART_STOPBIT_TWO
} kdp_uart_stopbit_e;

typedef enum {
    KDP_UART_FLOWCTRL_NONE,
    KDP_UART_FLOWCTRL_SW // XON/XOFF
} kdp_uart_flow_control_e;

// define UART configuratoin struct
typedef struct{
    kdp_uart_baudrate_e baud_rate;
    kdp_uart_parity_e parity;
    kdp_uart_stopbit_e stop_bit;
    kdp_uart_flow_control_e flowctrl;
    uint8_t data_bits;
    /* kdp_bool_e use_dma; */
} kdp_uart_config_t;

// define supported UART controllers
typedef enum{
    KDP_UART_CTRL_0 = 0,
    KDP_UART_CTRL_1,
    KDP_UART_CTRL_2,
    KDP_UART_CTRL_3
} kdp_uart_ctrl_e;

// init uart controller with specified controller ID
// also set up uart configurations1
extern kdp_status_e kdp_uart_init(kdp_uart_ctrl_e ctrl_id, kdp_uart_config_t *conf);

// de-init uart controller with specified controller ID
extern kdp_status_e kdp_uart_deinit(kdp_uart_ctrl_e ctrl_id);

// (polling)
// send data through UART interface, note: num means number of UART data frames (5~9 bits), not bytes
// this function will not return until tx all data or reach timeout
extern kdp_status_e kdp_uart_send(kdp_uart_ctrl_e ctrl_id, const uint8_t *data, uint32_t num, uint32_t try_timeout);

// (polling)
// receive data through UART interface, note: num means number of UART data frames (5~9 bits), not bytes
// this function will not return until rx all data or reach timeout
extern kdp_status_e kdp_uart_receive(kdp_uart_ctrl_e ctrl_id, uint8_t *data, uint32_t num, uint32_t try_timeout);


// to register a CMSIS-RTOS2 thread flag to let user to be notified when RX interrupt is occuring;
// user should use osThreadFlagsWait() to wait for the specified flag,
// once receiving the flag, user can use kdp_uart_receive() to read data.
extern kdp_status_e kdp_gpio_receive_register_notification(kdp_uart_ctrl_e ctrl_id, osThreadId_t tid, uint32_t tflag);

#endif
