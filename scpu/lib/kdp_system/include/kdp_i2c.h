/*
 * Kneron Peripheral API
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#ifndef __KDP_I2C_H__
#define __KDP_I2C_H__

#include "kdp_peripheral.h"

// define supported bus speed
typedef enum{
    KDP_I2C_SPEED_100K = 0,
    KDP_I2C_SPEED_400K,
    KDP_I2C_SPEED_1M
}kdp_i2c_bus_speed_e;

// define supported I2C controllers
typedef enum{
    KDP_I2C_CTRL_0 = 0,
    KDP_I2C_CTRL_1,
    KDP_I2C_CTRL_2,
    KDP_I2C_CTRL_3
}kdp_i2c_ctrl_e;

// init i2c controller with specified controller ID
// also set i2c bus speed, supporting standard(100kHz), fast(400kHz) and fast plus (1MHz)
extern kdp_status_e kdp_i2c_init(
    kdp_i2c_ctrl_e ctrl_id,
    kdp_i2c_bus_speed_e bus_speed);

// de-init i2c controller with specified controller ID
extern kdp_status_e kdp_i2c_deinit(kdp_i2c_ctrl_e ctrl_id);

// (polling) transmit data to a specified address, the STOP condition can be optionally not generated.
extern kdp_status_e kdp_i2c_transmit(
    kdp_i2c_ctrl_e ctrl_id,
    uint16_t slave_addr,
    const uint8_t *data,
    uint32_t num,
    kdp_bool_e with_STOP);

// (polling) receive data from a specified address, the STOP condition can be optionally not generated.
extern kdp_status_e kdp_i2c_receive(
    kdp_i2c_ctrl_e ctrl_id,
    uint16_t slave_addr,
    uint8_t *data,
    uint32_t num,
    kdp_bool_e with_STOP);

#endif
