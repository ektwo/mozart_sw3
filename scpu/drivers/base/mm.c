/*
 * @name : mm.c
 * @brief : map handle/pin_context into memory
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#include <types.h> // NULL
#include <framework/mm.h>
#include <dbg.h>


struct mm_struct init_mm = {};

//#define AUTO_CALCULATE_SIZE
    
void* dm_alloc(void *alloc_ctx, unsigned long size) {

    void *ret = NULL;
    struct mm_struct *mm = get_mm_struct();    
    
    if (alloc_ctx) {
        struct videobuf_dm_ctx *ctx = (struct videobuf_dm_ctx *)alloc_ctx;
        unsigned int next_start_addr;
    #ifdef AUTO_CALCULATE_SIZE        
        next_start_addr = ctx->phy_start_addr + ctx->allocated_length;
        ctx->allocated_length += size;
    #else
        DSG("dm_alloc ctx->idx=%d phy_start_addr=%x vv=%x", ctx->idx, ctx->phy_start_addr, (mm->mmap->vm_start + mm->csirx0_mm_phy));
        if (1 == ctx->idx) {
            if (ctx->phy_start_addr == (mm->mmap->vm_start + mm->csirx0_mm_phy))
                next_start_addr = mm->mmap->vm_start + mm->csirx0_mm_phy_2;
            else if (ctx->phy_start_addr == (mm->mmap->vm_start + mm->csirx1_mm_phy))
                next_start_addr = mm->mmap->vm_start + mm->csirx1_mm_phy_2;            
        }
        else {
            if (ctx->phy_start_addr == (mm->mmap->vm_start + mm->csirx0_mm_phy))
                next_start_addr = ctx->phy_start_addr + ctx->allocated_length; 
            else if (ctx->phy_start_addr == (mm->mmap->vm_start + mm->csirx1_mm_phy))
                next_start_addr = ctx->phy_start_addr + ctx->allocated_length;            
        }
        DSG("dm_alloc next_start_addr=%x", next_start_addr);
    #endif
        
        ctx->idx++;

        ret = (void*)next_start_addr;
    }

    return ret;
}

struct mm_struct *get_mm_struct(void) {
    return &init_mm;
}

void *kdp_mmap( unsigned int length, 
        int flags, 
        int fd, 
        unsigned int offset) {

    const struct mm_struct *mm = get_mm_struct();

    if ((0 == length) || (fd <= 0))
        return MAP_FAILED;
    
    struct vm_area_struct *vma = mm->mmap;

    if (offset > (vma->vm_end - vma->vm_start)) 
        offset = (vma->vm_end - vma->vm_start);

    return (void*)(init_mm.mmap_base + vma->vm_start + offset);
}
