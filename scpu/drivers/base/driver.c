/*
 * @name : driver.c
 * @brief : centralized driver management
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#include <string.h>
#include <framework/utils.h>
#include <framework/ioport.h>
#include <framework/driver.h>
#include <framework/errno.h>
#include <dbg.h>


struct kdp_list g_klist_drivers;

static struct driver_context *_next_driver(struct kdp_list_iter *i)
{
    struct kdp_list_node *node = kdp_list_next(i);
    struct driver_context *drv = NULL;
    
    if (node) {
        drv = container_of(node, struct driver_context, knode_to_buslist);
    }
    
    return drv;
}

void *dev_get_drvdata(const struct pin_context *pin_ctx) {
    return pin_ctx->driver_data;
}
void dev_set_drvdata(struct pin_context *pin_ctx, void *data) {
    pin_ctx->driver_data = data;
}

struct driver_context* find_driver(char *drv_name)
{
    struct kdp_list_iter iter;
    struct driver_context *drv = NULL;

    kdp_list_iter_init_node(&g_klist_drivers, &iter);
    do {
        drv = _next_driver(&iter);
        if ((drv) && (strstr(drv->name, drv_name)))
        {
            break;
        }
    } while (drv);
    
    kdp_list_iter_exit(&iter);

    return drv;
}

static int _core_drv_add(struct driver_context *dev_drv, struct pin_context *pin_ctx)
{
    int ret = -ENODEV;    
    struct core_driver *drv = to_core_driver(dev_drv);
    struct core_device *dev = to_core_device(pin_ctx);

    if (drv->probe)
        ret = drv->probe(dev);

    return ret;
}

static int _core_drv_del(struct driver_context *dev_drv, struct pin_context *pin_ctx)
{
    int ret = -ENODEV;    
    struct core_driver *drv = to_core_driver(dev_drv);
    struct core_device *dev = to_core_device(pin_ctx);

    if (drv->remove)
        ret = drv->remove(dev);

    return ret;
}

struct ioport_setting *driver_get_ioport_setting(struct core_device *core_d,
        unsigned int type)
{
    int i;
    for (i = 0; i < core_d->num_ioports; i++) {
        struct ioport_setting *io = &core_d->ioport[i];
        if (type == io->flags)
            return io;
    }

    return NULL;
}

int INITTEXT drivers_init(void)
{
    kdp_list_init(&g_klist_drivers);

    return 0;
}

int driver_register(struct driver_context *drv, struct pin_context *pin_ctx)
{
    int ret = -1;

    kdp_list_add_node_tail(&g_klist_drivers, &drv->knode_to_buslist);

    if (drv->add) {
        ret = drv->add(drv, pin_ctx);
        if (0 == ret)
            DSG("driver [%s] registered", drv->name);
    }

    return ret;
}

void driver_unregister(struct driver_context *drv)
{

}

//int driver_core_register(struct core_driver *drv)
int driver_core_register(void *core_driver)
{
    struct core_driver *drv = (struct core_driver *)core_driver;    
    drv->driver.add = _core_drv_add;
    drv->driver.del = _core_drv_del;

    return driver_register(&drv->driver, &(drv->core_dev->pin_ctx));
}

//void driver_core_unregister(struct core_driver *drv)
void driver_core_unregister(void *core_driver)
{
    struct core_driver *drv = (struct core_driver *)core_driver;    
    driver_unregister(&drv->driver);
}
