/*
 * @name : i2c_core.c
 * @brief : i2c driver interface for Mozart
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <framework/utils.h>
#include <framework/init.h>
#include <framework/driver.h>
#include <framework/errno.h>
#include <interface/i2c.h>
#include <dbg.h>


#define I2C_ADAPTER_MAX_SIZE 4

typedef enum e_i2c_adapter_status { 
    i2c_adapter_empty = -1,
    i2c_adapter_alloc,
    i2c_adapter_used
} i2c_adapter_status;

struct i2c_adapter_desc {
    int status;
    struct i2c_adapter *adapter;
};

static struct i2c_adapter_desc i2c_adapter_idr[I2C_ADAPTER_MAX_SIZE];

struct i2c_adapter *i2c_get_adapter(int nr)
{
    struct i2c_adapter *adapter = NULL;
    int i;
    
    for (i = 0; i < I2C_ADAPTER_MAX_SIZE; ++i) {
        if (i2c_adapter_idr[i].status == i2c_adapter_alloc) {
            i2c_adapter_idr[i].status = i2c_adapter_used;
            adapter = i2c_adapter_idr[i].adapter;
            break;
        }		
    }

    if (!adapter)
        goto exit;

 exit:
    return adapter;
}

int i2c_transfer(struct i2c_adapter *adap, struct i2c_msg *msgs, int num)
{
    int ret;

    if (adap->protocol->master_xfer) {
        int ret, try;

        for (ret = 0, try = 0; try <= adap->retries; try++) {
            ret = adap->protocol->master_xfer(adap, msgs, num);
            if (ret != -EAGAIN)
                break;
            // sleep
        }
    } else {
        ret = -EIO;
    }
    
    return ret;
}

int i2c_add_adapter(struct i2c_adapter *adap)
{
    int ret = -EINVAL;

    if (adap) {
        if (i2c_adapter_idr[adap->host_id].status == i2c_adapter_empty)
        {
            i2c_adapter_idr[adap->host_id].status = i2c_adapter_alloc;
            i2c_adapter_idr[adap->host_id].adapter = adap;
        }
    }

    return ret;
}

static int _i2c_drv_add(struct driver_context *dev_drv, struct pin_context *pin_ctx)
{
    struct i2c_driver *drv = to_i2c_driver(dev_drv);
    struct i2c_client *client = to_i2c_client(pin_ctx);
    int ret = -ENODEV;

    if (drv->probe)
        ret = drv->probe(client);

    return ret;
}

static int _i2c_drv_del(struct driver_context *dev_drv, struct pin_context *pin_ctx)
{
    struct i2c_driver *drv = to_i2c_driver(dev_drv);
    struct i2c_client *client = to_i2c_client(pin_ctx);
    int ret = -ENODEV;

    if (drv->remove)
        ret = drv->remove(client);

    return ret;
}

int i2c_driver_register(struct i2c_driver *drv)
{
    drv->driver.add = _i2c_drv_add;
    drv->driver.del = _i2c_drv_del;

    return driver_register(&drv->driver, &(drv->core_dev->pin_ctx));
}

void i2c_driver_unregister(struct i2c_driver *driver)
{
    driver_unregister(&driver->driver);
}

int INITTEXT i2c_entrance(void) {
    int i;
    for (i = 0; i < I2C_ADAPTER_MAX_SIZE; ++i) {
        i2c_adapter_idr[i].status = i2c_adapter_empty;
    }

    return 0;
}

void FINITEXT i2c_exit(void) {
    int i;
    for (i = 0; i < I2C_ADAPTER_MAX_SIZE; ++i) {
        i2c_adapter_idr[i].status = i2c_adapter_empty;
    }    
}
ARRANGE_ENTR_RO_SECTION(i2c_entrance, 2)
ARRANGE_EXIT_RO_SECTION(i2c_exit)

