#ifdef V2K_CAM_ENABLE
/*
 * @name : kdp520_i2c.c
 * @brief : i2c driver for kdp520 i2c controller
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#include <stdlib.h>
#include <string.h>
#include <config/board_kdp520.h>
#include <framework/errno.h>
#include <framework/ioport.h>
#include <framework/driver.h>
#include <framework/init.h>
#include <interface/i2c.h>
#include <dbg.h>


#define KDP520_I2C_DRV_NAME "kdp520_i2c"
#define I2C_GSR_Value       5
#define I2C_TSR_Value       5
#define I2C_MAX_FREQ        (400 * 1000)//(400 * 1024)

#define I2C_RD(dev)         ((((dev) << 1) & 0xfe) | 1)
#define I2C_WR(dev)         (((dev) << 1) & 0xfe)


/* I2C Register */
#define I2C_REG_CONTROL     0x0
#define I2C_REG_STATUS      0x4
#define I2C_REG_CLOCKDIV    0x8
#define I2C_REG_DATA        0xC
#define I2C_REG_TGSR        0x14

/* I2C control register */
#define CR_ALIRQ      0x2000  /* arbitration lost interrupt (master) */
#define CR_NAKRIRQ    0x400   /* NACK response interrupt (master) */
#define CR_DRIRQ      0x200   /* rx interrupt (both) */
#define CR_DTIRQ      0x100   /* tx interrupt (both) */
#define CR_TBEN       0x80    /* tx enable (both) */
#define CR_NAK        0x40    /* NACK (both) */
#define CR_STOP       0x20    /* stop (master) */
#define CR_START      0x10    /* start (master) */
#define CR_SCLEN      0x4     /* enable clock out (master) */
#define CR_I2CEN      0x2     /* enable I2C (both) */
#define CR_ENABLE     \
    (CR_ALIRQ | CR_NAKRIRQ | CR_DRIRQ | CR_DTIRQ | CR_SCLEN | CR_I2CEN)

/* I2C Status register */
#define SR_TD         0x20     /* tx/rx finish */
#define SR_BUSY       0x4      /* chip busy */	


struct kdp520_i2c_context {
    struct pin_context *pin_ctx;
    void *base;
    struct i2c_adapter adapter;
    u8 *msg_buf;
};

static void kdp520_i2c_reset(struct kdp520_i2c_context *);

static bool kdp_i2c_check_busy(u32 base) {
    if((readl(base + I2C_REG_STATUS) & SR_BUSY)== SR_BUSY)
     return true;

    return false;
}

static inline void *i2c_get_adapdata(const struct i2c_adapter *adapter) {
    return dev_get_drvdata(&adapter->pin_ctx);
}

static inline void i2c_set_adapdata(struct i2c_adapter *adapter, void *data) {
    dev_set_drvdata(&adapter->pin_ctx, data);
}

static int _kdp_i2c_wait(int base, uint32_t mask)
{
    int ret = -EIO;
    uint32_t stat, ts;

    for (ts = 0; ts < 55000; ts++) {
        stat = readl(base + I2C_REG_STATUS);
        writel(stat, base + I2C_REG_STATUS);
        if ((stat & mask) == mask) {
            ret = 0;//it means ok
            break;
        }
    }

    return ret;
}

static int kdp520_i2c_xfer_msg(struct kdp520_i2c_context *i2c_ctx, struct i2c_msg *msg, bool is_stop)
{
    int ret, i;
    u16 flags = msg->flags;
    u16 slave_addr = msg->addr;
    u16 len = msg->len;
    u32 base = (u32)i2c_ctx->base;
    u8 *buf = msg->buf;
    bool is_read = (flags & I2C_MASTER_READ);

    if (is_read)
        writel(I2C_RD(slave_addr), base + I2C_REG_DATA);
    else
        writel(I2C_WR(slave_addr), base + I2C_REG_DATA);

    writel(CR_ENABLE | CR_TBEN | CR_START, base + I2C_REG_CONTROL);
    ret = _kdp_i2c_wait(base, SR_TD);
    if (ret)
        goto ewait;
    {
        u32 ctrl = CR_ENABLE | CR_TBEN;
        for (i = 0; i < len; ++i) {
            if ((is_stop) && (i == len - 1)) {
                if (is_read)
                    ctrl |= CR_NAK | CR_STOP;
                else
                    ctrl |= CR_STOP;
            }
                
            writel(buf[i], base + I2C_REG_DATA);
            writel(ctrl, base + I2C_REG_CONTROL);
            ret = _kdp_i2c_wait(base, SR_TD);
            if (ret)
                break;
        }
    }

    //kdp520_i2c_reset(i2c_ctx);
    
ewait:
    return ret;
}

static int kdp520_i2c_xfer(struct i2c_adapter *, struct i2c_msg *, int );
static struct i2c_protocol kdp520_i2c_algorithm = {
    .master_xfer	= kdp520_i2c_xfer,
};

static int kdp520_i2c_xfer(struct i2c_adapter *adapter,
        struct i2c_msg *msgs,
        int num)
{
    int ret = 0;
    struct kdp520_i2c_context *i2c_ctx = i2c_get_adapdata(adapter);
    int i;

    //enable i2c clock

    for (i = 0; i < num; ++i) {
        ret = kdp520_i2c_xfer_msg(i2c_ctx, &msgs[i], (i == (num - 1)));
        if (0 != ret)
            break;
    }

    while (kdp_i2c_check_busy((u32)i2c_ctx->base));     

    //disable i2c clock

    return ret;
}

u32 kdp520_i2c_getgsr(struct kdp520_i2c_context *i2c_ctx)
{
    u32 tmp;

    tmp = readl(((u8*)i2c_ctx->base + I2C_REG_TGSR));

    return ((tmp >> 10) & 0x7);
}

static void kdp520_i2c_reset(struct kdp520_i2c_context *i2c_ctx)
{
    struct kdp520_i2c_context *ctx = i2c_ctx;
    u32 ts;
    writel(1, ((u8*)ctx->base + I2C_REG_CONTROL));
    for (ts = 0; ts < 50000; ts++) {
        if (!(readl(((u8*)ctx->base + I2C_REG_CONTROL)) & 0x1)) {
            break;
        }
    }
}

static void kdp520_i2c_init(struct kdp520_i2c_context *i2c_ctx)
{
    struct kdp520_i2c_context *ctx = i2c_ctx;	
    struct i2c_platform_data *pdata = ctx->pin_ctx->platform_data;//dev_get_platdata(ctx->dev_ctx);	

    writel((I2C_GSR_Value << 10)|I2C_TSR_Value, ((u8*)ctx->base + I2C_REG_TGSR));

    int bus_speed = pdata->bus_speed;// * 1000;
    int bits = ((APB_CLOCK - (bus_speed * kdp520_i2c_getgsr(ctx))) / ( 2 * bus_speed)) - 2;
    if (bits < BIT10) 
        writel(bits, ((u8*)ctx->base + I2C_REG_CLOCKDIV));

    kdp520_i2c_reset(ctx);
}

static int kdp520_i2c_probe(struct core_device *core_d)
{
    int ret;
    //struct i2c_platform_data *pdata = dev_get_platdata(&core_d->dev_ctx);	
    struct kdp520_i2c_context *i2c_ctx;
    
    DSG("<%s:%s>", __FILE__, __func__);
    
    i2c_ctx = (struct kdp520_i2c_context*)calloc(1, sizeof(*i2c_ctx));
    if (!i2c_ctx)
        return -ENOMEM;

    i2c_ctx->base = (void*)driver_get_ioport_setting(core_d, IORESOURCE_MEM)->start;
    i2c_ctx->pin_ctx = &core_d->pin_ctx;
    dev_set_drvdata(&core_d->pin_ctx, (void*)i2c_ctx);

    i2c_set_adapdata(&i2c_ctx->adapter, i2c_ctx);
    i2c_ctx->adapter.protocol = &kdp520_i2c_algorithm;	
    i2c_ctx->adapter.retries = 5;
    i2c_ctx->adapter.host_id = core_d->uuid;

    kdp520_i2c_init(i2c_ctx);

    ret = i2c_add_adapter(&i2c_ctx->adapter);
    if (ret) {
        // DSG("Adapter %s registration failed",i2c_ctx->adapter.name);
        return ret;
    }
    
    return 0;   /* Return OK */
}

static int kdp520_i2c_remove(struct core_device *core_d)
{
    return 0;
}

extern struct core_device kdp520_i2c0;
static struct core_driver kdp520_i2c_driver = {
    .probe      = kdp520_i2c_probe,
    .remove     = kdp520_i2c_remove,
    .driver     = {
        .name = KDP520_I2C_DRV_NAME,
    },
    .core_dev   = &kdp520_i2c0,
};
KDP_CORE_DRIVER_SETUP(kdp520_i2c_driver, 3); 


#endif
