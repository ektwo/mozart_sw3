#include <memxfer.h>
#include <string.h> 
#include <io.h>
#include <framework/utils.h>
#include <DrvSPI020.h>
#include <DrvDMAC020.h>
#include <dbg.h>

#define FLASH_WB_DEV 0xEF
#define FLASH_MXIC_DEV 0xC2
#define FLASH_Micron_DEV 0x20
#define FLASH_SIZE_1MB_ID 0x14

#define MEMXFER_INITED      0x10
#define MEMXFER_OPS_MASK MEMXFER_OPS_CPU | MEMXFER_OPS_DMA

#if FLASH_4BYTES_CMD_EN
extern void spi020_4Bytes_ctrl(UINT8 enable);
#endif    

#define _get_min(x,y) ( x < y ? x: y )

static u8 _flash_mode = MEMXFER_OPS_NONE;
static u8 _mem_mode = MEMXFER_OPS_NONE;

static SPI_Flash_T _flash_info;

extern void spi020_set_commands(UINT32 cmd0, UINT32 cmd1, UINT32 cmd2, UINT32 cmd3);
extern void spi020_wait_command_complete(void);
extern void spi020_write_control(UINT8 enable);
extern void spi020_check_status_til_ready(void);
extern void kdp_flash_to_ddr_dma_copy(UINT32 *src_addr, UINT32 *dst_addr, UINT32 size);
extern void kdp_ddr_to_flash_dma_copy(UINT32 src_addr, UINT32 dst_addr, UINT32 size);


int read_spi_flash_id(void)
{
    int ret = -1;
    u32 probe_90_instruction; 
    u32 size_id;
    char *flash_manu;

    probe_90_instruction = spi020_flash_probe(&_flash_info);
    if (1 != probe_90_instruction)
    {
        //DSG("probe_90_instruction=%d", probe_90_instruction);
        switch(_flash_info.manufacturer)
        {
        case FLASH_WB_DEV:
            flash_manu = "WINBOND";
            break;
        case FLASH_MXIC_DEV:
            flash_manu = "MXIC";
            break;
        case FLASH_Micron_DEV:
            flash_manu = "Micron";
        break;
        default:
            flash_manu = "Unknown";
        break;
        }

        size_id = (_flash_info.flash_id & 0xFF00) >> 8;
        if(size_id >= FLASH_SIZE_1MB_ID)
            _flash_info.flash_size = 0x100000 * (1 << (size_id - FLASH_SIZE_1MB_ID));

        fLib_printf("Manufacturer ID = 0x%02X (%s)\n", _flash_info.manufacturer, flash_manu);
        fLib_printf("Device ID       = 0x");
        if(probe_90_instruction)
            fLib_printf("%02X\n", _flash_info.flash_id);
        else
            fLib_printf("%04X\n", _flash_info.flash_id);

        fLib_printf("Flash Size      = ");
        if((_flash_info.flash_size % 1024) == 0x00)
        {
            if(((_flash_info.flash_size % 1024)%1024) == 0x00)
            {
                fLib_printf("%dByte(%dMByte)\n", _flash_info.flash_size, _flash_info.flash_size >> 10 >> 10);
            }
            else
            {
                fLib_printf("%dByte=%dKByte)\n", _flash_info.flash_size, _flash_info.flash_size >> 10);
            }
        }
        else
        {
            fLib_printf("%dByte\n", _flash_info.flash_size);
        }
        
        //FlashInfo.flash_size = _flash_info.flash_size;
        ret = 0;
    }
    
    return ret;
}

int kdp_memxfer_init(u8 flash_mode, u8 mem_mode)
{
    int ret = -1;
    u32 reg;
    _flash_mode = (_flash_mode & (~MEMXFER_OPS_MASK&0xFF)) | flash_mode;
    _mem_mode = (_mem_mode & (~MEMXFER_OPS_MASK&0xFF)) | mem_mode;
    if (!(_flash_mode & MEMXFER_INITED)) {
        vLib_LeWrite32(SPI020REG_CONTROL, SPI020_ABORT);
        do
        {
            if((u32Lib_LeRead32(SPI020REG_CONTROL)&SPI020_ABORT)==0x00)
             break;
        }while(1);

        /* Set control register */
        reg = u32Lib_LeRead32(SPI020REG_CONTROL); // 0x80
        reg &= ~(SPI020_CLK_MODE | SPI020_CLK_DIVIDER);
        reg |= SPI_CLK_MODE0 | SPI_CLK_DIVIDER_8;//SPI_CLK_DIVIDER_2;//SPI_CLK_DIVIDER_8;
        vLib_LeWrite32(SPI020REG_CONTROL, reg);
        ret = read_spi_flash_id();

        #if FLASH_4BYTES_CMD_EN
        spi020_4Bytes_ctrl(1);
        #else
        spi020_4Bytes_ctrl(0);
        #endif    

        //spi020_write_control(1);/* send write enabled */
        //spi020_set_commands(SPI020_C7_CMD0, SPI020_C7_CMD1, SPI020_C7_CMD2, SPI020_C7_CMD3);
        //spi020_check_status_til_ready();    
        _flash_mode |= MEMXFER_INITED;
    }
    
    return ret;
}

static 
int _kdp_memxfer_flash_to_ddr(u32 dst, u32 src, size_t bytes)
{
    s32 total_lens;
    s32 access_byte;
    u32 write_addr;
    u32 read_data;
    s32 rx_fifo_depth;
    
    if ((bytes & 0x3) > 0) return -1;
    
    total_lens = bytes;
    write_addr = dst;
    rx_fifo_depth = (s32)spi020_rxfifo_depth();
    //read from flash
    //write to ddr

    #if FLASH_4BYTES_CMD_EN
    spi020_4Bytes_ctrl(1);
    spi020_set_commands(src, SPI020_13_CMD1, total_lens, SPI020_13_CMD3);
    #else
    spi020_set_commands(src, SPI020_03_CMD1, total_lens, SPI020_03_CMD3);
    #endif

    while (total_lens > 0)
    {        
        spi020_wait_rx_full();

        access_byte = _get_min(total_lens, rx_fifo_depth);
        total_lens -= access_byte;   

        while (access_byte > 0)
        {
            read_data = u32Lib_LeRead32((unsigned char* )SPI020REG_DATAPORT);
            outw(write_addr, read_data);
            write_addr += 4;
            access_byte -= 4;
        }
    }    
    spi020_wait_command_complete();/* wait for command complete */

#if FLASH_4BYTES_CMD_EN
    spi020_4Bytes_ctrl(0);
#endif    

    return 0;
}

static
int kdp_memxfer_flash_to_ddr_dma(u32 dst, u32 src, size_t bytes)
{
    s32 total_lens = bytes;
    
    //read from flash
    vLib_LeWrite32(SPI020REG_INTERRUPT, SPI020_DMA_EN);/* enable DMA function */
    #if FLASH_4BYTES_CMD_EN
    spi020_4Bytes_ctrl(1);
    spi020_set_commands(src, SPI020_13_CMD1, total_lens, SPI020_13_CMD3);
    #else
    spi020_set_commands(src, SPI020_03_CMD1, total_lens, SPI020_03_CMD3);
    #endif

    //write to ddr
    
    //kdp_flash_to_ddr_dma_copy((UINT32 *)SPI020REG_DATAPORT, (UINT32 *)dst, (UINT32)total_lens);

#if FLASH_4BYTES_CMD_EN
    spi020_4Bytes_ctrl(0);
#endif
    
    return 0;
}

static
int _kdp_memxfer_ddr_to_flash(u32 dst, u32 src, size_t bytes)
{
#define BLOCK_SIZE 256
#define SECTOR_ERASE_SIZE 4096

    s32 total_lens;
    u32 erase_dst_addr;     
    u32 write_dst_addr;    
    s32 write_len, write_len2;        
    s32 access_byte;       
    u32 read_addr;
    u32 write_data;
    s32 tx_fifo_depth;

    if ((dst & 0x00000FFF) > 0) return -1;
    if ((src & 0x3) > 0) return -1;
    if ((bytes & 0x3) > 0) return -1;

    //erase flash
    total_lens = bytes;    
    erase_dst_addr = dst;

    #if FLASH_4BYTES_CMD_EN
    spi020_4Bytes_ctrl(1);
    #endif

    while (total_lens > 0) 
    {     
        spi020_write_control(1);
        #if FLASH_4BYTES_CMD_EN
        spi020_set_commands(erase_dst_addr, SPI020_21_CMD1, SPI020_21_CMD2, SPI020_21_CMD3);
        #else
        spi020_set_commands(erase_dst_addr, SPI020_20_CMD1, SPI020_20_CMD2, SPI020_20_CMD3);
        #endif       

        spi020_check_status_til_ready();
        
        total_lens -= SECTOR_ERASE_SIZE;
        if (total_lens <= 0)
            break;
        
        erase_dst_addr += SECTOR_ERASE_SIZE;
    }
    total_lens = bytes;
    write_dst_addr = dst;
    read_addr = src;
    tx_fifo_depth = (s32)spi020_txfifo_depth();    

    while (total_lens > 0) {
        spi020_write_control(1);
        
        write_len = _get_min(total_lens, BLOCK_SIZE);
        #if FLASH_4BYTES_CMD_EN     
        spi020_set_commands(write_dst_addr, SPI020_12_CMD1, write_len, SPI020_12_CMD3);
        #else
        spi020_set_commands(write_dst_addr, SPI020_02_CMD1, write_len, SPI020_02_CMD3);        
        #endif        
        
        write_dst_addr += write_len;
        write_len2 = write_len;
        
        while(write_len2 > 0)
        {
            spi020_wait_tx_empty();
            access_byte = GET_MIN(write_len2, tx_fifo_depth);
            write_len2 -= access_byte;
            while(access_byte > 0)
            {
                write_data = inw(read_addr);                
                vLib_LeWrite32((unsigned char* )SPI020REG_DATAPORT, write_data);
                read_addr += 4;
                access_byte -= 4;
            }
        }        
        
        spi020_check_status_til_ready();
        total_lens -= write_len;
    }
#if FLASH_4BYTES_CMD_EN
    spi020_4Bytes_ctrl(0);
#endif        
    
    return 0;
}

static
int kdp_memxfer_ddr_to_flash_dma(u32 dst, u32 src, size_t bytes)
{
    s32 total_lens = bytes;

    if ((total_lens & 0x3) > 0) return -1;

#if FLASH_4BYTES_CMD_EN
    spi020_4Bytes_ctrl(1);
#endif
    
    //read from ddr
    
    //write to flash
    spi020_write_control(1);/* send write enabled */   
//spi020_set_commands(dst, SPI020_02_CMD1, total_lens, SPI020_02_CMD3);
    vLib_LeWrite32(SPI020REG_INTERRUPT, SPI020_DMA_EN);/* enable DMA function */
    //kdp_ddr_to_flash_dma_copy((UINT32 *)SPI020REG_DATAPORT, (UINT32 *)dst, (UINT32)total_lens);
    //kdp_ddr_to_flash_dma_copy(src, dst, total_lens);
    // To Do ...

#if FLASH_4BYTES_CMD_EN
    spi020_4Bytes_ctrl(0);
#endif    
    
    return 0;
}

static
int _kdp_memxfer_ddr_to_ddr(u32 dst, u32 src, size_t bytes)
{
    u32 i;
    size_t loop = bytes >> 2;
    //DSG("loop=%d dst=%x src=%x", loop, dst, src);
    for (i = 0; i < loop; ++i) {
        outw(dst + (i << 2), inw(src + (i << 2)));
    }
    
    return 0;
}

static
int kdp_memxfer_ddr_to_ddr_dma(u32 dst, u32 src, size_t bytes)
{
    s32 total_lens = bytes;
    u32 dma_ch = 0;

    memset((void*)dst, 0, total_lens);

    fLib_InitDMA(FALSE, FALSE, 0x0);  
    fLib_DMA_ClearAllInterrupt(); 
    fLib_DMA_EnableDMAInt(); //Enable DMA Interrupt        
    
    fLib_DMA_NormalMode(dma_ch, src, dst, total_lens, 2, 2, 4, 0, 0, 0, 0, 0);
    fLib_EnableDMAChannel(dma_ch);
    fLib_DMA_WaitDMAInt(dma_ch);
    fLib_DisableDMAChannel(dma_ch);  


    return 0;
}
int kdp_memxfer_flash_to_ddr(u32 dst, u32 src, size_t bytes)
{
    //TODO: Fixed flash DMA
    //if (_flash_mode & MEMXFER_OPS_DMA) 
    //    return kdp_memxfer_flash_to_ddr_dma(dst, src, bytes);

    return _kdp_memxfer_flash_to_ddr(dst, src, bytes);
}

int kdp_memxfer_ddr_to_flash(u32 dst, u32 src, size_t bytes)
{
    //TODO: Fixed flash DMA
    //if (_flash_mode & MEMXFER_OPS_DMA)
    //    return kdp_memxfer_ddr_to_flash_dma(dst, src, bytes);

    return _kdp_memxfer_ddr_to_flash(dst, src, bytes);
}

int kdp_memxfer_ddr_to_ddr(u32 dst, u32 src, size_t bytes)
{
    //TODO: Fixed flash DMA
    //if (_mem_mode & MEMXFER_OPS_DMA) 
    //    return kdp_memxfer_ddr_to_ddr_dma(dst, src, bytes);

    return _kdp_memxfer_ddr_to_ddr(dst, src, bytes);
}


const struct s_kdp_memxfer kdp_memxfer_module = {
    kdp_memxfer_init,
    kdp_memxfer_flash_to_ddr,
    kdp_memxfer_ddr_to_flash,
    kdp_memxfer_flash_to_ddr_dma,
    kdp_memxfer_ddr_to_flash_dma,    
    kdp_memxfer_ddr_to_ddr,
};
