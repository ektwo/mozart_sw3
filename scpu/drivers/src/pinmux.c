#include "scu_extreg.h"


//#define PINMUX_TEST
#ifdef PINMUX_TEST
void pinmux_test() {
    SCU_EXTREG_CLK_EN0_SET_scpu_traceclk(1);
    SCU_EXTREG_CLK_MUX_SEL_SET_ncpu_traceclk(1);
    SCU_EXTREG_CLK_DIV7_SET_scpu_traceclk_div(1);
    
    //SCU_EXTREG_LC_PCLK_IOCTL_SET_sel(0);
    SCU_EXTREG_LC_PCLK_IOCTL_SET_sel(6);
}
#endif

void pinmux_set_lcd() 
{
	int i;
	
	for(i=0x17c;i<0x19c;i+=4){
		if( i == 0x194 || i==0x198) continue;
		outw(SCU_EXTREG_PA_BASE + i, 0x1);
	}
}
