
  	// --------------------------------------------------------------------
  	//	TV Encode
  	// --------------------------------------------------------------------
  	#undef PANEL_WIDTH
    #undef PANEL_HEIGHT
    #define PANEL_WIDTH             640
    #define PANEL_HEIGHT    		480
  	{ // panel id,	Descriptor
        "HAMI 640x480 TV Encode",
    
        //LCDEnable
         (LCD_FALSE	<< 11)			//Test pattern generator, not implement now
        |(0	<<  1)					//LCD screen on/off control
        |(0		 ),					//LCD controller enable control
        
        //PanelPixel
         (LCD_FALSE	<< 15)			//LC_CLK domain reset
        |(LCD_FALSE	<< 14)			//HCLK domain reset
        |(0			<< 11)			//TFT panel color depth selection, 0 -> 6-bit per channel
		|(0			<<  4),			//RGB or BGR format selection, 0 -> RGB

		#define H_FRONT			210
		#define H_PW			4
		#define H_LINES			858			// fixed value
		#define H_BACK			(H_LINES - H_FRONT - H_PW - PANEL_WIDTH)
		//HorizontalTiming
		 ((H_BACK	-1) << 24)	 		//Horizontal back porch
		|((H_FRONT	-1) << 16)			//Horizontal front porch
		|((H_PW		-1) <<  8)			//Horizontal Sync. pulse width
		|((PANEL_WIDTH >> 4)-   1),			//pixels-per-line = 16(PPL+1)

		#undef V_FRONT
		#undef V_PW
		#undef V_LINES
		#undef V_BACK
		#define V_FRONT		1
		#define V_PW		18			// pulse width
		#define V_LINES		525			// this value is fixed to 525 (defined by NTSC)
		#define V_BACK			(V_LINES - V_FRONT - V_PW - PANEL_HEIGHT)
		//VerticalTiming1
		 (V_FRONT	<< 24)			//Vertical front porch
		|((V_PW	-1)	<< 16) 			//Vertical Sync. pulse width
		|(PANEL_HEIGHT-1 ),			//Lines-per-frame
		
		//VerticalTiming2
		(V_BACK 	-1	 ),			//Vertical back porch

		//Polarity
		 (0			<<  8)			//Panel clock divisor = (DivNo + 1)
		|(0			<<  3)			//The invert output enable
		|(0		    <<  2)			//Select the edge of the panel clock
		|(0			<<  1)			//The invert horizontal sync bit
		|(0				 ),			//The invert vertical sync bit
		
		//SerialPanelPixel
		 (LCD_FALSE	<<  5)			//AUO052 mode
		|(LCD_FALSE	<<  4)			//Left shift rotate
		|(0	        <<  2)			//Color sequence of odd line
		|(LCD_FALSE	<<  1)			//Delta type arrangement color filter
		|(0     		 ),			//RGB serial output mode
		
		//CCIR656
		 (LCD_FALSE	<<  2)			//TVE clock phase
		|(LCD_FALSE	<<  1)			//720 pixels per line
		|(1		 		 ),			//NTSC/PAL select
		
		//CSTNPanelControl
		 (LCD_FALSE	<< 27)			//CSTN function enable bit
		|(LCD_FALSE	<< 25)			//Bus width of CSTN panel
		|(LCD_FALSE	<< 24)			//CSTN panel type
		|(LCD_FALSE	<< 16) 			//Virtual window width in framebuffer
		|(LCD_FALSE		 ),			//Horizontal dot's resolution of CSTN panel
		
		//CSTNPanelParam1
		 (LCD_FALSE	<< 16)			//CSTN horizontal back porch
		|(LCD_FALSE	<<  7) 			//Type of the signal, "AC", inverts
		|(LCD_FALSE		 ),			//Line numbers that "AC" inverts once
		
		PANEL_WIDTH,
		PANEL_HEIGHT,
		60,							// frame rate
		TVOut_Init,					// before
		SAA7121_NTSC_Init			// after
  	},
