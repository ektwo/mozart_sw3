
  	// --------------------------------------------------------------------
  	//	TPO TD070WGEC2 parallel
  	// --------------------------------------------------------------------
  	#undef PANEL_WIDTH
    #undef PANEL_HEIGHT
    #define PANEL_WIDTH             800
    #define PANEL_HEIGHT    		480
  	{ // Descriptor
        "800x480 TPO TD070WGEC2 parallel",
    
        //LCDEnable
         (LCD_FALSE	<< 11)			//Test pattern generator, not implement now
        |(LCD_FALSE	<<  1)			//LCD screen on/off control
        |(LCD_FALSE		 ),			//LCD controller enable control
        
        //PanelPixel
         (LCD_FALSE	<< 15)			//LC_CLK domain reset
        |(LCD_FALSE	<< 14)			//HCLK domain reset
        |(0			<< 11)			//TFT panel color depth selection, 0 -> 6-bit per channel
		|(1			<<  4),			//RGB or BGR format selection, 0 -> RGB
        
		//HorizontalTiming
		 ((212	-1) << 24)	 		//Horizontal back porch
		|((44	-1) << 16)			//Horizontal front porch
		|((4	-1) <<  8)			//Horizontal Sync. pulse width
		|((PANEL_WIDTH >> 4)-   1),			//pixels-per-line = 16(PPL+1)
    
		//VerticalTiming1
		 (10		<< 24)			//Vertical front porch
		|((2	-1)	<< 16) 			//Vertical Sync. pulse width
		|(PANEL_HEIGHT-1 ),			//Lines-per-frame
		
		//VerticalTiming2
		(0x22 	-1		 ),			//Vertical back porch
		
		//Polarity
	 (7		    <<  8)			//Panel clock divisor = (DivNo + 1)
		|(LCD_FALSE	<<  3)			//The invert output enable
		|(0		    <<  2)			//Select the edge of the panel clock
		|(LCD_TRUE	<<  1)			//The invert horizontal sync bit
		|(LCD_TRUE		 ),			//The invert vertical sync bit
		
		//SerialPanelPixel
		 (LCD_FALSE	<<  5)			//AUO052 mode
		|(LCD_FALSE	<<  4)			//Left shift rotate
		|(2	        <<  2)			//Color sequence of odd line
		|(LCD_FALSE	<<  1)			//Delta type arrangement color filter
		|(0     		 ),			//RGB serial output mode
		
		//CCIR656
		 (LCD_FALSE	<<  2)			//TVE clock phase
		|(LCD_FALSE	<<  1)			//720 pixels per line
		|(LCD_FALSE		 ),			//NTSC/PAL select
		
		//CSTNPanelControl
		 (LCD_FALSE	<< 27)			//CSTN function enable bit
		|(LCD_FALSE	<< 25)			//Bus width of CSTN panel
		|(LCD_FALSE	<< 24)			//CSTN panel type
		|(LCD_FALSE	<< 16) 			//Virtual window width in framebuffer
		|(LCD_FALSE		 ),			//Horizontal dot's resolution of CSTN panel
		
		//CSTNPanelParam1
		 (LCD_FALSE	<< 16)			//CSTN horizontal back porch
		|(LCD_FALSE	<<  7) 			//Type of the signal, "AC", inverts
		|(LCD_FALSE		 ),			//Line numbers that "AC" inverts once
		
		PANEL_WIDTH,
		PANEL_HEIGHT,
		60,							// frame rate
		LCD_Init,
		NULL
  	},
