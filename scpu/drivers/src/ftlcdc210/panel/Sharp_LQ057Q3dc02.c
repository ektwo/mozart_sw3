  	// --------------------------------------------------------------------
  	//	Sharp Sharp LQ057Q3DC02
  	// --------------------------------------------------------------------
  	#undef PANEL_WIDTH
    #undef PANEL_HEIGHT
    #define PANEL_WIDTH             320
    #define PANEL_HEIGHT    		240
  	{ // Descriptor
    	"320x240 Sharp LQ057Q3DC02 TFT",

    	//LCDEnable
     	(LCD_FALSE	<< 11)			//Test pattern generator, not implement now
    	|(LCD_FALSE	<<  1)			//LCD screen on/off control
    	|(LCD_FALSE		 ),			//LCD controller enable control

    	//PanelPixel
     	(LCD_FALSE	<< 15)			//LC_CLK domain reset
    	|(LCD_FALSE	<< 14)			//HCLK domain reset
    	|(0			<< 11)			//TFT panel color depth selection, 6-bit per channel
		|(1 		<<  4),			//RGB or BGR format selection

		//HorizontalTiming
	 	((0x11	-1) << 24)	 		//Horizontal back porch
		|((0x11	-1) << 16)			//Horizontal front porch
		|((0x11	-1) <<  8)			//Horizontal Sync. pulse width
		|((PANEL_WIDTH >> 4)   -1),			//pixels-per-line = 16(PL+1)

		//VerticalTiming1
	 	(0xF		<< 24)			//Vertical front porch
		|((1	-1)	<< 16) 			//Vertical Sync. pulse width
		|(PANEL_HEIGHT-1 ),			//Lines-per-frame

		//VerticalTiming2
		(7/*7*/ 	-1		 	),			//Vertical back porch

		//Polarity
	 	(3			<<  8)			//Panel clock divisor = (DivNo + 1), DivNo >= 3 --> does not FIFO under run
		|(LCD_FALSE	<<  3)			//The invert output enable
		|(LCD_FALSE	<<  2)			//Select the edge of the panel clock
		|(LCD_TRUE	<<  1)			//The invert horizontal sync bit
		|(LCD_FALSE		 ),			//The invert vertical sync bit

		//SerialPanelPixel
		 (LCD_FALSE	<<  5)			//AUO052 mode
		|(LCD_FALSE	<<  4)			//Left shift rotate
		|(LCD_FALSE	<<  2)			//Color sequence of odd line
		|(LCD_FALSE	<<  1)			//Delta type arrangement color filter
		|(LCD_FALSE		 ),			//RGB serial output mode

		//CCIR656
	 	(LCD_FALSE	<<  2)			//TVE clock phase
		|(LCD_FALSE	<<  1)			//720 pixels per line
		|(LCD_FALSE		 ),			//NTSC/PAL select

		//CSTNPanelControl
	 	(LCD_FALSE	<< 27)			//CSTN function enable bit
		|(LCD_FALSE	<< 25)			//Bus width of CSTN panel
		|(LCD_FALSE	<< 24)			//CSTN panel type
		|(LCD_FALSE	<< 16) 			//Virtual window width in framebuffer
		|(LCD_FALSE		 ),			//Horizontal dot's resolution of CSTN panel

		//CSTNPanelParam1
	 	(LCD_FALSE	<< 16)			//CSTN horizontal back porch
		|(LCD_FALSE	<<  7) 			//Type of the signal, "AC", inverts
		|(LCD_FALSE		 ),			//Line numbers that "AC" inverts once

		PANEL_WIDTH,
		PANEL_HEIGHT,
		60,							// frame rate
		LCD_Init,
		NULL
  	},
