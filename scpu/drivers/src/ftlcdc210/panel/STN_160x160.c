  	// --------------------------------------------------------------------
  	//	160x160 STN-LCD
  	// --------------------------------------------------------------------
  	#undef PANEL_WIDTH
    #undef PANEL_HEIGHT
    #define PANEL_WIDTH             160
    #define PANEL_HEIGHT    		160
  	{ // Descriptor
        "160x160 STN",
    
        //LCDEnable
         (LCD_FALSE	<< 11)			//Test pattern generator, not implement now
        |(LCD_TRUE	<<  1)			//LCD screen on/off control
        |(LCD_TRUE		 ),			//LCD controller enable control
        
        //PanelPixel
         (LCD_FALSE	<< 15)			//LC_CLK domain reset
        |(LCD_FALSE	<< 14)			//HCLK domain reset
        |(LCD_FALSE	<< 11)			//TFT panel color depth selection
		|(LCD_FALSE	<<  4),			//RGB or BGR format selection
        
		//HorizontalTiming
		 ((6	-1) << 24)	 		//Horizontal back porch
		|((1	-1) << 16)			//Horizontal front porch
		|((2	-1) <<  8)			//Horizontal Sync. pulse width
		|((PANEL_WIDTH >>4)-1),		//pixels-per-line = 16(PL+1)
    
		//VerticalTiming1
		 (0			<< 24)			//Vertical front porch
		|((1	-1)	<< 16) 			//Vertical Sync. pulse width
		|(PANEL_HEIGHT-1 ),			//Lines-per-frame
		
		//VerticalTiming2
		(1 	-1		 ),				//Vertical back porch
		
		//Polarity
		 (5			<<  8)			//Panel clock divisor = (DivNo + 1)
		|(LCD_FALSE	<<  3)			//The invert output enable
		|(LCD_FALSE	<<  2)			//Select the edge of the panel clock
		|(LCD_FALSE	<<  1)			//The invert horizontal sync bit
		|(LCD_FALSE		 ),			//The invert vertical sync bit
		
		//SerialPanelPixel
		 (LCD_FALSE	<<  5)			//AUO052 mode
		|(LCD_FALSE	<<  4)			//Left shift rotate
		|(LCD_FALSE	<<  2)			//Color sequence of odd line
		|(LCD_FALSE	<<  1)			//Delta type arrangement color filter
		|(LCD_FALSE		 ),			//RGB serial output mode
		
		//CCIR656
		 (LCD_FALSE	<<  2)			//TVE clock phase
		|(LCD_FALSE	<<  1)			//720 pixels per line
		|(LCD_FALSE		 ),			//NTSC/PAL select
    
		//CSTNPanelControl
		 (LCD_TRUE	<< 27)			//CSTN function enable bit
		|(2			<< 25)			//Bus width of CSTN panel
		|(1			<< 24)			//CSTN panel type
		|((PANEL_WIDTH/16)<< 16) 			//Virtual window width in framebuffer
		|(PANEL_WIDTH-1  ),			//Horizontal dot's resolution of CSTN panel
		
		//CSTNPanelParam1
		 (1			<< 16)			//CSTN horizontal back porch
		|(LCD_FALSE	<<  7) 			//Type of the signal, "AC", inverts
		|(2*PANEL_HEIGHT+1),			//Line numbers that "AC" inverts once
    
		PANEL_WIDTH,
		PANEL_HEIGHT,
		60,							// frame rate
		LCD_Init,
		NULL
  	},
