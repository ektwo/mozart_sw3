#include "common_include.h"
#include "ftlcdc210/panel.h"

//#define fLib_printf printf
#define LCD_TRUE	1
#define LCD_FALSE   0

#ifndef NULL
	#define NULL		0
#endif

#define LCD_PLL 148500000//126000000  // HAPS PLL2_1

u32 LCD_Init(void)
{
//	pin_mux_enable(IP_LCD, 0);
    return LCD_PLL;
}



LCDMTYPE_T FLcdModule[] =
{    
	#include "./panel/VESA_800x600_60Hz.c"
	#include "./panel/VESA_1024x768_60Hz.c"
	#include "./panel/VESA_640x480_60Hz.c"
	#include "./panel/CEA_1280x720_60Hz.c"
	#include "./panel/CEA_1920x1080_60Hz.c"
    #include "./panel/VESA_864x491_60Hz.c"
	#include "./panel/MZT_480x272_60Hz.c"    
  	// --------------------------------------------------------------------
   	// 	Null
   	// --------------------------------------------------------------------
   	//  Descriptor, Timing1, Timing2, Timing2, Control,	Width, Height
     {NULL,		0,0,0,0,0,0,0,0,0,		 0,		  0,	   0, }
};
