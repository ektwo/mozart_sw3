#include <string.h>
#include "common_include.h"

#include "ftlcdc210/flcd.h"
#include "ftlcdc210/display.h"
//#define fLib_printf printf
#ifdef CONFIG_RSVD
	#define inline __inline
#endif

struct ModeUnit
{
	int x_unit;			/// when draw picture, use how many pixel as a unit
	int y_unit;			/// when draw picture, use how many row as a unit
	int bpp;			/// bit per pixel
	int num_pp;			/// one byte could put how many pixel ( only used in palette_4, 2, 1)
};

struct ModeUnit mode_unit[]= {
	{1, 1, 16},				// MODE_RGB565
	{1, 1, 16},				// MODE_RGB555
	{1, 1, 16},				// MODE_RGB444
	{1, 1, 32},				// MODE_RGB24
	{2, 1, 16},				// MODE_YCbCr422
	{2, 2, 16},				// MODE_YCbCr420
	{1, 1, 8},				// MODE_PALETTE_8
	{1, 1, 4, 2},			// MODE_PALETTE_4
	{1, 1, 2, 4},			// MODE_PALETTE_2
	{1, 1, 1, 8}			// MODE_PALETTE_1
};



void RGB_to_YCbCr(unsigned char r, unsigned char g, unsigned char b, unsigned char *y, unsigned char *cb, unsigned char *cr)
{
	*y= (unsigned char)(0.299   * r + 0.587  * g + 0.114  * b);        //Y =¡@0.299 R + 0.587¡@G¡@ + 0.114 B
	*cb= (unsigned char) (-0.1687 * r - 0.3313 * g + 0.5    * b   +128);  //Cb = - 0.1687R - 0.3313G  + 0.5¡@B + 128
	*cr= (unsigned char) (0.5     * r - 0.4187 * g - 0.0813 * b  + 128); //Cr = 0.5 R - 0.4187G - 0.0813 B + 128
}


unsigned int RGB_to_YCbCr422(unsigned char r, unsigned char g, unsigned char b)
{   
	unsigned int y;
	unsigned int cb;
	unsigned int cr;
	
	y =    0.299 * r +  0.587 * g +  0.114 * b;        			//Y =¡@0.299 R + 0.587¡@G¡@ + 0.114 B
	cb = -0.1687 * r - 0.3313 * g +    0.5 * b  + 128;  		//Cb = - 0.1687R - 0.3313G  + 0.5¡@B + 128
	cr = 0.5     * r - 0.4187 * g - 0.0813 * b  + 128; 			//Cr = 0.5 R - 0.4187G - 0.0813 B + 128
	
	return cb | (y<<8) | (cr<<16) | (y<<24);
	//return cr | (y<<8) | (cb<<16) | (y<<24);
}



static __inline void Draw_Pixel(struct faradayfb_info *fbi, int frame_no, int x, int y, 
								unsigned int color, /* or (y<<8)|y */ unsigned char cb, unsigned char cr)
{
	int idx;		// index in the word
	int pixel_pos; 
	unsigned int tmp_color;

	
	if (fbi->mode==MODE_RGB24)
	{
		*(unsigned int *)(fbi->pFrameBuffer[frame_no]+(y*fbi->frame_width[frame_no]+x)*4)=color;
	}
	else if (fbi->mode==MODE_YCbCr422)
	{
		// draw two pixel a time
		if (fbi->endian == BBBP)
		{
			tmp_color = ((color&0xffff)<<16)|((color>>16)&0xffff);
		}
		else
		{
			tmp_color = color;
		}

		*(unsigned int *)(fbi->pFrameBuffer[frame_no]+(y*fbi->frame_width[frame_no]+x)*2)=tmp_color;
	}
	else if (fbi->mode==MODE_RGB565)
	{
		idx = fbi->idx_16tbl[(int)(x&1)];

		*(unsigned short *)(fbi->pFrameBuffer[frame_no]+(y*fbi->frame_width[frame_no]+x)*2 + idx) = color;
	}
	else if (fbi->mode==MODE_YCbCr420)
	{
		idx = fbi->idx_16tbl[(int)((x/2)&1)];
		*(unsigned short *)(fbi->pFrameBuffer[0]+(y*fbi->frame_width[0]+x+idx))=color;
		*(unsigned short *)(fbi->pFrameBuffer[0]+((y+1)*fbi->frame_width[0]+x+idx))=color;

		pixel_pos = (y>>1)*fbi->frame_width[0]/2+(x>>1);
		idx = fbi->idx_8tbl[(int)(pixel_pos&3)];
		*(unsigned char *)(fbi->ubase + pixel_pos + idx)=cb;
		*(unsigned char *)(fbi->vbase + pixel_pos + idx)=cr;
	}
	else if (fbi->mode==MODE_PALETTE_8)
	{
		idx = fbi->idx_8tbl[(int)(x&3)];
		*(unsigned char *)(fbi->pFrameBuffer[frame_no]+(y*fbi->frame_width[frame_no]+x)+idx) = color;
	}
	else if (fbi->mode==MODE_PALETTE_4)
	{
		unsigned int *ptr;
		
		pixel_pos = (y*fbi->frame_width[frame_no]+x);
		idx = pixel_pos%8;
		ptr = (unsigned int *)(fbi->pFrameBuffer[frame_no] + pixel_pos/8*4);
		*ptr = (*ptr & ~fbi->mask_4tbl[idx]) | (color<<fbi->shift_4tbl[idx]);
	}
	else if (fbi->mode==MODE_PALETTE_2)
	{
		unsigned int *ptr;
		
		pixel_pos = (y*fbi->frame_width[frame_no]+x);
		idx = pixel_pos%16;
		ptr = (unsigned int *)(fbi->pFrameBuffer[frame_no] + pixel_pos/16*4);
		*ptr = (*ptr & ~fbi->mask_2tbl[idx]) | (color<<fbi->shift_2tbl[idx]);
	}
	else
	{		
	}
}


// return the palette index for the (r, g, b)
unsigned char get_paletteColor(struct faradayfb_info *fbi, unsigned char r, unsigned char g, unsigned char b)
{
	int i;
	unsigned int color;
	unsigned char r1;
	unsigned char g1;
	unsigned char b1;
	int next_entry;
	
	for (i=0; i<fbi->entry_num; ++i)
	{
		if (fbi->entry[i].r == r && fbi->entry[i].g == g && fbi->entry[i].b==b)
		{	
			return i;
		}
	}
	fbi->last_entry = (fbi->last_entry+1)%fbi->entry_num;		/// waiting to do: change to use &, not used %
	fbi->entry[fbi->last_entry].r = r;
	fbi->entry[fbi->last_entry].g = g;
	fbi->entry[fbi->last_entry].b = b;
	if (fbi->last_entry%2==0)
	{
		next_entry = fbi->last_entry+1;
		r1 = fbi->entry[next_entry].r;
		g1 = fbi->entry[next_entry].g;
		b1 = fbi->entry[next_entry].b;
		color = RGB565(r, g, b) | (RGB565(r1, g1, b1)<<16);
		install_palette(fbi, fbi->last_entry/2, color);		// install two entry a time
	}
	else
	{
		next_entry = fbi->last_entry-1;
		r1 = fbi->entry[next_entry].r;
		g1 = fbi->entry[next_entry].g;
		b1 = fbi->entry[next_entry].b;
		color = RGB565(r1, g1, b1) | (RGB565(r, g, b)<<16);
		install_palette(fbi, fbi->last_entry/2, color);		// install two entry a time
	}
	return fbi->last_entry;
}


// --------------------------------------------------------------------
//	pCb, pCr is only used when fbi->mode is MODE_YCbCr420
//	return
//		color (for YCbCr420 ==> (y<<8)|y
//			  (for YCbCr422 ==> cb | (y<<8) | (cr<<16) | (y<<24))
// --------------------------------------------------------------------
unsigned int getColor(struct faradayfb_info *fbi, unsigned char r, unsigned char g, unsigned char b, unsigned char *pCb, unsigned char *pCr)
{
	unsigned int color;
	unsigned char y;
	
	switch(fbi->mode)
	{
		case MODE_RGB24:
			color = RGB24(r, g, b);
			break;
			
		case MODE_RGB565:
			color = RGB565(r, g, b);
			break;

		case MODE_YCbCr422:
			color = RGB_to_YCbCr422(r, g, b);
			break;

		case MODE_YCbCr420:
			RGB_to_YCbCr(r, g, b, &y, pCb, pCr);
			color = (y<<8) | y;
			break;

		case MODE_PALETTE_8:
		case MODE_PALETTE_4:
		case MODE_PALETTE_2:
		case MODE_PALETTE_1:
			color = get_paletteColor(fbi, r, g, b);
			break;

		case MODE_RGB555:
		case MODE_RGB444:			
		default:
			break;
	}
	return color;
}


// --------------------------------------------------------------------
//	input:
//		frame_no ==> to clear which window
//					no use when fbi->mode == MODE_YCbCr420
// --------------------------------------------------------------------
void Display_Clear(struct faradayfb_info *fbi, int frame_no, unsigned char r, unsigned char g, unsigned char b)
{
	int i;
	unsigned int color;
	unsigned int tmp_color;
	unsigned char y, cb, cr;
	unsigned char *fb_y;				// y planet
	unsigned char *fb_u;				// cb planet
	unsigned char *fb_v;				// cr planet
	
	switch(fbi->mode)
	{
		case MODE_RGB24:		// nothing need to do, each pixel is 32 bit when mode is RGB24
			color = RGB24(r, g, b);
			break;
			
		case MODE_RGB565:
			color = RGB565(r, g, b);
			color = (color<<16) | color;
			break;
		
		case MODE_YCbCr422:
			tmp_color = RGB_to_YCbCr422(r, g, b);
			if (fbi->endian == BBBP)
			{
				color = ((tmp_color&0xffff)<<16)|((tmp_color>>16)&0xffff);
			}
			else
			{
				color = tmp_color;
			}
			break;

		case MODE_YCbCr420:
			RGB_to_YCbCr(r, g, b, &y, &cb, &cr);
			fb_y = (unsigned char *)fbi->pFrameBuffer[0];
			fb_u = (unsigned char *)fbi->ubase;
			fb_v = (unsigned char *)fbi->vbase;
			memset(fb_y, y, fbi->frame_width[0]*fbi->frame_height[0]);
			memset(fb_u, cb, fbi->frame_width[0]*fbi->frame_height[0]/4);
			memset(fb_v, cr, fbi->frame_width[0]*fbi->frame_height[0]/4);
			return;
		
		case MODE_PALETTE_8:
			color = get_paletteColor(fbi, r, g, b);
			color = (color<<24)|(color<<16)|(color<<8)|(color);		// draw 4 pixel a time
			break;
			
		case MODE_PALETTE_4:
			color = get_paletteColor(fbi, r, g, b);		// draw 8 pixel a time
			color = (color<<28)|(color<<24)|(color<<20)|(color<<16)|(color<<12)|(color<<8)|(color<<4)|color;
			break;
		
		case MODE_PALETTE_2:
			color = get_paletteColor(fbi, r, g, b);		// draw 16 pixel a time
			color = (color<<14)|(color<<12)|(color<<10)|(color<<8)|(color<<6)|(color<<4)|(color<<2)|color;
			color = (color<<16)|color;
			break;
		
		case MODE_PALETTE_1:
			color = get_paletteColor(fbi, r, g, b);		// draw 32 pixel a time			
			
		case MODE_RGB555:
		case MODE_RGB444:
		default:
			color = 0;
			
	}
	for (i=0; i<fbi->frame_size[frame_no]; i+=4)
	{
		*(unsigned int *)(fbi->pFrameBuffer[frame_no]+i) = color;
	}
}


// waiting to do: endian ==> draw according fbi->endian
void Display_ColorBar( struct faradayfb_info *fbi, int frame_no)
{
	int x_pos, y_pos;
	unsigned int color;
	unsigned char cb;
	unsigned char cr;
	int x_unit;
	int y_unit;
	
	x_unit = mode_unit[fbi->mode].x_unit;
	y_unit = mode_unit[fbi->mode].y_unit;

	for (y_pos=0; y_pos<fbi->frame_height[frame_no]; y_pos+=y_unit)
	{
		//fLib_printf("y_pos=%d\n", y_pos);
		// waiting to do: §令 Red_color, Red_cb, Red_cr, Green_color,...
		color = getColor(fbi, 255, 0, 0, &cb, &cr);
		for (x_pos=0; x_pos<fbi->frame_width[frame_no]/3; x_pos+=x_unit)
		{
			if (fbi->mode==MODE_YCbCr420)
			{
				//fLib_printf("\ry_pos=%d, x_pos=%d", y_pos, x_pos);
			}
			Draw_Pixel(fbi, frame_no, x_pos, y_pos, color, cb, cr);
		}
		
		color = getColor(fbi, 0, 255, 0, &cb, &cr);
		for (; x_pos<fbi->frame_width[frame_no]*2/3; x_pos+=x_unit)
		{
			if (fbi->mode==MODE_YCbCr420)
			{
				//fLib_printf("\ry_pos=%d, x_pos=%d", y_pos, x_pos);
			}
			Draw_Pixel(fbi, frame_no, x_pos, y_pos, color, cb, cr);
		}

		color = getColor(fbi, 0, 0, 255, &cb, &cr);
		for (; x_pos<fbi->frame_width[frame_no]; x_pos+=x_unit)
		{
			if (fbi->mode==MODE_YCbCr420)
			{
				//fLib_printf("\ry_pos=%d, x_pos=%d", y_pos, x_pos);
			}
			Draw_Pixel(fbi, frame_no, x_pos, y_pos, color, cb, cr);
		}		
	}
}


//Draw_Square can be used to test the boundary of the panel
void Display_drawSquare( struct faradayfb_info *fbi, int frame_no, unsigned int thickness, unsigned char r, unsigned char g, unsigned char b )
{
	int x_pos, y_pos;
	unsigned int color;
	unsigned char cb;
	unsigned char cr;
	int x_unit;
	int y_unit;
	
	x_unit = mode_unit[fbi->mode].x_unit;
	y_unit = mode_unit[fbi->mode].y_unit;
	color = getColor(fbi, r, g, b, &cb, &cr);

	for (y_pos=0; y_pos<fbi->frame_height[frame_no]; y_pos+=y_unit)
	{
		if ( (y_pos>=0 && y_pos<thickness) || (y_pos>=fbi->frame_height[frame_no]-thickness))			// top, bottom row		
		{
			for (x_pos=0; x_pos<fbi->frame_width[frame_no]; x_pos+=x_unit)
			{
				Draw_Pixel(fbi, frame_no, x_pos, y_pos, color, cb, cr);
			}
		}
		else
		{
		
			for (x_pos=0; x_pos<thickness; x_pos+=x_unit)
			{
				Draw_Pixel(fbi, frame_no, x_pos, y_pos, color, cb, cr);
			}
			
			for (x_pos=fbi->frame_width[frame_no]-thickness; x_pos<fbi->frame_width[frame_no]; x_pos+=x_unit)
			{
				Draw_Pixel(fbi, frame_no, x_pos, y_pos, color, cb, cr);
			}
			
		}
	}
}


void Display_DrawRect( struct faradayfb_info *fbi, int frame_no, unsigned int thickness,unsigned int xs,unsigned int ys,unsigned int xe,unsigned int ye ,unsigned char r, unsigned char g, unsigned char b , unsigned char fill)
{
	int x_pos, y_pos;
	unsigned int color;
	unsigned char cb;
	unsigned char cr;
	int x_unit;
	int y_unit;
	
	x_unit = mode_unit[fbi->mode].x_unit;
	y_unit = mode_unit[fbi->mode].y_unit;
	color = getColor(fbi, r, g, b, &cb, &cr);

	for (y_pos=ys; y_pos<ye; y_pos+=y_unit)
	{
		 
			for (x_pos=xs; x_pos<xe; x_pos+=x_unit)
			{
				Draw_Pixel(fbi, frame_no, x_pos, y_pos, color, cb, cr);
			}
	 	
	}
}

#if 0
char bmp_640x480[] = {
	#include "castle565_640x480.c"
};
//#else
char bmp_800x600[] = {
	#include "800x600.c"
};
#endif

#if 0
char bmp_1024x768[] = {
	#include "1024x768.c"
};
#else
char *bmp_640x480 = (void *)(0x60000000);
char *bmp_800x600 = (void *)(0x60000000);
char *bmp_1024x768 = (void *)(0x60000000);
#endif

//char cursor_32x32[] = {
//	#include "cursor.c"
//};


//char cat[] = {
//	#include "cat.c"
//};

// --------------------------------------------------------------------
//		load specified image to frame buffer
//	return
//		1 ==> success
//		0 ==> fail
// --------------------------------------------------------------------
int Load_Image(struct faradayfb_info *fbi, int frame_no, char *filename)
{
	unsigned int pFrameBuffer = fbi->pFrameBuffer[frame_no];
	int x_pos;
	int y_pos;
	int x_unit;
	int y_unit;
	unsigned short *ptr;
	unsigned int color;
	unsigned char cb;
	unsigned char cr;
	unsigned char r, g, b;
	unsigned short val;
//	int i;

	fLib_printf("%s\n",__func__);
	if ( strcmp(filename, "bmp_800x600")==0 )
	{
		fLib_printf("load 800x600\n");
		ptr = (unsigned short *)bmp_800x600;
	}
	else if (strcmp(filename, "bmp_640x480")==0 )
	{
		fLib_printf("load 640x480\n");
		ptr = (unsigned short *)bmp_640x480;
	}
	else if (strcmp(filename, "bmp_1024x768")==0 )
	{
		fLib_printf("load 1024x768\n");
		ptr = (unsigned short *)bmp_1024x768;
	}
	
	else
	{
		fLib_printf("load image , return 0? \n");
		return 0;
	}
	
	if (fbi->mode == MODE_RGB565)
	{
		fLib_printf("copy size of %x\n",fbi->frame_size[frame_no]);
		fLib_printf("memcpy: pFrameBuffer=0x%x, ptr=0x%x\n",pFrameBuffer,ptr);
		memcpy((void *)pFrameBuffer, ptr, fbi->frame_size[frame_no]);
		//while(1);
	}
	else
	{
	
		x_unit = mode_unit[fbi->mode].x_unit;
		y_unit = mode_unit[fbi->mode].y_unit;
		fLib_printf("load image: draw pixel\n");
		for (y_pos=0; y_pos<fbi->frame_height[frame_no]; y_pos+=y_unit)
		{
			for (x_pos=0; x_pos<fbi->frame_width[frame_no]; x_pos+=x_unit)
			{
				val = ptr[y_pos*fbi->frame_width[frame_no] + x_pos];
				r = (val>>11)<<3;
				g = ((val>>5)&0x3f)<<2;
				b = (val&0x1f)<<3;
				color = getColor(fbi, r, g, b, &cb, &cr);
				Draw_Pixel(fbi, frame_no, x_pos, y_pos, color, cb, cr);
			}
		}
	}
	return 1;
}
