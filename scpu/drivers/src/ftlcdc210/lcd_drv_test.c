#include "common_include.h"
#include "ftlcdc210/flcd.h"
#include "ftlcdc210/panel.h"
#include "ftlcdc210/display.h"
#include "DrvPWMTMR010.h"
#include "io.h"

//#define fLib_printf printf

struct faradayfb_info lcd_fbi;
//extern mdelay();
extern void Delay(int sec);
struct image_pattern
{
	char *filename;
	int image_width;
	int image_height;
};

// in LBLP's format
struct image_pattern patterns[] = {
	{"bmp_1024x768", 1024, 768},
	{"castle565_640x480", 640, 480},
	{NULL, 0, 0}		// filename=NULL
};

extern void delay_ms(int msec);
void mdelay(unsigned int t)
{
		//delay_ms(t);
	delay_ms(t);
}

void do_smalldelay(void)
{
	delay_ms(200);
}

void do_delay(void)
{
	//mdelay(22100);
	delay_ms(100);
}

char *mode_to_msg[] = {
	"MODE_RGB565",
	"MODE_RGB555",
	"MODE_RGB444",
	"MODE_RGB24",
	"MODE_YCbCr422",
	"MODE_YCbCr420",
	"MODE_PALETTE_8",
	"MODE_PALETTE_4",
	"MODE_PALETTE_2",
	"MODE_PALETTE_1"
};


// --------------------------------------------------------------------
//	test the specified frame's specified mode
// --------------------------------------------------------------------
void Basic_Test(struct faradayfb_info *fbi, int frame_num, char *comment )
{
   	int i, j, k;
	int endians[]={BBBP, LBBP, LBLP};


	fLib_printf("going to do test for mode: %s in %s\n", mode_to_msg[fbi->mode], comment);
	
	for (j=0; j<ARRAY_SIZE(endians); ++j)						// which endian
	{
		LCD_SetEndian(fbi, endians[j]);			// Display_Clear() 目前還沒有 support 其它 endian
		for (i=0; i<frame_num; ++i)
		{
			//Display_Clear(fbi, i, 255, 255, 255);		do_delay();
			//for (;;) ;
			Display_Clear(fbi, i, 255, 0, 0);		do_delay();
			Display_Clear(fbi, i, 0, 255, 0);		do_delay();
			Display_Clear(fbi, i, 0, 0, 255);		do_delay();
			Display_Clear(fbi, i, 0, 0, 0);			do_delay();
			Display_drawSquare(fbi, i, 4, 0, 255, 0);	do_delay();
		}
	}
	#if 0
	fLib_printf("ColorBar test in %s\n", comment);
	for (i=0; i<ARRAY_SIZE(endians); ++i)						// which endian
	{
		LCD_SetEndian(fbi, endians[i]);			// Display_Clear() 目前還沒有 support 其它 endian
		for (j=0; j<frame_num; ++j)					// which windows
		{
			ColorBar_Test(fbi, j);	do_delay();
		}
	}


	fLib_printf("ImageLoad test in %s\n", comment);
	for (i=0; patterns[i].filename != NULL; ++i)		// which pattern
	{
		fLib_printf("[1]\n");
		for (j=0; j<ARRAY_SIZE(endians); ++j)						// which endian
		{
			fLib_printf("[2]\n");
			LCD_SetEndian(fbi, endians[j]);

			for (k=0; k<frame_num; ++k)			// which framebuffer
			{
				fLib_printf("patterns[i].image_width %x ==fbi->frame_width[k] %x \n",patterns[i].image_width,fbi->frame_width[k]);
				if (patterns[i].image_width==fbi->frame_width[k] && 
							patterns[i].image_height==fbi->frame_height[k])
				{
					Load_Image(fbi, k, patterns[i].filename);
					do_delay();
				}
			}
		}
	}
#endif
}
void ColorBar_Test(struct faradayfb_info *fbi, int frame_no)
{
	Display_ColorBar(fbi, frame_no);
	//Basic_Test(fbi, 1, 0 );
}

void Do_Basic_Test(struct faradayfb_info *fbi, int mode )
{
	PiP_Off(fbi);
	PoP_Off(fbi);
	LCD_SetInputMode(fbi, mode);
	
	//Basic_Test(fbi, 0, "basic test" );

	fLib_printf("patterns[1].image_width = %d \n",patterns[1].image_width);
	fLib_printf("fbi->frame_width = %d \n",fbi->frame_width[0]);

	if (patterns[1].image_width==fbi->frame_width[0] && 
				patterns[1].image_height==fbi->frame_height[0])
	{
		fLib_printf("load image\n");
		Load_Image(fbi, 0, patterns[1].filename);
	}	
}


struct OSD_window
{
	int x;
	int y;
	int width;
	int height;
};


void Do_PiP_Test(struct faradayfb_info *fbi, int mode )
{
  int i;
	int pip_num=2;			// so pip_num=2
	int hstep1=1;
	int vstep1=1;
	int hstep2=1;
	int vstep2=1;	
  struct OSD_window osd_win1 = {150, 150, 8, 8};	
	struct OSD_window osd_win2 = {50, 50, 8, 8};	
	
	fbi->isPOP = 0;
	//fLib_printf("fbi->panel_width/2 %x fbi->panel_height/2 %x\n",fbi->panel_width/2,fbi->panel_height/2);
	LCD_SetInputMode(fbi, mode);

  for(i=0;i<pip_num;i++){
		PiP_Pos(fbi, i+1, 50+50*i, 50+50*i);	//HPos needs to be odd if YCbCr422 is used
		PiP_Dim(fbi, i+1, fbi->panel_width/4, fbi->panel_height/4);	
	}			
	
	PiP_On(fbi, pip_num);	
	PiP_Blending(fbi, 0, 0, 0);

	//Basic_Test(fbi, pip_num+1, "do pip test" );
	delay_ms(3000);
	
		while(1){	
  //while(fLib_getchar(DEBUG_CONSOLE)!='q'){			
			osd_win1.x += hstep1;
			osd_win1.y += vstep1;
			PiP_Pos(fbi, 1, osd_win1.x, osd_win1.y);	//HPos needs to be odd if YCbCr422 is used
			if((osd_win1.x +(fbi->panel_width/4)) > fbi->panel_width-1){
				hstep1 = -1;
			}
			if(osd_win1.y + (fbi->panel_height/4) > fbi->panel_height-1){
				vstep1 = -1;
			}
			if(osd_win1.x == 0){
				hstep1 = 1;
			}
			if(osd_win1.y == 0){
				vstep1 = 1;
			}	
			
			//group2
			osd_win2.x += hstep2;
			osd_win2.y += vstep2;
			PiP_Pos(fbi, 2, osd_win2.x, osd_win2.y);	//HPos needs to be odd if YCbCr422 is used
			if((osd_win2.x +(fbi->panel_width/4)) > fbi->panel_width-1){
				PiP_Blending(fbi, 0, 0x3, 0x1);
				hstep2 = -1;
			}
			if(osd_win2.y + (fbi->panel_height/4) > fbi->panel_height-1){
				PiP_Blending(fbi, 0, 0, 0x6);
				vstep2 = -1;
			}
			if(osd_win2.x == 0){
				PiP_Blending(fbi, 0, 0, 0x7);
				hstep2 = 1;
			}
			if(osd_win2.y == 0){
				PiP_Blending(fbi, 0, 6, 0x0);
				vstep2 = 1;
			}				
			//end of group2
			
			delay_ms(10);
			flcd_orl(fbi, 0x304, (1 << 28));	//PiP update
			
			
		}
	while(1);
	// --------------------------------------------------------------------
	//	other special test for PiP
	// --------------------------------------------------------------------

	PiP_Blending(fbi, 0, 0x3, 0xb);

	for(i=0; i<900000; i++);

	PiP_Blending(fbi, 1, 0, 0);
	for(i=0; i<300000; i++);
	
	PiP_Blending(fbi, 1, 0, 0xa);
	for(i=0; i<300000; i++);

	PiP_Blending(fbi, 1, 0, 0xf);
	for(i=0; i<300000;i++);
	
	//PiP_Blending(fbi, 1, 0xf, 0);
	//for(i=0; i<300000;i++);	
	//PiP_Off(fbi);
}

void Do_PoP_Test(struct faradayfb_info *fbi, int mode )
{
	int frame_num=4;		// pop has 4 window
	//int bpp;

	PoP_ImageScalDown(fbi, 1, 1, 1, 1);	//all four window are scaling down to 1/4
	PoP_On(fbi);
	LCD_SetInputMode(fbi, mode);
	
	Basic_Test(fbi, frame_num, "do PoP test" );
}

void enable_cursor(struct faradayfb_info *fbi)
{
	flcd_andl(fbi, 0 , 0xffffefff);
	flcd_orl(fbi, 0 , 1<<12);
}

void disable_cursor(struct faradayfb_info *fbi)
{
	flcd_andl(fbi, 0 , 0xffffefff);
}



void init_cursor_data(struct faradayfb_info *fbi)
{
	memset((void*)(fbi->io_base+ 0x1380), 0xff,64);
}

void hardware_curosr_position(struct faradayfb_info *fbi, int x,int y)
{
	int value;

	value = (1<<29) | (1<<28) | (x<<16) | (y);
	flcd_writel(fbi, 0x1200, value);
}

void Do_Hardware_Cursor_Test(struct faradayfb_info *fbi)
{
	int x_pos,y_pos;
	int x_step, y_step;

	x_step = y_step = 1;
	x_pos = y_pos = 0;
	init_cursor_data(fbi);
	enable_cursor(fbi);
		
	while(getchar()=='q'){
		hardware_curosr_position(fbi,  x_pos, y_pos);
		x_pos += x_step;
		y_pos += y_step;

		if(x_pos > fbi->panel_width)
			x_step = -1;
		if(x_pos < 0)
			x_step = 1;
		if(y_pos > fbi->panel_height)
			y_step = -1;
		if(y_pos < 0)
			y_step = 1;	
		do_smalldelay();
	}
	disable_cursor(fbi);
}



void lcd_pll_setting(void)
{
	int data;

		data = inw(SCU_EXTREG_PA_BASE + 0x14);
		outw(SCU_EXTREG_PA_BASE + 0x14, data & ~0x300); // pll5_out1/2 clock gated
		
		data = inw(SCU_EXTREG_PA_BASE + 0x3C);
		outw(SCU_EXTREG_PA_BASE + 0x3C, data & ~0x1); // pll5_out1/2 power down
		
		outw(SCU_EXTREG_PA_BASE + 0x3C, 0x0505415C); // pll5 M/N/P value

		data = inw(SCU_EXTREG_PA_BASE + 0x3C);
		outw(SCU_EXTREG_PA_BASE + 0x3C, data | 0x1); // pll5 enable
		
		do_delay();do_delay();do_delay();
		
		data = inw(SCU_EXTREG_PA_BASE + 0x14);
		outw(SCU_EXTREG_PA_BASE + 0x14, data | 0x300); // pll5_out1/2 clock gated

}
void lcd_pinmux(void)
{
	int i;
	
	for(i=0x17c;i<0x19c;i+=4){
		if( i == 0x194 || i==0x198) continue;
		outw(SCU_EXTREG_PA_BASE + i, 0x1);
	}
}

/*
#define VESA_800x600 0
#define VESA_1024x768 1
#define VESA_640x480 2
#define CEA_720p	3
#define CEA_1080p	4
*/

void FTLCDC210_Test_Main(void)
{
	//unsigned char *pFrameBuffer;
    struct faradayfb_info *fbi = &lcd_fbi;
    int irq[IP_IRQ_NUM];
    int val;
		int data;
	
	#ifdef VESA_640x480
		//PLL_UPDATE  (PLL5)
		lcd_pll_setting();
	#endif
	
		fLib_printf("LCD PINMUX EN, UART0 disabled.\n");	
		lcd_pinmux();
		delay_ms(1000);
#if 1
    irq[ClrVstatus_IRQ] = LCDC_FTLCDC210_VSTATUS_IRQ; 
    irq[ClrBaudP_IRQ] = LCDC_FTLCDC210_BAUPD_IRQ;
    irq[ClrFIFOUdn_IRQ] =LCDC_FTLCDC210_FUR_IRQ;
    irq[ClrMemErr_IRQ] = LCDC_FTLCDC210_MERR_IRQ;

    //val=Init_LCD(fbi, LCD_FTLCDC210_PA_BASE , irq, CEA_1080p, MODE_RGB565);
		val=Init_LCD(fbi, LCD_FTLCDC210_PA_BASE , irq, VESA_640x480, MODE_RGB565);
    if (val<0){
			 fLib_printf("LCD initial failed\n");	
			while(1);
    	//return 0;
    }
    fLib_printf("LCD initial ok\n");	
  //Display_DrawRect( fbi,0, 1,260,130,380,220 ,221,11,22,1);
		*(unsigned short *)(0x6001EA50) = 0x1F00;
delay_ms(3000);
		ColorBar_Test(fbi,0);
		//delay_ms(3000);
		//Do_Basic_Test(fbi,MODE_RGB565);
    
    //Do_PoP_Test(fbi, MODE_RGB565);		
		Do_PiP_Test(fbi, MODE_RGB565);
    //Do_OSD_Test( fbi );
    //Do_Simple_Scaler(fbi, MODE_RGB565);
		Do_Scalar_Test(fbi,MODE_RGB565);
    //Do_Hardware_Cursor_Test(fbi);
	#endif
	//return 0;
}
