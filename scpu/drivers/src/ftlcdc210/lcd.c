#include "common_include.h"
#include "ftlcdc210/flcd.h"
#include "ftlcdc210/panel.h"


#define CONFIG_FRAME_RATE		60
//#define fLib_printf printf


void disable_lcd_controller(struct faradayfb_info *fbi)
{
	/*
	if (fbi->panel_id >= SITRONIX_CSTN && fbi->panel_id<=SITRONIX_STN_mono)		//for STN/CSTN panel
	{
		flcd_andl(fbi, REG_GPIO, 0xfffffeff);				//GPIO, for disable the power
	}
	*/
    flcd_andl(fbi, REG_FuncEnable, ~0x1);
}


void enable_lcd_controller(struct faradayfb_info *fbi)
{
	///flcd_orl(fbi, REG_FuncEnable, flcd_readl(fbi, REG_FuncEnable)|0x1);
	flcd_orl(fbi, REG_FuncEnable, 0x1);
	/*
	if (fbi->panel_id >= SITRONIX_CSTN && fbi->panel_id<=SITRONIX_STN_mono)		//for STN/CSTN panel
	{
		flcd_orl(fbi, REG_GPIO, 0x00000100);
	}
	*/
}

void LCD_ScreenOn(struct faradayfb_info *fbi)
{
	flcd_orl(fbi, REG_FuncEnable, 0x2);
}

void LCD_ScreenOff(struct faradayfb_info *fbi)
{
	flcd_andl(fbi, REG_FuncEnable, ~0x2);
}


void CSTN_On(struct faradayfb_info *fbi)
{
	int i;

	flcd_orl(fbi, 0x50, 0x00000100);
	flcd_writel(fbi, 0x54, 0x0000000F);					//bypass rgb<->ycbcr and other, or some data will lost
	flcd_orl(fbi, 0x0C00, ( 1 << 27 ));
	flcd_orl(fbi, 0x0C08, ( 1 << 6 ));					//reverse the sequence of the default value(the STN pattern)

	for( i=0; i<3; i++ )
	{
		flcd_writel(fbi, 0x0E00 + i*(0x40), 0x0);
		flcd_writel(fbi, 0x0E04 + i*(0x40), 0x1);
		flcd_writel(fbi, 0x0E08 + i*(0x40), 0x3);
		flcd_writel(fbi, 0x0E0C + i*(0x40), 0x5);
		flcd_writel(fbi, 0x0E10 + i*(0x40), 0x7);
		flcd_writel(fbi, 0x0E14 + i*(0x40), 0x9);
		flcd_writel(fbi, 0x0E18 + i*(0x40), 0xb);
		flcd_writel(fbi, 0x0E1C + i*(0x40), 0xd);
		flcd_writel(fbi, 0x0E20 + i*(0x40), 0x11);
		flcd_writel(fbi, 0x0E24 + i*(0x40), 0x13);
		flcd_writel(fbi, 0x0E28 + i*(0x40), 0x14);
		flcd_writel(fbi, 0x0E2C + i*(0x40), 0x17);
		flcd_writel(fbi, 0x0E30 + i*(0x40), 0x18);
		flcd_writel(fbi, 0x0E34 + i*(0x40), 0x1a);
		flcd_writel(fbi, 0x0E38 + i*(0x40), 0x1c);
		flcd_writel(fbi, 0x0E3C + i*(0x40), 0x1F);
	}
}


void CSTN_Off(struct faradayfb_info *fbi)
{
	flcd_andl(fbi, 0x0050, 0xfffffeff);			//GPIO, for disable the power
	flcd_andl(fbi, 0x0C00, ~( 1 << 27 ));
}


void STN_On(struct faradayfb_info *fbi)
{
	int i;
	unsigned int pattern[] = {0x0, 0x1, 0x3, 0x5, 0x7, 0x9, 
						     0xb, 0xd, 0x11, 0x13, 0x14, 0x17,
						     0x18, 0x1a, 0x1c, 0x1F};

	flcd_writel(fbi, 0x50, 0x00000200);
	flcd_writel(fbi, 0x54, 0x0000000F);	//bypass rgb<->ycbcr and other, or some data will lost
	flcd_orl(fbi, 0x0C00, ( 1 << 27 ));
	flcd_andl(fbi, 0x0C08, 0xffffffbf);	//ensure the pattern is for STN

   	//choose the grayscale level
	for (i=0; i<16; ++i)
	{
		flcd_writel(fbi, 0x0E00+i*4, pattern[i]);
	}
}


void STN_Off(struct faradayfb_info *fbi)
{
	CSTN_Off(fbi);
}
	

void STN_160x80_On(struct faradayfb_info *fbi)
{
	flcd_writel(fbi, 0x50, 0x00000600);	//GPIO output bit 1 & 2 set to 1
	flcd_writel(fbi, 0x54, 0x0000000F);	//bypass rgb<->ycbcr and other, or some data will lost
	flcd_orl(fbi, 0x0C00, ( 1 << 27 ));
	flcd_andl(fbi, 0x0C08, 0xffffffbf);	//ensure the pattern is for STN

	flcd_writel(fbi, 0x0E00, 0x0);
	flcd_writel(fbi, 0x0E04, 0x1F);
}


/********************* Dithering *************************/
/* type 00: 888 to 565, 01: 888 to 555, 02: 888 to 444 */
// --------------------------------------------------------------------
//	input:
//		type: DitherType_565, DitherType_555, DitherType_444
// --------------------------------------------------------------------
void Dithering(struct faradayfb_info *fbi, int enable, unsigned int type )
{
	flcd_andl(fbi, REG_PixelParam, ~DitherType_Mask);
	flcd_andl(fbi, REG_FuncEnable, ~DitherEn);			//disable dithering
	
	if( enable != 0 )
	{
		flcd_orl(fbi, REG_FuncEnable, DitherEn);		// enable dithering
		flcd_orl(fbi, REG_PixelParam, type);
	}
}

// --------------------------------------------------------------------
//	LCD panel clock divisor control. The actual divisor value is equal to (DivNo+1)
//	return ==> original DivNo
// --------------------------------------------------------------------
int SetDivNo(struct faradayfb_info *fbi, unsigned int DivNo)
{
	int org_DivNo;
	
	if( DivNo >= 64 )
	{
		fLib_printf("wrong divisor \n");
	}
	org_DivNo = (flcd_readl(fbi, 0x10C)>>8)&0x7f;
	flcd_andl(fbi, 0x10C, 0xffffc0ff);
	flcd_orl(fbi, 0x10C, DivNo << 8);
	
	return org_DivNo;
}


int SetFrameBase(struct faradayfb_info *fbi, unsigned short image, unsigned short frame, unsigned int pFrameBuffer )
{	
	if( image>3 || frame>2 )	//value of image = 0,1,2,3 and value of frame = 0,1,2
	{
		fLib_printf("Frame base set error\n");
		return -1;
	}
	
	//assert((pFrameBuffer&0x3)==0);
	fLib_printf("write %x to addr %x\n",pFrameBuffer, 0x18 + (image * 3 + frame) * 0x4);
	flcd_writel(fbi, 0x18 + (image * 3 + frame) * 0x4, pFrameBuffer);
	return 0;
}


#define FRAME_SIZE_RGB(xres,yres,mbpp)     ((xres) * (yres) * (mbpp) / 8)
#define FRAME_SIZE_YUV422(xres,yres,mbpp)  (((xres) * (yres) * (mbpp) / 8) * 2)
#define FRAME_SIZE_YUV420(xres,yres,mbpp)  (((((xres) * (yres) * (mbpp) / 8) + 0xffff) & 0xffff0000) * 3 / 2)


int calc_frameSize(int frame_width, int frame_height, int mode)
{
	switch(mode)
	{
   		case MODE_YCbCr422:
			return FRAME_SIZE_YUV422(frame_width, frame_height, 16);
		
		case MODE_YCbCr420:
        	return FRAME_SIZE_YUV420(frame_width, frame_height, 8);
        	
		case MODE_RGB24:
			return FRAME_SIZE_RGB(frame_width, frame_height, 32);	/// one pixel is 32 bit, not 24 bit, in RGB24
			
    	case MODE_RGB565:
    	case MODE_RGB555:
    	case MODE_RGB444:
    		return FRAME_SIZE_RGB(frame_width, frame_height, 16);

		case MODE_PALETTE_8:
			return FRAME_SIZE_RGB(frame_width, frame_height, 8);
		
		case MODE_PALETTE_4:
			return FRAME_SIZE_RGB(frame_width, frame_height, 4);
			
		case MODE_PALETTE_2:
			return FRAME_SIZE_RGB(frame_width, frame_height, 2);
			
		case MODE_PALETTE_1:
			return FRAME_SIZE_RGB(frame_width, frame_height, 1);

		default:
			//assert(0);
			break;
	}
	return 0;
}


void decide_frameNum(struct faradayfb_info *fbi)
{
	if (fbi->mode==MODE_RGB565 || fbi->mode==MODE_RGB555 || 
			fbi->mode==MODE_RGB444 || fbi->mode==MODE_RGB24 || fbi->mode==MODE_YCbCr422)
	{
		if (fbi->isPOP)
		{
			fbi->frame_num = 4;
		}
		else if (fbi->pip_num>0)
		{
			fbi->frame_num = fbi->pip_num+1;
		}
		else
		{
			fbi->frame_num = 1;
		}
	}
	else
	{
		fbi->frame_num = 1;
	}
}


// --------------------------------------------------------------------
//	decide frame dimention (width/height)
// --------------------------------------------------------------------
void decide_frameDim(struct faradayfb_info *fbi)
{
	int i;

	for (i=0; i<fbi->frame_num; ++i)
	{
		// 1. scalar
		if (fbi->scalar_width!=0 && fbi->scalar_height!=0)
		{
			fbi->frame_width[i] = fbi->scalar_width;
			fbi->frame_height[i] = fbi->scalar_height;
		}
		else
		{
			fbi->frame_width[i] = fbi->panel_width;
			fbi->frame_height[i] = fbi->panel_height;
		}
		// 2. pip or pip (pip & pop will not used at the same time)
		if (fbi->frame_num>1)		// currently is PIP on or POP on
		{
			if (fbi->isPOP)
			{
				switch(fbi->ImScalDown[i])
    			{
    				case 0:
    					fbi->frame_width[i]=fbi->frame_width[i]/2;
    					fbi->frame_height[i]=fbi->frame_height[i]/2;
    					break;
    			
    				case 1:
    					fbi->frame_width[i]=fbi->frame_width[i];
    					fbi->frame_height[i]=fbi->frame_height[i];
    					break;
    			
    				case 2:
    					fbi->frame_width[i]=fbi->frame_width[i];
    					fbi->frame_height[i]=fbi->frame_height[i]/2;
    					break;
    			}
			}
			else			/// is pip
			{
				if (i>0)
				{
					fbi->frame_width[i] = fbi->pip_width[i-1]*(float)fbi->frame_width[i]/(float)fbi->panel_width;
					//fbi->frame_width[i] = fbi->pip_width[i];		// currently not sure is fbi->pip_width[i], or upper line is correct
					fbi->frame_height[i] = fbi->pip_height[i-1]*(float)fbi->frame_height[i]/(float)fbi->panel_height;
					//fbi->frame_height[i] = fbi->pip_height[i];
				}
			}
		}
	}
}

void alloc_frameBuffer(struct faradayfb_info *fbi)
{
	int i;
	int color[]={0x00ffff,0x00008888,0x00004444,0x00002222};
	decide_frameNum(fbi);
	decide_frameDim(fbi);

    for (i=0; i<fbi->frame_num; ++i)
    {
    	fbi->frame_size[i] = calc_frameSize(fbi->frame_width[i], fbi->frame_height[i], fbi->mode);
			
    	if (fbi->frameBuf_size[i]<fbi->frame_size[i])
    	{
    		if (fbi->pFrameBuffer[i] != 0)
    		{
    			//free((void *)fbi->pFrameBuffer[i]);
    			fbi->pFrameBuffer[i] = 0;
    		}
    	}
    	if (fbi->pFrameBuffer[i] == 0)
    	{
    		//fbi->pFrameBuffer[i] = (unsigned int)malloc(fbi->frame_size[i]);
    		switch(i){
			case 0: 
				fbi->pFrameBuffer[i] = LCD_FRAMEBUFFER_BASE;	
				break;
			case 1:
				fbi->pFrameBuffer[i] = LCD_FRAMEBUFFER_BASE1;	
				break;
			case 2:
				fbi->pFrameBuffer[i] = LCD_FRAMEBUFFER_BASE2;	
				break;
			case 3:
				fbi->pFrameBuffer[i] = LCD_FRAMEBUFFER_BASE3;	
				break;
    		}
    		if (fbi->pFrameBuffer[i]==0)
    		{
    			//assert(fbi->pFrameBuffer[i] != 0);
    			fLib_printf("alloc frame buffer fail , size %x, we got %x\n", fbi->frame_size[i], fbi->pFrameBuffer[i]);
    			for (;;) ;
    		}
    		fbi->frameBuf_size[i] = fbi->frame_size[i];
    	}
			fLib_printf("%s allocate pFramebuffer %d @ %x\n",__func__,i,fbi->pFrameBuffer[i]);
			if(fbi->frameBuf_size[i] == 0){
				fbi->frameBuf_size[i] = fbi->panel_height*fbi->panel_width*2;
			}
			memset((void*)(fbi->pFrameBuffer[i]), color[i], fbi->frameBuf_size[i]);
			fLib_printf("memset ok\n");
			SetFrameBase(fbi, i, 0, fbi->pFrameBuffer[i]);
	}
}

// color ==> RGB565's value
void install_palette(struct faradayfb_info *fbi, int entry, unsigned int color)
{
	unsigned int *palette_ram = (unsigned int *)(fbi->io_base + PALETTE_RAM);
	
	palette_ram[entry] = color;
}

// --------------------------------------------------------------------
//	input:
//		mode
//			MODE_RGB565, MODE_RGB555,  ...
// --------------------------------------------------------------------
void LCD_SetInputMode(struct faradayfb_info *fbi, int mode)
{
	unsigned int pix_param;
	int i;

  fbi->mode = mode;
	alloc_frameBuffer(fbi);

	pix_param = flcd_readl(fbi, REG_PixelParam);
	fbi->entry_num = 0;
   	if (mode == MODE_YCbCr422)
    {
		flcd_writel(fbi, 0, (flcd_readl(fbi, 0)|EnYCbCr)&~EnYCbCr420);
    }
   	else if (mode == MODE_YCbCr420)
    {
        flcd_orl(fbi, 0, EnYCbCr|EnYCbCr420);
    	fbi->ubase = fbi->pFrameBuffer[0] + fbi->frame_width[0]*fbi->frame_height[0];
    	fbi->vbase = fbi->ubase + fbi->frame_width[0]*fbi->frame_height[0]/4;
    	SetFrameBase(fbi, 1, 0, fbi->ubase);
		SetFrameBase(fbi, 2, 0, fbi->vbase);
    }
    else if (mode == MODE_RGB24)
    {
    	flcd_andl(fbi, 0, ~(EnYCbCr|EnYCbCr420));
    	pix_param = (pix_param&~BppFifo_Mask)|BppFifo_24bpp;
    }
    else if (mode == MODE_RGB565)
    {
    	flcd_andl(fbi, 0, ~(EnYCbCr|EnYCbCr420));
    	pix_param = (pix_param&~(BppFifo_Mask|RGBTYPE_Mask))|BppFifo_16bpp|RGBTYPE_RGB565;
    }
    else if (mode == MODE_RGB555)
    {
    	flcd_andl(fbi, 0, ~(EnYCbCr|EnYCbCr420));
    	pix_param = (pix_param&~(BppFifo_Mask|RGBTYPE_Mask))|BppFifo_16bpp|RGBTYPE_RGB555;
    }
    else if (mode == MODE_RGB444)
    {
    	flcd_andl(fbi, 0, ~(EnYCbCr|EnYCbCr420));
    	pix_param = (pix_param&~(BppFifo_Mask|RGBTYPE_Mask))|BppFifo_16bpp|RGBTYPE_RGB444;
    }
    else if (mode == MODE_PALETTE_8)
    {
    	flcd_andl(fbi, 0, ~(EnYCbCr|EnYCbCr420));
    	pix_param = (pix_param&~BppFifo_Mask)|BppFifo_8bpp;
    	fbi->entry_num = 256;
    }
    else if (mode == MODE_PALETTE_4)
    {
    	flcd_andl(fbi, 0, ~(EnYCbCr|EnYCbCr420));
    	pix_param = (pix_param&~BppFifo_Mask)|BppFifo_4bpp;
    	fbi->entry_num = 16;
    }
    else if (mode == MODE_PALETTE_2)
    {
    	flcd_andl(fbi, 0, ~(EnYCbCr|EnYCbCr420));
    	pix_param = (pix_param&~BppFifo_Mask)|BppFifo_2bpp;
    	fbi->entry_num = 4;
    }
    else if (mode == MODE_PALETTE_1)
    {
    	flcd_andl(fbi, 0, ~(EnYCbCr|EnYCbCr420));
    	pix_param = (pix_param&(~BppFifo_Mask))|BppFifo_2bpp;
    	fbi->entry_num = 2;
    }
    flcd_writel(fbi, REG_PixelParam, pix_param);
	
	for (i=0; i<fbi->entry_num; ++i)
	{
		fbi->entry[i].r = 0;
		fbi->entry[i].g = 0;
		fbi->entry[i].b = 0;
		install_palette(fbi, i, 0);		// palette all set to RGB565(0, 0, 0)
	}
}


static int LBLP_idx_16tbl[] = {0, 0};
static int LBLP_idx_8tbl[] = {0, 0, 0, 0};
static int LBLP_mask_4tbl[] = {0xf, 0xf0, 0xf00, 0xf000, 0xf0000, 0xf00000, 0xf000000, 0xf0000000};
static int LBLP_shift_4tbl[] = {0, 4, 8, 12, 16, 20, 24, 28};
static int LBLP_mask_2tbl[] = {0x3, 0xc, 0x30, 0xc0, 0x300, 0xc00, 0x3000, 0xc000, 
							   0x30000, 0xc0000, 0x300000, 0xc00000, 0x3000000, 0xc000000, 0x30000000, 0xc0000000};
static int LBLP_shift_2tbl[] = {0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30};


static int LBBP_idx_16tbl[] = {0, 0};
static int LBBP_idx_8tbl[] = {0, 0, 0, 0};
static int LBBP_mask_4tbl[] = {0xf0, 0xf, 0xf000, 0xf00, 0xf00000, 0xf0000, 0xf0000000, 0xf000000};
static int LBBP_shift_4tbl[] = {4, 0, 12, 8, 20, 16, 28, 24};
static int LBBP_mask_2tbl[] = {0xc0, 0x30, 0xc, 0x3, 0xc000, 0x3000, 0xc00, 0x300,
					0xc00000, 0x300000, 0xc0000, 0x30000, 0xc0000000, 0x30000000, 0xc000000, 0x3000000};
static int LBBP_shift_2tbl[] = {6, 4, 2, 0, 14, 12, 10, 8, 22, 20, 18, 16, 30, 28, 26, 24};

static int BBBP_idx_16tbl[] = {2, -2};
static int BBBP_idx_8tbl[] = {3, 1, -1, -3};
static int BBBP_mask_4tbl[] = {0xf0000000, 0xf000000, 0xf00000, 0xf0000, 0xf000, 0xf00, 0xf0, 0xf};
static int BBBP_shift_4tbl[] = {28, 24, 20, 16, 12, 8, 4, 0};

static int BBBP_mask_2tbl[] = {0xc0000000, 0x30000000, 0xc000000, 0x3000000, 0xc00000, 0x300000, 
							   0xc0000, 0x30000, 0xc000, 0x3000, 0xc00, 0x300, 0xc0, 0x30, 0xc, 0x3};
static int BBBP_shift_2tbl[] = {30, 28, 26, 24, 22, 20, 18, 16, 14, 12, 10, 8, 6, 4, 2, 0};


void LCD_SetEndian(struct faradayfb_info *fbi, unsigned char endian )
{
	flcd_andl(fbi, 0x04, 0xFFFFFF9F);
   
	switch( endian )
	{
		case LBLP:
			memcpy(fbi->idx_16tbl, LBLP_idx_16tbl, sizeof(LBLP_idx_16tbl));
			memcpy(fbi->idx_8tbl, LBLP_idx_8tbl, sizeof(LBLP_idx_8tbl));
			memcpy(fbi->mask_4tbl, LBLP_mask_4tbl, sizeof(LBLP_mask_4tbl));
			memcpy(fbi->shift_4tbl, LBLP_shift_4tbl, sizeof(LBLP_shift_4tbl));
			memcpy(fbi->mask_2tbl, LBLP_mask_2tbl, sizeof(LBLP_mask_2tbl));
			memcpy(fbi->shift_2tbl, LBLP_shift_2tbl, sizeof(LBLP_shift_2tbl));
			break;
			
		case BBBP:
			memcpy(fbi->idx_16tbl, BBBP_idx_16tbl, sizeof(LBBP_idx_16tbl));
			memcpy(fbi->idx_8tbl, BBBP_idx_8tbl, sizeof(LBBP_idx_8tbl));
			memcpy(fbi->mask_4tbl, BBBP_mask_4tbl, sizeof(BBBP_mask_4tbl));
			memcpy(fbi->shift_4tbl, BBBP_shift_4tbl, sizeof(BBBP_shift_4tbl));
			memcpy(fbi->mask_2tbl, BBBP_mask_2tbl, sizeof(BBBP_mask_2tbl));
			memcpy(fbi->shift_2tbl, BBBP_shift_2tbl, sizeof(BBBP_shift_2tbl));
			flcd_orl(fbi, 0x04, 0x00000020);
			break;
			
		case LBBP:
			memcpy(fbi->idx_16tbl, LBBP_idx_16tbl, sizeof(LBBP_idx_16tbl));
			memcpy(fbi->idx_8tbl, LBBP_idx_8tbl, sizeof(LBBP_idx_8tbl));
			memcpy(fbi->mask_4tbl, LBBP_mask_4tbl, sizeof(LBBP_mask_4tbl));
			memcpy(fbi->shift_4tbl, LBBP_shift_4tbl, sizeof(LBBP_shift_4tbl));
			memcpy(fbi->mask_2tbl, LBBP_mask_2tbl, sizeof(LBBP_mask_2tbl));
			memcpy(fbi->shift_2tbl, LBBP_shift_2tbl, sizeof(LBBP_shift_2tbl));
			flcd_orl(fbi, 0x04, 0x00000040);
			break;
	}
	fbi->endian = endian;
}


/*fill the Red, Green, and Blue Gamma Correction lookup table*/
void Color_FillGammaTable(struct faradayfb_info *fbi, unsigned int item, int r, int g, int b )
{
	if( item < 64 )
	{
		flcd_writel(fbi, 0x600 + item*(0x04), r);
		flcd_writel(fbi, 0x700 + item*(0x04), g);
		flcd_writel(fbi, 0x800 + item*(0x04), b);
	}
	else
	{
		fLib_printf("Gamma lookup table item error\n");
	}
}


void Set_FrameRate(struct faradayfb_info *fbi, int frame_rate)
{
	struct LcdMTypeTag *lcd_param = fbi->lcd_param;
	u32 pixel_clock;
	int serial_mode;
	int upper_margin;
	int lower_margin;
	int vsync_len;
	int left_margin;
	int right_margin;
	int hsync_len;
	int div;

	serial_mode = lcd_param->SerialPanelPixel&0x1;
	upper_margin = lcd_param->VerticalTiming2 + 1;
	lower_margin = (lcd_param->VerticalTiming1>>24)&0xff;
	vsync_len = ((lcd_param->VerticalTiming1>>16)&0x3f) + 1;
	left_margin = ((lcd_param->HorizontalTiming>>24)&0xff) + 1;
	right_margin = ((lcd_param->HorizontalTiming>>16)&0xff) + 1;
	hsync_len = ((lcd_param->HorizontalTiming>>8)&0xff) + 1;
	
	pixel_clock = (serial_mode ? 3 : 1 ) * frame_rate *
		(lcd_param->Height + upper_margin + lower_margin + vsync_len) *
		(lcd_param->Width + left_margin + right_margin + hsync_len);
		
	fLib_printf("upper_margin=%d, lower_margin=%d, vsync_len = %d\n", upper_margin, lower_margin,vsync_len);
	fLib_printf("left_margin=%d, right_margin=%d, hsync_len = %d\n", left_margin, right_margin,hsync_len);
	
	div = fbi->input_clock/pixel_clock ;
	fLib_printf("div=0x%x, fbi->input_clock=%d, pclk = %d\n", div-1, fbi->input_clock,pixel_clock);
	if (div<1)
	{
		fLib_printf("invalid input clock for LCD\n");
		//for (;;) ;
	}
  if((fbi->panel_height == 1080) && (fbi->panel_width == 1920))
		div = 0;
	else if ((fbi->panel_height == 480) && (fbi->panel_width == 640))
		div = 1;
	else
	  div = 5;

	SetDivNo(fbi, div);
}

void init_rgb_value(struct faradayfb_info *fbi, int mode )
{
	int i , color;
	int rgb[] ={0x600,0x700,0x800};

	for(color = 0; color<3;color++){
		for(i=0;i<0x800;i++)
		{
			writeb(i, fbi->io_base+rgb[color]+i);
		}
	}
}

// --------------------------------------------------------------------
//	input:
//		panel_id:
//      	-1 ==> chose panel bye interactive
//          other ==> fixed panel choice
//  return
//		-1 ==> failure
//      other ==> panel id
// --------------------------------------------------------------------
int Init_LCD(struct faradayfb_info *fbi, unsigned int io_base, int irq[IP_IRQ_NUM], int panel_idx, int mode)
{  
	int i;
	struct LcdMTypeTag *lcd_param;

	flcd_writel(fbi, 0x0000, 0);			// disable lcd
	flcd_writel(fbi, 0x0048, 0);
	memset(fbi, 0, sizeof(struct faradayfb_info));

        if (panel_idx==-1)
        {
            fLib_printf("\rexit testing...\n");
            return -1;
        }
        if (panel_idx<-1)
        {
            fLib_printf("incorrect panel choice\n");
            return -2;
        }
        lcd_param = fbi->lcd_param = &FLcdModule[panel_idx];
        //assert(lcd_param->before_func != NULL);
	
   	fbi->input_clock = lcd_param->before_func();

	fbi->io_base = io_base;
	for (i=0; i<IP_IRQ_NUM; ++i)
	{
		fbi->irq[i] = irq[i];
	}
	fbi->frame_width[0] = fbi->panel_width = lcd_param->Width;
	fbi->frame_height[0] = fbi->panel_height = lcd_param->Height;
	
fLib_printf("\rwidth=%d,height=%d\n",fbi->frame_width[0],fbi->frame_height[0]);	
	flcd_writel(fbi, 0x0000, lcd_param->LCDEnable);

	flcd_writel(fbi, 0x0004, lcd_param->PanelPixel);
	flcd_writel(fbi, 0x0100, lcd_param->HorizontalTiming);
	flcd_writel(fbi, 0x0104, lcd_param->VerticalTiming1);
	flcd_writel(fbi, 0x0108, lcd_param->VerticalTiming2);
	flcd_writel(fbi, 0x010C, lcd_param->Polarity);
	flcd_writel(fbi, 0x0200, lcd_param->SerialPanelPixel);
	flcd_writel(fbi, 0x0204, lcd_param->CCIR656);
	flcd_writel(fbi, 0x0C00, lcd_param->CSTNPanelControl);
	flcd_writel(fbi, 0x0C04, lcd_param->CSTNPanelParam1);
	Set_FrameRate(fbi, lcd_param->FrameRate);	
	flcd_writel(fbi, 0x0000, lcd_param->LCDEnable);

	//LCD Image Color Management
	flcd_writel(fbi, 0x400, 0x2000);
	flcd_writel(fbi, 0x404, 0x2000);
	flcd_writel(fbi, 0x408, 0x0);
	flcd_writel(fbi, 0x40C, 0x40000);
	
	//FrameBuffer Parameter
	flcd_writel(fbi, 0x0014, 0x0);

	//Ignore some transformation
	flcd_writel(fbi, 0x0050, 0x0);

	//reset the scalar control register	
	///*(volatile unsigned int *) (LCD_Base + 0x1110 ) &= 0x0;
	flcd_writel(fbi, 0x1110, 0x0);


	//fLib_printf("In Init_LCD function, FIE3420_LCDC_BASE = %x\n", LCD_Base);
	flcd_writel(fbi, 0x000C, 0x0000001f);	//interrupt status clear
	flcd_writel(fbi, 0x0008, 0x0000001f);	//enable interrupt
	
	flcd_writel(fbi, 0x0318, 0x00004444);	//bpp  kay

	flcd_writel(fbi, 0x004c, 0x04040404);		// set FIFO thread

	LCD_SetInputMode(fbi, mode); 

	LCD_SetEndian(fbi, LBLP);

	#ifdef GAMMA_ENABLE
		//fill the straight line x-y=0 to the contrast and gamma lookup table
		for( i=0; i<64; i++ )
   		{
			int val;
	  
			val = 0x03020100 + 0x04040404 * i;
			Color_FillGammaTable(fbi, i, val, val, val );	
		}
	#endif
	
	
	enable_lcd_controller(fbi);
		
//	*(volatile unsigned int *) (LCD_FTLCDC210_PA_BASE + 0 ) |= ( 1 << 16) ;
//	*(volatile unsigned int *) (LCD_FTLCDC210_PA_BASE + 4 ) |= ( 1 << 16) ;
	LCD_ScreenOn(fbi);

	if (lcd_param->after_func != NULL)
    {
    	lcd_param->after_func();
    }

#ifdef not_complete_yet
	//suppose we will use dithering for all the test, except AUO serial panel 3.6", CSTN, need modify further
	if( ( panel_id != 1 ) && ( panel_id != 9 ) && ( panel_id != 10 ) && ( panel_id != 11 ) )
	{
		Dithering(fbi, 1, 0 );
	}
	else if ( panel_id == 9 )
	{
		Dithering(fbi, 0, 0);	//CSTN 16 gray-color uses 4bit for each color, CSTN cannot use dither, bus width = 8
   		CSTN_On(fbi);
	}
	else if ( panel_id == 10 )
	{
		Dithering(fbi, 0, 0);	//STN 16 gray-color uses 4bit for each color, bus width = 4
   		STN_On(fbi);
	}
	else if ( panel_id == 11 )
	{
		Dithering(fbi, 0, 0);	//STN 16 gray-color uses 4bit for each color, bus width = 1
   		STN_160x80_On(fbi);
	}
#endif /* end_of_not */

	if(fbi->mode == MODE_RGB565)
		init_rgb_value(fbi,MODE_RGB565); 

	/* Set Pattern Generator: 
	0x0: Vertical Color Bar   0x1: Horizontal color bar   0x2: Single Color   */
#ifdef LCD_PATGEN	
	flcd_writel(fbi, 0x048, 0x0); 
	flcd_writel(fbi, 0x000, (flcd_readl(fbi, 0x2) | 0x4000)); // Enable PatGen Function
	fLib_printf("\rPatGen Set\n");
#endif
	return panel_idx;
}



void SetYCbCr(struct faradayfb_info *fbi, int type )
{
	flcd_andl(fbi, 0, 0xfffffff3);
	if (type == 422)
	{
		flcd_orl(fbi, 0, 0x00000008);
	}
	else if( type == 420)
	{
		flcd_orl(fbi, 0, 0x0000000C);
	}
}


void SetBGRSW( struct faradayfb_info *fbi, unsigned char choice )
{
	flcd_andl(fbi, 0x04, 0xffffffef);
	
	if( choice == BGR )
	{
		flcd_orl(fbi, 0x04, 0x00000010);
	}
}



// --------------------------------------------------------------------
//	input:
//		pipNum: 0 -> off, 1 -> single PiP window, 2 -> double PiP window
//
//	return 
//		0 ==> fail
//		1 ==> success
// --------------------------------------------------------------------
int PiP_On(struct faradayfb_info *fbi, unsigned int pipNum)
{
	int i;
	
	//assert(pipNum<=3);
	//assert(fbi->mode==MODE_RGB565 || fbi->mode==MODE_RGB555 || fbi->mode==MODE_RGB444 || 
	//				fbi->mode==MODE_RGB24 || fbi->mode==MODE_YCbCr422);
	for (i=0; i<pipNum; ++i)
	{
		if (fbi->pip_width[i]==0 || fbi->pip_height[i]==0)
		{
			fLib_printf("need set sub-pic:%d dimension first\n", i);
			return 0;
		}
	}

    flcd_andl(fbi, 0, 0xfffff9ff);	//PiP off
    flcd_orl(fbi, 0, (pipNum << 10));	//PiP on
    fbi->pip_num = pipNum;
		fLib_printf("pip num %d \n", fbi->pip_num);
    fbi->frame_num = pipNum+1;//kay
    alloc_frameBuffer(fbi);
    return 1;
}


void PiP_Off(struct faradayfb_info *fbi)
{
	flcd_andl(fbi, 0, 0xfffff9ff);
}


//HPos needs to be odd if YCbCr422 is used
void PiP_Pos(struct faradayfb_info *fbi, unsigned int which, int  HPos, int VPos)
{
	unsigned int offset[]={0, 0x304, 0x30c};		// offset[0] is not used
	//assert(which==1 || which==2);
	
	flcd_andl(fbi, offset[which], 0xf800f800);
   	flcd_orl(fbi, offset[which], VPos | (HPos << 16 | (1<<28)));
 
}



/*make sure that blend1 + blend2 < 16*/
void PiP_Blending(struct faradayfb_info *fbi, unsigned int on, int blend1, int blend2)
{	
	//assert(blend1+blend2<=16);

	if (on)
	{
    	flcd_orl(fbi, 0, (on << 8));
    	
    	flcd_andl(fbi, 0x300, 0xffffe0e0);
    	flcd_orl(fbi, 0x300, blend1 | (blend2 << 8));
	}
	else
	{
		flcd_andl(fbi, 0, 0xfffffeff);
	}
}


// --------------------------------------------------------------------
//	set sub-picture 's width/height
// --------------------------------------------------------------------
void PiP_Dim(struct faradayfb_info *fbi, unsigned int which, int HDim, int VDim)
{
	unsigned int offset[]={0, 0x308, 0x310};		// offset[0] is not used
	//assert(which==1 || which==2);
	//assert(HDim<=fbi->panel_width && VDim<=fbi->panel_height);

	fbi->pip_width[which-1]=HDim;
	fbi->pip_height[which-1]=VDim;
	flcd_andl(fbi, offset[which], 0xf800f800);
   	flcd_orl(fbi, offset[which], VDim | (HDim << 16));
} 


void PoP_On(struct faradayfb_info *fbi)
{		
    flcd_orl(fbi, 0, 0x00000080);	//PoP on
    fbi->isPOP = 1;
    fbi->frame_num = 4;
    alloc_frameBuffer(fbi);
}


void PoP_Off(struct faradayfb_info *fbi)
{
	fbi->isPOP = 0;
	fbi->frame_width[0] = fbi->panel_width;
	fbi->frame_height[0] = fbi->panel_height;
    flcd_andl(fbi, 0, 0xffffff7f);
}

//Image scaling down can be used in not only PoP but also any other situation
void PoP_ImageScalDown(struct faradayfb_info *fbi, unsigned int im0, unsigned int im1, unsigned int im2, unsigned int im3 )
{
	if( im0 > 2 || im1 > 2 || im2 > 2 || im3 > 2 )
	{
		fLib_printf("PoP windows enable error\n");
	}
		
	flcd_andl(fbi, 0x0014, 0xffff00ff);
	flcd_orl(fbi, 0x0014, (im0 << 8) | (im1 << 10) | (im2 << 12) | (im3 << 14));
	fbi->ImScalDown[0] = im0;
	fbi->ImScalDown[1] = im1;
	fbi->ImScalDown[2] = im2;
	fbi->ImScalDown[3] = im3;
    alloc_frameBuffer(fbi);
}

