#include <stdio.h>
#include <stdlib.h>
#include "common_include.h"
//#define fLib_printf printf
#include "ftlcdc210/lcd_config.h"
#include "ftlcdc210/flcd.h"
#include "ftlcdc210/display.h"
#include "ftlcdc210/scalar.h"


void do_delay(void);

// --------------------------------------------------------------------
//	stage-2 related setting
// --------------------------------------------------------------------

// 	set stage 2 resolution parameters
// return
//		1 ==> accept this ratio
//		0 ==> don't accept this ratio
int Scalar_Stage2_Resolution(struct faradayfb_info *fbi, int ver_no_in, int hor_no_in)
{
	int ver_no_out = fbi->panel_width;
	int hor_no_out = fbi->panel_height;
	int Scal_ver_num;
	int Scal_hor_num;
	float ratio;
	
	// scaling ration need 1/2 x 1/2 ~ 2 x 2
	ratio = (float)ver_no_out/(float)ver_no_in;
	if (ratio>2.0 || ratio < 0.5)
	{
		fLib_printf("invalid ration: %f\n", ratio);
		return 0;
	}
	
	if (ver_no_in <= ver_no_out)		// up-scaling
	{
		//Scal_ver_num = (ver_no_in+1)/ver_no_out*256;
		Scal_ver_num = (ver_no_in+1)*256/ver_no_out;
	}
	else
	{
		Scal_ver_num = ((ver_no_in+1)%ver_no_out) * 256 / ver_no_out;
	}

	ratio = (float)hor_no_out/(float)hor_no_in;
	if (ratio>2.0 || ratio < 0.5)
	{
		fLib_printf("invalid ration: %f\n", ratio);
		return 0;
	}

	if (hor_no_in <= hor_no_out)		// up-scaling
	{
		//Scal_hor_num = (hor_no_in+1)/hor_no_out*256;
		Scal_hor_num = (hor_no_in+1)*256/hor_no_out;
	}
	else
	{
		Scal_hor_num = ((hor_no_in+1)%hor_no_out) * 256 / hor_no_out;
	}

	//User define 2nd-stage horizontal coefficient	
	flcd_writel(fbi, 0x112C, (Scal_hor_num<<8) | Scal_ver_num);
	return 1;
}


//2nd-stage related. 0: nearly bilinear mode, 1: threshold nearly bilinear mode, 2: most neighborhood mode, 3: reserved 
// --------------------------------------------------------------------
//	stage 2 want to chose which interpolation method
// --------------------------------------------------------------------
void Scalar_SetInterpMode(struct faradayfb_info *fbi, int mode )
{
	//assert(mode<=2);
	
	flcd_andl(fbi, 0x1110, 0xffffffe1);
	flcd_orl(fbi, 0x1110, (mode << 3) | (mode << 1));
}


// --------------------------------------------------------------------
//		define the upscaling thresholds, all threshold 0~511
//	(only used in Threshold Nearly Bilinear Interpolation Mode)
// --------------------------------------------------------------------
void Scalar_UPThreshold(struct faradayfb_info *fbi, unsigned int Hor_high_th, unsigned int Hor_low_th, unsigned int Ver_high_th, unsigned int Ver_low_th )
{
	if( Hor_high_th > 511 || Hor_low_th > 511 || Ver_high_th > 511 || Ver_low_th > 511 )
	{
		fLib_printf("Scalar upscaling threshold error\n");
		return;
		//exit(-1);
	}
		
	flcd_andl(fbi, 0x1114, 0xfffffe00);
	flcd_orl(fbi, 0x1114, Hor_high_th);	//The real value equal to Hor_high_th/256
		
	flcd_andl(fbi, 0x1118, 0xfffffe00);
	flcd_orl(fbi, 0x1118, Hor_low_th);	//The real value equal to Hor_low_th/256
	
	flcd_andl(fbi, 0x111C, 0xfffffe00);
	flcd_orl(fbi, 0x111C, Ver_high_th);	//The real value equal to Ver_high_th/256
		
	flcd_andl(fbi, 0x1120, 0xfffffe00);
	flcd_orl(fbi, 0x1120, Ver_low_th);	//The real value equal to Ver_low_th/256
}


// --------------------------------------------------------------------
//	input:
//		mode ==> stage 2's Hor/Ver interpolation mode
//	return
//		0 ==> scalar on fail: maybe scalar ratio is not support
//		1 ==> scalar on
// --------------------------------------------------------------------
int Scalar_On(struct faradayfb_info *fbi, int frame_width, int frame_height, int mode)
{
	int i;
	int resolution_minus[]={0, 1, 1, 1, 1, 1, 1, 1};
	int bypass_stage2=0;
	int out_frame_width;			// stage 1 output image resolution
	int out_frame_height;
	int ver_no_in;					// stage 2 input parameter
	int hor_no_in;
		
	Scalar_Off(fbi);

	if( frame_width > 2048 || frame_height > 2048)
	{
		fLib_printf("Scalar resolution error\n");
		return 0;
	}

	// --------------------------------------------------------------------
	//	1. set Scalar Input/Output Resolution
	// --------------------------------------------------------------------
	// set Resolution Register of Scalar Input
	flcd_writel(fbi, 0x1100, frame_width-1);	
	flcd_writel(fbi, 0x1104, frame_height-1);

	// set Resolution Register of Scalar Output
	flcd_writel(fbi, 0x1108, fbi->panel_width);
	flcd_writel(fbi, 0x110C, fbi->panel_height);

	// --------------------------------------------------------------------
	//	2. decide stage1 ratio
	// --------------------------------------------------------------------
	for (i=0; i<=7; ++i)		// scaling down ratio = 1<<i
	{
		out_frame_width = frame_width/(1<<i);			// output image resolution after stage 1
		out_frame_height = frame_height/(1<<i);
		ver_no_in = out_frame_width - resolution_minus[i];
		hor_no_in = out_frame_height - resolution_minus[i];
		if (out_frame_width < fbi->panel_width*2 && out_frame_height < fbi->panel_height*2)
		{
			break;
		}
	}
	if (out_frame_width == fbi->panel_width && out_frame_height == fbi->panel_height)	// don't need stage 2
	{
		bypass_stage2 = 1;
	}
	else
	{
		bypass_stage2 = 0;
	}
	flcd_orl(fbi, 0x1110, (i<<6) | (mode<<3) | (mode<<1) | bypass_stage2);
	if (bypass_stage2==0)		// need stage 2 scalar
	{
		if (Scalar_Stage2_Resolution(fbi, ver_no_in, hor_no_in)==0)
		{
			return 0;
		}
	}
	Scalar_SetInterpMode(fbi, mode);

	fbi->scalar_width=frame_width;
	fbi->scalar_height = frame_height;
   	//fbi->frame_width[0]=frame_width;
   	//fbi->frame_height[0]=frame_height;
    	alloc_frameBuffer(fbi);
    
	flcd_orl(fbi, REG_FuncEnable, ScalerEn);		// scalar on
	
	return 1;
}


void Scalar_Off(struct faradayfb_info *fbi)
{
	fbi->scalar_width = 0;
	fbi->scalar_height = 0;
	fbi->frame_width[0]=fbi->panel_width;
	fbi->frame_height[0]=fbi->panel_height;
	alloc_frameBuffer(fbi);
	
	flcd_andl(fbi, REG_FuncEnable, ~ScalerEn);
}

void Do_Simple_Scaler(struct faradayfb_info *fbi, int mode)
{
	int ret;

	LCD_SetInputMode(fbi, mode);
	Display_Clear(fbi, 0, 0, 0, 0);

	/*ret = Scalar_On(fbi, 800,600,Interpolation_Nearly);
	if(ret){
		Load_Image(fbi, 0,"bmp_800x600");
		do_delay();
	}
	Scalar_Off(fbi);
	*/
	ret = Scalar_On(fbi, 640,480,Interpolation_Nearly);
	if(ret){
		Load_Image(fbi, 0,"bmp_640x480");		
	}
}

extern char *mode_to_msg[];
void Do_Scalar_Test(struct faradayfb_info *fbi, int mode)
{	
	int i;
	//int in_width;			// stage 2's input width, height
	//int in_height;
	int frame_widths[]={256, 720, 640, 320, 160};
	int frame_heights[]={192, 480, 480, 240, 120};

	fLib_printf("going to do scalar test for mode: %s\n", mode_to_msg[fbi->mode]);
	LCD_SetInputMode(fbi, mode);

	Display_Clear(fbi, 0, 0, 0, 0);
	
	for (i=0; i<ARRAY_SIZE(frame_widths); ++i)
	{
		fLib_printf("doing scalar: (%dx%d) -> (%dx%d)\n", frame_widths[i], frame_heights[i], fbi->panel_width, fbi->panel_height);
		fLib_printf("with Interpolation_Nearly\n");
		if (Scalar_On(fbi, frame_widths[i], frame_heights[i], Interpolation_Nearly)==0)
		{
			fLib_printf("scalar on fail\n");
			continue;
		}
		Display_ColorBar(fbi, 0); do_delay(); do_delay();		// LBLP
		
		fLib_printf("with Interpolation_Threshold\n");
		Scalar_SetInterpMode(fbi, Interpolation_Threshold);
		delay_ms(1000);
		
		fLib_printf("with Interpolation_Most\n");
		Scalar_SetInterpMode(fbi, Interpolation_Most);
		delay_ms(100);
	}

	Scalar_Off(fbi);
}





#ifdef not_complete_yet
			flcd_writel(fbi, 0x0108, flcd_readl(fbi, 0x0108)+1);	//VBP+1, to fix RGB888 partial the upper line problem
			///Load_Image(fbi, filename, 1228800, pBuffer, LBLP, 24 );
			flcd_writel(fbi, 0x0108, flcd_readl(fbi, 0x0108)-1);	//VBP-1, reload the original VBP value
#endif /* end_of_not */
