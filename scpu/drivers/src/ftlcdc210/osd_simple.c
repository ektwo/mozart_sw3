#include "common_include.h"
#include "ftlcdc210/lcd_config.h"
#include "ftlcdc210/flcd.h"
#include "ftlcdc210/osd_simple.h"
#define fLib_printf printf
#define FONT_RAM			0x8000
#define DISPLAY_RAM			0xc000				/// display ram's position

#define FONT_HEIGHT					16			// font height is 16 pixel
#define FONT_WIDTH					12			

struct OSDFont			/// simple font
{
	char c;
	unsigned int data[16];
};

#include "simple_font.c"

int num_OSDFont=0;			/// currently has install how many complex font
int OSDFont_index[256];		/// the character is install at which font index
							/// example: character '1' 's response font index is OSDFont_index['1']

unsigned int font_ram_base;

int OSD_fontStart(struct faradayfb_info *fbi)
{
	num_OSDFont = 0;
	font_ram_base = fbi->io_base + FONT_RAM;
	
	return 0;
}

int OSD_fontInstall(struct faradayfb_info *fbi, char c, unsigned int *font)
{
	if (num_OSDFont + 1 > 256)
	{
		fLib_printf("OSD font full\n");
		return 0;			// full
	}
	memcpy((void *)font_ram_base, font, 16*4);
	OSDFont_index[(unsigned char)c] = num_OSDFont;
	num_OSDFont += 1;
	font_ram_base += 16*4;				// one simple font 16 word
	return 1;
}


void OSD_SetWindow(struct faradayfb_info *fbi, int XPos, int YPos, int width, int height)
{
	unsigned int value;
	//assert(width<0x40 && height<0x40);
	//assert(width * height < 512);		// total only 512 font space
	// --------------------------------------------------------------------
	//	set pos
	// --------------------------------------------------------------------
	flcd_writel(fbi, 0x2004, (XPos << 12) | YPos);
	
	// --------------------------------------------------------------------
	//	set dim
	// --------------------------------------------------------------------  
  	value = flcd_readl(fbi, 0x2000);
  	value &= ~0x0003ffff;	//clear the horizontal and vertical dimension of the OSD window
  	value |= ((1<<20)|(width << 12) | (height << 4));	//set horizontal and vertical dimension
  	flcd_writel(fbi, 0x2000, value);
}


void OSD_putc(struct faradayfb_info *fbi, char c, int pos, int foreground, int background )
{
	//assert(foreground<4 && background<4);
	//assert(pos<512);
	//fLib_printf("putc: write %x to %x\n",(OSDFont_index[c]<<4) | (foreground<<2) | (background<<0), DISPLAY_RAM+pos*4);
	flcd_writel(fbi, DISPLAY_RAM+pos*4, (OSDFont_index[(unsigned char)c]<<4) | (foreground<<2) | (background<<0));
}


void OSD_puts(struct faradayfb_info *fbi, char *str, int pos, int foreground, int background )
{
  	int i;
  
  	for(i=0; i< strlen(str); i++)
	{
		OSD_putc(fbi, *(str +i), pos+i, foreground, background );
    }
}


// --------------------------------------------------------------------
//	input:
//		HScal, VScal ==> FONT_NORMAL, FONT_ZOOMOUT, ...
// --------------------------------------------------------------------
void OSD_FontScal(struct faradayfb_info *fbi, int HScal, int VScal)	//define the horizontal and vertical up-scaling factor
{
  	unsigned int value;
  
  	value = flcd_readl(fbi, 0x2000);
  	value &= 0xfffffff0;
  	value |= (HScal<<2) | VScal;
  	flcd_writel(fbi, 0x2000, value);
}

void OSD_On(struct faradayfb_info *fbi)
{
	flcd_orl(fbi, 0, OSDEn);
}

void OSD_Off(struct faradayfb_info *fbi)
{
	flcd_andl(fbi, 0, ~OSDEn);
}

void OSDS_transparent(struct faradayfb_info *fbi, int level)
{
	flcd_andl(fbi, 0x200C, 0xfffffffc);		//set OSD background transparency = 25%
   	flcd_orl(fbi, 0x200C, level);			//bit 5-4 :set OSD background transparency, 00=25%, 01=50%, 10=75%, 11=100%
}


void OSDS_fg_color(struct faradayfb_info *fbi, int pal0, int pal1, int pal2, int pal3)	//OSD foreground color control
{
	flcd_writel(fbi, 0x2008, (pal0) | (pal1 <<8) | (pal2 << 16) | (pal3<< 24));	//palette entry 0~3
}


void OSDS_bg_color(struct faradayfb_info *fbi, int pal1, int pal2, int pal3)
{
  	unsigned int value;
  
  	value = flcd_readl(fbi, 0x200C);
  	value &= 0x000000ff;
  	value |= (pal1 <<8) | (pal2 << 16) | (pal3 << 24);
   	flcd_writel(fbi, 0x200C, value);
}

void OSD_Init(struct faradayfb_info *fbi)
{
	int i;

	// --------------------------------------------------------------------
	// 1. set palette
	// --------------------------------------------------------------------
	if (fbi->mode==MODE_YCbCr422 || fbi->mode==MODE_YCbCr420)   //YCbCr
   	{
    	OSDS_fg_color(fbi, 0x57, 0x88, 0x3B, 0xFF);
     	OSDS_bg_color(fbi, 0x57, 0x88, 0x3B);
//     	OSD_puts(fbi, "YCBCR OSD   ", 0);    
   	}
   	else
   	{
    	OSDS_fg_color(fbi, 0x07, 0x38, 0xC0, 0xFF);
    	OSDS_bg_color(fbi, 0x07, 0x38, 0xc0);
//    	OSD_puts(fbi, "RGB OSD ", 0);   
   	}
	OSDS_transparent(fbi, 1);
	
	// --------------------------------------------------------------------
	//	2. install font
	// --------------------------------------------------------------------
	OSD_FontScal(fbi, FONT_NORMAL, FONT_NORMAL);
	
	OSD_fontStart(fbi);	
	for (i=0; i<ARRAY_SIZE(OSDSimple_Font); ++i)
	{
		OSD_fontInstall(fbi, OSDSimple_Font[i].c, OSDSimple_Font[i].data);
	}
	OSD_On(fbi);
}


struct OSD_window
{
	int x;
	int y;
	int width;
	int height;
};

void OSD_putstr(struct faradayfb_info *fbi, char *str)
{
	int str_len, i;

	if(str == NULL) return;
	str_len = strlen(str);
	fLib_printf("strlen %d\n",str_len);
	for(i=0;i<str_len;i++){
		OSD_putc(fbi, str[i],i , 2,2 );
	}
}

void Do_OSD_Test(struct faradayfb_info *fbi)
{
 	char str[]="THIS IS TEST";
	struct OSD_window osd_win = {50, 50, 8, 8};
	int hstep=1;
	int vstep=1;
	
//	struct OSD_window osd_win = {50, 50, 16, 16};	
	OSD_Init(fbi);
	
	OSD_putstr(fbi, str);
	//OSD_SetWindow(fbi, osd_win.x, osd_win.y, strlen(str), 1);
	OSD_SetWindow(fbi, osd_win.x, osd_win.y, osd_win.width, osd_win.height);	
	/*
	for (pos=0; pos<512; ++pos)
	{
		OSD_putc(fbi, str[pos%16], pos, (pos/16)%4, pos%4 );
	}
	*/
	//OSD_putstr(fbi, "THIS IS TEST");
	while(1){	
  //while(fLib_getchar(DEBUG_CONSOLE)!='q'){			
		osd_win.x += hstep;
		osd_win.y += vstep;
		OSD_SetWindow(fbi, osd_win.x, osd_win.y, strlen(str), 1);
		if((osd_win.x +(strlen(str)*FONT_WIDTH)) > fbi->panel_width){
			hstep = -1;
		}
		if(osd_win.y > fbi->panel_height){
			vstep = -1;
		}
		if(osd_win.x == 0){
			hstep = 1;
		}
		if(osd_win.y == 0){
			vstep = 1;
		}	
		do_smalldelay();
	}
	
	OSD_Off(fbi);
}

