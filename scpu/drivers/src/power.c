#include "power.h"
#include "scu_reg.h"
#include "scu_extreg.h"


enum power_mgr_mode __power_mgr_mode = power_mgr_mode_rtc;
extern void delay_ms(unsigned int count);
static void _power_mgr_ops_fcs()
{
    masked_outw(SCU_REG_PWR_MOD, 
                SCU_REG_PWR_MOD_SELFR_CMD_OFF | //scu will NOT issue the self-refresh command to DDR/SDR (in FCS)
                SCU_REG_PWR_MOD_FCS_PLL_RSTn |  //Keep PLL active in FCS
                SCU_REG_PWR_MOD_FCS,            //enter FCS
    
                SCU_REG_PWR_MOD_SELFR_CMD_OFF | SCU_REG_PWR_MOD_FCS_PLL2_RSTn |
                SCU_REG_PWR_MOD_FCS_DLL_RSTn | SCU_REG_PWR_MOD_FCS_PLL_RSTn |
                SCU_REG_PWR_MOD_FCS | SCU_REG_PWR_MOD_BUS_SPEED);
}

void power_mgr_set_mode(enum power_mgr_mode mode) 
{
    if (mode != __power_mgr_mode) {
        switch (mode) {
        case power_mgr_mode_rtc:
            break;
        case power_mgr_mode_alwayson:
            power_mgr_set_domain(power_domain_default, TRUE);
            power_mgr_wait_domain_ready(power_domain_default);            
            break;
        case power_mgr_mode_full:
            break;
        case power_mgr_mode_retention:
            break;
        case power_mgr_mode_deep_retention:
            break;  
        default:;
        }        
    }
    
    delay_ms(1);    
}

void power_mgr_ops(enum power_mgr_ops ops)
{   
    switch (ops) {    
    case power_mgr_ops_fcs:
        _power_mgr_ops_fcs();
        break;
    case power_mgr_ops_change_bus_speed:
        break;
    case power_mgr_ops_pll_update:
        break;    
    case power_mgr_ops_sleeping:
        //stop cpu 
        //stop ddr
        //send self-refresh command to ddr
        //wait ack
        break;
    default:;    
    }
}

void power_mgr_set_domain(enum power_domain_type type, bool_t enable)
{
    u32 val;
    u32 mask = SCU_REG_PWR_CTRL_PWRUP_CTRL_MASK | SCU_REG_PWR_CTRL_PWRCTRL_UPDATE;
    switch (type) {
    case power_domain_default: val = SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_DEFAULT; break;
    case power_domain_npu: val = SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_NPU; break;         
    case power_domain_ddr: val = SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_DDRCK; break;
    case power_domain_all: val = SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_DEFAULT |
                                 SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_NPU |
                                 SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_DDRCK; break;
    default:
        val = SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_DEFAULT;
    }
  
    val |= (enable) ? (SCU_REG_PWR_CTRL_PWRCTRL_UPDATE) : (0);
    
    masked_outw(SCU_REG_PWR_CTRL, 
                val,
                mask);
    
    delay_ms(1);    
}

void power_mgr_wait_domain_ready(enum power_domain_type type) 
{
    u32 val;
    u32 vsssts = SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_DEFAULT;
    u32 miscpwr = SCU_EXTREG_MISC_PWR_RESET_RELEASE_DOMAIN_DEFAULT;

    switch (type) {
    case power_domain_default: 
        vsssts = SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_DEFAULT;
        miscpwr = SCU_EXTREG_MISC_PWR_RESET_RELEASE_DOMAIN_DEFAULT;
    break;
    case power_domain_npu: 
        vsssts = SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_NPU;
        miscpwr = SCU_EXTREG_MISC_PWR_RESET_RELEASE_DOMAIN_NPU;
    break;         
    case power_domain_ddr: 
        vsssts = SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_DDRCK; 
        miscpwr = SCU_EXTREG_MISC_PWR_RESET_RELEASE_DOMAIN_DDRCK; 
    break;
    case power_domain_all: 
        vsssts = SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_DEFAULT |
                 SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_NPU |
                 SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_DDRCK; 
        miscpwr = SCU_EXTREG_MISC_PWR_RESET_RELEASE_DOMAIN_DEFAULT |
                  SCU_EXTREG_MISC_PWR_RESET_RELEASE_DOMAIN_NPU |
                  SCU_EXTREG_MISC_PWR_RESET_RELEASE_DOMAIN_DDRCK;
    break;
    default:;
    }    
    
    do {
        val = inw(SCU_REG_PWR_VCCSTS);
    } while((val & SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_MASK) != vsssts);
    
    do {
        val = inw(SCU_EXTREG_MISC);
    } while((val & SCU_EXTREG_MISC_PWR_RESET_RELEASE_DOMAIN_MASK) != miscpwr);
}