#include "system.h"
#include <string.h>
#include "scu_reg.h"
#include "scu_extreg.h"
#include "power.h"
#include "clock.h"
#include "DrvPWMTMR010.h"
#include "dbg.h"

#if 1

u32 __sys_int_flag;

extern void delay_ms(int sec);

void system_isr(void)
{
    outw(SCU_REG_INT_STS, 0xffffffff); //clear sleep wakeup interrupt   

    __sys_int_flag = 0x1;
}

static void _reset_handler(void) 
{
    outw(SCU_REG_BTUP_STS, SCU_REG_BTUP_STS_PWRBTN_STS);  // register boot-up event is power button
    outw(SCU_REG_BTUP_CTRL, SCU_REG_BTUP_CTRL_PWRBTN_EN | // send power button output signal
                            SCU_REG_BTUP_CTRL_GPO_OUT);
    
    NVIC_SetVector((IRQn_Type)SYS_SYSTEM_IRQ, (u32)system_isr);
    
    NVIC_ClearPendingIRQ((IRQn_Type)SYS_SYSTEM_IRQ);
    NVIC_EnableIRQ((IRQn_Type)SYS_SYSTEM_IRQ);
    
    outw(SCU_REG_INT_STS, 0xffffffff); // clear all interrupt status
    
    //can't directly write to the PLL control pins, it needs to use the 
    //FCS or PLL_UPDATE command that contained he power-ode register
    clock_mgr_set_scuclkin(scuclkin_pll0div3, TRUE);

    power_mgr_ops(power_mgr_ops_fcs);

    __WFI();
    do{
    }while((__sys_int_flag)!= 0x1);

    clock_mgr_set_muxsel(CLOCK_MUXSEL_NCPU_TRACECLK_DEFAULT |
                         CLOCK_MUXSEL_SCPU_TRACECLK_SRC_PLL0DIV3 |
                         CLOCK_MUXSEL_CSIRX1_CLK_PLL5 |
                         CLOCK_MUXSEL_NPU_CLK_PLL0 | /* CLOCK_MUXSEL_NPU_CLK_PLL4 | */ 
                         CLOCK_MUXSEL_PLL4_FREF_PLL0DIV | 
                         CLOCK_MUXSEL_UART_0_IRDA_UCLK_UART);
    delay_ms(1);
}
#include "scu_extreg.h"
extern struct clock_node clock_node_pll2_out;
extern struct clock_node clock_node_pll4_out1;
extern struct clock_node clock_node_pll4_fref_pll0;
extern struct clock_node clock_node_pll5_out1;
extern struct clock_node clock_node_pll5_out2;
extern struct clock_node clock_node_csirx0_hs_csi;
extern struct clock_node clock_node_csirx0_hs_vc0;
extern struct clock_node clock_node_csirx0_lp;
void system_init(void)
{
    int val,i;
    struct clock_value clock_val;

    fLib_Timer_Init(DRVPWMTMR1, PWMTMR_1MSEC_PERIOD);
    
    clock_mgr_init();

    _reset_handler();

    power_mgr_set_mode(power_mgr_mode_alwayson);
    
    //PLL4 en - npu
    {
    #if 1        
        memset(&clock_val, 0, sizeof(clock_val));
        clock_val.u.enable = 1;
        //clock_mgr_open(&clock_node_pll4_out1, &clock_val);
        clock_mgr_open(&clock_node_pll4_fref_pll0, &clock_val);
    #else        
       SCU_EXTREG_CLK_EN0_SET_pll4_fref_pll0(1);
       delay_ms(1);
       SCU_EXTREG_PLL4_SETTING_SET_en(1);
       delay_ms(1);
       SCU_EXTREG_CLK_EN0_SET_pll4_out1(1);
       delay_ms(1);
    #endif
    }

    power_mgr_set_domain(power_domain_all, TRUE);  
    power_mgr_wait_domain_ready(power_domain_all);

    //PLL1 en - ncpu
    {
        #if 1
        memset(&clock_val, 0, sizeof(clock_val));
        clock_val.u.enable = 1;
        clock_mgr_open(&clock_node_pll1_out, &clock_val);
        #else        
        SCU_EXTREG_PLL1_SETTING_SET_en(1);
        delay_ms(1);
        SCU_EXTREG_CLK_EN0_SET_pll1_out(1);
        delay_ms(1);
        #endif
    }
    //PLL2 en - ddr3  
    {
        #if 1
        memset(&clock_val, 0, sizeof(clock_val));
        clock_val.u.enable = 1;
        clock_mgr_open(&clock_node_pll2_out, &clock_val);
        #else        
       SCU_EXTREG_PLL2_SETTING_SET_en(1); 
       delay_ms(1);
       SCU_EXTREG_CLK_EN0_SET_pll2_out(1); 
       delay_ms(1);
       #endif
    }
    // {    
    //     #if 0
    //     memset(&clock_val, 0, sizeof(clock_val));
    //     clock_val.u.enable = 1;
    //     clock_mgr_open(&clock_node_pll5_out1, &clock_val);
    //     clock_mgr_open(&clock_node_pll5_out2, &clock_val);
    //     #else
    //     SCU_EXTREG_PLL5_SETTING_SET_en(1);
    //     delay_ms(1);
    //     SCU_EXTREG_CLK_EN0_SET_pll5_out1(1);
    //     SCU_EXTREG_CLK_EN0_SET_pll5_out2(1);
    //     delay_ms(1);
    //     #endif
    // }
    delay_ms(10);

    {
        u32 clk;

        struct clock_value clock_val_hs;
        memset(&clock_val_hs, 0, sizeof(clock_val_hs));

            clock_val_hs.u.pll.ns = 283;//120;//141;
            clock_val_hs.u.pll.ms = 2;//1;
            clock_val_hs.u.pll.ps = 1;//2;//1;
            clock_val_hs.u.pll.div = 6;

        clk = clock_mgr_calculate_clockout(pll_3, clock_val_hs.u.pll.ms, clock_val_hs.u.pll.ns, clock_val_hs.u.pll.ps);

        clock_mgr_open(&clock_node_csirx0_hs_csi, &clock_val_hs);

            clock_val_hs.u.pll.ns = 283;//120;//141;
            clock_val_hs.u.pll.ms = 2;//1;
            clock_val_hs.u.pll.ps = 1;//2;//1;
            clock_val_hs.u.pll.div = 29;
        clock_mgr_open(&clock_node_csirx0_hs_vc0, &clock_val_hs);             
    }
    {
        struct clock_value clock_val_lp;
        memset(&clock_val_lp, 0, sizeof(clock_val_lp));      
        clock_val_lp.u.pll.ns = 283;
        clock_val_lp.u.pll.ms = 2;
        clock_val_lp.u.pll.ps = 1;
        clock_val_lp.u.pll.div = 0x04;

        clock_mgr_open(&clock_node_csirx0_lp, &clock_val_lp);
    }

    // For fcs function to gating pll4(from pll0)
    //delay_ms(5);
    //PLL4 disable - npu
    {
        SCU_EXTREG_CLK_EN0_SET_pll4_fref_pll0(0);
        delay_ms(1);
        SCU_EXTREG_PLL4_SETTING_SET_en(0);
        delay_ms(1);
        SCU_EXTREG_CLK_EN0_SET_pll4_out1(0);
        delay_ms(1);
    }	    

    {       
        NVIC_ClearPendingIRQ((IRQn_Type)SYS_SYSTEM_IRQ);
        NVIC_EnableIRQ((IRQn_Type)SYS_SYSTEM_IRQ);

        outw(SCU_REG_INT_STS, 0xffffffff);
        
        {
            //The procedure should accompany a FCS command to stop or enable PLL2
            SCU_REG_PLL2_CTRL_SET_PLL2EN(1);
            //The procedure should accompany a FCS command to stop or enable DLL
            SCU_REG_DLL_CTRL_SET_DLLEN(1);
            //can't directly write to the PLL control pins, it needs to use the 
            //FCS or PLL_UPDATE command that contained he power-ode register
            clock_mgr_set_scuclkin(scuclkin_pll0div3, FALSE);
            power_mgr_ops(power_mgr_ops_fcs);  
            __WFI();
            do{
            }while((__sys_int_flag)!= 0x1);
        }        
    }

    //npu 
        SCU_EXTREG_CLK_EN0_SET_pll4_out1(1);
        delay_ms(1);
        SCU_EXTREG_PLL4_SETTING_SET_en(1);
        delay_ms(1);
        SCU_EXTREG_CLK_EN0_SET_pll4_fref_pll0(1);

    {
        SCU_EXTREG_SWRST_SET_PD_NPU_resetn(1);
        delay_ms(1);
    }
}

void system_wakeup_ncpu(int boot_loader_flag)
{
    return;
    if (boot_loader_flag)
        delay_ms(200);
    
    clock_mgr_open(&clock_node_pll1_out, NULL);
    
    SCU_EXTREG_CLK_EN0_SET_pll4_fref_pll0(1);
    delay_ms(30);
    SCU_EXTREG_PLL4_SETTING_SET_en(1);
    delay_ms(30);
    SCU_EXTREG_CLK_EN0_SET_pll4_out1(1);
    
    SCU_EXTREG_CLK_EN0_SET_ncpu_fclk_src(1);
    SCU_EXTREG_CLK_EN1_SET_npu(1);
    SCU_EXTREG_SWRST_SET_NPU_resetn(1);

    SCU_EXTREG_CM4_NCPU_CTRL_SET_wakeup(1);
    //outw(SCU_EXTREG_PA_BASE + 0x14, (inw(SCU_EXTREG_PA_BASE + 0x14) | 0x00400000)); // NCPU fclk src en setting
    //outw(SCU_EXTREG_PA_BASE + 0x18, (inw(SCU_EXTREG_PA_BASE + 0x18) | 0x00800000)); // Enable npu clk_en
    ////outw(SCU_EXTREG_PA_BASE + 0x4c, (inw(SCU_EXTREG_PA_BASE + 0x4c) | 0x00800002)); // Enable PD_NPU_reset_n
    //outw(SCU_EXTREG_PA_BASE + 0x4c, (inw(SCU_EXTREG_PA_BASE + 0x4c) | 0x00800004)); // Enable NPU_reset_n
    //outw(SCU_EXTREG_PA_BASE + 0x68, (inw(SCU_EXTREG_PA_BASE + 0x68) | 0x00001000)); // wakeup NCPU
}
#endif
