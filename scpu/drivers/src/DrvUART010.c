#if 1
/***************************************************************************
*--------------------------------------------------------------------------*
* Name:serial.c                                                            *
* Description: serial library routine                                      *
* Author:                                                                  *
****************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include "io.h"
#include "base.h"
#include "DrvUART010.h"
#include "kneron_mozart.h"
#include "scu_extreg.h"


extern UINT32 fLib_CurrentT1Tick(void);

#ifdef MOZART_CHIP
UINT32 UART_PORT[5]={UART_FTUART010_0_PA_BASE, UART_FTUART010_1_PA_BASE, UART_FTUART010_1_1_PA_BASE, UART_FTUART010_1_2_PA_BASE, UART_FTUART010_1_3_PA_BASE};
#else
UINT32 UART_PORT[4]={UART_FTUART010_0_PA_BASE, UART_FTUART010_1_PA_BASE, UART_FTUART010_1_1_PA_BASE, UART_FTUART010_1_2_PA_BASE};
#endif

void fLib_SetSerialMode(DRVUART_PORT port_no, UINT32 mode)
{
    UINT32 mdr;

    mdr = inw(UART_PORT[port_no] + SERIAL_MDR);
    mdr &= ~SERIAL_MDR_MODE_SEL;
    outw(UART_PORT[port_no] + SERIAL_MDR, mdr | mode);
}


void fLib_EnableIRMode(DRVUART_PORT port_no, UINT32 TxEnable, UINT32 RxEnable)
{
    UINT32 acr;

    acr = inw(UART_PORT[port_no] + SERIAL_ACR);
    acr &= ~(SERIAL_ACR_TXENABLE | SERIAL_ACR_RXENABLE);
    if(TxEnable)
        acr |= SERIAL_ACR_TXENABLE;
    if(RxEnable)
        acr |= SERIAL_ACR_RXENABLE;
    outw(UART_PORT[port_no] + SERIAL_ACR, acr);
}

/*-----------------------------------------------------------------------------
  Function:        fLib_SerialInit

  Parameter:
                None
  Returns:
                   None
  Description:
                   Initialize UART0, 38400bps, 8N1.
 *-----------------------------------------------------------------------------*/
void fLib_SerialInit (DRVUART_PORT port_no, UINT32 baudrate, UINT32 parity,UINT32 num,UINT32 len, UINT32 interruptMode)
{
    UINT32 lcr;

    lcr = inw(UART_PORT[port_no] + SERIAL_LCR) & ~SERIAL_LCR_DLAB;
    // fLib_printf("uart fifo depth=%u\n", inw(UART_PORT[port_no] + SERIAL_FEATURE)  & 0xF);
    /* Set DLAB=1 */
    outw(UART_PORT[port_no] + SERIAL_LCR,SERIAL_LCR_DLAB);
    /* Set baud rate */
    outw(UART_PORT[port_no] + SERIAL_DLM, ((baudrate & 0xff00) >> 8)); //ycmo090930
    outw(UART_PORT[port_no] + SERIAL_DLL, (baudrate & 0xff));
    // outw(UART_PORT[port_no] + SERIAL_DLM, 0x0); //ycmo090930 0x0
    // outw(UART_PORT[port_no] + SERIAL_DLL, 0x10);  //legend: 0xD
    //while(1);
    //clear orignal parity setting
    lcr &= 0xc0;

    switch (parity)
    {
        case PARITY_NONE:
            //do nothing
            break;
        case PARITY_ODD:
            lcr|=SERIAL_LCR_ODD;
                break;
        case PARITY_EVEN:
            lcr|=SERIAL_LCR_EVEN;
            break;
        case PARITY_MARK:
            lcr|=(SERIAL_LCR_STICKPARITY|SERIAL_LCR_ODD);
            break;
        case PARITY_SPACE:
            lcr|=(SERIAL_LCR_STICKPARITY|SERIAL_LCR_EVEN);
            break;

        default:
            break;
    }

    if(num==2)
        lcr|=SERIAL_LCR_STOP;

    len-=5;

    lcr|=len;

    outw(UART_PORT[port_no]+SERIAL_LCR,lcr);
    if (1 == interruptMode)
        outw(UART_PORT[port_no] + SERIAL_FCR, SERIAL_FCR_FE);
}

void fLib_SetSerialLoopback(DRVUART_PORT port_no, UINT32 onoff)
{
    UINT32 temp;

    temp=inw(UART_PORT[port_no]+SERIAL_MCR);
    if(onoff==ON)
        temp|=SERIAL_MCR_LPBK;
    else
        temp&=~(SERIAL_MCR_LPBK);

    outw(UART_PORT[port_no]+SERIAL_MCR,temp);
}

void fLib_SetSerialFifoCtrl(DRVUART_PORT port_no, UINT32 level_tx, UINT32 level_rx, UINT32 resettx, UINT32 resetrx)  //V1.20//ADA10022002
{
    UINT8 fcr = 0;

    fcr |= SERIAL_FCR_FE;

    switch(level_rx)  //V1.20//ADA10022002//Start
    {
        case 4:
            fcr|=0x40;
            break;
        case 8:
            fcr|=0x80;
            break;
        case 14:
            fcr|=0xc0;
            break;
        default:
            break;
    }
    //V1.20//ADA10022002//Start
    switch(level_tx)
    {
        case 3:
            fcr|=0x01<<4;
            break;
        case 9:
            fcr|=0x02<<4;
            break;
        case 13:
            fcr|=0x03<<4;
            break;
        default:
            break;
    }
    //V1.20//ADA10022002//End
    if(resettx)
        fcr|=SERIAL_FCR_TXFR;

    if(resetrx)
        fcr|=SERIAL_FCR_RXFR;

    outw(UART_PORT[port_no]+SERIAL_FCR,fcr);
}


void fLib_DisableSerialFifo(DRVUART_PORT port_no)
{
    outw(UART_PORT[port_no]+SERIAL_FCR,0);
}


void fLib_SetSerialInt(DRVUART_PORT port_no, UINT32 IntMask)
{
    outw(UART_PORT[port_no] + SERIAL_IER, IntMask);
}

char fLib_GetSerialChar(DRVUART_PORT port_no)
{
    char Ch;
    UINT32 status;

       do
    {
         status=inw(UART_PORT[port_no]+SERIAL_LSR);
    }
    while (!((status & SERIAL_LSR_DR)==SERIAL_LSR_DR));    // wait until Rx ready
    Ch = inw(UART_PORT[port_no] + SERIAL_RBR);
    return (Ch);
}

void fLib_PutSerialChar(DRVUART_PORT port_no, char Ch)
{
    UINT32 status;

    do
    {
        status=inw(UART_PORT[port_no]+SERIAL_LSR);
    }while (!((status & SERIAL_LSR_THRE)==SERIAL_LSR_THRE));    // wait until Tx ready
    outw(UART_PORT[port_no] + SERIAL_THR,Ch);
}

void fLib_PutSerialStr(DRVUART_PORT port_no, char *Str)
{
      char *cp;

    for(cp = Str; *cp != 0; cp++)
        fLib_PutSerialChar(port_no, *cp);
}


void fLib_Modem_waitcall(DRVUART_PORT port_no)
{
    fLib_PutSerialStr(port_no, "ATS0=2\r");
}

void fLib_Modem_call(DRVUART_PORT port_no, char *tel)
{
    fLib_PutSerialStr(port_no, "ATDT");
    fLib_PutSerialStr(port_no,  tel);
    fLib_PutSerialStr(port_no, "\r");
}

int fLib_Modem_getchar(DRVUART_PORT port_no,int TIMEOUT)
{
    UINT64 start_time, middle_time, dead_time;
    UINT32 status;
    INT8 ch;
    UINT32 n=0;

    start_time = fLib_CurrentT1Tick();
    dead_time = start_time + TIMEOUT;

    do
    {
        if(n>1000)
        {
            middle_time = fLib_CurrentT1Tick();
            if (middle_time > dead_time)
                return 0x100;
        }
        status = inw(UART_PORT[port_no] + SERIAL_LSR);
        n++;
    }while (!((status & SERIAL_LSR_DR)==SERIAL_LSR_DR));

    ch = inw(UART_PORT[port_no] + SERIAL_RBR);
    return (ch);
}

BOOL fLib_Modem_putchar(DRVUART_PORT port_no, INT8 Ch)
{
    UINT64 start_time, middle_time, dead_time;
      UINT32 status;
    UINT32 n=0;

      start_time = fLib_CurrentT1Tick();
      dead_time = start_time + 5;

    do
    {
        if(n>1000)
        {
            middle_time = fLib_CurrentT1Tick();
            if (middle_time > dead_time)
                return FALSE;
        }
        status = inw(UART_PORT[port_no] + SERIAL_LSR);
        n++;
    }while (!((status & SERIAL_LSR_THRE)==SERIAL_LSR_THRE));

    outw(UART_PORT[port_no] + SERIAL_THR, Ch);

    return TRUE;
}

void fLib_EnableSerialInt(DRVUART_PORT port_no, UINT32 mode)
{
    outw(UART_PORT[port_no] + SERIAL_IER, 0);
    outw(UART_PORT[port_no] + SERIAL_IER, mode);

}


void fLib_DisableSerialInt(DRVUART_PORT port_no, UINT32 mode)
{
UINT32 data;

    data = inw(UART_PORT[port_no] + SERIAL_IER);
    mode = data & (~mode);
    outw(UART_PORT[port_no] + SERIAL_IER, mode);
}

UINT32 fLib_ReadSerialIER(DRVUART_PORT port_no)
{
    return inw(UART_PORT[port_no] + SERIAL_IER);
}

UINT32 fLib_SerialIntIdentification(DRVUART_PORT port_no)
{
    return inw(UART_PORT[port_no] + SERIAL_IIR);
}

void fLib_SetSerialLineBreak(DRVUART_PORT port_no)
{
UINT32 data;

    data = inw(UART_PORT[port_no] + SERIAL_LCR);
    outw(UART_PORT[port_no] + SERIAL_LCR, data | SERIAL_LCR_SETBREAK);
}

void fLib_SerialRequestToSend(DRVUART_PORT port_no)
{
UINT32 data;

    data = inw(UART_PORT[port_no] + SERIAL_MCR);
    outw(UART_PORT[port_no] + SERIAL_MCR, data | SERIAL_MCR_RTS);
}

void fLib_SerialStopToSend(DRVUART_PORT port_no)
{
UINT32 data;

    data = inw(UART_PORT[port_no] + SERIAL_MCR);
    data &= ~(SERIAL_MCR_RTS);
    outw(UART_PORT[port_no] + SERIAL_MCR, data);
}

void fLib_SerialDataTerminalReady(DRVUART_PORT port_no)
{
UINT32 data;

    data = inw(UART_PORT[port_no] + SERIAL_MCR);
    outw(UART_PORT[port_no] + SERIAL_MCR, data | SERIAL_MCR_DTR);
}

void fLib_SerialDataTerminalNotReady(DRVUART_PORT port_no)
{
UINT32 data;

    data = inw(UART_PORT[port_no] + SERIAL_MCR);
    data &= ~(SERIAL_MCR_DTR);
    outw(UART_PORT[port_no] + SERIAL_MCR, data);
}

UINT32 fLib_ReadSerialLineStatus(DRVUART_PORT port_no)
{
    return inw(UART_PORT[port_no] + SERIAL_LSR);
}

UINT32 fLib_ReadSerialModemStatus(DRVUART_PORT port_no)
{
    return inw(UART_PORT[port_no] + SERIAL_MSR);
}
// End of file - serial.c

//used for CLI
//#define CLI_PORT     DebugSerialPort

UINT32 GetUartStatus(DRVUART_PORT port_no)
{
    UINT32 status;
    status=inw(UART_PORT[port_no]+SERIAL_LSR);
    return status;
}


UINT32 IsThrEmpty(UINT32 status)
{
    //if (((status & SERIAL_LSR_THRE)==SERIAL_LSR_THRE) && ((status & SERIAL_LSR_TE)==SERIAL_LSR_TE))
    if ((status & SERIAL_LSR_THRE)==SERIAL_LSR_THRE)
        return TRUE;
    else
        return FALSE;
}

UINT32 IsDataReady(UINT32 status)
{
    if((status & SERIAL_IER_DR)==SERIAL_IER_DR)
        return TRUE;
    else
        return FALSE;

}

void CheckRxStatus(DRVUART_PORT port_no)
{
    UINT32 Status;
    do
    {
        Status = GetUartStatus(port_no);
    }
    while (!IsDataReady(Status));    // wait until Rx ready
}

void CheckTxStatus(DRVUART_PORT port_no)
{
    UINT32 Status;
    do
    {
        Status = GetUartStatus(port_no);
    }while (!IsThrEmpty(Status));    // wait until Tx ready
}

UINT32 fLib_kbhit(DRVUART_PORT port_no)
{
  UINT32 Status;

    Status = GetUartStatus(port_no);
    if(IsDataReady(Status))
        return 1;
    else
        return 0;
}

char fLib_getch(DRVUART_PORT port_no)
{
    char ch;

    if(fLib_kbhit(port_no))
        ch=inw(UART_PORT[port_no]+SERIAL_RBR);
       else
           ch=0;

    return ch;
}

char fLib_getchar(DRVUART_PORT port_no)
{
    char Ch;

    CheckRxStatus(port_no);
    Ch = inw(UART_PORT[port_no]+SERIAL_RBR);
    return (Ch);
}

char fLib_getchar_timeout(DRVUART_PORT port_no, unsigned long timeout)
{
    char        Ch;

    /* wait until rx status ready */
    while((GetUartStatus(port_no) & SERIAL_IER_DR) == 0x0)
    {
        if(timeout != 0)/* 0 means never timeout */
        {
            timeout --;/* count down timeout value */
            if(timeout == 0)/* 0 means timeout */
            {
                return (0xFF);/* return timeout value */
            }
        }
    }
    /* return rx character */
    Ch = inw(UART_PORT[port_no]+SERIAL_RBR);
    return (Ch);
}

void fLib_putchar(DRVUART_PORT port_no, char Ch)
{
    if(Ch!='\0')
    {
        CheckTxStatus(port_no);
        outw(UART_PORT[port_no]+SERIAL_THR,Ch);
    }

    if (Ch == '\n')
    {
        CheckTxStatus(port_no);
        outw(UART_PORT[port_no]+SERIAL_THR,'\r');
    }
}

void fLib_putc(DRVUART_PORT port_no, char Ch)
{
    CheckTxStatus(port_no);
    outw(UART_PORT[port_no]+SERIAL_THR,Ch);

    if (Ch == '\n')
    {
        CheckTxStatus(port_no);
        outw(UART_PORT[port_no]+SERIAL_THR,'\r');
    }
}


void fLib_putstr(DRVUART_PORT port_no, char *str)
{
    char *cp;
    for(cp = str; *cp != 0; cp++)
        fLib_putchar(port_no, *cp);
}

void fLib_printf(const char *f, ...)    /* variable arguments */
{
    va_list arg_ptr;
    char buffer[256];

    INT32 i;
    //    unsigned long flags;

    //spin_lock_irqsave(NULL, flags);
    // i=strlen(f);
    //  if(i>254)
    //  {
    //     while(1);
    //   }

    //put the character to buffer
    va_start(arg_ptr, f);
    vsprintf(&buffer[0], f, arg_ptr);
    va_end(arg_ptr);

    //output the buffer
    i=0;
    //while((buffer[i])&&(i<255))
    while(buffer[i])
    {
        fLib_putchar(DEBUG_CONSOLE, buffer[i]);
        i++;
    }
    /* restore the previous mode */
    //spin_unlock_irqrestore(NULL, flags);

    // return i;
}

//int fLib_scanf(char *buf)
int fLib_gets(DRVUART_PORT port_no, char *buf)
{
    char    *cp;
    char    data;
    UINT32  count;
    count = 0;
    cp = buf;

    do
    {
        data = fLib_getchar(port_no);

        switch(data)
        {
            case RETURN_KEY:
                if(count < 256)
                {
                    *cp = '\0';
                    fLib_putchar(port_no, '\n');
                }
                break;
            case BACKSP_KEY:
            case DELETE_KEY:
                if(count)
                {
                    count--;
                    *(--cp) = '\0';
                    fLib_putstr(port_no, "\b \b");
                }
                break;
            default:
                if( data > 0x1F && data < 0x7F && count < 256)
                {
                    *cp = (char)data;
                    cp++;
                    count++;
                    fLib_putchar(port_no, data);
                }
                break;
        }
    } while(data != RETURN_KEY);

  return (count);
}


//for SWI call
void fLib_DebugPrintChar(DRVUART_PORT port_no, char ch)
{
    if(ch != '\0' && ch != '\n')
    {
          fLib_PutSerialChar(port_no, ch);
    }
    else if(ch == '\n')
    {
        fLib_PutSerialChar(port_no, '\r');//CR
      fLib_PutSerialChar(port_no, '\n');//LF
    }
}

void fLib_DebugPrintString(DRVUART_PORT port_no, char *str)
{
    while(*str)
    {
        fLib_DebugPrintChar(port_no, *str);
        str++;
    }
}

char fLib_DebugGetChar(DRVUART_PORT port_no)
{
    return fLib_GetSerialChar(port_no);
}

UINT32 fLib_DebugGetUserCommand(DRVUART_PORT port_no, UINT8 * buffer, UINT32 Len)
{
    int offset = 0, c;

    buffer[0] = '\0';
    while (offset < (Len - 1)) {
        c = fLib_GetSerialChar(port_no);

        if (c == '\b')        //backspace
        {
            if (offset > 0) {
            // Rub out the old character & update the console output
            offset--;
            buffer[offset] = 0;

            fLib_DebugPrintString(port_no, "\b \b");
            }
        } else if (c == DELETE_KEY)    //backspace
        {
            if (offset > 0) {
            // Rub out the old character & update the console output
            offset--;
            buffer[offset] = 0;

            fLib_DebugPrintString(port_no, "\b \b");
            }
        }

        else {
            if (c == '\r')
            c = '\n';    // treat \r as \n

            fLib_PutSerialChar(port_no, c);

            buffer[offset++] = c;

            if (c == '\n')
            break;
        }
    }

    buffer[offset] = '\0';

    return offset;
}

#ifdef MOZART_CHIP
void uart4_enable(void)
{
    PINMUX_SET(SCU_EXTREG_SD_DATA2, PINMUX_MODE6);
    PINMUX_SET(SCU_EXTREG_SD_DATA3, PINMUX_MODE6);
    SCU_EXTREG_CLK_EN2_SET_uart1_3_fref(1);
}


void uart4_disable(void)
{
    PINMUX_CLR(SCU_EXTREG_SD_DATA2);
    PINMUX_CLR(SCU_EXTREG_SD_DATA3);
    SCU_EXTREG_CLK_EN2_SET_uart1_3_fref(0);    
}
#endif

#else
/***************************************************************************
* Copyright  Faraday Technology Corp 2002-2003.  All rights reserved.      *
*--------------------------------------------------------------------------*
* Name:serial.c                                                            *
* Description: serial library routine                                      *
* Author:                                                        *
****************************************************************************/

#include <stdarg.h>
#include <stdio.h>
#include <stdbool.h>

#include "io.h"
#include "DrvUART010.h"
#include "kneron_mozart.h"
#include "scu_extreg.h"
//#include "Driver_USART.h"

FT_UART0_Type *UART_PORT[2] = {FT_UART0, FT_UART1};

void fLib_SetSerialMode(DRVUART_PORT port_no, uint32_t mode)
{	
	UART_PORT[port_no]->MDR &= ~SERIAL_MDR_MODE_SEL;	 
  UART_PORT[port_no]->MDR |= mode;	
}


void fLib_EnableIRMode(DRVUART_PORT port_no, uint32_t TxEnable, uint32_t RxEnable)
{	
	UART_PORT[port_no]->ACR &= ~(SERIAL_ACR_TXENABLE | SERIAL_ACR_RXENABLE);
	
	if(TxEnable)
      UART_PORT[port_no]->ACR |= SERIAL_ACR_TXENABLE;
  
	if(RxEnable)
      UART_PORT[port_no]->ACR |= SERIAL_ACR_RXENABLE;	
}

/*-----------------------------------------------------------------------------
  Function:		fLib_SerialInit                                                                         
                                                                                                         
  Parameter:        																					   	
	        	None                                     
  Returns:                                                                                                
               	None                                                                                      
  Description:                                                                                            
               	Initialize UART0, 38400bps, 8N1.                                    
 *-----------------------------------------------------------------------------*/

void fLib_SerialInit (DRVUART_PORT port_no, uint32_t baudrate, uint32_t parity,uint32_t num,uint32_t len)
{
	uint32_t lcr;


	lcr = UART_PORT[port_no]->LCR & ~SERIAL_LCR_DLAB;
	/* Set DLAB=1 */  
	UART_PORT[port_no]->LCR = SERIAL_LCR_DLAB;
	
  /* Set baud rate */
  baudrate = (port_no == DRVUART_PORT0 || port_no == DRVUART_PORT3)?(UART_CLOCK/baudrate):(UART_CLOCK_2/baudrate);
  baudrate >>= 4;	//divided by 16
  UART_PORT[port_no]->DLM = ((baudrate & 0xff00) >> 8);
  UART_PORT[port_no]->DLL = (baudrate & 0xff);

	//clear orignal parity setting
	lcr &= 0xc0;
	
	switch (parity)
	{
		case PARITY_NONE:	
			//do nothing
    		break;
    	case PARITY_ODD:
		    lcr|=SERIAL_LCR_ODD;
   		 	break;
    	case PARITY_EVEN:
    		lcr|=SERIAL_LCR_EVEN;
    		break;
    	case PARITY_MARK:
    		lcr|=(SERIAL_LCR_STICKPARITY|SERIAL_LCR_ODD);
    		break;
    	case PARITY_SPACE:
    		lcr|=(SERIAL_LCR_STICKPARITY|SERIAL_LCR_EVEN);
    		break;
    
    	default:
    		break;
    }
    
    if(num==2)
			lcr|=SERIAL_LCR_STOP;	
		
		len-=5;	
		lcr|=len;	    
    UART_PORT[port_no]->LCR = lcr; 	
}


void fLib_SetSerialLoopback(DRVUART_PORT port_no, uint32_t onoff)
{	
	if(onoff==ON)	
		UART_PORT[port_no]->MCR |= SERIAL_MCR_LPBK;
	else
		UART_PORT[port_no]->MCR &= ~(SERIAL_MCR_LPBK);	
}

void fLib_SetSerialFifoCtrl(DRVUART_PORT port_no, uint32_t level_tx, uint32_t level_rx, uint32_t resettx, uint32_t resetrx)  //V1.20//ADA10022002
{
	uint8_t fcr = 0;
	
 
 	fcr |= SERIAL_FCR_FE;
 
 	switch(level_rx)  //V1.20//ADA10022002//Start
 	{
 		case 4:
 			fcr|=0x40;
 			break;
 		case 8:
 			fcr|=0x80;
 			break;
 		case 14:
 			fcr|=0xc0;
 			break;
 		default:
 			break;
 	}
  //V1.20//ADA10022002//Start
 	switch(level_tx)
 	{
 		case 3:
 			fcr|=0x01<<4;
 			break;
 		case 9:
 			fcr|=0x02<<4;
 			break;
 		case 13:
 			fcr|=0x03<<4;
 			break;
 		default:
 			break;
 	}
  //V1.20//ADA10022002//End 	
	if(resettx)
		fcr|=SERIAL_FCR_TXFR;

	if(resetrx)
		fcr|=SERIAL_FCR_RXFR; 	

	UART_PORT[port_no]->FCR = fcr;	
}


void fLib_DisableSerialFifo(DRVUART_PORT port_no)
{
	UART_PORT[port_no]->FCR = 0;	
}


void fLib_SetSerialInt(DRVUART_PORT port_no, uint32_t IntMask)
{
	UART_PORT[port_no]->IER = IntMask;	
}

char fLib_GetSerialChar(DRVUART_PORT port_no)
{	
	while(!((UART_PORT[port_no]->LSR & SERIAL_LSR_DR)==SERIAL_LSR_DR));	// wait until Rx ready  
	
  return (UART_PORT[port_no]->RBR);	
}				

void fLib_PutSerialChar(DRVUART_PORT port_no, char Ch)
{	
	while (!((UART_PORT[port_no]->LSR & SERIAL_LSR_THRE)==SERIAL_LSR_THRE));	// wait until Tx ready	   
  
	UART_PORT[port_no]->THR = Ch;
}

void fLib_PutSerialStr(DRVUART_PORT port_no, char *Str)
{
  char *cp;
   
 	for(cp = Str; *cp != 0; cp++)       
   		fLib_PutSerialChar(port_no, *cp);	
}

void fLib_Modem_waitcall(DRVUART_PORT port_no)
{
	fLib_PutSerialStr(port_no, "ATS0=2\r");	
}					

void fLib_Modem_call(DRVUART_PORT port_no, char *tel)
{
	fLib_PutSerialStr(port_no, "ATDT");
	fLib_PutSerialStr(port_no,  tel);
	fLib_PutSerialStr(port_no, "\r");
}

void fLib_EnableSerialInt(DRVUART_PORT port_no, uint32_t mode)
{	
	UART_PORT[port_no]->IER |= mode;	
}


void fLib_DisableSerialInt(DRVUART_PORT port_no, uint32_t mode)
{	
	UART_PORT[port_no]->IER &= (~mode);
}

uint32_t fLib_ReadSerialIER(DRVUART_PORT port_no)
{
	return UART_PORT[port_no]->IER;	
}

uint32_t fLib_SerialIntIdentification(DRVUART_PORT port_no)
{
	return UART_PORT[port_no]->IIR;	
}

void fLib_SetSerialLineBreak(DRVUART_PORT port_no)
{	
	UART_PORT[port_no]->LCR |= (SERIAL_LCR_SETBREAK);	
}

void fLib_SerialRequestToSend(DRVUART_PORT port_no)
{	
	UART_PORT[port_no]->MCR |= (SERIAL_MCR_RTS);	
}

void fLib_SerialStopToSend(DRVUART_PORT port_no)
{
	UART_PORT[port_no]->MCR &= ~(SERIAL_MCR_RTS);	
}

void fLib_SerialDataTerminalReady(DRVUART_PORT port_no)
{	
	UART_PORT[port_no]->MCR |= (SERIAL_MCR_DTR);	
}

void fLib_SerialDataTerminalNotReady(DRVUART_PORT port_no)
{	
	UART_PORT[port_no]->MCR &= ~(SERIAL_MCR_DTR);	
}

uint32_t fLib_ReadSerialLineStatus(DRVUART_PORT port_no)
{
	return UART_PORT[port_no]->LSR;	
}

uint32_t fLib_ReadSerialModemStatus(DRVUART_PORT port_no)
{
	return UART_PORT[port_no]->MSR;
}
// End of file - serial.c


//used for CLI
//#define CLI_PORT 	DebugSerialPort

uint32_t GetUartStatus(DRVUART_PORT port_no)
{    
    return UART_PORT[port_no]->LSR; 
}


uint32_t IsThrEmpty(uint32_t status)
{
    if((status & SERIAL_LSR_THRE)==SERIAL_LSR_THRE)
        return true;
    else
        return false;  
}

uint32_t IsDataReady(uint32_t status)
{
    if((status & SERIAL_LSR_DR)==SERIAL_LSR_DR)
        return true;
    else
        return false;  

}

void CheckRxStatus(DRVUART_PORT port_no)
{
  uint32_t Status;
	
	
  do
  {
		Status = GetUartStatus(port_no);
	}
	while (!IsDataReady(Status));	// wait until Rx ready
}

void CheckTxStatus(DRVUART_PORT port_no)
{   
  uint32_t Status;
	
	do
  {
	  Status = GetUartStatus(port_no);	    
  }while (!IsThrEmpty(Status));	// wait until Tx ready	   
}
uint32_t fLib_kbhit(DRVUART_PORT port_no)
{
  uint32_t Status;
	
	
	Status = GetUartStatus(port_no);
	
	if(IsDataReady(Status))
		return 1;
	else
		return 0;
}

char fLib_getch(DRVUART_PORT port_no)
{   
  char ch;    
	
	
	if(fLib_kbhit(port_no))
		ch = UART_PORT[port_no]->RBR;       	
  else
  	ch=0;
   		
  return ch;
}	

char fLib_getchar(DRVUART_PORT port_no)
{ 

	CheckRxStatus(port_no);  

  return (UART_PORT[port_no]->RBR);
}				

char fLib_getchar_timeout(DRVUART_PORT port_no, unsigned long timeout)
{
	/* wait until rx status ready */
	while((GetUartStatus(port_no) & SERIAL_IER_DR) == 0x0)
	{
		if(timeout != 0)/* 0 means never timeout */
		{
			timeout --;/* count down timeout value */
			
			if(timeout == 0)/* 0 means timeout */
			{
				return (0xFF);/* return timeout value */
			}
		}
	}
	
	/* return rx character */	
	return (UART_PORT[port_no]->RBR);
}


void fLib_putchar(DRVUART_PORT port_no, char Ch)
{
    if(Ch!='\0')
    {        
      CheckTxStatus(port_no);			
      UART_PORT[port_no]->THR = Ch;  
    }
    
    if (Ch == '\n')
    {
	    CheckTxStatus(port_no);
      UART_PORT[port_no]->THR = '\r';
    }    
}



void fLib_putc(DRVUART_PORT port_no, char Ch)
{
  CheckTxStatus(port_no);
	UART_PORT[port_no]->THR = Ch;    
    
  if (Ch == '\n')
  {
	  CheckTxStatus(port_no);
    UART_PORT[port_no]->THR = '\r';
  }      
}


void fLib_putstr(DRVUART_PORT port_no, char *str)
{
  char *cp;   
    
	
	for(cp = str; *cp != 0; cp++)       
		fLib_putchar(port_no, *cp);
}


void fLib_printf(const char *f, ...)	/* variable arguments */
{
    va_list arg_ptr;    
    char buffer[256];
    int32_t i;
   
   	//put the character to buffer
   	va_start(arg_ptr, f);
   	vsprintf(&buffer[0], f, arg_ptr);
   	va_end(arg_ptr);   
	
   	//output the buffer
    i=0;
    //while((buffer[i])&&(i<255))
    while(buffer[i])
    {
    	fLib_putchar(DEBUG_CONSOLE, buffer[i]);
    	i++;
    }
   /* restore the previous mode */
   //spin_unlock_irqrestore(NULL, flags);

   // return i;
}


//int fLib_scanf(char *buf)
int fLib_gets(DRVUART_PORT port_no, char *buf)
{
    char    *cp;
    char    data;
    uint32_t  count;
    count = 0;
    cp = buf;
    
    do
    {
        data = fLib_getchar(port_no);

        switch(data)
        {
            case RETURN_KEY:
                if(count < 256)
                {
                    *cp = '\0';
                    fLib_putchar(port_no, '\n');
                }          
                break;
            case BACKSP_KEY:
            case DELETE_KEY:
                if(count)
                {
                    count--;
                    *(--cp) = '\0';
                    fLib_putstr(port_no, "\b \b");
                }         
                break;
            default:         
                if( data > 0x1F && data < 0x7F && count < 256)
                {
                    *cp = (char)data;
                    cp++;
                    count++;
                    fLib_putchar(port_no, data);
                }
                break;
        }
    } while(data != RETURN_KEY);  
    
  return (count);
}


//for SWI call
void fLib_DebugPrintChar(DRVUART_PORT port_no, char ch)
{
    if(ch != '\0' && ch != '\n')
    {
		  fLib_PutSerialChar(port_no, ch);
    }
    else if(ch == '\n')
    {
  	  fLib_PutSerialChar(port_no, '\r');//CR
      fLib_PutSerialChar(port_no, '\n');//LF
    }
}

void fLib_DebugPrintString(DRVUART_PORT port_no, char *str)
{
	while(*str)
	{
		fLib_DebugPrintChar(port_no, *str);
		str++;
	}
}

char fLib_DebugGetChar(DRVUART_PORT port_no)
{
	return fLib_GetSerialChar(port_no);
}

uint32_t fLib_DebugGetUserCommand(DRVUART_PORT port_no, uint8_t * buffer, uint32_t Len)
{
    int offset = 0, c;

    buffer[0] = '\0';
    while (offset < (Len - 1)) {
	c = fLib_GetSerialChar(port_no);

	if (c == '\b')		//backspace
	{
	    if (offset > 0) {
		// Rub out the old character & update the console output
		offset--;
		buffer[offset] = 0;

		fLib_DebugPrintString(port_no, "\b \b");
	    }
	} else if (c == DELETE_KEY)	//backspace
	{
	    if (offset > 0) {
		// Rub out the old character & update the console output
		offset--;
		buffer[offset] = 0;

		fLib_DebugPrintString(port_no, "\b \b");
	    }
	}

	else {
	    if (c == '\r')
		c = '\n';	// treat \r as \n

	    fLib_PutSerialChar(port_no, c);

	    buffer[offset++] = c;

	    if (c == '\n')
		break;
	}
    }

    buffer[offset] = '\0';

    return offset;
}

#ifdef MOZART_CHIP
void uart4_enable(void)
{
    PINMUX_SET(SCU_EXTREG_SD_DATA2, PINMUX_MODE6);
    PINMUX_SET(SCU_EXTREG_SD_DATA3, PINMUX_MODE6);
    SCU_EXTREG_CLK_EN2_SET_uart1_3_fref(1);
}


void uart4_disable(void)
{
    PINMUX_CLR(SCU_EXTREG_SD_DATA2);
    PINMUX_CLR(SCU_EXTREG_SD_DATA3);
    SCU_EXTREG_CLK_EN2_SET_uart1_3_fref(0);    
}
#endif
#endif