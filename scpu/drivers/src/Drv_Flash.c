#include "Driver_Flash.h"
#include "DrvSPI020.h"
#include "common_include.h"
//#include "flib.h"
#include "W25Q64CV.h"

#define ARM_FLASH_DRV_VERSION    ARM_DRIVER_VERSION_MAJOR_MINOR(1, 0) /* driver version */

/* Sector Information */
#ifdef FLASH_SECTORS
static ARM_FLASH_SECTOR FLASH_SECTOR_INFO[FLASH_SECTOR_COUNT] = {
    FLASH_SECTORS
};
#else
#define FLASH_SECTOR_INFO    NULL
#endif

/* Flash Information */
static ARM_FLASH_INFO FlashInfo = {
    0, /* FLASH_SECTOR_INFO  */
    0, /* FLASH_SECTOR_COUNT */
    0, /* FLASH_SECTOR_SIZE  */
    0, /* FLASH_PAGE_SIZE    */
    0, /* FLASH_PROGRAM_UNIT */
    0,  /* FLASH_ERASED_VALUE */
    0
};

/* Flash Status */
static ARM_FLASH_STATUS FlashStatus;

/* Driver Version */
static const ARM_DRIVER_VERSION DriverVersion = {
    ARM_FLASH_API_VERSION,
    ARM_FLASH_DRV_VERSION
};

/* Driver Capabilities */
static const ARM_FLASH_CAPABILITIES DriverCapabilities = {
    0, /* event_ready */
    0, /* data_width = 0:8-bit, 1:16-bit, 2:32-bit */
    0  /* erase_chip */
};

//static ARM_FLASH_STATUS cur_status;
//static unsigned int spi_bus_speed;
//static unsigned int spi_total_data_count;


SPI_Flash_T flash_info;

//
//  Functions
//
#define min_t(x,y) ( x < y ? x: y )

#define DMAC_CSR        0x24
#define DMAC_Cn_CSR     0x100
#define DMAC_Cn_CFG     0x104
#define DMAC_Cn_SrcAddr 0x108
#define DMAC_Cn_DstAddr 0x10C
#define DMAC_Cn_SIZE    0x114

#if 0
#define DMAMAINCSR              (DMAC_FTDMAC020_PA_BASE+0x24)
#define DMATCREG                (DMAC_FTDMAC020_PA_BASE+0x04)
#define DMATCCLEAR              (DMAC_FTDMAC020_PA_BASE+0x08)
#else //bessel:fie3100+fie3101:right now, because SPI020 is in FIE3101, we need to use DMA in FIE3101 instead of one in FIE3100
#define DMAMAINCSR              (AHB_LITE_FTDMAC020_PA_BASE+0x24)
#define DMATCREG                (AHB_LITE_FTDMAC020_PA_BASE+0x04)
#define DMATCCLEAR              (AHB_LITE_FTDMAC020_PA_BASE+0x08)
#endif

#define DMACEN      BIT0        //DMA controller enable
#define CH_EN       BIT0        //Channel Enable


void spi020_set_commands(UINT32 cmd0, UINT32 cmd1, UINT32 cmd2, UINT32 cmd3)
{
    vLib_LeWrite32((INT8U * )SPI020REG_CMD0, cmd0);
    vLib_LeWrite32((INT8U * )SPI020REG_CMD1, cmd1);
    vLib_LeWrite32((INT8U * )SPI020REG_CMD2, cmd2);
    vLib_LeWrite32((INT8U * )SPI020REG_CMD3, cmd3);
}

/* Wait until command complete */
void spi020_wait_command_complete(void)
{
    UINT32  reg;

    do {
        reg = u32Lib_LeRead32((INT8U * )SPI020REG_INTR_ST);
    } while ((reg & SPI020_CMD_CMPLT)==0x0);
//  vLib_LeWrite32(SPI020REG_INTR_ST, (reg | SPI020_CMD_CMPLT));/* clear command complete status */
    vLib_LeWrite32((INT8U * )SPI020REG_INTR_ST, SPI020_CMD_CMPLT);/* clear command complete status */
}

/* Wait until the rx fifo ready */
void spi020_wait_rx_full(void)
{
    while(!(u32Lib_LeRead32((INT8U * )SPI020REG_STATUS) & SPI020_RXFIFO_READY));
}

/* Wait until the tx fifo ready */
void spi020_wait_tx_empty(void)
{
    while(!(u32Lib_LeRead32((INT8U * )SPI020REG_STATUS) & SPI020_TXFIFO_READY));
}

/* Get the rx fifo depth, unit in byte */
UINT32 spi020_rxfifo_depth(void)
{
    return ((u32Lib_LeRead32((INT8U * )SPI020REG_FEATURE) & SPI020_RX_DEPTH) >> (8-2));
}

/* Get the tx fifo depth, unit in byte */
UINT32 spi020_txfifo_depth(void)
{
//      unsigned int read;
        
    return ((u32Lib_LeRead32((INT8U * )SPI020REG_FEATURE) & SPI020_TX_DEPTH) << 2);
}

void spi020_read_data(/*UINT8*/UINT32 *buf, UINT32 length)
{
    UINT32  access_byte;//, tmp_read;

    while(length > 0)
    {
        spi020_wait_rx_full();
        access_byte = min_t(length, spi020_rxfifo_depth());
        length -= access_byte;
        while(access_byte > 0)
        {
            *buf= u32Lib_LeRead32((INT8U * )SPI020REG_DATAPORT);
            buf ++;
            if(access_byte>=4)
                access_byte -= 4;
            else
                access_byte=0;
            #if 0
            switch(access_byte)
            {
            case 1:
                tmp_read = u32Lib_LeRead32((INT8U * )SPI020REG_DATAPORT);
                *buf = tmp_read&0xFF;
                access_byte = 0;//break while loop 
                break;
            case 2:
                tmp_read = u32Lib_LeRead32((INT8U * )SPI020REG_DATAPORT);
                *buf = tmp_read&0xFF;
                buf++;
                *buf = (tmp_read&0xFF00)>>8;
                access_byte = 0;// break while loop 
                break;
            case 3:// read chip id will use this case 
                tmp_read = u32Lib_LeRead32((INT8U * )SPI020REG_DATAPORT);
                *buf = tmp_read&0x00FF;
                buf++;
                *buf = (tmp_read&0xFF00)>>8;
                buf++;
                *buf = (tmp_read&0xFF0000)>>16;
                access_byte = 0;// break while loop 
                break;
            default:// access_byte>=4 
                *(UINT32 *)buf= u32Lib_LeRead32((INT8U * )SPI020REG_DATAPORT);
                buf +=4;
                access_byte -= 4;
                break;
            }
            #endif
        }
    }
}

void spi020_check_status_til_ready_2(void)
{
//  main_delay_count(0x5100);

    /* fill in command 0~3 */
    spi020_set_commands(SPI020_05_CMD0, SPI020_05_CMD1, SPI020_05_CMD2, SPI020_05_CMD3);
//  main_delay_count(0x80);
    /* wait for command complete */
    spi020_wait_command_complete();
}

void spi020_check_status_til_ready(void)
{
    /* savecodesize, move into here */
    spi020_wait_command_complete();

    /* read status */
    spi020_check_status_til_ready_2();

}


void spi020_4Bytes_ctrl(UINT8 enable)
{
    if (enable) {
        #if FLASH_4BYTES_CMD_EN                    
        spi020_set_commands(SPI020_B7_CMD0, SPI020_B7_CMD1, SPI020_B7_CMD2, SPI020_B7_CMD3);
        #endif
    } else {
        spi020_set_commands(SPI020_E9_CMD0, SPI020_E9_CMD1, SPI020_E9_CMD2, SPI020_E9_CMD3);
    }
    /* wait for command complete */
    spi020_wait_command_complete();
}


void spi020_write_control(UINT8 enable)
{
    /* fill in command 0~3 */
    if (enable) {
        spi020_set_commands(SPI020_06_CMD0, SPI020_06_CMD1, SPI020_06_CMD2, SPI020_06_CMD3);
    } else {
        spi020_set_commands(SPI020_04_CMD0, SPI020_04_CMD1, SPI020_04_CMD2, SPI020_04_CMD3);
    }
    /* wait for command complete */
    spi020_wait_command_complete();
}

void spi020_write_data(UINT8 *buf, UINT32 length)
{
    INT32  access_byte;

    /* This function assume length is multiple of 4 */
    while(length > 0) {
        spi020_wait_tx_empty();
        access_byte = min_t(length, spi020_txfifo_depth());
        length -= access_byte;
        while(access_byte > 0) {
            vLib_LeWrite32((INT8U * )SPI020REG_DATAPORT, *((UINT32 *)buf));
            buf += 4;
            access_byte -= 4;
        }
    }
}

void spi020_flash_64kErase(UINT32 offset)
{
    /* The start offset should be in 64K boundary */
    /* if(offset % SPI020_64K) return 1; */

//  spi020_check_status_til_ready_2();

    spi020_write_control(1);/* send write enabled */

    /* fill in command 0~3 */
    spi020_set_commands(offset, SPI020_D8_CMD1, SPI020_D8_CMD2, SPI020_D8_CMD3);
    /* wait for command complete */
//  spi020_wait_command_complete();/* savecodesize, move into spi020_check_status_til_ready */
//  main_delay_count(0x120000);
    spi020_check_status_til_ready();
//  spi020_check_status_til_ready_2();
}

void spi020_flash_32kErase(UINT32 offset)
{
    /* The start offset should be in 64K boundary */
    /* if(offset % SPI020_64K) return 1; */

//  spi020_check_status_til_ready_2();

    spi020_write_control(1);/* send write enabled */

    /* fill in command 0~3 */
    spi020_set_commands(offset, SPI020_52_CMD1, SPI020_52_CMD2, SPI020_52_CMD3);
    /* wait for command complete */
//  spi020_wait_command_complete();/* savecodesize, move into spi020_check_status_til_ready */
//  main_delay_count(0x120000);
    spi020_check_status_til_ready();
//  spi020_check_status_til_ready_2();
}

void spi020_flash_4kErase(UINT32 offset)
{
    /* The start offset should be in 64K boundary */
    /* if(offset % SPI020_4K) return 1; */

//  spi020_check_status_til_ready_2();

    spi020_write_control(1);/* send write enabled */

    /* fill in command 0~3 */
    #if FLASH_4BYTES_CMD_EN
    spi020_set_commands(offset, SPI020_21_CMD1, SPI020_21_CMD2, SPI020_21_CMD3);
    #else
    spi020_set_commands(offset, SPI020_20_CMD1, SPI020_20_CMD2, SPI020_20_CMD3);
    #endif
    /* wait for command complete */
//  spi020_wait_command_complete();/* savecodesize, move into spi020_check_status_til_ready */
//  main_delay_count(0x120000);
    spi020_check_status_til_ready();
//  spi020_check_status_til_ready_2();
}

UINT32 spi020_flash_probe(SPI_Flash_T *flash)
{
    UINT32  chip_id=0;

    UINT32  probe_90_instruction=0;
    /* fill in command 0~3 */
    //Read Manufacturer and Device Identification by JEDEC ID(0x9F)
    spi020_set_commands(SPI020_9F_CMD0, SPI020_9F_CMD1, SPI020_9F_CMD2, SPI020_9F_CMD3);
    /* read data */
    spi020_read_data(/*(UINT8 *)*/&chip_id, 0x3);
    /* wait for command complete */
    spi020_wait_command_complete();

    //flash->manufacturer = (chip_id>>24);
    flash->manufacturer = (UINT8)chip_id;
    if (flash->manufacturer == 0x00 || flash->manufacturer == 0xFF) {
       /* fill in command 0~3 */
       //Read Manufacturer and Device Identification by 0x90
        spi020_set_commands(SPI020_90_CMD0, SPI020_90_CMD1, SPI020_90_CMD2, SPI020_90_CMD3);
        /* read data */
        spi020_read_data(/*(UINT8 *)*/&chip_id, 0x02/*0x4*/);
        /* wait for command complete */
        spi020_wait_command_complete();
        //flash->manufacturer = (chip_id>>24);
        flash->manufacturer = (UINT8)chip_id;
        probe_90_instruction=1;
    }
    flash->flash_id = (chip_id>>8);
    return probe_90_instruction;
}

/* ===================================
 * Init SPI controller and flash device.
 * Init flash information and register functions.
 * =================================== */
void Read_SPI_Flash_ID(void)
{
    UINT32 probe_90_instruction; 
    UINT32 sizeId;
    char *flash_manu;

    probe_90_instruction=spi020_flash_probe(&flash_info);
    switch (flash_info.manufacturer) {
    case FLASH_WB_DEV:
        flash_manu = "WINBOND";
        break;
    case FLASH_MXIC_DEV:
        flash_manu = "MXIC";
        break;
    case FLASH_Micron_DEV:
        flash_manu = "Micron";
        break;
    default:
        flash_manu = "Unknown";
        break;
    }
    sizeId = (flash_info.flash_id & 0xFF00)>>8;
    if(sizeId >= FLASH_SIZE_1MB_ID)
        flash_info.flash_size = 0x100000 * (1<<(sizeId-FLASH_SIZE_1MB_ID));

    fLib_printf("Manufacturer ID = 0x%02X (%s)\n", flash_info.manufacturer,flash_manu);
    fLib_printf("Device ID       = 0x");
    if(probe_90_instruction)
        fLib_printf("%02X\n", flash_info.flash_id);
    else
        fLib_printf("%04X\n", flash_info.flash_id);

    fLib_printf("Flash Size      = ");
    if((flash_info.flash_size%1024)==0x00) {
        if(((flash_info.flash_size%1024)%1024)==0x00) {
            fLib_printf("%dByte(%dMByte)\n", flash_info.flash_size,flash_info.flash_size>>10>>10);
        } else {
            fLib_printf("%dByte=%dKByte)\n", flash_info.flash_size,flash_info.flash_size>>10);
        }   
    } else {
        fLib_printf("%dByte\n", flash_info.flash_size);
    }
    
    FlashInfo.flash_size = flash_info.flash_size;
}



ARM_DRIVER_VERSION ARM_Flash_GetVersion(void)
{
    return DriverVersion;
}

ARM_FLASH_CAPABILITIES ARM_Flash_GetCapabilities(void)
{
    return DriverCapabilities;
}

int32_t ARM_Flash_Initialize(ARM_Flash_SignalEvent_t cb_event)
{
    UINT32  reg;
#if 1
    vLib_LeWrite32(SPI020REG_CONTROL, SPI020_ABORT);
    /* Wait reset completion */
    do {
        if((u32Lib_LeRead32(SPI020REG_CONTROL)&SPI020_ABORT)==0x00)
            break;
    } while(1);
#endif
    /* Set control register */
    reg = u32Lib_LeRead32(SPI020REG_CONTROL); // 0x80
    reg &= ~(SPI020_CLK_MODE | SPI020_CLK_DIVIDER);
    reg |= SPI_CLK_MODE0 | SPI_CLK_DIVIDER_2;//SPI_CLK_DIVIDER_8;
    vLib_LeWrite32(SPI020REG_CONTROL, reg); 
    Read_SPI_Flash_ID();
    #if FLASH_4BYTES_CMD_EN
    spi020_4Bytes_ctrl(1);
    #else
    spi020_4Bytes_ctrl(0);
    #endif
    return 0;       
}

int32_t ARM_Flash_Uninitialize(void)
{
    return 0;
}

int32_t ARM_Flash_PowerControl(ARM_POWER_STATE state)
{
    switch (state) {
    case ARM_POWER_OFF:
        break;

    case ARM_POWER_LOW:
        break;

    case ARM_POWER_FULL:
        break;

    default:
        return ARM_DRIVER_ERROR_UNSUPPORTED;
    }
    return ARM_DRIVER_OK;
}

void spi020_flash_read(UINT8 type, UINT32 offset, UINT32 len, void *buf)
{
    UINT32 *read_buf;//UINT8               *read_buf;

    if (type & FLASH_DMA_READ) {
        vLib_LeWrite32(SPI020REG_INTERRUPT, SPI020_DMA_EN);/* enable DMA function */
    }

    /* fill in command 0~3 */
    if (type & FLASH_DTR_RW) {
        if (type & FLASH_DUAL_READ)
                spi020_set_commands(offset, SPI020_BD_CMD1, len, SPI020_BD_CMD3);
            else if(type & FLASH_QUAD_RW)
                spi020_set_commands(offset, SPI020_ED_CMD1, len, SPI020_ED_CMD3);
            else
                spi020_set_commands(offset, SPI020_0D_CMD1, len, SPI020_0D_CMD3);
    } else if (type & FLASH_DUAL_READ) {
        if(type & FLASH_IO_RW) {
            //fLib_printf("Daul (0xBB) read\n");
            #if FLASH_4BYTES_CMD_EN
            spi020_set_commands(offset, SPI020_BC_CMD1, len, SPI020_BC_CMD3);
            #else
            spi020_set_commands(offset, SPI020_BB_CMD1, len, SPI020_BB_CMD3);
            #endif
        } else {
            //fLib_printf("Daul (0x3B) read\n");
            #if FLASH_4BYTES_CMD_EN
            spi020_set_commands(offset, SPI020_3C_CMD1, len, SPI020_3C_CMD3);
            #else
            spi020_set_commands(offset, SPI020_3B_CMD1, len, SPI020_3B_CMD3);
            #endif
        }
    } else if(type & FLASH_QUAD_RW) {
        if(type & FLASH_IO_RW) {
            //fLib_printf("Quad (0xEB) read\n");
            #if FLASH_4BYTES_CMD_EN
            spi020_set_commands(offset, SPI020_EC_CMD1, len, SPI020_EC_CMD3);
            #else
            spi020_set_commands(offset, SPI020_EB_CMD1, len, SPI020_EB_CMD3);
            #endif
        } else {
            //fLib_printf("Quad (0x6B) read\n");
            spi020_set_commands(offset, SPI020_6B_CMD1, len, SPI020_6B_CMD3);
        }
    } else if(type & FLASH_FAST_READ) {
        //fLib_printf("Fast (0x0B) read\n");
        #if FLASH_4BYTES_CMD_EN
        spi020_set_commands(offset, SPI020_0C_CMD1, len, SPI020_0C_CMD3);
        #else
        spi020_set_commands(offset, SPI020_0B_CMD1, len, SPI020_0B_CMD3);
        #endif
    } else {/* normal read */ 
        //fLib_printf("Normal (0x03) read\n");
        #if FLASH_4BYTES_CMD_EN
        spi020_set_commands(offset, SPI020_13_CMD1, len, SPI020_13_CMD3);
        #else
        spi020_set_commands(offset, SPI020_03_CMD1, len, SPI020_03_CMD3);
        #endif
    }

    if (type & FLASH_DMA_READ) {
        return;
    }

    read_buf = (UINT32 *)buf;//read_buf = (UINT8 *)buf;
    spi020_read_data(read_buf, len);/* read data */
    spi020_wait_command_complete();/* wait for command complete */
//  spi020_check_status_til_ready();
}
#if 1 //add by legend for dma
void spi020_dma_read_stop(void)
{
    spi020_wait_command_complete();/* wait for command complete */
//  spi020_check_status_til_ready();

    vLib_LeWrite32((INT8U * )SPI020REG_INTERRUPT, 0x0);/* disable DMA function */
}

void spi020_dma_write_stop(void)
{
    spi020_wait_command_complete();/* savecodesize, move into spi020_check_status_til_ready */
    vLib_LeWrite32((INT8U * )SPI020REG_INTERRUPT, 0x0);/* disable DMA function */
    //spi020_check_status_til_ready();
    spi020_check_status_til_ready_2();
}
UINT8 spi020_flash_r_state_OpCode_35(void)
{
    UINT8   tmpbuf;
    /* fill in command 0~3 */
    //spi020_set_commands(SPI020_05_CMD0, SPI020_05_CMD1, SPI020_05_CMD2, SPI020_05_CMD3&0xFFFFFFFE);
    spi020_set_commands(SPI020_35_CMD0, SPI020_35_CMD1, SPI020_35_CMD2, SPI020_35_CMD3);//bessel:wait interrupt instead of delay
    //main_delay_count(1000);
    spi020_wait_command_complete();
    tmpbuf = u32Lib_LeRead32((INT8U * )SPI020REG_READ_ST);
    return tmpbuf;
}

#endif
void spi020_flash_write(UINT8 type, UINT32 offset, UINT32 len, void *buf)
{
    UINT8 *write_buf;

    /* This function does not take care about 4 bytes alignment */
    /* if ((UINT32)(para->buf) % 4) return 1; */

//  spi020_check_status_til_ready_2();
    //fLib_printf("write: offset:%x\n", offset);
    spi020_write_control(1);/* send write enabled */   

    /* fill in command 0~3 */
    if(type & FLASH_QUAD_RW) {
        //fLib_printf("Quad (0x32) write\n");
        spi020_set_commands(offset, SPI020_32_CMD1, len, SPI020_32_CMD3);
    } else {
        //fLib_printf("Normal (0x02) write\n");
        #if FLASH_4BYTES_CMD_EN
        spi020_set_commands(offset, SPI020_12_CMD1, len, SPI020_12_CMD3);
        #else
        spi020_set_commands(offset, SPI020_02_CMD1, len, SPI020_02_CMD3);
        #endif
    }

    if (type & FLASH_DMA_WRITE) {
        vLib_LeWrite32(SPI020REG_INTERRUPT, SPI020_DMA_EN);/* enable DMA function */
        return;
    }

    write_buf = (UINT8 *)buf;
        //fLib_printf("write_buf:%x, len=%x\n",write_buf, len);
    spi020_write_data(write_buf, len);
//  spi020_wait_command_complete();/* savecodesize, move into spi020_check_status_til_ready */
    spi020_check_status_til_ready();
    return;
}


int32_t ARM_Flash_ReadData(uint32_t addr, void *data, uint32_t cnt)
{
    UINT8   Option;

    Option = FLASH_NORMAL;
    spi020_flash_read(Option, addr , cnt , data);
    return 0;
}

int32_t ARM_Flash_ProgramData(uint32_t addr, const void *data, uint32_t cnt)
{
    UINT8   Option;

    Option = FLASH_NORMAL;
    spi020_flash_write(Option, addr, cnt, (void *)data);
    return 0;
}

int32_t ARM_Flash_EraseSector(uint32_t addr)
{
    spi020_flash_4kErase(addr);
    return 0;
}

int32_t ARM_Flash_EraseChip(void)
{
//  spi020_check_status_til_ready_2();

    spi020_write_control(1);/* send write enabled */

    /* fill in command 0~3 */
    spi020_set_commands(SPI020_C7_CMD0, SPI020_C7_CMD1, SPI020_C7_CMD2, SPI020_C7_CMD3);
    /* wait for command complete */
//  spi020_wait_command_complete();/* savecodesize, move into spi020_check_status_til_ready */
    /* wait for flash ready */
    spi020_check_status_til_ready();
    return 0;
}

ARM_FLASH_STATUS ARM_Flash_GetStatus(void)
{
    return FlashStatus;
}

ARM_FLASH_INFO * ARM_Flash_GetInfo(void)
{
    return &FlashInfo;
}

void ARM_Flash_SignalEvent(uint32_t event)
{
}

ARM_DRIVER_FLASH Driver_Flash = {
    ARM_Flash_GetVersion,
    ARM_Flash_GetCapabilities,
    ARM_Flash_Initialize,
    ARM_Flash_Uninitialize,
    ARM_Flash_PowerControl,
    ARM_Flash_ReadData,
    ARM_Flash_ProgramData,
    ARM_Flash_EraseSector,
    ARM_Flash_EraseChip,
    ARM_Flash_GetStatus,
    ARM_Flash_GetInfo,
};
