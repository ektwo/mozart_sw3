#include "ddr.h"
#include "kneron_mozart.h" 
#include "io.h"
#include "scu_extreg.h"
#include "dbg.h"


#ifdef MOZART_CHIP
#define DDR_BASE DDRC_FTDDR3030_PA_BASE
//extern void small_d(int delay);
extern void delay_ms(int sec);
void ddr_init_spl(void)
{   
    int val;

    SCU_EXTREG_DDR_CTRL_SET_wakeup(1);
    SCU_EXTREG_DDR_CTRL_SET_SELFBIAS(1);
	delay_ms(1);
    
	//outw(SCU_EXTREG_PA_BASE + 0x68, (inw(SCU_EXTREG_PA_BASE + 0x68) | 0x00001000)); // wakeup NCPU
	//outw(0xC2380044, 0x1F0200);
    
    
    outw(DDR_BASE + 0x100 , 0x000206C3);
    outw(DDR_BASE + 0x104 , 0x0000ff00);
    outw(DDR_BASE + 0x108 , 0x000000ff);

    outw(DDR_BASE + 0x04 , 0x00000000);
    outw(DDR_BASE + 0x08 , 0x00041c70);
    outw(DDR_BASE + 0x0C , 0x00000018);

    outw(DDR_BASE + 0x10 , 0x60007272);
    outw(DDR_BASE + 0x14 , 0x170D0F0B);
    outw(DDR_BASE + 0x18 , 0x21611323);
    outw(DDR_BASE + 0x1C , 0x00002041);

    outw(DDR_BASE + 0x20 , 0x00000f02);
    outw(DDR_BASE + 0x24 , 0x77777777);
    outw(DDR_BASE + 0x130 , 0x77777777);
	outw(DDR_BASE + 0x28 , /*0x00000a29*/1);

    outw(DDR_BASE + 0x30 , 0x90000000);
    outw(DDR_BASE + 0x34 , 0x03030303);
    outw(DDR_BASE + 0x38 , 0x03030303);
    outw(DDR_BASE + 0x3C , 0x00030011);

    outw(DDR_BASE + 0x48 , 0x00000002);

	outw(DDR_BASE + 0x70 , /*0x000000ff*/0x0);
    //outw(DDR_BASE + 0x74 , 0x44444444);
    outw(DDR_BASE + 0x78 , 0xEEEEEEEE);

    outw(DDR_BASE + 0xA8 , 0x0000D000);
    outw(DDR_BASE + 0xAC , 0x00000100);

    outw(DDR_BASE + 0x10C , 0x00000000);
    outw(DDR_BASE + 0x110 , 0x00000000);
    outw(DDR_BASE + 0x114 , 0x00000000);
    outw(DDR_BASE + 0x11C , 0x00000000);
    outw(DDR_BASE + 0x138 , 0x00000005);

    delay_ms(1);

    outw(DDR_BASE + 0x00 , 0x080b9d03);
    outw(DDR_BASE + 0x04 , 0x00000001);
    do {
        val = inw(DDR_BASE + 0x04);
    } while ((val & 0x100) == 0);
    
	// Reset Dphy after FTDDR3030 init OK
	val = inw(SCU_EXTREG_PA_BASE+0x080);
	outw( SCU_EXTREG_PA_BASE+0x080 , val | 0x20000000 );
	delay_ms(1);
	outw( SCU_EXTREG_PA_BASE+0x080 , val & 0xdfffffff );
	delay_ms(1);
}

#endif