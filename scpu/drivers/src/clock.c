#include "clock.h"
#include "scu_reg.h"
#include "scu_extreg.h"
#include "DrvPWMTMR010.h"
#include "dbg.h"

/* PLL 3/4/5
VCOOUT = FREF * (ns / ms)
CKOUT1 = VCOOUT / ps / 2
CKOUT2 = VCOOUT / ps / 8
if FREF = 12MHz,
CKOUT1 = 12 * 120 /2 = 720
CKOUT2 = 12 * 120 /8 = 180
csirx0_vc0_pclk = 720 / (0x1d+1) = 720/30=24
csirx0_csi_pclk = 720 / (0x1d+1) = 720/30=24
csirx0_TxEscClk = 180 / (0x4+1) = 36
*/ 

//#define DEBUG_PLL_CLOCK
#define SCU_EXTREG_PRINT(__desc, __regtype, __symbol) \
    _DSG(LOG_INFO, "%s %s %x", __desc, #__symbol, SCU_EXTREG_##__regtype##_GET##_##__symbol())

#define DEBUG_CLOCK(__node, __clock_val) { \
        if (clock_val) { \
            DSG("%s name=%s is_enabled=%d ns=%u ms=%u ps=%u div=%u %x", \
            __func__, node->name, node->is_enabled, \
            __clock_val->u.pll.ns, \
            __clock_val->u.pll.ms, \
            __clock_val->u.pll.ps, \
            __clock_val->u.pll.div, \
            (*((u32*)__clock_val))); \
        } else { \
            DSG("%s name=%s is_enabled=%d %x", \
            __func__, node->name, node->is_enabled, (*((u32*)__clock_val))); \
        } \
    }

#define _clock_ref_add(__node) {  ++__node->ref_cnt; }
#define _clock_ref_sub(__node) {  if (__node->ref_cnt > 0) --__node->ref_cnt; }

static bool_t osc_is_30m = FALSE;
static u16 T_OSC_in = 12;
static u16 osc_div_1n2 = 0;
static u16 pll0_out = 0;
static const u16 pll0_ms = 1;
static const u16 pll0_ns = 50;
static const u16 pll0_F = 3;

/* pll1 nodes */
struct clock_node clock_node_pll1 = { .name = "pll1" };
struct clock_node clock_node_pll1_out = { .name = "pll1o" };
/* pll2 nodes */
struct clock_node clock_node_pll2 = { .name = "pll2" };
struct clock_node clock_node_pll2_out = { .name = "pll2o" };
/* pll3 nodes */
struct clock_node clock_node_pll3 = { .name = "pll3" };
struct clock_node clock_node_pll3_out1 = { .name = "pll3o1" };
struct clock_node clock_node_pll3_out2 = {.name = "pll3o2" };
struct clock_node clock_node_csirx0_hs_csi = { .name = "pll3csirx0csi" };
struct clock_node clock_node_csirx0_hs_vc0 = { .name = "pll3csirx0vc0" };
struct clock_node clock_node_csirx0_lp = { .name = "pll3csirx0lp" };
/* pll4 nodes */
struct clock_node clock_node_pll4 = { .name = "pll4" };
struct clock_node clock_node_pll4_out1 = { .name = "pll4o1" };
struct clock_node clock_node_pll4_fref_pll0 = { .name = "pll4frefpll0" };
/* pll5 nodes */
struct clock_node clock_node_pll5 = { .name = "pll5" };
struct clock_node clock_node_pll5_out1 = { .name = "pll5o1" };
struct clock_node clock_node_pll5_out2 = { .name = "pll5o2" };
struct clock_node clock_node_lcdc = { .name = "pll5lcclk" };


extern void delay_ms(int msec);

static int _clock_set_pll1(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_PLL1_SETTING_SET_en((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_PLL1_SETTING_SET_en(0);
    }
    
    delay_ms(1);
            
    return ret;
}

static int _clock_set_pll1_out(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_EN0_SET_pll1_out((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_CLK_EN0_SET_pll1_out(0);
    }
    
    delay_ms(1);
    
    return ret;
}

static int _clock_set_pll2(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_PLL2_SETTING_SET_en((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_PLL2_SETTING_SET_en(0);
    }
    
    delay_ms(1);
    
    return ret;
}

static int _clock_set_pll2_out(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_EN0_SET_pll2_out((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_CLK_EN0_SET_pll2_out(0);
    }
    
    delay_ms(1);
    
    return ret;
}

static int _clock_set_pll3(struct clock_node *node, struct clock_value *clock_val)
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_PLL3_SETTING_SET_en((*((u32*)clock_val) > 0));
        delay_ms(1);
        SCU_EXTREG_PLL3_SETTING_SET_ms(clock_val->u.pll.ms);
        SCU_EXTREG_PLL3_SETTING_SET_ns(clock_val->u.pll.ns);
        SCU_EXTREG_PLL3_SETTING_SET_ps(clock_val->u.pll.ps);
    } else {
        SCU_EXTREG_PLL3_SETTING_SET_en(0);
    }
   
    delay_ms(1);
    
    return ret;
}

static int _clock_set_pll3_out1(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_EN0_SET_pll3_out1((*((u32*)clock_val) > 0));    
        delay_ms(1);
        SCU_EXTREG_CLK_EN0_SET_pll3_out1(1);
    } else {
        SCU_EXTREG_CLK_EN0_SET_pll3_out1(0);
    }
    
    delay_ms(1);
    
    return ret;
}

static int _clock_set_pll3_out2(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_EN0_SET_pll3_out2((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_CLK_EN0_SET_pll3_out2(0);
    }
    
    delay_ms(1);
    
    return ret;
}

static int _clock_set_csirx0_hs_csi(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_DIV1_SET_csirx0_csi(clock_val->u.pll.div);
        delay_ms(1);
        SCU_EXTREG_CLK_EN1_SET_csirx0_csi(1);
    } else {
        SCU_EXTREG_CLK_EN1_SET_csirx0_csi(0);
    }

    delay_ms(1);

    return ret;
}


static int _clock_set_csirx0_hs_vc0(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_DIV1_SET_csirx0_vc0(clock_val->u.pll.div);
        delay_ms(1);
        SCU_EXTREG_CLK_EN1_SET_csirx0_vc0(1);
    } else {
        SCU_EXTREG_CLK_EN1_SET_csirx0_vc0(0);
    }

    delay_ms(1);

    return ret;
}

static int _clock_set_csirx0_lp(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_DIV1_SET_csirx0_TxEscClk(clock_val->u.pll.div);
        delay_ms(1);
        SCU_EXTREG_CLK_EN1_SET_csirx0_TxEscClk(1);
    } else {
        SCU_EXTREG_CLK_EN1_SET_csirx0_TxEscClk(0);
    }
    
    delay_ms(1);
    
    return ret;
}

static int _clock_set_pll4(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_PLL4_SETTING_SET_en((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_PLL4_SETTING_SET_en(0);
    }
    
    delay_ms(1);
    
    return ret;
}

static int _clock_set_pll4_out1(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_EN0_SET_pll4_out1((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_CLK_EN0_SET_pll4_out1(0);
    }
    
    delay_ms(1);
    
    return ret;
}

static int _clock_set_pll4_fref_pll0(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_EN0_SET_pll4_fref_pll0((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_CLK_EN0_SET_pll4_fref_pll0(0);
    }

    delay_ms(1);

    return ret;
}

static int _clock_set_pll5(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {

        SCU_EXTREG_PLL5_SETTING_SET_en((*((u32*)clock_val) > 0));
        delay_ms(1);

        // SCU_EXTREG_PLL5_SETTING_SET_ms(clock_val->u.pll.ms);
        // SCU_EXTREG_PLL5_SETTING_SET_ns(clock_val->u.pll.ns);
        // SCU_EXTREG_PLL5_SETTING_SET_ps(clock_val->u.pll.ps);

    } else {
        SCU_EXTREG_PLL5_SETTING_SET_en(0);
    }
    
    delay_ms(1);

    return ret;
}

static int _clock_set_pll5_out1(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_EN0_SET_pll5_out1((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_CLK_EN0_SET_pll5_out1(0);
    }

    delay_ms(1);

    return ret;
}

static int _clock_set_pll5_out2(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        SCU_EXTREG_CLK_EN0_SET_pll5_out2((*((u32*)clock_val) > 0));
    } else {
        SCU_EXTREG_CLK_EN0_SET_pll5_out2(0);
    }

    delay_ms(1);

    return ret;
}

static int _clock_set_lcdc(struct clock_node *node, struct clock_value *clock_val) 
{
    int ret = 0;

    DEBUG_CLOCK(node, clock_val);

    if (clock_val) {
        delay_ms(1);
        SCU_EXTREG_SWRST_SET_LCDC_resetn(1);
        delay_ms(1);
    // outw(SCU_EXTREG_SWRST_MASK1, 
    //      SCU_EXTREG_SWRST_MASK1_AResetn_u_FTLCDC210 |
    //      SCU_EXTREG_SWRST_MASK1_PRESETn_u_FTLCDC210 |
    //      SCU_EXTREG_SWRST_MASK1_TV_RSTn_FTLCDC210 |
    //      SCU_EXTREG_SWRST_MASK1_LC_SCALER_RSTn_FTLCDC210 |
    //      SCU_EXTREG_SWRST_MASK1_LC_RSTn_FTLCDC210);

        SCU_EXTREG_CLK_EN1_SET_LC_CLK(1);
        SCU_EXTREG_CLK_EN1_SET_LC_SCALER(1);

    } else {
        SCU_EXTREG_CLK_EN1_SET_LC_SCALER(0);
        SCU_EXTREG_CLK_EN1_SET_LC_CLK(0);
    }
    
    delay_ms(1);
    
    return ret;
}

inline struct clock_node * _find_child_head(struct clock_node *node) 
{
    struct clock_node *p = node; 
    while (p->child_next) p = p->child_next; 
    return p;
}

struct clock_node * _find_sibling_is_enabled(struct clock_node *node) 
{
    struct clock_node *p = NULL; 
    struct clock_node *next = node->child_head;
    while (next)
    {
        if (next->is_enabled) {
            p = next;
            break;
        }
        next = next->child_next;
    }

    return p;
}

static void _clock_node_register(struct clock_node *node, struct clock_node *parent, fn_set set) 
{
    node->is_enabled = FALSE;
    node->child_head = NULL;
    node->parent = parent;
    node->set = set;
    if (parent) {
        struct clock_node *child = parent->child_head;

        if (!parent->child_head) {
            parent->child_head = node;
            parent->child_next = node;
        } else 
            (_find_child_head(parent))->child_next = node;

        child = parent->child_head;
        while (child) {
            child = child->child_next;
        } 
    } 
}

void clock_mgr_init() 
{
    _clock_node_register(&clock_node_pll1, NULL, _clock_set_pll1);
    _clock_node_register(&clock_node_pll1_out, &clock_node_pll1, _clock_set_pll1_out);
    
    _clock_node_register(&clock_node_pll2, NULL, _clock_set_pll2);
    _clock_node_register(&clock_node_pll2_out, &clock_node_pll2, _clock_set_pll2_out);

    _clock_node_register(&clock_node_pll3, NULL, _clock_set_pll3);
    _clock_node_register(&clock_node_pll3_out1, &clock_node_pll3, _clock_set_pll3_out1);
    _clock_node_register(&clock_node_pll3_out2, &clock_node_pll3, _clock_set_pll3_out2);
    _clock_node_register(&clock_node_csirx0_hs_csi, &clock_node_pll3_out1, _clock_set_csirx0_hs_csi);
    _clock_node_register(&clock_node_csirx0_hs_vc0, &clock_node_pll3_out1, _clock_set_csirx0_hs_vc0);
    _clock_node_register(&clock_node_csirx0_lp, &clock_node_pll3_out2, _clock_set_csirx0_lp);
    
    _clock_node_register(&clock_node_pll4, NULL, _clock_set_pll4);
    _clock_node_register(&clock_node_pll4_out1, &clock_node_pll4, _clock_set_pll4_out1);
    _clock_node_register(&clock_node_pll4_fref_pll0, &clock_node_pll4, _clock_set_pll4_fref_pll0);

    _clock_node_register(&clock_node_pll5, NULL, _clock_set_pll5);
    _clock_node_register(&clock_node_pll5_out1, &clock_node_pll5, _clock_set_pll5_out1);
    _clock_node_register(&clock_node_pll5_out2, &clock_node_pll5, _clock_set_pll5_out2);
    _clock_node_register(&clock_node_lcdc, &clock_node_pll5_out1, _clock_set_lcdc);
    // for debug 
    // //clock_mgr_open(&clock_node_pll1_out);
    // //clock_mgr_close(&clock_node_pll1_out);
    // struct clock_value val;
    // val.u.enable = 1;

    // clock_mgr_open(&clock_node_csirx0_hs_csi, &val);
    // clock_mgr_open(&clock_node_csirx0_hs_vc0, &val);
    // //clock_mgr_open(&clock_node_csirx0_lp, &val);
    
    // //clock_mgr_close(&clock_node_csirx0_lp);
    // clock_mgr_close(&clock_node_csirx0_hs_vc0);
    // clock_mgr_close(&clock_node_csirx0_hs_csi);

    // DSG("size of my want =%d", sizeof(struct clock_node));
}

void clock_mgr_open(struct clock_node *node, struct clock_value *clock_val) 
{
    //DSG("%s node->name=%s", __func__, node->name);    
    if (node) {
        if (node->parent) {
            clock_mgr_open(node->parent, clock_val);
        }

        if (FALSE == node->is_enabled) {
            node->set(node, clock_val);
            node->is_enabled = TRUE;
        }
    }
}

void clock_mgr_close(struct clock_node *node) 
{
    //DSG("%s node->name=%s", __func__, node->name);        
    if (node) {
        struct clock_node *sibling;
        node->set(node, NULL);
        node->is_enabled = FALSE;
    
        sibling = _find_sibling_is_enabled(node->parent);
        if (!sibling)
            clock_mgr_close(node->parent);   
    }
}

void clock_mgr_set_scuclkin(enum scuclkin_type type, bool_t enable) 
{
//    SCU_REG_PLL_CTRL_SET_CLKIN_MUX(type);
//    SCU_REG_PLL_CTRL_PLLEN(enable);
    u32 val = ((type << SCU_REG_PLL_CTRL_CLKIN_MUX_BIT_START) |
              ((enable) ? (SCU_REG_PLL_CTRL_PLLEN) : (0)));
    masked_outw(SCU_REG_PLL_CTRL, val, SCU_REG_PLL_CTRL_CLKIN_MUX_MASK);
    
    osc_is_30m = ((scuclkin_pll0div4 == type) ? (TRUE) : (FALSE));
    osc_div_1n2 = (!osc_is_30m) ? (T_OSC_in) : (T_OSC_in >> 1);
    pll0_out = osc_div_1n2 * pll0_ns / pll0_ms;
    //switch (pll0_F) {
        pll0_out = pll0_out >> (3 - pll0_F);
    //}

    delay_ms(2);
    //DSG("%s osc_div_1n2=%d pll0_out=%d", __func__, osc_div_1n2, pll0_out);
}

void clock_mgr_set_muxsel(u32 flags)
{
    u32 val = 0;
    if (CLOCK_MUXSEL_NCPU_TRACECLK_FROM_SCPU_TRACECLK == (flags & CLOCK_MUXSEL_NCPU_TRACECLK_MASK))
        val |= BIT14;
    if (CLOCK_MUXSEL_SCPU_TRACECLK_SRC_PLL0DIV2 == (flags & CLOCK_MUXSEL_SCPU_TRACECLK_MASK))
        val |= BIT13;
    if (CLOCK_MUXSEL_CSIRX1_CLK_PLL3 == (flags & CLOCK_MUXSEL_CSIRX1_CLK_MASK))
        val |= BIT12;

    switch (flags & CLOCK_MUXSEL_CSIRX1_CLK_MASK) {
        case CLOCK_MUXSEL_NPU_CLK_PLL0:
            val |= BIT9 | BIT8;
            break;
        case CLOCK_MUXSEL_NPU_CLK_PLL5:
            val |= BIT9;
            break;
        case CLOCK_MUXSEL_NPU_CLK_PLL4:
            val |= BIT8;
            break;                   
    }
    if (CLOCK_MUXSEL_PLL4_FREF_OSC == (flags & CLOCK_MUXSEL_PLL4_MASK))
        val |= BIT6;
    if (CLOCK_MUXSEL_UART_0_IRDA_UCLK_IRDA == (flags & CLOCK_MUXSEL_UART_0_IRDA_UCLK_MASK))
        val |= BIT4;        
}

u32  clock_mgr_calculate_clockout(enum pll_id id, u16 ms, u16 ns, u16 F_ps)
{
    u32 fref = 0;
    u32 ckout = 0;

    switch (id) {
    //case pll_0:
    case pll_1:
    case pll_2:
        fref = T_OSC_in;
        ckout = (fref * ns / ms) >> (3 - F_ps);
        break;
    case pll_3:
    case pll_5:
        fref = osc_div_1n2;
        ckout = (fref * ns / ms) / F_ps;
        break;
    case pll_4:
        if (0 == SCU_EXTREG_CLK_MUX_SEL_GET_pll4_fref()) {
            fref = (osc_div_1n2 * pll0_ns / pll0_ms) >> (3 - pll0_F) / (1 + SCU_EXTREG_CLK_DIV0_GET_pll4_fref_pll0());
        } else
            fref = osc_div_1n2;
        ckout = (fref * ns / ms) / F_ps;
        break;
    default:;
    }
    
    return ckout;
}

#define DEBUG_PLL_CLOCK
#ifndef DEBUG_PLL_CLOCK
void debug_pll_clock(void) {}
#else
void debug_pll_clock() {
   DSG("SSCU_EXTREG_CLK_MUX_SEL_GET_npu_clk()=%x", SCU_EXTREG_CLK_MUX_SEL_GET_npu_clk());
   SCU_EXTREG_CLK_MUX_SEL_SET_npu_clk(0);
   DSG("SSCU_EXTREG_CLK_MUX_SEL_GET_npu_clk()=%x", SCU_EXTREG_CLK_MUX_SEL_GET_npu_clk());
   SCU_EXTREG_CLK_MUX_SEL_SET_npu_clk(1);
   DSG("SSCU_EXTREG_CLK_MUX_SEL_GET_npu_clk()=%x", SCU_EXTREG_CLK_MUX_SEL_GET_npu_clk());
   SCU_EXTREG_CLK_MUX_SEL_SET_npu_clk(2);
   DSG("SSCU_EXTREG_CLK_MUX_SEL_GET_npu_clk()=%x", SCU_EXTREG_CLK_MUX_SEL_GET_npu_clk());    
   SCU_EXTREG_CLK_MUX_SEL_SET_npu_clk(3);
   DSG("SSCU_EXTREG_CLK_MUX_SEL_GET_npu_clk()=%x", SCU_EXTREG_CLK_MUX_SEL_GET_npu_clk());        

    u32 clken0 = inw(SCU_EXTREG_PA_BASE + 0x14);
    u32 clken1 = inw(SCU_EXTREG_PA_BASE + 0x18);
    u32 clken2 = inw(SCU_EXTREG_PA_BASE + 0x1C);
    u32 clkms = inw(SCU_EXTREG_PA_BASE + 0x20);
    u32 clkdiv0 = inw(SCU_EXTREG_PA_BASE + 0x24);
    u32 clkdiv1 = inw(SCU_EXTREG_PA_BASE + 0x28);
    u32 clkdiv2 = inw(SCU_EXTREG_PA_BASE + 0x2c);
    u32 clkdiv3 = inw(SCU_EXTREG_PA_BASE + 0x30);
    u32 clkdiv4 = inw(SCU_EXTREG_PA_BASE + 0x34);
    DSG("clock enable register 0 =0x%x", clken0);
    DSG("clock enable register 1 =%x", clken1);
    DSG("clock enable register 2 =%x", clken2);
    DSG("clock mux selection register =%x", clkms);
    DSG("clock divider register 0 =%x", clkdiv0);
    DSG("clock divider register 1 =%x", clkdiv1);
    DSG("clock divider register 2 =%x", clkdiv2);
    DSG("clock divider register 3 =%x", clkdiv3);
    DSG("clock divider register 4 =%x", clkdiv4);

    SCU_EXTREG_PRINT("PLL 0 setting", PLL0_SETTING, ns);
    SCU_EXTREG_PRINT("PLL 0 setting", PLL0_SETTING, ms);
    SCU_EXTREG_PRINT("PLL 0 setting", PLL0_SETTING, cc);
    SCU_EXTREG_PRINT("PLL 0 setting", PLL0_SETTING, f);
    SCU_EXTREG_PRINT("PLL 0 setting", PLL0_SETTING, en);

    SCU_EXTREG_PRINT("PLL 1 setting", PLL1_SETTING, ns);
    SCU_EXTREG_PRINT("PLL 1 setting", PLL1_SETTING, ms);
    SCU_EXTREG_PRINT("PLL 1 setting", PLL1_SETTING, cc);
    SCU_EXTREG_PRINT("PLL 1 setting", PLL1_SETTING, f);
    SCU_EXTREG_PRINT("PLL 1 setting", PLL1_SETTING, en);
 
    SCU_EXTREG_PRINT("PLL 2 setting", PLL2_SETTING, ns);
    SCU_EXTREG_PRINT("PLL 2 setting", PLL2_SETTING, ms);
    SCU_EXTREG_PRINT("PLL 2 setting", PLL2_SETTING, cc);
    SCU_EXTREG_PRINT("PLL 2 setting", PLL2_SETTING, f);
    SCU_EXTREG_PRINT("PLL 2 setting", PLL2_SETTING, en);
 
    SCU_EXTREG_PRINT("PLL 3 setting", PLL3_SETTING, ps);
    SCU_EXTREG_PRINT("PLL 3 setting", PLL3_SETTING, ns);
    SCU_EXTREG_PRINT("PLL 3 setting", PLL3_SETTING, ms);
    SCU_EXTREG_PRINT("PLL 3 setting", PLL3_SETTING, is);
    SCU_EXTREG_PRINT("PLL 3 setting", PLL3_SETTING, rs);
    SCU_EXTREG_PRINT("PLL 3 setting", PLL3_SETTING, en);	
 
    SCU_EXTREG_PRINT("PLL 4 setting", PLL4_SETTING, ps);
    SCU_EXTREG_PRINT("PLL 4 setting", PLL4_SETTING, ns);
    SCU_EXTREG_PRINT("PLL 4 setting", PLL4_SETTING, ms);
    SCU_EXTREG_PRINT("PLL 4 setting", PLL4_SETTING, is);
    SCU_EXTREG_PRINT("PLL 4 setting", PLL4_SETTING, rs);
    SCU_EXTREG_PRINT("PLL 4 setting", PLL4_SETTING, en);	
 
    SCU_EXTREG_PRINT("PLL 5 setting", PLL5_SETTING, ps);
    SCU_EXTREG_PRINT("PLL 5 setting", PLL5_SETTING, ns);
    SCU_EXTREG_PRINT("PLL 5 setting", PLL5_SETTING, ms);
    SCU_EXTREG_PRINT("PLL 5 setting", PLL5_SETTING, is);
    SCU_EXTREG_PRINT("PLL 5 setting", PLL5_SETTING, rs);
    SCU_EXTREG_PRINT("PLL 5 setting", PLL5_SETTING, en);	
 
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, ncpu_traceclk);
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, scpu_traceclk);
    
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, ncpu_fclk_src);
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, pll4_fref_pll0);
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, pll5_out2);
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, pll5_out1);
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, pll4_out1);
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, pll3_out2);
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, pll3_out1);
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, pll2_out);
    SCU_EXTREG_PRINT("clock enable 0", CLK_EN0, pll1_out);
    
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, spi_clk);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, npu);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, adcclk);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, wdt_extclk);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, sdclk);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, TxHsPllRefClk);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, tx_EscClk);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, csitx_dsi);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, csitx_csi);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, csirx1_TxEscClk);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, csirx1_csi);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, csirx1_vc0);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, csirx0_TxEscClk);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, csirx0_csi);    
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, csirx0_vc0);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, LC_SCALER);
    SCU_EXTREG_PRINT("clock enable 1", CLK_EN1, LC_CLK);
    
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, tmr1_extclk3);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, tmr1_extclk2);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, tmr1_extclk1);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, tmr0_extclk3);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, tmr0_extclk2);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, tmr0_extclk1);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, pwm_extclk6);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, pwm_extclk5);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, pwm_extclk4);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, pwm_extclk3);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, pwm_extclk2);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, pwm_extclk1);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, uart1_3_fref);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, uart1_2_fref);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, uart1_1_fref);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, uart1_0_fref);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, uart0_fref);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, ssp1_1_sspclk);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, ssp1_0_sspclk);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, ssp0_1_sspclk);
    SCU_EXTREG_PRINT("clock enable 2", CLK_EN2, ssp0_0_sspclk);

    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, ncpu_traceclk);
    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, scpu_traceclk_src);
    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, csirx1_clk);
    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, npu_clk);
    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, pll4_fref);
    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, uart_0_irda_uclk);
    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, ssp1_1_sspclk);
    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, ssp1_0_sspclk);
    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, ssp0_1_sspclk);
    SCU_EXTREG_PRINT("clock mux selection", CLK_MUX_SEL, ssp0_0_sspclk);
    
    SCU_EXTREG_PRINT("clock divider 0", CLK_DIV0, ncpu_fclk);
    SCU_EXTREG_PRINT("clock divider 0", CLK_DIV0, sdclk2x);
    SCU_EXTREG_PRINT("clock divider 0", CLK_DIV0, spi_clk);
    SCU_EXTREG_PRINT("clock divider 0", CLK_DIV0, pll4_fref_pll0);
    
    SCU_EXTREG_PRINT("clock divider 1", CLK_DIV1, csirx0_TxEscClk);
    SCU_EXTREG_PRINT("clock divider 1", CLK_DIV1, csirx0_csi);
    SCU_EXTREG_PRINT("clock divider 1", CLK_DIV1, csirx0_vc0);
    SCU_EXTREG_PRINT("clock divider 1", CLK_DIV1, LC_CLK);
    SCU_EXTREG_PRINT("clock divider 1", CLK_DIV1, LC_SCALER_CLK);   
    
    SCU_EXTREG_PRINT("clock divider 2", CLK_DIV2, npu_clk_pll5);
    SCU_EXTREG_PRINT("clock divider 2", CLK_DIV2, npu_clk_pll4);
    SCU_EXTREG_PRINT("clock divider 2", CLK_DIV2, npu_clk_pll0);

    SCU_EXTREG_PRINT("clock divider 3", CLK_DIV3, ssp0_1_sspclk_slv);
    SCU_EXTREG_PRINT("clock divider 3", CLK_DIV3, ssp0_1_sspclk_mst);
    SCU_EXTREG_PRINT("clock divider 3", CLK_DIV3, ssp0_0_sspclk_slv);
    SCU_EXTREG_PRINT("clock divider 3", CLK_DIV3, ssp0_0_sspclk_mst);
    
    SCU_EXTREG_PRINT("clock divider 4", CLK_DIV4, ssp1_1_sspclk_slv);
    SCU_EXTREG_PRINT("clock divider 4", CLK_DIV4, ssp1_1_sspclk_mst);
    SCU_EXTREG_PRINT("clock divider 4", CLK_DIV4, ssp1_0_sspclk_slv);
    SCU_EXTREG_PRINT("clock divider 4", CLK_DIV4, ssp1_0_sspclk_mst);
    
    SCU_EXTREG_PRINT("clock divider 6", CLK_DIV6, uart1_3_fref);
    SCU_EXTREG_PRINT("clock divider 6", CLK_DIV6, uart1_2_fref);
    SCU_EXTREG_PRINT("clock divider 6", CLK_DIV6, uart1_1_fref);
    SCU_EXTREG_PRINT("clock divider 6", CLK_DIV6, uart1_0_fref);       
    SCU_EXTREG_PRINT("clock divider 6", CLK_DIV6, uart0_fref);
    SCU_EXTREG_PRINT("clock divider 6", CLK_DIV6, uart0_fir_fref);

    SCU_EXTREG_PRINT("clock divider 7", CLK_DIV7, ncpu_traceclk_div);
    SCU_EXTREG_PRINT("clock divider 7", CLK_DIV7, scpu_traceclk_div);
    SCU_EXTREG_PRINT("clock divider 7", CLK_DIV7, csirx1_TxEscClk_pll3);
    SCU_EXTREG_PRINT("clock divider 7", CLK_DIV7, csi1_csi_pll3);       
    SCU_EXTREG_PRINT("clock divider 7", CLK_DIV7, csi1_vc0_pll3);

    SCU_EXTREG_PRINT("csirx ctrl 0", CSIRX_CTRL0, apb_rst_n);
    SCU_EXTREG_PRINT("csirx ctrl 0", CSIRX_CTRL0, pwr_rst_n);
    SCU_EXTREG_PRINT("csirx ctrl 0", CSIRX_CTRL0, sys_rst_n);
    SCU_EXTREG_PRINT("csirx ctrl 0", CSIRX_CTRL0, ClkLnEn);
    SCU_EXTREG_PRINT("csirx ctrl 0", CSIRX_CTRL0, Enable);
    
    SCU_EXTREG_PRINT("csirx ctrl 1", CSIRX_CTRL1, apb_rst_n);
    SCU_EXTREG_PRINT("csirx ctrl 1", CSIRX_CTRL1, pwr_rst_n);
    SCU_EXTREG_PRINT("csirx ctrl 1", CSIRX_CTRL1, sys_rst_n);
    SCU_EXTREG_PRINT("csirx ctrl 1", CSIRX_CTRL1, ClkLnEn);
    SCU_EXTREG_PRINT("csirx ctrl 1", CSIRX_CTRL1, Enable);   
}
#endif
