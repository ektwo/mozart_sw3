#ifndef __DRV_MIPI_H__
#define __DRV_MIPI_H__

// CSIRX MIPI Control Registrer
#define MIPI_VIDR_ADDR      0x00
#define MIPI_DIDR_ADDR      0x01
#define MIPI_CR_ADDR        0x04 //Control Register
#define MIPI_VSCR_ADDR      0x05 //DPI V Sync Control Register
#define MIPI_ECR_ADDR       0x06
#define MIPI_TCNR_ADDR      0x08
#define MIPI_HRTVR_ADDR     0x0A //HS RX Timeout Value Register
#define MIPI_FIUR_ADDR      0x0B 
#define MIPI_ITR_ADDR       0x12 //Initialization Timer Register
#define MIPI_VSTR0_ADDR     0x14 //DPI VC0 V Sync Timing Register
#define MIPI_HSTR0_ADDR     0x15 //DPI VC0 H Sync Timing Register
#define MIPI_VSTR1_ADDR     0x16
#define MIPI_HSTR1_ADDR     0x17
#define MIPI_VSTR2_ADDR     0x18
#define MIPI_HSTR2_ADDR     0x19
#define MIPI_VSTR3_ADDR     0x1A
#define MIPI_HSTR3_ADDR     0x1B
#define MIPI_MCR_ADDR       0x1C //DPI Mapping Control Register
#define MIPI_VSTER_ADDR     0x1E
#define MIPI_HPNR_ADDR      0x20 //DPI Horizontal Pixel Number
#define MIPI_PECR_ADDR      0x28 //PPI Enable Control Register
#define MIPI_DLMR_ADDR      0x2A
#define MIPI_CSIERR_ADDR    0x30
#define MIPI_INTSTS_ADDR    0x33
#define MIPI_ESR_ADDR       0x34
#define MIPI_DPISR_ADDR     0x38
#define MIPI_INTER_ADDR     0x3C
#define MIPI_FFR_ADDR       0x3D
#define MIPI_DPCMR_ADDR     0x48
#define MIPI_FRR_ADDR       0x4C
#define MIPI_PFTR_ADDR      0x50
#define MIPI_PUDTR_ADDR     0x58
#define MIPI_FRCR_ADDR      0x80
#define MIPI_FNR_ADDR       0x88
#define MIPI_BPGR_ADDR      0x90

extern void mipi_init(void);
#endif
