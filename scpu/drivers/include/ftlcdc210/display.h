#ifndef DISPLAY_H
#define DISPLAY_H

#define RGB24(r, g, b)		((((unsigned char)r)<<16) | (((unsigned char)g)<<8) | ((unsigned char)b))
#define RGB565(r, g, b)     (((((unsigned char)r)>>3)<<11) | ((((unsigned char)g)>>2)<<5) | (((unsigned char)b)>>3))

void RGB_to_YCbCr(unsigned char r, unsigned char g, unsigned char b, unsigned char *y, unsigned char *cb, unsigned char *cr);
void Display_Clear(struct faradayfb_info *fbi, int frame_no, unsigned char r, unsigned char g, unsigned char b);
void Display_drawSquare( struct faradayfb_info *fbi, int frame_no, unsigned int thickness, unsigned char r, unsigned char g, unsigned char b );
void Display_ColorBar(struct faradayfb_info *fbi, int frame_no);
int Load_Image(struct faradayfb_info *fbi, int frame_no, char *filename);
void Display_DrawRect( struct faradayfb_info *fbi, int frame_no, unsigned int thickness,unsigned int xs,unsigned int ys,unsigned int xe,unsigned int ye ,unsigned char r, unsigned char g, unsigned char b , unsigned char fill);

extern char cursor_32x32[];

#endif
