#ifndef __FLCD200_H
#define __FLCD200_H

// --------------------------------------------------------------------
//	waiting to do:	some code moving to display.h, then display.h add a data structure
// --------------------------------------------------------------------
#include "lcd_config.h"
#include "panel.h"

#define FRAME_NUM				4

#define REG_FuncEnable		0x0
	#define DitherEn	(1<<6)
	#define ScalerEn	(1<<5)
	#define OSDEn		(1<<4)
	#define EnYCbCr		(1<<3)
	#define EnYCbCr420	(1<<2)
	
#define REG_PixelParam		0x4
	#define BppFifo_Mask	(0x7<<0)
	#define BppFifo_1bpp	(0x0<<0)
	#define BppFifo_2bpp	(0x1<<0)
	#define BppFifo_4bpp	(0x2<<0)
	#define BppFifo_8bpp	(0x3<<0)
	#define BppFifo_16bpp	(0x4<<0)
	#define BppFifo_24bpp	(0x5<<0)
	
	#define RGBTYPE_Mask	(0x3<<7)
	#define RGBTYPE_RGB565	(0x0<<7)
	#define RGBTYPE_RGB555	(0x1<<7)
	#define RGBTYPE_RGB444  (0x2<<7)
	
	#define Vcomp_Mask			(0x3<<9)		// generate interrupt at:
	#define Vcomp_Sync			(0x0<<9)		// ==> start of vertical sync
	#define Vcomp_backPorch		(0x1<<9)		// ==> start of vertical back porch
	#define Vcomp_activeImg 	(0x2<<9)		// ==> start of vertical active image
	#define Vcomp_frontPorch	(0x3<<9)		// ==> start of vertical front porch
	
	#define DitherType_Mask (0x3<<12)
	#define DitherType_565  (0x0<<12)
	#define DitherType_555  (0x1<<12)
	#define DitherType_444  (0x2<<12)

#define REG_IntEnable		0x8
	#define IntBusErrEn		(0x1<<3)
	#define IntVstatusEn	(0x1<<2)
	#define IntNxtBaseEn	(0x1<<1)
	#define IntFIFOUdnEn	(0x1<<0)
	
	
#define REG_GPIO			0x50

#define PALETTE_RAM			0xa00

// --------------------------------------------------------------------
//              faradayfb_info->mode
// --------------------------------------------------------------------
#define MODE_RGB565						0
#define MODE_RGB555						1
#define MODE_RGB444						2
#define MODE_RGB24                      3
#define MODE_YCbCr422                   4
#define MODE_YCbCr420                   5
#define MODE_PALETTE_8                  6
#define MODE_PALETTE_4                  7
#define MODE_PALETTE_2                  8
#define MODE_PALETTE_1                  9

// --------------------------------------------------------------------
//				faradayfb_info->endian
// --------------------------------------------------------------------
#define LBLP					0
#define BBBP					1
#define LBBP					2

#define VESA_800x600 0
#define VESA_1024x768 1
#define VESA_640x480 2
#define CEA_720p	3
#define CEA_1080p	4
#define MZT_480p	5
#define IP_IRQ_NUM					4		
/*
	#define ClrBusErr_IRQ			3		// irq[3] = ClrBusErr
	#define ClrVstatus_IRQ			2		// irq[2] = ClrVstatus
	#define ClrNxtBase_IRQ			1		// irq[1] = ClrNxtBase
	#define ClrFIFOUdn_IRQ			0		// irq[0] = ClrFIFOUdn
*/
	#define ClrVstatus_IRQ			0
	#define ClrBaudP_IRQ			1
	#define ClrFIFOUdn_IRQ		2
	#define ClrMemErr_IRQ			3



enum { RGB, BGR };

struct RGBColor
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
};

struct faradayfb_info
{
    unsigned long io_base;                  /// IO base of LCDC
    int irq[IP_IRQ_NUM];
    int panel_width;				// panel's width
    int panel_height;
    int frame_width[FRAME_NUM];				// frame �� width 	(frame_width = panel_width*scaling_factor)
    int frame_height[FRAME_NUM];			// frame �� height
    void *arg;
    int mode;                                   /// MODE_RGB565, MODE_RGB555,...
    int endian;									/// LBLP, BBBP, ...
    struct LcdMTypeTag *lcd_param;
    u32 input_clock;

    int frame_size[FRAME_NUM];
    unsigned int pFrameBuffer[FRAME_NUM];    	// ybase for 420, 422 mode
    int frameBuf_size[FRAME_NUM];				// the memory size actually allocate to pFrame[i]
    unsigned int ubase;							// ubase & vbase is for 420 mode
    unsigned int vbase;

    int pip_num;			/// 0 -> off, 1 -> single PiP window, 2 -> double PiP window
    int isPOP;				/// whether is POP mode
    int ImScalDown[4];		/// pop's four window's scaldow
    int pip_width[FRAME_NUM];		/// pip's width	(the value setting to offset 308, 30c)
    int pip_height[FRAME_NUM];		/// pip's height
    
    int scalar_width;		/// frame_width after scalar on
    int scalar_height;		/// frame_height after scalar on
    int frame_num;			/// currently there how many window (frame buffer)
    ///int gpio_ctl;
    
    // --------------------------------------------------------------------
    //	for display.c
    // --------------------------------------------------------------------
    int idx_16tbl[2];			// pixel position when LBBP
	int idx_8tbl[4];
	int mask_4tbl[8];			// palette 4
	int shift_4tbl[8];
	int mask_2tbl[16];			// palette 2
	int shift_2tbl[16];
    
    // --------------------------------------------------------------------
    //	for palette
    // --------------------------------------------------------------------
    int entry_num;
    int last_entry;		// last added entry
    struct RGBColor entry[256];
};



// --------------------------------------------------------------------
//		function prototype
// --------------------------------------------------------------------
#ifndef readl
	#define readl(addr)                         (*(volatile unsigned int *)(addr))
	#define writel(val, addr)            		(*(volatile unsigned int *)(addr) = (val))
#endif

#if 0
	#define flcd_readl(fbi, offset)				readl(FIE3420_LCDC_BASE+offset)
	#define flcd_writel(fbi, offset, value)		writel(value, FIE3420_LCDC_BASE+offset)
#else
	#define flcd_readl(fbi, offset)				readl((fbi)->io_base+(offset))
	#define flcd_writel(fbi, offset, value)		writel((value), (fbi)->io_base+(offset))
#endif

// *reg &= val
#define flcd_andl(fbi, offset, value)			flcd_writel(fbi, offset, flcd_readl(fbi, offset)&value)
// *reg |= val
#define flcd_orl(fbi, offset, value)			flcd_writel(fbi, offset, flcd_readl(fbi, offset)|value)




void disable_lcd_controller(struct faradayfb_info *fbi);
void enable_lcd_controller(struct faradayfb_info *fbi);
void LCD_ScreenOn(struct faradayfb_info *fbi);
void LCD_ScreenOff(struct faradayfb_info *fbi);
void CSTN_On(struct faradayfb_info *fbi);
void CSTN_Off(struct faradayfb_info *fbi);
void STN_On(struct faradayfb_info *fbi);
void STN_Off(struct faradayfb_info *fbi);
void STN_160x80_On(struct faradayfb_info *fbi);
void Dithering(struct faradayfb_info *fbi, int enable, unsigned int type );
int SetDivNo(struct faradayfb_info *fbi, unsigned int DivNo);
int SetFrameBase(struct faradayfb_info *fbi, unsigned short image, unsigned short frame, unsigned int pFrameBuffer );
void alloc_frameBuffer(struct faradayfb_info *fbi);
void install_palette(struct faradayfb_info *fbi, int entry, unsigned int color);
void LCD_SetInputMode(struct faradayfb_info *fbi, int mode);
void LCD_SetEndian(struct faradayfb_info *fbi, unsigned char endian );
int Init_LCD(struct faradayfb_info *fbi, unsigned int io_base, int irq[IP_IRQ_NUM], int panel_id, int mode);
void SetEndian(struct faradayfb_info *fbi, unsigned char endian );
void SetYCbCr(struct faradayfb_info *fbi, int type );
void SetBGRSW( struct faradayfb_info *fbi, unsigned char choice );
void OSD_Off(struct faradayfb_info *fbi);
int PiP_On(struct faradayfb_info *fbi, unsigned int pipNum);
void PiP_Off(struct faradayfb_info *fbi);
void PiP_Pos(struct faradayfb_info *fbi, unsigned int which, int  HPos, int VPos);
void PiP_Blending(struct faradayfb_info *fbi, unsigned int on, int blend1, int blend2);
void PiP_Dim(struct faradayfb_info *fbi, unsigned int which, int HDim, int VDim);
void PoP_On(struct faradayfb_info *fbi);
void PoP_Off(struct faradayfb_info *fbi);
void PoP_ImageScalDown(struct faradayfb_info *fbi, unsigned int im0, unsigned int im1, unsigned int im2, unsigned int im3 );


#endif



