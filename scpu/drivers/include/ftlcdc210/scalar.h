#ifndef SCALAR_H
#define SCALAR_H


#define Interpolation_Nearly 		0				// Nearly bilinear mode
#define Interpolation_Threshold		1				// Threshold nearly bilinear mode
#define Interpolation_Most			2				// Most neighborhood mode


int Scalar_On(struct faradayfb_info *fbi, int frame_width, int frame_height, int mode);
void Scalar_Off(struct faradayfb_info *fbi);
void Scalar_SetInterpMode(struct faradayfb_info *fbi, int mode );
void Scalar_UPThreshold(struct faradayfb_info *fbi, unsigned int Hor_high_th, unsigned int Hor_low_th, unsigned int Ver_high_th, unsigned int Ver_low_th );


#endif
