#ifndef OSD_SIMPLE_H
#define OSD_SIMPLE_H

// --------------------------------------------------------------------
//	if the following define's value is change, OSDC_FontScal() need to care that may also need change
// --------------------------------------------------------------------
#define FONT_NORMAL					0			// size not change
#define FONT_ZOOMIN_2				1			// become double
#define FONT_ZOOMIN_3				2			// become 3 times
#define FONT_ZOOMIN_4				3			// become 4 times


#endif
