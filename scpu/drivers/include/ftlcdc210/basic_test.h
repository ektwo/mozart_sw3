#ifndef BASIC_TEST_H
#define BASIC_TEST_H

#include "flcd.h"

void animation_test(struct faradayfb_info *fbi, int frame_no);
int RegisterFile_Test(struct faradayfb_info *fbi);
void ColorBar_Test(struct faradayfb_info *fbi, int frame_no);
void Vertical_Int_Test(struct faradayfb_info *fbi);


#endif
