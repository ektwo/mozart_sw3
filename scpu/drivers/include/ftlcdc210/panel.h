#ifndef PANEL_H
#define PANEL_H

//#include <Misc/types.h>
#include "types.h"

// --------------------------------------------------------------------
//              Panel type ID
// --------------------------------------------------------------------
//#define PANEL_AUO_A036QN01_Parallel			0
//#define PANEL_AUO_A036QN01_Serial			1
//#define SHARP_LQ057Q3DC02					2
//#define SHARP_LQ084V1DG21					3
//#define PRIME_VIEW_PD035VX2					4
//#define KOROLA_NTSC_640						5
//#define KOROLA_NTSC_720						6
//#define KOROLA_PAL_640						7
//#define KOROLA_PAL_720						8
//#define SITRONIX_CSTN						9
//#define SITRONIX_STN						10
//#define SITRONIX_STN_mono					11
//#define TPO_TD070WGEC2						12			// TPO TD070WGEC2 parallel lcd panel
//#define HAMI_TV								13
//#define VESA_800x600_60Hz					14
//#define VESA_1024x768_60Hz					15
//#define VESA_640x480_60Hz					16

enum panel_type {
    PANEL_AUO_A036QN01_Parallel			= 0,
    PANEL_AUO_A036QN01_Serial			,
    SHARP_LQ057Q3DC02					,
    SHARP_LQ084V1DG21					,
    PRIME_VIEW_PD035VX2					,
    KOROLA_NTSC_640						,
    KOROLA_NTSC_720						,
    KOROLA_PAL_640						,
    KOROLA_PAL_720						,
    SITRONIX_CSTN						,
    SITRONIX_STN						,
    SITRONIX_STN_mono					,
    TPO_TD070WGEC2						,
    HAMI_TV								,
    VESA_800x600_60Hz					,
    VESA_1024x768_60Hz					,
    VESA_640x480_60Hz					,
    CEA_1280x720_60Hz                   ,
    CEA_1920x1080_60Hz                  ,  
    VESA_864x491_60Hz					,
    MZT_480x272_60Hz                    
};

typedef struct LcdMTypeTag
{
  	//int panel_id;
  	unsigned char *Descriptor;
  	unsigned int LCDEnable;				// 0x0
  	unsigned int PanelPixel;			// 0x4
  	unsigned int HorizontalTiming;		// 0x100
  	unsigned int VerticalTiming1;		// 0x104
  	unsigned int VerticalTiming2;		// 0x108
  	unsigned int Polarity;				// 0x10c
  	unsigned int SerialPanelPixel;		// 0x200
  	unsigned int CCIR656;
  	unsigned int CSTNPanelControl;
  	unsigned int CSTNPanelParam1;
  	int Width;
  	int Height;
  	int FrameRate;
  	u32 (*before_func)(void);			// return 0 ==> fail, other ==> clock (enable lcd 之前, 例: 設 pin mux)
  	int (*after_func)(void);			// return 0 ==> fail, 1 ==> success (enable lcd 之後, 例: tv encoder 的 i2c)
  	
} LCDMTYPE_T;

extern LCDMTYPE_T FLcdModule[];

#endif
