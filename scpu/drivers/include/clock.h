#ifndef __CLOCK_H__
#define __CLOCK_H__


#include <types.h>


#define CLOCK_MUXSEL_NCPU_TRACECLK_DEFAULT              0x10000000
#define CLOCK_MUXSEL_NCPU_TRACECLK_FROM_SCPU_TRACECLK   0x20000000
#define CLOCK_MUXSEL_NCPU_TRACECLK_MASK                 0x30000000
#define CLOCK_MUXSEL_SCPU_TRACECLK_SRC_PLL0DIV3         0x01000000
#define CLOCK_MUXSEL_SCPU_TRACECLK_SRC_PLL0DIV2         0x02000000
#define CLOCK_MUXSEL_SCPU_TRACECLK_MASK                 0x03000000
#define CLOCK_MUXSEL_CSIRX1_CLK_PLL5                    0x00100000
#define CLOCK_MUXSEL_CSIRX1_CLK_PLL3                    0x00200000
#define CLOCK_MUXSEL_CSIRX1_CLK_MASK                    0x00300000
#define CLOCK_MUXSEL_NPU_CLK_PLL4                       0x00010000
#define CLOCK_MUXSEL_NPU_CLK_PLL5                       0x00020000
#define CLOCK_MUXSEL_NPU_CLK_PLL0                       0x00040000
#define CLOCK_MUXSEL_NPU_CLK_MASK                       0x00070000
#define CLOCK_MUXSEL_PLL4_FREF_PLL0DIV                  0x00001000
#define CLOCK_MUXSEL_PLL4_FREF_OSC                      0x00002000
#define CLOCK_MUXSEL_PLL4_MASK                          0x00003000
#define CLOCK_MUXSEL_UART_0_IRDA_UCLK_UART              0x00000100
#define CLOCK_MUXSEL_UART_0_IRDA_UCLK_IRDA              0x00000200
#define CLOCK_MUXSEL_UART_0_IRDA_UCLK_MASK              0x00000300

//enum clock_mux_selection {
//    ncpu_traceclk_default = 0x10000000,
//    ncpu_traceclk_from_scpu_traceclk = 0x20000000,
//    scpu_traceclk_src_pll0div3 = 0x01000000,
//    scpu_traceclk_src_pll0div2 = 0x02000000,    
//};

enum pll_id {
    /* pll_0 = 0, */
    pll_1 = 0,
    pll_2,
    pll_3,
    pll_4,
    pll_5
};

enum scuclkin_type {
    scuclkin_osc = 0,
    scuclkin_rtcosc,
    scuclkin_pll0div3,
    scuclkin_pll0div4,
};

struct clock_value_pll {
    u16 ms;
    u16 ns;
    u16 ps;
    u16 div;
};

struct clock_value {
    union {
        struct clock_value_pll pll;
        unsigned long freq;
        bool_t enable;
    } u;
};
struct clock_list {
    struct clock_list *next;
};

struct clock_node;
typedef int (*fn_set)(struct clock_node *, struct clock_value *);
struct clock_node {
    struct clock_node *parent;
    struct clock_node *child_head;
    struct clock_node *child_next;
    fn_set set;//int (*set)(struct clock_node *, struct clock_value *);
    u8 is_enabled;
    char name[15];

} __attribute__((packed));


extern struct clock_node clock_node_pll1_out;

void clock_mgr_init(void);
void clock_mgr_open(struct clock_node *node, struct clock_value *clock_val);
void clock_mgr_close(struct clock_node *node);
void clock_mgr_set_scuclkin(enum scuclkin_type type, bool_t enable);
void clock_mgr_set_muxsel(u32 flags);
u32  clock_mgr_calculate_clockout(enum pll_id id, u16 ms, u16 ns, u16 F_ps);

void debug_pll_clock(void);

#endif
