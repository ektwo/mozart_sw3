#ifndef __MEMXFER_H__
#define __MEMXFER_H__


#include <framework/types.h>
#include <stddef.h>

#define MEMXFER_OPS_NONE 	0x00
#define MEMXFER_OPS_CPU 	0x01
#define MEMXFER_OPS_DMA 	0x02

/*  sample code for using memxfer api 
#include <memxfer.h>
extern const struct s_kdp_memxfer kdp_memxfer_module;
void test_kdp_memxfer_module(void) {
    if (0 == kdp_memxfer_module.init(MEMXFER_OPS_CPU, MEMXFER_OPS_DMA)) {
        outw(0x60001000, 0x11111111);
        outw(0x60001004, 0x22222222);
        outw(0x60001008, 0x33333333);
        outw(0x6000100c, 0x44444444);
        outw(0x60001010, 0x55555555);
        outw(0x60001014, 0x66666666);
        outw(0x60001018, 0x77777777);
        outw(0x6000101c, 0x88888888);
        kdp_memxfer_module.ddr_to_flash(0x00001000, 0x60001000, 32);
        kdp_memxfer_module.flash_to_ddr(0x60001020, 0x00001000, 32);
		kdp_memxfer_module.ddr_to_ddr(0x60001040, 0x60001020, 32);
    }
}
*/

struct s_kdp_memxfer {
    int (*init)(u8 flash_mode, u8 mem_mode);
    int (*flash_to_ddr)(u32 dst, u32 src, size_t bytes);
    int (*ddr_to_flash)(u32 dst, u32 src, size_t bytes);
    int (*flash_to_ddr_dma)(u32 dst, u32 src, size_t bytes);
    int (*ddr_to_flash_dma)(u32 dst, u32 src, size_t bytes);    
    int (*ddr_to_ddr)(u32 dst, u32 src, size_t bytes);
} ;


#endif
