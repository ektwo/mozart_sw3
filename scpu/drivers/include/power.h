#ifndef __POWER_H__
#define __POWER_H__


#include <types.h>


enum power_mgr_ops {
    power_mgr_ops_fcs = 0,
    power_mgr_ops_change_bus_speed,
    power_mgr_ops_pll_update,
    power_mgr_ops_sleeping,    
};

enum power_mgr_mode {
    power_mgr_mode_rtc = 0,          //rtc
    power_mgr_mode_alwayson,         //rtc + default 
    power_mgr_mode_full,             //rtc + default + ddr + npu
    power_mgr_mode_retention,        //rtc + default + ddr(self-refresh)
    power_mgr_mode_deep_retention    //rtc + ddr(self-refresh)
};

enum power_domain_type {
    power_domain_default = 0,
    power_domain_npu,    
    power_domain_ddr,
    power_domain_all
};

//high level power manager api
void power_mgr_ops(enum power_mgr_ops ops);
void power_mgr_set_mode(enum power_mgr_mode mode);
//low level power manager api
void power_mgr_set_domain(enum power_domain_type type, bool_t enable);
void power_mgr_wait_domain_ready(enum power_domain_type type);

#endif
