#ifndef __USB_H_INC_DRVOTG210_PE_USB_H
#define __USB_H_INC_DRVOTG210_PE_USB_H

//#include "flib.h"
//#include "SoFlexible.h"
#include "common_include.h"
#include "DrvOTG210_Pe_peripheral.h"
#include "DrvOTG210_Pe_my_usbtable.h"
#include "DrvOTG210_Pe_usb.h"

#define W1C				0x01  //USB interrupt:write 1 clear(new design)
#define W0C				0x02  //old design

#define SHOW_DEBUG_MESSAGE

#ifdef CONFIG_FOTG_CLEAR_INT_W0C
#define USB_CLEAR_INT	W0C
#else
#define USB_CLEAR_INT	W1C  //default new design
#endif



//#define HS_SUPPORTED
#define USB_VENDOR_COMMAND_SUPPORTED
#define MANUFACTURER_STRING_SUPPORTED 1
#define PRODUCT_STRING_SUPPORTED  1
// Define Bulk transfer FIFO test ///////

// Block toggle number define
#define SINGLE_BLK		1
#define DOUBLE_BLK		2
#define TRIBLE_BLK		3

// FIFO number define
#define FIFO0	0x0
#define FIFO1	0x1
#define FIFO2	0x2
#define FIFO3	0x3
#define FIFO4	0x4
#define FIFO5	0x5
#define FIFO6	0x6
#define FIFO7	0x7
#define FIFO8	0x8
#define FIFO9	0x9

#define Bulk_FIFO_SingleDir 	0	//Fixed
#define Bulk_FIFO_BiDir 		1//Fixed
#define Bulk_Satus			Bulk_FIFO_BiDir//Bulk_FIFO_SingleDir//////////
#define EP1_FIFO_NO			DOUBLE_BLK//SINGLE_BLK////////////////the other selection:SINGLE_BLK
#define FIFO_DIRECTION_OUT	0x00//Fixed
#define FIFO_DIRECTION_IN	0x01//Fixed
#define FIFO_DIRECTION_BIDRIECTIONAL	0x02//Fixed

#define EP1_START_FIFO		FIFO0 //Fixed
#if(Bulk_Satus == Bulk_FIFO_SingleDir)
#define EP2_START_FIFO	EP1_START_FIFO+EP1_FIFO_NO//Fixed
#define TOTAL_FIFO_NO	 (EP1_FIFO_NO)*2 //Fixed
#else
#define EP2_START_FIFO	EP1_START_FIFO//Fixed
#define TOTAL_FIFO_NO	 EP1_FIFO_NO //Fixed
#endif

#if(TOTAL_FIFO_NO==0x01)
#define FIFOCONFIG_0x1AC_VALUE	((FIFOEnBit |((EP1_FIFO_NO - 1) << 2)| FS_C1_I0_A0_EP1_TYPE)<<(8* (TOTAL_FIFO_NO-1)))|FIFOEnBit |((EP1_FIFO_NO - 1) << 2)| FS_C1_I0_A0_EP1_TYPE
#elif(TOTAL_FIFO_NO==0x02)
#define FIFOCONFIG_0x1AC_VALUE	((FIFOEnBit |((EP1_FIFO_NO - 1) << 2)| FS_C1_I0_A0_EP1_TYPE)<<(8* (TOTAL_FIFO_NO-2)))|((FIFOEnBit |((EP1_FIFO_NO - 1) << 2)| FS_C1_I0_A0_EP1_TYPE)<<(8* (TOTAL_FIFO_NO-1)))|FIFOEnBit |((EP1_FIFO_NO - 1) << 2)| FS_C1_I0_A0_EP1_TYPE
#else
#define FIFOCONFIG_0x1AC_VALUE	((FIFOEnBit |((EP1_FIFO_NO - 1) << 2)| FS_C1_I0_A0_EP1_TYPE)<<(8* (TOTAL_FIFO_NO-3)))|((FIFOEnBit |((EP1_FIFO_NO - 1) << 2)| FS_C1_I0_A0_EP1_TYPE)<<(8* (TOTAL_FIFO_NO-2)))|((FIFOEnBit |((EP1_FIFO_NO - 1) << 2)| FS_C1_I0_A0_EP1_TYPE)<<(8* (TOTAL_FIFO_NO-1)))|FIFOEnBit |((EP1_FIFO_NO - 1) << 2)| FS_C1_I0_A0_EP1_TYPE
#endif

#define PIO_MODE 		1  //fixed
#define USB_DMA_MODE	2//fixed
#define SPI020_MODE		USB_DMA_MODE


#define CX_EP	0xFE

#define HighBandWidth_None         0x00
#define HighBandWidth_2            0x01
#define HighBandWidth_3            0x02

////// Define USB transfer test defines//////
#define Bulk_AP					0
#define Interrupt_AP			1   //Important:this setting only for cross-connection, NOT for PC test-bench
#define IsochronousIN_AP		2
#define IsochronousOUT_AP		3
#define OTG_AP_Satus			Bulk_AP


#define FOTG200_Periph_MAX_EP		2	// 1..10
#define FOTG200_Periph_MAX_FIFO	4	// 0.. 9
#define EP0MAXPACKETSIZE	0x40
// #define EP0FIFOSIZE			64	// EP0_FIFO

// Max. Packet Size define
#define MX_PA_SZ_8			8
#define MX_PA_SZ_16			16
#define MX_PA_SZ_32			32
#define MX_PA_SZ_64			64
#define MX_PA_SZ_128		128
#define MX_PA_SZ_256		256
#define MX_PA_SZ_512		512
#define MX_PA_SZ_1024		1024

#define MASK_F0				0xF0

// Block Size define
#define BLK512BYTE		1
#define BLK1024BYTE		2

#define BLK64BYTE		1
#define BLK128BYTE		2


#define TYPE_STANDARD	0x00
#define TYPE_CLASS		0x20  //Bit[6,5] = 01b
#define TYPE_VENDOR		0x40  //Bit[6,5] = 02b


// Endpoint transfer type
#define TF_TYPE_ISOCHRONOUS		1
#define TF_TYPE_BULK			2
#define TF_TYPE_INTERRUPT		3

// Endpoint or FIFO direction define
#define DIRECTION_IN		0
#define DIRECTION_OUT	1

// Descriptor Table uses the following parameters : fixed
#define DEVICE_LENGTH				0x12
#define CONFIG_LENGTH				0x09
#define INTERFACE_LENGTH			0x09
#define EP_LENGTH					0x07
#define DEVICE_QUALIFIER_LENGTH		0x0A
#define OTG_LENGTH                  0x03 //For OTG
// Endpoint number define
#define EP0        0x00
#define EP1        0x01
#define EP2        0x02
#define EP3        0x03
#define EP4        0x04
#define EP5        0x05
#define EP6        0x06
#define EP7        0x07
#define EP8        0x08
#define EP9        0x09
#define EP10        0x10
#define EP11        0x11
#define EP12        0x12
#define EP13        0x13
#define EP14        0x14
#define EP15        0x15

#define STRING_00_LENGTH			0x04
#define STRING_10_LENGTH			0x1a
#define STRING_20_LENGTH			0x22
#define STRING_30_LENGTH			0x2e
#define STRING_40_LENGTH			0x26
#define STRING_50_LENGTH			0x10
#define STRING_60_LENGTH			0x1e
#define STRING_70_LENGTH			0x10
#define STRING_80_LENGTH			0x0e
#define STRING_90_LENGTH			0x00

//================ 1. Define for control Vandor test==================================================
extern INT8U u8CxOUTVandorDataCount;
extern INT8U u8CxINVandorDataCount;


//================ 2.Define for Bulk =====================================================================================
#define ShowMassMsg		1
#define MAX_BUFFER_SIZE	0x10000//0x2000//bruce;;04182005;;0x10000
#define DBUS_RD				1
#define DBUS_WR				2
#define APB_RD				3
#define APB_WR				4
#define DBUS2APB			5
#define APB2DBUS			6

#define CBW_SIGNATE		0x43425355
#define CSW_SIGNATE			0x53425355
#define CSW_STATUS_CMD_PASS			0x00
#define CSW_STATUS_CMD_FAIL			0x01
#define CSW_STATUS_PHASE_ERROR		0x02

#define DATA_OUT	0x00
#define DATA_IN		0x80
typedef enum
{
    IDLE,
    STATE_CBW,
    STATE_CB_DATA_IN,
    STATE_CB_DATA_OUT,
    STATE_CSW
} MassStorageState;

typedef struct CommandBlockWrapper
{
    INT32U u32Signature;
    INT32U u32Tag;
    INT32U u32DataTransferLength;
    INT8U u8Flags;
    INT8U u8LUN;
    INT8U u8CBLength;
    INT8U u8CB[16];
} CBW;

typedef struct CommandStatusWrapper
{
    INT32U u32Signature;
    INT32U u32Tag;
    INT32U u32DataResidue;
    INT8U u8Status;
} CSW;

extern CBW tOTGCBW;
extern CSW tOTGCSW;
extern MassStorageState eUsbOTGMassStorageState;

extern INT8U   *u8VirtOTGMemory;//[MAX_BUFFER_SIZE];
extern INT32U*   u32VirtOTGMemory;
//extern INT32U   u32VirtOTGMemoryIndex;

//extern INT32U	 u32OTGFIFOUseDMA;
extern INT32U	 u32UseDMAOTGChannel;
//extern INT8U   bOTGDMARunning;

extern void vUsb_Bulk_Out(INT16U u16FIFOByteCount);
extern void vUsb_Bulk_In(void);
extern void vOTG_APInit(void);
//extern void vOTGCheckDMA(void);

//================ 3.Define for Interrupt =====================================================================================
#define IntTransSizeFix 		0

//extern INT8U *u8InterruptOTGArray;
//extern INT8U u8OTGInterruptCount;
//extern INT32U u32OTGInterrupt_TX_COUNT ;

//extern INT8U u8OTGInterruptOutCount ;
//extern INT32U u32OTGInterrupt_RX_COUNT ;

extern void vOTG_Interrupt_In(void);
extern void vOTG_Interrupt_Initial(void);
extern void vOTG_Interrupt_Out(void);

//================   Define for Isochronous =========================
#define ISO_Wrap 	254//254

extern INT32U *u32OTGISOArray;
extern INT32U u32ISOOTGInTransferCount;
extern INT32U u32ISOOTGOutTransferCount;
extern INT32U u32ISOOTGInTest[4096];
extern INT32U *u32ISOOTGOutArray;
extern INT8U u8ISOOTGOutCount ;

extern void vOTG_ISO_Initial(void);
extern void vOTG_ISO_In(void);
extern void vOTG_ISO_Out(void);
extern void vAHB_DMA_WaitIntStatus(UINT32 Channel);

//============== 4.Define for main.c =========================================


//============== 5.Define for usb.c  =========================================
typedef enum
{
    CMD_VOID,						// No command
    CMD_GET_DESCRIPTOR,			// Get_Descriptor command
    CMD_SET_DESCRIPTOR,			// Set_Descriptor command
    CMD_CxOUT_Vendor,				//Cx OUT Vandor command test
    CMD_CxIN_Vendor				//Cx IN Vandor command test
} CommandType;

typedef enum
{
    ACT_IDLE,
    ACT_DONE,
    ACT_STALL
} Action;

#define DCacheEnable				0
// Table 9-5. Descriptor Types
#define DT_DEVICE							1
#define DT_CONFIGURATION					2
#define DT_STRING							3
#define DT_INTERFACE						4
#define DT_ENDPOINT							5
#define DT_DEVICE_QUALIFIER					6
#define DT_OTHER_SPEED_CONFIGURATION		7
#define DT_INTERFACE_POWER					8
#define DT_OTG				                9 //For OTG


//For OTG
#define OTG_SRP_SUPPORT			    0x01
#define OTG_HNP_SUPPORT			    0x02
#define FS_OTG_SUPPORT          (OTG_SRP_SUPPORT | OTG_HNP_SUPPORT)


#define TEST_J								0x02
#define TEST_K								0x04
#define TEST_SE0_NAK						0x08
#define TEST_PKY								0x10


//*******************************************************
// Structure
typedef struct Setup_Packet
{
    INT8U Direction;		/*Data transfer direction: IN, OUT*/
    INT8U Type;			/*Request Type: Standard, Class, Vendor*/
    INT8U Object;		/*Recipient: Device, Interface, Endpoint,other*/
    INT16U Request;		/*Refer to Table 9-3*/
    INT16U Value;
    INT16U Index;
    INT16U Length;
} SetupPacket;

extern BOOLEAN bOTGBufferEmpty;
//extern INT8U u8OTGMessageLevel;

//extern INT32U OTG_interrupt_level1;
//extern INT32U OTG_interrupt_level1_Save;
//extern INT32U OTG_interrupt_level1_Mask;

extern INT8U u8OTGConfigValue;
#ifdef HS_SUPPORTED
extern INT8U u8OTGInterfaceValue;
//extern INT8U u8OTGInterfaceAlternateSetting;
extern BOOLEAN bOTGHighSpeed;
#endif
extern INT32U u32SCSIRespLength;
//extern void vOTGInit(void);
extern void FOTG210_IRQHandler(void);
#ifdef HS_SUPPORTED
extern void vOTGFIFO_EPxCfg_HS(void);
#endif
//extern void vOTGFIFO_EPxCfg_FS(void);

//extern void vFOTG200_Dev_Init(void);
extern void vOTGDxFWr(INT8U FIFONum, INT8U * pu8Buffer, INT16U u16Num);
extern INT8U *Replace_SdRAM_Address(INT8U *pu8Buffer);

extern void vOTGDxFRd(INT8U FIFONum, INT8U * pu8Buffer, INT16U u16Num);
#endif
