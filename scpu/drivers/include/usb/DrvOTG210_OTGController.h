//////////////////////////////////////////////////////////////////////////////
//
//	File name: OTGController.h
//	Version: 1.0
//	Date: 2005/1/31
//	Author: Bruce
//	Company: Faraday Tech. Corp.
//	Description: 1.Define Data Type
//               2.Define for Bulk
//               3.Define Macro
///////////////////////////////////////////////////////////////////////////////
#ifndef __OTGCONTROL_H
#define __OTGCONTROL_H

//#include "flib.h"

//=================== 1.OTG Condition Setting ============================================================
//========================================================================================================
#define   OTGC_Device_Not_Support_Then_Return             1

//=================== 2.Define Macro =====================================================================
//========================================================================================================

//#define OTG_BASE_ADDRESS	                      USB_FOTG210_PA_BASE//0x92000000//0x92500000//bessel:Base address
extern unsigned int OTG_BASE_ADDRESS;
#define mbFOTGPort(bOffset)	                       *((volatile UINT8 *) ( OTG_BASE_ADDRESS | bOffset))
#define mwFOTGPort(bOffset)	                       *((volatile UINT16 *) ( OTG_BASE_ADDRESS | bOffset))
#define mdwFOTGPort(bOffset)	                   *((volatile UINT32 *) ( OTG_BASE_ADDRESS | bOffset))

#define mwHost20Port(bOffset)	                           *((volatile UINT32 *) ( OTG_BASE_ADDRESS | bOffset))
#define mwHost20Bit_Rd(bByte,wBitNum)                     (mwHost20Port(bByte)&wBitNum)
#define mwHost20Bit_Set(bByte,wBitNum)                    (mwHost20Port(bByte)|=wBitNum)
#define mwHost20Bit_Clr(bByte,wBitNum)                    (mwHost20Port(bByte)&=~wBitNum)

//Offset:0x080(OTG Control/Status Register) => Suppose Word-Read & Word-Write
//~B Function
#define mdwOTGC_Control_B_BUS_REQ_Rd()		      (mdwFOTGPort(0x80)& BIT0)
#define mdwOTGC_Control_B_BUS_REQ_Set()  		  (mdwFOTGPort(0x80) |=  BIT0)
#define mdwOTGC_Control_B_BUS_REQ_Clr()  		  (mdwFOTGPort(0x80) &=  (~BIT0))

#define mdwOTGC_Control_B_HNP_EN_Rd()		      (mdwFOTGPort(0x80)& BIT1)
#define mdwOTGC_Control_B_HNP_EN_Set()		      (mdwFOTGPort(0x80) |=  BIT1)
#define mdwOTGC_Control_B_HNP_EN_Clr()  		  (mdwFOTGPort(0x80) &=  (~BIT1))

#define mdwOTGC_Control_B_DSCHG_VBUS_Rd()		  (mdwFOTGPort(0x80)& BIT2)
#define mdwOTGC_Control_B_DSCHG_VBUS_Set()		  (mdwFOTGPort(0x80) |=  BIT2)
#define mdwOTGC_Control_B_DSCHG_VBUS_Clr() 		  (mdwFOTGPort(0x80) &=  (~BIT2))

//~A Function
#define mdwOTGC_Control_A_BUS_REQ_Rd()	          (mdwFOTGPort(0x80)& BIT4)
#define mdwOTGC_Control_A_BUS_REQ_Set()	          (mdwFOTGPort(0x80) |=  BIT4)
#define mdwOTGC_Control_A_BUS_REQ_Clr()	          (mdwFOTGPort(0x80) &=  (~BIT4))

#define mdwOTGC_Control_A_BUS_DROP_Rd()	          (mdwFOTGPort(0x80)& BIT5)
#define mdwOTGC_Control_A_BUS_DROP_Set()	      (mdwFOTGPort(0x80) |=  BIT5)
#define mdwOTGC_Control_A_BUS_DROP_Clr()	      (mdwFOTGPort(0x80) &=  (~BIT5))

#define mdwOTGC_Control_A_SET_B_HNP_EN_Rd()	      (mdwFOTGPort(0x80)& BIT6)
#define mdwOTGC_Control_A_SET_B_HNP_EN_Set()	  (mdwFOTGPort(0x80) |=  BIT6)
#define mdwOTGC_Control_A_SET_B_HNP_EN_Clr()	  (mdwFOTGPort(0x80) &=  (~BIT6))

#define mdwOTGC_Control_A_SRP_DET_EN_Rd()	      (mdwFOTGPort(0x80)& BIT7)
#define mdwOTGC_Control_A_SRP_DET_EN_Set()	      (mdwFOTGPort(0x80) |=  BIT7)
#define mdwOTGC_Control_A_SRP_DET_EN_Clr()	      (mdwFOTGPort(0x80) &=  (~BIT7))

#define mdwOTGC_Control_A_SRP_RESP_TYPE_Rd()	  (mdwFOTGPort(0x80)& BIT8)
#define mdwOTGC_Control_A_SRP_RESP_TYPE_Set(b)	  (mdwFOTGPort(0x80) |=  b)
#define mdwOTGC_Control_A_SRP_RESP_TYPE_Clr()	  (mdwFOTGPort(0x80) &=  (~BIT8))

#define mdwOTGC_Control_VBUS_FLT_SEL_Rd()	      (mdwFOTGPort(0x80)& BIT10)
#define mdwOTGC_Control_VBUS_FLT_SEL_Set()	      (mdwFOTGPort(0x80) |=  BIT10)
#define mdwOTGC_Control_VBUS_FLT_SEL_Clr()	      (mdwFOTGPort(0x80) &=  (~BIT10))


#define mdwOTGC_Control_711MA_Phy_Issue_Rd()		  (mdwFOTGPort(0x80)& BIT28)
#define mdwOTGC_Control_711MA_Phy_Issue_Set()  		  (mdwFOTGPort(0x80) |=  BIT28)
#define mdwOTGC_Control_711MA_Phy_Issue_Clr()  		  (mdwFOTGPort(0x80) &=  (~BIT28))


#define mdwOTGC_Control_B_SESS_END_Rd()	          (mdwFOTGPort(0x80)& BIT16)
#define mdwOTGC_Control_B_SESS_VLD_Rd()	          (mdwFOTGPort(0x80)& BIT17)
#define mdwOTGC_Control_A_SESS_VLD_Rd()	          (mdwFOTGPort(0x80)& BIT18)
#define mdwOTGC_Control_A_VBUS_VLD_Rd()	          (mdwFOTGPort(0x80)& BIT19)
#define mdwOTGC_Control_CROLE_Rd()	              (mdwFOTGPort(0x80)& BIT20) //0:Host 1:Peripheral
#define mdwOTGC_Control_ID_Rd()	                  (mdwFOTGPort(0x80)& BIT21) //0:A-Device 1:B-Device
#define mdwOTGC_Control_Rd()	                  (mdwFOTGPort(0x80))
#define mdwOTGC_Control_Speed_Rd()	              ((mdwFOTGPort(0x80)& 0x00C00000)>>22)

#define A_SRP_RESP_TYPE_VBUS	                  0x00
#define A_SRP_RESP_TYPE_DATA_LINE                 0x100


//Offset:0x004(OTG Interrupt Status Register)
#define mdwOTGC_INT_STS_Rd()                      (mdwFOTGPort(0x84))
#define mdwOTGC_INT_STS_Clr(wValue)               (mdwFOTGPort(0x84) |= wValue)

#define OTGC_INT_BSRPDN                           BIT0
#define OTGC_INT_ASRPDET                          BIT4
#define OTGC_INT_AVBUSERR                         BIT5
#define OTGC_INT_RLCHG                            BIT8
#define OTGC_INT_IDCHG                            BIT9
#define OTGC_INT_OVC                              BIT10
#define OTGC_INT_BPLGRMV                          BIT11
#define OTGC_INT_APLGRMV                          BIT12

//
#define OTGC_INT_A_TYPE                           (OTGC_INT_ASRPDET|OTGC_INT_AVBUSERR|OTGC_INT_OVC|OTGC_INT_RLCHG|OTGC_INT_IDCHG|OTGC_INT_BPLGRMV|OTGC_INT_APLGRMV)
//
#define OTGC_INT_B_TYPE                           (OTGC_INT_BSRPDN|OTGC_INT_AVBUSERR|OTGC_INT_OVC|OTGC_INT_RLCHG|OTGC_INT_IDCHG)



//Offset:0x008(OTG Interrupt Enable Register)
#define mdwOTGC_INT_Enable_Rd()                   (mdwFOTGPort(0x88))
#define mdwOTGC_INT_Enable_Set(wValue)            (mdwFOTGPort(0x88)|= wValue)
#define mdwOTGC_INT_Enable_Clr(wValue)            (mdwFOTGPort(0x88)&= (~wValue))

#define mdwOTGC_INT_Enable_BSRPDN_Set()           (mdwFOTGPort(0x88) |=  BIT0)
#define mdwOTGC_INT_Enable_ASRPDET_Set()          (mdwFOTGPort(0x88) |=  BIT4)
#define mdwOTGC_INT_Enable_AVBUSERR_Set()         (mdwFOTGPort(0x88) |=  BIT5)
#define mdwOTGC_INT_Enable_RLCHG_Set()            (mdwFOTGPort(0x88) |=  BIT8)
#define mdwOTGC_INT_Enable_IDCHG_Set()            (mdwFOTGPort(0x88) |=  BIT9)
#define mdwOTGC_INT_Enable_OVC_Set()              (mdwFOTGPort(0x88) |=  BIT10)
#define mdwOTGC_INT_Enable_BPLGRMV_Set()          (mdwFOTGPort(0x88) |=  BIT11)
#define mdwOTGC_INT_Enable_APLGRMV_Set()          (mdwFOTGPort(0x88) |=  BIT12)

#define mdwOTGC_INT_Enable_BSRPDN_Clr()           (mdwFOTGPort(0x88) &= ~BIT0)
#define mdwOTGC_INT_Enable_ASRPDET_Clr()          (mdwFOTGPort(0x88) &= ~BIT4)
#define mdwOTGC_INT_Enable_AVBUSERR_Clr()         (mdwFOTGPort(0x88) &= ~BIT5)
#define mdwOTGC_INT_Enable_RLCHG_Clr()            (mdwFOTGPort(0x88) &= ~BIT8)
#define mdwOTGC_INT_Enable_IDCHG_Clr()            (mdwFOTGPort(0x88) &= ~BIT9)
#define mdwOTGC_INT_Enable_OVC_Clr()              (mdwFOTGPort(0x88) &= ~BIT10)
#define mdwOTGC_INT_Enable_BPLGRMV_Clr()          (mdwFOTGPort(0x88) &= ~BIT11)
#define mdwOTGC_INT_Enable_APLGRMV_Clr()          (mdwFOTGPort(0x88) &= ~BIT12)


//Global Mask of HC/OTG/DEV Interrupt Register (Address = 0x0C4)
#define mwOTG20_Interrupt_Mask_Rd()		                  (mwHost20Port(0xC4)&0x00000007)
#define mwOTG20_Interrupt_Mask_Set(bValue)		          (mwHost20Port(0xC4)=bValue)
#define mwOTG20_Interrupt_Mask_Device_Set()		          (mwHost20Bit_Set(0xC4,BIT0))
#define mwOTG20_Interrupt_Mask_OTG_Set()		          (mwHost20Bit_Set(0xC4,BIT1))
#define mwOTG20_Interrupt_Mask_HOST_Set()		          (mwHost20Bit_Set(0xC4,BIT2))
#define mwOTG20_Interrupt_OutPut_High_Set()		          (mwHost20Bit_Set(0xC4,BIT3))
#define mwOTG20_Interrupt_OutPut_High_Clr()		          (mwHost20Bit_Clr(0xC4,BIT3))


//===================3.Define Stricture ==================================================================
//========================================================================================================
typedef enum
{
    OTG_ID_A_TYPE = 0,
    OTG_ID_B_TYPE

} OTGC_ID_Type;

typedef enum
{
    OTG_CurrentRole_Host = 0,
    OTG_CurrentRole_Peripheral

} OTGC_CurrentRole;


typedef struct OTGC_STS
{
    OTGC_ID_Type                     ID;                       //0:A-Type   1: B-Type
    volatile OTGC_CurrentRole        CurrentRole;	           //1:Host  0:Peripheral
    INT8U                            bVBUSAlwaysOn;             //0:For Standard-ISR 1:For AP-ISR


    INT32U                           wCurrentInterruptMask; //OTGC_INT_B_TYPE: for Type-B ���Y plugged in
    //OTGC_INT_A_TYPE: for Type-A ���Y plugged in
    volatile INT8U                   bVBUS_Vaild;
    INT8U                            bHostISRType;             //0:For Standard-ISR 1:For AP-ISR
    volatile INT8U                   bBusResetRequest;
    volatile INT8U                   bPortSuspendStatusChange;
    volatile INT8U                   bPortEnableStatusChange;
    volatile INT8U                   bPortConnectStatusChange;
    volatile INT8U                   bDevice_RESET_Received;
    volatile INT8U                   bDevice_RemoteWakeUp_Received;

    INT8U                            A_bGet_SRP_HNP_Support;
    volatile INT8U                   A_bASRPDET;
    volatile INT8U                   B_bBSRPDN;

    volatile INT8U                   A_BPLGRMV;
    volatile INT8U                   A_APLGRMV;
    volatile INT8U                   IDCHG;
    INT32U                           wOTGH_Interrupr_Save ;
    INT32U                           wOTGP_Interrupr_Save ;

} OTGC_STS;


//===================4.Define Extern Function =====================================================================
//========================================================================================================
//--From OTGController.C
extern void OTG_Init(void);
extern int OTG_INT_ISR(int irq, void *dev_id);
extern void OTG_RoleChange(INT8U bRole);
extern void OTGC_Init(void);
extern void OTGC_INT_ISR(UINT32 wINTStatus);
extern void OTGC_A_Bus_Drop(void);
extern INT8U OTGC_A_SRP_Init(void);
extern INT8U OTGC_A_HNP_Init(void);
extern void OTGC_A_Menu(void);
extern UINT8 OTGC_B_SRP_Init(void);
extern UINT8 OTGC_B_HNP_Init(void);
//extern void OTGC_B_Menu(void);
extern UINT8 OTGC_VBS_Valid(void);
//--From other file
extern void OTGH_PathTest_Main(void);
extern void OTGP_CV_Test_main(void);
extern void OTGP_Resume_Test_main(void);
extern void OTGH_AP_Main(void);
extern void OTGC_A_PHY_Reset(void);

//===================5.Define Extern Variable =====================================================================
//========================================================================================================


extern OTGC_STS  OTG;

#endif
