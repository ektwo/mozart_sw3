#ifndef __MY_USBTABLE_H
#define __MY_USBTABLE_H

//#include "Pe_usb_hs_pos.h"
//#include "Pe_usb_fs_pos.h"
#include "DrvOTG210_Pe_peripheral.h"
#include "DrvOTG210_Pe_usb.h"

#define FS_USB_SPEC_VER        0X0200

//#define FS_VENDOR_ID           0X2310
//#define FS_PRODUCT_ID          0X5678
#define FS_VENDOR_ID           0XD7D
#define FS_PRODUCT_ID          0X100

#define FS_DEVICE_RELEASE_NO   0X0001

#define FS_C1_CONFIG_TOTAL_LENGTH				(CONFIG_LENGTH + INTERFACE_LENGTH +  FS_C1_INTERFACE_LENGTH)//+3//Bruce;;01172004//bessel:remove 3
#define FS_MAX_CONFIG_TOTAL_LENGTH				(FS_C1_CONFIG_TOTAL_LENGTH)
#define FS_C1_I0_ALT_LENGTH 					(EP_LENGTH * FS_C1_I0_A0_EP_NUMBER)
#define FS_C1_INTERFACE_LENGTH					(FS_C1_I0_ALT_LENGTH)

// device configuration:
#define FS_bDeviceClass         0X00
#define FS_bDeviceSubClass      0X00
#define FS_bDeviceProtocol      0X00
#ifdef MANUFACTURER_STRING_SUPPORTED
#define FS_iManufacturer        0x10
#else
#define FS_iManufacturer        0x00
#endif

#ifdef PRODUCT_STRING_SUPPORTED
#define FS_iProduct        0x20
#else
#define FS_iProduct        0x00
#endif
#define FS_iSerialNumber        0x00

#define FS_CONFIGURATION_NUMBER 0X01


// Configuration 0X01
#define FS_C1_INTERFACE_NUMBER  0X01
#define FS_C1                   0X01
#define FS_C1_iConfiguration    0x00//0X30   //bessel:reduce code size
#define FS_C1_bmAttribute       0XC0
#define FS_C1_iMaxPower         0X00

// Interface 0
#define FS_C1_I0_ALT_NUMBER    0X01

// AlternateSetting 0X00
#define FS_C1_I0_A0_bInterfaceNumber   0X00
#define FS_C1_I0_A0_bAlternateSetting  0X00
#define FS_C1_I0_A0_EP_NUMBER          0X02//bessel:0x04->0x02

#define FS_C1_I0_A0_bInterfaceClass    0XFF//bessel:MSC device
#define FS_C1_I0_A0_bInterfaceSubClass 0X01
#define FS_C1_I0_A0_bInterfaceProtocol 0X50

#define FS_C1_I0_A0_iInterface         0x00//0X40 //bessel:0x40 -> 0x00

#if (FS_C1_I0_A0_EP_NUMBER >= 0X01)
//EP0X01
#define FS_C1_I0_A0_EP1_BLKSIZE    BLK128BYTE//BLK64BYTE
#define FS_C1_I0_A0_EP1_BLKNO      DOUBLE_BLK
#define FS_C1_I0_A0_EP1_DIRECTION  DIRECTION_IN
#define FS_C1_I0_A0_EP1_TYPE       TF_TYPE_BULK
#define FS_C1_I0_A0_EP1_MAX_PACKET 0x200//0040
#define FS_C1_I0_A0_EP1_bInterval  00
#endif
#if (FS_C1_I0_A0_EP_NUMBER >= 0X02)
//EP0X02
#define FS_C1_I0_A0_EP2_BLKSIZE    BLK128BYTE//BLK64BYTE
#define FS_C1_I0_A0_EP2_BLKNO      DOUBLE_BLK
#define FS_C1_I0_A0_EP2_DIRECTION  DIRECTION_OUT
#define FS_C1_I0_A0_EP2_TYPE       TF_TYPE_BULK
#define FS_C1_I0_A0_EP2_MAX_PACKET 0X200//0x0040
#define FS_C1_I0_A0_EP2_bInterval  00
#endif


#if (HS_MAX_CONFIG_TOTAL_LENGTH > FS_MAX_CONFIG_TOTAL_LENGTH)
#define CONFIG_LENGTH_EX				(HS_MAX_CONFIG_TOTAL_LENGTH)
#define OTHER_SPEED_CONFIG_LENGTH_EX	(HS_MAX_CONFIG_TOTAL_LENGTH)
#else
#define CONFIG_LENGTH_EX				(FS_MAX_CONFIG_TOTAL_LENGTH)
#define OTHER_SPEED_CONFIG_LENGTH_EX	(FS_MAX_CONFIG_TOTAL_LENGTH)
#endif


#endif
