#ifndef __FOTG200_Peripheral_M_H
#define __FOTG200_Peripheral_M_H

//#include "flib.h"
#include "DrvOTG210_Pe_usb.h"

//#define IRQ_FOTG200 					40	//27
//#define FOTG200_BASE_ADDRESS			USB_FOTG210_PA_BASE//0x92000000//0x92500000//0x96700000//  0x90600000  
extern unsigned int FOTG200_BASE_ADDRESS;			  

//========== 1.Define Data Type  =========================================================================================

#define MESS_ERROR		(0x01 << 0)
#define MESS_WARNING	(0x01 << 1)
#define MESS_INFO		(0x01 << 2)
//	#define TRUE				1
//	#define FALSE			0

#define mLowByte(u16)	((INT8U)(u16	 ))
#define mHighByte(u16)	((INT8U)(u16 >> 8))

// Macro
#define bFOTGPeri_Port(bOffset)		*((volatile INT8U *) ( FOTG200_BASE_ADDRESS | (bOffset)))
#define hwFOTGPeri_Port(bOffset)	*((volatile INT16U *) ( FOTG200_BASE_ADDRESS | (bOffset)))
#define wFOTGPeri_Port(bOffset)		*((volatile INT32U *) ( FOTG200_BASE_ADDRESS | (bOffset)))

// Macro

// Main control register(0x100)
#define mUsbRmWkupST()			(wFOTGPeri_Port(0x100) & BIT0)
#define mUsbRmWkupSet()		    (wFOTGPeri_Port(0x100) |= BIT0)
#define mUsbRmWkupClr()			(wFOTGPeri_Port(0x100) &= ~BIT0)

#define mUsbTstHalfSpeedEn()	(wFOTGPeri_Port(0x100) |= BIT1)
#define mUsbTstHalfSpeedDis()	(wFOTGPeri_Port(0x100) &= ~BIT1)

#define mUsbGlobIntEnRd()		(wFOTGPeri_Port(0x100) & BIT2)
#define mUsbGlobIntEnSet()		(wFOTGPeri_Port(0x100) |= BIT2)
#define mUsbGlobIntDis()		(wFOTGPeri_Port(0x100) &= ~BIT2)

#define mUsbGoSuspend()			(wFOTGPeri_Port(0x100) |=  BIT3)

#define mUsbSoftRstSet()		(wFOTGPeri_Port(0x100) |=  BIT4)
#define mUsbSoftRstClr()		(wFOTGPeri_Port(0x100) &= ~BIT4)

#define mUsbChipEnSet()			(wFOTGPeri_Port(0x100) |= BIT5)

#define mUsbOTGHighSpeedST()	(wFOTGPeri_Port(0x100) & BIT6)
#define mUsbOTGDevFS()			(wFOTGPeri_Port(0x100) |= BIT9)
#define mUsbOTGDevHS()			(wFOTGPeri_Port(0x100) &= ~BIT9)

// Device address register(0x104)
#define mUsbDevAddrSet(Value)	(wFOTGPeri_Port(0x104) = (INT32U)Value)
#define mUsbCfgST()				(wFOTGPeri_Port(0x104) & BIT7)
#define mUsbCfgSet()			(wFOTGPeri_Port(0x104) |= BIT7)
#define mUsbCfgClr()			(wFOTGPeri_Port(0x104) &= ~BIT7)
#define mUsbDMARst()            (wFOTGPeri_Port(0x100) |= BIT8)
#define mUsbFIFOClr(fifo_num)   (wFOTGPeri_Port(0x1b0+fifo_num*4) |= BIT12)

// Test register(0x108)
#define mUsbClrAllFIFOSet()		(wFOTGPeri_Port(0x108) |= BIT0)
#define mUsbClrAllFIFOClr()		(wFOTGPeri_Port(0x108) &= ~BIT0)
#define mUsbTSTMODSet()		(wFOTGPeri_Port(0x108) |= BIT5)

// SOF Frame Number register(0x10C)
#define mUsbFrameNo()			(INT16U)(wFOTGPeri_Port(0x10C) & 0x7FF)
#define mUsbMicroFrameNo()		(INT8U)((wFOTGPeri_Port(0x10C) & 0x3800)>>11)

// SOF Mask register(0x110)
#define mUsbSOFMaskHS()		    (wFOTGPeri_Port(0x110) = 0x44c)
#define mUsbSOFMaskFS()		    (wFOTGPeri_Port(0x110) = 0x2710)

// PHY Test Mode Selector register(0x114)
#define mUsbTsMdWr(item)		(wFOTGPeri_Port(0x114) = (INT32U)item)
#define mUsbUnPLGClr()			(wFOTGPeri_Port(0x114) &= ~BIT0)
#define mUsbUnPLGSet()			(wFOTGPeri_Port(0x114) |= BIT0)
// Vendor Specific IO Control register(0x118)

// Cx configuration and status register(0x11C)

// Cx configuration and FIFO Empty Status register(0x120)
#define mUsbEP0DoneSet()		(wFOTGPeri_Port(0x120) |= BIT0)
#define mUsbTsPkDoneSet()		(wFOTGPeri_Port(0x120) |= BIT1)
#define mUsbEP0StallSet()		(wFOTGPeri_Port(0x120) |= BIT2)
#define mUsbCxFClr()			(wFOTGPeri_Port(0x120) |= BIT3)

#define mUsbCxFFull()			(wFOTGPeri_Port(0x120) & BIT4)
#define mUsbCxFEmpty()			(wFOTGPeri_Port(0x120) & BIT5)
#define mUsbCxFByteCnt()		(INT8U)((wFOTGPeri_Port(0x120) & 0x7F000000)>>24)
#define mUsbFIFO0Empty()	(wFOTGPeri_Port(0x120) & BIT8)
#define mUsbFIFO1Empty()	(wFOTGPeri_Port(0x120) & BIT9)
#define mUsbFIFO2Empty()	(wFOTGPeri_Port(0x120) & BIT10)
#define mUsbFIFO3Empty()	(wFOTGPeri_Port(0x120) & BIT11)

// IDLE Counter register(0x124)
#define mUsbIdleCnt(time)		(wFOTGPeri_Port(0x124) = (INT32U)time)

// Mask of interrupt group(0x130)
#define mUsbIntGrp0Dis()		(wFOTGPeri_Port(0x130) |= BIT0)
#define mUsbIntGrp1Dis()		(wFOTGPeri_Port(0x130) |= BIT1)
#define mUsbIntGrp2Dis()		(wFOTGPeri_Port(0x130) |= BIT2)

#define mUsbIntGroupMaskRd()	(wFOTGPeri_Port(0x130))

// Mask of interrupt source group 0(0x134)
#define mUsbIntEP0SetupDis()	(wFOTGPeri_Port(0x134) |= BIT0)
#define mUsbIntEP0InDis()		(wFOTGPeri_Port(0x134) |= BIT1)
#define mUsbIntEP0OutDis()		(wFOTGPeri_Port(0x134) |= BIT2)
#define mUsbIntEP0EndDis()		(wFOTGPeri_Port(0x134) |= BIT3)
#define mUsbIntEP0FailDis()		(wFOTGPeri_Port(0x134) |= BIT4)

#define mUsbIntEP0SetupEn()		(wFOTGPeri_Port(0x134) &= ~(BIT0))
#define mUsbIntEP0InEn()			(wFOTGPeri_Port(0x134) &= ~(BIT1))
#define mUsbIntEP0OutEn()		(wFOTGPeri_Port(0x134) &= ~(BIT2))
#define mUsbIntEP0EndEn()		(wFOTGPeri_Port(0x134) &= ~(BIT3))
#define mUsbIntEP0FailEn()		(wFOTGPeri_Port(0x134) &= ~(BIT4))

#define mUsbIntSrc0MaskRd()		(wFOTGPeri_Port(0x134))

// Mask of interrupt source group 1(0x138)
#define mUsbIntSrc1Mask_Set(wValue)		          (wFOTGPeri_Port(0x138)=wValue)
#define mUsbIntFIFO0_3OUTDis()	(wFOTGPeri_Port(0x138) |= 0xFF)
#define mUsbIntFIFO0_3INDis()	(wFOTGPeri_Port(0x138) |= 0xF0000)

#define mUsbIntF0OUTEn()		(wFOTGPeri_Port(0x138) &= ~(BIT1 | BIT0))
#define mUsbIntF0OUTDis()		(wFOTGPeri_Port(0x138) |= (BIT1 | BIT0))
#define mUsbIntF1OUTEn()		(wFOTGPeri_Port(0x138) &= ~(BIT3 | BIT2))
#define mUsbIntF1OUTDis()		(wFOTGPeri_Port(0x138) |= (BIT3 | BIT2))
#define mUsbIntF2OUTEn()		(wFOTGPeri_Port(0x138) &= ~(BIT5 | BIT4))
#define mUsbIntF2OUTDis()		(wFOTGPeri_Port(0x138) |= (BIT5 | BIT4))

#define mUsbIntF0INEn()			(wFOTGPeri_Port(0x138) &= ~BIT16)
#define mUsbIntF0INDis()		(wFOTGPeri_Port(0x138) |= BIT16)
#define mUsbIntF3OUTEn()		(wFOTGPeri_Port(0x138) &= ~(BIT7 | BIT6))
#define mUsbIntF3OUTDis()		(wFOTGPeri_Port(0x138) |= (BIT7 | BIT6))

#define mUsbIntF2INEn()			(wFOTGPeri_Port(0x138) &= ~BIT18)
#define mUsbIntF2INDis()		(wFOTGPeri_Port(0x138) |= BIT18)

#define mUsbIntSrc1MaskRd()		(wFOTGPeri_Port(0x138))

// Mask of interrupt source group 2(DMA int mask)(0x13C)
#define mUsbIntSrc2Mask_Set(wValue)		          (wFOTGPeri_Port(0x13C)=wValue)
#define mUsbIntSuspDis()		(wFOTGPeri_Port(0x13C) |= BIT1)
#define mUsbIntDmaErrDis()		(wFOTGPeri_Port(0x13C) |= BIT8)
#define mUsbIntDmaFinishDis()	(wFOTGPeri_Port(0x13C) |= BIT7)

#define mUsbIntSuspEn()			(wFOTGPeri_Port(0x13C) &= ~(BIT1))
#define mUsbIntDmaErrEn()		(wFOTGPeri_Port(0x13C) &= ~(BIT8))
#define mUsbIntDmaFinishEn()		(wFOTGPeri_Port(0x13C) &= ~(BIT7))

#define mUsbIntSrc2MaskRd()		(wFOTGPeri_Port(0x13C))

// Interrupt group (0x140)
#define mUsbIntGroupRegRd()		(wFOTGPeri_Port(0x140))
#define mUsbIntGroupRegSet(wValue)	(wFOTGPeri_Port(0x140) |= wValue)

// Interrupt source group 0(0x144)
#define mUsbIntSrc0Rd()			(wFOTGPeri_Port(0x144))
#if(USB_CLEAR_INT == W1C )
#define mUsbIntEP0AbortClr()		(wFOTGPeri_Port(0x144) |= (BIT5))
#define mUsbIntSrc0Clr()			(wFOTGPeri_Port(0x144) = BIT5)
#elif(USB_CLEAR_INT == W0C )
#define mUsbIntEP0AbortClr()		(wFOTGPeri_Port(0x144) &= ~(BIT5))
#define mUsbIntSrc0Clr()			(wFOTGPeri_Port(0x144) = 0)
#else
#error "Need to define how to clear usb Interrupt"
#endif
#define mUsbIntSrc0Set(wValue)	(wFOTGPeri_Port(0x144) |= wValue)

// Interrupt source group 1(0x148)
#define mUsbIntSrc1Rd()			(wFOTGPeri_Port(0x148))
#define mUsbIntSrc1Set(wValue)	(wFOTGPeri_Port(0x148) |= wValue)

// Interrupt source group 2(0x14C)
#define mUsbIntSrc2Rd()			(wFOTGPeri_Port(0x14C))
#define mUsbIntSrc2Set(wValue)	(wFOTGPeri_Port(0x14C) |= wValue)
#if(USB_CLEAR_INT == W1C )
//#error "oooooooooooooo W1C" 
#define mUsbIntSrc2Clr()			(wFOTGPeri_Port(0x14C) = (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6|BIT7|BIT8))
#define mUsbIntBusRstClr()		(wFOTGPeri_Port(0x14C) |= BIT0)
#define mUsbIntSuspClr()		(wFOTGPeri_Port(0x14C) |=BIT1)
#define mUsbIntResmClr()		(wFOTGPeri_Port(0x14C) |=BIT2)
#define mUsbIntIsoSeqErrClr()	(wFOTGPeri_Port(0x14C) |=BIT3)
#define mUsbIntIsoSeqAbortClr()	(wFOTGPeri_Port(0x14C) |=BIT4)
#define mUsbIntTX0ByteClr()		(wFOTGPeri_Port(0x14C) |=BIT5)
#define mUsbIntRX0ByteClr()		(wFOTGPeri_Port(0x14C) |=BIT6)
#define mUsbIntDmaFinishClr()	(wFOTGPeri_Port(0x14C) |=BIT7)
#define mUsbIntDmaErrClr()		(wFOTGPeri_Port(0x14C) |=BIT8)
#elif(USB_CLEAR_INT == W0C )
#define mUsbIntSrc2Clr()			(wFOTGPeri_Port(0x14C) = 0)
#define mUsbIntBusRstClr()		(wFOTGPeri_Port(0x14C) &= ~BIT0)
#define mUsbIntSuspClr()		(wFOTGPeri_Port(0x14C) &= ~BIT1)
#define mUsbIntResmClr()		(wFOTGPeri_Port(0x14C) &= ~BIT2)
#define mUsbIntIsoSeqErrClr()	(wFOTGPeri_Port(0x14C) &= ~BIT3)
#define mUsbIntIsoSeqAbortClr()	(wFOTGPeri_Port(0x14C) &= ~BIT4)
#define mUsbIntTX0ByteClr()		(wFOTGPeri_Port(0x14C) &= ~BIT5)
#define mUsbIntRX0ByteClr()		(wFOTGPeri_Port(0x14C) &= ~BIT6)
#define mUsbIntDmaFinishClr()	(wFOTGPeri_Port(0x14C) &= ~BIT7)
#define mUsbIntDmaErrClr()		(wFOTGPeri_Port(0x14C) &= ~BIT8)
#else
#error "Need to define how to clear usb Interrupt"
#endif

#define mUsbIntDmaFinishRd()	(wFOTGPeri_Port(0x14C) &BIT7)

#define mUsbIntDmaFinish()		(wFOTGPeri_Port(0x14C) & BIT7)
#define mUsbIntDmaErr()			(wFOTGPeri_Port(0x14C) & BIT8)

// Rx 0 byte packet register(0x150)
#define mUsbIntRX0ByteRd()		(INT8U)(wFOTGPeri_Port(0x150))
#define mUsbIntRX0ByteSetClr(set)		(wFOTGPeri_Port(0x150) &= ~((INT32U)set))

// Tx 0 byte packet register(0x154)
#define mUsbIntTX0ByteRd()		(INT8U)(wFOTGPeri_Port(0x154))
#define mUsbIntTX0ByteSetClr(data)		(wFOTGPeri_Port(0x154) &= ~((INT32U)data))

// ISO sequential Error/Abort register(0x158)
#define mUsbIntIsoSeqErrRd()		(INT8U)((wFOTGPeri_Port(0x158) & 0xff0000)>>16)
#define mUsbIntIsoSeqErrSetClr(data)		(wFOTGPeri_Port(0x158) &= ~(((INT32U)data)<<16))

#define mUsbIntIsoSeqAbortRd()	(INT8U)(wFOTGPeri_Port(0x158) & 0xff)
#define mUsbIntIsoSeqAbortSetClr(data)	(wFOTGPeri_Port(0x158) &= ~((INT32U)data))

// IN Endpoint MaxPacketSize register(0x160,0x164,...,0x17C)
#define mUsbEPinHighBandSet(EPn, dir , size )	(wFOTGPeri_Port(0x160 + ((EPn - 1) << 2)) &= ~(BIT14 |BIT13));  (wFOTGPeri_Port(0x160 + ((EPn - 1) << 2)) |= ((((INT8U)(size >> 11)+1) << 13)*(1 - dir)) )
#define mUsbEPMxPtSz(EPn, dir, size)		(wFOTGPeri_Port(0x160 + (dir * 0x20) + ((EPn - 1) << 2)) = (INT16U)(size))
#define mUsbEPMxPtSzClr(EPn, dir)			(wFOTGPeri_Port(0x160 + (dir * 0x20) + ((EPn - 1) << 2)) = 0)

#define mUsbEPinMxPtSz(EPn)		(wFOTGPeri_Port(0x160 + ((EPn - 1) << 2)) & 0x7ff)
#define mUsbEPinStallST(EPn)		((wFOTGPeri_Port(0x160 + ((EPn - 1) << 2)) & BIT11) >> 11)
#define mUsbEPinStallClr(EPn)		(wFOTGPeri_Port(0x160 + ((EPn - 1) << 2)) &= ~BIT11)
#define mUsbEPinStallSet(EPn)		(wFOTGPeri_Port(0x160 + ((EPn - 1) << 2)) |=  BIT11)
#define mUsbEPinRsTgClr(EPn)		(wFOTGPeri_Port(0x160 + ((EPn - 1) << 2)) &= ~BIT12)
#define mUsbEPinRsTgSet(EPn)	(wFOTGPeri_Port(0x160 + ((EPn - 1) << 2)) |=  BIT12)
#define mUsbEPinMxPtSz_Set(EPn,value)		(wFOTGPeri_Port(0x160 + ((EPn - 1) << 2)))= value& 0x7ff

// OUT Endpoint MaxPacketSize register(0x180,0x164,...,0x19C)
#define mUsbEPoutMxPtSz(EPn)	((wFOTGPeri_Port(0x180 + ((EPn - 1) << 2))) & 0x7ff)
#define mUsbEPoutStallST(EPn)	((wFOTGPeri_Port(0x180 + ((EPn - 1) << 2)) & BIT11) >> 11)
#define mUsbEPoutStallClr(EPn)	(wFOTGPeri_Port(0x180 + ((EPn - 1) << 2)) &= ~BIT11)
#define mUsbEPoutStallSet(EPn)	(wFOTGPeri_Port(0x180 + ((EPn - 1) << 2)) |=  BIT11)
#define mUsbEPoutRsTgClr(EPn)	(wFOTGPeri_Port(0x180 + ((EPn - 1) << 2)) &= ~BIT12)
#define mUsbEPoutRsTgSet(EPn)	(wFOTGPeri_Port(0x180 + ((EPn - 1) << 2)) |=  BIT12)
#define mUsbEPoutMxPtSz_Set(EPn,value)		(wFOTGPeri_Port(0x180 + ((EPn - 1) << 2)))= value & 0x7ff

// Endpoint & FIFO Configuration
// Endpoint 1~4 Map register(0x1a0), Endpoint 5~8 Map register(0x1a4)
#define mUsbEPMap(EPn, MAP)	(bFOTGPeri_Port(0x1a0 + (EPn-1)) = MAP)
#define mUsbEPMapRd(EPn)		(bFOTGPeri_Port(0x1a0+ (EPn-1)))
#define mUsbEPMapAllClr()		(wFOTGPeri_Port(0x1a0) = 0);(wFOTGPeri_Port(0x1a4) = 0)
#define mUsbEPMap_0x1A0(MAP)	(wFOTGPeri_Port(0x1a0)= MAP)
// FIFO Map register(0x1a8)
#define mUsbFIFOMap(FIFOn, MAP)	(bFOTGPeri_Port(0x1a8 + FIFOn) = MAP)
#define mUsbFIFOMapRd(FIFOn)		(bFOTGPeri_Port(0x1a8 + FIFOn))
#define mUsbFIFOMapAllClr()			(wFOTGPeri_Port(0x1a8) = 0)
#define mUsbFIFOMap_0x1A8(MAP)	(wFOTGPeri_Port(0x1a8)= MAP)

// FIFO Configuration register(0x1ac)
#define mUsbFIFOConfig(FIFOn, CONFIG)	(bFOTGPeri_Port(0x1ac + FIFOn) = CONFIG)
#define mUsbFIFOConfigRd(FIFOn)			(bFOTGPeri_Port(0x1ac + FIFOn))
#define mUsbFIFOConfigAllClr()		(bFOTGPeri_Port(0x1ac) = 0)
#define FIFOEnBit					0x20
#define mUsbFIFOConfig_0x1AC(MAP)	(wFOTGPeri_Port(0x1ac)= MAP)

// FIFO byte count register(0x1b0)
#define mUsbFIFOOutByteCount(fifo_num)	(((wFOTGPeri_Port(0x1b0+fifo_num*4)&0x7ff)))
#define mUsbFIFODone(fifo_num)			(wFOTGPeri_Port(0x1b0+fifo_num*4) |= BIT11)

// DMA target FIFO register(0x1c0)
#define FOTG200_DMA2FIFO_Non 		0
#define FOTG200_DMA2FIFO0 			BIT0
#define FOTG200_DMA2FIFO1 			BIT1
#define FOTG200_DMA2FIFO2 			BIT2
#define FOTG200_DMA2FIFO3 			BIT3
#define FOTG200_DMA2CxFIFO 		BIT4

#define mUsbDMA2FIFOSel(sel)		(wFOTGPeri_Port(0x1c0) = sel)
#define mUsbDMA2FIFORd()			(wFOTGPeri_Port(0x1c0))

// DMA parameter set 1 (0x1c8)
#define DMA_IO						BIT2
#define mUsbDmaConfig(len,Dir)		(wFOTGPeri_Port(0x1c8) = (((INT32U)len)<<8)|((1-Dir)<<1))
#define mUsbDmaConfigDMA_IO(len,Dir)		(wFOTGPeri_Port(0x1c8) = (((INT32U)len)<<8)|DMA_IO|((1-Dir)<<1))
//#define mUsbDmaLenRd()				((wFOTGPeri_Port(0x1c8) & 0x1fff0000) >> 8)
#define mUsbDmaLenRd()				((wFOTGPeri_Port(0x1c8) & 0x00FFFFFF00) >> 8)
#define mUsbDmaConfigRd()			(wFOTGPeri_Port(0x1c8))
#define mUsbDmaConfigSet(set)		(wFOTGPeri_Port(0x1c8) = set)

#define mUsbDmaStart()				(wFOTGPeri_Port(0x1c8) |= BIT0)
#define mUsbDmaStop()				(wFOTGPeri_Port(0x1c8) &= ~BIT0)

// DMA parameter set 2 (0x1cc)
#define mUsbDmaAddr(addr)			(wFOTGPeri_Port(0x1cc) = addr)
#define mUsbDmaAddrRd()			(wFOTGPeri_Port(0x1cc))

// 8 byte command data port(0x1d0)
#define mUsbEP0CmdDataRdDWord()	(wFOTGPeri_Port(0x1d0))


#endif
