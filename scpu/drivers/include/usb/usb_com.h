#ifndef __USB_H__
#define __USB_H__

void usb_init(void);
void usb_driver_probe(void);

int usb_com_write(uint8_t *BufferPtr, uint32_t BufferLen, uint32_t flags);
int usb_com_read(uint8_t *BufferPtr, uint32_t BufferLen, uint32_t flags);

void usb_done_notify(void);

#endif 
