#ifndef __USB_SCSI_H
#define __USB_SCSI_H
//	#include "usb_pre.h"
//	#include "define.h"
//#include "flib.h"
#include "DrvOTG210_Pe_usb.h"

/* Define USB device mode disk resoure type */
#define USBDEV_iRAM_DISK	1
#define USBDEV_SD_DISK		2
#define USBDEV_SPI_DISK		3/* not support yet */
#define USBDEV_DISK_MODE	USBDEV_iRAM_DISK

#if (USBDEV_DISK_MODE==USBDEV_iRAM_DISK)
//#define FIX_ADDR_RW			1
#endif

// SCSI command operation code
#define SCSI_OP_TEST_UNIT_READY			0x00
#define SCSI_OP_REQUEST_SENSE			0x03
#define SCSI_OP_INQUIRY					0x12
#define SCSI_OP_MODE_SELECT				0x15
#define SCSI_OP_MODE_SENSE				0x1A
#define SCSI_OP_MEDIUM_REMOVAL			0x1E
#define SCSI_OP_READ_FORMAT_CAPACITY	0x23
#define SCSI_OP_READ_CAPACITY			0x25
#define SCSI_OP_READ_10					0x28
#define SCSI_OP_WRITE_10				0x2A
#define SCSI_OP_VERIFY					0x2F

/* For SCSI vendor command code */
//vendor command
#define SCSI_CMD_CODE			0xF1 

//sub commands for SPI Flash
#define SUBCMD_ERASE_ALL_SPI_FLASH			0x11
#define SUBCMD_ERASE_SEC_SPI_FLASH			0x16
#define SUBCMD_ERASE_4K_SPI_FLASH			0x17

#define SUBCMD_WRITE_SPI_FLASH				0x22
#define SUBCMD_READ_SPI_FLASH				0x33
#define SUBCMD_GET_INFO_SPI_FLASH			0x55	//actually, not only to get SPI Flash info but also device info
#define SUBCMD_READ_REG						0x66  //not just only for SPI Flash. It's command to read value from HW registers.
#define SUBCMD_WRITE_REG					0x67  //not just only for SPI Flash .It's command to write value to HW registers.
#define SUBCMD_READ_STATUS_SPI_FLASH		0x68
#define SUBCMD_WRITE_STATUS_SPI_FLASH		0x69


typedef  struct _usb_command {
    UINT8	scsi_cmd;	/* it should always equal U320_SCSI_CMD_CODE */
    UINT8	subcmd;
    UINT8	pad1;
    UINT8	pad2;
    UINT32	offset;
    UINT32	size;
    //UINT8	buffer[4]; //bessel:no used
}USB_COMMAND_T, *pUSB_COMMAND_T;
#define USB_CMD_LEN				(12)


typedef enum {
    MS13Case_1 = 0,
    MS13Case_2_3,
    MS13Case_4,
    MS13Case_5,
    MS13Case_6,
    MS13Case_7_8,
    MS13Case_9,
    MS13Case_10_13,
    MS13Case_11,
    MS13Case_12
} MassStorage13Case;

//
#define CARD_INDEX_NAND		0
#define CARD_INDEX_NAND_SS	1
#define CARD_INDEX_SPI_SS	2
#define CARD_INDEX_SRAM		7
#define CARD_TYPE_MAX_REPORT 1


typedef struct	// SCSI device response
{
    UINT8 u8MemIndex;		// current access memory type
    UINT8 u8Flags;			// In or Out
    UINT32 u32IOAddr;		// block addr in Card controller
    UINT16 u16DataResidue;	// residue (block count for card content, or byte count for information)
    UINT16 u16TfSzCurrent;		// current transfer length (block count for card content, or byte count for information)
} SCSIDeviceResp;

typedef struct SCSI_Sense_Data
{
    UINT8 u8Key;
    UINT8 u8KeyAdd;
} SCSISense;


typedef struct SCSI_Capacity_Header
{
    UINT8	Reserve1;
    UINT8	Reserve2;
    UINT8	Reserve3;
    UINT8	Capacity_List_Length;

} Cap_Head_D;


typedef struct SCSI_Current_Capacity
{
    UINT32	Num_Of_Blocks;
    UINT8	Desc_Code;
    UINT8	Block_Len0;
    UINT8	Block_Len1;
    UINT8	Block_Len2;

} Current_Cap_D;

typedef struct SCSI_Format_Capacity
{
    UINT32	Num_Of_Blocks;
    UINT8	Desc_Code;
    UINT8	Reserved;
    UINT8	Block_Len1;
    UINT8	Block_Len2;

} Format_Cap_D;


typedef struct SCSI_Format_Descriptor
{
    Cap_Head_D	Cap_Head;
    Current_Cap_D	Current_Cap;
    //Format_Cap_D	Format_Cap_1;

} SCSIFormat_D;


// Request Sense data format
#define SENSE_OFFSET_KEY					0x02
#define SENSE_OFFSET_ADD					0x0C
#define SENSE_OFFSET_ADD_QUALIFIER		0x0D

// Sense key
#define KEY_NO_SENSE			0x00
#define KEY_RECOVERED_ERROR	0x01
#define KEY_NOT_READY		0x02
#define KEY_MEDIUM_ERROR		0x03
#define KEY_HARDWARE_ERROR	0x04
#define KEY_ILLEGAL_REQUEST	0x05
#define KEY_UNIT_ATTENTION	0x06
#define KEY_DATA_PROTECT	0x07
#define KEY_BLANK_CHECK		0x08
#define KEY_VENDOR_SPECIFIC	0x09
#define KEY_COPY_ABORTED	0x0A
#define KEY_ABORTED_CMD		0x0B
#define KEY_OBSOLETE			0x0C
#define KEY_VOLUMN_OVERFLOW	0x0D
#define KEY_MISCOMPARE		0x0E
#define KEY_RESERVED			0x0F

// KEY_NO_SENSE: Additional key
#define ADDKEY_NO_ADDITIONAL				0x00

// KEY_NOT_READY: Additional key
#define ADDKEY_LOGICAL_UNIT_NOT_READY	0x04
#define ADDKEY_LOGICAL_UNIT_NOT_SUPPORT	0x25
#define ADDKEY_MEDIUM_NOT_PRESENT		0x3A

// KEY_ILLEGAL_REQUEST: Additional key
#define ADDKEY_INVALID_CMD_OP_CODE		0x20
#define ADDKEY_INVALID_FIELD_IN_CMD 		0x24

// KEY_UNIT_ATTENTION: Additional key
#define ADDKEY_MEDIUM_CHANGED			0x28

// KEY_DATA_PROTECT: Additional key
#define ADDKEY_DATA_PROTECT				0x27

// all response data length
#define DATA_LENGTH_INQUIRY				36
#define DATA_LENGTH_REQUEST_SENSE		18
//	#define DATA_LENGTH_MODE_SENSE			56
#define DATA_LENGTH_MODE_SENSE			8
#define DATA_LENGTH_FORMATCAP		   12
#define DATA_LENGTH_READCAP			  8

#if (DATA_LENGTH_INQUIRY > DATA_LENGTH_REQUEST_SENSE)
#if (DATA_LENGTH_INQUIRY > DATA_LENGTH_MODE_SENSE)
#define DATA_LENGTH_SCSI_RESP	DATA_LENGTH_INQUIRY
#else
#define DATA_LENGTH_SCSI_RESP	DATA_LENGTH_MODE_SENSE
#endif
#else
#if (DATA_LENGTH_REQUEST_SENSE > DATA_LENGTH_MODE_SENSE)
#define DATA_LENGTH_SCSI_RESP	DATA_LENGTH_REQUEST_SENSE
#else
#define DATA_LENGTH_SCSI_RESP	DATA_LENGTH_MODE_SENSE
#endif
#endif


#ifdef USB_SCSI_GLOBALS
#define USB_SCSI_EXT
#else
#define USB_SCSI_EXT extern
#endif

extern  SCSISense tSCSIsense;
extern  SCSIDeviceResp tSCSIDeviceResp;
extern  UINT32 u32Usb_buffer[256/4];	// may less than 256

//USB_SCSI_EXT void vUsbScsiInit(void);
extern MassStorageState eSCSI_CmdDecode(void);
#endif

