#ifndef __V2K_SUBDEV_H__
#define __V2K_SUBDEV_H__


#include <framework/v2k.h> 

#define v2k_subdev_call(sd, f, args...)                 \
    (!(sd) ? -ENODEV : (((sd)->ops && (sd)->ops->f) ?   \
        (sd)->ops->f((sd) , ##args) : -ENOIOCTLCMD))

struct v2k_subdev_enum_fmt {
    u32 index;
    u32 code;
};

struct v2k_subdev_ops;
struct pin_context;
struct v2k_subdev {
    const struct v2k_subdev_ops *ops;
    void *dev_priv;
};

struct v2k_subdev_ops {
    int (*s_power)(struct v2k_subdev *sd, int on);
    int (*s_stream)(struct v2k_subdev *sd, int enable);
    int (*enum_fmt)(struct v2k_subdev *sd, unsigned int index, u32 *fourcc);
    // Optional, can be set to NULL in this stage	
    int (*get_fmt)(struct v2k_subdev *sd, struct v2k_format *format);
    int (*set_fmt)(struct v2k_subdev *sd, struct v2k_format *format);
};

static inline void v2k_set_subdevdata(struct v2k_subdev *sd, void *p) {
    sd->dev_priv = p;
}

static inline void *v2k_get_subdevdata(const struct v2k_subdev *sd) {
    return sd->dev_priv;
}

void v2k_subdev_init(struct v2k_subdev *sd, const struct v2k_subdev_ops *ops);

struct i2c_client;
///* Initialize a v2k_subdev with data from an i2c_client struct */
void v2k_i2c_subdev_init(struct v2k_subdev *sd, struct i2c_client *client,
        const struct v2k_subdev_ops *ops);

#endif
