/*
 * Standard image size definitions
 */
#ifndef __V2K_IMAGE_SIZES_H__
#define __V2K_IMAGE_SIZES_H__

#define TFT43_WIDTH     480
#define TFT43_HEIGHT    272

#define VGA_WIDTH       640
#define VGA_HEIGHT      480

#define HD_WIDTH        1280
#define HD_HEIGHT       720

#define FHD_WIDTH       1920
#define FHD_HEIGHT      1080

#define HMX_RICA_WIDTH  864
#define HMX_RICA_HEIGHT 491

#endif
