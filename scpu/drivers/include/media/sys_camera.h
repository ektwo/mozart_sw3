#ifndef __SYS_CAMERA_H__
#define __SYS_CAMERA_H__

#include <framework/driver.h>
#include <framework/utils.h> 
#include <framework/bitops.h>
#include <media/videodev.h>
#include <media/videobuf_core.h>


//typedef enum {
//    SYS_CAMERA_ORDER_LE,
//    SYS_CAMERA_ORDER_BE,
//} sys_camera_order;

typedef enum {
    SYS_CAMERA_LAYOUT_PACKED = 0,
    SYS_CAMERA_LAYOUT_PLANAR,
} sys_camera_layout;

typedef enum {
    /* @SYS_CAMERA_PACKING_NONE:	no packing, bit-for-bit transfer to RAM, one
           sample represents one pixel */
    SYS_CAMERA_PACKING_NONE,
    /* @SYS_CAMERA_PACKING_2X8_PADHI:	16 bits transferred in 2 8-bit samples, in the
        possibly incomplete byte high bits are padding */
    SYS_CAMERA_PACKING_2X8_PADHI, 
    /* @SYS_CAMERA_PACKING_2X8_PADLO:	as above, but low bits are padding */
    SYS_CAMERA_PACKING_2X8_PADLO,
} sys_camera_packing;

struct sys_camera_pixelfmt {
    unsigned int fourcc;
    unsigned char bits_per_sample;	
    sys_camera_packing  packing;
    //sys_camera_order    order;
    sys_camera_layout	layout;
};


struct v2k_dev_handle;
struct sys_camera_desc;

struct sys_camera_device {
    unsigned int num_user_formats;
    unsigned int user_width;
    unsigned int user_height;
    unsigned int bytesperline;           /* for padding, zero if unused */
    unsigned int sizeimage;
    
    enum v2k_colorspace colorspace;
    enum v2k_field field;

    struct sys_camera_desc *sdesc;
    struct video_device_context *vdc;
    struct pin_context *parent_pin;     /* Camera host pin */ /* to_sys_camera_host */
    struct pin_context *child_pin;      /* E.g., the i2c client.*/ 
    struct sys_camera_format_xlate *user_formats; 
    const struct sys_camera_format_xlate *current_fmt;
    
    struct kdp_list list;               /* list of all registered devices */
    struct video_buf_queue vb_q;

    unsigned char connect_to; 
    unsigned char idx_on_host;
};

struct sys_camera_host {
    unsigned int capabilities;
    void *priv;                         /* i.e kdp520_csi2rx_context */
    struct sys_camera_device *cam_d;    /* Currently attached client */
    struct sys_camera_host_ops *ops;
    struct kdp_list list;
    struct v2k_pin_obj pin_obj;
    char drv_name[16];                  /* capability info	*/    
    unsigned char host_id;  
};
struct sys_camera_host *to_sys_camera_host(const struct pin_context *);

struct sys_camera_host_ops {
    int (*clock_start)(struct sys_camera_host *);
    void (*clock_stop)(struct sys_camera_host *);
    int (*querycap)(struct sys_camera_host *, struct v2k_capability *);		
    int (*set_fmt)(struct sys_camera_device *, struct v2k_format *);	
    int (*set_bus_param)(struct sys_camera_device *);
    int (*init_videobuf)(struct video_buf_queue *, struct sys_camera_device *);
};

struct sys_camera_subdev_desc {
    unsigned int flags;
    void *drv_priv;                     /* sensor driver private platform data */
    int (*power)(struct pin_context *, int);
    int (*reset)(struct pin_context *);
};

struct i2c_board_info;
struct sys_camera_host_desc {
    int connect_to;                   /* Camera bus id, mipi rx1 or mipi rx 2 */
    int i2c_adapter_id;
    struct i2c_board_info *board_info;
};

struct sys_camera_desc {
    struct sys_camera_subdev_desc subdev_desc;
    struct sys_camera_host_desc host_desc;
};

struct sys_camera_link {
    unsigned int flags;
    void *priv;
    /* Optional callbacks to power on or off and reset the sensor */
    int (*power)(struct pin_context *, int);
    int (*reset)(struct pin_context *);

    int connect_to; // mipi rx0, mipi rx1
    int i2c_adapter_id;
    struct i2c_board_info *board_info;
};

struct sys_camera_format_xlate {
    unsigned int fourcc;
    const struct sys_camera_pixelfmt *host_fmt;
};

const struct sys_camera_pixelfmt *sys_camera_get_fmtdesc(unsigned int fourcc);
int sys_camera_bytes_per_line(unsigned int fourcc, unsigned int width);
struct v2k_subdev *sys_camera_to_subdev(const struct sys_camera_device *);
struct sys_camera_subdev_desc *sys_camera_i2c_to_desc(const struct i2c_client *);
int sys_camera_host_register(struct sys_camera_host *);
void sys_camera_host_unregister(struct sys_camera_host *);
int sys_camera_power_on(struct pin_context *, struct sys_camera_subdev_desc *);
int sys_camera_power_off(struct pin_context *, struct sys_camera_subdev_desc *);
const struct sys_camera_format_xlate *sys_camera_xlate_by_fourcc(
        struct sys_camera_device *, unsigned int );

#endif
