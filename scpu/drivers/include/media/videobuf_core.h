#ifndef __VIDEOBUF_CORE_H__
#define __VIDEOBUF_CORE_H__

#include <framework/mutex.h>
#include <framework/atomic.h>
#include <framework/spinlock.h>
#include <framework/kdp_list.h>
#include <framework/v2k.h>


#define VIDEO_MAX_FRAME	(10)

enum video_buf_state {
    VB_STATE_DEQUEUED,
    VB_STATE_PREPARING,
    VB_STATE_PREPARED,
    VB_STATE_QUEUED,
    VB_STATE_ACTIVE,
    VB_STATE_DONE,
    VB_STATE_ERROR,
};

struct video_buf_plane {
    void *mem_priv;
    unsigned int length;
    unsigned int offset;
};

struct video_buf {
    struct v2k_buffer embed_v2k_buffer;
    unsigned int index;
    enum video_buf_state state;
    struct video_buf_queue *vb_queue;
    struct video_buf_plane plane;
    struct kdp_list queued_entry;
    struct kdp_list done_entry;
};

struct videobuf_ops {
    int (*queue_setup)(struct video_buf_queue *q,
            unsigned int *num_buffers,
            unsigned int *num_planes,
            unsigned int *sizes, void **alloc_ctx);
    int (*buf_init)(struct video_buf *vb);
    void (*buf_queue)(struct video_buf *vb);	
    int (*start_streaming)(struct video_buf_queue *q);
    void (*stop_streaming)(struct video_buf_queue *q);    
    void (*wait_prepare)(struct video_buf_queue *q);
    void (*wait_finish)(struct video_buf_queue *q);    
};

struct videobuf_mem_ops {
    void *(*alloc)(void *alloc_ctx, unsigned long size);
};

struct video_buf_queue {
    const struct videobuf_ops *vb_ops;
    const struct videobuf_mem_ops *vb_mem_ops;
    struct video_buf *bufs[VIDEO_MAX_FRAME];    
    
    void *alloc_ctx;
    
    unsigned int buf_struct_size;
    unsigned int num_buffers;
    struct kdp_list queued_list;
    unsigned int queued_count;
    struct kdp_list done_list;
    struct spinlock_t done_lock;
 	struct mutex lock;
    
    unsigned int streaming;//:1;
};

int videobuf_done(struct video_buf *vb, enum video_buf_state state);

int videobuf_reqbufs(struct video_buf_queue *q, struct v2k_requestbuffers *req);
int videobuf_querybuf(struct video_buf_queue *q, struct v2k_buffer *b);
int videobuf_qbuf(struct video_buf_queue *q, struct v2k_buffer *b);
int videobuf_dqbuf(struct video_buf_queue *q, struct v2k_buffer *b, bool nonblocking);
int videobuf_streamon(struct video_buf_queue *q, unsigned long arg);
int videobuf_streamoff(struct video_buf_queue *q, unsigned long arg);

#endif 
