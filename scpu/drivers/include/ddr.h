#ifndef __DDR_H__
#define __DDR_H__

#ifdef MOZART_CHIP
void ddr_init_spl(void);
void ddr_init_chip_0429(void);
#endif

void ddr_init_v35_mipi(unsigned char ncpu_wakeup);
void ddr_init_1228_kingston_v35(unsigned char ncpu_wakeup);
void ddr_init_1222_kingston_v33(unsigned char ncpu_wakeup);
void ddr_init_1222_kingston(unsigned char ncpu_wakeup);
void ddr_init_1218_kingston(unsigned char ncpu_wakeup);
void ddr_init_1212_kingston(void);
void ddr_init_1207_kingston(void);
void ddr_init_1130_kingston(void);
void lpddr2_init(void);

#endif
