#ifndef __SCU_REG_H__
#define __SCU_REG_H__


#include "kneron_mozart.h"
#include "types.h"
#include "io.h"


#define SCU_REG_BTUP_STS                                (SCU_FTSCU100_PA_BASE + 0x0000)
#define SCU_REG_BTUP_CTRL                               (SCU_FTSCU100_PA_BASE + 0x0004)
#define SCU_REG_PWR_CTRL                                (SCU_FTSCU100_PA_BASE + 0x0008)
#define SCU_REG_PWR_MOD                                 (SCU_FTSCU100_PA_BASE + 0x0020)
#define SCU_REG_INT_STS                                 (SCU_FTSCU100_PA_BASE + 0x0024)
#define SCU_REG_PLL_CTRL                                (SCU_FTSCU100_PA_BASE + 0x0030)
#define SCU_REG_PLL2_CTRL                               (SCU_FTSCU100_PA_BASE + 0x0040)
#define SCU_REG_DLL_CTRL                                (SCU_FTSCU100_PA_BASE + 0x0044)
#define SCU_REG_PWR_VCCSTS                              (SCU_FTSCU100_PA_BASE + 0x0048)

/* Boot-up Status Register (Offset: 0x0000) */
#define SCU_REG_BTUP_STS_PWRBTN_STS                     BIT16
/* Boot-up Control Register (Offset: 0x0004) */
#define SCU_REG_BTUP_CTRL_PWRBTN_EN                     BIT16
#define SCU_REG_BTUP_CTRL_GPO_OUT                       BIT0
/* Power Control Register (Offset: 0x0008) */
#define SCU_REG_PWR_CTRL_PWRCTRL_UPDATE                 BIT24
#define SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_DDRCK        BIT10 // DDRCK domain
#define SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_NPU          BIT9 // NPU domain
#define SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_DEFAULT      BIT8 // Default domain
#define SCU_REG_PWR_CTRL_PWRUP_CTRL_MASK                (BIT15|BIT14|BIT13|BIT12|BIT11| \
                                                         SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_DDRCK | \
                                                         SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_NPU | \
                                                         SCU_REG_PWR_CTRL_PWRUP_CTRL_DOMAIN_DEFAULT)

/* Power Mode Register (Offset: 0x0020) */
#define SCU_REG_PWR_MOD_SELFR_CMD_OFF                   BIT31 // DDR/SDR self-refresh command off
#define SCU_REG_PWR_MOD_FCS_PLL2_RSTn                   BIT30 // PLL2 resets in the frequency change sequence
#define SCU_REG_PWR_MOD_FCS_DLL_RSTn                    BIT29 // DLL resets in the frequency change sequence
#define SCU_REG_PWR_MOD_FCS_PLL_RSTn                    BIT28 // PLL resets in the frequency change sequence
#define SCU_REG_PWR_MOD_FCS                             BIT6  // Frequency Change Sequence (FCS)
#define SCU_REG_PWR_MOD_BUS_SPEED                       BIT5  // BUS speed change command
/* PLL Control Register (Offset: 0x0030) */
#define SCU_REG_PLL_CTRL_GET_CLKIN_MUX()                GET_BITS(SCU_EXTREG_USB_OTG_CTRL, 4, 5)
#define SCU_REG_PLL_CTRL_SET_CLKIN_MUX(val)             SET_MASKED_BITS(SCU_EXTREG_USB_OTG_CTRL, val, 4, 5)
#define SCU_REG_PLL_CTRL_CLKIN_MUX_BIT_START            4
#define SCU_REG_PLL_CTRL_CLKIN_MUX_MASK                 BIT4 | BIT5 | BIT6 | BIT7
#define SCU_REG_PLL_CTRL_PLLEN                          BIT0

#define SCU_REG_PLL_CTRL_SET_PLLEN(val)                 SET_MASKED_BIT(SCU_REG_DLL_CTRL, val, 0)

/* Software Reset Mask Register (Offset: 0x0040) */
#define SCU_REG_PLL2_CTRL_PLL2EN                        BIT0
#define SCU_REG_PLL2_CTRL_SET_PLL2EN(val)               SET_MASKED_BIT(SCU_REG_PLL2_CTRL, val, 0)
/* Software Reset Mask Register (Offset: 0x0044) */
#define SCU_REG_DLL_CTRL_DLLEN                          BIT0
#define SCU_REG_DLL_CTRL_SET_DLLEN(val)                 SET_MASKED_BIT(SCU_REG_DLL_CTRL, val, 0)
/* Power Domain Voltage Supplied Status Register (Offset: 0x0048) */
#define SCU_REG_PWR_VCCSTS_GET_PWR_READY()              GET_BITS(SCU_REG_PWR_VCCSTS, 0, 2)
#define SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_DDRCK       BIT2
#define SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_NPU         BIT1
#define SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_DEFAULT     BIT0
#define SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_MASK        (SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_DDRCK | \
                                                         SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_NPU | \
                                                         SCU_REG_PWR_VCCSTS_PWR_READY_DOMAIN_DEFAULT)







#endif
