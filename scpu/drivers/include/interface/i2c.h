#ifndef __I2C_H__
#define __I2C_H__


#include <framework/driver.h>


#define I2C_NAME_SIZE   16
#define I2C_MASTER_READ 0x01 /* read data, from slave to master */

struct i2c_msg {
    u16 addr;   /* slave address */
    u8 flags;
    u8 len;     /* msg length */
    u8 *buf;    /* pointer to msg data */
};

struct i2c_protocol;
struct i2c_adapter {
    int host_id;
    int retries;
    const struct i2c_protocol *protocol; /* the algorithm to access the bus */
    struct pin_context pin_ctx;
};

struct i2c_client;
struct i2c_driver {
    int (*probe)(struct i2c_client *);
    int (*remove)(struct i2c_client *);

    struct core_device *core_dev;
    struct driver_context driver;
};
#define to_i2c_driver(__adap__) container_of(__adap__, struct i2c_driver, driver)

struct i2c_client {
    struct i2c_adapter *adapter;        /* the adapter we sit on	*/
    struct pin_context pin_ctx;         /* the pin_context structure */ // sys_camera_device->control
    unsigned short addr;                /* chip address - NOTE: 7bit */
};
#define to_i2c_client(d) container_of(d, struct i2c_client, pin_ctx)

struct i2c_protocol {
    int (*master_xfer)(struct i2c_adapter *adap, struct i2c_msg *msgs,
        int num);
};

struct i2c_board_info {
    char name[I2C_NAME_SIZE];
    unsigned short addr;
    void *platform_data;
};

int i2c_driver_register(struct i2c_driver *);
void i2c_driver_unregister(struct i2c_driver *);
int i2c_transfer(struct i2c_adapter *adap, struct i2c_msg *msgs, int num);
int i2c_add_adapter(struct i2c_adapter *);
struct i2c_adapter *i2c_get_adapter(int nr);

#define KDP_I2C_DRIVER_SETUP(__i2c_driver) \
    DRIVER_SETUP(__i2c_driver, 3, i2c_driver_register, i2c_driver_unregister)

#endif 
