#include <config/board_kdp520.h>
#if V2K_CAM_ENABLE==SENSOR_HMXRICA_RAW8
#include <stdlib.h> 
#include <framework/init.h>
#include <framework/driver.h>
#include <framework/v2k.h>
#include <framework/errno.h>
#include <interface/i2c.h>
#include <media/sys_camera.h>
#include <media/v2k_subdev.h>
#include <media/v2k_image_sizes.h>
#include <utility.h>
#include <dbg.h>


struct hmxrica_datafmt {
    u32 fourcc;
    enum v2k_colorspace colorspace;
};

struct hmxrica_context {
    struct v2k_subdev subdev;
    const struct hmxrica_datafmt *fmt;
};

struct hmxrica_win_size {
    u32 width;
    u32 height;
};

static const struct hmxrica_win_size hmxrica_supported_win_sizes[] = {
    { .width = HMX_RICA_WIDTH, .height = HMX_RICA_HEIGHT, },
};

struct init_seq {
    u16 addr;
    u8 value;
};

static struct init_seq hmxrica_init_regs[] = {
    {0xC005, 0x49}, 
    {0xC073, 0x65}, 
    //pll PLL_O 200MHZ, MCLK is 24, where MCU SPI clock = 25MHz.
    {0xC092, 0x42}, 
    {0xC093, 0x64}, 
    //pll PLL_R 200MHZ, where Uploader SPI clock = 50MHz
    {0xC0B9, 0x01}, 
    //{0xC0A2, 0x42},   //dont need to set it , since it is default.
    {0xC0A3, 0x64}, 
    {0xC0AA, 0x06}, 
    {0xC0BF, 0x03}, 
    {0xC0A0, 0x01}, 
    {0xC0BF, 0x03}, 
    {0xC0A0, 0x03}, 
    {0xC0BF, 0x03}, 
    //enable N9
    {0xc004, 0x00}, 
    {0xc0bf, 0x01}, 
    {0xc003, 0x00}, 
    {0xc0b6, 0x01},         // register for selecting group of AE Init Parameater.
    {0xc07F, 0x01},         // register for selecting designated package to boot.
    {0xc0bf, 0x01}, 
    {0xc249, 0x000},
    //{0xc24A, 0x011},      //interleave 11
    //{0xc24B, 0x011}, 
    //{0xc24C, 0x011}, 
    {0xc24A, 0x012},        //interleave 12
    {0xc24B, 0x012},        //interleave 12
    {0xc24C, 0x012},        //interleave 12
    {0xC0BF, 0x003}, 
    // Modified new test test pattern
    {0xC026, 0x007}, 
    {0xC0BF, 0x003}, 
    {0x0, 0x0},
};

static int hmxrica_write_reg(struct i2c_client *client, u16 reg, u8 data)
{
    int ret;
    u8 paddr[3] = { (reg >> 8) & 0xff, reg & 0XFF, data };

    struct i2c_msg msgs[] = {
        {
            .addr = client->addr,
            .flags = 0,
            .len = 3,
            .buf = paddr,
        }
    };

    ret = i2c_transfer(client->adapter, msgs, 1);

    return ret;
}

//static int hmxrica_read_reg(struct i2c_client *client, u16 reg, u8 *data)
//{
//	int ret;
//    u8 paddr[2] = { (reg >> 8) & 0xff, reg & 0XFF };

//	struct i2c_msg msgs[] = {
//		{
//			.addr = client->addr,
//			.flags = 0,
//			.len = 2,
//			.buf = paddr,
//		},
//		{
//			.addr = client->addr,
//			.flags = I2C_M_RD,
//			.len = 1,
//			.buf = data,
//		},
//	};

//	ret = i2c_transfer(client->adapter, msgs, 2);

//	return ret;
//}

void hmxrica_init(struct i2c_client *i2c_client, struct init_seq *seq)
{
       struct i2c_client *client = i2c_client;
       struct init_seq *init_fnc_ptr;
    
    for (init_fnc_ptr = seq; ; ++init_fnc_ptr) {
        if(init_fnc_ptr->addr == 0 && init_fnc_ptr->value == 0) break; //reaches end
          hmxrica_write_reg(client, init_fnc_ptr->addr ,init_fnc_ptr->value);
    }	
}

static const struct hmxrica_datafmt hmxrica_colour_fmts[] = {
    { V2K_PIX_FMT_GREY, V2K_COLORSPACE_RAW },
};

static struct hmxrica_context *to_hmxrica(const struct i2c_client *client)
{
    return container_of(dev_get_drvdata(&client->pin_ctx), struct hmxrica_context, subdev);
}

static const struct hmxrica_win_size *hmxrica_select_win(u32 *width, u32 *height)
{
    int i, default_size = ARRAY_SIZE(hmxrica_supported_win_sizes) - 1;

    for (i = 0; i < ARRAY_SIZE(hmxrica_supported_win_sizes); i++) {
        if (hmxrica_supported_win_sizes[i].width  >= *width &&
            hmxrica_supported_win_sizes[i].height >= *height) {
            *width = hmxrica_supported_win_sizes[i].width;
            *height = hmxrica_supported_win_sizes[i].height;
            return &hmxrica_supported_win_sizes[i];
        }
    }

    *width = hmxrica_supported_win_sizes[default_size].width;
    *height = hmxrica_supported_win_sizes[default_size].height;
    return &hmxrica_supported_win_sizes[default_size];
}

static
int hmxrica_set_params(struct i2c_client *client, u32 *width, u32 *height, u32 fourcc)
{
    struct hmxrica_context *ctx = to_hmxrica(client);

    switch (fourcc) {	
        case V2K_PIX_FMT_RGB565:
            DSG("%s: Selected V2K_PIX_FMT_RGB565", __func__);
            break;

        case V2K_PIX_FMT_GREY:
            DSG("%s: Selected V2K_PIX_FMT_GREY", __func__);
            break;
        default:;
    }

    /* reset hardware */
    //hmxrica_reset(client);

    /* initialize the sensor with default settings */

    hmxrica_init(client, hmxrica_init_regs);

    return 0;
}

static int hmxrica_s_power(struct v2k_subdev *sd, int on)
{
    struct i2c_client *client = v2k_get_subdevdata(sd);
    struct sys_camera_subdev_desc *ssdd = sys_camera_i2c_to_desc(client);
    //struct hmxrica_context *ctx = to_hmxrica(client);
    int ret;

    if (!on)
        return sys_camera_power_off(&client->pin_ctx, ssdd);

    ret = sys_camera_power_on(&client->pin_ctx, ssdd);
    if (ret < 0)
        return ret;

    return ret;
}

static int hmxrica_s_stream(struct v2k_subdev *sd, int enable)
{
    struct i2c_client *client = v2k_get_subdevdata(sd);

    return 0;
}

static int hmxrica_enum_fmt(struct v2k_subdev *sd, unsigned int index, u32 *code)
{
    if (index >= ARRAY_SIZE(hmxrica_colour_fmts))
        return -EINVAL;

    *code = hmxrica_colour_fmts[index].fourcc;
    return 0;
}

static int hmxrica_get_fmt(struct v2k_subdev *sd, struct v2k_format *format)
{
    struct i2c_client *client = v2k_get_subdevdata(sd);

    return 0;
}

static int hmxrica_set_fmt(struct v2k_subdev *sd, struct v2k_format *format)
{
    struct v2k_format *fmt = format;
    struct i2c_client *client = v2k_get_subdevdata(sd);

    //DSG("<%s:%s>", __FILE__, __func__);

    hmxrica_select_win(&fmt->width, &fmt->height);

    return hmxrica_set_params(client, &fmt->width, &fmt->height, fmt->pixelformat);
}

static int hmxrica_video_probe(struct i2c_client *client)
{
    struct hmxrica_context *ctx = to_hmxrica(client);	

    int ret = hmxrica_s_power(&ctx->subdev, 1);

    if (0 == ret)
        hmxrica_s_power(&ctx->subdev, 0);

    return ret;
}

static struct v2k_subdev_ops hmxrica_subdev_ops = {
    .s_power    = hmxrica_s_power,
    .s_stream   = hmxrica_s_stream,
    .enum_fmt   = hmxrica_enum_fmt,
    .get_fmt    = hmxrica_get_fmt,
    .set_fmt    = hmxrica_set_fmt,
};


static int hmxrica_probe(struct i2c_client *client)
{
    int ret;    
    struct hmxrica_context *ctx;
    struct sys_camera_subdev_desc *ssdd = sys_camera_i2c_to_desc(client);

    if (!ssdd) {
        DSG("hmxrica_probe: missing platform data!");
        return -EINVAL;
    }

    ctx = calloc(1, sizeof(struct hmxrica_context));
    if (!ctx)
        return -ENOMEM;

    v2k_i2c_subdev_init(&ctx->subdev, client, &hmxrica_subdev_ops);

    ctx->fmt = &hmxrica_colour_fmts[0];

    ret = hmxrica_video_probe(client);
    if (ret) {

    } else {
    //  DSG("hmxrica Probed");
    }

    return ret;
}

static int hmxrica_remove(struct i2c_client *client)
{
    struct sys_camera_subdev_desc *ssdd = sys_camera_i2c_to_desc(client);
    struct hmxrica_context *ctx = to_hmxrica(client);

    //if (ssdd->free_bus)
    //	ssdd->free_bus(ssdd);

    return 0;
}

extern struct core_device hmxrica_link_device;
struct i2c_driver hmxrica_i2c_driver = {
    .driver = {
        .name = "sensor-hmxrica",
    },
    .probe      = hmxrica_probe,
    .remove     = hmxrica_remove,
    .core_dev   = &hmxrica_link_device,
};
KDP_I2C_DRIVER_SETUP(hmxrica_i2c_driver);

#endif
