#include "scu_extreg.h"
#include <framework/driver.h>
#include "DrvPWMTMR010.h"


static int _lcdc_driver_probe(struct driver_context2 *drv_ctx, struct pin_context *pin_ctx) 
{    

    return 0;
}

static int _lcdc_driver_reset(struct driver_context2 *drv_ctx, struct pin_context *pin_ctx) 
{
    SCU_EXTREG_SWRST_SET_LCDC_resetn(1);

    outw(SCU_EXTREG_SWRST_MASK1, 
         SCU_EXTREG_SWRST_MASK1_AResetn_u_FTLCDC210 |
         SCU_EXTREG_SWRST_MASK1_PRESETn_u_FTLCDC210 |
         SCU_EXTREG_SWRST_MASK1_TV_RSTn_FTLCDC210 |
         SCU_EXTREG_SWRST_MASK1_LC_SCALER_RSTn_FTLCDC210 |
         SCU_EXTREG_SWRST_MASK1_LC_RSTn_FTLCDC210);
    
    return 0;
}

static struct driver_context2 lcdc_driver = {
    .probe = _lcdc_driver_probe,
    .reset = _lcdc_driver_reset,
};
