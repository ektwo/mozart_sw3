#if 1//def V2K_CAM_ENABLE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmsis_os2.h>
#include <kneron_mozart.h>
#include <io.h>
#include <config/board_kdp520.h>
#include <framework/init.h>
#include <framework/ioport.h>
#include <framework/errno.h>
#include <framework/mm.h>
#include <framework/driver.h>
#include "scu_extreg.h"
#include "lcdc/lcdc.h"
#include "clock.h"
#include "dbg.h"


struct pip_window
{
	int x;
	int y;
	int width;
	int height;
};

struct kdp520_lcdc_data {
    u32 xres, yres;         /* resolution of screen in pixels */
    u32 fb_phys;
};

static struct kdp520_lcdc_data kdp520_lcdc_default_data = {
    .xres = 640,
    .yres = 480,
};

#define MAX_FRAME_NUM 4
struct kdp520_lcdc_context {

    struct fb_info info; /* FB driver info record */

    u8 *base;
    void *alloc_ctx;    

    u32 xres;
    u32 yres;

    enum lcd_img_input_fmt input_fmt;
    u32 frame_num;
    u32 pip_num;
    u32 frame_width[MAX_FRAME_NUM];
    u32 frame_height[MAX_FRAME_NUM];
    u32 pip_width[MAX_FRAME_NUM];
    u32 pip_height[MAX_FRAME_NUM];
    u32 frame_size[MAX_FRAME_NUM];
    u32 frame_buffer[MAX_FRAME_NUM];
};
#define to_kdp520_lcdc_drvdata(_info) \
    container_of(_info, struct kdp520_lcdc_context, info)
  

extern void delay_ms(unsigned int count);

int gval = 0;
void LCDC_VSTATUS_IRQHandler(void)
{
	//int status = inw(LCDC_REG_INTR_STATUS);
	//if(status & LCDC_REG_INTR_STATUS_IntVstatus)
	{		
		//outw(LCDC_REG_INTR_CLEAR, 0xf);
        //LCDC_REG_INTR_CLEAR_SET_ClrVstatus();

        
		//outw(LCDC_REG_PANEL_IMAGE0_FRAME0, gval);
        //DSG("gval s =%x", gval);
	}
}

void lcdc_vstatus_isr(void)
{
	int status = inw(LCDC_REG_INTR_STATUS);
	if(status & LCDC_REG_INTR_STATUS_IntVstatus)
	{		
		LCDC_REG_INTR_CLEAR_SET_ClrVstatus();	
        
		outw(LCDC_REG_PANEL_IMAGE0_FRAME0, gval);
        //DSG("gval l =%x", gval);
	}
}

static void ddr_zeros(volatile u32 start, u32 size) 
{
    u32 end = start + size;
    while (start < end) {
        outw(start, 0x0);
          start += 4;
    }
}

static void _kdp520_lcdc_set_panel_type(enum lcdc_panel_type type)
{
    switch (type) {
    case lcdc_6bits_per_channel:
        LCDC_REG_PANEL_PIXEL_SET_PanelType(0);
        break;
    case lcdc_8bits_per_channel:
        LCDC_REG_PANEL_PIXEL_SET_PanelType(1);
        break;
    default:;
    }

}

static void _kdp520_lcdc_set_endian(enum lcdc_fb_data_endianness_type type)
{
    switch (type) {
    case lcdc_fb_data_endianness_lblp:
        LCDC_REG_PANEL_PIXEL_SET_Endian(0);
        break;
    case lcdc_fb_data_endianness_bbbp:
        LCDC_REG_PANEL_PIXEL_SET_Endian(1);
        break;
    case lcdc_fb_data_endianness_lbbp:
        LCDC_REG_PANEL_PIXEL_SET_Endian(2);
        break;
    default:;
    }
}

static void _kdp520_lcdc_set_bgrsw(enum lcdc_output_fmt_sel type)
{
    switch (type) {
    case lcdc_output_fmt_sel_rgb:
        LCDC_REG_PANEL_PIXEL_SET_BGRSW(0);
        break;
    case lcdc_output_fmt_sel_bgr:
        LCDC_REG_PANEL_PIXEL_SET_BGRSW(1);
        break;
    default:;
    }
}

void kdp520_lcdc_set_divno(u32 div)
{
    LCDC_REG_POLARITY_CTRL_SET_DivNo(div);
}

void kdp520_lcdc_set_framerate(int frame_rate, u32 width, u32 height) 
{
#define LCD_PLL 148500000
    u32 div;
    u32 serial_mode = LCDC_REG_SERIAL_PANEL_PIXEL_GET_SerialMode();
    u32 upper_margin = LCDC_REG_VERTICAL_BACK_PORCH_GET_VBP() + 1;
    u32 lower_margin = LCDC_REG_VERTICAL_TIMING_CTRL_GET_VFP();
    u32 vsync_len = LCDC_REG_VERTICAL_TIMING_CTRL_GET_VW() + 1;
    u32 left_margin = LCDC_REG_HORIZONTAL_TIMING_CTRL_GET_HBP() + 1;
    u32 right_margin = LCDC_REG_HORIZONTAL_TIMING_CTRL_GET_HFP() + 1;
    u32 hsync_len = LCDC_REG_HORIZONTAL_TIMING_CTRL_GET_HW() + 1;   
    u32 pixel_clock = (serial_mode ? 3 : 1 ) * frame_rate *
        (height + upper_margin + lower_margin + vsync_len) *
        (width + left_margin + right_margin + hsync_len);
        
    DSG("[lcdc] Set_FrameRate upper_margin=%d, lower_margin=%d, vsync_len = %d", upper_margin, lower_margin,vsync_len);
    DSG("[lcdc] Set_FrameRate left_margin=%d, right_margin=%d, hsync_len = %d", left_margin, right_margin,hsync_len);
    
    div = LCD_PLL / pixel_clock;
    DSG("[lcdc] Set_FrameRate div=0x%x, fbi->input_clock=%d, pclk = %d", div, LCD_PLL,pixel_clock);
    if (div < 1)
    {
        DSG("invalid input clock for LCD");
        for (;;) ;
    }
    if((height == 1080) && (width == 1920))
        div = 0;
    else if ((height == 480) && (width == 640))
        div = 1;
    else
        div = 5;

    kdp520_lcdc_set_divno(div);
}

static
u32 _lcdc_get_bpp_from_img_pixfmt(enum lcdc_img_pixfmt pixfmt) 
{
    u32 bpp = 0;
    switch (pixfmt) {
    case lcdc_img_pixfmt_1bpp: bpp = 0; break;
    case lcdc_img_pixfmt_2bpp: bpp = 1; break;
    case lcdc_img_pixfmt_4bpp: bpp = 2; break;
    case lcdc_img_pixfmt_8bpp: bpp = 3; break;
    case lcdc_img_pixfmt_16bpp: bpp = 4; break;
    case lcdc_img_pixfmt_24bpp: bpp = 5; break;
    case lcdc_img_pixfmt_argb888: bpp = 6; break;
    case lcdc_img_pixfmt_argb1555: bpp = 7; break;
    default: bpp = 4; break;
    }
}

void kdp520_lcdc_set_img_fmt(struct lcdc_img_pixfmt_pxp *fmt)
{
    u32 val = inw(LCDC_REG_PIPPOP_FMT_1);
    u32 bpp0 = _lcdc_get_bpp_from_img_pixfmt(fmt->pixfmt_img0);
    u32 bpp1 = _lcdc_get_bpp_from_img_pixfmt(fmt->pixfmt_img1) << 4;
    u32 bpp2 = _lcdc_get_bpp_from_img_pixfmt(fmt->pixfmt_img2) << 8;
    u32 bpp3 = _lcdc_get_bpp_from_img_pixfmt(fmt->pixfmt_img3) << 12;

    //val |= bpp0 | bpp1 | bpp2 | bpp3;
    val = bpp0 | bpp1 | bpp2 | bpp3;
    outw(LCDC_REG_PIPPOP_FMT_1, val);
}

static
void _lcdc_decide_frame_dim(
        struct kdp520_lcdc_context *context)    
{
    struct kdp520_lcdc_context *ctx = context;
    int i;

    for (i = 0; i < ctx->frame_num; ++i)
    {
        ctx->frame_width[i] = ctx->xres;
        ctx->frame_height[i] = ctx->yres;

        if (ctx->frame_num > 1)
        {
            if (i>0)
            {
                //fbi->frame_width[i] = fbi->pip_width[i-1]*(float)fbi->frame_width[i]/(float)fbi->panel_width;
                ctx->frame_width[i] = ctx->pip_width[i];		
                //fbi->frame_height[i] = fbi->pip_height[i-1]*(float)fbi->frame_height[i]/(float)fbi->panel_height;
                ctx->frame_height[i] = ctx->pip_height[i];
            }
        }
    }
}

static
void _lcdc_decide_frame_num(
        struct kdp520_lcdc_context *context)
{
    struct kdp520_lcdc_context *ctx = context;

    if (lcd_img_input_fmt_rgb565 == ctx->input_fmt || 
        lcd_img_input_fmt_rgb555 == ctx->input_fmt || 
        lcd_img_input_fmt_rgb444 == ctx->input_fmt || 
        lcd_img_input_fmt_rgb24 == ctx->input_fmt || 
        lcd_img_input_fmt_ycbcr422 == ctx->input_fmt)
    {
        if (ctx->pip_num > 0)
        {
            ctx->frame_num = ctx->pip_num + 1;
        }
        else
        {
            ctx->frame_num = 1;
        }
    }
    else
    {
        ctx->frame_num = 1;
    }
}

#define FRAME_SIZE_RGB(xres,yres,mbpp) ((xres) * (yres) * (mbpp) / 8)
static
int _lcdc_calc_framesize(
        int frame_width, int frame_height, enum lcd_img_input_fmt input_fmt)
{
    switch(input_fmt)
    {
        case lcd_img_input_fmt_rgb565:
            return FRAME_SIZE_RGB(frame_width, frame_height, 16);

        default:
            break;
    }
    return 0;
}

int _lcdc_set_framebase(struct kdp520_lcdc_context *context, 
        u16 image, u32 frame_buffer_addr)
{
    switch (image) {
        case 0: outw(LCDC_REG_PANEL_IMAGE0_FRAME0, frame_buffer_addr); break;
        case 1: outw(LCDC_REG_PANEL_IMAGE1_FRAME0, frame_buffer_addr); break;
        case 2: outw(LCDC_REG_PANEL_IMAGE2_FRAME0, frame_buffer_addr); break;
        case 3: outw(LCDC_REG_PANEL_IMAGE3_FRAME0, frame_buffer_addr); break;
        default:;
    }

    return 0;
}

static
void _lcdc_alloc_framebuffer(
        struct kdp520_lcdc_context *context)
{
    struct kdp520_lcdc_context *ctx = context;
    int i;
    int color[]={0x00ffff, 0x00008888, 0x00004444, 0x00002222};
#ifdef V2K_CAM_ENABLE    
    const struct mm_struct *mm = get_mm_struct();
#endif    
    _lcdc_decide_frame_dim(ctx);
    _lcdc_decide_frame_num(ctx);

    for (i = 0; i < ctx->frame_num; ++i) {
        ctx->frame_size[i] = _lcdc_calc_framesize(
            ctx->frame_width[i], ctx->frame_height[i], ctx->input_fmt);
            
        switch(i){
        case 0: 
        #ifdef V2K_CAM_ENABLE
            ctx->frame_buffer[i] = (mm->csirx0_mm_phy + mm->mmap->vm_start);
            DSG("[lcdc] %s i=%d mm->csirx0_mm_phy=0x%x mm->mmap->vm_start=0x%x", __func__, i, mm->csirx0_mm_phy, mm->mmap->vm_start);
        #else
            ctx->frame_buffer[i] = LCD_FRAMEBUFFER_BASE;
        #endif
            break;
        case 1:
        #ifdef V2K_CAM_ENABLE
            ctx->frame_buffer[i] = (mm->csirx0_mm_phy_2 + mm->mmap->vm_start);
        #else
            ctx->frame_buffer[i] = LCD_FRAMEBUFFER_BASE1;
        #endif
            break;
        case 2:
        #ifdef V2K_CAM_ENABLE
            ctx->frame_buffer[i] = (mm->csirx1_mm_phy + mm->mmap->vm_start);
        #else
            ctx->frame_buffer[i] = LCD_FRAMEBUFFER_BASE2;
        #endif
            break;
        case 3:
        #ifdef V2K_CAM_ENABLE
            ctx->frame_buffer[i] = (mm->csirx1_mm_phy_2 + mm->mmap->vm_start);
        #else
            ctx->frame_buffer[i] = LCD_FRAMEBUFFER_BASE3;
        #endif
            break;
        }

        memset((void*)(ctx->frame_buffer[i]), color[i], ctx->frame_size[i]);

        DSG("%s i=%d ctx->frame_buffer[i]=0x%x", __func__, i, ctx->frame_buffer[i]);
        _lcdc_set_framebase(ctx, i, ctx->frame_buffer[i]);
    }
}

static
void _kdp520_lcdc_set_img_input_fmt(struct kdp520_lcdc_context *context)
{    
    struct kdp520_lcdc_context *ctx = context;
    u32 pix_param;
    int i;

    _lcdc_alloc_framebuffer(ctx);

    pix_param = inw(LCDC_REG_PANEL_PIXEL);
    switch(ctx->input_fmt)
    {
        case lcd_img_input_fmt_rgb565:
            LCDC_REG_FUNC_ENABLE_SET_EnYCbCr(0);
            pix_param = (pix_param & ~(LCDC_REG_PANEL_PIXEL_RGBTYPE_MASK |
                                       LCDC_REG_PANEL_PIXEL_BppFifo_MASK) ) | 
                                       LCDC_REG_PANEL_PIXEL_BppFifo_16bpp | 
                                       LCDC_REG_PANEL_PIXEL_RGBTYPE_565;
            break;

        default:
            break;
    }    

    outw(LCDC_REG_PANEL_PIXEL, pix_param);
}

static
void _lcdc_init_rgb_value()
{
	int i;
    for(i = 0; i < 0x100; i++)
    {
        writeb(i, LCDC_REG_LT_OF_GAMMA_RED + i);
        writeb(i, LCDC_REG_LT_OF_GAMMA_GREEN + i);
        writeb(i, LCDC_REG_LT_OF_GAMMA_BLUE + i);
    }
}

static 
void _lcdc_pip_update(struct kdp520_lcdc_context *context)
{
    LCDC_REG_PIP_SUBPIC1_POS_SET_PiP_Update(1);
}

static
void _lcdc_set_pip_pos(struct kdp520_lcdc_context *context, int pip_idx, u32  h_pos, u32 v_pos)
{
    struct kdp520_lcdc_context *ctx = context;

    switch (pip_idx) {
    case 0:
        LCDC_REG_PIP_SUBPIC1_POS_SET_PiP1Hpos(0);
        LCDC_REG_PIP_SUBPIC1_POS_SET_PiP1Vpos(0);
        LCDC_REG_PIP_SUBPIC1_POS_SET_PiP1Hpos(h_pos);
        LCDC_REG_PIP_SUBPIC1_POS_SET_PiP1Vpos(v_pos);
        break;

    case 1:
        LCDC_REG_PIP_SUBPIC2_POS_SET_PiP1Hpos(0);
        LCDC_REG_PIP_SUBPIC2_POS_SET_PiP1Vpos(0);
        LCDC_REG_PIP_SUBPIC2_POS_SET_PiP1Hpos(h_pos);
        LCDC_REG_PIP_SUBPIC2_POS_SET_PiP1Vpos(v_pos);
        break;
    case 2:
        break;
    default:;
    }

    LCDC_REG_PIP_SUBPIC1_POS_SET_PiP_Update(1);
}

static
void _lcdc_set_pip_dim(struct kdp520_lcdc_context *context, int pip_idx, u32 h_dim, u32 v_dim)
{
    struct kdp520_lcdc_context *ctx = context;

    ctx->pip_width[pip_idx] = h_dim;
    ctx->pip_height[pip_idx] = v_dim;

    switch (pip_idx) {
    case 0:
        LCDC_REG_PIP_SUBPIC1_DIM_SET_PiP1Hdim(0);
        LCDC_REG_PIP_SUBPIC1_DIM_SET_PiP1Vdim(0);
        LCDC_REG_PIP_SUBPIC1_DIM_SET_PiP1Hdim(h_dim);
        LCDC_REG_PIP_SUBPIC1_DIM_SET_PiP1Vdim(v_dim);
        break;

    case 1:
        LCDC_REG_PIP_SUBPIC1_DIM_SET_PiP2Hdim(0);
        LCDC_REG_PIP_SUBPIC1_DIM_SET_PiP2Vdim(0);

        LCDC_REG_PIP_SUBPIC1_DIM_SET_PiP2Hdim(h_dim);
        LCDC_REG_PIP_SUBPIC1_DIM_SET_PiP2Vdim(v_dim);    
        break;
    case 2:
        break;
    default:;
    }
} 

static
int _lcdc_set_pip_enable(struct kdp520_lcdc_context *context, u32 pip_num)
{
    struct kdp520_lcdc_context *ctx = context;

    ctx->pip_num = pip_num;
    ctx->frame_num = pip_num + 1;

    LCDC_REG_FUNC_ENABLE_SET_PiPEn(0);
    LCDC_REG_FUNC_ENABLE_SET_PiPEn(pip_num);

    _lcdc_alloc_framebuffer(ctx);

    return 1;
}

void _lcdc_set_pip_alpha_blending(
        struct kdp520_lcdc_context *context, 
        enum alpha_blending_type type, int blend_l, int blend_h)
{
    struct kdp520_lcdc_context *ctx = context;

    switch (type) {
    case alpha_blending_none:
        LCDC_REG_FUNC_ENABLE_SET_BlendEn(0);
        break;
    case alpha_blending_global:
        LCDC_REG_PIP_BLENDING_SET_PiPBlend_l(blend_l);
        LCDC_REG_PIP_BLENDING_SET_PiPBlend_h(blend_h);
        LCDC_REG_FUNC_ENABLE_SET_BlendEn(1);
        break;
    case alpha_blending_pixel:
        LCDC_REG_FUNC_ENABLE_SET_BlendEn(2);
        break;
    }
}

void _lcdc_pip_test(struct kdp520_lcdc_context *context)
{
    struct kdp520_lcdc_context *ctx = context;

    int i;
    int pip_num=2;
    int hstep1=1;
    int vstep1=1;
    int hstep2=1;
    int vstep2=1;	
    u32 panel_width = ctx->xres;
    u32 panel_height = ctx->yres;
    struct pip_window osd_win1 = {150, 150, 8, 8};	
    struct pip_window osd_win2 = {50, 50, 8, 8};	
    

    _lcdc_set_pip_pos(ctx, 0, 50, 50);
    _lcdc_set_pip_pos(ctx, 1, 50+50, 50+50);

    _lcdc_set_pip_dim(ctx, 0, ctx->xres >> 2, ctx->yres >> 2);	
    _lcdc_set_pip_dim(ctx, 1, ctx->xres >> 2, ctx->yres >> 2);	

    _lcdc_set_pip_enable(ctx, pip_num);	
    _lcdc_set_pip_alpha_blending(ctx, alpha_blending_none, 0, 0);

    delay_ms(1000);
    
    while(1){		
        osd_win1.x += hstep1;
        osd_win1.y += vstep1;
        _lcdc_set_pip_pos(ctx, 0, osd_win1.x, osd_win1.y);	
        if((osd_win1.x +(panel_width >> 2)) > (panel_width - 1)){
            hstep1 = -1;
        }
        if(osd_win1.y + (panel_height >> 2) > (panel_height - 1)){
            vstep1 = -1;
        }
        if(osd_win1.x == 0){
            hstep1 = 1;
        }
        if(osd_win1.y == 0){
            vstep1 = 1;
        }	

        osd_win2.x += hstep2;
        osd_win2.y += vstep2;
        _lcdc_set_pip_pos(ctx, 1, osd_win2.x, osd_win2.y);
        if((osd_win2.x +(panel_width >> 2)) > (panel_width - 1)){
            _lcdc_set_pip_alpha_blending(ctx, alpha_blending_none, 0x3, 0x1);
            hstep2 = -1;
        }
        if(osd_win2.y + (panel_height >> 2) > (panel_height - 1)){
            _lcdc_set_pip_alpha_blending(ctx, alpha_blending_none, 0, 0x6);
            vstep2 = -1;
        }
        if(osd_win2.x == 0){
            _lcdc_set_pip_alpha_blending(ctx, alpha_blending_none, 0, 0x7);
            hstep2 = 1;
        }
        if(osd_win2.y == 0){
            _lcdc_set_pip_alpha_blending(ctx, alpha_blending_none, 6, 0x0);
            vstep2 = 1;
        }				

        delay_ms(10);
        
        _lcdc_pip_update(ctx);
    }
}

static 
int kdp520_fb_blank(int blank_mode, struct fb_info *fbi)
{
    //struct kdp520fb_drvdata *drvdata = to_kdp520_lcdc_drvdata(fbi);

    /* to Do */

    return 0; 
}

#define IP_IRQ_NUM 4
#include "clock.h"
extern struct clock_node clock_node_lcdc;
extern struct faradayfb_info lcd_fbi;
extern int Init_LCD(struct faradayfb_info *fbi, unsigned int io_base, int irq[IP_IRQ_NUM], int panel_idx, int mode);
extern void Do_PiP_Test(struct faradayfb_info *fbi, int mode );
static int kdp520_lcdc_probe(struct core_device *core_d)
{
    int size;
    struct kdp520_lcdc_context *ctx;
    struct mm_struct *mm = get_mm_struct();
    struct fb_platform_data *pdata = (struct fb_platform_data *)(core_d->pin_ctx.platform_data);
    struct kdp520_lcdc_data data = kdp520_lcdc_default_data;    
    int err;
    
    DSG("<%s>", __func__);

    delay_ms(50);

    struct clock_value clock_val;
    memset(&clock_val, 0, sizeof(clock_val));
    clock_val.u.enable = TRUE;
    clock_mgr_open(&clock_node_lcdc, &clock_val);

    ctx = calloc(1, sizeof(*ctx));
    if (!ctx) {
        return -ENOMEM;
    }

    memset(&ctx->info, 0, sizeof(ctx->info));
    //physical size
    //data.screen_width_mm = 480;
    //data.screen_height_mm = 272;
    //resolution
    ctx->xres = pdata->xres;
    ctx->yres = pdata->yres;
    ctx->input_fmt = pdata->input_fmt;
    //virtual resolution
    //data.xvirt = 480;
    //data.yvirt = 272;

    dev_set_drvdata(&core_d->pin_ctx, (void*)ctx);

    ctx->base = (void*)driver_get_ioport_setting(core_d, IORESOURCE_MEM)->start;
    {
        u32 fbsize = _lcdc_calc_framesize(data.xres, data.yres, ctx->input_fmt);
        struct videobuf_dm_ctx *p = calloc(1, sizeof(struct videobuf_dm_ctx));

        p->allocated_length = 0;
        //ddr_zeros(p->phy_start_addr, fbsize);
        //ddr_zeros(p->vir_start_addr, fbsize);
        
        ctx->alloc_ctx = (void*)p;
    }
#if 1
    LCDC_REG_FUNC_ENABLE_SET_LCDon(0);
    LCDC_REG_FUNC_ENABLE_SET_LCDen(0);
    LCDC_REG_PANEL_PIXEL_SET_UpdateSrc(0);

    DSG("LCDC_REG_PANEL_PIXEL_GET_Vcomp()=%x", LCDC_REG_PANEL_PIXEL_GET_Vcomp());

    
    _kdp520_lcdc_set_panel_type(lcdc_6bits_per_channel);
    _kdp520_lcdc_set_bgrsw(lcdc_output_fmt_sel_rgb); //RGB
    //LCDC_REG_PATGEN_SET_Img0PatGen(0);

    //horizontal timing control
    LCDC_REG_HORIZONTAL_TIMING_CTRL_SET_HBP(1);
    LCDC_REG_HORIZONTAL_TIMING_CTRL_SET_HFP(1);
    LCDC_REG_HORIZONTAL_TIMING_CTRL_SET_HW(40);
    LCDC_REG_HORIZONTAL_TIMING_CTRL_SET_PL((pdata->xres >> 4) - 1);

    //vertical timing control
    LCDC_REG_VERTICAL_TIMING_CTRL_SET_VFP(2);
    LCDC_REG_VERTICAL_TIMING_CTRL_SET_VW(9);
    LCDC_REG_VERTICAL_TIMING_CTRL_SET_LF(pdata->yres - 1);

    //vertical back porch
    //DSG("vbp=%x", LCDC_REG_VERTICAL_BACK_PORCH_GET_VBP());
    LCDC_REG_VERTICAL_BACK_PORCH_SET_VBP(2);
    //DSG("vbp2=%x", LCDC_REG_VERTICAL_BACK_PORCH_GET_VBP());
#endif
    //polarity control
//    DSG("LCDC_REG_POLARITY_CTRL_GET_DivNo()=%x", LCDC_REG_POLARITY_CTRL_GET_DivNo());
//    DSG("LCDC_REG_POLARITY_CTRL_GET_IDE()=%x", LCDC_REG_POLARITY_CTRL_GET_IDE());
//    DSG("LCDC_REG_POLARITY_CTRL_GET_ICK()=%x", LCDC_REG_POLARITY_CTRL_GET_ICK());
//    DSG("LCDC_REG_POLARITY_CTRL_GET_IHS()=%x", LCDC_REG_POLARITY_CTRL_GET_IHS());
//    DSG("LCDC_REG_POLARITY_CTRL_GET_IVS()=%x", LCDC_REG_POLARITY_CTRL_GET_IVS());
    LCDC_REG_POLARITY_CTRL_SET_DivNo(1);
    LCDC_REG_POLARITY_CTRL_SET_IDE(1);
    LCDC_REG_POLARITY_CTRL_SET_ICK(1);
    LCDC_REG_POLARITY_CTRL_SET_IHS(1);
    LCDC_REG_POLARITY_CTRL_SET_IVS(1);

    //serial panel pixel
    LCDC_REG_SERIAL_PANEL_PIXEL_SET_AUO052(0);
    LCDC_REG_SERIAL_PANEL_PIXEL_SET_LSR(0);
    LCDC_REG_SERIAL_PANEL_PIXEL_SET_ColorSeq(2);
    LCDC_REG_SERIAL_PANEL_PIXEL_SET_DeltaType(0);
    LCDC_REG_SERIAL_PANEL_PIXEL_SET_SerialMode(0);

    //TV control
    LCDC_REG_TV_SET_ImgFormat(0);
    LCDC_REG_TV_SET_PHASE(0);
    LCDC_REG_TV_SET_OutFormat(0);

    kdp520_lcdc_set_framerate(60, pdata->xres, pdata->yres);

    //LCDC_REG_FUNC_ENABLE_SET_LCDon(0);
    //LCDC_REG_FUNC_ENABLE_SET_LCDen(0);



    //LCD Image Color Management
    outw(LCDC_REG_COLOR_MGR_0, 0x2000);
    outw(LCDC_REG_COLOR_MGR_1, 0x2000);
    outw(LCDC_REG_COLOR_MGR_2, 0x0);
    outw(LCDC_REG_COLOR_MGR_3, 0x40000);
    //LCDC_REG_COLOR_MGR_3_SET_Contr_slope(4);    

    //FrameBuffer Parameter
    outw(LCDC_REG_FRAME_BUFFER, 0x0);

    //Ignore some transformation
    outw(LCDC_REG_BANDWIDTH_CTRL, 0x0);

    //reset the scalar control register	
    outw(LCDC_REG_SCAL_MISC, 0x0);

    //clear all interrupt status
    outw(LCDC_REG_INTR_CLEAR, 0x0f);
    
    //enable buserr / vstatus / next frame / fifo underrun interrupt
    outw(LCDC_REG_INTR_ENABLE_MASK, 0x0f);

    {
        struct lcdc_img_pixfmt_pxp img_pixfmt;
        if (lcd_img_input_fmt_rgb565 == ctx->input_fmt) {
            img_pixfmt.pixfmt_img0 = lcdc_img_pixfmt_16bpp;
            img_pixfmt.pixfmt_img1 = lcdc_img_pixfmt_16bpp;
            img_pixfmt.pixfmt_img2 = lcdc_img_pixfmt_16bpp;
            img_pixfmt.pixfmt_img3 = lcdc_img_pixfmt_16bpp;
        }
        kdp520_lcdc_set_img_fmt(&img_pixfmt);
    }
    {
        // set FIFO thread
        outw(LCDC_REG_FIFO_THRESHOLD, 0x04040404);       
    }

    _kdp520_lcdc_set_img_input_fmt(ctx); 

    _kdp520_lcdc_set_endian(lcdc_fb_data_endianness_lblp);

    LCDC_REG_FUNC_ENABLE_SET_LCDen(1);
    LCDC_REG_FUNC_ENABLE_SET_LCDon(1);

    if(lcd_img_input_fmt_rgb565 == ctx->input_fmt)
        _lcdc_init_rgb_value();
    
    NVIC_SetVector((IRQn_Type)LCDC_FTLCDC210_VSTATUS_IRQ, (uint32_t)lcdc_vstatus_isr);
    NVIC_EnableIRQ(LCDC_FTLCDC210_VSTATUS_IRQ);
    
    return 0;
}

static int kdp520_lcdc_remove(struct core_device *core_d)
{
    struct kdp520_lcdc_context *ctx = dev_get_drvdata(&core_d->pin_ctx);
    //kdp520_fb_blank(0, &drvdata->info);

    LCDC_REG_FUNC_ENABLE_SET_LCDon(0);
    LCDC_REG_FUNC_ENABLE_SET_LCDen(0);

    return 0;
}

static int kdp520_lcdc_reset(struct core_device *core_d)
{

    SCU_EXTREG_SWRST_SET_LCDC_resetn(1);

    // outw(SCU_EXTREG_SWRST_MASK1, 
    //      SCU_EXTREG_SWRST_MASK1_AResetn_u_FTLCDC210 |
    //      SCU_EXTREG_SWRST_MASK1_PRESETn_u_FTLCDC210 |
    //      SCU_EXTREG_SWRST_MASK1_TV_RSTn_FTLCDC210 |
    //      SCU_EXTREG_SWRST_MASK1_LC_SCALER_RSTn_FTLCDC210 |
    //      SCU_EXTREG_SWRST_MASK1_LC_RSTn_FTLCDC210);
    
    return 0;
}

static int kdp520_lcdc_pip_test(struct core_device *core_d)
{
    struct kdp520_lcdc_context *ctx = dev_get_drvdata(&core_d->pin_ctx);
    
    DSG("%s", __func__);
    
    _lcdc_pip_test(ctx);    
}    


extern struct core_device kdp520_lcdc;
//static 
//struct core_driver kdp520_lcdc_driver = {
struct lcdc_driver kdp520_lcdc_driver = {
    .driver     = {
        .name   = "kdp520_lcdc",
    },
    .probe      = kdp520_lcdc_probe,
    .remove     = kdp520_lcdc_remove,
    .reset      = kdp520_lcdc_reset,
    .core_dev   = &kdp520_lcdc,
    
    .pip_test   = kdp520_lcdc_pip_test,
};
//KDP_CORE_DRIVER_SETUP_2(kdp520_lcdc_driver, 5);

#endif
