#ifdef V2K_CAM_ENABLE
/*
 * @name : sys_camera.c
 * @brief : camera image capture (abstract) bus driver
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utility.h>
#include <config/board_kdp520.h>
#include <framework/init.h>
#include <framework/kdp_list.h>
#include <framework/utils.h>
#include <framework/bitops.h>
#include <framework/driver.h>
#include <framework/errno.h>
#include <interface/i2c.h>
#include <media/videodev.h>
#include <media/videobuf_core.h>
#include <media/v2k_subdev.h>
#include <media/v2k_ioctl.h>
#include <media/v2k_image_sizes.h>
#include <media/sys_camera.h>
#include <dbg.h>


#define CAMERA_NUMS_LIMIT 1
// #define MAP_MAX_NUM 2
// static unsigned long device_map[BITS_TO_LONGS(MAP_MAX_NUM)];

struct kdp_list hosts = { &(hosts), &(hosts) };
struct kdp_list devices = { &(devices), &(devices) };

extern struct kdp_list g_klist_drivers;

static int _sys_camera_probe(struct sys_camera_host *, struct sys_camera_device *);
static int _sys_camera_probe_finish(struct sys_camera_device *);
static void _scan_add_host(struct sys_camera_host *);

static inline struct sys_camera_desc *to_sys_camera_desc(
        const struct sys_camera_device *cam_d) {
    return cam_d->sdesc;
}

static inline struct pin_context *to_sys_camera_control(
        const struct sys_camera_device *cam_d) {
    return cam_d->child_pin;
}
        
static const struct sys_camera_pixelfmt sys_camera_fmt[] =
{
    {
        .fourcc             = V2K_PIX_FMT_RGB565,
        .bits_per_sample    = 8,
        .packing            = SYS_CAMERA_PACKING_2X8_PADHI,
        //.order              = SYS_CAMERA_ORDER_LE,
        .layout             = SYS_CAMERA_LAYOUT_PACKED,
    },
    {
        .fourcc             = V2K_PIX_FMT_GREY,
        .bits_per_sample    = 8,
        .packing            = SYS_CAMERA_PACKING_NONE,
        //.order              = SYS_CAMERA_ORDER_LE,
        .layout             = SYS_CAMERA_LAYOUT_PACKED,
    },
};

const struct sys_camera_pixelfmt *sys_camera_get_fmtdesc(unsigned int fourcc) {
    int n = ARRAY_SIZE(sys_camera_fmt);
    int i;
    for (i = 0; i < n; ++i)
        if (sys_camera_fmt[i].fourcc == fourcc)
            return &sys_camera_fmt[i];
    return NULL;
}

int sys_camera_bytes_per_line(unsigned int fourcc, unsigned int width) {
    const struct sys_camera_pixelfmt *p = sys_camera_get_fmtdesc(fourcc);
    switch (p->packing) {
        case SYS_CAMERA_PACKING_NONE:
            return width * p->bits_per_sample / 8;
        case SYS_CAMERA_PACKING_2X8_PADHI:
        case SYS_CAMERA_PACKING_2X8_PADLO:
            return width * 2;
        default:;
    }
    return 0;
}

struct sys_camera_host *to_sys_camera_host(const struct pin_context *pin_ctx) {
    struct v2k_pin_obj *pin_obj = dev_get_drvdata(pin_ctx);
    return container_of(pin_obj, struct sys_camera_host, pin_obj);
    //return container_of(pin_ctx, struct sys_camera_host, pin_ctx);
}

struct v2k_subdev *sys_camera_to_subdev(const struct sys_camera_device *cam_d) {
    struct pin_context *pin_ctx = to_sys_camera_control(cam_d);
    return dev_get_drvdata(pin_ctx);
}

struct sys_camera_subdev_desc *sys_camera_i2c_to_desc(
        const struct i2c_client *client) {
     return (struct sys_camera_subdev_desc*)(client->pin_ctx.platform_data);
}

int sys_camera_host_register(struct sys_camera_host *cam_h)
{
    int ret;
    struct sys_camera_host *sch;
    
    DSG("<%s:%s>", __FILE__, __func__);
    
    if (!sch || !sch->ops ||
        !sch->ops->set_fmt ||
        !sch->ops->querycap ||
        !sch->ops->init_videobuf)
        return -EINVAL;

    struct kdp_list *head = &hosts;
DSG("<%s:%s> 1", __FILE__, __func__);
    for (sch = kdp_list_first_entry(head, typeof(*sch), list);
         &sch->list != head;
         sch = kdp_list_next_entry(sch, list))
    {
        if (sch->host_id == cam_h->host_id) {
            //shouldn't happen
            ret = -EBUSY;
            goto edevreg;
        }
    }
DSG("<%s:%s> 2", __FILE__, __func__);
    dev_set_drvdata(cam_h->pin_obj.pin_ctx, &cam_h->pin_obj);

    kdp_list_add_tail(&hosts, &cam_h->list);
DSG("<%s:%s> 3", __FILE__, __func__);
    _scan_add_host(cam_h);
DSG("<%s:%s> 4", __FILE__, __func__);
    return 0;

edevreg:

    return ret;
}

void sys_camera_host_unregister(struct sys_camera_host *cam_h)
{
    dev_set_drvdata(cam_h->pin_obj.pin_ctx, NULL);
    cam_h->pin_obj.pin_ctx = NULL;
}

int sys_camera_power_on(struct pin_context *pin_ctx, 
        struct sys_camera_subdev_desc *ssdd)
{
    int ret;

    if (ssdd->power) {
        ret = ssdd->power(pin_ctx, 1);
        if (ret < 0) {
        }
    }

    return 0;
}

int sys_camera_power_off(struct pin_context *pin_ctx, 
        struct sys_camera_subdev_desc *ssdd)
{
    int ret = 0;
    int err;

    if (ssdd->power) {
        err = ssdd->power(pin_ctx, 0);
        if (err < 0) {
            ret = err;
        }
    }

    return ret;
}

static int _sys_camera_power_on(struct sys_camera_device *cam_d)
{
    struct v2k_subdev *sd = sys_camera_to_subdev(cam_d);
    int ret;

    ret = v2k_subdev_call(sd, s_power, 1);
    if (ret < 0 && ret != -ENOIOCTLCMD && ret != -ENODEV)
        return ret;

    return 0;
}

static int _sys_camera_power_off(struct sys_camera_device *cam_d)
{
    struct v2k_subdev *sd = sys_camera_to_subdev(cam_d);
    int ret;

    ret = v2k_subdev_call(sd, s_power, 0);
    if (ret < 0 && ret != -ENOIOCTLCMD && ret != -ENODEV)
        return ret;

    return 0;
}

static int _sys_camera_set_fmt(struct sys_camera_device *cam_d,
        struct v2k_format *format)
{
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);
    struct v2k_format *fmt = format;
    int ret;
    DSG("%s %s", __FILE__, __func__);
    ret = cam_h->ops->set_fmt(cam_d, fmt);

    if (ret < 0) {
        return ret;
    } else if (!cam_d->current_fmt || 
                cam_d->current_fmt->host_fmt->fourcc != fmt->pixelformat) {
//		DSG("Host driver hasn't set up current format correctly");
        return -EINVAL;
    }
    DSG("%s width=%d height=%d", __func__, fmt->width, fmt->height);
    cam_d->user_width     = fmt->width;
    cam_d->user_height    = fmt->height;
    //cam_d->bytesperline = fmt->bytesperline;
    //cam_d->sizeimage    = fmt->sizeimage;
    cam_d->bytesperline   = sys_camera_bytes_per_line(fmt->pixelformat, cam_d->user_width);
    cam_d->sizeimage      = cam_d->bytesperline * cam_d->user_height;
    cam_d->colorspace     = (enum v2k_colorspace)fmt->colorspace;
    cam_d->field          = (enum v2k_field)fmt->field;

    /* set physical bus parameters */
    return cam_h->ops->set_bus_param(cam_d);
}
    
static int sys_camera_clock_start(struct sys_camera_host *cam_h)
{
    int ret;

    if (!cam_h->ops->clock_start)
        return 0;

    ret = cam_h->ops->clock_start(cam_h);

    return ret;
}

static void sys_camera_clock_stop(struct sys_camera_host *cam_h)
{
    if (!cam_h->ops->clock_stop)
        return;

    cam_h->ops->clock_stop(cam_h);
}
const struct sys_camera_format_xlate *sys_camera_xlate_by_fourcc(
        struct sys_camera_device *cam_d, unsigned int fourcc)
{
    unsigned int i;
    for (i = 0; i < cam_d->num_user_formats; i++) {
        if (cam_d->user_formats[i].host_fmt->fourcc == fourcc)
            return cam_d->user_formats + i;
    }
    return NULL;
}

static int _sys_camera_g_fmt_vid_cap(struct v2k_dev_handle *handle, 
        void *priv, struct v2k_format *format)
{
    struct sys_camera_device *cam_d = handle->private_data;
    struct v2k_format *fmt = format;
    
	DSG("<%s:%s>", __FILE__, __func__);

    fmt->width          = cam_d->user_width;
    fmt->height         = cam_d->user_height;
    fmt->bytesperline   = cam_d->bytesperline;
    fmt->sizeimage      = cam_d->sizeimage;
    fmt->field          = cam_d->field;
    fmt->pixelformat    = cam_d->current_fmt->host_fmt->fourcc;
    fmt->colorspace     = cam_d->colorspace;

    return 0;
}

static int _sys_camera_s_fmt_vid_cap(struct v2k_dev_handle *handle, 
        void *priv, struct v2k_format *f)
{
    struct sys_camera_device *cam_d = handle->private_data;
    int ret;

    //DSG("<%s:%s>", __FILE__, __func__);

    if (priv != handle->private_data)
        return -EINVAL;

    ret = _sys_camera_set_fmt(cam_d, f);

    return ret;
}

static int _sys_camera_reqbufs(struct v2k_dev_handle *handle, 
        void *priv, struct v2k_requestbuffers *p)
{
    struct sys_camera_device *cam_d = handle->private_data;

    return videobuf_reqbufs(&cam_d->vb_q, p);
}

static int _sys_camera_querybuf(struct v2k_dev_handle *handle, 
        void *priv, struct v2k_buffer *p)
{
    struct sys_camera_device *cam_d = handle->private_data;

    return videobuf_querybuf(&cam_d->vb_q, p);
}

static int _sys_camera_qbuf(struct v2k_dev_handle *handle, 
        void *priv, struct v2k_buffer *p)
{
    struct sys_camera_device *cam_d = handle->private_data;

    if (priv != handle->private_data)
        return -EINVAL;

    return videobuf_qbuf(&cam_d->vb_q, p);
}

static int _sys_camera_dqbuf(struct v2k_dev_handle *handle, 
        void *priv, struct v2k_buffer *p)
{
    struct sys_camera_device *cam_d = handle->private_data;

    if (priv != handle->private_data)
        return -EINVAL;

    return videobuf_dqbuf(&cam_d->vb_q, p, false);
}


static int sys_camera_init_user_formats(struct sys_camera_device *cam_d)
{
    struct v2k_subdev *sd = sys_camera_to_subdev(cam_d);
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);
    unsigned int i, fmts = 0;
    u32 fourcc;

    while (!v2k_subdev_call(sd, enum_fmt, fmts, &fourcc))
        fmts++;

    if (!fmts)
        return -ENXIO;	

    cam_d->user_formats = calloc(fmts, sizeof(struct sys_camera_format_xlate));
    if (!cam_d->user_formats)
        return -ENOMEM;

    //DSG("<%s> Found %d supported formats.", __func__, fmts);

    for (i = 0; i < fmts; i++) {
        v2k_subdev_call(sd, enum_fmt, i, &fourcc);
        cam_d->user_formats[i].host_fmt = sys_camera_get_fmtdesc(fourcc);
    }
    cam_d->num_user_formats = fmts;
    cam_d->current_fmt = &cam_d->user_formats[0];

    return 0;
}

static void _sys_camera_free_user_formats(struct sys_camera_device *cam_d)
{
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);

    cam_d->current_fmt = NULL;
    cam_d->num_user_formats = 0;
    if (cam_d->user_formats)
        free(cam_d->user_formats);
    cam_d->user_formats = NULL;
}

static void _sys_camera_remove_device(struct sys_camera_device *cam_d)
{
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);

    sys_camera_clock_stop(cam_h);

    cam_h->cam_d = NULL;
}

static int _sys_camera_open(struct v2k_dev_handle *handle)
{
    struct video_device_context *vdc = video_devdata(handle); 
    struct sys_camera_device *cam_d;
    struct sys_camera_host *cam_h;
    int ret;

    if (!vdc) {
        return -ENODEV;
    }

    cam_d = dev_get_drvdata(&vdc->pin_ctx);
    cam_h = to_sys_camera_host(cam_d->parent_pin);

    if (!to_sys_camera_control(cam_d)) {
        /* No device driver attached */
        ret = -ENODEV;
        goto econtrol;
    }

    {
        struct sys_camera_desc *cam_desc = to_sys_camera_desc(cam_d);
        if (cam_desc->subdev_desc.reset)
                cam_desc->subdev_desc.reset(cam_d->child_pin);

        ret = sys_camera_clock_start(cam_h);
        if (ret < 0) {
            // DSG("Couldn't activate the camera: %d", ret);
            goto eclkstart;
        }

        ret = _sys_camera_power_on(cam_d);
        if (ret < 0)
            goto epower;

        ret = cam_h->ops->init_videobuf(&cam_d->vb_q, cam_d);
        if (ret < 0)
            goto einitvb;

    }

    handle->private_data = cam_d;

    DSG("camera device open");

    return 0;

einitvb:

    _sys_camera_power_off(cam_d);
epower:
    _sys_camera_remove_device(cam_d);
eclkstart:

econtrol:

    return ret;
}

static int _sys_camera_close(struct v2k_dev_handle *handle)
{
    return 0;
}

static int _sys_camera_querycap(struct v2k_dev_handle *handle, 
        void  *priv, struct v2k_capability *cap)
{
    struct sys_camera_device *cam_d = handle->private_data;
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);

    //DSG("<%s:%s>", __FILE__, __func__);

    strncpy(cap->driver, cam_h->drv_name, sizeof(cap->driver) - 1);
    //cap->driver[strlen(cap->driver) - 1] = '\0';

    int ret = cam_h->ops->querycap(cam_h, cap);

    return ret;
}

static int _sys_camera_streamon(struct v2k_dev_handle *handle, 
        void *priv, unsigned long arg)
{
    struct sys_camera_device *cam_d = handle->private_data;
    struct v2k_subdev *sd = sys_camera_to_subdev(cam_d);
    int ret;

    ret = videobuf_streamon(&cam_d->vb_q, arg);
    if (!ret)
        v2k_subdev_call(sd, s_stream, 1);

    return ret;
}

static int _sys_camera_streamoff(struct v2k_dev_handle *handle, 
        void *priv, unsigned long arg)
{
//    struct sys_camera_device *cam_d = handle->private_data;
//    struct v2k_subdev *sd = sys_camera_to_subdev(cam_d);
//    int ret;

//    if (cam_d->streamer != handle)
//        return -EBUSY;

//    ret = videobuf_streamoff(&cam_d->vb_q, arg);

//    v2k_subdev_call(sd, s_stream, 0);

    return 0;//ret;
}

static void _scan_add_host(struct sys_camera_host *cam_h)
{
    struct sys_camera_device *cam_d;
    struct kdp_list *head = &devices;
         
    DSG("<%s:%s>", __FILE__, __func__);
    
    for (cam_d = kdp_list_first_entry(head, typeof(*cam_d), list);
         &cam_d->list != head;
         cam_d = kdp_list_next_entry(cam_d, list))
    {
        if (cam_d->connect_to == cam_h->host_id) {
            if ((0 == _sys_camera_probe(cam_h, cam_d)) &&
                (0 == _sys_camera_probe_finish(cam_d))) {
                break;
            }
        }
    }    
}

static int sys_camera_i2c_init(struct sys_camera_device *cam_d,
        struct sys_camera_desc *sdesc)
{
    int ret = -ENODEV;    
    struct i2c_client *client;
    struct i2c_adapter *adap;
    struct v2k_subdev *sd;
    struct sys_camera_subdev_desc *p_camera_subdev_desc = &sdesc->subdev_desc;
    struct sys_camera_host_desc *cam_desc_h = &sdesc->host_desc;
    struct i2c_board_info *info = cam_desc_h->board_info;

    DSG("<%s:%s>", __FILE__, __func__);
    
    adap = i2c_get_adapter(cam_desc_h->i2c_adapter_id); 
DSG("adap <%p>", adap);
    info->platform_data = (void*)p_camera_subdev_desc;
DSG("info->platform_data <%p>", info->platform_data);
    client = (struct i2c_client *)calloc(1, sizeof(*client));
    if (client) {
        struct i2c_driver *i2c_drv;
        client->adapter = adap;
        client->pin_ctx.platform_data = info->platform_data; //(void*)p_camera_subdev_desc
        client->addr = info->addr;
        DSG("cam_desc_h->board_info->name <%s>", cam_desc_h->board_info->name);
        
        i2c_drv = to_i2c_driver(find_driver(cam_desc_h->board_info->name));
        DSG("i2c_drv <%p>", i2c_drv);
        DSG("i2c_drv->probe <%p>", i2c_drv->probe);
        if (i2c_drv->probe)
            ret = i2c_drv->probe(client);
        if (0 == ret)
        {
            sd = dev_get_drvdata(&client->pin_ctx);
            if (sd) {
                client = v2k_get_subdevdata(sd);
                cam_d->child_pin = &client->pin_ctx;
            }
        }
        else {
            free(client);
            client = NULL;   
            ret = -ENODEV;
        } 
    }
    return ret;    
}

//static void sys_camera_i2c_free(struct sys_camera_device *cam_d)
//{
//    struct i2c_client *client =
//        to_i2c_client(to_sys_camera_control(cam_d));

//    struct sys_camera_subdev_desc *ssdd;

//    cam_d->control = NULL;

//    free(ssdd);
//}

static int _sys_camera_device_register(struct sys_camera_device *cam_d)
{
    struct sys_camera_device *scd;
    //int num = -1, i;
    int i;
    int num = 0;

    struct kdp_list *head = &devices;

    for (i = 0; i < CAMERA_NUMS_LIMIT && num < 0; i++) {
        num = i;
        for (scd = kdp_list_first_entry(head, typeof(*scd), list);
                &scd->list != head;
                scd = kdp_list_next_entry(scd, list))
        {
            if (scd->connect_to == cam_d->connect_to && scd->idx_on_host == i) {
                num = -1;
                break;
            }
        }
    }


    cam_d->idx_on_host = num;

    kdp_list_add_tail(&devices, &cam_d->list);

    return 0;
}

static const struct v2k_dev_operations sys_camera_dev_ops = {
    .open       = _sys_camera_open,
    .release    = _sys_camera_close,
    .ioctl      = video_default_ioctl,
};

static const struct v2k_ioctl_ops sys_camera_ioctl_ops = {
    .vioc_querycap      = _sys_camera_querycap,
    .vioc_g_fmt_vid_cap = _sys_camera_g_fmt_vid_cap,
    .vioc_s_fmt_vid_cap = _sys_camera_s_fmt_vid_cap,
    .vioc_reqbufs       = _sys_camera_reqbufs,
    .vioc_querybuf      = _sys_camera_querybuf,
    .vioc_qbuf          = _sys_camera_qbuf,
    .vioc_dqbuf         = _sys_camera_dqbuf,
    .vioc_streamon      = _sys_camera_streamon,
    .vioc_streamoff     = _sys_camera_streamoff,
};

static int _sys_camera_probe(
        struct sys_camera_host *cam_h, struct sys_camera_device *cam_d)
{
    int ret;
    struct sys_camera_desc *cam_desc = to_sys_camera_desc(cam_d);
    struct sys_camera_host_desc *cam_desc_h = &cam_desc->host_desc; //sys_camera_link->host_desc
    struct video_device_context *vdc;

    DSG("<%s:%s>", __FILE__, __func__);
    
    vdc = calloc(1, sizeof(struct video_device_context));
    if (!vdc) {
        ret = -ENOMEM;
        goto errvdc;
    }

    vdc->dev_ops   = &sys_camera_dev_ops;
    vdc->ioctl_ops = &sys_camera_ioctl_ops;
    cam_d->parent_pin = cam_h->pin_obj.pin_ctx; 
    cam_d->vdc = vdc;

    if (cam_desc_h->board_info) {
        ret = sys_camera_i2c_init(cam_d, cam_desc);
        if (ret < 0 && ret != -EPROBE_DEFER)
            goto eadd;
    }

    return 0;

eadd:
    if (cam_d->vdc) {
        video_device_release(cam_d->vdc);
        cam_d->vdc = NULL;
    }
errvdc:
    return ret;
}

static int _sys_camera_probe_finish(struct sys_camera_device *cam_d)
{
    int ret;
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);
    struct v2k_subdev *sd = sys_camera_to_subdev(cam_d); //kdp520_i2c
    
    DSG("<%s>", __func__);
    
    ret = sys_camera_init_user_formats(cam_d);
    if (0 == ret) {
        cam_d->field = V2K_FIELD_ANY;

        dev_set_drvdata(&cam_d->vdc->pin_ctx, cam_d);
        ret = video_device_register(cam_d->vdc, cam_d->connect_to);
        if (ret < 0) {
            _sys_camera_free_user_formats(cam_d);
            _sys_camera_remove_device(cam_d);
        }
    }
    else {
        _sys_camera_remove_device(cam_d);
    }

    return ret;
}

static int _sys_camera_core_probe(struct core_device *core_d) 
{
    struct sys_camera_desc *cam_desc = (struct sys_camera_desc *)core_d->pin_ctx.platform_data; //sys_camera_link
    struct sys_camera_device *cam_d;

    DSG("<%s:%s>", __FILE__, __func__);

    cam_d = (struct sys_camera_device *)calloc(1, sizeof(*cam_d));
    if (!cam_d)
        return -ENOMEM;

    cam_d->connect_to = cam_desc->host_desc.connect_to; //sensor_link : connect_to
    cam_d->sdesc = cam_desc;

    dev_set_drvdata(&core_d->pin_ctx, (void*)cam_d);

//#if LANEL == 1
    cam_d->user_width = TFT43_WIDTH;
    cam_d->user_height = TFT43_HEIGHT;    
//#else    
//    cam_d->user_width = VGA_WIDTH;
//    cam_d->user_height = VGA_HEIGHT;    
//#endif    
    
    return _sys_camera_device_register(cam_d);
}

static int _sys_camera_core_remove(struct core_device *core_d)
{
    struct sys_camera_device *cam_d = dev_get_drvdata(&core_d->pin_ctx); 

    int i;

    if (!cam_d)
        return -EINVAL;
    
    i = core_d->uuid;
    if (i < 0)
        i = 0;

    //clear_bit(i, device_map);
    kdp_list_del(&cam_d->list);

    return 0;
}

extern struct core_device kdp520_camera_dev;
static struct core_driver sys_camera_core = {
    .probe      = _sys_camera_core_probe,
    .remove     = _sys_camera_core_remove,
    .driver = {
        .name   = SYS_CAMERA_CORE_NAME,
    },
    .core_dev   = &kdp520_camera_dev,
};
KDP_CORE_DRIVER_SETUP(sys_camera_core, 4);

#endif

