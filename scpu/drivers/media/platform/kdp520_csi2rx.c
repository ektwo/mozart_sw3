#if 1//def V2K_CAM_ENABLE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cmsis_os2.h>
#include <kneron_mozart.h>
#include <utility.h>
#include <io.h>
#include <config/board_kdp520.h>
#include <framework/init.h>
#include <framework/ioport.h>
#include <framework/kdp_list.h>
#include <framework/errno.h>
#include <framework/mm.h>
#include <framework/spinlock.h>
#include <framework/event.h>
#include <framework/driver.h>
#include <framework/v2k.h>
#include <drv_mipi.h>
#include <media/v2k_subdev.h>
#include <media/sys_camera.h>
#include "clock.h"
#include "lcdc/lcdc.h"
#include <dbg.h>


#define MAX_VIDEO_MEM       8 << 20 

#define D2A_REG_P0ADDR      0x08
#define D2A_REG_P1ADDR      0x0C
#define D2A_REG_IS          0x14
#define D2A_REG_ST          0x18
#define D2A_REG_PTR         0x1C
#define D2A_REG_FIU         0x20
/* D2A interrupt status register */
#define D2A_IS_WRD          0x01

/* D2A status register */
#define D2A_ST_PG           0x3

/* D2A Packet type register */
#define D2A_PT_RGB565       0x22
#define D2A_PT_RAW8         0x2A

#define FLAG_CSI2RX_START   0x0001
#define FLAG_CSI2RX_ISR     0x0010

osThreadId_t tid_csi2rx;

#include <memxfer.h>
extern const struct s_kdp_memxfer kdp_memxfer_module;


struct kdp520_fb {
    struct video_buf vb;	
    struct kdp_list list;
};

struct kdp520_csi2rx_context {
    struct sys_camera_host cam_h;
    enum v2k_field field;
    u8 *dpi2ahb_base;
    u8 *base;
    void *alloc_ctx;
    struct kdp520_fb *active;
    struct kdp_list fb_list;
    struct spinlock_t lock;	/* Protects video buffer lists */
};

struct init_seq {
    unsigned int addr;
    unsigned int value;
};

static struct init_seq dpi2ahb_init_seq[] = {
#if 1
    {0x00, 0x3000},//{0x00, 0xb020},
    {0x04, 0x0},
	//{0x08,LCD_FRAMEBUFFER_BASE},
	//{0x0c,LCD_FRAMEBUFFER_BASE1},    
    {0x10, 0x3f}, //{0x10,0xf},
    {0x20,0x1},
    
//	{0x00,0x3000}, //RICA: 0x3020  HM2056:0xb020
//	{0x04,0x0},
//	{0x08,LCD_FRAMEBUFFER_BASE},
//	{0x0c,LCD_FRAMEBUFFER_BASE1},
//	{0x10,0xf},
//	{0x1c,0x22},//RGB565
//	{0x20,0x1},
	//{0,0}
#else    
    {0x00, 0xb020},//{0x00, 0x3020},//{0x00, 0xb020},
    {0x04, 0x2},
    {0x10,0xf},//{0x10, 0x3f}, //{0x10,0xf},
    {0x20,0x1},
#endif    
};

static inline struct sys_camera_device *sys_camera_from_vb2q(
        const struct video_buf_queue *vq) {
    return container_of(vq, struct sys_camera_device, vb_q);
}

static struct kdp520_fb *to_kdp520_vb(struct video_buf *vb)
{
    return container_of(vb, struct kdp520_fb, vb);
}
extern int gval;
int old_gval = 0;
static void csi2rx_thread(void *argument)
{
    u32 flags;
    u32 status;
    u32 page_num;
    struct kdp520_csi2rx_context *ctx = (struct kdp520_csi2rx_context*)argument;      
    struct videobuf_dm_ctx *p = (struct videobuf_dm_ctx *)ctx->alloc_ctx;    

    struct kdp520_fb *fb;
    static int old_page_num = 1;

    static int dbg_cnt = 0;
    static int total_cnt = 0;
    
    flags = osThreadFlagsWait(FLAG_CSI2RX_START, osFlagsWaitAny ,osWaitForever);
    osThreadFlagsClear(flags);
    DSG(" ====================================");
    DSG(" csi2rx : startstreaming successfully");
    DSG(" ====================================");
    //u64 base_tick = osKernelGetTickCount();
    //DSG("tick:%llu", base_tick);

    for (;;) {
        flags = osThreadFlagsWait(FLAG_CSI2RX_ISR, osFlagsWaitAny, osWaitForever);

        osThreadFlagsClear(flags);

        //spin_lock_irqsave(&ctx->lock, flags);
        
        if (flags & FLAG_CSI2RX_ISR) {
 
            //status = inw(ctx->dpi2ahb_base + D2A_REG_IS);
            //DSG("status=%x", status);
            //if (status & D2A_IS_WRD) {
                //page_num = (inw(ctx->dpi2ahb_base + D2A_REG_ST) & D2A_ST_PG);
//                if (page_num != old_page_num) {
//                    old_page_num = page_num;
//                    dbg_cnt = 1;
//                } else {
//                    dbg_cnt++;
//                }
                if (gval != old_gval) {
                    old_gval = gval;
                    dbg_cnt = 1;
                } else {
                    dbg_cnt++;
                }                
                ++total_cnt;

                fb = ctx->active;
                if (fb) {
                    //DSG("<%s> fb->vb.index=%d page_num=%x dbg_cnt=%d total_cnt=%d", 
                    //__func__, fb->vb.index, page_num, dbg_cnt, total_cnt);
                    
                    kdp_list_del_init(&fb->list);
                    
                    if (!kdp_list_empty(&ctx->fb_list)) {
                        ctx->active = (struct kdp520_fb*)
                            &container_of(ctx->fb_list.next, struct kdp520_fb, list)->vb;
                    } else
                        ctx->active = NULL;

                    fb->vb.embed_v2k_buffer.type = old_gval;//page_num;

                    if (0 == videobuf_done(&fb->vb, VB_STATE_DONE)) {
                        DSG("p->phy=%x vir=%x old_gval=%x vb.plane.length=%d", p->phy_start_addr, p->vir_start_addr, old_gval, fb->vb.plane.length);
                        //if (p->phy_start_addr != p->vir_start_addr)
                        {
                            //DSG("ts1=%llu", osKernelGetTickCount());
                            //kdp_memxfer_module.ddr_to_ddr(p->vir_start_addr, p->phy_start_addr, fb->vb.plane.length);
                            kdp_memxfer_module.ddr_to_ddr(p->vir_start_addr, old_gval, fb->vb.plane.length);
                            //DSG("ts2=%llu", osKernelGetTickCount());
                        }                        
                    }
                    //videobuf_done(&ctx->active->vb, VB_STATE_ERROR);
                }

                //outw(ctx->dpi2ahb_base + D2A_REG_IS, status | D2A_IS_WRD);

                
                

            //}

            NVIC_ClearPendingIRQ((IRQn_Type)D2A_FTDPI2AHB_IRQ);
            NVIC_EnableIRQ((IRQn_Type)D2A_FTDPI2AHB_IRQ);
        }

        //spin_unlock_irqrestore(&ctx->lock, flags);
    }
}


//void CSI_RX_IRQHandler(void) {
//    u32 status = inw(CSIRX_FTCSIRX100_PA_BASE + 0x33);
//    DSG("csi status=%x", status);
//        
//}

//void DPI2AHB_IRQHandler(void)
void dpi2ahb_isr(void)
{   
	int val;
    u32 status;
#ifdef V2K_CAM_ENABLE    
    const struct mm_struct *mm = get_mm_struct();
#endif     
    //status = inw(ctx->dpi2ahb_base + D2A_REG_IS);
    //status = inw(DPI2AHB_CSR_PA_BASE + D2A_REG_IS);
    
    NVIC_DisableIRQ((IRQn_Type)D2A_FTDPI2AHB_IRQ);
    
    //outw(DPI2AHB_CSR_PA_BASE + D2A_REG_IS, status);

	 //change framebuffer address to another frame
	gval = inw(LCDC_REG_PANEL_IMAGE0_FRAME0);
	old_gval = gval;
    LCDC_REG_INTR_CLEAR_SET_ClrFIFOUdn();
    
    //DSG("gval dpi2=%x", gval);
#ifdef V2K_CAM_ENABLE
    if (gval == (mm->mmap->vm_start + mm->csirx0_mm_phy)) {
        gval = (mm->mmap->vm_start + mm->csirx0_mm_phy_2);         
    }
    else if (gval == (mm->mmap->vm_start + mm->csirx0_mm_phy_2)) {
        gval = (mm->mmap->vm_start + mm->csirx0_mm_phy);
    }
    else if (gval == (mm->mmap->vm_start + mm->csirx1_mm_phy_2)) {
        gval = (mm->mmap->vm_start + mm->csirx1_mm_phy);
    }
    else if (gval == (mm->mmap->vm_start + mm->csirx1_mm_phy)) {
        gval = (mm->mmap->vm_start + mm->csirx1_mm_phy_2);
    }
    else
        gval = mm->mmap->vm_start + mm->csirx0_mm_phy;
     //DSG("gval dpi3=%x", gval);
#else    
    if(gval == LCD_FRAMEBUFFER_BASE)
	{		
		gval=LCD_FRAMEBUFFER_BASE1;
		//writel(LCD_FRAMEBUFFER_BASE1, LCD_FTLCDC210_PA_BASE+0x18);
	}
	else if(gval==LCD_FRAMEBUFFER_BASE1)
	{		
		gval=LCD_FRAMEBUFFER_BASE;
		//writel(LCD_FRAMEBUFFER_BASE, LCD_FTLCDC210_PA_BASE+0x18);
	}
	else if(gval==LCD_FRAMEBUFFER_BASE2)
	{		
		gval=LCD_FRAMEBUFFER_BASE3;
		//writel(LCD_FRAMEBUFFER_BASE, LCD_FTLCDC210_PA_BASE+0x18);
	}
	else if(gval==LCD_FRAMEBUFFER_BASE3)
	{		
		gval=LCD_FRAMEBUFFER_BASE2;
		//writel(LCD_FRAMEBUFFER_BASE, LCD_FTLCDC210_PA_BASE+0x18);
	}
	else
	{
		gval=LCD_FRAMEBUFFER_BASE;
	}
#endif    
	LCDC_REG_PANEL_PIXEL_SET_AddrUpdate(1);
    
    osThreadFlagsSet(tid_csi2rx, FLAG_CSI2RX_ISR);
}

void dpi2ahb_init(struct kdp520_csi2rx_context *csi2rx_ctx, 
        struct v2k_format *format)
{
    struct kdp520_csi2rx_context *ctx = csi2rx_ctx;
    struct init_seq *start = dpi2ahb_init_seq;
    int i;

    //NVIC_EnableIRQ(D2A_FTDPI2AHB_IRQ);
    
    for (i = 0; i < ARRAY_SIZE(dpi2ahb_init_seq); ++i) {
        DSG("ctx->dpi2ahb_base + start->addr=%x start->value=%x",
        ctx->dpi2ahb_base + start->addr, start->value);
        outw(ctx->dpi2ahb_base + start->addr, start->value);
        ++start;
    }

    switch (format->pixelformat) {
    case V2K_PIX_FMT_RGB565:
        outw(ctx->dpi2ahb_base + D2A_REG_PTR, D2A_PT_RGB565);
        break;        
    case V2K_PIX_FMT_GREY:
        outw(ctx->dpi2ahb_base + D2A_REG_PTR, D2A_PT_RAW8);
        break;
    default:;
    }
}

void csi2rx_init(struct kdp520_csi2rx_context *csi2rx_ctx, 
        struct v2k_format *format)
{
#if 1
	/*
	FTCSIRX:
	Control Register (0x04) = 0x1d
	DPI V Sync Control (0x05) =  0x1
	Data Lane Mapping Register 1 (0x2a) = 0x20
	Data Lane Mapping Register 1 (0x2b) = 0x13
	Horizontal Pixel Number Register (0x20) = 0x80
	Horizontal Pixel Number Register (0x21) = 0x07
	DPI V Sync Timing Register (0x14) = 0xff
	DPI V Sync Timing Extended Register (0x1e) = 0x5
	IMAGE TYPE (0x1c) = 0x22 RGB565 (kay)
	*/
	
	int val;
	
	int base;
	//NVIC_EnableIRQ(SBS_CSI_RX_IRQ);
    int id = 0;
	if(id == 0 ){
		base = CSIRX_FTCSIRX100_PA_BASE;
	}else{
		base = CSIRX_FTCSIRX100_1_PA_BASE;
	}
	

	val = inw(base + 0x20);
	val = (val&(~0xffff))|0x01e0;
	outw(base + 0x20 , val);

	//0x04 = 0x0d
	val = inw(base + 0x4);
	val = (val&(~0xffff))|0x010d;
	outw(base + 0x4 , val);
	
  //0x1c = 0x22;
	val = inw(base + 0x1c);
	if(id == 0 )
	{	
		val = (val&(~0xff))|0x22;
	}
	else
	{	
		val = (val&(~0xff))|0x2A;
	}
	outw(base + 0x1c , val);
	
	//0x28 ppi enable bit0;bit1
	val = inw(base + 0x28);
	val |= 0x01; 
	outw(base + 0x28 , val);
	
	
  //0x3c enable RX irq
	// fail
	/*
	val = inw(base + 0x3c);
	val |= 0x40; 
	outw(base + 0x3c , val);
	*/
#else    
    int val;
    struct kdp520_csi2rx_context *ctx = csi2rx_ctx;

    u16 width = ROUND_UP(format->width, 4);
	DSG("%s width=%u", __func__, width);
	DSG("%s ctx->base=%x", __func__, ctx->base);
    val = inw(ctx->base + MIPI_HPNR_ADDR);
    val = (val&(~0xffff)) | width;
    outw(ctx->base + MIPI_HPNR_ADDR , val);
    
    val = inw(ctx->base + MIPI_CR_ADDR);
    val = (val&(~0xffff))|0x010d; 
    outw(ctx->base + MIPI_CR_ADDR , val);
    
    val = inw(ctx->base + MIPI_MCR_ADDR);
    val = (val&(~0xff))|0x22;
    outw(ctx->base + MIPI_MCR_ADDR , val);
    
    //0x28 ppi enable bit0;bit1
    val = inw(ctx->base + MIPI_PECR_ADDR);
    val |= 0x01; 
    outw(ctx->base + MIPI_PECR_ADDR , val);
#endif    
}

extern int FTLCDC210_Test_Main(void);

#include "scu_extreg.h"
extern struct clock_node clock_node_csirx0_hs_csi;
extern struct clock_node clock_node_csirx0_hs_vc0;
extern struct clock_node clock_node_csirx0_lp;
  
static int kdp520_csi2rx_clock_start(struct sys_camera_host *cam_h)
{
#if 0    
    {
        u32 clk;

        struct clock_value clock_val_hs;

        // clock_val_hs.ns = 120;
        // clock_val_hs.ms = 1;
        // clock_val_hs.ps = 2;
        // clock_val_hs.div = 0x1d;        

        // u32 clk;
        // clk = clock_mgr_calculate_clockout(pll_3, clock_val_hs.ms, clock_val_hs.ns, clock_val_hs.ps);
        // DSG("WWWWWW 111=%d %d", clk, clk / clock_val_hs.div);

            clock_val_hs.ns = 283;//120;//141;
            clock_val_hs.ms = 2;//1;
            clock_val_hs.ps = 1;//2;//1;
            clock_val_hs.div = 6;

        clk = clock_mgr_calculate_clockout(pll_3, clock_val_hs.ms, clock_val_hs.ns, clock_val_hs.ps);
        DSG("WWWWWW 222=%d %d", clk, (clk / clock_val_hs.div)/2);//141.5
        DSG("WWWWWW 222=%d %d", clk, (clk / clock_val_hs.div)/8);

        clock_mgr_open(&clock_node_csirx0_hs_csi, &clock_val_hs);

            clock_val_hs.ns = 283;//120;//141;
            clock_val_hs.ms = 2;//1;
            clock_val_hs.ps = 1;//2;//1;
            clock_val_hs.div = 29;
        clock_mgr_open(&clock_node_csirx0_hs_vc0, &clock_val_hs);             
    }
    {
        struct clock_value clock_val_lp;
        clock_val_lp.div = 0x04;    

        clock_mgr_open(&clock_node_csirx0_lp, &clock_val_lp);
    }
#endif    
    return 0;
}

static void kdp520_csi2rx_clock_stop(struct sys_camera_host *cam_h)
{

}

static int kdp520_csi2rx_querycap(struct sys_camera_host *cam_h,
        struct v2k_capability *cap)
{
    strcpy(cap->desc, "Kdp520 camera");
    cap->capabilities = V2K_CAP_VIDEO_CAPTURE | V2K_CAP_STREAMING | V2K_CAP_DEVICE_CAPS;

    return 0;
}
extern void dpi2ahb_init_org(int id);
extern void ftcsirx_init(int id);
extern void mipi_test_main(int id);
static int kdp520_csi2rx_set_fmt(struct sys_camera_device *cam_d,
        struct v2k_format *format)
{
    int ret;
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);
    struct v2k_subdev *sd = sys_camera_to_subdev(cam_d);
    const struct sys_camera_format_xlate *xlate;

    DSG("<%s:%s> width=%d height=%d", __FILE__, __func__, format->width, format->height);

    xlate = sys_camera_xlate_by_fourcc(cam_d, format->pixelformat); //fourcc
    if (!xlate) {
    //  DSG("<%s> Format %x not found", __func__, want_fmt->pixelformat);
        return -EINVAL;
    }

	//dpi2ahb_init_org(0);
	//ftcsirx_init(0);    
    
    dpi2ahb_init((struct kdp520_csi2rx_context*)cam_h->priv, format);
    csi2rx_init((struct kdp520_csi2rx_context*)cam_h->priv, format);
    //NVIC_EnableIRQ(LCDC_FTLCDC210_VSTATUS_IRQ); //james
    //mipi_test_main(0);
    
    ret = v2k_subdev_call(sd, set_fmt, format);

    cam_d->current_fmt = xlate;
    
    return ret;
}

static int kdp520_csi2rx_set_bus_param(struct sys_camera_device *cam_d)
{
    //DSG("<%s:%s>", __FILE__, __func__);
    //set hsync / vsync / pclk / 
    return 0;
}

static int kdp520_videobuf_setup(struct video_buf_queue *vq,
        unsigned int *num_buffers, 
        unsigned int *num_planes,
        unsigned int *size, void **alloc_ctx)
{
    struct sys_camera_device *cam_d = sys_camera_from_vb2q(vq);
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);
    struct kdp520_csi2rx_context *ctx = (struct kdp520_csi2rx_context *)cam_h->priv;

    if (*num_buffers == 0) return -EINVAL;

    *num_planes = 1;//2;
    *size = cam_d->sizeimage;
    *alloc_ctx = ctx->alloc_ctx;

    if (*size * *num_buffers * *num_planes > MAX_VIDEO_MEM) 
        *num_buffers = (MAX_VIDEO_MEM) / (*size * *num_planes);

    if (*num_buffers > 2)
        *num_buffers = 2;

    return 0;
}

static int kdp520_videobuf_init(struct video_buf *vb)
{
    struct video_buf_queue *vq = vb->vb_queue;
    struct sys_camera_device *cam_d = sys_camera_from_vb2q(vq);
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);
    struct kdp520_csi2rx_context *ctx = (struct kdp520_csi2rx_context *)cam_h->priv;
    struct kdp520_fb *fb = to_kdp520_vb(vb);

    switch (vb->index) {
    case 0:
        outw(ctx->dpi2ahb_base + D2A_REG_P0ADDR, (unsigned long)vb->plane.mem_priv);
        break;
    case 1:
        outw(ctx->dpi2ahb_base + D2A_REG_P1ADDR, (unsigned long)vb->plane.mem_priv);
        break;
    default:;
    }

    kdp_list_init(&fb->list);

    //kdp_memxfer_module.init(MEMXFER_OPS_CPU, MEMXFER_OPS_DMA);
     
    return 0;
}

static void kdp520_videobuf_queue(struct video_buf *vb)
{
    struct sys_camera_device *cam_d = sys_camera_from_vb2q(vb->vb_queue);
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);
    struct kdp520_csi2rx_context *ctx = (struct kdp520_csi2rx_context *)cam_h->priv;
    struct kdp520_fb *fb = to_kdp520_vb(vb);

    spin_lock_irq(&ctx->lock);

    kdp_list_add_tail(&ctx->fb_list, &fb->list);

    if (!ctx->active) {
        ctx->active = fb;
        //DSG("<%s> ctx->active_fb_idx=%d", __func__, vb->index);
    }
    
    spin_unlock_irq(&ctx->lock);
}

static int kdp520_start_streaming(struct video_buf_queue *q)
{
    DSG("<%s>", __func__);
    osThreadFlagsSet(tid_csi2rx, FLAG_CSI2RX_START);
    NVIC_EnableIRQ((IRQn_Type)D2A_FTDPI2AHB_IRQ);

    return 0;
}

static void kdp520_stop_streaming(struct video_buf_queue *q)
{
    DSG("<%s>", __func__);
}

void kdp520_wait_prepare(struct video_buf_queue *vq)
{
    mutex_unlock(&vq->lock);
}

void kdp520_wait_finish(struct video_buf_queue *vq)
{
    mutex_lock(&vq->lock);
}

// static void kdp520_videobuf_release(struct video_buf *vb)
// {

// }

static struct videobuf_ops kdp520_videobuf_ops = {
    .queue_setup    = kdp520_videobuf_setup,
    .buf_init       = kdp520_videobuf_init,        
    .buf_queue      = kdp520_videobuf_queue,
    .start_streaming = kdp520_start_streaming,
    .stop_streaming = kdp520_stop_streaming,    
    .wait_prepare   = kdp520_wait_prepare,
    .wait_finish    = kdp520_wait_finish,
    //.buf_cleanup    = kdp520_videobuf_release,
};

const struct videobuf_mem_ops kdp520_videobuf_mem_ops = {
    .alloc = dm_alloc,
};

static int kdp520_csi2rx_init_videobuf(struct video_buf_queue *q,
        struct sys_camera_device *cam_d)
{
    struct sys_camera_host *cam_h = to_sys_camera_host(cam_d->parent_pin);

    q->vb_ops = &kdp520_videobuf_ops;
    q->vb_mem_ops = &kdp520_videobuf_mem_ops;
    q->buf_struct_size = sizeof(struct kdp520_fb);

    kdp_list_init(&q->queued_list);
    kdp_list_init(&q->done_list);

    mutex_init(&q->lock);

    return 0;
}

static struct sys_camera_host_ops kdp520_sys_camera_host_ops = {
    .clock_start    = kdp520_csi2rx_clock_start,
    .clock_stop     = kdp520_csi2rx_clock_stop,	
    .querycap       = kdp520_csi2rx_querycap,	
    .set_fmt        = kdp520_csi2rx_set_fmt,
    .set_bus_param  = kdp520_csi2rx_set_bus_param,	
    .init_videobuf  = kdp520_csi2rx_init_videobuf, 
};

static int kdp520_csi2rx_probe(struct core_device *core_d)
{
    struct kdp520_csi2rx_context *ctx;
    struct mm_struct *mm = get_mm_struct();    
    int err;
    
    DSG("<%s>", __func__);
    
    u32 val;

    //csi rx 0 reset
    SCU_EXTREG_CSIRX_CTRL0_SET_Enable(1);   
    SCU_EXTREG_CSIRX_CTRL0_SET_ClkLnEn(1);  
    SCU_EXTREG_CSIRX_CTRL0_SET_sys_rst_n(1);       
    SCU_EXTREG_CSIRX_CTRL0_SET_pwr_rst_n(1);    
    SCU_EXTREG_CSIRX_CTRL0_SET_apb_rst_n(1);

    //mipi phy settle count
    outb(0xc3700011, 3);
    outw(0xc238009c, 0xff); //DPI2AHB   
     
//    debug_pll_clock();
    ctx = calloc(1, sizeof(*ctx));
    if (!ctx) {
        //DSG("<%s> Could not allocate pdev", __func__);
        return -ENOMEM;
    }

    kdp_list_init(&ctx->fb_list);

    ctx->field = V2K_FIELD_NONE;
    ctx->dpi2ahb_base = (void*)driver_get_ioport_setting(core_d, IORESOURCE_BUS)->start;
    ctx->base = (void*)driver_get_ioport_setting(core_d, IORESOURCE_MEM)->start;
    
    ctx->cam_h.priv = ctx;
    ctx->cam_h.pin_obj.pin_ctx = &core_d->pin_ctx;
    ctx->cam_h.host_id = core_d->uuid;

    ctx->cam_h.ops = &kdp520_sys_camera_host_ops;
    {
        struct videobuf_dm_ctx *p = calloc(1, sizeof(struct videobuf_dm_ctx));
        p->idx = 0;
        // To do ...
        p->vir_start_addr = mm->mmap->vm_start + mm->csirx0_mm_user;//mm->mmap_base;
        p->phy_start_addr = mm->mmap->vm_start + mm->csirx0_mm_phy;//csirx1_mem_base
        p->allocated_length = 0;

        ctx->alloc_ctx = (void*)p;
    }
    err = sys_camera_host_register(&ctx->cam_h);
    if (err) {
        goto exit_csi2_unregister;
    }

#if 1
    NVIC_SetVector((IRQn_Type)D2A_FTDPI2AHB_IRQ, (uint32_t)dpi2ahb_isr);
    tid_csi2rx = osThreadNew(csi2rx_thread, (void*)ctx, NULL);
#endif    

    return 0;

exit_csi2_unregister:
    sys_camera_host_unregister(&ctx->cam_h);

    return err;
}

static int kdp520_csi2rx_remove(struct core_device *core_d)
{
    struct sys_camera_host *p = to_sys_camera_host(&core_d->pin_ctx);

    sys_camera_host_unregister(p);

    return 0;
}

extern struct core_device kdp520_csi2rx0;
static struct core_driver kdp520_csi2rx0_driver = {
    .driver     = {
        .name   = "kdp520_csi2rx0",
    },
    .probe      = kdp520_csi2rx_probe,
    .remove     = kdp520_csi2rx_remove,
    .core_dev   = &kdp520_csi2rx0,
};
KDP_CORE_DRIVER_SETUP(kdp520_csi2rx0_driver, 5);

#endif
