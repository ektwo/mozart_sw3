/*
 * @name : v2k_ioctl.c
 * @brief : A generic framework to process V2K ioctl commands
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */    
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <utility.h>
#include <framework/types.h>
#include <framework/errno.h>
#include <media/videodev.h>
#include <media/v2k_ioctl.h>
#include <dbg.h>


#define V2K_IOCTLS ARRAY_SIZE(v2k_ioctls)

/* used to decode ioctl numbers.. */

#define _DEV_IOCTL_DIRMASK       ((1 << _DEV_IOCTL_DIRBITS) - 1)
#define _DEV_IOCTL_NUMMASK       ((1 << _DEV_IOCTL_NUMBITS) - 1)
#define _DEV_IOCTL_SIZEMASK      ((1 << _DEV_IOCTL_SIZEBITS) - 1)

#define _DEV_IOCTL_GET_DIR(_x_)  (((_x_) >> _DEV_IOCTL_DIRSHIFT)  & _DEV_IOCTL_DIRMASK)
#define _DEV_IOCTL_GET_ID(_x_)   (((_x_) >> _DEV_IOCTL_NUMSHIFT)  & _DEV_IOCTL_NUMMASK)
#define _DEV_IOCTL_GET_SIZE(_x_) (((_x_) >> _DEV_IOCTL_SIZESHIFT) & _DEV_IOCTL_SIZEMASK)


struct v2k_dev_handle;
typedef long (*fn_v2k_ioctl)(struct v2k_dev_handle *handle, unsigned int cmd, void *arg);


static inline unsigned long copy_from_user(
        void *to, const void *from, unsigned long n)
{
    memcpy(to, from, n);
    return 0;
}
static inline unsigned long copy_to_user(
        void *to, const void *from, unsigned long n)
{
    memcpy(to, from, n);
    return 0;
}

static int v2k_querycap(const struct v2k_ioctl_ops *ops,
        struct v2k_dev_handle *handle, void *priv, void *arg)
{
    struct v2k_capability *cap = (struct v2k_capability *)arg;

    return ops->vioc_querycap(handle, priv, cap);
}

static int v2k_g_fmt(const struct v2k_ioctl_ops *ops,
        struct v2k_dev_handle *handle, void *priv, void *arg)
{
     return ops->vioc_g_fmt_vid_cap(handle, priv, arg);
}

static int v2k_s_fmt(const struct v2k_ioctl_ops *ops,
        struct v2k_dev_handle *handle, void *priv, void *arg)
{
    //DSG("<%s:%s>", __FILE__, __func__);
    return ops->vioc_s_fmt_vid_cap(handle, priv, arg);
}

static int v2k_streamon(const struct v2k_ioctl_ops *ops,
        struct v2k_dev_handle *handle, void *priv, void *arg)
{
    return ops->vioc_streamon(handle, priv, (unsigned long)arg);
}

static int v2k_streamoff(const struct v2k_ioctl_ops *ops,
        struct v2k_dev_handle *handle, void *priv, void *arg)
{
    return ops->vioc_streamoff(handle, priv, (unsigned long)arg);
}

static int v2k_reqbufs(const struct v2k_ioctl_ops *ops,
        struct v2k_dev_handle *handle, void *priv, void *arg)
{
    struct v2k_requestbuffers *p = arg;

    return ops->vioc_reqbufs(handle, priv, p);
}

static int v2k_querybuf(const struct v2k_ioctl_ops *ops,
        struct v2k_dev_handle *handle, void *priv, void *arg)
{
    struct v2k_buffer *p = arg;

    return ops->vioc_querybuf(handle, priv, p);
}

static int v2k_qbuf(const struct v2k_ioctl_ops *ops,
        struct v2k_dev_handle *handle, void *priv, void *arg)
{
    struct v2k_buffer *p = arg;

    return ops->vioc_qbuf(handle, priv, p);
}

static int v2k_dqbuf(const struct v2k_ioctl_ops *ops,
        struct v2k_dev_handle *handle, void *priv, void *arg)
{
    struct v2k_buffer *p = arg;

    return ops->vioc_dqbuf(handle, priv, p);
}

struct v2k_ioctl_info {
    unsigned int ioctl;
    u32 flags;
    int (*func)(const struct v2k_ioctl_ops *ops,
                struct v2k_dev_handle *handle, 
                void *priv, 
                void *p);
};

#define INFO_FL_FUNC    (1 << 0)
#define INFO_FL_QUEUE   (1 << 1)
#define IOCTL_INFO_FNC(__ioctl, __func, __flags)                 \
[_DEV_IOCTL_GET_ID(__ioctl)] = {.ioctl = __ioctl,                \
                                .flags = __flags | INFO_FL_FUNC, \
                                .func  = __func }

static struct v2k_ioctl_info v2k_ioctls[] = {
    IOCTL_INFO_FNC(V2K_VIOC_QUERYCAP,   v2k_querycap,   0),
    IOCTL_INFO_FNC(V2K_VIOC_G_FMT,      v2k_g_fmt,      0),
    IOCTL_INFO_FNC(V2K_VIOC_S_FMT,      v2k_s_fmt,      0),
    IOCTL_INFO_FNC(V2K_VIOC_REQBUFS,    v2k_reqbufs,    INFO_FL_QUEUE),
    IOCTL_INFO_FNC(V2K_VIOC_QUERYBUF,   v2k_querybuf,   INFO_FL_QUEUE),
    IOCTL_INFO_FNC(V2K_VIOC_QBUF,       v2k_qbuf,       INFO_FL_QUEUE),
    IOCTL_INFO_FNC(V2K_VIOC_DQBUF,      v2k_dqbuf,      INFO_FL_QUEUE),
    IOCTL_INFO_FNC(V2K_VIOC_STREAMON,   v2k_streamon,   INFO_FL_QUEUE),
    IOCTL_INFO_FNC(V2K_VIOC_STREAMOFF,  v2k_streamoff,  INFO_FL_QUEUE),
};

static long __video_do_ioctl(struct v2k_dev_handle *handle,
        unsigned int cmd, void *arg)
{
    long ret = -ENOTTY;    
    struct video_device_context *vdc = video_devdata(handle);
    const struct v2k_ioctl_ops *ops = vdc->ioctl_ops;
    if (ops) {
        void *priv = handle->private_data;
        const struct v2k_ioctl_info *info = &v2k_ioctls[_DEV_IOCTL_GET_ID(cmd)];
        if (info->flags & INFO_FL_FUNC) {
            ret = info->func(ops, handle, priv, arg);
        }
    }

    return ret;
}

char sbuf[128];
//long video_usercopy(struct v2k_dev_handle *handle, 
//        unsigned int cmd, unsigned long arg, fn_v2k_ioctl func)
long video_usercopy(struct v2k_dev_handle *handle,
        unsigned int cmd, void *arg, fn_v2k_ioctl func)
{
    void *parg = arg;
    void *mbuf = NULL;
    long err  = -EINVAL;
    bool has_array_args;
    size_t array_size = 0;
    void *user_ptr = NULL;
    void **kernel_ptr = NULL;

    if (_DEV_IOCTL_GET_DIR(cmd) & _DEV_IOCTL_RW)
    {
        unsigned int struct_size = _DEV_IOCTL_GET_SIZE(cmd);
        if (struct_size <= sizeof(sbuf)) {
            parg = sbuf;
        } else {
            /* too big to allocate from stack */
            mbuf = calloc(1, _DEV_IOCTL_GET_SIZE(cmd));
            if (NULL == mbuf)
                return -ENOMEM;
            parg = mbuf;
        }

        err = -EFAULT;
        if (_DEV_IOCTL_GET_DIR(cmd) & _DEV_IOCTL_WRITE) {

             if (copy_from_user(parg, (void *)arg, struct_size)) {
                 goto out;
             }

             if (struct_size < _DEV_IOCTL_GET_SIZE(cmd))
                 memset((u8 *)parg + struct_size, 0, _DEV_IOCTL_GET_SIZE(cmd) - struct_size);
        }
        else
        {
            memset(parg, 0, _DEV_IOCTL_GET_SIZE(cmd));
        }
    }

    /* Handles IOCTL */
    err = func(handle, cmd, parg);
    if (err == -ENOIOCTLCMD)
        err = -ENOTTY;
    if (err == 0) {
    }

    if (has_array_args) {
        *kernel_ptr = user_ptr;
        if (copy_to_user(user_ptr, mbuf, array_size))
            err = -EFAULT;
        goto out_array_args;
    }

out_array_args:
    switch (_DEV_IOCTL_GET_DIR(cmd)) {
    case _DEV_IOCTL_READ:
    case _DEV_IOCTL_RW:
        if (copy_to_user((void *)arg, parg, _DEV_IOCTL_GET_SIZE(cmd)))
            err = -EFAULT;
        break;
    }

out:
    if (mbuf)
        free(mbuf);

    return err;
}

long video_default_ioctl(struct v2k_dev_handle *handle, unsigned int cmd, void* arg)
{
    return video_usercopy(handle, cmd, arg, __video_do_ioctl);
}

