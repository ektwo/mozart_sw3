/*
 * @name : v2k_dev_ops.c
 * @brief : Video capture interface for Mozart
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#include <stdlib.h>
#include <string.h>
#include <framework/types.h>
#include <framework/bitops.h>
#include <framework/utils.h>
#include <framework/errno.h>
//#include <framework/v2k.h>
#include <media/videodev.h>
#include <dbg.h>


struct v2k_dev_handle g_curr_dev_handle;

static int v2k_open(struct v2k_dev_handle *handle)
{
    int ret = -ENODEV;    
    struct video_device_context *vdc = video_devdata(handle);
    
    if ((vdc) &&(vdc->dev_ops->open)) {
        ret = vdc->dev_ops->open(handle);
    }
    
    return ret;
}

static int v2k_release(struct v2k_dev_handle *handle)
{
    int ret = 0;    
    struct video_device_context *vdc = video_devdata(handle);

    //DSG("<%s:%s>", __FILE__, __func__);
    if (vdc->dev_ops->release)
        ret = vdc->dev_ops->release(handle);

    return ret;
}
    
static long v2k_ioctl(struct v2k_dev_handle *handle, 
        unsigned int cmd, void *arg)
{
    int ret = -ENODEV;
    struct video_device_context *vdc = video_devdata(handle);

    if (vdc->dev_ops->ioctl) {
        ret = vdc->dev_ops->ioctl(handle, cmd, arg);
    } else
        ret = -ENOTTY;

    return ret;
}

const struct v2k_dev_operations v2k_dev_ops = {
    .open = v2k_open,
    .release = v2k_release,
    .ioctl = v2k_ioctl,    
};

const struct v2k_dev_operations* v2k_get_dev_ops(void) {
    return &v2k_dev_ops;
} 

struct v2k_dev_handle* v2k_get_dev_handle(void) {
    return &g_curr_dev_handle;
}

void v2k_set_dev_handle(struct v2k_dev_handle *handle) {
    g_curr_dev_handle.i_rdev = handle->i_rdev;
    g_curr_dev_handle.dev_ops = &v2k_dev_ops;
}
