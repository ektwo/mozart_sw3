/*
 * @name : videobuf_core.c
 * @brief : video buffer helper functions
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#include <stdlib.h>
#include <stddef.h>
#include <framework/utils.h>
#include <framework/kdp_list.h>
#include <framework/errno.h>
#include <framework/v2k.h>
#include <framework/atomic.h>
#include <framework/spinlock.h>
#include <framework/mm.h>
#include <framework/event.h>
#include <media/videobuf_core.h>
#include <dbg.h>

#define V2K_BUF_MAX_FRAME       5
#define V2K_BUF_FLAG_INITED     0x00000001
#define V2K_BUF_FLAG_QUEUED     0x00000002
#define V2K_BUF_FLAG_DONE       0x00000004
#define V2K_BUF_FLAG_ERROR      0x00000040
#define V2K_BUF_FLAG_PREPARED   0x00000400
#define V2K_BUF_FLAG_MASK       (V2K_BUF_FLAG_INITED | V2K_BUF_FLAG_QUEUED | \
                                 V2K_BUF_FLAG_DONE | V2K_BUF_FLAG_ERROR | \
                                 V2K_BUF_FLAG_PREPARED)
                                 
#define FLAGS_DONE_LIST_EVT 0xfd1e

osEventFlagsId_t evt_done_list_id;

static void __fill_v2k_buffer(struct video_buf *vb, struct v2k_buffer *lp_v2k_buf)
{    
    
    lp_v2k_buf->index = vb->embed_v2k_buffer.index;
    lp_v2k_buf->type = vb->embed_v2k_buffer.type;
    //lp_v2k_buf->flags = vb->embed_v2k_buffer.flags;
    //lp_v2k_buf->field = vb->embed_v2k_buffer.field;
    lp_v2k_buf->length = vb->plane.length;
    lp_v2k_buf->offset = vb->plane.offset;

    lp_v2k_buf->flags = 0;
  
    switch (vb->state) {
    case VB_STATE_QUEUED:
    case VB_STATE_ACTIVE:
        lp_v2k_buf->flags |= V2K_BUF_FLAG_QUEUED;
        break;
    case VB_STATE_ERROR:
        lp_v2k_buf->flags |= V2K_BUF_FLAG_ERROR;
    case VB_STATE_DONE:
        lp_v2k_buf->flags |= V2K_BUF_FLAG_DONE;
        break;
    case VB_STATE_PREPARED:
        lp_v2k_buf->flags |= V2K_BUF_FLAG_PREPARED;
        break;
    case VB_STATE_PREPARING:
    case VB_STATE_DEQUEUED:
        /* nothing */
        break;
    }
    
    lp_v2k_buf->flags |= V2K_BUF_FLAG_INITED;
}

int videobuf_done(struct video_buf *vb, enum video_buf_state state)
{
    int ret = -1;
    struct video_buf_queue *q = vb->vb_queue;
    //unsigned long flags;

    //DSG("<%s> vb->state[%d]=%x", __func__, vb->index, vb->state);
 	if (vb->state != VB_STATE_ACTIVE)
 		return ret;
    
    if (state != VB_STATE_DONE &&
        state != VB_STATE_ERROR &&
        state != VB_STATE_QUEUED)
        state = VB_STATE_ERROR;

    //flags = spin_lock_irqsave(&q->done_lock);

    if (state == VB_STATE_QUEUED) {
        vb->state = VB_STATE_QUEUED;
    } else {
        /* Add the buffer to the done buffers list */
        kdp_list_add_tail(&q->done_list, &vb->done_entry);
        vb->state = state;
        set_event(evt_done_list_id, FLAGS_DONE_LIST_EVT);   
    }

    //spin_unlock_irqrestore(&q->done_lock, flags);
    
    switch (state) {
    case VB_STATE_QUEUED:
        break;
    default:
        ret = 0;
        break;
    }
    
    return ret;
}

int videobuf_reqbufs(struct video_buf_queue *q, struct v2k_requestbuffers *req)
{
    int ret = 0;	

    int i;
    unsigned int num_buffers = GET_MIN(req->count, (u32)V2K_BUF_MAX_FRAME);
    unsigned int num_planes;
    unsigned int size;	
    struct mm_struct *mm = get_mm_struct();  
    struct video_buf *vb;
    

    //DSG("<%s:%s>");
    ret = q->vb_ops->queue_setup(q, &num_buffers, &num_planes, &size, &q->alloc_ctx);
    if (ret)
        return ret;

    for (i = 0; i < num_buffers; ++i) {
        vb = (struct video_buf*)calloc(1, q->buf_struct_size);
        if (!vb) {
            //DSG("memory alloc for buffer struct failed");
            ret = -ENOMEM;
            break;
        }

        vb->state = VB_STATE_DEQUEUED;
        vb->vb_queue = q;
        vb->index = i;
        vb->plane.length = size;
        q->bufs[vb->index] = vb;
        unsigned long actual_size = PAGE_ALIGN(vb->plane.length);
        vb->plane.mem_priv = q->vb_mem_ops->alloc(q->alloc_ctx, actual_size);
        DSG("vb->plane.mem_priv=%x", vb->plane.mem_priv);
        struct video_buf_queue *q = vb->vb_queue;
    #ifdef AUTO_CALCULATE_SIZE        
        unsigned long off = 0;
        DSG("vb->index=%x", vb->index);
        if (vb->index) {
            struct video_buf *prev_vb = q->bufs[vb->index - 1];
            struct video_buf_plane *prev_plane = &prev_vb->plane;
            off = PAGE_ALIGN(prev_plane->offset + prev_plane->length);
        }
        vb->plane.offset = off; 
        DSG("vb->plane.offset=%x", vb->plane.offset);
        vb->vb_queue->vb_ops->buf_init(vb);
        vb->embed_v2k_buffer.index = vb->index;
        vb->embed_v2k_buffer.flags = vb->embed_v2k_buffer.flags;
        //vb->embed_v2k_buffer.field = V2K_FIELD_NONE;
        vb->embed_v2k_buffer.length = vb->plane.length;
        vb->embed_v2k_buffer.offset = vb->plane.offset;                
        DSG("vb->plane.length=%x", vb->plane.length);
        DSG("vb->plane.offset=%x", vb->plane.offset);
    #else
        struct videobuf_dm_ctx *dm_ctx = (struct videobuf_dm_ctx *)q->alloc_ctx; 
        
        DSG("dm_ctx->vir_start_addr=%x", dm_ctx->vir_start_addr);
        
        vb->plane.offset = dm_ctx->vir_start_addr - mm->mmap->vm_start;
        vb->vb_queue->vb_ops->buf_init(vb);
        vb->embed_v2k_buffer.index = vb->index;
        vb->embed_v2k_buffer.flags = vb->embed_v2k_buffer.flags;
        vb->embed_v2k_buffer.length = vb->plane.length;
        vb->embed_v2k_buffer.offset = vb->plane.offset; 
    #endif
    }

    q->num_buffers = num_buffers;
    req->count = num_buffers;

    return ret;	
}

int videobuf_querybuf(struct video_buf_queue *q, struct v2k_buffer *lp_v2k_buf)
{
    struct video_buf *vb = q->bufs[lp_v2k_buf->index];

    __fill_v2k_buffer(vb, lp_v2k_buf);

    return 0;
}

/* 
    Queue a buffer from application
*/
int videobuf_qbuf(struct video_buf_queue *q, struct v2k_buffer *b)
{
    int ret = 0;
    struct video_buf *vb;
    static int dbg_cnt = 0;

    //DSG("<%s:%s>", __FILE__, __func__);

    if (b->index >= q->num_buffers) {
        return -EINVAL;
    }

    vb = q->bufs[b->index];
    //DSG("<%s> vb->state[%d]=%x", __func__, vb->index, vb->state);
    switch (vb->state) {
    case VB_STATE_DEQUEUED:
        vb->state = VB_STATE_PREPARING;
        vb->state = VB_STATE_PREPARED;
        break;
    case VB_STATE_PREPARED:
        break;
    case VB_STATE_PREPARING:
        DSG("buffer still being prepared");
        return -EINVAL;
    default:
        DSG("invalid buffer state %d", vb->state);
        return -EINVAL;
    }

    kdp_list_add_tail(&q->queued_list, &vb->queued_entry);
    
    q->queued_count++;
    ++dbg_cnt;
    vb->state = VB_STATE_QUEUED;
    //DSG("<%s> q->queued_count=%d dbg_cnt=%d vb->state=%d", __func__, q->queued_count, dbg_cnt, vb->state);
    if (q->streaming) {
        vb->state = VB_STATE_ACTIVE;
        vb->vb_queue->vb_ops->buf_queue(vb);
    }

    __fill_v2k_buffer(vb, b);

    return ret;
}

/*
    Dequeue a buffer to the userspace
*/

int videobuf_dqbuf(struct video_buf_queue *q, struct v2k_buffer *lp_v2k_b, bool nonblocking)
{
    struct video_buf *vb = NULL;
    int ret;
    static int dbg_cnt = 0;

    //for (;;) {
        if (q->streaming) {
            wait_event(evt_done_list_id, FLAGS_DONE_LIST_EVT);

            q->vb_ops->wait_prepare(q);

            // To Do ...

            q->vb_ops->wait_finish(q);
        }

        if (!kdp_list_empty(&q->done_list)) {
            ret = 0;
        }

        ret = 0;
    //}
    //if (ret)
    //    return ret;

    //spin_lock_irqsave(&q->done_lock, flags);

    vb = container_of(q->done_list.next, struct video_buf, done_entry);

    kdp_list_del(&(vb->done_entry));

    //spin_unlock_irqrestore(&q->done_lock, flags);

    switch (vb->state) {
    case VB_STATE_DONE:
        //DSG("returning done buffer");
        break;
    case VB_STATE_ERROR:
       // DSG("returning done buffer with errors");
        break;
    default:
        //DSG("invalid buffer state=%x", vb->state);
        return -EINVAL;
    }

    __fill_v2k_buffer(vb, lp_v2k_b);
    lp_v2k_b->timestamp = osKernelGetTickCount();

    kdp_list_del(&vb->queued_entry);
    q->queued_count--;
    
    if (vb->state != VB_STATE_DEQUEUED)
        vb->state = VB_STATE_DEQUEUED;

    ++dbg_cnt;
    //DSG("<%s> vb->state[%d]=%x q->queued_count=%d cnt=%d", __func__, vb->index, vb->state, q->queued_count, dbg_cnt);

    return ret;
}


int videobuf_streamon(struct video_buf_queue *q, unsigned long arg)
{
    int ret = 0;
    struct video_buf *vb;

    DSG("<%s:%s>", __FILE__, __func__);

    struct kdp_list *head = &q->queued_list;
         
    //list_for_each_entry(vb, head, queued_entry) 
    for (vb = kdp_list_first_entry(head, typeof(*vb), queued_entry);
         &vb->queued_entry != head;
         vb = kdp_list_next_entry(vb, queued_entry))
    {
        vb->state = VB_STATE_ACTIVE;
        //DSG("<%s> vb->state[%d]=%x", __func__, vb->index, vb->state);
        vb->vb_queue->vb_ops->buf_queue(vb);
    }

    evt_done_list_id = create_event();

    ret = q->vb_ops->start_streaming(q);
    if (!ret)
        q->streaming = 1;

    return ret;
}

int videobuf_streamoff(struct video_buf_queue *q, unsigned long arg)
{
    int ret = 0;

    if (q->streaming)
        q->vb_ops->stop_streaming(q); 

    q->streaming = 0;
    q->queued_count = 0;

    /* Remove all buffers from videobuf's list */

    kdp_list_init(&q->queued_list);
    kdp_list_init(&q->done_list);

    set_event(evt_done_list_id, FLAGS_DONE_LIST_EVT);

    return ret;
}
