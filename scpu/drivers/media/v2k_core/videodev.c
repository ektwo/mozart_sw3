/*
 * @name : v2k_api.c
 * @brief : Video capture interface for Mozart
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#include <stdlib.h>
#include <framework/types.h>
#include <framework/bitops.h>
#include <framework/utils.h>
#include <framework/errno.h>
#include <media/videodev.h>
#include <dbg.h>


#define VD_TYPE_MAX         1
#define VID_DEV_NUM_LIMIT   1//2

static struct video_device_context *video_devices[VID_DEV_NUM_LIMIT];

struct video_device_context *video_devdata(struct v2k_dev_handle *handle) {
    return video_devices[handle->i_rdev];
}

int video_device_register(struct video_device_context *vdc, int id)
{
    if (id >= VID_DEV_NUM_LIMIT)
        id = 0;

    video_devices[id] = vdc;

    return 0;
}

void video_device_release(struct video_device_context *vdc)
{
    if (vdc) 
        free((void*)vdc);
}
