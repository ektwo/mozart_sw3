/*
 * @name : v2k_subdev.c 
 * @brief : The base handler that will be needed for all sub devices
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#include <stdio.h> 
#include <framework/kdp_list.h> 
#include <interface/i2c.h>
#include <media/v2k_subdev.h>
#include <dbg.h>


void v2k_subdev_init(struct v2k_subdev *sd, const struct v2k_subdev_ops *ops)
{
    sd->ops = ops;
    sd->dev_priv = NULL;
}

/* I2C Helper functions */
void v2k_i2c_subdev_init(struct v2k_subdev *sd, struct i2c_client *client,
        const struct v2k_subdev_ops *ops)
{
    //DSG("<%s:%s>", __FILE__, __func__);
    
    v2k_subdev_init(sd, ops);
    v2k_set_subdevdata(sd, client);
    
    dev_set_drvdata(&client->pin_ctx, sd);
}
