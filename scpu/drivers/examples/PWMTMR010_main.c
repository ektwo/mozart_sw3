#include "DrvPWMTMR010.h"
#include "DrvUART010.h"
#include "kneron_mozart.h"
#include "io.h"
#include "types.h"
#include <stdio.h>
//#include "utility.h"

UINT8 hr,min,sec;
extern volatile UINT32 T1_Tick, T2_Tick, T3_Tick, T4_Tick;

void WaitForTick (void)  {
  UINT32 curTicks;

  curTicks = T1_Tick;                            // Save Current SysTick Value
  while (T1_Tick == curTicks)  {                 // Wait for next SysTick Interrupt
  #ifdef __MCU_FA606TE__
		__asm("SWI 0x0");
  #else
	  __WFE (); 								   // Power-Down until next Event/Interrupt
  #endif
  }

}

void delay_ms(unsigned int count)
{
    unsigned i;
    for ( i = count; i!= 0; --i )
    {
        WaitForTick();
    }
}
/*void delay_ms(int msec)
{
    for (;msec != 0; --msec)
    {
        WaitForTick();
    }
}*/

void Delay(int sec)
{
    for (;sec != 0; --sec)
    {
        WaitForTick();
    }
}

// waiting to do: to arrangement the code
int pwmtmr010_main(void)
{
		UINT8 chr;
    //PinMux_UART010(0);
#if defined(__MCU_FA606TE__)
    fLib_Int_Init();
#endif	
    //fLib_SerialInit(DEBUG_CONSOLE,DEFAULT_CONSOLE_BAUD, PARITY_NONE, 0, 8);
    fLib_printf("\n\n\nPWMTMR010 Test Begin...............\n"); 
    fLib_printf("Begin PwmTmr Test...\n");

    //Timer1
    fLib_printf("Timer1 Test...(As clock, Press \"ESC\" key to End Timer1 Test ...)\n");
  
    sec=min=hr=0;   
    #if defined(__MCU_FA606TE__)		
    if(!fLib_Timer_Init(DRVPWMTMR1, PWMTMR_1000MSEC_PERIOD, (UINT32)PWMTMR1_IRQHandler))//tick every 1 sec
    #else
	if(!fLib_Timer_Init(DRVPWMTMR1, PWMTMR_1000MSEC_PERIOD))//tick every 1 sec
    #endif
    {
        fLib_printf("Init timer%d Fail!\n",DRVPWMTMR1);
    }
    else
    {
			while(fLib_getchar(DEBUG_CONSOLE) != 0x1b)
			{
				chr = fLib_getchar(DEBUG_CONSOLE);	//Press "ESC" key to terminate GPIO test					
				if (chr==0x1b)
				{
					break;
				}
			}
    }

    fLib_Timer_Close(DRVPWMTMR1);
#if 0
	if( MAX_TIMER >= 2)
	{
		//Timer2
	    fLib_printf("\nTimer2 Test..., Press \"ESC\" key to Timer3 Test ...)\n");
	    // sec=min=hr=0;   
	    #if defined(__MCU_FA606TE__)
	    if(!fLib_Timer_Init(DRVPWMTMR2, PWMTMR_1000MSEC_PERIOD, (UINT32)PWMTMR2_IRQHandler))//tick every 1 sec
	    #else
		if(!fLib_Timer_Init(DRVPWMTMR2, PWMTMR_1000MSEC_PERIOD))//tick every 1 sec
	    #endif		
		{
		  fLib_printf("Init timer%d Fail!\n",DRVPWMTMR2);
		}
		else
		{
		  while(1)
		  {
			  chr = fLib_getchar(DEBUG_CONSOLE);  //Press "ESC" key to terminate GPIO test	  
			  if (chr==0x1b)
			  {
				  break;
			  }
		  }
		}
	    fLib_Timer_Close(DRVPWMTMR2);    
    }
    
	if( MAX_TIMER >= 3)
	{
		//Timer3
		fLib_printf("\nTimer3 Test...,Press \"ESC\" key to Timer4 Test ...)\n");
	   // sec=min=hr=0;   
		#if defined(__MCU_FA606TE__)
	    if(!fLib_Timer_Init(DRVPWMTMR3, PWMTMR_1000MSEC_PERIOD, (UINT32)PWMTMR3_IRQHandler))//tick every 1 sec
	    #else
		if(!fLib_Timer_Init(DRVPWMTMR3, PWMTMR_1000MSEC_PERIOD))//tick every 1 sec
	    #endif
		{
			fLib_printf("Init timer%d Fail!\n",DRVPWMTMR3);
		}
		else
		{
			while(1)
			{				
			chr = fLib_getchar(DEBUG_CONSOLE);	//Press "ESC" key to terminate GPIO test	
			  if (chr==0x1b)
			  {
				  break;
			  }
			}
		}
		fLib_Timer_Close(DRVPWMTMR3);   
	}
	
	if( MAX_TIMER >= 4)
	{
		//Timer4
		//sec=min=hr=0;   
		fLib_printf("\nTimer4 Test...,Press \"ESC\" key to finish Test ...)\n");
		#if defined(__MCU_FA606TE__)
	    if(!fLib_Timer_Init(DRVPWMTMR4, PWMTMR_1000MSEC_PERIOD, (UINT32)PWMTMR4_IRQHandler))//tick every 1 sec
	    #else
		if(!fLib_Timer_Init(DRVPWMTMR4, PWMTMR_1000MSEC_PERIOD))//tick every 1 sec
	    #endif
		{
		  fLib_printf("Init timer%d Fail!\n",DRVPWMTMR4);
		}
		else
		{
		  while(1)
		  {
			  chr = fLib_getchar(DEBUG_CONSOLE);  //Press "ESC" key to terminate GPIO test	  
				if (chr==0x1b)
				{
					break;
				}
		  }
		}
	   fLib_Timer_Close(DRVPWMTMR4);
	}
#endif
    fLib_printf("End Timer Test!\n");
    //while(1);
}

#if defined(__MCU_FA606TE__)
#if defined(__GHS__)
__interrupt
#else
__irq
#endif
#endif
void PWMTMR1_IRQHandler(void)
{	
 	fLib_Timer_IntClear(DRVPWMTMR1);

    T1_Tick++;
 	sec++;
#ifdef PWMTMR_TEST
	if( sec == 60 )
	{
		min++;
		sec=0;
		if( min == 60 )
		{
			hr++;
			min=0;
		}
	}
	
	fLib_printf("1: %02d: %02d: %02d\r",hr,min,sec);
#endif
}

#if defined(__MCU_FA606TE__)
#if defined(__GHS__)
__interrupt
#else
__irq
#endif
#endif
void PWMTMR2_IRQHandler(void)
{
 	fLib_Timer_IntClear(DRVPWMTMR2);

    T2_Tick++;
 	sec++;
	if( sec == 60 )
	{
		min++;
		sec=0;
		if( min == 60 )
		{
			hr++;
			min=0;
		}
	}
	
	fLib_printf("2: %02d: %02d: %02d\r",hr,min,sec);
}

#if defined(__MCU_FA606TE__)
#if defined(__GHS__)
__interrupt
#else
__irq
#endif
#endif
void PWMTMR3_IRQHandler(void)
{
 	fLib_Timer_IntClear(DRVPWMTMR3);

    T3_Tick++;
 	sec++;
	if( sec == 60 )
	{
		min++;
		sec=0;
		if( min == 60 )
		{
			hr++;
			min=0;
		}
	}
	
	fLib_printf("3: %02d: %02d: %02d\r",hr,min,sec);    
}

#if defined(__MCU_FA606TE__)
#if defined(__GHS__)
__interrupt
#else
__irq
#endif
#endif
void PWMTMR4_IRQHandler(void)
{
 	fLib_Timer_IntClear(DRVPWMTMR4);

    T4_Tick++;
 	sec++;
	if( sec == 60 )
	{
		min++;
		sec=0;
		if( min == 60 )
		{
			hr++;
			min=0;
		}
	}
	
	fLib_printf("4: %02d: %02d: %02d\r",hr,min,sec);    
}

