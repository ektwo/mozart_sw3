/*
 * Kneron USB Communication driver
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#include "cmsis_os2.h"
#include "types.h"
#include "kneron_mozart.h"
#include "scu_extreg.h"
#include "base.h"
#include "usb_com.h"
#include "dbg.h"

//#define USE_DIRECT_DMA_OPTION

#define FLAG_USB_COMM_ISR           BIT(1)
#define FLAG_USB_COMM_WRITE         BIT(2)
#define FLAG_USB_COMM_READ          BIT(3)

#define FLAG_USB_COMM_ALL          (FLAG_USB_COMM_ISR)

extern INT8U *usb_write_buf;
extern INT8U *usb_read_buf;
extern INT32 i32UsbReadLength;
extern INT32 i32UsbWriteLength;

unsigned int FOTG200_BASE_ADDRESS;	
unsigned int OTG_BASE_ADDRESS;
unsigned int FOTG210_IRQ;	

osThreadId_t tid_usb_comm;
osThreadId_t tid_to_notify;
uint32_t flags_to_notify;

void OTGP_main(UINT8 bWaitForVBUS);
void OTG_IRQHandler(void);

#ifdef USE_DIRECT_DMA_OPTION
void vUsbWrite(u8* buffer, u32 size);
void vUsbRead(u8* buf, u32 size);
#endif

void usb_driver_probe(void) {
    
    FOTG200_BASE_ADDRESS = USB_FOTG210_PA_BASE;
    OTG_BASE_ADDRESS = USB_FOTG210_PA_BASE;
    FOTG210_IRQ = OTG_SBS_3_IRQ;    
    
    SCU_EXTREG_USB_OTG_CTRL_SET_EXTCTRL_SUSPENDM(1);
    SCU_EXTREG_USB_OTG_CTRL_SET_u_iddig(1);
    SCU_EXTREG_USB_OTG_CTRL_SET_wakeup(0);
    SCU_EXTREG_USB_OTG_CTRL_SET_l1_wakeup(0);
    SCU_EXTREG_USB_OTG_CTRL_SET_OSCOUTEN(0);
    SCU_EXTREG_USB_OTG_CTRL_SET_PLLALIV(0);
    SCU_EXTREG_USB_OTG_CTRL_SET_XTLSEL(0);
    SCU_EXTREG_USB_OTG_CTRL_SET_OUTCLKSEL(0);
}

static void usb_comm_isr(void)
{
    osThreadFlagsSet(tid_usb_comm, FLAG_USB_COMM_ISR);
    NVIC_DisableIRQ((IRQn_Type)FOTG210_IRQ);
}

static void usb_comm_thread(void *argument)
{
    int32_t flags;

    usb_driver_probe();
    OTGP_main(1);

    NVIC_EnableIRQ(FOTG210_IRQ);
    NVIC_SetVector((IRQn_Type)FOTG210_IRQ, (uint32_t)usb_comm_isr);
    
    while (1) {
        flags = osThreadFlagsWait(FLAG_USB_COMM_ALL, osFlagsWaitAny, osWaitForever);
        osThreadFlagsClear(flags);

        if (flags & FLAG_USB_COMM_ISR) {
            OTG_IRQHandler();

            NVIC_EnableIRQ(FOTG210_IRQ);
        }
    }
}

void usb_done_notify(void)
{
    osThreadFlagsSet(tid_to_notify, flags_to_notify);
}

int usb_com_write(uint8_t *BufferPtr, uint32_t BufferLen, uint32_t flags)
{
#ifdef MOZART_CHIP
    usb_write_buf = BufferPtr;
    i32UsbWriteLength = 	BufferLen;

#ifdef USE_DIRECT_DMA_OPTION
    vUsbWrite(BufferPtr, BufferLen);
#else
    tid_to_notify = osThreadGetId();
    flags_to_notify = flags;
    osThreadFlagsWait(flags, osFlagsWaitAll, osWaitForever);
#endif
#endif

    return 0;
}

int usb_com_read(uint8_t *BufferPtr, uint32_t BufferLen, uint32_t flags)
{
#ifdef MOZART_CHIP
    usb_read_buf = BufferPtr;
    i32UsbReadLength =  BufferLen;

#ifdef USE_DIRECT_DMA_OPTION
    vUsbRead(BufferPtr, BufferLen);
#else
    tid_to_notify = osThreadGetId();
    flags_to_notify = flags;
    osThreadFlagsWait(flags, osFlagsWaitAll, osWaitForever);
#endif
#endif

    return 0;
}

void usb_init(void)
{
#ifdef MOZART_CHIP
    tid_usb_comm = osThreadNew(usb_comm_thread, NULL, NULL);
#endif
}
