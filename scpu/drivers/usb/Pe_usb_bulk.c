///////////////////////////////////////////////////////////////////////////////
//
//  File name: usb_bulk.c
//  Version: 1.0
//  Date: 2003/8/05
//
//  Author: Andrew
//  Email: yping@faraday.com.tw
//  Phone: (03) 578-7888
//  Company: Faraday Tech. Corp.
//
//  Description: USB Bulk IN OUT test Rountine & relatived subroutine
//
///////////////////////////////////////////////////////////////////////////////
#define USB_BULK_GLOBALS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include "flib.h"
//#include "DrvOTG210_Pe_usb.h"
#include "common_include.h"
#include "DrvOTG210_Pe_peripheral.h"
#include "DrvOTG210_Pe_my_usbtable.h"
#include "Pe_usb_scsi.h"
#include "usb_com.h"
#undef CONFIG_FTSPI020
#ifdef CONFIG_FTSPI020
#include "FTSPI020/DrvSPI020.h"
#if (SPI020_MODE == PIO_MODE)
UINT8 flash_buf[512];

#define SPI020_DMA_DATAPORT	(FTSPI020_PA_BASE+0x100)
#endif

#endif //CONFIG_FTSPI020

#define WATCH_DMA2 1

#define min_t(x,y) ( x < y ? x: y )
#define MAX_DMA_LENGTH 0x20000
static MassStorageState eOTGProessCBW(void);
//#ifdef CONFIG_FTSPI020
MassStorageState eOTGDataOut(INT16U u16FIFOByteCount);
//#endif //CONFIG_FTSPI020
MassStorageState eOTGDataIn(void);
#ifdef SHOW_DEBUG_MESSAGE
static void vShowCBW(void);
//static void vShowCSW(void);
#endif
static void vOTGSendCSW(void);
static void vOTG_FIFO_INT_action(MassStorageState eState);

MassStorageState eUsbOTGMassStorageState;
INT8U*   u8VirtOTGMemory = NULL;//[MAX_BUFFER_SIZE];
INT32U*   u32VirtOTGMemory = NULL;

//INT32 usb_read_off;
//INT32 usb_write_off;
uint8_t ACK[8] = {0x35, 0x8A, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00};
BOOL wReady = FALSE;
BOOL rReady = FALSE;
//volatile INT32U usb_dma_read = (INT32U)DDR_MEM_BASE + 0x2000000;
//volatile INT32U usb_dma_write;
INT32U usb_write_buf;
INT32U usb_read_buf;
INT32 i32UsbReadLength = 0;
INT32 i32UsbWriteLength = 0;


CSW tOTGCSW;
//INT32U   u32VirtOTGMemoryIndex;
CBW tOTGCBW;

//INT32U   u32OTGFIFOUseDMA;
//INT32U   u32UseDMAOTGChannel;
INT8U   bOTGDMARunning;
//INT8U u8SCSIRespData[DATA_LENGTH_SCSI_RESP];
INT8U *pSCSIRespData;

extern USB_COMMAND_T        vendor_command;
#ifdef CONFIG_FTSPI020
extern SPI_Flash_T  flash_info;
#endif //CONFIG_FTSPI020

//INT8U scsi_count=1;
///////////////////////////////////////////////////////////////////////////////
//      vOTG_APInit()
//      Description: User specified circuit (AP) init
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vOTG_APInit(void)
{
    ////// initial Bulk //////
    eUsbOTGMassStorageState = STATE_CBW;    // Init State
    tOTGCSW.u32Signature = CSW_SIGNATE; // Init u32Signature

}

///////////////////////////////////////////////////////////////////////////////
//      vUsb_Bulk_Out()
//      Description: USB FIFO2 interrupt service process
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vUsb_Bulk_Out(INT16U u16FIFOByteCount)
{


    eUsbOTGMassStorageState = eOTGDataOut(u16FIFOByteCount);

    vOTG_FIFO_INT_action(eUsbOTGMassStorageState);
			
}

///////////////////////////////////////////////////////////////////////////////
//		vUsb_Bulk_In()
//		Description: USB FIFO0 interrupt service process
//		input: none
//		output: none
///////////////////////////////////////////////////////////////////////////////
void vUsb_Bulk_In(void)
{

	eOTGDataIn();

}


///////////////////////////////////////////////////////////////////////////////
//		vOTG_FIFO_INT_action()
//		Description: FIFO interrupt enable or not
//					 depend on the STORAGE state
//		input: none
//		output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTG_FIFO_INT_action(MassStorageState eState)
{
    switch(eState)
    {
    case STATE_CBW:
    case STATE_CB_DATA_OUT:

#if(Bulk_Satus == Bulk_FIFO_SingleDir)
#if(EP2_START_FIFO == DOUBLE_BLK)
        mUsbIntF2OUTEn();
#else
        mUsbIntF1OUTEn();
#endif
#elif(Bulk_Satus == Bulk_FIFO_BiDir)
        mUsbIntF0OUTEn();
#endif
        mUsbIntF0INDis();
#if 0
#ifndef FIFO_AUTO_SWITCH
        if (eState == STATE_CBW )//if next state is DATA_OUT, it doesn't need to change FIFO direction.This is because last state is CBW
        {
            while( !mUsbFIFO0Empty() ); //bessel:test:sometimes, enter into unlimited loop
            mUsbClrAllFIFOSet(); //bessel:testing:should not have
            mUsbFIFOMap(FS_C1_I0_A0_EP2_FIFO_START, FS_C1_I0_A0_EP2_FIFO_MAP);//FIFO 0 map to EP2 and FIFO direction is OUT
        }
#endif
#endif
        break;
    case STATE_CSW:
    case STATE_CB_DATA_IN:
#if(Bulk_Satus == Bulk_FIFO_SingleDir)
#if(EP2_START_FIFO == DOUBLE_BLK)
        mUsbIntF2OUTDis();
#else
        mUsbIntF1OUTDis();
#endif
#elif(Bulk_Satus == Bulk_FIFO_BiDir)
        mUsbIntF0OUTDis();
#endif
        mUsbIntF0INEn();

#if 0
#ifndef FIFO_AUTO_SWITCH
        while( !mUsbFIFO0Empty() );//bessel:test:sometimes, enter into unlimited loop
        mUsbClrAllFIFOSet(); //bessel:testing:should not have
        mUsbFIFOMap(FS_C1_I0_A0_EP1_FIFO_START, FS_C1_I0_A0_EP1_FIFO_MAP);//FIFO 0 map to EP1 and FIFO direction is IN
#endif
#endif
        break;
    default:
        break;
    }
}


///////////////////////////////////////////////////////////////////////////////
//		eOTGProessCBW()
//		Description: Process the CBW
//		input: none
//		output: MassStorageState
///////////////////////////////////////////////////////////////////////////////
//#define SCSI_COMMAND	SCSI_OP_INQUIRY
//#define SCSI_COMMAND	U320_SCSI_CMD_CODE
#define SCSI_COMMAND	SCSI_OP_READ_CAPACITY

static MassStorageState eOTGProessCBW(void)
{
    MassStorageState eState;
    INT8U EP_No;
//	CBW cbw_temp;
#if(Bulk_Satus == Bulk_FIFO_SingleDir)
    vOTGDxFRd(EP2_START_FIFO, (INT8U *)(&tOTGCBW), 31);			// change little endian to big endian
    //vOTGDxFRd(EP2_START_FIFO, (INT8U *)(&(tOTGCBW.u8Flags)), 19);
#elif(Bulk_Satus == Bulk_FIFO_BiDir)
    //vOTGDxFRd(FIFO0, (INT8U *)(&tOTGCBW), 12);			// change little endian to big endian
    //vOTGDxFRd(FIFO0, (INT8U *)(&(tOTGCBW.u8Flags)), 19);
    vOTGDxFRd(EP2_START_FIFO, (INT8U *)(&tOTGCBW), 31);
#endif

//#if ShowMassMsg
#ifdef SHOW_DEBUG_MESSAGE
    //if(u8OTGMessageLevel & MESS_INFO) //bessel:test:if want to show CBW contents
      //  vShowCBW();
#endif
//#endif

    if(tOTGCBW.u32Signature != CBW_SIGNATE)
    {

        fLib_printf("Signature is not correct\n");
       
        #ifdef SHOW_DEBUG_MESSAGE
        vShowCBW();
        #endif
        EP_No=mUsbFIFOMapRd(FIFO0);
        mUsbEPinStallSet(EP_No);
        //mUsbEPinStallSet(mUsbFIFOMapRd(FIFO0));
#if(Bulk_Satus == Bulk_FIFO_SingleDir)
        mUsbEPoutStallSet(mUsbFIFOMapRd(FIFO2));
#elif(Bulk_Satus == Bulk_FIFO_BiDir)
        EP_No=mUsbFIFOMapRd(FIFO0);
		mUsbEPoutStallSet(EP_No);
        //mUsbEPoutStallSet(mUsbFIFOMapRd(FIFO0));
#endif
        eState = STATE_CBW;
    }
    else
    {
        // pass u32DataTransferLength to u32DataResidue
        tOTGCSW.u32DataResidue = tOTGCBW.u32DataTransferLength;
        // pass Tag from CBW to CSW
        tOTGCSW.u32Tag = tOTGCBW.u32Tag;
        // Assume Status is CMD_PASS
        //tOTGCSW.u8Status = CSW_STATUS_CMD_PASS;
        // Get Virtual Memory start address.

#if 1
        //u32VirtOTGMemoryIndex = (INT32U)(tOTGCBW.u8CB[0]) | ((INT32U)tOTGCBW.u8CB[1] << 8);
#else
        u32VirtOTGMemoryIndex = ((INT32U)(tOTGCBW.u8CB[0]) | ((INT32U)tOTGCBW.u8CB[1] << 8))>>2;
#endif
#if 0
        if (tOTGCSW.u32DataResidue == 0)
            eState = STATE_CSW;
        else if (tOTGCBW.u8Flags == 0x00)
            eState = STATE_CB_DATA_OUT;
        else
            eState = STATE_CB_DATA_IN;
#endif
        //scsi_count=1;
        //eState=eSCSI_CmdDecode();
    }
    return eState;
}

///////////////////////////////////////////////////////////////////////////////
//		Waiting_For_DMA_Complete()
//		Description: Process the data intput stage
//		input: none
//		output: MassStorageState
///////////////////////////////////////////////////////////////////////////////
#if 1
void Waiting_For_DMA_Complete(INT8U FIFONum,INT8U source)
{
    UINT32 wTemp;
//	  UINT32 tmp;
	
//static	UINT32 i=0;
//Waiting for Complete
  //  bOTGDMARunning = TRUE;
#ifdef WATCH_DMA1  
	fLib_printf("FIFONum is %d \n",FIFONum);
	fLib_printf("source is %d \n",source);
#endif
    while(1)
    {
        wTemp = mUsbIntSrc2Rd();
        if(wTemp & BIT8)//DMA Error Interrupt
        {
        	if(FIFONum == CX_EP)
				mUsbCxFClr();
        	else
            	mUsbFIFOClr((INT32U)FIFONum);
            mUsbIntDmaErrClr();
            fLib_printf("FIFO%d transfer DMA error.., source=%d\n",FIFONum,source);
            while(1);
            //break;
        }
        if(wTemp & BIT7)//DMA Completion Interrupt
        {
            mUsbIntDmaFinishClr();
		      // fLib_printf("d%d \n",i++);
#ifdef WATCH_DMA2
//            if(mUsbFIFO0Empty())
//            	fLib_printf("FIFO 0 is  empty\n");
         //   fLib_printf("DMA_C\n");
#endif
            break;
        }
#if 0
        if((wTemp & 0x00000007)>0)//If (Resume/Suspend/Reset) exit
        {
            mUsbDMARst();
            mUsbFIFOClr((INT32U)FIFONum);
            mUsbIntDmaFinishClr();
            fLib_printf("L%x: FIFO%d OUT transfer DMA stop because USB Resume/Suspend/Reset..", u8LineOTGCount ++,FIFONum);
            break;
        }
#endif
    }
    mUsbDMA2FIFOSel(FOTG200_DMA2FIFO_Non);
   // bOTGDMARunning = FALSE;
}
#endif

//#ifdef CONFIG_FTSPI020
///////////////////////////////////////////////////////////////////////////////
//		eOTGDataOut()
//		Description: Process the data output stage
//		input: none
//		output: MassStorageState
///////////////////////////////////////////////////////////////////////////////
#if 1
MassStorageState eOTGDataOut(INT16U u16FIFOByteCount)
{

    if (i32UsbReadLength <= 0 ) {
        rReady = FALSE;
        usb_read_buf = 0;
        return STATE_CB_DATA_OUT;
    }

    //	usb_dma_read = (INT32U)DDR_MEM_BASE + 0x2000000;
 
	  mUsbDmaConfig(u16FIFOByteCount, DIRECTION_OUT);
		//fLib_printf("EP2_START_FIFO=%d\n", EP2_START_FIFO);
		mUsbDMA2FIFOSel(1<<(EP2_START_FIFO));	
		mUsbDmaAddr(usb_read_buf);

    mUsbDmaStart();
    Waiting_For_DMA_Complete(EP2_START_FIFO,13);
		//usb_read_off += u16FIFOByteCount;
		i32UsbReadLength -= u16FIFOByteCount;
    usb_read_buf += u16FIFOByteCount;

    if (0 == i32UsbReadLength) {
        rReady = TRUE;
        usb_read_buf = 0;

        usb_done_notify();
    }

    return STATE_CB_DATA_OUT;

}
#endif 
#if 0
MassStorageState eOTGDataOut(INT16U u16FIFOByteCount)
{

    UINT32 		 reg;

	if (u32KReadLength <= 0 )
		return STATE_CB_DATA_OUT;
//	usb_dma_read = (INT32U)DDR_MEM_BASE + 0x2000000;
	 memset((INT8U * )usb_read_buf, 0, u32KWriteLength);
					   u32 num = u32KReadLength / 8* 1024;
           u32 leftover = u32KReadLength % 8* 1024;

if (num > 0) { 
	mUsbDmaConfig(8* 1024, DIRECTION_OUT);
for (int i = 0; i < num; i++)
{
 
	
			//fLib_printf("EP2_START_FIFO=%d\n", EP2_START_FIFO);
			mUsbDMA2FIFOSel(1<<EP2_START_FIFO);	
		
		mUsbDmaAddr(usb_dma_read);

			mUsbDmaStart();
			Waiting_For_DMA_Complete(EP2_START_FIFO,13);
			
	//		memcpy(OTG_K_read_buf + usb_read_off, ( INT8U *)usb_dma_read, u16FIFOByteCount);
			usb_read_off += 8* 1024;
			u32KReadLength -= 8* 1024;
	    usb_dma_read += 8* 1024;
	//			fLib_printf("%s\n\r", OTG_K_read_buf);
	//	OTG_K_read_buf+= u16FIFOByteCount;
//while (1) {}
}
}
if (leftover != 0)
  {
 memset((INT8U * )usb_dma_read, 0, leftover);
	mUsbDmaConfig(leftover, DIRECTION_OUT);
			//fLib_printf("EP2_START_FIFO=%d\n", EP2_START_FIFO);
			mUsbDMA2FIFOSel(1<<EP2_START_FIFO);	
		
		mUsbDmaAddr(usb_dma_read);

			mUsbDmaStart();
			Waiting_For_DMA_Complete(EP2_START_FIFO,13);
			
	//		memcpy(OTG_K_read_buf + usb_read_off, ( INT8U *)usb_dma_read, u16FIFOByteCount);
			usb_read_off += leftover;
			u32KReadLength -= leftover;
	    usb_dma_read += leftover;
}
	        return STATE_CB_DATA_OUT;
}
#endif 
void Remaining_DataIn_Configure()
{
    mUsbDMA2FIFOSel(FOTG200_DMA2FIFO0);
#if 0
    mUsbIntEP0SetupDis();//Disable setup command interrupt during data transmission
    mUsbIntEP0InDis();
    mUsbIntEP0OutDis();
    mUsbIntEP0EndDis();
#endif

    //u32OTGFIFOUseDMA = FIFO0;

    mUsbDmaStart();
    Waiting_For_DMA_Complete(FIFO0,10);
#ifdef CONFIG_FTSPI020
#if (SPI020_MODE == USB_DMA_MODE)
    if(vendor_command.subcmd == SUBCMD_READ_SPI_FLASH)
        spi020_dma_read_stop();/* Stop SPI data */
#endif
#endif //CONFIG_FTSPI020
    mUsbFIFODone(FIFO0);
}
///////////////////////////////////////////////////////////////////////////////
//		eOTGDataIn()
//		Description: Process the data intput stage
//		input: none
//		output: MassStorageState
///////////////////////////////////////////////////////////////////////////////
MassStorageState eOTGDataIn(void)
{


if  (i32UsbWriteLength <= 0)
	 return STATE_CB_DATA_IN;

//usb_dma_write = (INT32U)DDR_MEM_BASE + 0x1000000;


					   u32 num = i32UsbWriteLength / MAX_DMA_LENGTH;
           u32 leftover = i32UsbWriteLength % MAX_DMA_LENGTH;

//  
//  for (int i = 0; i < num; i++)
//  {
//	   memcpy((INT8U * )usb_dma_write, OTG_K_write_buf + usb_write_off, MAX_DMA_LENGTH);
//			 mUsbDmaAddr((INT32U)usb_dma_write);
//       mUsbDmaConfig(MAX_DMA_LENGTH,DIRECTION_IN);
//		Remaining_DataIn_Configure();
//		usb_write_off += MAX_DMA_LENGTH;

//  }
  if (leftover != 0)
  {
	   
			 mUsbDmaAddr((INT32U)ACK);
       mUsbDmaConfig(8,DIRECTION_IN);
  		Remaining_DataIn_Configure();
  }
					
			
    return STATE_CSW;
}



///////////////////////////////////////////////////////////////////////////////
//		vShowCBW()
//		Description: show the whole CBW structure
//		input: none
//		output: none
///////////////////////////////////////////////////////////////////////////////
#ifdef SHOW_DEBUG_MESSAGE
static void vShowCBW(void)
{
    INT8U i;
    INT8U * pp;
    pp = (INT8U *)&tOTGCBW;
    //fLib_printf("tOTGCBW: \n");
    for (i = 0; i < 16; i ++)
        fLib_printf("%02x ", *(pp ++));
    fLib_printf("\n");
    for (i = 16; i < 31; i ++)
        fLib_printf("%02x ", *(pp ++));
    fLib_printf("\n");
}
#endif
///////////////////////////////////////////////////////////////////////////////
//		vShowCSW()
//		Description: show the whole CSW structure
//		input: none
//		output: none
///////////////////////////////////////////////////////////////////////////////
#if 0
static void vShowCSW(void)
{
    INT8U i;
    INT8U * pp;
    pp = (INT8U *)&tOTGCSW;
    fLib_printf("tOTGCSW: \n");
    for (i = 0; i < 13; i ++)
        fLib_printf("%02x ", *(pp ++));
    fLib_printf("\n");
}
#endif
///////////////////////////////////////////////////////////////////////////////
//		vOTGSendCSW()
//		Description: Send out the CSW structure to PC
//		input: none
//		output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTGSendCSW()
{
    // Send CSW to F0 via DBUS;

//	vShowCSW();

    vOTGDxFWr(FIFO0, (INT8U *)(&tOTGCSW) , 13);
    mUsbFIFODone(FIFO0);
}
