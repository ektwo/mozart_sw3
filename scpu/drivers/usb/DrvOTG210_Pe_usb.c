/******************** ******************** ******************** ***************
//
//  File name: usb.c
//  Version: 1.0
//  Date: 2003/8/05
//
//  Author: Andrew
//  Email: yping@faraday.com.tw
//  Phone: (03) 578-7888
//  Company: Faraday Tech. Corp.
//
//  Description: USB Service Rountine & relatived subroutine
//
//  1. vOTGInit(void)
//      1.1 vFOTG200_Dev_Init(void);
//  2. vOTGHandler(void)
//      2.1 vOTG_ep0setup(void);
//          2.1.1 bOTGStandardCommand(void);
//              2.1.1.1 bGet_OTGstatus();
//              2.1.1.2 bClear_OTGfeature();
//              2.1.1.3 bSet_OTGfeature();
//              2.1.1.4 bSet_OTGaddress();
//              2.1.1.5 bGet_OTGdescriptor();
//                  2.1.1.5.1 vOTGEP0TxData(void);
//              2.1.1.6 bSet_OTGdescriptor();
//                  2.1.1.6.1 vOTGEP0RxData(void);
//              2.1.1.7 vGet_OTGconfiguration();
//              2.1.1.8 bSet_OTGconfiguration();
//                  2.1.1.8.1 vOTGClrEPx(void);
//              2.1.1.9 bGet_OTGinterface();
//              2.1.1.10 bSet_OTGinterface();
//                  2.1.1.10.1 vOTGClrEPx(void);
//              2.1.1.11 bSynch_OTGframe();
//      2.2 vOTG_ep0tx(void);
//          2.2.1 vOTGEP0TxData(void);
//      2.3 vOTG_ep0rx(void);
//          2.3.1 vOTGEP0RxData(void);
//      2.4 vOTG_ep0end(void);
//      2.5 vOTG_ep0fail(void);
//      2.6 vOTG_rst(void);
//      2.7 vOTG_suspend(void);
//      2.8 vOTG_resm(void);
//      2.9 vOTG_ISO_SeqErr(void);
//      2.10 vOTG_ISO_SeqAbort(void);
//      2.11 vOTG_TX0Byte(void);
//      2.12 vOTG_RX0Byte(void);
******************************************************************************/
#define USB_GLOBALS

#define NoFixPort           0
#define FixPort         1
#define USB_DataPort        FixPort

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "common_include.h"
//#include "flib.h"
//#include "tool/command.h"
//#include "DrvOTG210_Pe_usb.h"
//#include "Pe_usb_hs_pos.h"
//#include "Pe_usb_fs_pos.h"
#include "DrvOTG210_Pe_peripheral.h"
#include "DrvOTG210_Pe_my_usbtable.h"
#include "DrvOTG210_OTGController.h"
#include "DrvOTG210_Pe_usb.h"
//#include "spec.h"
//INT32U u32KReadLength = 0;
//INT32U u32KWriteLength = 0;
extern unsigned int FOTG210_IRQ;	
//#define BLUETOOTH_FUNCTION_TEST
#define CONFIG_FOTG021_INVERSE_INT_POLARITY
//#define VDMA
extern INT8U   bOTGDMARunning;
#ifdef BLUETOOTH_FUNCTION_TEST
#define GETCH fLib_getchar
#endif
#ifdef HS_SUPPORTED
// High speed Configuration
static INT8U u8OTGHSDeviceDescriptor[DEVICE_LENGTH] =
{
    //  DEVICE descriptor
    DEVICE_LENGTH,                  // bLength
    DT_DEVICE,                      // bDescriptorType
    mLowByte(HS_USB_SPEC_VER),          // bcdUSB
    mHighByte(HS_USB_SPEC_VER),
    HS_bDeviceClass,                // bDeviceClass
    HS_bDeviceSubClass,             // bDeviceSubClass
    HS_bDeviceProtocol,             // bDeviceProtocol
    EP0MAXPACKETSIZE,               // bMaxPacketSize0
    mLowByte(HS_VENDOR_ID),         // idVendor
    mHighByte(HS_VENDOR_ID),
    mLowByte(HS_PRODUCT_ID),            // idProduct
    mHighByte(HS_PRODUCT_ID),
    mLowByte(HS_DEVICE_RELEASE_NO), // bcdDeviceReleaseNumber
    mHighByte(HS_DEVICE_RELEASE_NO),
    HS_iManufacturer,               // iManufacturer
    HS_iProduct,                    // iProduct
    HS_iSerialNumber,               // iSerialNumber
    HS_CONFIGURATION_NUMBER         // bNumConfigurations
};

// High speed Configuration
#if (HS_CONFIGURATION_NUMBER >= 1)
// Configuration 1
static INT8U u8HSConfigOTGDescriptor01[HS_C1_CONFIG_TOTAL_LENGTH] =
{
    //  CONFIGURATION descriptor
    CONFIG_LENGTH,                  // bLength
    DT_CONFIGURATION,               // bDescriptorType CONFIGURATION
    mLowByte(HS_C1_CONFIG_TOTAL_LENGTH),    // wTotalLength, include all descriptors
    mHighByte(HS_C1_CONFIG_TOTAL_LENGTH),
    HS_C1_INTERFACE_NUMBER,         // bNumInterface
    HS_C1,                          // bConfigurationValue
    HS_C1_iConfiguration,           // iConfiguration
    HS_C1_bmAttribute,              // bmAttribute
    // D7: Reserved(set to one), D6: Self-powered, D5: Remote Wakeup, D4..0: Reserved(reset to zero)
    HS_C1_iMaxPower,                // iMaxPower (2mA / units)

#if (HS_C1_INTERFACE_NUMBER >= 1)
    // Interface 0
#if (HS_C1_I0_ALT_NUMBER >= 1)
    // Alternate Setting 0
    INTERFACE_LENGTH,               // bLength
    DT_INTERFACE,                   // bDescriptorType INTERFACE
    HS_C1_I0_A0_bInterfaceNumber,      // bInterfaceNumber
    HS_C1_I0_A0_bAlternateSetting,      // bAlternateSetting
    HS_C1_I0_A0_EP_NUMBER,          // bNumEndpoints(excluding endpoint zero)
    HS_C1_I0_A0_bInterfaceClass,    // bInterfaceClass
    HS_C1_I0_A0_bInterfaceSubClass,// bInterfaceSubClass
    HS_C1_I0_A0_bInterfaceProtocol,// bInterfaceProtocol
    HS_C1_I0_A0_iInterface,     // iInterface

#if (HS_C1_I0_A0_EP_NUMBER >= 1)
    // EP1
    EP_LENGTH,                      // bLength
    DT_ENDPOINT,                    // bDescriptorType ENDPOINT
    (((1 - HS_C1_I0_A0_EP1_DIRECTION) << 7) | EP1), // bEndpointAddress
    // D7: Direction, 1=IN, 0=OUT
    // D6..4: Reserved(reset to zero), D3..0: The endpointer number
    HS_C1_I0_A0_EP1_TYPE,           // bmAttributes
    // D1..0: Transfer Type 00=Control, 01=Isochronous, 10=Bulk, 11=Interrupt
    // if not an isochronous endpoint, D7..2 are Reserved
    mLowByte(HS_C1_I0_A0_EP1_MAX_PACKET),   // wMaxPacketSize
    mHighByte(HS_C1_I0_A0_EP1_MAX_PACKET),
    HS_C1_I0_A0_EP1_bInterval,          // Interval for polling endpoint for data transfers.
#endif
#if (HS_C1_I0_A0_EP_NUMBER >= 2)
    // EP2
    EP_LENGTH,
    DT_ENDPOINT,
    (((1 - HS_C1_I0_A0_EP2_DIRECTION) << 7) | EP2),
    HS_C1_I0_A0_EP2_TYPE,
    mLowByte(HS_C1_I0_A0_EP2_MAX_PACKET),
    mHighByte(HS_C1_I0_A0_EP2_MAX_PACKET),
    HS_C1_I0_A0_EP2_bInterval,
#endif
#endif
#endif
    //OTG_LENGTH,       //bessel:remove below three items
    //DT_OTG,
    //FS_OTG_SUPPORT,
};
#endif
#endif
// Full speed Configuration
static INT8U u8OTGFSDeviceDescriptor[DEVICE_LENGTH] =
{
    //  DEVICE descriptor : from 0
    DEVICE_LENGTH,                  // bLength
    DT_DEVICE,                      // bDescriptorType
    mLowByte(FS_USB_SPEC_VER),          // bcdUSB
    mHighByte(FS_USB_SPEC_VER),
    FS_bDeviceClass,                // bDeviceClass
    FS_bDeviceSubClass,             // bDeviceSubClass
    FS_bDeviceProtocol,             // bDeviceProtocol
    EP0MAXPACKETSIZE,               // bMaxPacketSize0
    mLowByte(FS_VENDOR_ID),         // idVendor
    mHighByte(FS_VENDOR_ID),
    mLowByte(FS_PRODUCT_ID),            // idProduct
    mHighByte(FS_PRODUCT_ID),
    mLowByte(FS_DEVICE_RELEASE_NO), // bcdDeviceReleaseNumber
    mHighByte(FS_DEVICE_RELEASE_NO),
    FS_iManufacturer,               // iManufacturer
    FS_iProduct,                    // iProduct
    FS_iSerialNumber,               // iSerialNumber
    FS_CONFIGURATION_NUMBER         // bNumConfigurations
};

#if (FS_CONFIGURATION_NUMBER >= 1)
// Configuration 1
static INT8U u8FSConfigOTGDescriptor01[FS_C1_CONFIG_TOTAL_LENGTH] =
{
    //  CONFIGURATION descriptor
    CONFIG_LENGTH,                  // bLength
    DT_CONFIGURATION,               // bDescriptorType CONFIGURATION
    mLowByte(FS_C1_CONFIG_TOTAL_LENGTH),    // wTotalLength, include all descriptors
    mHighByte(FS_C1_CONFIG_TOTAL_LENGTH),
    FS_C1_INTERFACE_NUMBER,         // bNumInterface
    FS_C1,                          // bConfigurationValue
    FS_C1_iConfiguration,           // iConfiguration
    FS_C1_bmAttribute,              // bmAttribute
    // D7: Reserved(set to one), D6: Self-powered, D5: Remote Wakeup, D4..0: Reserved(reset to zero)
    FS_C1_iMaxPower,                // iMaxPower (2mA units)

#if (FS_C1_INTERFACE_NUMBER >= 1)
    // Interface 0
#if (FS_C1_I0_ALT_NUMBER >= 1)
    // Alternate Setting 0
    INTERFACE_LENGTH,               // bLength
    DT_INTERFACE,                   // bDescriptorType INTERFACE
    FS_C1_I0_A0_bInterfaceNumber,      // bInterfaceNumber
    FS_C1_I0_A0_bAlternateSetting,      // bAlternateSetting
    FS_C1_I0_A0_EP_NUMBER,          // bNumEndpoints(excluding endpoint zero)
    FS_C1_I0_A0_bInterfaceClass,    // bInterfaceClass
    FS_C1_I0_A0_bInterfaceSubClass,// bInterfaceSubClass
    FS_C1_I0_A0_bInterfaceProtocol,// bInterfaceProtocol
    FS_C1_I0_A0_iInterface,     // iInterface

#if (FS_C1_I0_A0_EP_NUMBER >= 1)
    // EP1
    EP_LENGTH,                      // bLength
    DT_ENDPOINT,                    // bDescriptorType ENDPOINT
    (((1 - FS_C1_I0_A0_EP1_DIRECTION) << 7) | EP1), // bEndpointAddress
    // D7: Direction, 1=IN, 0=OUT
    // D6..4: Reserved(reset to zero), D3..0: The endpointer number
    FS_C1_I0_A0_EP1_TYPE,           // bmAttributes
    // D1..0: Transfer Type 00=Control, 01=Isochronous, 10=Bulk, 11=Interrupt
    // if not an isochronous endpoint, D7..2 are Reserved
    mLowByte(FS_C1_I0_A0_EP1_MAX_PACKET),   // wMaxPacketSize
    mHighByte(FS_C1_I0_A0_EP1_MAX_PACKET),
    FS_C1_I0_A0_EP1_bInterval,                          // Interval for polling endpoint for data transfers.
#endif
#if (FS_C1_I0_A0_EP_NUMBER >= 2)
    // EP2
    EP_LENGTH,
    DT_ENDPOINT,
    (((1 - FS_C1_I0_A0_EP2_DIRECTION) << 7) | EP2),
    FS_C1_I0_A0_EP2_TYPE,
    mLowByte(FS_C1_I0_A0_EP2_MAX_PACKET),
    mHighByte(FS_C1_I0_A0_EP2_MAX_PACKET),
    FS_C1_I0_A0_EP2_bInterval,
#endif

#endif
#endif

    //OTG_LENGTH,    //bessel:remove below three items
    //DT_OTG,
    //FS_OTG_SUPPORT,
};
#endif

INT8U u8ConfigOTGDescriptorEX[CONFIG_LENGTH_EX];
#ifdef  HS_SUPPORTED
static INT8U u8OtherSpeedConfigOTGDescriptorEX[OTHER_SPEED_CONFIG_LENGTH_EX];
static INT8U u8OTGDeviceQualifierDescriptorEX[DEVICE_QUALIFIER_LENGTH];
#endif

static INT8U  u8OTGString00Descriptor[STRING_00_LENGTH] = {
    STRING_00_LENGTH,            // Size of this descriptor
    // STRING Descriptor type
    // Language ID, 0409: English, 0404: Chinese Taiwan
    0X03,     //bDescriptorType
    0X09,     //bLang
    0X04,
};
#ifdef MANUFACTURER_STRING_SUPPORTED
static INT8U  u8OTGString10Descriptor[STRING_10_LENGTH] = {
    STRING_10_LENGTH,            // Size of this descriptor
    DT_STRING,                   // STRING Descriptor type
    0X4B, 0,  //K
    0X4E, 0,  //N
    0X4F, 0,  //O
    0X52, 0,  //R
    0X45, 0,  //E
    0X4E, 0,  //N
    0X20, 0,  //
    0X69, 0,  //i
    0X6E, 0,  //n
    0X63, 0,  //c
    0X2E, 0   //.
};
#endif
#ifdef PRODUCT_STRING_SUPPORTED
static INT8U  u8OTGString20Descriptor[STRING_20_LENGTH] = {
    STRING_20_LENGTH,            // Size of this descriptor
    DT_STRING,                   // STRING Descriptor type
    0X46, 0,  //M
    0X6F, 0,  //O
    0X54, 0,  //Z
    0X61, 0,  //A
    0X72, 0,  //R
    0X74, 0,  //T
    0X20, 0,  //
    0X20, 0,  //
    0X45, 0,  //E
    0X56, 0,  //V
    0X2D, 0,  //-
    0X62, 0,  //b
    0X6F, 0,  //o
    0X61, 0,  //a
    0X72, 0,  //r
    0X64, 0   //d
};
#endif

#if 0 //bessel:don't need to support string descriptors except for language string
static INT8U  u8OTGString30Descriptor[STRING_30_LENGTH] = {
    STRING_30_LENGTH,            // Size of this descriptor
    DT_STRING,                   // STRING Descriptor type
    0X45, 0,  //E
    0X56, 0,  //V
    0X2D, 0,  //-
    0X62, 0,  //b
    0X6F, 0,  //o
    0X61, 0,  //a
    0X72, 0,  //r
    0X64, 0,  //d
    0X20, 0,  //
    0X43, 0,  //C
    0X6F, 0,  //o
    0X6E, 0,  //n
    0X66, 0,  //f
    0X69, 0,  //i
    0X67, 0,  //g
    0X75, 0,  //u
    0X72, 0,  //r
    0X61, 0,  //a
    0X74, 0,  //t
    0X69, 0,  //i
    0X6F, 0,  //o
    0X6E, 0   //n
};

static INT8U  u8OTGString40Descriptor[STRING_40_LENGTH] = {
    STRING_40_LENGTH,            // Size of this descriptor
    DT_STRING,                   // STRING Descriptor type
    0X45, 0,  //E
    0X56, 0,  //V
    0X2D, 0,  //-
    0X62, 0,  //b
    0X6F, 0,  //o
    0X61, 0,  //a
    0X72, 0,  //r
    0X64, 0,  //d
    0X20, 0,  //
    0X49, 0,  //I
    0X6E, 0,  //n
    0X74, 0,  //t
    0X65, 0,  //e
    0X72, 0,  //r
    0X66, 0,  //f
    0X61, 0,  //a
    0X63, 0,  //c
    0X65, 0   //e
};

static INT8U  u8OTGString50Descriptor[STRING_50_LENGTH] = {
    STRING_50_LENGTH,            // Size of this descriptor
    DT_STRING,                   // STRING Descriptor type
    0X50, 0,  //P
    0X72, 0,  //r
    0X69, 0,  //i
    0X6E, 0,  //n
    0X74, 0,  //t
    0X65, 0,  //e
    0X72, 0   //r
};

static INT8U  u8OTGString60Descriptor[STRING_60_LENGTH] = {
    STRING_60_LENGTH,            // Size of this descriptor
    DT_STRING,                   // STRING Descriptor type
    0X44, 0,  //D
    0X69, 0,  //i
    0X67, 0,  //g
    0X69, 0,  //i
    0X74, 0,  //t
    0X61, 0,  //a
    0X6C, 0,  //l
    0X5F, 0,  //_
    0X43, 0,  //C
    0X61, 0,  //a
    0X6D, 0,  //m
    0X65, 0,  //e
    0X72, 0,  //r
    0X61, 0   //a
};

static INT8U  u8OTGString70Descriptor[STRING_70_LENGTH] = {
    STRING_70_LENGTH,            // Size of this descriptor
    DT_STRING,                   // STRING Descriptor type
    0X53, 0,  //S
    0X74, 0,  //t
    0X6F, 0,  //o
    0X72, 0,  //r
    0X61, 0,  //a
    0X67, 0,  //g
    0X65, 0   //e
};

static INT8U  u8OTGString80Descriptor[STRING_80_LENGTH] = {
    STRING_80_LENGTH,            // Size of this descriptor
    DT_STRING,                   // STRING Descriptor type
    0X4D, 0,  //M
    0X65, 0,  //e
    0X6D, 0,  //m
    0X6F, 0,  //o
    0X72, 0,  //r
    0X79, 0   //y
};
#endif

INT8U u8OTGString00DescriptorEX[STRING_00_LENGTH];
#ifdef MANUFACTURER_STRING_SUPPORTED
static INT8U u8OTGString10DescriptorEX[STRING_10_LENGTH];
#endif
#ifdef PRODUCT_STRING_SUPPORTED
static INT8U u8OTGString20DescriptorEX[STRING_20_LENGTH];
#endif
#if 1 //bessel:don't need to support string descriptors except for language string
static INT8U u8OTGString30DescriptorEX[STRING_30_LENGTH];
static INT8U u8OTGString40DescriptorEX[STRING_40_LENGTH];
static INT8U u8OTGString50DescriptorEX[STRING_50_LENGTH];
//static INT8U u8OTGString60DescriptorEX[STRING_60_LENGTH];
//static INT8U u8OTGString70DescriptorEX[STRING_70_LENGTH];
//static INT8U u8OTGString80DescriptorEX[STRING_80_LENGTH];
#endif

#ifdef TEST
extern INT8U test_data[0x10000];
#endif

SetupPacket ControlOTGCmd;
INT8U   * pu8OTGDescriptorEX;

INT16U u16OTGTxRxCounter;
BOOLEAN bOTGEP0HaltSt;
CommandType eOTGCxCommand;
Action eOTGCxFinishAction;

//INT8U u8OTGMessageLevel;

//INT32U OTG_interrupt_level1;
//INT32U OTG_interrupt_level1_Save;
//INT32U OTG_interrupt_level1_Mask;

INT8U u8OTGConfigValue;
INT8U u8OTGInterfaceValue;
INT8U u8OTGInterfaceAlternateSetting;
INT32U u32SCSIRespLength;

BOOLEAN bOTGBufferEmpty;
#ifdef HS_SUPPORTED
BOOLEAN bOTGHighSpeed;
#endif
//BOOLEAN bOTGChirpFinish;

//INT8U u8OTGInterruptCount;
//INT32U u32OTGInterrupt_TX_COUNT ;
//INT32U u32OTGInterrupt_RX_COUNT ;
//INT8U u8OTGInterruptOutCount ;

#ifdef SHOW_DEBUG_MESSAGE
INT8U* u8CxOUTVendorTest = NULL;
INT8U* u8CxINVendorTest = NULL;
INT8U u8CxOUTVandorDataCount = 0;
INT8U u8CxINVandorDataCount = 0;
void vCxOUT_VendorTest(void);
void vCxOUT_VendorRxData(void);
void vCxIN_VendorTest(void);
void vCxIN_VendorTxData(void);
#endif
INT8U u8OTGDeviceDescriptorEX[DEVICE_LENGTH];

//void vFOTG200_Dev_Init(void);

static void vOTG_rst(void);
static void vOTG_suspend(void);
static void vOTG_resm(void);
static UINT8 bGet_OTGstatus(void);
static UINT8 bClear_OTGfeature(void);
static UINT8 bSet_OTGfeature(void);
static UINT8 bSet_OTGaddress(void);
static UINT8 bGet_OTGdescriptor(void);
static void vGet_OTGconfiguration(void);
static UINT8 bSet_OTGconfiguration(void);
static void vOTGCxFWr( INT8U *pu8Buffer, INT16U u16Num);
static void vOTGCxFRd( INT8U *pu8Buffer, INT16U u16Num);   
static UINT8 bOTGStandardCommand(void);
static UINT8 bOTGClassCommand(void);  
static void vOTG_ep0setup(void);
void vOTGEP0TxData(void);
static  BOOLEAN bSet_OTGdescriptor(void);
static void vOTGEP0RxData(void);
static void vOTGClrEPx(void);
static void vOTGClrEPxFIFOxReg(void);
static BOOLEAN bGet_OTGinterface(void);
static BOOLEAN bSet_OTGinterface(void);
static BOOLEAN bSynch_OTGframe(void);
static void vOTG_ep0tx(void);
static void vOTGEP0TxData(void);
static void vOTG_ep0rx(void);
static void vOTGEP0RxData(void);
void vOTG_ep0end(void);
static void vOTG_ep0fail(void);
static void vOTG_ep0abort(void);
static void vOTGFIFO_EPxCfg_FS(void);

extern CBW tOTGCBW;
extern void Waiting_For_DMA_Complete(INT8U FIFONum,INT8U source);

/// adding usb read/write function
void vUsbWrite(u8* buffer, u32 size);
void vUsbRead(u8* buffer, u32 size);

#include "dbg.h"
INT8U* Replace_SdRAM_Address(INT8U *pu8Buffer)
{
    UINT32 tmp;

    if(((uint32_t)pu8Buffer&(SdRAM_MEM_BASE)) == SdRAM_MEM_BASE){
		tmp = (((uint32_t)pu8Buffer)&(~0x10000000)) | 0x20000000;
		return (INT8U* )tmp;
    }
    //fLib_printf("not translating pu8Buffer %x\n",pu8Buffer);
    return pu8Buffer;
}

//=============================================================================
//      OTGP_HNP_Enable()
//      Description:
//
//      input: none
//      output: none
//=============================================================================
#if 0
int Debug_Check_R138(void)
{
    UINT32 u32Temp;

    u32Temp = mUsbIntSrc1MaskRd();

    if ((u32Temp&0x3C)!=0x3C)
    {
        fLib_printf("??? Check 0x138 error = 0x%x\n",u32Temp);
        while(1);
        //return (0);
    }
    else {
        return (1);
    }
}
#endif
//=============================================================================
//      OTGP_HNP_Enable()
//      Description:
//
//      input: none
//      output: none
//=============================================================================
#if 0/* for PHY testing. 2013/11/20 */
void OTGP_HNP_Enable(void)
{
    //<1>.Set b_Bus_Request
    mdwOTGC_Control_B_BUS_REQ_Set();

    //<2>.Set the HNP enable
    mdwOTGC_Control_B_HNP_EN_Set();
}
#endif
///////////////////////////////////////////////////////////////////////////////
//      vOTGIsr2()
//      Description:
//          1. Record FOTG200 Device group interrupt register.
//          2. Disable Usb interrupt.
//          3. Expect "Usb interrupt will be renabled after all events had been
//             serviced in main()".
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
#if 0
void vOTGIsr2(void)
{
    OTG_interrupt_level1_Save = mUsbIntGroupRegRd();
    OTG_interrupt_level1_Mask = mUsbIntGroupMaskRd();
    OTG_interrupt_level1 = OTG_interrupt_level1_Save & ~OTG_interrupt_level1_Mask;

    if (OTG_interrupt_level1 != 0)
    {
        //bruce;;04182005;;   fLib_DisableInt(IRQ_FOTG200);
    }
}
#endif
///////////////////////////////////////////////////////////////////////////////
//      vFOTG200_Dev_Init()
//      Description:
//          1. Reset all interrupt and clear all fifo data of FOTG200 Device
//          2. Turn on the "Global Interrupt Enable" bit of FOTG200 Device
//          3. Turn on the "Chip Enable" bit of FOTG200 Device
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
#if 0
void vFOTG200_Dev_Init(void)
{
    // suspend counter
    //mUsbIdleCnt(7);

    // Clear interrupt
    //mUsbIntBusRstClr();
    //mUsbIntSuspClr();
    //mUsbIntResmClr();

    // Disable all fifo interrupt
    //mUsbIntFIFO0_3OUTDis();
    //mUsbIntFIFO0_3INDis();
    //if ((mUsbIntSrc1MaskRd()&0xF00FF)!=0xF00FF)//Bruce;;07192005
    //    {fLib_printf("??? 0x138 Set Fail...\n");//Bruce;;07192005
    //     while(1);//Bruce;;07192005
    //    }//Bruce;;07192005

    // Soft Reset
    mUsbSoftRstSet();           // All circuit change to which state after Soft Reset?
    mUsbSoftRstClr();

    // Clear all fifo
    mUsbClrAllFIFOSet();            // will be cleared after one cycle.
    //mUsbTSTMODSet();

    // Enable usb200 global interrupt
    //mUsbGlobIntEnSet();
    //mUsbChipEnSet();
}
#endif
///////////////////////////////////////////////////////////////////////////////
//      vOTGInit()
//      Description:
//          1. Configure the FIFO and EPx map.
//          2. Initiate FOTG200 Device.
//          3. Set the usb interrupt source as edge-trigger.
//          4. Enable Usb interrupt.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTGInit(void)
{
    // init variables
    u16OTGTxRxCounter = 0;
    eOTGCxCommand = CMD_VOID;
    u8OTGConfigValue = 0;
    #ifdef HS_SUPPORTED
    u8OTGInterfaceValue = 0;
    #endif
    u8OTGInterfaceAlternateSetting = 0;
    bOTGEP0HaltSt = FALSE;

    eOTGCxFinishAction = ACT_IDLE;
    //OTG_interrupt_level1 = 0;
    // Initiate FUSB220
    //vFOTG200_Dev_Init();
    // suspend counter
    mUsbIdleCnt(7);
    // Clear interrupt
    //mUsbIntBusRstClr();
    //mUsbIntSuspClr();
    //mUsbIntResmClr();

    // Disable all fifo interrupt
    //mUsbIntFIFO0_3OUTDis();
    //mUsbIntFIFO0_3INDis();
    //if ((mUsbIntSrc1MaskRd()&0xF00FF)!=0xF00FF)//Bruce;;07192005
    //    {fLib_printf("??? 0x138 Set Fail...\n");//Bruce;;07192005
    //     while(1);//Bruce;;07192005
    //    }//Bruce;;07192005

    // Soft Reset
    //fLib_printf("softrst!!\n");
    mUsbSoftRstSet();           // All circuit change to which state after Soft Reset?
    //mUsbSoftRstClr();//Software reset will be self-cleared
    // Clear all fifo
    mUsbClrAllFIFOSet();            // will be cleared after one cycle.

    // Enable usb200 global interrupt
    //mUsbGlobIntEnSet();
    //mUsbChipEnSet();

    // Initiate Interrupt
//      fLib_CloseInt(IRQ_FOTG200);
//      fLib_SetIntTrig(IRQ_FOTG200,LEVEL,L_ACTIVE);
//      fLib_ConnectInt(IRQ_FOTG200, vOTGIsr2);
//      fLib_EnableInt(IRQ_FOTG200);
}

///////////////////////////////////////////////////////////////////////////////
//      vOTGHandler()
//      Description:
//          1. Service all Usb events
//          2. ReEnable Usb interrupt.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
void OTG_IRQHandler(void)
{
//   INT32U *u32pData;
    INT32U OTG_Interrupt_level2;
    INT32U temp;
//   INT32U OTG_Interrupt_Mask;
//   INT32U OTG_Interrupt_Origan;
//INT32U count;
//printf("FOTG210_IRQHandler\n");
	//NVIC_DisableIRQ(FOTG210_IRQ);  
   // if (bOTGDMARunning)
  //  {
  //      return;
  //  }
    temp=mUsbIntSrc2Rd();
    //printf("mUsbIntSrc2Rd temp %x\n",temp);
    OTG_Interrupt_level2 =temp&(~(mUsbIntSrc2MaskRd()));
    //printf("1OTG_Interrupt_level2 %x\n",OTG_Interrupt_level2);
    //  OTG_Interrupt_level2 = mUsbIntSrc2Rd()&(~(mUsbIntSrc2MaskRd()));// Interrupt source group 2(0x14C)
    if(OTG_Interrupt_level2 != 0)
    {
        if (OTG_Interrupt_level2 & BIT0)
            vOTG_rst();
        if (OTG_Interrupt_level2 & BIT1)
         	 vOTG_suspend();
        if (OTG_Interrupt_level2 & BIT2)
            vOTG_resm();
#if 0//kay
        if (OTG_Interrupt_level2 & BIT5)
        {
            mUsbIntTX0ByteClr();
            vOTG_TX0Byte();
        }
        if (OTG_Interrupt_level2 & BIT6)
        {
            mUsbIntRX0ByteClr();
            vOTG_RX0Byte();
        }
				
        if (OTG_Interrupt_level2 & BIT7)
        {

            mUsbIntDmaFinishClr();

            if(bOTGDMARunning)
                vOTGCheckDMA();
            else
                fLib_printf("L%02x:DMA finish Interrupt error.",u8LineOTGCount++);
        }

        if (OTG_Interrupt_level2 & BIT8)
        {
            fLib_printf("L%02x:DMA error.",u8LineOTGCount++);
            mUsbIntDmaErrClr();

            if(bOTGDMARunning)
            {
                vOTGCheckDMA();
                fLib_printf("L%02x:DMA error.",u8LineOTGCount++);
            }
            else
                fLib_printf("L%02x:DMA error Interrupt error.",u8LineOTGCount++);
        }
				
#endif
    }
    temp=mUsbIntSrc0Rd();
    //printf("mUsbIntSrc0 Rd temp %x\n",temp);	
    OTG_Interrupt_level2 = temp&(~(mUsbIntSrc0MaskRd()));// Interrupt source group 0(0x144)
    //printf("2OTG_Interrupt_level2 %x\n",OTG_Interrupt_level2);
    //OTG_Interrupt_level2 = mUsbIntSrc0Rd()&(~(mUsbIntSrc0MaskRd()));// Interrupt source group 0(0x144)
    if(OTG_Interrupt_level2 != 0)
    {

        if (OTG_Interrupt_level2 & BIT5)
            vOTG_ep0abort();
        if (OTG_Interrupt_level2 & BIT2)
        {  
            vOTG_ep0rx();
        }

        if (OTG_Interrupt_level2 & BIT0){
            vOTG_ep0setup();
        }
        //else if (OTG_Interrupt_level2 & BIT3)
        //  vOTG_ep0end();

        if (OTG_Interrupt_level2 & BIT1)
        {
            vOTG_ep0tx();
        }
        if (OTG_Interrupt_level2 & BIT4)
            vOTG_ep0fail();

    }
    if (eOTGCxFinishAction == ACT_STALL)
    {
        mUsbEP0StallSet();
    }
    else if (eOTGCxFinishAction == ACT_DONE)
        mUsbEP0DoneSet();

    // Clear Action
    eOTGCxFinishAction = ACT_IDLE;


    temp=mUsbIntSrc1Rd();
		
//  fLib_printf("mUsbIntSrc1Rd temp %x\n",temp);	
		
    OTG_Interrupt_level2 = temp&(~(mUsbIntSrc1MaskRd()));// Interrupt source group 0(0x144)
	
		
//   fLib_printf("&&&&3OTG_Interrupt_level2 %x\n",OTG_Interrupt_level2);

    //OTG_Interrupt_level2 = mUsbIntSrc1Rd()&(~(mUsbIntSrc1MaskRd()));// Interrupt source group 1(0x148)

    if(OTG_Interrupt_level2 != 0)
    {
        //count=mUsbFIFOOutByteCount(FIFO0);
      //  if(bOTGDMARunning == FALSE)
      //  {
#if(Bulk_Satus == Bulk_FIFO_SingleDir)
            // use FIFO2 for ep2( bulk out)
            if (OTG_Interrupt_level2 &((1<<(((EP2_START_FIFO)<<1)+1))|(1<<(((EP2_START_FIFO)<<1)+1)))){// short and full packet
                vUsb_Bulk_Out(mUsbFIFOOutByteCount(EP2_START_FIFO));
            	}
            //else if (OTG_Interrupt_level2 & (1<<((EP2_START_FIFO)<<1)))		// full packet
            //        vUsb_Bulk_Out(mUsbFIFOOutByteCount(EP2_START_FIFO));

            //use FIFO0 for ep1( bulk in)
            if (OTG_Interrupt_level2 & BIT16)
                vUsb_Bulk_In();

#elif(Bulk_Satus == Bulk_FIFO_BiDir)

            // use FIFO0 for ep2( bulk out)
            if (OTG_Interrupt_level2 & ((1<<((EP2_START_FIFO)+1))|(1<<(EP2_START_FIFO)))){ // short and full packet
                vUsb_Bulk_Out(mUsbFIFOOutByteCount(EP2_START_FIFO));
            	}

           // else if (OTG_Interrupt_level2 & (1<<(EP2_START_FIFO)))		// full packet
            //    vUsb_Bulk_Out(mUsbFIFOOutByteCount(EP2_START_FIFO));

            //  use FIFO0 for ep1( bulk in)
            if (OTG_Interrupt_level2 & BIT16)
                vUsb_Bulk_In();
#endif
       // }
	//NVIC_EnableIRQ(FOTG210_IRQ);	
    }
}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_ep0setup()
//      Description:
//          1. Read 8-byte setup packet.
//          2. Decode command as Standard, Class, Vendor or NOT support command
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTG_ep0setup(void)
{
   // INT8U c;
    u8 u8index;
    INT8U SetupCommand[8];
    INT32U *u32UsbCmd;
    INT8U *u8UsbCmd;
#ifdef  HS_SUPPORTED
    INT8U u8index;
#endif
    // First we must check if this is the first Cx 8 byte command after USB reset.
    // If this is the first Cx 8 byte command, we can check USB High/Full speed right now.

   // if(!bOTGChirpFinish)
   // {
        // first ep0 command after usb reset, means we can check usb speed right now.
    //    bOTGChirpFinish = TRUE;
#ifdef  HS_SUPPORTED
        if (mUsbOTGHighSpeedST())					// First we should judge HS or FS
        {
            bOTGHighSpeed = TRUE;
            // Device stays in High Speed
            // copy Device descriptors (from rom to sdram)
            //for (u8index = 0 ; u8index < sizeof(u8OTGHSDeviceDescriptor); u8index ++)
              //  u8OTGDeviceDescriptorEX[u8index] = u8OTGHSDeviceDescriptor[u8index];

            // copy Device Qualifierdescriptors (from rom to sdram)
            // bLength
            u8OTGDeviceQualifierDescriptorEX[0] = DEVICE_QUALIFIER_LENGTH;
            // bDescriptorType Device_Qualifier
            u8OTGDeviceQualifierDescriptorEX[1] = DT_DEVICE_QUALIFIER;
            for (u8index = 2 ; u8index < 8; u8index ++)
                u8OTGDeviceQualifierDescriptorEX[u8index] = u8OTGFSDeviceDescriptor[u8index];
            // Number of Other-speed Configurations
            u8OTGDeviceQualifierDescriptorEX[8] = u8OTGFSDeviceDescriptor[17];
            //Reserved for future use, must be zero
            u8OTGDeviceQualifierDescriptorEX[9] = 0x00;

            // copy Config descriptors (from rom to sdram)
            //for (u8index = 0 ; u8index < sizeof(u8HSConfigOTGDescriptor01); u8index ++)
                u8ConfigOTGDescriptorEX[u8index] = u8HSConfigOTGDescriptor01[u8index];

            // copy Other speed Config descriptors (from rom to sdram)
            for (u8index = 0 ; u8index < sizeof(u8FSConfigOTGDescriptor01); u8index ++)
                u8OtherSpeedConfigOTGDescriptorEX[u8index] = u8FSConfigOTGDescriptor01[u8index];
            // Change Descriptor type "DT_OTHER_SPEED_CONFIGURATION"
            u8OtherSpeedConfigOTGDescriptorEX[1] = DT_OTHER_SPEED_CONFIGURATION;
        }
        else
        {

            bOTGHighSpeed = FALSE;
      #endif 
            // Device stays in Full Speed
            // copy Device descriptors (from rom to sram)
           // for (u8index = 0 ; u8index < sizeof(u8OTGFSDeviceDescriptor); u8index ++)
            //    u8OTGDeviceDescriptorEX[u8index] = u8OTGFSDeviceDescriptor[u8index];

#ifdef  HS_SUPPORTED
            // copy Device Qualifierdescriptors (from rom to sram)
            // bLength
            u8OTGDeviceQualifierDescriptorEX[0] = DEVICE_QUALIFIER_LENGTH;
            // bDescriptorType Device_Qualifier
            u8OTGDeviceQualifierDescriptorEX[1] = DT_DEVICE_QUALIFIER;
            for (u8index = 2 ; u8index < 8; u8index ++)
                u8OTGDeviceQualifierDescriptorEX[u8index] = u8OTGHSDeviceDescriptor[u8index];
            // Number of Other-speed Configurations
            u8OTGDeviceQualifierDescriptorEX[8] = u8OTGHSDeviceDescriptor[17];
            //Reserved for future use, must be zero
            u8OTGDeviceQualifierDescriptorEX[9] = 0x00;
#endif

            // copy Config descriptors (from rom to sram)
          //  for (u8index = 0 ; u8index < sizeof(u8FSConfigOTGDescriptor01); u8index ++)
            //    u8ConfigOTGDescriptorEX[u8index] = u8FSConfigOTGDescriptor01[u8index];

#ifdef  HS_SUPPORTED
            // copy Other speed Config descriptors (from rom to sram)
            for (u8index = 0 ; u8index < sizeof(u8HSConfigOTGDescriptor01); u8index ++)
                u8OtherSpeedConfigOTGDescriptorEX[u8index] = u8HSConfigOTGDescriptor01[u8index];
            // Change Descriptor type "DT_OTHER_SPEED_CONFIGURATION"
            u8OtherSpeedConfigOTGDescriptorEX[1] = DT_OTHER_SPEED_CONFIGURATION;
#endif
#ifdef  HS_SUPPORTED
        }
#endif
        // copy String descriptors (from rom to sram)
       // for (u8index = 0 ; u8index < sizeof(u8OTGString00Descriptor); u8index ++)
         //   u8OTGString00DescriptorEX[u8index] = u8OTGString00Descriptor[u8index];

#ifdef MANUFACTURER_STRING_SUPPORTED
        for (u8index = 0 ; u8index < sizeof(u8OTGString10Descriptor); u8index ++)
            u8OTGString10DescriptorEX[u8index] = u8OTGString10Descriptor[u8index];
#endif
#ifdef PRODUCT_STRING_SUPPORTED
        for (u8index = 0 ; u8index < sizeof(u8OTGString20Descriptor); u8index ++)
            u8OTGString20DescriptorEX[u8index] = u8OTGString20Descriptor[u8index];
#endif
#if 0
        for (u8index = 0 ; u8index < sizeof(u8OTGString30Descriptor); u8index ++)
            u8OTGString30DescriptorEX[u8index] = u8OTGString30Descriptor[u8index];

        for (u8index = 0 ; u8index < sizeof(u8OTGString40Descriptor); u8index ++)
            u8OTGString40DescriptorEX[u8index] = u8OTGString40Descriptor[u8index];

        for (u8index = 0 ; u8index < sizeof(u8OTGString50Descriptor); u8index ++)
            u8OTGString50DescriptorEX[u8index] = u8OTGString50Descriptor[u8index];
#endif
   // }

    //u32UsbCmd = (INT32U*)malloc(8);
    // Read 8-byte setup packet from FIFO
#if 1//Read 8 byte setup command by PIO mode
    u32UsbCmd =(INT32U *)SetupCommand;
    mUsbDMA2FIFOSel(FOTG200_DMA2CxFIFO);
    u32UsbCmd[0] = mUsbEP0CmdDataRdDWord();
    u32UsbCmd[1] = mUsbEP0CmdDataRdDWord();
#else//Read 8 byte setup command by invoke DMA
    vOTGCxFRd((INT8U *)SetupCommand , 0x08);
    fLib_printf("EP0: %x %x %x %x %x %x %x %x\n",(INT8U *)SetupCommand[0],(INT8U *)SetupCommand[1], (INT8U *)SetupCommand[2],(INT8U *)SetupCommand[3],(INT8U *)SetupCommand[4],
		(INT8U *)SetupCommand[5],(INT8U *)SetupCommand[6],(INT8U *)SetupCommand[7]);
#endif
    //mUsbDMA2FIFOSel(FOTG200_DMA2FIFO_Non);
	u8UsbCmd = SetupCommand;
    //for (c = 0; c < 0x08; c ++)
    //  fLib_printf("%02x ",u8UsbCmd[c]);
    //fLib_printf("\n");

    //c = u8UsbCmd[0];                                                    // get 1st byte
    //ControlOTGCmd.Direction = (unsigned char)(u8UsbCmd[0] & 0x80);            // xfer Direction(IN, OUT) //No used
    ControlOTGCmd.Type =(u8UsbCmd[0] & 0x60);                 // type(Standard, Class, Vendor)
    ControlOTGCmd.Object = (u8UsbCmd[0] & 0x03);               // Device, Interface, Endpoint

    ControlOTGCmd.Request = u8UsbCmd[1];                            // get 2nd byte

    ControlOTGCmd.Value = u8UsbCmd[2];                              // get 3rd byte
    //c = u8UsbCmd[3];                                        // get 4th byte
    ControlOTGCmd.Value |= (u8UsbCmd[3]<<8);

    ControlOTGCmd.Index = u8UsbCmd[4];                      // get 5th byte
    //c = u8UsbCmd[5];                                        // get 6th byte
    ControlOTGCmd.Index |= ( u8UsbCmd[5]<<8);

    ControlOTGCmd.Length = u8UsbCmd[6];                     // get 7th byte
    //c = u8UsbCmd[7];                                        // get 8th byte
    ControlOTGCmd.Length |= (u8UsbCmd[7]<<8);
#if 0
    if(!(((u8UsbCmd[0] == 0x40)&&
            (u8UsbCmd[1] == 0x00)&&
            (u8UsbCmd[2] == 0x00)&&
            (u8UsbCmd[3] == 0x00)&&
            (u8UsbCmd[4] == 0x00)&&
            (u8UsbCmd[5] == 0x00))||
            ((u8UsbCmd[0] == 0xC0)&&
             (u8UsbCmd[1] == 0x00)&&
             (u8UsbCmd[2] == 0x00)&&
             (u8UsbCmd[3] == 0x00)&&
             (u8UsbCmd[4] == 0x00)&&
             (u8UsbCmd[5] == 0x00))))		// do not print test vendor command
    {
        if(u8OTGMessageLevel & MESS_INFO)
        {
            fLib_printf("L%x: EP0Cmd:", u8LineOTGCount ++);
            for (c = 0; c < 0x08; c ++)
                fLib_printf(" %02x", u8UsbCmd[c]);
            fLib_printf("\n");

        }
    }
#endif

    //  Command Decode
    switch(ControlOTGCmd.Type)
    {
    	case TYPE_STANDARD:
			if (bOTGStandardCommand() == FALSE)
				eOTGCxFinishAction = ACT_STALL;
    	break;
     	case TYPE_CLASS:
            if (bOTGClassCommand() == FALSE)// ClassCommand(); //bessel:to handle class commands from PC
                eOTGCxFinishAction = ACT_STALL;
    	break;   
#ifdef USB_VENDOR_COMMAND_SUPPORTED    	
     	case TYPE_VENDOR:
			// Vendor command test (for Cx OUT test)
			// If we OUT Cx data as below, FOTG200 Device will wait for .
			/*On Host20_AP.c
			//UINT8 HOST_AP_CONTROL_VENDOR_IN[] = {0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x01};
			//UINT8 HOST_AP_CONTROL_VENDOR_OUT[]= {0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x01};
			//Vendor command w/ different data size during data stage of control transfer
			//		  //This vendor command will be issued by Faraday's Host
			*/
			if((u8UsbCmd[0] == 0x40)&&
					(u8UsbCmd[1] == 0x00)&&
					(u8UsbCmd[2] == 0x00)&&
					(u8UsbCmd[3] == 0x00)&&
					(u8UsbCmd[4] == 0x00)&&
			(u8UsbCmd[5] == 0x00))
			{
				vCxOUT_VendorTest();//CONTROL_VENDOR_OUT
			}
			else if((u8UsbCmd[0] == 0xC0)&&
								(u8UsbCmd[1] == 0x00)&&
								(u8UsbCmd[2] == 0x00)&&
								(u8UsbCmd[3] == 0x00)&&
								(u8UsbCmd[4] == 0x00)&&
					(u8UsbCmd[5] == 0x00))
			{
				vCxIN_VendorTest();//CONTROL_VENDOR_IN
				vCxIN_VendorTxData();
			}
			else
			{
				eOTGCxFinishAction = ACT_STALL;
			}
    	break;
#endif
    	default:
			eOTGCxFinishAction = ACT_STALL;
    	break;
    }
#if 0    
    if (ControlOTGCmd.Type == 0)							// standard command
    {
        if (bOTGStandardCommand() == FALSE)
            eOTGCxFinishAction = ACT_STALL;
    }
    else if ((ControlOTGCmd.Type & 0x60) >> 5 == 1)		// class command
    {
            if (bOTGClassCommand() == FALSE)// ClassCommand(); //bessel:to handle class commands from PC
                eOTGCxFinishAction = ACT_STALL;
        }
#ifdef USB_VENDOR_COMMAND_SUPPORTED
    else if ((ControlOTGCmd.Type & 0x60) >> 5 == 2)		// vendor command
    {
                // Vendor command test (for Cx OUT test)
                // If we OUT Cx data as below, FOTG200 Device will wait for .
                /*On Host20_AP.c
                //UINT8 HOST_AP_CONTROL_VENDOR_IN[] = {0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x01};
                //UINT8 HOST_AP_CONTROL_VENDOR_OUT[]= {0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x01};
                //Vendor command w/ different data size during data stage of control transfer
                //        //This vendor command will be issued by Faraday's Host
                */
                if((u8UsbCmd[0] == 0x40)&&
                        (u8UsbCmd[1] == 0x00)&&
                        (u8UsbCmd[2] == 0x00)&&
                        (u8UsbCmd[3] == 0x00)&&
                        (u8UsbCmd[4] == 0x00)&&
                (u8UsbCmd[5] == 0x00))
        {
            vCxOUT_VendorTest();//CONTROL_VENDOR_OUT
        }
        else if((u8UsbCmd[0] == 0xC0)&&
                            (u8UsbCmd[1] == 0x00)&&
                            (u8UsbCmd[2] == 0x00)&&
                            (u8UsbCmd[3] == 0x00)&&
                            (u8UsbCmd[4] == 0x00)&&
                (u8UsbCmd[5] == 0x00))
        {
            vCxIN_VendorTest();//CONTROL_VENDOR_IN
            vCxIN_VendorTxData();
        }
        else
        {
            eOTGCxFinishAction = ACT_STALL;
        }

            }
#endif
    else
    {
        // Invalid(bad) command, Return EP0_STALL flag
        eOTGCxFinishAction = ACT_STALL;
    }
    //free(u32UsbCmd);
#endif
}


///////////////////////////////////////////////////////////////////////////////
//      vOTG_ep0tx()
//      Description:
//          1. Transmit data to EP0 FIFO.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTG_ep0tx(void)
{
    switch(eOTGCxCommand)
    {
    case CMD_GET_DESCRIPTOR:
        vOTGEP0TxData();
        break;
#ifdef USB_VENDOR_COMMAND_SUPPORTED
    case CMD_CxIN_Vendor:
        vCxIN_VendorTxData();
        break;
#endif
    default:
        mUsbEP0StallSet();
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_ep0rx()
//      Description:
//          1. Receive data from EP0 FIFO.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTG_ep0rx(void)
{
    switch(eOTGCxCommand)
    {
    case CMD_SET_DESCRIPTOR:
        vOTGEP0RxData();
        break;
#ifdef USB_VENDOR_COMMAND_SUPPORTED
    case CMD_CxOUT_Vendor:
        vCxOUT_VendorRxData();
        break;
#endif
    default:
    	printf("STALL!!!\n");
        mUsbEP0StallSet();
        break;
    }
}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_ep0end()
//      Description:
//          1. End this transfer.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vOTG_ep0end(void)
{
    eOTGCxCommand = CMD_VOID;
    mUsbEP0DoneSet();                               // Return EP0_Done flag
}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_ep0fail()
//      Description:
//          1. Stall this transfer.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTG_ep0fail(void)
{
    mUsbEP0StallSet();                              // Return EP0_Stall
}
///////////////////////////////////////////////////////////////////////////////
//      vOTG_ep0abort()
//      Description:
//          1. Stall this transfer.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTG_ep0abort(void)
{
    mUsbIntEP0AbortClr();                               // Clean EP0 abort
}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_rst()
//      Description:
//          1. Change descriptor table (High or Full speed).
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTG_rst(void)
{
    fLib_printf("Rset\n");
    mUsbDevAddrSet(0);
    // Init AP
    vOTG_APInit();

// start
#if (OTG_AP_Satus    == Bulk_AP)
#if(Bulk_Satus == Bulk_FIFO_SingleDir)
#if(EP2_START_FIFO == DOUBLE_BLK)
   mUsbIntF2OUTEn();
#else
    mUsbIntF1OUTEn();
#endif
#elif(Bulk_Satus == Bulk_FIFO_BiDir)
    mUsbIntF0OUTEn();
#endif
   mUsbIntF0INDis();
	 //  mUsbIntF0INEn();
#endif

    mUsbIntBusRstClr();
    mUsbClrAllFIFOSet();
   // bOTGChirpFinish = FALSE;
}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_suspend()
//      Description:
//          1. Clear suspend interrupt, and set suspend register.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTG_suspend(void)
{
    //if(u8OTGMessageLevel & MESS_INFO)
    //  fLib_printf("L%x: Bus suspend\n", u8LineOTGCount ++);
    // We have already set USB suepend counter in vFOTG200_Dev_Init().
    // Before enter into the suspend mode, we must finish all things about USB.
    // And then USB device into Suspend mode.
    //fLib_printf("S\n");
    mUsbIntSuspClr();
   // fLib_printf("S\n");
    //Reserve for OTG;;mUsbGoSuspend();
}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_resm()
//      Description:
//          1. Clear resume interrupt status and leave supend mode.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTG_resm(void)
{
    mUsbIntResmClr();
}


///////////////////////////////////////////////////////////////////////////////
//      bOTGStandardCommand()
//      Description:
//          1. Process standard Cx 8 bytes command.
//      input: none
//      output: TRUE or FALSE
///////////////////////////////////////////////////////////////////////////////

static UINT8 bOTGStandardCommand(void)
{

    switch (ControlOTGCmd.Request) // by Standard Request codes
    {
    case 0:     // get status
        return (bGet_OTGstatus());

    case 1:     // clear feature
        return (bClear_OTGfeature());

    case 2:     // Reserved for further use
        break;

    case 3:     // set feature
        return (bSet_OTGfeature());

    case 4:     // Reserved for further use
        break;

    case 5:     // set address
        //fLib_printf("A\n");
        if (!bOTGEP0HaltSt)
            return(bSet_OTGaddress());
        break;

    case 6:     // get descriptor
//              fLib_printf("bOTGEP0HaltSt=%d\n", bOTGEP0HaltSt);
        if (!bOTGEP0HaltSt)
            return(bGet_OTGdescriptor());
        break;
#if 1 //bessel:set descriptor is option
    case 7:     // set descriptor
        if (!bOTGEP0HaltSt)
            return(bSet_OTGdescriptor());
        break;
#endif

    case 8:     // get configuration
        if (!bOTGEP0HaltSt)
            vGet_OTGconfiguration();
        return TRUE;

    case 9:     // set configuration
        if (!bOTGEP0HaltSt)
            return(bSet_OTGconfiguration());
        break;
#if 1
    case 10:    // get interface
        if (!bOTGEP0HaltSt)
            return(bGet_OTGinterface());
        break;

    case 11:    // set interface
        if (!bOTGEP0HaltSt)
            return(bSet_OTGinterface());
        break;

    case 12:    // synch frame
        if (!bOTGEP0HaltSt)
            return(bSynch_OTGframe());
        break;
#endif
    default:
        break;
    }
    return FALSE;
}

///////////////////////////////////////////////////////////////////////////////
//      bGet_OTGstatus()
//      Description:
//          1. Send 2 bytes status to host.
//      input: none
//      output: TRUE or FALSE (BOOLEAN)
///////////////////////////////////////////////////////////////////////////////
static UINT8 bGet_OTGstatus(void)
{
    INT8U u8ep_n;
    INT8U u8fifo_n;
    BOOLEAN bdir;
    INT8U RecipientStatusLow, RecipientStatusHigh;
    INT8U u8Tmp[2];
    RecipientStatusLow = 0;
    RecipientStatusHigh = 0;
    switch (ControlOTGCmd.Object) // Judge which recipient type is at first
    {
    case 0:                 // Device
        // Return 2-byte's Device status (Bit1:Remote_Wakeup, Bit0:Self_Powered) to Host
        // Notice that the programe sequence of RecipientStatus
        RecipientStatusLow = (mUsbRmWkupST() == 1)? BIT1: 0; //(((INT8U)mUsbRmWkupST()) << 1);
        // Bit0: Self_Powered--> DescriptorTable[0x23], D6(Bit 6)
        #ifdef HS_SUPPORTED
		if(bOTGHighSpeed == TRUE)
			RecipientStatusLow |= ((u8HSConfigOTGDescriptor01[7] >> 6) & 0x01);
		else
        #endif
        	RecipientStatusLow |= ((u8FSConfigOTGDescriptor01[7] >> 6) & 0x01);
         break;
    case 1:                 // Interface
        // Return 2-byte ZEROs Interface status to Host
        break;

    case 2:                 // Endpoint
        if(ControlOTGCmd.Index == 0x00)
            RecipientStatusLow = (INT8U)bOTGEP0HaltSt;
        else
        {
            u8ep_n = ControlOTGCmd.Index & 0x7F;        // which ep will be clear
            bdir = ControlOTGCmd.Index >> 7;            // the direction of this ep
            if (u8ep_n > FOTG200_Periph_MAX_EP)         // over the Max. ep count ?
                return FALSE;
            else
            {
                u8fifo_n = mUsbEPMapRd(u8ep_n);     // get the relatived FIFO number
                if (bdir == 1)
                    u8fifo_n &= 0x0F;
                else
                    u8fifo_n >>= 4;
                if (u8fifo_n >= FOTG200_Periph_MAX_FIFO)    // over the Max. fifo count ?
                    return FALSE;

                // Check the FIFO had been enable ?
                if ((mUsbFIFOConfigRd(u8fifo_n) & FIFOEnBit) == 0)
                    return FALSE;
                if (bdir == 1)                      // IN direction ?
                    RecipientStatusLow = mUsbEPinStallST(u8ep_n);
                else
                    RecipientStatusLow = mUsbEPoutStallST(u8ep_n);
            }
        }
        break;
    default :
        return FALSE;
    }

    // return RecipientStatus;
    u8Tmp[0] = RecipientStatusLow;
    u8Tmp[1] = RecipientStatusHigh;
    vOTGCxFWr( u8Tmp, 2);

    eOTGCxFinishAction = ACT_DONE;
    return TRUE;
}



///////////////////////////////////////////////////////////////////////////////
//      bClear_OTGfeature()
//      Description:
//          1. Send 2 bytes status to host.
//      input: none
//      output: TRUE or FALSE (BOOLEAN)
///////////////////////////////////////////////////////////////////////////////
static UINT8 bClear_OTGfeature(void)
{
    INT32U u8ep_n;
    INT32U u8fifo_n;
    INT32U bdir;
    switch (ControlOTGCmd.Value)		// FeatureSelector
    {
    case 0:     // ENDPOINT_HALE
        // Clear "Endpoint_Halt", Turn off the "STALL" bit in Endpoint Control Function Register
        if(ControlOTGCmd.Index == 0x00)
            bOTGEP0HaltSt = FALSE;
        else
        {
            u8ep_n = ControlOTGCmd.Index & 0x7F;        // which ep will be clear
            bdir = ControlOTGCmd.Index >> 7;            // the direction of this ep
            if (u8ep_n > FOTG200_Periph_MAX_EP)         // over the Max. ep count ?
                return FALSE;
            else
            {
                u8fifo_n = mUsbEPMapRd(u8ep_n);     // get the relatived FIFO number
                if (bdir == 1)
                    u8fifo_n &= 0x0F;
                else
                    u8fifo_n >>= 4;
                if (u8fifo_n >= FOTG200_Periph_MAX_FIFO)    // over the Max. fifo count ?
                    return FALSE;

                // Check the FIFO had been enable ?
                if ((mUsbFIFOConfigRd(u8fifo_n) & FIFOEnBit) == 0)
                    return FALSE;
                if (bdir == 1)						// IN direction ?
                {
                    mUsbEPinRsTgSet(u8ep_n);        // Set Rst_Toggle Bit
                    mUsbEPinRsTgClr(u8ep_n);        // Clear Rst_Toggle Bit
                    mUsbEPinStallClr(u8ep_n);       // Clear Stall Bit
                }
                else
                {
                    mUsbEPoutRsTgSet(u8ep_n);       // Set Rst_Toggle Bit
                    mUsbEPoutRsTgClr(u8ep_n);       // Clear Rst_Toggle Bit
                    mUsbEPoutStallClr(u8ep_n);      // Clear Stall Bit
                }
            }
        }
        break;

    case 1 :        // Device Remote Wakeup
        // Clear "Device_Remote_Wakeup", Turn off the"RMWKUP" bit in Main Control Register
        mUsbRmWkupClr();
        break;

   // case 2 :        // Test Mode
    //    return FALSE;

    default :
        return FALSE;
    }
    eOTGCxFinishAction = ACT_DONE;
    return TRUE;
}



///////////////////////////////////////////////////////////////////////////////
//      bSet_OTGfeature()
//      Description:
//          1. Process Cx Set feature command.
//      input: none
//      output: TRUE or FALSE (BOOLEAN)
///////////////////////////////////////////////////////////////////////////////
static UINT8 bSet_OTGfeature(void)
{
    INT8U u8ep_n;
    INT8U u8fifo_n;
    BOOLEAN bdir;
#if 0/* for PHY testing. 2013/11/20 */
    INT8U i;
    INT8U u8Tmp[53];
    INT8U * pp;
#endif

    switch (ControlOTGCmd.Value)		// FeatureSelector
    {
    case 0: // ENDPOINT_HALE
        // Set "Endpoint_Halt", Turn on the "STALL" bit in Endpoint Control Function Register
        if(ControlOTGCmd.Index == 0x00)
            bOTGEP0HaltSt = TRUE;
        else
        {
            u8ep_n = ControlOTGCmd.Index & 0x7F;        // which ep will be clear
            bdir = ControlOTGCmd.Index >> 7;            // the direction of this ep
            if (u8ep_n > FOTG200_Periph_MAX_EP)         // over the Max. ep count ?
                return FALSE;
            else
            {
                u8fifo_n = mUsbEPMapRd(u8ep_n);     // get the relatived FIFO number
                if (bdir == 1)
                    u8fifo_n &= 0x0F;
                else
                    u8fifo_n >>= 4;
                if (u8fifo_n >= FOTG200_Periph_MAX_FIFO)    // over the Max. fifo count ?
                    return FALSE;

                // Check the FIFO had been enable ?
                if ((mUsbFIFOConfigRd(u8fifo_n) & FIFOEnBit) == 0)
                    return FALSE;
                if (bdir == 1)                      // IN direction ?
                    mUsbEPinStallSet(u8ep_n);       // Clear Stall Bit
                else
                    mUsbEPoutStallSet(u8ep_n);      // Set Stall Bit
            }
        }
        break;
#if 0/* for PHY testing. 2013/11/20 */
        case 1 :        // Device Remote Wakeup
            // Set "Device_Remote_Wakeup", Turn on the"RMWKUP" bit in Mode Register
            mUsbRmWkupSet();
            eOTGCxFinishAction = ACT_DONE;
            break;

        case 2 :        // Test Mode
            switch ((ControlOTGCmd.Index >> 8)) // TestSelector
            {
                case 0x1:   // Test_J
                    mUsbTsMdWr(TEST_J);
                    eOTGCxFinishAction = ACT_DONE;
                    break;

                case 0x2:   // Test_K
                    mUsbTsMdWr(TEST_K);
                    eOTGCxFinishAction = ACT_DONE;
                    break;

                case 0x3:   // TEST_SE0_NAK
                    mUsbTsMdWr(TEST_SE0_NAK);
                    eOTGCxFinishAction = ACT_DONE;
                    break;

                case 0x4:   // Test_Packet
                    mUsbTsMdWr(TEST_PKY);
                    mUsbEP0DoneSet();           // special case: follow the test sequence
                    //////////////////////////////////////////////
                    // Jay ask to modify, 91-6-5 (Begin)        //
                    //////////////////////////////////////////////
                    pp = u8Tmp;
                    for (i = 0; i < 9; i++)//JKJKJKJK x 9//
                       *pp++ = 0x00;

                    for (i = 0; i < 8; i++) // 8*AA //
                       *pp++ = 0xAA;

                    for (i = 0; i < 8; i++) // 8*EE //
                       *pp++ = 0xEE;

                    *pp++ = 0xFE;

                    for (i = 0; i < 11; i++) // 11*FF //
                       *pp++ = 0xFF;

                    *pp++ = 0x7F;
                    *pp++ = 0xBF;
                    *pp++ = 0xDF;
                    *pp++ = 0xEF;
                    *pp++ = 0xF7;
                    *pp++ = 0xFB;
                    *pp++ = 0xFD;
                    *pp++ = 0xFC;
                    *pp++ = 0x7E;
                    *pp++ = 0xBF;
                    *pp++ = 0xDF;
                    *pp++ = 0xEF;
                    *pp++ = 0xF7;
                    *pp++ = 0xFB;
                    *pp++ = 0xFD;
                    *pp++ = 0x7E;
                    vOTGCxFWr( u8Tmp,53);
                    //////////////////////////////////////////////
                    // Jay ask to modify, 91-6-5 (End)          //
                    //////////////////////////////////////////////

                    // Turn on "r_test_packet_done" bit(flag) (Bit 5)
                    mUsbTsPkDoneSet();
                    break;

                case 0x5:   // Test_Force_Enable
                    //FUSBPort[0x08] = 0x20;    //Start Test_Force_Enable
                    break;

                default:
                    return FALSE;
            }
                break;

        case 3 :        //For OTG => b_hnp_enable

              OTGP_HNP_Enable();
              eOTGCxFinishAction = ACT_DONE;

            break;

        case 4 :        //For OTG => b_hnp_enable


              eOTGCxFinishAction = ACT_DONE;

            break;
        case 5 :        //For OTG => b_hnp_enable

             fLib_printf(">>> Please Connect to an alternate port on the A-device for HNP...\n");
              eOTGCxFinishAction = ACT_DONE;

            break;
#endif

    default :
        return FALSE;
    }
    return TRUE;
}



///////////////////////////////////////////////////////////////////////////////
//      bSet_OTGaddress()
//      Description:
//          1. Set USB bus addr to FOTG200 Device register.
//      input: none
//      output: TRUE or FALSE (BOOLEAN)
///////////////////////////////////////////////////////////////////////////////
static UINT8 bSet_OTGaddress(void)
{
    if (ControlOTGCmd.Value >= 0x0100)
        return FALSE;
    else
    {
        mUsbDevAddrSet(ControlOTGCmd.Value);
        eOTGCxFinishAction = ACT_DONE;
        //fLib_printf("Addr\n");
        return TRUE;
    }
}


///////////////////////////////////////////////////////////////////////////////
//      bGet_OTGdescriptor()
//      Description:
//          1. Point to the start location of the correct descriptor.
//          2. set the transfer length and return descriptor information back to host
//      input: none
//      output: TRUE or FALSE (BOOLEAN)
///////////////////////////////////////////////////////////////////////////////
static UINT8 bGet_OTGdescriptor(void)
{
//    DSG("(INT8U)(ControlOTGCmd.Value >> 8)=%d", (INT8U)(ControlOTGCmd.Value >> 8));
    //////////////// Decide the display descriptor length(range) //////////////
    switch ((INT8U)(ControlOTGCmd.Value >> 8))
    {
    case 1:                 // device descriptor
    	#ifdef HS_SUPPORTED
    	if(bOTGHighSpeed == TRUE)
    	     pu8OTGDescriptorEX = &u8OTGHSDeviceDescriptor[0];
        else
        #endif
			pu8OTGDescriptorEX = &u8OTGFSDeviceDescriptor[0];
        u16OTGTxRxCounter=DEVICE_LENGTH;
       // fLib_printf("Get_Dev\n");
        break;

    case 2:                 // configuration descriptor
        // It includes Configuration, Interface and Endpoint Table
       // fLib_printf("Get_Conf\n");
        switch ((INT8U)ControlOTGCmd.Value)
        {
        case 0x00:      // configuration no: 0
        #ifdef HS_SUPPORTED
		if(bOTGHighSpeed == TRUE)
		{
			 pu8OTGDescriptorEX = &u8HSConfigOTGDescriptor01[0];
			 u16OTGTxRxCounter = u8HSConfigOTGDescriptor01[2] + (u8HSConfigOTGDescriptor01[3] << 8);
		}
		else
       #endif
       {
			pu8OTGDescriptorEX = &u8FSConfigOTGDescriptor01[0];
	  		u16OTGTxRxCounter = u8FSConfigOTGDescriptor01[2] + (u8FSConfigOTGDescriptor01[3] << 8);
	  	}
        break;
        default:
            return FALSE;
        }
        break;

    case 3:                 // string descriptor
        // DescriptorIndex = low_byte of wValue
        switch ((INT8U)ControlOTGCmd.Value)
        {
        case 0x00:
            //pu8OTGDescriptorEX = &u8OTGString00DescriptorEX[0];
            //u16OTGTxRxCounter = u8OTGString00DescriptorEX[0];
            pu8OTGDescriptorEX = &u8OTGString00Descriptor[0];
            u16OTGTxRxCounter = u8OTGString00Descriptor[0];
            break;
#ifdef MANUFACTURER_STRING_SUPPORTED
        case 0x10:
            pu8OTGDescriptorEX = &u8OTGString10DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString10DescriptorEX[0];
            break;
#endif
        
#ifdef PRODUCT_STRING_SUPPORTED
        case 0x20:
            pu8OTGDescriptorEX = &u8OTGString20DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString20DescriptorEX[0];
            break;
#endif
        
#if 0
        case 0x30:
            pu8OTGDescriptorEX = &u8OTGString30DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString30DescriptorEX[0];
            break;

        case 0x40:
            pu8OTGDescriptorEX = &u8OTGString40DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString40DescriptorEX[0];
            break;

        case 0x50:
            pu8OTGDescriptorEX = &u8OTGString50DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString50DescriptorEX[0];
            break;
#endif
        default:
            return FALSE;
        }
        break;

    case 4:                     // interface descriptor
        // It cannot be accessed individually, it must follow "Configuraton"
        break;

    case 5:                     // endpoint descriptor
        // It cannot be accessed individually, it must follow "Configuraton"
        break;

    case 6:                     // Device_Qualifier descritor
#ifdef HS_SUPPORTED
        if (mUsbOTGHighSpeedST())
        {
            pu8OTGDescriptorEX = &u8OTGDeviceQualifierDescriptorEX[0];
            u16OTGTxRxCounter = u8OTGDeviceQualifierDescriptorEX[0];
        }
        else
#endif
            return FALSE;

//        break;

    case 7:                     // Other_Speed_Configuration
        // It includes Configuration, Interface and Endpoint Table
#ifdef HS_SUPPORTED
        pu8OTGDescriptorEX = &u8OtherSpeedConfigOTGDescriptorEX[0];
        u16OTGTxRxCounter = u8OtherSpeedConfigOTGDescriptorEX[2] +
                            (u8OtherSpeedConfigOTGDescriptorEX[3] << 8);
#else
        return FALSE;
#endif
//        break;

    default:
        return FALSE;
    }

    if (u16OTGTxRxCounter > ControlOTGCmd.Length)
        u16OTGTxRxCounter = ControlOTGCmd.Length;


    //if (u16OTGTxRxCounter==0)//Bruce;;Add;;Test
    //   {mUsbEP0DoneSet();//Bruce;;Add;;Test
    //    return TRUE;//Bruce;;Add;;Test
    //   }//Bruce;;Add;;Test


    eOTGCxCommand = CMD_GET_DESCRIPTOR;
    vOTGEP0TxData();
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
//      bSet_OTGdescriptor()
//      Description:
//          1. Point to the start location of the correct descriptor.
//          2. Set the transfer length, and we will save data into sdram when Rx interrupt occure
//      input: none
//      output: TRUE or FALSE (BOOLEAN)
///////////////////////////////////////////////////////////////////////////////
#if 1
BOOLEAN bSet_OTGdescriptor(void)
{

    switch ((INT8U)(ControlOTGCmd.Value >> 8))
    {
    case 1:                 // device descriptor
        pu8OTGDescriptorEX = &u8OTGDeviceDescriptorEX[0];
        u16OTGTxRxCounter = u8OTGDeviceDescriptorEX[0];
        break;

    case 2:	 				// configuration descriptor
        // It includes Configuration, Interface and Endpoint Table
        // DescriptorIndex = low_byte of wValue
        switch ((INT8U)ControlOTGCmd.Value)
        {
        case 0x00:		// configuration no: 0
            pu8OTGDescriptorEX = &u8ConfigOTGDescriptorEX[0];
            u16OTGTxRxCounter = u8ConfigOTGDescriptorEX[0];
            break;
        default:
            return FALSE;
        }
        break;


    case 3:   				// string descriptor
        // DescriptorIndex = low_byte of wValue
        switch ((INT8U)ControlOTGCmd.Value)
        {
        case 0x00:
            pu8OTGDescriptorEX = &u8OTGString00DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString00DescriptorEX[0];
            break;

        case 0x10:
            pu8OTGDescriptorEX = &u8OTGString10DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString10DescriptorEX[0];
            break;

        case 0x20:
            pu8OTGDescriptorEX = &u8OTGString20DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString20DescriptorEX[0];
            break;

        case 0x30:
            pu8OTGDescriptorEX = &u8OTGString30DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString30DescriptorEX[0];
            break;

        case 0x40:
            pu8OTGDescriptorEX = &u8OTGString40DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString40DescriptorEX[0];
            break;

        case 0x50:
            pu8OTGDescriptorEX = &u8OTGString50DescriptorEX[0];
            u16OTGTxRxCounter = u8OTGString50DescriptorEX[0];
            break;

        default:
            return FALSE;
        }
        break;
    default:
        return FALSE;
    }

    if (u16OTGTxRxCounter > ControlOTGCmd.Length)
        u16OTGTxRxCounter = ControlOTGCmd.Length;

    eOTGCxCommand = CMD_SET_DESCRIPTOR;
    return TRUE;
}
#endif

///////////////////////////////////////////////////////////////////////////////
//      vGet_OTGconfiguration()
//      Description:
//          1. Send 1 bytes configuration value to host.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vGet_OTGconfiguration(void)
{
   // INT8U u8Tmp[2];
   // u8Tmp[0] = u8OTGConfigValue;
   // vOTGCxFWr( u8Tmp, 1);
   vOTGCxFWr(&u8OTGConfigValue, 1);
    eOTGCxFinishAction = ACT_DONE;
}


///////////////////////////////////////////////////////////////////////////////
//      bSet_OTGconfiguration()
//      Description:
//          1. Get 1 bytes configuration value from host.
//          2-1. if(value == 0) then device return to address state
//          2-2. if(value match descriptor table)
//                  then config success & Clear all EP toggle bit
//          2-3  else stall this command
//      input: none
//      output: TRUE or FALSE
///////////////////////////////////////////////////////////////////////////////
static UINT8 bSet_OTGconfiguration(void)
{
	UINT8 configured_value=ControlOTGCmd.Value&0xFF;

    //if ((ControlOTGCmd.Value&0xFF) == 0)
    if(configured_value == 0)
    {
        u8OTGConfigValue = 0;
        mUsbCfgClr();
    }
    else
    {
#ifdef HS_SUPPORTED
        if (mUsbOTGHighSpeedST())					// First judge HS or FS??
        {
           // if ((INT8U)ControlOTGCmd.Value > HS_CONFIGURATION_NUMBER)
           if ( configured_value > HS_CONFIGURATION_NUMBER)
                return FALSE;
            u8OTGConfigValue = configured_value;//(INT8U)ControlOTGCmd.Value;
            vOTGClrEPxFIFOxReg();
            vOTGFIFO_EPxCfg_HS();
            mUsbSOFMaskHS();
        }
        else
        {
#endif
          //  if ((INT8U)ControlOTGCmd.Value > FS_CONFIGURATION_NUMBER)
          if (configured_value > FS_CONFIGURATION_NUMBER)
                return FALSE;
            u8OTGConfigValue = configured_value;//(INT8U)ControlOTGCmd.Value;
            vOTGFIFO_EPxCfg_FS();
            mUsbSOFMaskFS();
#ifdef HS_SUPPORTED
        }
#endif
        mUsbCfgSet();
        vOTGClrEPx();
    }

   // if ((INT8U)ControlOTGCmd.Value == 1)			// Card Reader App.
    if (configured_value == 1 )
    {
#if(Bulk_Satus == Bulk_FIFO_SingleDir)
#if(EP2_START_FIFO == DOUBLE_BLK)
        mUsbIntF2OUTEn();
#else
        mUsbIntF1OUTEn();
#endif
#elif(Bulk_Satus == Bulk_FIFO_BiDir)
        mUsbIntF0OUTEn();
#endif
        mUsbIntF0INDis();
		//	mUsbIntEP0InEn();
    }
    eOTGCxFinishAction = ACT_DONE;
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////
//      bGet_OTGinterface()
//      Description:
//          Getting interface
//      input: none
//      output: TRUE or FALSE
///////////////////////////////////////////////////////////////////////////////
#if 1
BOOLEAN bGet_OTGinterface(void)
{
    INT8U u8Tmp[2];
    if (mUsbCfgST() == 0)
        return FALSE;

    // If there exists many interfaces, Interface0,1,2,...N,
    // You must check & select the specific one
//	fLib_printf("u8OTGConfigValue %d \n\r", u8OTGConfigValue);
    switch (u8OTGConfigValue)
    {
#if (HS_CONFIGURATION_NUMBER >= 1)
        // Configuration 1
    case 1:
        if (ControlOTGCmd.Index > HS_C1_INTERFACE_NUMBER)
            return FALSE;
        break;
#endif
#if (HS_CONFIGURATION_NUMBER >= 2)
        // Configuration 2
    case 2:
        if (ControlOTGCmd.Index > HS_C2_INTERFACE_NUMBER)
            return FALSE;
        break;
#endif
 //   default:
       // return FALSE;
    }
//		fLib_printf("u8OTGInterfaceAlternateSetting %d \n\r", u8OTGInterfaceAlternateSetting);
    u8Tmp[0] = u8OTGInterfaceAlternateSetting;
    vOTGCxFWr( u8Tmp, 1);

    eOTGCxFinishAction = ACT_DONE;
    return TRUE;
}
#endif
///////////////////////////////////////////////////////////////////////////////
//      bSet_OTGinterface()
//      Description:
//          1-1. If (the device stays in Configured state)
//                  &(command match the alternate setting)
//                      then change the interface
//          1-2. else stall it
//      input: none
//      output: TRUE or FALSE
///////////////////////////////////////////////////////////////////////////////
#if 1
BOOLEAN bSet_OTGinterface(void)
{
    if (mUsbCfgST() )
    {
//		if(u8OTGMessageLevel & MESS_INFO)
//		{
//	   		fLib_printf("L%x: EP0Cmd:", u8LineOTGCount ++);
//		}
        // If there exists many interfaces, Interface0,1,2,...N,
        // You must check & select the specific one
        switch (ControlOTGCmd.Index)
        {
        case 0:	// Interface0
            if((INT8U)ControlOTGCmd.Value == u8ConfigOTGDescriptorEX[12])
            {
                u8OTGInterfaceValue = (INT8U)ControlOTGCmd.Index;
                u8OTGInterfaceAlternateSetting = (INT8U)ControlOTGCmd.Value;
                vOTGClrEPxFIFOxReg();
#ifdef HS_SUPPORTED
                if (mUsbOTGHighSpeedST())                   // First judge HS or FS??
                    vOTGFIFO_EPxCfg_HS();
                else
#endif
					
                    vOTGFIFO_EPxCfg_FS();
                vOTGClrEPx();
                eOTGCxFinishAction = ACT_DONE;
                return TRUE;
            }
        case 1: // Interface1
        case 2: // Interface2
        default:
            break;
        }
    }
    return FALSE;

}
#endif
///////////////////////////////////////////////////////////////////////////////
//    bOTGClassCommand()
//    Description:
//       1. Process class command.
//    input: none
//    output: TRUE or FALSE
///////////////////////////////////////////////////////////////////////////////
static UINT8 bOTGClassCommand(void)//bessel:add
{
    unsigned char u8Tmp=0x00;
    switch (ControlOTGCmd.Request) // by Class Request codes
    {
    case 0xFE:// Get Max LUN feature
        vOTGCxFWr( &u8Tmp, 1);
        eOTGCxFinishAction = ACT_DONE;
        break;
#if 0
    case 0xFF:     // reset
        vOTG_APInit;
        vUsbScsiInit();
        eUsbCxAction = ACT_DONE;
        break;
#endif
    default:
        return FALSE;
    }
    return TRUE;
}
///////////////////////////////////////////////////////////////////////////////
//      bSynch_OTGframe()
//      Description:
//          1. If the EP is a Iso EP, then return the 2 bytes Frame number.
//               else stall this command
//      input: none
//      output: TRUE or FALSE
///////////////////////////////////////////////////////////////////////////////
#if 1
BOOLEAN bSynch_OTGframe(void)
{
    INT8S TransferType;
    INT16U u16Tmp;

//  TransferType = -1;
    // Does the Endpoint support Isochronous transfer type?
    switch(ControlOTGCmd.Index)
    {
    case 1:     // EP1
        TransferType = u8ConfigOTGDescriptorEX[22] & 0x03;
        break;
#if 0
    case 2:     // EP2
        TransferType = u8ConfigOTGDescriptorEX[29] & 0x03;
        break;

    case 3:     // EP3
        TransferType = u8ConfigOTGDescriptorEX[36] & 0x03;
        break;

    case 4:     // EP4
        TransferType = u8ConfigOTGDescriptorEX[43] & 0x03;
        break;

    case 5:     // EP5
        TransferType = u8ConfigOTGDescriptorEX[50] & 0x03;
        break;
#endif
    default:
        break;
    }

    if (TransferType == 1)	// Isochronous
    {
        u16Tmp = mUsbFrameNo();
        vOTGCxFWr( (INT8U *)&u16Tmp, 2);

        eOTGCxFinishAction = ACT_DONE;
        return TRUE;
    }
    else
        return FALSE;
}
#endif

///////////////////////////////////////////////////////////////////////////////
//      vOTGEP0TxData()
//      Description:
//          1. Send data(max or short packet) to host.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vOTGEP0TxData(void)
{
    INT8U u8temp;

    if(u16OTGTxRxCounter < EP0MAXPACKETSIZE)
        u8temp = (INT8U)u16OTGTxRxCounter;
    else
        u8temp = EP0MAXPACKETSIZE;

    u16OTGTxRxCounter -= (INT16U)u8temp;

    // Transmit u8Temp bytes data
    vOTGCxFWr( pu8OTGDescriptorEX, u8temp);
    pu8OTGDescriptorEX = pu8OTGDescriptorEX + u8temp;

    eOTGCxFinishAction = ACT_DONE;
    // end of the data stage
    if (u16OTGTxRxCounter == 0)
    {
        eOTGCxCommand = CMD_VOID;
    }
}

///////////////////////////////////////////////////////////////////////////////
//      vOTGEP0RxData()
//      Description:
//          1. Receive data(max or short packet) from host.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTGEP0RxData(void)
{
    INT8U u8temp;

    if(u16OTGTxRxCounter < EP0MAXPACKETSIZE)
        u8temp = (INT8U)u16OTGTxRxCounter;
    else
        u8temp = EP0MAXPACKETSIZE;

    u16OTGTxRxCounter -= (INT16U)u8temp;
    vOTGCxFRd(pu8OTGDescriptorEX , u8temp);
    pu8OTGDescriptorEX = pu8OTGDescriptorEX + u8temp;

    // end of the data stage
    if (u16OTGTxRxCounter == 0)
    {
        eOTGCxCommand = CMD_VOID;
        eOTGCxFinishAction = ACT_DONE;
    }
}

///////////////////////////////////////////////////////////////////////////////
//      vOTGClrEPx()
//      Description:
//          1. Clear all endpoint Toggle Bit
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTGClrEPx(void)
{
    INT8U u8ep;

    // Clear All EPx Toggle Bit
    for (u8ep = 1; u8ep <= FOTG200_Periph_MAX_EP; u8ep ++)
    {
        mUsbEPinRsTgSet(u8ep);
        mUsbEPinRsTgClr(u8ep);
    }
    for (u8ep = 1; u8ep <= FOTG200_Periph_MAX_EP; u8ep ++)
    {
        mUsbEPoutRsTgSet(u8ep);
        mUsbEPoutRsTgClr(u8ep);
    }
}

///////////////////////////////////////////////////////////////////////////////
//      vOTGClrEPxFIFOxReg()
//      Description:
//          1. Clear all endpoint & FIFO register setting
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vOTGClrEPxFIFOxReg(void)
{
    INT8U u8ep;
    for (u8ep = 1; u8ep <= FOTG200_Periph_MAX_EP; u8ep ++)
    {
        mUsbEPMxPtSzClr(u8ep,DIRECTION_IN);
        mUsbEPMxPtSzClr(u8ep,DIRECTION_OUT);
    }
    mUsbEPMapAllClr();
    mUsbFIFOMapAllClr();
    mUsbFIFOConfigAllClr();
}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_ISO_SeqErr()
//      Description:
//          1. FOTG200 Device Detects High bandwidth isochronous Data PID sequential error.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
#if 0
void vOTG_ISO_SeqErr(void)
{
    INT8U u8Tmp = mUsbIntIsoSeqErrRd();
    INT8U i;
    mUsbIntIsoSeqErrSetClr(u8Tmp);

    for(i = 1; i < 8; i ++)
    {
        if(u8Tmp & (BIT0 << i))
        {
            if(u8OTGMessageLevel & MESS_INFO)
            {
                fLib_printf("L%x: EP%x Isochronous Sequential Error\n", u8LineOTGCount ++, i);
            }
        }
    }

}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_ISO_SeqAbort()
//      Description:
//          1. FOTG200 Device Detects High bandwidth isochronous Data PID sequential abort.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vOTG_ISO_SeqAbort(void)
{
    INT8U u8Tmp = mUsbIntIsoSeqAbortRd();
    INT8U i;
    mUsbIntIsoSeqAbortSetClr(u8Tmp);

    for(i = 1; i < 8; i ++)
    {
        if(u8Tmp & (BIT0 << i))
        {
            if(u8OTGMessageLevel & MESS_INFO)
            {
                fLib_printf("L%x: EP%x Isochronous Sequential Abort\n", u8LineOTGCount ++, i);
            }
        }
    }

}
#endif
///////////////////////////////////////////////////////////////////////////////
//      vOTG_TX0Byte()
//      Description:
//          1. Send 0 byte data to host.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vOTG_TX0Byte(void)
{
    INT8U u8Tmp = mUsbIntTX0ByteRd();
    INT8U i;
    mUsbIntTX0ByteSetClr(u8Tmp);

    for(i = 1; i < 8; i ++)
    {
        if(u8Tmp & (BIT0 << i))
        {
          //  if(u8OTGMessageLevel & MESS_INFO)
            {
            #ifdef SHOW_DEBUG_MESSAGE
			   	fLib_printf("EP%x IN data 0 byte to host\n", i);
			#endif
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//      vOTG_RX0Byte()
//      Description:
//          1. Receive 0 byte data from host.
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vOTG_RX0Byte(void)
{
    INT8U u8Tmp = mUsbIntRX0ByteRd();
    INT8U i;
    mUsbIntRX0ByteSetClr(u8Tmp);

    for(i = 1; i < 8; i ++)
    {
        if(u8Tmp & (BIT0 << i))
        {
           // if(u8OTGMessageLevel & MESS_ERROR)
            {
	            #ifdef SHOW_DEBUG_MESSAGE
               		 fLib_printf("EP%x OUT data 0 byte to Device\n", i);
                #endif
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
//      vOTGCheckDMA()
//      Description: Check OTG DMA finish or error
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
#if 1
void vOTGCheckDMA(void)
{

    // UINT8 *pData;

#if 0
    if(eUsbOTGMassStorageState == STATE_CB_DATA_IN)
        mUsbFIFODone(FIFO0);
#endif


    bOTGDMARunning = FALSE;
    tOTGCSW.u32DataResidue -= u32SCSIRespLength;//tOTGCSW.u32DataResidue = 0;   //bessel:
    u32SCSIRespLength = 0;//bessel:
#if 0
#if(Bulk_Satus == Bulk_FIFO_SingleDir)
    mUsbIntF2OUTEn();
#elif(Bulk_Satus == Bulk_FIFO_BiDir)
    mUsbIntF0OUTEn();
#endif
    mUsbIntF0INEn();

    mUsbIntEP0SetupEn();
    mUsbIntEP0InEn();
    mUsbIntEP0OutEn();
    mUsbIntEP0EndEn();
#endif
    //mUsbDMA2FIFOSel(FOTG200_DMA2FIFO_Non);
//  Do_Delay(100);
}
#endif
///////////////////////////////////////////////////////////////////////////////
//      vOTGDxFWr()
//      Description: Write the buffer content to USB220 Data FIFO
//      input: Buffer pointer, byte number to write, Change Endian type or NOT
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vOTGDxFWr(INT8U FIFONum, INT8U   * pu8Buffer, INT16U u16Num)
{
    //INT32U wTemp;
    /*
    INT32U FIFO_Sel;

    if (u16Num==0)//Bruce;;03152005
       return;


    if(FIFONum<4)
        FIFO_Sel = 1<<FIFONum;
    else
    {
        fLib_printf("L%x: Set to Wrong FIFO,FIFO%d not support..", u8LineOTGCount ++,FIFONum);
        return;
    }
    */
    mUsbDmaConfig(u16Num,DIRECTION_IN);
    //mUsbDMA2FIFOSel(FIFO_Sel);
    mUsbDMA2FIFOSel(1<<FIFONum);
//    mUsbDmaAddr(((INT32U)pu8Buffer));
    mUsbDmaAddr((UINT32)Replace_SdRAM_Address(pu8Buffer));

    mUsbDmaStart();

#if 0
    while(1)
    {
        wTemp = mUsbIntSrc2Rd();
        if(wTemp & BIT8)
        {
            mUsbFIFOClr((INT32U)FIFONum);

            mUsbIntDmaErrClr();
            fLib_printf("L%x: FIFO%d IN transfer DMA error..", u8LineOTGCount ++,FIFONum);
            break;
        }
        if(wTemp & BIT7)
        {
            mUsbIntDmaFinishClr();
            break;
        }
#if 0
        if((wTemp & 0x00000007)>0)//If (Resume/Suspend/Reset) exit
        {
            mUsbDMARst();
            mUsbFIFOClr((INT32U)FIFONum);
            mUsbIntDmaFinishClr();
            fLib_printf("L%x: FIFO%d IN transfer DMA stop because USB Resume/Suspend/Reset..", u8LineOTGCount ++,FIFONum);
            break;
        }
#endif
    }
    mUsbDMA2FIFOSel(FOTG200_DMA2FIFO_Non);
#else
    Waiting_For_DMA_Complete(FIFONum,1);
#endif
}

///////////////////////////////////////////////////////////////////////////////
//      vOTGDxFRd()
//      Description: Fill the buffer from USB220 Data FIFO
//      input: Buffer pointer, byte number to write, Change Endian type or NOT
//      output: none
///////////////////////////////////////////////////////////////////////////////
void vOTGDxFRd(INT8U FIFONum, INT8U * pu8Buffer, INT16U u16Num)
{
     INT8U *tmp;
//  INT32U wTemp;
    //INT32U FIFO_Sel;

    //if (u16Num==0)//Bruce;;03152005
    //   return;

//  if(FIFONum<4)
//      FIFO_Sel = 1<<FIFONum;
    /*
    else
    {
        fLib_printf("L%x: Set to Wrong FIFO,FIFO%d not support..", u8LineOTGCount ++,FIFONum);
        return;
    }
    */
    mUsbDmaConfig(u16Num,DIRECTION_OUT);
    //mUsbDMA2FIFOSel(FIFO_Sel);
    mUsbDMA2FIFOSel(1<<FIFONum);
    tmp = Replace_SdRAM_Address(pu8Buffer);
    mUsbDmaAddr(((INT32U)tmp));
    //mUsbDmaAddr(((INT32U)pu8Buffer));
    //mUsbDmaAddr((UINT32)Replace_SdRAM_Address(pu8Buffer));
    mUsbDmaStart();
#if 0
    while(1)
    {
        wTemp = mUsbIntSrc2Rd();
        if(wTemp & BIT8)//DMA Error Interrupt
        {
            mUsbFIFOClr((INT32U)FIFONum);
            mUsbIntDmaErrClr();
            fLib_printf("L%x: FIFO%d OUT transfer DMA error..", u8LineOTGCount ++,FIFONum);
            break;
        }
        if(wTemp & BIT7)//DMA Completion Interrupt
        {
            mUsbIntDmaFinishClr();
            break;
        }
#if 0
        if((wTemp & 0x00000007)>0)//If (Resume/Suspend/Reset) exit
        {
            mUsbDMARst();
            mUsbFIFOClr((INT32U)FIFONum);
            mUsbIntDmaFinishClr();
            fLib_printf("L%x: FIFO%d OUT transfer DMA stop because USB Resume/Suspend/Reset..", u8LineOTGCount ++,FIFONum);
            break;
        }
#endif
    }
    mUsbDMA2FIFOSel(FOTG200_DMA2FIFO_Non);
    bOTGDMARunning = FALSE;
#else
    Waiting_For_DMA_Complete(FIFONum,2);
#endif
}


///////////////////////////////////////////////////////////////////////////////
//      vOTGCxFWr()
//      Description: Write the buffer content to USB220 Cx FIFO
//      input: Buffer pointer, byte number to write, Change Endian type or NOT
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTGCxFWr( INT8U *pu8Buffer, INT16U u16Num)
{
//    INT32U wTemp;
	
    mUsbDmaConfig(u16Num,DIRECTION_IN);
    mUsbDMA2FIFOSel(FOTG200_DMA2CxFIFO);
    //if we are using S-dRAM as  memory source , destination , we should add extra 0x1000_0000 for memory mapping
    //pu8Buffer = Replace_SdRAM_Address(pu8Buffer);
    //mUsbDmaAddr((INT32U)(pu8Buffer));
    mUsbDmaAddr((UINT32)Replace_SdRAM_Address(pu8Buffer));
    mUsbDmaStart();
	
#if 0 //kay
    while(1)
    {
        wTemp = mUsbIntSrc2Rd();
        if(wTemp & BIT8)
        {
            mUsbCxFClr();
            mUsbIntDmaErrClr();
            break;
        }
        if(wTemp & BIT7)
        {
            mUsbIntDmaFinishClr();
            break;
        }
    }
    mUsbDMA2FIFOSel(FOTG200_DMA2FIFO_Non);
#else
	Waiting_For_DMA_Complete(CX_EP,11);
#endif
}


///////////////////////////////////////////////////////////////////////////////
//      vOTGCxFRd()
//      Description: Fill the buffer from USB220 Cx FIFO
//      input: Buffer pointer, byte number to write, Change Endian type or NOT
//      output: none
///////////////////////////////////////////////////////////////////////////////
static void vOTGCxFRd(INT8U *pu8Buffer, INT16U u16Num)
{

//    INT32U wTemp;

    //if (u16Num==0)//Bruce;;03152005
    //   return;
    mUsbDmaConfig(u16Num,DIRECTION_OUT);
    mUsbDMA2FIFOSel(FOTG200_DMA2CxFIFO);
    //mUsbDmaAddr((INT32U)(pu8Buffer));
    mUsbDmaAddr((UINT32)Replace_SdRAM_Address(pu8Buffer));
    mUsbDmaStart();
#if 0
    while(1)
    {
        wTemp = mUsbIntSrc2Rd();
        if(wTemp & BIT8)
        {
            mUsbCxFClr();
            mUsbIntDmaErrClr();
            fLib_printf("Cx OUT DMA error..");
            break;
        }
        if(wTemp & BIT7)
        {
            mUsbIntDmaFinishClr();
            break;
        }

    }
    mUsbDMA2FIFOSel(FOTG200_DMA2FIFO_Non);
#else
		Waiting_For_DMA_Complete(CX_EP,12);
#endif

	

}

///////////////////////////////////////////////////////////////////////////////
//      vUsbCxLoopBackTest()
//      Description: Do Cx Data loop back test
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////
#if 0
UINT32 vUsbCxLoopBackTest(CommandTableStruct_T *CmdTable)
{
    INT8U u8TxTmp[64],u8RxTmp[64];
    INT32U u32i;

    // set for Cx loop back test enable
    bFOTGPeri_Port(0x02) = BIT1;

    // write data
    for(u32i = 0; u32i<64; u32i++)
        u8TxTmp[u32i] = u32i;
    vOTGCxFWr(u8TxTmp,64);

    // check Cx Done bit
    mUsbEP0DoneSet();
    while(!(bFOTGPeri_Port(0x0B) | BIT0));

    // set for Cx loop back test clear
    bFOTGPeri_Port(0x02) = BIT2;
    bFOTGPeri_Port(0x0B) &= ~BIT0;

    // read data
    memset(u8RxTmp,0,64);
    vOTGCxFRd(u8RxTmp,64);

    //compare data
    for(u32i = 0; u32i<64; u32i++)
    {
        if(u8TxTmp[u32i] != u8RxTmp[u32i])
        {
            fLib_printf("CxTest%d:***Cx loop test error, Tx=0x%x, Rx=0x%x\n",
                   (INT16U)u32i,u8TxTmp[u32i],u8RxTmp[u32i]);
        }
        else
        {
            fLib_printf("CxTest%d:Cx loop test ok, Tx=0x%x, Rx=0x%x\n",
                   (INT16U)u32i,u8TxTmp[u32i],u8RxTmp[u32i]);
        }
    }
    return 1;
}


///////////////////////////////////////////////////////////////////////////////
//      vUsbPamLoopBackTest()
//      Description: Do PAM (Only FIFO0 or FIFO2) loop back test .
//                  FIFO0 => FIFO0 & FIFO1 ping pong, packet size = 512, transfer type = Bulk
//                  FIFO2 => FIFO2 & FIFO3 ping pong, packet size = 512, transfer type = Bulk
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////

void vUsbPamLoopBackTest(INT32U u32StartFIFONum)
{
    INT8U *u8TxTmp,*u8RxTmp;
    INT32U u32i;
    BOOLEAN bTestPass = TRUE;

    u8TxTmp =(INT8U *) malloc(1024);
    u8RxTmp =(INT8U *) malloc(1024);

    mUsbIntEP0InEn();
    mUsbEPMap(EP1, ((INT8U)u32StartFIFONum));
    mUsbEPMxPtSz(EP1, DIRECTION_IN, 0x200 );
    mUsbFIFOMap(u32StartFIFONum, 0x11);
    mUsbFIFOConfig(u32StartFIFONum, 0x26);
    mUsbFIFOConfig(u32StartFIFONum+1, 0x06);

    // write data
    for(u32i = 0; u32i<1024; u32i++)
        u8TxTmp[u32i] = (INT8U)u32i;
    vOTGDxFWr(u32StartFIFONum,u8TxTmp,1024);
    mUsbFIFODone(u32StartFIFONum);


    mUsbIntEP0OutEn();
    mUsbEPMap(EP1, (((INT8U)u32StartFIFONum) << 4));
    mUsbEPMxPtSz(EP1, DIRECTION_OUT, 0x200 );
    mUsbFIFOMap(u32StartFIFONum, 0x01);
    mUsbFIFOConfig(u32StartFIFONum, 0x26);
    mUsbFIFOConfig(u32StartFIFONum+1, 0x06);

    // read data
    memset(u8RxTmp,0,1024);
    vOTGDxFRd(u32StartFIFONum,u8RxTmp,1024 );

    //compare data
    for(u32i = 0; u32i<1024; u32i++)
    {
        if(u8TxTmp[u32i] != u8RxTmp[u32i])
        {
            bTestPass = FALSE;
            fLib_printf("PAMTest%d:***PAM loop test error, Tx=0x%x, Rx=0x%x\n",
                   (INT16U)u32i,u8TxTmp[u32i],u8RxTmp[u32i]);
        }
        else
        {
//			fLib_printf("PAMTest%d:PAM loop test ok, Tx=0x%x, Rx=0x%x\n",
//				(INT16U)u32i,u8TxTmp[u32i],u8RxTmp[u32i]);
        }
    }

    if(bTestPass)
    {
        fLib_printf("PAMTest:PAM FIFO%d loop test Pass\n",u32StartFIFONum);
    }
    fLib_printf("Please re-plug device into host.\n");
}
#endif

#ifdef USB_VENDOR_COMMAND_SUPPORTED
void vCxOUT_VendorTest(void)
{
    u8CxOUTVendorTest = (INT8U*)malloc(ControlOTGCmd.Length);
    memset(u8CxOUTVendorTest, 0, ControlOTGCmd.Length);

    u16OTGTxRxCounter = ControlOTGCmd.Length;
    pu8OTGDescriptorEX = u8CxOUTVendorTest;
    eOTGCxCommand = CMD_CxOUT_Vendor;
}
void vCxOUT_VendorRxData(void)
{
    INT8U u8temp;
    INT16U u16i;//,j;
    u8 u8LineOTGCount = 0;
    BOOLEAN bCxOUT_CmpOK = TRUE;

    if(u16OTGTxRxCounter < EP0MAXPACKETSIZE)
        u8temp = (INT8U)u16OTGTxRxCounter;
    else
        u8temp = EP0MAXPACKETSIZE;

    u16OTGTxRxCounter -= (INT16U)u8temp;
    vOTGCxFRd(pu8OTGDescriptorEX , u8temp);
    pu8OTGDescriptorEX = pu8OTGDescriptorEX + u8temp;

    // end of the data stage
    if (u16OTGTxRxCounter == 0)
    {
        for(u16i = 0; u16i< ControlOTGCmd.Length; u16i++)
        {
            if(u8CxOUTVendorTest[u16i] != u8CxOUTVandorDataCount)
            {
#if 1
                fLib_printf("L%02x:Vandor CxData Receive error(0x%x/0x%x)-receive=0x%02x,expect=0x%02x\n",
                       u8LineOTGCount++,u16i,ControlOTGCmd.Length, u8CxOUTVendorTest[u16i], u8CxOUTVandorDataCount);
#endif
                bCxOUT_CmpOK = FALSE;
            }
            u8CxOUTVandorDataCount++;
        }
        if(!bCxOUT_CmpOK)
        {
            mUsbEP0StallSet();
            fLib_printf("L%02x:Cx OUT Vendor test error.\n",u8LineOTGCount++);
        }
        eOTGCxCommand = CMD_VOID;
        eOTGCxFinishAction = ACT_DONE;
        free(u8CxOUTVendorTest);
    }

}

void vCxIN_VendorTest(void)
{
    INT16U u16i;
    u8CxINVendorTest = (INT8U*)malloc(ControlOTGCmd.Length);
    for(u16i = 0; u16i< ControlOTGCmd.Length; u16i++)
    {
        u8CxINVendorTest[u16i] = u8CxINVandorDataCount;//0xbb;//
        u8CxINVandorDataCount++;
    }

    u16OTGTxRxCounter = ControlOTGCmd.Length;
    pu8OTGDescriptorEX = u8CxINVendorTest;
    eOTGCxCommand = CMD_CxIN_Vendor;
}

void vCxIN_VendorTxData(void)
{
    INT8U u8temp;

    if(u16OTGTxRxCounter < EP0MAXPACKETSIZE)
        u8temp = (INT8U)u16OTGTxRxCounter;
    else
        u8temp = EP0MAXPACKETSIZE;

    u16OTGTxRxCounter -= (INT16U)u8temp;
    vOTGCxFWr(pu8OTGDescriptorEX , u8temp);
    pu8OTGDescriptorEX = pu8OTGDescriptorEX + u8temp;

    // end of the data stage
    if (u16OTGTxRxCounter == 0)
    {
#if 1
        eOTGCxFinishAction = ACT_DONE;
#else
        mUsbEP0DoneSet();
        eOTGCxFinishAction = ACT_IDLE;
#endif
        eOTGCxCommand = CMD_VOID;
        free(u8CxINVendorTest);
    }

}
#endif
/////////////////////////////////////////////////////
//      vOTGFIFO_EPxCfg_FS(void)
//      Description:
//          1. Configure the FIFO and EPx map
//      input: none
//      output: none
/////////////////////////////////////////////////////
static void vOTGFIFO_EPxCfg_FS(void)
{
#if 0
    *((volatile INT32U *) ( FOTG200_BASE_ADDRESS | (0x1A0)))=0x33330330;
    *((volatile INT32U *) ( FOTG200_BASE_ADDRESS | (0x1A8)))=0x0f0f0f22;
    *((volatile INT32U *) ( FOTG200_BASE_ADDRESS | (0x1AC)))=0x00000626;
    *((volatile INT32U *) ( FOTG200_BASE_ADDRESS | (0x160)))=0x00000040;
    *((volatile INT32U *) ( FOTG200_BASE_ADDRESS | (0x164)))=0x00000040;//Must have this setting for bidirectional FIFO
    *((volatile INT32U *) ( FOTG200_BASE_ADDRESS | (0x184)))=0x00000040;
#endif

    mUsbEPMap_0x1A0((EP2_START_FIFO<<12)|EP1_START_FIFO);
#if(Bulk_Satus == Bulk_FIFO_SingleDir)
    mUsbFIFOMap_0x1A8((((FIFO_DIRECTION_OUT<<4)|EP2)<<(8*(EP2_START_FIFO)))|(FIFO_DIRECTION_IN<<4)|EP1);
#else
    mUsbFIFOMap_0x1A8((FIFO_DIRECTION_BIDRIECTIONAL<<4)|EP2);
#endif
    mUsbFIFOConfig_0x1AC(FIFOCONFIG_0x1AC_VALUE);

    mUsbEPinMxPtSz_Set(EP1,FS_C1_I0_A0_EP1_MAX_PACKET);
    mUsbEPoutMxPtSz_Set(EP2,FS_C1_I0_A0_EP1_MAX_PACKET);
#if(Bulk_Satus == Bulk_FIFO_BiDir)
    mUsbEPinMxPtSz_Set(EP2,FS_C1_I0_A0_EP1_MAX_PACKET);
#endif
}

/////////////////////////////////////////////////////
//      vOTGFIFO_EPxCfg_HS(void)
//      Description:
//          1. Configure the FIFO and EPx map
//      input: none
//      output: none
/////////////////////////////////////////////////////
#ifdef HS_SUPPORTED
void vOTGFIFO_EPxCfg_HS(void)
{
    int i;
    switch (u8OTGConfigValue)
    {
#if (HS_CONFIGURATION_NUMBER >= 0X01)
        // Configuration 0X01
    case 0X01:
        switch (u8OTGInterfaceValue)
        {
#if (HS_C1_INTERFACE_NUMBER >= 0x01)
            // Interface 0
        case 0:
            switch (u8OTGInterfaceAlternateSetting)
            {
#if (HS_C1_I0_ALT_NUMBER >= 0X01)
                // AlternateSetting 0
            case 0:
#if (HS_C1_I0_A0_EP_NUMBER >= 0X01)
                //EP0X01
                mUsbEPMap(EP1, HS_C1_I0_A0_EP1_MAP);
                mUsbFIFOMap(HS_C1_I0_A0_EP1_FIFO_START, HS_C1_I0_A0_EP1_FIFO_MAP);
                mUsbFIFOConfig(HS_C1_I0_A0_EP1_FIFO_START, HS_C1_I0_A0_EP1_FIFO_CONFIG);

                for(i = HS_C1_I0_A0_EP1_FIFO_START + 1 ;
                        i < HS_C1_I0_A0_EP1_FIFO_START + HS_C1_I0_A0_EP1_FIFO_NO ;
                        i ++)
                {
                    //mUsbFIFOConfig(i, (HS_C1_I0_A0_EP1_FIFO_CONFIG & (~BIT7)) );
                    mUsbFIFOConfig(i, (HS_C1_I0_A0_EP1_FIFO_CONFIG & (~BIT5)));//bessel:Enable FIFO x is not bit7(for fusb220, yes, it's on bit7), it is on bit5
                }

                mUsbEPMxPtSz(EP1, HS_C1_I0_A0_EP1_DIRECTION, (HS_C1_I0_A0_EP1_MAX_PACKET & 0x7ff) );
                mUsbEPinHighBandSet(EP1 , HS_C1_I0_A0_EP1_DIRECTION , HS_C1_I0_A0_EP1_MAX_PACKET);
#endif
#if (HS_C1_I0_A0_EP_NUMBER >= 0X02)
                //EP0X02
                mUsbEPMap(EP2, HS_C1_I0_A0_EP2_MAP);

#if((OTG_AP_Satus == Bulk_AP) && (Bulk_Satus == Bulk_FIFO_SingleDir))
                mUsbFIFOMap(HS_C1_I0_A0_EP2_FIFO_START, HS_C1_I0_A0_EP2_FIFO_MAP);
#endif

                mUsbFIFOConfig(HS_C1_I0_A0_EP2_FIFO_START, HS_C1_I0_A0_EP2_FIFO_CONFIG);

                for(i = HS_C1_I0_A0_EP2_FIFO_START + 1 ;
                        i < HS_C1_I0_A0_EP2_FIFO_START + HS_C1_I0_A0_EP2_FIFO_NO ;
                        i ++)
                {
                    mUsbFIFOConfig(i, (HS_C1_I0_A0_EP2_FIFO_CONFIG & (~BIT5/*7*/)) );
                }

                mUsbEPMxPtSz(EP2, HS_C1_I0_A0_EP2_DIRECTION, (HS_C1_I0_A0_EP2_MAX_PACKET & 0x7ff) );
                mUsbEPinHighBandSet(EP2 , HS_C1_I0_A0_EP2_DIRECTION , HS_C1_I0_A0_EP2_MAX_PACKET);
#endif

#if (HS_C1_I0_A0_EP_NUMBER >= 0X03)
                //EP0X03
                mUsbEPMap(EP3, HS_C1_I0_A0_EP3_MAP);
                mUsbFIFOMap(HS_C1_I0_A0_EP3_FIFO_START, HS_C1_I0_A0_EP3_FIFO_MAP);
                mUsbFIFOConfig(HS_C1_I0_A0_EP3_FIFO_START, HS_C1_I0_A0_EP3_FIFO_CONFIG);

                for(i = HS_C1_I0_A0_EP3_FIFO_START + 1 ;
                        i < HS_C1_I0_A0_EP3_FIFO_START + HS_C1_I0_A0_EP3_FIFO_NO ;
                        i ++)
                {
                    mUsbFIFOConfig(i, (HS_C1_I0_A0_EP3_FIFO_CONFIG & (~BIT5/*7*/)) );
                }

                mUsbEPMxPtSz(EP3, HS_C1_I0_A0_EP3_DIRECTION, (HS_C1_I0_A0_EP3_MAX_PACKET & 0x7ff) );
                mUsbEPinHighBandSet(EP3, HS_C1_I0_A0_EP3_DIRECTION , HS_C1_I0_A0_EP3_MAX_PACKET);
#endif

#if (HS_C1_I0_A0_EP_NUMBER >= 0X04)
                //EP0X04
                mUsbEPMap(EP4, HS_C1_I0_A0_EP4_MAP);
                mUsbFIFOMap(HS_C1_I0_A0_EP4_FIFO_START, HS_C1_I0_A0_EP4_FIFO_MAP);
                mUsbFIFOConfig(HS_C1_I0_A0_EP4_FIFO_START, HS_C1_I0_A0_EP4_FIFO_CONFIG);

                for(i = HS_C1_I0_A0_EP4_FIFO_START + 1 ;
                        i < HS_C1_I0_A0_EP4_FIFO_START + HS_C1_I0_A0_EP4_FIFO_NO ;
                        i ++)
                {
                    mUsbFIFOConfig(i, (HS_C1_I0_A0_EP4_FIFO_CONFIG & (~BIT5/*7*/)) );
                }

                mUsbEPMxPtSz(EP4, HS_C1_I0_A0_EP4_DIRECTION, (HS_C1_I0_A0_EP4_MAX_PACKET & 0x7ff) );
                mUsbEPinHighBandSet(EP4 , HS_C1_I0_A0_EP4_DIRECTION , HS_C1_I0_A0_EP4_MAX_PACKET);
#endif
                break;
#endif
            default:
                break;
            }
            break;
#endif
        default:
            break;
        }
        break;
#endif
    default:
        break;
    }
}
#endif
//============================================================================= ok
//      OTGP_init()
//      Description:
//      input: Reserved
//      output: Reserved
//=============================================================================
static void OTGP_init(UINT8 bInitAP)
{
    // Initial Interrupt and ISO transfer buffer
    if (bInitAP==1) {
        vOTG_APInit();
    }

    // Initial global variable
    //bOTGDMARunning = FALSE;

    // Initial FUSB220 Reg
    //vOTGSysInit();
    //For OPT;;vOTG_APInit();
    vOTGInit();
}

//=============================================================================
//      OTGP_main()
//      Description: Leave when the VBus is not valid
//
//             bWaitForVBUS =0: Do not wait for VBUS
//                                  =1: wait for VBUS
//      output:
//=============================================================================
void OTGP_main(UINT8 bWaitForVBUS)
{
  #ifdef BLUETOOTH_FUNCTION_TEST
    unsigned int show=0;
    unsigned int count=0;
    unsigned int stop_show_message=0;
  #endif

		fLib_printf("Wait for VBUS high..\n");
    //<1>.Waiting for the VBus high
    if(bWaitForVBUS>0)
        while(mdwOTGC_Control_A_VBUS_VLD_Rd()==0)
            ;
		fLib_printf("VBUS Done\n");
    //<2>.Enable the Peripheral Interrupt
   // OTGP_init(0);//0:Do not init AP
		OTGP_init(1);//0:Do not init AP
    //<3>.Waiting for the VBUS is not valid
//  bFlagLeave=0;
    //mUsbTstHalfSpeedEn();//bessel:test:
    //mUsbOTGDevFS();//bessel:add to set FOTG210 operates at Full-Speed mode
    //mUsbGlobIntEnSet();
    mUsbChipEnSet();
	fLib_printf("port register 0x80 = %x\n",wFOTGPeri_Port(0x80));

    mUsbIntSrc0Clr();//Clear interrupt status
		//	 mUsbIntSrc1Clr();	
    mUsbIntSrc2Clr();//Clear interrupt status
    //Device Mask of Interrupt Source Group 0 Register (Address = 0x134): enable all interrupts(Default is all enabled)
		
    mUsbIntSrc1Mask_Set(0xFFFEEFCF);//Device Mask of Interrupt Source Group 1 Register (Address = 0x138): disable all interrupts
    mUsbIntSrc2Mask_Set(0xFFF8);//Device Mask of Interrupt Source Group 2 Register (Address = 0x13C):Only to enable Rese,Resume and Suspend interrups
    //Device Mask of Interrupt Group Register (Address = 0x130)://Default is to enable interrupt of source group 0,1 and 2
    mUsbGlobIntEnSet();//Global Interrupt Enable
//mUsbGlobIntEnSet();
//mUsbIntF0INEn();
//mUsbIntF2INEn();
//Iterrupt is high active (trigger-mode=Level-triggered & Trigger Level =  Active-high ) and only enable Device mode interrupt
#ifdef CONFIG_FOTG021_INVERSE_INT_POLARITY
    mwOTG20_Interrupt_Mask_Set(0x06);
#else
  dddd  mwOTG20_Interrupt_Mask_Set(0x0E);
#endif

    //vLib_LeWrite32(0x50000000+0x80000, 0xeeee0000);
    //<4>.Turn on D+
    mUsbUnPLGClr();//unplug issue;;
    //Clear_Reg(0x58000000+0x31c,BIT7);
    // Initial FOTG200 device Reg
    //  mUsbIntSuspDis();
    //  mUsbTstHalfSpeedEn();       // Set for FPGA Testing, if AHB>30Mhz, doesn't need to set.
    //  mUsbChipEnSet();            // Set for PAM enable
    //  mUsbUnPLGClr();
    //  vOTGSysInit();
#if 0 //Just for test
    u32UsbCmd =(INT32U *)SetupCommand;
    // Read 8-byte setup packet from FIFO
    mUsbDMA2FIFOSel(FOTG200_DMA2CxFIFO);
    u32UsbCmd[0] = mUsbEP0CmdDataRdDWord();
    u32UsbCmd[1] = mUsbEP0CmdDataRdDWord();
    mUsbDMA2FIFOSel(FOTG200_DMA2FIFO_Non);
    u8UsbCmd = (INT8U *)u32UsbCmd;

#endif
#if 0
    while(1)
        mUsbDMA2FIFOSel((BIT0<<i));
#endif
    //fLib_memset((char *)test_data, 0xFF, sizeof(test_data));

    while(0)
    {
#ifdef BLUETOOTH_FUNCTION_TEST
        show++;
        if((stop_show_message==0)&&(show == 40000))
        {
            count++;
            fLib_printf("%d\n",count);
            fLib_printf("Message is from Faraday's Cortex-M0 EVB\n");
            show=0;
        }
		switch(GETCH())
		{
			case 's'://Stop to show message
			case 'S':
				stop_show_message=1;
			break;
			case 'g'://Resume to show message
			case 'G':
				stop_show_message=0;
				show=0;
			break;			
		}
#endif
    }
    
}
//============================================================================= ok
//      OTGP_Close()
//      Description:
//      input: Reserved
//      output: Reserved
//=============================================================================
void OTGP_Close(void)
{
    //UINT32 wTemp;

    //<1>.Clear All the Interrupt
    mUsbGlobIntDis();

    //<2>.Clear all the Interrupt Status
    mUsbIntGroupRegRd();  //wTemp=mUsbIntGroupRegRd();
    mUsbIntGroupRegSet(0);

    //Interrupt source group 0(0x144)
    mUsbIntSrc0Rd();  //wTemp=mUsbIntSrc0Rd();
    mUsbIntSrc0Set(0);

    //Interrupt source group 1(0x148)
    mUsbIntSrc1Rd(); //wTemp=mUsbIntSrc1Rd();
    mUsbIntSrc1Set(0);

    //Interrupt source group 2(0x14C)
    mUsbIntSrc2Rd();  //wTemp=mUsbIntSrc2Rd();
    mUsbIntSrc2Set(0);

    //<3>.Turn off D+
    if (mdwOTGC_Control_CROLE_Rd()>0)//For Current Role = Peripheral
        mUsbUnPLGSet();
}

//==================== For OTG Control ============================

///////////////////////////////////////////////////////////////////////////////
//      input: none
//      output: none
///////////////////////////////////////////////////////////////////////////////

void vUsbWrite(u8* buffer, u32 size)
{
    INT8U *u8TxTmp = (INT8U*) buffer;
	INT32U u32StartFIFONum = FIFO0;
    
    mUsbIntEP0InEn();
    mUsbEPMap(EP1, ((INT8U)u32StartFIFONum));
    mUsbEPMxPtSz(EP1, DIRECTION_IN, size );
    mUsbFIFOMap(u32StartFIFONum, 0x11);
    mUsbFIFOConfig(u32StartFIFONum, 0x26);
    mUsbFIFOConfig(u32StartFIFONum+1, 0x06);

    // write data
    vOTGDxFWr(u32StartFIFONum,u8TxTmp,size);
    mUsbFIFODone(u32StartFIFONum);
}

void vUsbRead(u8* buf, u32 size)
{
    INT8U *buffer = (INT8U*) buf;
	INT32U u32StartFIFONum = FIFO0;
    
    mUsbIntEP0OutEn();
    mUsbEPMap(EP1, (INT8U)u32StartFIFONum);
    mUsbEPMxPtSz(EP1, DIRECTION_OUT, 0x200 );
    mUsbFIFOMap(u32StartFIFONum, 0x01);
    mUsbFIFOConfig(u32StartFIFONum, 0x26);
    mUsbFIFOConfig(u32StartFIFONum+1, 0x06);

    // read data
    vOTGDxFRd(u32StartFIFONum, buffer, size );
}
