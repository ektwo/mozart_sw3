/**************************************************************************//**
 * @file     kneron_mozart_ext.h
 * @brief    CMSIS Core Peripheral Extension Access Layer Header File for
 *           ARMCM4 Device (configured for CM4 without FPU)
 * @version  V5.3.1
 * @date     09. July 2018
 ******************************************************************************/
#ifndef Kneron_Mozart_EXT
#define Kneron_Mozart_EXT


/* NCPU/SCPU ID status */
#define CPU_ID_STS_ADDR                 0xE00FF01C 
#define CPU_ID_SCPU                     0x53430000
#define CPU_ID_NCPU                     0x4e430000



#endif  /* Kneron_Mozart */

