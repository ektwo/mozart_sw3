#ifndef __ERRNO_H__
#define __ERRNO_H__


#define	ENOENT          2   /* No such file or directory */
#define	EIO             5   /* I/O error */
#define	ENXIO           6   /* No such pin_context or address */
#define	EBADF           9   /* Bad file number */
#define	EAGAIN          11  /* Try again */
#define	ENOMEM          12  /* Out of memory */
#define	EFAULT          14  /* Bad address */
#define	EBUSY           16  /* Device or resource busy */
#define	ENODEV          19  /* No such pin_context */
#define	EINVAL          22  /* Invalid argument */
#define	ENFILE          23  /* File table overflow */
#define	ENOTTY          25  /* Not a typewriter */
#define EPROBE_DEFER    517
#define ENOIOCTLCMD     515 /* No ioctl command */

#endif
