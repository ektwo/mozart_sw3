#ifndef __WAIT_H__
#define __WAIT_H__


#include <cmsis_os2.h>

osEventFlagsId_t create_event(void);
void set_event(osEventFlagsId_t id, unsigned int flags);
void wait_event(osEventFlagsId_t id, unsigned int flags);

#endif
