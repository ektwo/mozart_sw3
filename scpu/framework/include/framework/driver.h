#ifndef __DRIVER_H__
#define __DRIVER_H__


#include <stddef.h>
#include <framework/init.h> 
#include <framework/kdp_list.h>


struct pin_context {
    void *platform_data;
    void *driver_data;
};
struct pin_context2 {
    void *platform_data;
};

struct driver_context {
    char name[16];

    int (*add) (struct driver_context *, struct pin_context *);
    int (*del) (struct driver_context *, struct pin_context *);

    struct kdp_list_node knode_to_buslist;
};

struct driver_context2 {
    int (*probe)(struct driver_context2 *, struct pin_context *);
    int (*reset)(struct driver_context2 *, struct pin_context *);
};

struct core_device {
    //const char *name;
    struct ioport_setting *ioport;
    u32 num_ioports;
    int uuid; // universal unique id
    struct pin_context pin_ctx;
};
#define to_core_device(x) container_of((x), struct core_device, pin_ctx)

struct core_driver {
    int (*probe)(struct core_device *);
    int (*remove)(struct core_device *);
    int (*reset)(struct core_device *);
    struct core_device *core_dev;
    struct driver_context driver;
};
#define to_core_driver(drv)	(container_of((drv), struct core_driver, driver))

struct lcdc_driver {
    int (*probe)(struct core_device *);
    int (*remove)(struct core_device *);
    int (*reset)(struct core_device *);    
    struct core_device *core_dev;
    struct driver_context driver;
    
    int (*pip_test)(struct core_device *);
};

extern struct driver_context* find_driver(char *);
extern struct ioport_setting *driver_get_ioport_setting(struct core_device *, unsigned int);

extern int drivers_init(void);
extern int driver_register(struct driver_context *, struct pin_context *);
extern void driver_unregister(struct driver_context *);
//extern int driver_core_register(struct core_driver *);
//extern void driver_core_unregister(struct core_driver *);
extern int driver_core_register(void *);
extern void driver_core_unregister(void *);

void *dev_get_drvdata(const struct pin_context *);
void dev_set_drvdata(struct pin_context *, void *data);

#define DRIVER_SETUP(__driver, __setup_level, __register, __unregister, ...) \
static int INITTEXT __driver##_entr(void) { \
    return __register(&(__driver) , ##__VA_ARGS__); \
} \
ARRANGE_ENTR_RO_SECTION(__driver##_entr, __setup_level); \
static void FINITEXT __driver##_exit(void) { \
    __unregister(&(__driver) , ##__VA_ARGS__); \
} \
ARRANGE_EXIT_RO_SECTION(__driver##_exit);

#define DRIVER_SETUP_2(__driver, __setup_level, __register, __unregister, ...) \
static int INITTEXT __driver##_entr(void) { \
    return __register((struct core_driver*)(&(__driver)), ##__VA_ARGS__); \
} \
ARRANGE_ENTR_RO_SECTION(__driver##_entr, __setup_level); \
static void FINITEXT __driver##_exit(void) { \
    __unregister((struct core_driver*)(&(__driver)), ##__VA_ARGS__); \
} \

#define KDP_CORE_DRIVER_SETUP(__core_driver, __setup_level) \
    DRIVER_SETUP(__core_driver, __setup_level, driver_core_register, driver_core_unregister)
#define KDP_CORE_DRIVER_SETUP_2(__core_driver, __setup_level) \
    DRIVER_SETUP_2(__core_driver, __setup_level, driver_core_register, driver_core_unregister)

#endif /* __DEVICE_H__ */
