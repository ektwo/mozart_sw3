#ifndef __MM_H__
#define __MM_H__

#define MAP_FAILED  ((void *)-1)

#ifndef PAGE_SIZE
#define PAGE_SIZE 32
#endif

#define __ALIGN_MASK(x, mask)   (((x) + (mask)) & ~(mask))
#define __ALIGN(x, a)           __ALIGN_MASK(x, (typeof(x))(a) - 1)
#define PAGE_ALIGN(addr)        __ALIGN(addr, PAGE_SIZE)


struct vm_area_struct {
    unsigned long vm_start;
    unsigned long vm_end;
    //char name[16];
    //struct vm_area_struct *vm_next, *vm_prev;
};

struct mm_struct {
    struct vm_area_struct *mmap;

    unsigned long mmap_base;
    unsigned long csirx0_mm_user;
    unsigned long csirx0_mm_phy;
    unsigned long csirx0_mm_phy_2;
    unsigned long csirx1_mm_user;
    unsigned long csirx1_mm_phy;
    unsigned long csirx1_mm_phy_2;
};

struct videobuf_dm_ctx {
    unsigned char idx;
    unsigned long phy_start_addr;
    unsigned long vir_start_addr;
    unsigned long allocated_length;
};

// for driver (internal use)
void* dm_alloc(void *alloc_ctx, unsigned long size);
struct mm_struct *get_mm_struct(void);

// for application api
void *kdp_mmap( unsigned int length, 
                int flags, 
                int fd, 
                unsigned int offset);

#endif
