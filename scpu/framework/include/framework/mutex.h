#ifndef __MUTEX_H__
#define __MUTEX_H__

#include <cmsis_os2.h>

struct mutex {
    osMutexId_t id; 
};

void mutex_init(struct mutex *lock);
void mutex_lock(struct mutex *lock);
void mutex_unlock(struct mutex *lock);

#endif
