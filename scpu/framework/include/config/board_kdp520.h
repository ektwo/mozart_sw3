#ifndef __BOARD_KDP520_H__
#define __BOARD_KDP520_H__

#include <kneron_mozart.h>

#define SYS_CAMERA_CORE_NAME    "sys_camera_core"

#define SENSOR_HMX2056          1
#define SENSOR_HMXRICA_RAW8     2


/* i2c platform data */
struct i2c_platform_data {
    unsigned long bus_speed;
};

/* lcdc platform data */

enum lcd_img_input_fmt {
    lcd_img_input_fmt_rgb565 = 0,
    lcd_img_input_fmt_rgb555,
    lcd_img_input_fmt_rgb444,
    lcd_img_input_fmt_rgb24,
    lcd_img_input_fmt_ycbcr422,
    lcd_img_input_fmt_ycbcr420,
    lcd_img_input_fmt_palette_8,
    lcd_img_input_fmt_palette_4,
    lcd_img_input_fmt_palette_2,
    lcd_img_input_fmt_palette_1,
};

struct fb_platform_data {
    unsigned long input_fmt; // refer to enum lcd_img_input_fmt definition
    unsigned short xres;
    unsigned short yres; 
};


#endif
