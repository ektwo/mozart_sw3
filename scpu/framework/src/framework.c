/*
 * @name : framework.c
 * @brief : The startup thread is responsible for the core initialization
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#ifdef V2K_CAM_ENABLE
#include <stdio.h>
#include <stdlib.h>
#include <cmsis_os2.h>
#include <framework/init.h>
#include <framework/driver.h>
#include <framework/framework.h>
#include <utility.h>
#include <dbg.h>


#define FLAG_FRAMEWORK_INIT_START   0x0011
#define FLAG_KERNEL_INIT_STOP       0x0012

osThreadId_t tid_framework;
extern osThreadId_t tid_v2k_cam;

//extern pf_entr entr_sectionn0$$Base;
//extern pf_entr entr_section0$$Limit;
extern pf_entr entr_section1$$Base;
extern pf_entr entr_section1$$Limit;
extern pf_entr entr_section2$$Base;
extern pf_entr entr_section2$$Limit;
extern pf_entr entr_section3$$Base;
extern pf_entr entr_section3$$Limit;
extern pf_entr entr_section4$$Base;
extern pf_entr entr_section4$$Limit;
extern pf_entr entr_section5$$Base;
extern pf_entr entr_section5$$Limit;
extern pf_entr entr_section6$$Base;
extern pf_entr entr_section6$$Limit;

static pf_entr *entrance_section_levels[] INITDATA = {
	//&entr_section0$$Base, &entr_section0$$Limit,
    &entr_section1$$Base, &entr_section1$$Limit,
    &entr_section2$$Base, &entr_section2$$Limit,
    &entr_section3$$Base, &entr_section3$$Limit,
    &entr_section4$$Base, &entr_section4$$Limit,
    &entr_section5$$Base, &entr_section5$$Limit,
    &entr_section6$$Base, &entr_section6$$Limit,
};


static void INITTEXT _do_entrance_section(int level) {

	pf_entr *fn;
	for (fn = entrance_section_levels[level]; fn < entrance_section_levels[level + 1]; fn++)
	{
        if (*fn)
            (*fn)();
	}
}

static void INITTEXT do_entrance_sections(void) {
	int level;

	for (level = 0; level < ARRAY_SIZE(entrance_section_levels); level += 2) {
        DSG("do_entrance_sections");
		_do_entrance_section(level);
	}
}

static void framework_init(void *argument) {    
    uint32_t flags;    
    //flags = osThreadFlagsWait(FLAG_FRAMEWORK_INIT_START, osFlagsWaitAny ,osWaitForever);
    //osThreadFlagsClear(flags);

	drivers_init();
 
	do_entrance_sections();

    osThreadFlagsSet(tid_v2k_cam, FLAG_KERNEL_INIT_READY_EVT);

    for (;;) {
        flags = osThreadFlagsWait(FLAG_KERNEL_INIT_STOP, osFlagsWaitAny, osWaitForever);
        osThreadFlagsClear(flags);
    }
}

void start_framework(void) {
	tid_framework = osThreadNew(framework_init, NULL, NULL);
    //if (tid_framework)
    //    osThreadFlagsSet(tid_framework, FLAG_FRAMEWORK_INIT_START); 
}

int late_entrance(void) {
    DSG("late_entrance");
	return 0;
}
ARRANGE_ENTR_RO_SECTION(late_entrance, 6);
#endif
