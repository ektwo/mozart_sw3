#include <framework/event.h>
#include <dbg.h>

osEventFlagsId_t create_event() {
    return osEventFlagsNew(NULL);
}

void set_event(osEventFlagsId_t id, unsigned int flags) {
    osEventFlagsSet(id, flags);
}

void wait_event(osEventFlagsId_t id, unsigned int flags) {
    osEventFlagsWait(id, flags, osFlagsWaitAny, osWaitForever);
}
