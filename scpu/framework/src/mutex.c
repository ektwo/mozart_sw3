#include <framework/mutex.h>


void mutex_init(struct mutex *lock)
{
    lock->id = osMutexNew(NULL);
}

void mutex_lock(struct mutex *lock)
{
	//might_sleep();
	osMutexAcquire(lock->id, 0);
}

void mutex_unlock(struct mutex *lock)
{
    osMutexRelease(lock->id);   
}
