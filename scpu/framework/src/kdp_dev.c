/*
 * @name : v2k_dev_handle.c
 * @brief : open/close the device handler specified by dev_name. 
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */ 
#include <string.h>
#include <framework/types.h>
#include <framework/utils.h>
#include <framework/errno.h>
#include <framework/kdp_dev.h>
#include <media/videodev.h>
#include <dbg.h>


#define KDP_DEV_PREFIX_NAME "kdp_dev"

extern const struct v2k_dev_operations* v2k_get_dev_ops(void);
extern struct v2k_dev_handle* v2k_get_dev_handle(void);
extern void v2k_set_dev_handle(struct v2k_dev_handle *handle);

typedef struct v2k_dev_handle kdp_dev_handle;


static const char *get_v2k_namebase(void) {
#define V2K_NAMEBASE "video"
    return V2K_NAMEBASE;
}

int kdp_dev_open(const char *kdp_dev_name)
{
    char tmp1[16], tmp2[16];
    int tmp_fd, fd = -1;
    
    strcpy(tmp1, kdp_dev_name);
    strncpy(tmp2, kdp_dev_name, strlen(kdp_dev_name) - 1);

    char *dev_name = strrchr(tmp1, '/');
    
    (dev_name) ? ++dev_name : (dev_name = tmp1);
    tmp1[dev_name - tmp1 - 1] = '\0';

    if (0 == strcmp(tmp1, KDP_DEV_PREFIX_NAME) &&
       (strstr(&tmp2[dev_name - tmp1], get_v2k_namebase()))) {
        kdp_dev_handle f, *pf;
        tmp_fd = dev_name[strlen(dev_name) - 1] - 0x30;
        DSG("dev_name=%s c=%d", dev_name, tmp_fd);

        f.i_rdev = tmp_fd;
        v2k_set_dev_handle(&f);
        pf = v2k_get_dev_handle();
        if (pf->dev_ops) { 
            if (0 == pf->dev_ops->open(pf)) {
                fd = tmp_fd + 1;
            }
        }
    }

    return fd;
}

int kdp_dev_close(int fd)
{
    return 0;
}

int kdp_dev_ioctl(int fd, unsigned int cmd, void *arg)
{
    int error = -EBADF;
    kdp_dev_handle *handle = v2k_get_dev_handle();
    if ((fd == (handle->i_rdev + 1)) &&
        (handle->dev_ops) && (handle->dev_ops->ioctl)) {
        error = (int)handle->dev_ops->ioctl(handle, cmd, arg);
    }

    return error;
}
