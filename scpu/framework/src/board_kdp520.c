/*
 * @name : board_kdp520.c
 * @brief : Setup code for mozart/Haps
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#ifdef V2K_CAM_ENABLE
#include <stdio.h> 
#include <string.h> 
#include <config/board_kdp520.h>
#include <framework/init.h>
#include <framework/driver.h>
#include <framework/ioport.h> 
#include <framework/mm.h>
#include <interface/i2c.h>
#include <media/sys_camera.h>
#include <utility.h>
#include "memory.h"
#include "dbg.h"


static int kdp520_csi2_power(struct pin_context *pin_ctx, int on)
{
    //DSG("<%s:%s>", __FILE__, __func__);

    return 0;
}

struct ioport_setting io_i2c0[] = {
    {
        .start      = IIC_FTIIC010_0_PA_BASE,
        .flags      = IORESOURCE_MEM,
    }, {
        .start      = IIC_FTIIC010_0_IRQ,
        .flags      = IORESOURCE_IRQ,
    },
};

struct ioport_setting io_i2c1[] = {
    {
        .start      = IIC_FTIIC010_1_PA_BASE,
        .flags      = IORESOURCE_MEM,
    }, {
        .start      = IIC_FTIIC010_1_IRQ,
        .flags      = IORESOURCE_IRQ,
    },
};

struct ioport_setting io_i2c2[] = {
    {
        .start      = IIC_FTIIC010_2_PA_BASE,
        .flags      = IORESOURCE_MEM,
    }, {
        .start      = IIC_FTIIC010_2_IRQ,
        .flags      = IORESOURCE_IRQ,
    },
};
struct ioport_setting io_i2c3[] = {
    {
        .start      = IIC_FTIIC010_3_PA_BASE,
        .flags      = IORESOURCE_MEM,
    }, {
        .start      = IIC_FTIIC010_3_IRQ,
        .flags      = IORESOURCE_IRQ,
    },
};

static struct i2c_platform_data i2c_data = {
    .bus_speed = 200000,
};

struct core_device kdp520_i2c0 = {
    //.name           = "kdp520_i2c",
    .ioport         = io_i2c0,
    .num_ioports    = ARRAY_SIZE(io_i2c0) ,
    .uuid           = 0,
    .pin_ctx = {
        .platform_data = &i2c_data,
    },
};
struct core_device kdp520_i2c1 = {
    //.name           = "kdp520_i2c",
    .ioport         = io_i2c1,
    .num_ioports    = ARRAY_SIZE(io_i2c1) ,
    .uuid           = 1,
    .pin_ctx = {
        .platform_data = &i2c_data,
    },
};
struct core_device kdp520_i2c2 = {
    //.name           = "kdp520_i2c",
    .ioport         = io_i2c2,
    .num_ioports    = ARRAY_SIZE(io_i2c2) ,
    .uuid           = 2,
    .pin_ctx = {
        .platform_data = &i2c_data,
    },
};
struct core_device kdp520_i2c3 = {
    //.name           = "kdp520_i2c",
    .ioport         = io_i2c3,
    .num_ioports    = ARRAY_SIZE(io_i2c3) ,
    .uuid           = 3,
    .pin_ctx = {
        .platform_data = &i2c_data,
    },    
};

/* camera pin_context */
static struct i2c_board_info INITDATA i2c_info0[] = {
    { .name = "sensor-hmx2056", .addr = 0x24 },
    { .name = "sensor-hmxrica", .addr = 0x38 },
};

#if V2K_CAM_ENABLE == SENSOR_HMX2056
static struct sys_camera_link hmx2056_link = {
    .connect_to     = 10, // mipi rx 0
    .power          = kdp520_csi2_power,
    .board_info     = &i2c_info0[0], 
    .i2c_adapter_id	= 0,
};
struct core_device hmx2056_link_device = {
    .uuid           = 4,
    .pin_ctx = {
        .platform_data = &hmx2056_link,
    },
};
#else
static struct sys_camera_link hmxrica_link = {
    .connect_to     = 10, // mipi rx 0
    .power          = kdp520_csi2_power,
    .board_info     = &i2c_info0[1], 
    .i2c_adapter_id	= 0,
};
struct core_device hmxrica_link_device = {
    .uuid           = 4,
    .pin_ctx = {
        .platform_data = &hmxrica_link,
    },
};
#endif

struct core_device kdp520_camera_dev = {
    //.name           = SYS_CAMERA_CORE_NAME,
    .uuid           = 5, //nothing
    .pin_ctx = {
    #if V2K_CAM_ENABLE == SENSOR_HMX2056
        .platform_data = &hmx2056_link,
    #elif V2K_CAM_ENABLE == SENSOR_HMXRICA_RAW8
        .platform_data = &hmxrica_link,
    #endif
    },
};

static struct ioport_setting csi2rx_resources0[] = {
    [0] = {
        .start = DPI2AHB_CSR_PA_BASE,
        .flags = IORESOURCE_BUS,
    },
    [1] = {
        .start = CSIRX_FTCSIRX100_PA_BASE,
        .flags = IORESOURCE_MEM,
    },
};

struct core_device kdp520_csi2rx0 = {
    //.name           = "kdp520_csi2rx0",
    .ioport         = csi2rx_resources0,
    .num_ioports    = ARRAY_SIZE(csi2rx_resources0),
    .uuid           = 10, 
};
//struct core_device kdp520_csi2rx1 = {
//    .name           = "kdp520_csi2rx1",
//    .ioport         = csi2rx_resources1,
//    .num_ioports    = ARRAY_SIZE(csi2rx_resources1),
//    .uuid             = 11, 
//};


static struct fb_platform_data lcdc_info = {
    .input_fmt          = lcd_img_input_fmt_rgb565,
    .xres               = 480,
    .yres               = 272,
};
struct core_device kdp520_lcdc = {
    .pin_ctx = {
        .platform_data = &lcdc_info,
    },
};

/* setup.c */
struct vm_area_struct ddr_vm_area = {
    .vm_start       = KDP_DDR_BASE_MOZART,
    .vm_end         = KDP_DDR_END,
};

int INITTEXT setup_framework(void) 
{
    struct mm_struct *mm = get_mm_struct();
    mm->mmap            = &ddr_vm_area;    
    mm->mmap_base       = 0x00000000;
    mm->csirx0_mm_user  = 0x02FFE000;
    mm->csirx0_mm_phy   = 0x032A1000;
    mm->csirx0_mm_phy_2 = 0x03544000;
    mm->csirx1_mm_user  = 0x037E7000;
    mm->csirx1_mm_phy   = 0x03A8A000;
    mm->csirx1_mm_phy_2 = 0x03D2D000;
    
    return 0;
}
ARRANGE_ENTR_RO_SECTION(setup_framework, 1);

#endif
