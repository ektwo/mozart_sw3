/*
 * Kneron Host Communication driver
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */
 
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include "cmsis_os2.h"
#include "io.h"
#include "base.h"
#include "DrvUART010.h"
#include "host_com.h"
#include "msg.h"
#include "com.h"
#include "crc.h"
#include "kdp_com.h"
#include "kneron_mozart.h"
#include "dbg.h"
#include "kdpAppMgr.h"
#include "kdpAppFID.h"   //for app input/output data structure
#include "kdp_app_db.h"  //for app input/output data structure
#include "kdpApp.h"
#include "usb_com.h"

#define TIMER_DEFAULT					1
#ifdef MOZART_CHIP
#define COM_IRQ     					UART_FTUART010_1_3_IRQ
#else
#define COM_IRQ     					UART_FTUART010_1_IRQ
#endif
#define COM_BAUD                        BAUD_115200//BAUD_921600//BAUD_460800//BAUD_115200

// SFID prototype (temp)
u32 usb_mem_read(u32 address, u32 image_size);
#define KDP_APP_DB_DIFF_UID 30  // ERR_INCONSISTENT_UID, needs to go into kdpApp.h <<<<<<<<<<<<
#define MSG_APP_BAD_IMAGE 0x100  // 256
#define MSG_APP_BAD_INDEX 0x101
#define MSG_APP_UID_EXIST 0x102
#define MSG_APP_UID_DIFF 0x103
#define MSG_APP_IDX_FIRST 0x104
#define MSG_APP_IDX_MISSING 0x105
#define MSG_APP_DB_NO_USER 0x106
#define MSG_APP_DB_FAIL 0x107  // unknow general failure
#define MSG_APP_DB_NO_MATCH 10

osThreadId_t tid_host_comm;
osTimerId_t tmr_host_comm;

// Global Application Mode Control
u16 SFID_user;   // SFID user id
u16 SFID_index;  // SFID image index
u32 SFID_img_base, SFID_img_count;  // image buffer address
u32 SFID_img_size, SFID_img_alignment;  // image buffer attribute

uint32_t dbg_model_type = 0;

/**
 * @brief cmd_parser(), uart/usb message parser
 */
#if PACKET
int cmd_parser(u8 *buf, int len)
{
	MsgHdr *msghdr;
	CmdPram *msgcmd_arg;
	RspPram *msgrsp_arg;
	OpPram *opcmdpram;
	u8 *pdata = NULL;
	u32 *pwdata = NULL;
	u8 rstatus = STS_OK;
	int wlen;
    kdp_app_mgr_t *mgr = kdp_appmgr();
    int ret;
	
	msghdr = (MsgHdr*)buf;
	msgcmd_arg = (CmdPram*)(buf + sizeof(MsgHdr));
	msgrsp_arg = (RspPram*)(buf + sizeof(MsgHdr));  // use the same rcv buffer to format responses
	opcmdpram = (OpPram*)msgcmd_arg;  // used to parse operational commands

    // set up call back event to continue after calling AppMgr
    //mgr->_tid_output = tid_host_comm;
    //mgr->_threadNotifyFlag = FLAG_HOST_COMM_APP_DONE;

	dbg_msg("cmd_parser[%d]: cmd %d [addr, len]=[0x%x, %d]\n", len, msghdr->cmd, msgcmd_arg->addr, msgcmd_arg->len);

	pdata = (u8*)(buf + (sizeof(MsgHdr) + sizeof(CmdPram)));  // points to stuff beyond error and byte count
	pwdata = (u32*)(buf + (sizeof(MsgHdr) + sizeof(CmdPram)));
	wlen = (msgcmd_arg->len % 4) == 0 ? msgcmd_arg->len / 4 : msgcmd_arg->len / 4 + 1;  // last bytes is treated as word

    /* Command parsing */
    if (msghdr->cmd < 0x100)  // build-in commands
    {        
        switch (msghdr->cmd) {
		// Test Commands
        case CMD_MEM_WRITE:
#if MSG_MEM_BYTE_WRITE
		/* NPU memory(0x30000000) doest not support byte R/W */
            if ((msgcmd_arg->addr >= NPU_PA_BASE) && (msghdr->addr < 0x40000000)) {
#else
            if (1) {
#endif
                for (int i = 0; i < wlen; i++) {
                    outw(msgcmd_arg->addr + i * 4, (u32) *(pwdata + i));
                }
            }
            else {
                for (int i = 0; i < msgcmd_arg->len; i++) {
                    outb(msgcmd_arg->addr + i, *(pdata + i));
                }
            }
            // msg byte write size is unchanged
            msgrsp_arg->error = NO_ERROR;
            msg_pack(CMD_MEM_WRITE, NULL, 0);
            break;

        case CMD_MEM_READ:
#if MSG_MEM_BYTE_WRITE
		/* NPU memory(0x30000000) doest not support byte R/W */
            if ((msgcmd_arg->addr >= NPU_PA_BASE) && (msghdr->addr < 0x40000000)) {
#else
            if (1) {
#endif
                for (int i = 0; i < wlen; i++) {
                    *(pwdata + i) = inw(msgcmd_arg->addr + 4 * i);
                }
            }
            else {
                for (int i = 0; i < msgcmd_arg->len; i++) {
                    *(pdata + i) = inb(msgcmd_arg->addr + i);
                }
            }
            // msg byte read size is unchanged
            msgrsp_arg->error = NO_ERROR;
            msg_pack(CMD_MEM_READ, (u8 *)pdata, msgcmd_arg->len);
            break;

        case CMD_TEST_ECHO:
            // msg bytes echo'ed is unchanged
            msgrsp_arg->error = NO_ERROR;
            msg_pack(CMD_TEST_ECHO, (u8 *)pdata, msgcmd_arg->len);
            break;

    	case CMD_MEM_CLR:
	    	memset((void *)msgcmd_arg->addr, 0, msgcmd_arg->len);
		    // msg clear size is unchanged
    		msgrsp_arg->error = NO_ERROR;
	    	msg_pack(CMD_MEM_CLR, NULL, 0);
		    break;

    	case CMD_USB_WRITE:
	    	;
    		u32 bytes_read;
	    	bytes_read = usb_mem_read(msgcmd_arg->addr, msgcmd_arg->len);
		    if (bytes_read == msgcmd_arg->len)
                msgrsp_arg->error = NO_ERROR;
            else
                msgrsp_arg->error = USB_ERROR;
            msgrsp_arg->bytes = bytes_read;
            msg_pack(CMD_USB_WRITE, NULL, 0);
            break;

            // Demo Commands
    	case CMD_DEMO_SET_MODEL:
	    	kdp_com_demo_set_model(pdata, msgcmd_arg->len);
		    msgrsp_arg->error = NO_ERROR;
    		msgrsp_arg->bytes = 0;
	    	msg_pack(CMD_ACK, NULL, 0);
		    break;
    	case CMD_DEMO_SET_IMAGES:
	    	kdp_com_demo_set_images(pdata, msgcmd_arg->len);
		    msgrsp_arg->error = NO_ERROR;
    		msgrsp_arg->bytes = 0;
	    	msg_pack(CMD_ACK, NULL, 0);
		    break;
    	case CMD_DEMO_RUN_ONCE:
	    	kdp_com_demo_run_once();
		    msgrsp_arg->error = NO_ERROR;
    		msgrsp_arg->bytes = 0;
	    	msg_pack(CMD_ACK, NULL, 0);
		    break;
    	case CMD_DEMO_RUN:
	    	kdp_com_demo_run(1);
		    msgrsp_arg->error = NO_ERROR;
    		msgrsp_arg->bytes = 0;
	    	msg_pack(CMD_ACK, NULL, 0);
		    break;
    	case CMD_DEMO_STOP:
	    	kdp_com_demo_run(0);
		    msgrsp_arg->error = NO_ERROR;
    		msgrsp_arg->bytes = 0;
	    	msg_pack(CMD_ACK, NULL, 0);
		    break;

    	//vincent, temporally use CMD_DEMO_FD to test app_fdr()
        case CMD_DEMO_FD:
        {
            struct fd_out_data app_data;
            mgr->run_app(2, NULL, &app_data);   /*fd */
     
            dbg_msg("    (x,y,w,h) = %d,%d,%d,%d\n", 
                    app_data.x, app_data.y,
                    app_data.w, app_data.h);

            msgrsp_arg->error = NO_ERROR;
             msgrsp_arg->bytes = 0;
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }
        case CMD_DEMO_INFERENCE:
        {
            struct fdr_in_data app_data;
            app_data.mode = 0; //inference
            
            ret = mgr->run_app(1, &app_data ,NULL); //fdr-inference in FID application
            
            dbg_msg("[INFO] host_com: fdr(inference): return=%d\n", ret);
    
            if(ret == KDP_APP_OK){
                dbg_msg("      user_id = %d\n", app_data.user_id);
            }

            msgrsp_arg->error = NO_ERROR;
    		msgrsp_arg->bytes = 0;        
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }

        case CMD_DEMO_TINY_YOLO:
        {
          struct yolo_result app_out_data;
		  dbg_msg("Received cmd: CMD_DEMO_TINY_YOLO\n");
          ret = mgr->run_app(1, NULL, &app_out_data);  // tiny yolo is 1 in obj_detc application 
          msg_pack(CMD_ACK, NULL, 0);
          break;
        }
    
        case CMD_DEMO_REG1:
        {
            struct fdr_in_data app_data;
            app_data.mode = 1; //register 1 fm
            app_data.user_id = 123; 
        
            ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application
        
            dbg_msg("[INFO] host_com: fdr(register=1, user_id=123): return=%d\n", ret);
    		msgrsp_arg->error = NO_ERROR;
	    	msgrsp_arg->bytes = 0;        
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }
    
        case CMD_DEMO_REG2:
        {
            struct fdr_in_data app_data;
            app_data.mode = 2; //register 2nd fm
            app_data.user_id = 123; 
        
            ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application
            
            //get data from kdp_appmgr()->_buf
            dbg_msg("[INFO] host_com: fdr(register=2, user_id=123): return=%d\n", ret);
		    msgrsp_arg->error = NO_ERROR;
    		msgrsp_arg->bytes = 0;        
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }
        case CMD_DEMO_REG3:
        {
            struct fdr_in_data app_data;
            app_data.mode = 3; //register 3rd fm
            app_data.user_id = 123; 
            
            ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application
            
            //get data from kdp_appmgr()->_buf
            dbg_msg("[INFO] host_com: fdr(register=3, user_id=123): return=%d\n", ret);
		    msgrsp_arg->error = NO_ERROR;
    		msgrsp_arg->bytes = 0;        
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }
        case CMD_DEMO_REG4:
        {
            struct fdr_in_data app_data;
            app_data.mode = 4; //register 3rd fm
            app_data.user_id = 123; 
            
            ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application
            
            //get data from kdp_appmgr()->_buf
            dbg_msg("[INFO] host_com: fdr(register=4, user_id=123): return=%d\n", ret);
	    	msgrsp_arg->error = NO_ERROR;
		    msgrsp_arg->bytes = 0;        
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }
        case CMD_DEMO_REG5:
        {
            struct fdr_in_data app_data;
            app_data.mode = 5; //register 3rd fm
            app_data.user_id = 123; 
            
            ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application

            dbg_msg("[INFO] host_com: fdr(register=5, user_id=123): return=%d\n", ret);
    		msgrsp_arg->error = NO_ERROR;
	    	msgrsp_arg->bytes = 0;        
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }
        case CMD_DEMO_ADDUSER:
        {
            struct fid_db_user_data app_data;
            app_data.user_id_in = 123;
            
            ret = mgr->run_app(3, &app_data ,NULL); //db_add in FID application

            dbg_msg("[INFO] host_com: db_add(user_id=123): return=%d\n", ret);
    		msgrsp_arg->error = NO_ERROR;
	    	msgrsp_arg->bytes = 0;        
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }
    
        case CMD_DEMO_DELUSER:
        {
            struct fid_db_user_data app_data;
            app_data.user_id_in = 123;
        
            ret = mgr->run_app(4, &app_data ,NULL); //db_add in FID application

            dbg_msg("[INFO] host_com: db_del(user_id=123): return=%d\n", ret);
    		msgrsp_arg->error = NO_ERROR;
	    	msgrsp_arg->bytes = 0;        
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }
        case CMD_DEMO_ABORT_REG:
        {
            ret = mgr->run_app(7, NULL ,NULL); //abort_reg in FID application
            
            dbg_msg("[INFO] host_com: abort_reg: return=%d\n", ret);
    		msgrsp_arg->error = NO_ERROR;
	    	msgrsp_arg->bytes = 0;        
            msg_pack(CMD_ACK, NULL, 0);
            break;
        }

	// Operational Commands
        case CMD_RESET:
        {
            u32 reset_mode;
		
            reset_mode = opcmdpram->op_parm1;
    		if ((reset_mode == 0) || (reset_mode == 1) || (reset_mode == 255))
	    	{
		    	if (reset_mode == (!opcmdpram->op_parm2))
			    	msgrsp_arg->error = NO_ERROR;  // not doing anything for now
    			else
	    			msgrsp_arg->error = BAD_CONFIRMATION;
		    }
		    else
			    msgrsp_arg->error = BAD_MODE;
		    msg_pack(CMD_RESET, NULL, 0);
    		break;
        }

    	case CMD_SYSTEM_STATUS:
	    	msgrsp_arg->error = DEVICE_ID;   // use error parameter for device ID
		    msgrsp_arg->bytes = FW_VERSION;  // and len parameter for firmware version
		    msg_pack(CMD_SYSTEM_STATUS, NULL, 0);
		    break;
    
    	case CMD_QUERY_APPS:
        case CMD_SELECT_APP:
	    case CMD_SET_MODE:
    	case CMD_SET_EVENTS:
	    case CMD_UPDATE:
    	case CMD_ABORT:
	    default:
		    msgrsp_arg->error = RSP_UKNOWN_CMD;
    		msgrsp_arg->bytes = 0;
	    	msg_pack(msghdr->cmd, NULL, 0);
		    break;
        }
    }
    
    // Application Specific Commands
    else if (msghdr->cmd < 0x200)  // group 1 of applications (0x100-0x1FF)
    {
        switch(msghdr->cmd)
        {
            case CMD_SFID_START:
            {
                // check if we were in ADD USER mode previously
                if (SFID_user) {
                    // abort any previous session
                    ret = mgr->run_app(7, NULL, NULL); /*abort register*/

                    if (ret != KDP_APP_OK) {
                        dbg_msg("[---SFID---] Start Abort Old Session FAILED.\r\n");
                    }
                }
                dbg_msg("[---SFID---] Start FD/FR/DB.\r\n");
                SFID_user = 0;  // switch to NORMAL FD/FR/DB MODE
                SFID_index = 0;
                // start continuous inference & DB match

                kdpapp_query_img_transfer_setting(&SFID_img_count, &SFID_img_base, &SFID_img_size, &SFID_img_alignment);
                dbg_msg("[---SFID---] START count = %d, base = %x, size = %d, alignment = %d\r\n", 
                        SFID_img_count, SFID_img_base, SFID_img_size, SFID_img_alignment);
                msgrsp_arg->error = NO_ERROR;
                msgrsp_arg->bytes = SFID_img_size; 
                msg_pack(CMD_SFID_START, NULL, 0);
                // wait for image stream
                break;
            }
            case CMD_SFID_NEW_USER:
            {
                if (SFID_user == 0) {  // switch to ADD USER MODE
                    SFID_user = msgcmd_arg->addr;
                    SFID_index = msgcmd_arg->len;
                }
                else {
                    if (SFID_user == msgcmd_arg->addr)
                        SFID_index = msgcmd_arg->len;
                    else {
                        if (msgcmd_arg->len == 1) {
                            //  abort any previous NEW USER session first
                            ret = mgr->run_app(7, NULL, NULL); /*abort register*/
                            if (ret == KDP_APP_OK) {
                                SFID_user = msgcmd_arg->addr;
                                SFID_index = 1;
                            }
                            // else, this condition shouldn't happen
                            else {
                                dbg_msg("[---SFID---] Add user abort old session %d Failed -> %d\r\n",
                                        msgcmd_arg->addr, ret);
                            // don't know how to handle this, leave to image cmd
                                SFID_user = msgcmd_arg->addr;  // force the case anyway
                            }
                        }
                        else {  // we don't send error code out anymore
                            SFID_user = msgcmd_arg->addr;  // let image cmd deal with the problem
                            SFID_index = msgcmd_arg->len;
                        }
                    }
                }
                dbg_msg("[---SFID---] Add New User %d Indx -> %d\r\n", SFID_user, SFID_index);
                msgrsp_arg->error = NO_ERROR;
                msgrsp_arg->bytes = 0;
                msg_pack(CMD_SFID_NEW_USER, NULL, -1);  // use zero payload response
                // wait for image stream
                break;
            }
            case CMD_SEND_IMAGE:
            {
                u32 image_buffer = SFID_img_base;  // use first image buffer for now
                
                struct fdr_in_data app_data;
                app_data.mode = SFID_index; //reg mode, fm index
                app_data.user_id = SFID_user;

                // get image
                ret = usb_mem_read(image_buffer, msgcmd_arg->addr);  // specify how many bytes
            
                if (ret == msgcmd_arg->addr) { // got image
                    
                    // input range check first
                    if ((SFID_user != 0) && (SFID_index != 0)) {
                        if ((SFID_user > 15) || (SFID_index > 5)) {
                            // bad user id or image index
                            msgrsp_arg->error = MSG_APP_BAD_INDEX;
                            msgrsp_arg->bytes = 0;
                            msg_pack(CMD_SEND_IMAGE, NULL, 0);
                            break;
                        }
                    }
                    // now send command to AppMgr
                    ret = mgr->run_app(1, &app_data, NULL);

                    u16* mode;
                    mode = (u16*) &msgrsp_arg->bytes;
                    if (ret == KDP_APP_OK) {
                        // got user_id or feature map stored
                        msgrsp_arg->error = NO_ERROR;
                        // we have u16 mode and u16 user_id/fm_indx in a u32 field (Little Endian)
                        if (SFID_user) {
                            *mode = 1;
                            mode++;
                            *mode = SFID_index;
                        }
                        else {
                            *mode = 0;
                            mode++;
                            *mode = app_data.user_id;
                        }
                    }
                    else { // error conditions
                        msgrsp_arg->error = MSG_APP_BAD_IMAGE;  // default case, fd/fr failure
                        if (ret == KDP_APP_DB_NO_MATCH)
                            msgrsp_arg->error = MSG_APP_DB_NO_MATCH;
                        if (ret == KDP_APP_DB_ALREADY_SAVED)
                            msgrsp_arg->error = MSG_APP_UID_EXIST;
                        if (ret == KDP_APP_DB_REG_FIRST)
                            msgrsp_arg->error = MSG_APP_IDX_FIRST;
                        if (ret == KDP_APP_DB_DIFF_UID)
                            msgrsp_arg->error = MSG_APP_UID_DIFF;
                        if (ret == KDP_APP_DB_USER_NOT_REG)
                            msgrsp_arg->error = MSG_APP_IDX_MISSING;
                        if (ret == KDP_APP_DB_NO_SPACE)
                            msgrsp_arg->error = MSG_APP_DB_FAIL;
                        if (ret == KDP_APP_FDR_WRONG_USAGE)
                            msgrsp_arg->error = MSG_APP_BAD_INDEX;
                        if (ret == KDP_APP_DB_DEL_NOT_VALID)
                            msgrsp_arg->error = MSG_APP_DB_NO_USER;
                    }
                    msg_pack(CMD_SEND_IMAGE, NULL, 0);
                }
                else { // no image
                    msgrsp_arg->error = MSG_APP_BAD_IMAGE;
                    msgrsp_arg->bytes = 0;
                    msg_pack(CMD_SEND_IMAGE, NULL, 0);
                }
                break;
            }

            case CMD_SFID_ADD_DB:
            {
                struct fid_db_user_data app_data;
                app_data.user_id_in = SFID_user;
           
                ret = mgr->run_app(3, &app_data, NULL);  //db_add

                if (ret == KDP_APP_OK) {  // success
                    msgrsp_arg->error = NO_ERROR;
                }
                else { // error conditions
                    switch (ret)
                    {
                        case KDP_APP_DB_ALREADY_SAVED:
                            msgrsp_arg->error = MSG_APP_UID_EXIST;
                            break;
                        case KDP_APP_DB_REG_FIRST:
                            msgrsp_arg->error = MSG_APP_IDX_FIRST;
                            break;
                        case KDP_APP_DB_DIFF_UID:
                            msgrsp_arg->error = MSG_APP_UID_DIFF;
                            break;
                        case KDP_APP_DB_USER_NOT_REG:
                            msgrsp_arg->error = MSG_APP_IDX_MISSING;
                            break;
                        default:
                            // all other cases, unknown failure
                            msgrsp_arg->error = MSG_APP_DB_FAIL;
                            break;
                    }
                }
                msgrsp_arg->bytes = 0;
                msg_pack(CMD_SFID_ADD_DB, NULL, 0);
                break;
            }
            case CMD_SFID_DELETE_DB:
            {
                struct fid_db_user_data app_data;
                app_data.user_id_in = msgcmd_arg->addr;
                if(msgcmd_arg->addr) 
                    app_data.del_all = FALSE;
                else
                    app_data.del_all = TRUE;

                dbg_msg("[---SFID---] DELETE USER %d\n", msgcmd_arg->addr);

                ret = mgr->run_app(4, &app_data, NULL); //db_delete

                if (ret == KDP_APP_OK) // success
                    msgrsp_arg->error = NO_ERROR;
                else {  //user id not found
                    if (ret == KDP_APP_DB_DEL_FAIL)
                        msgrsp_arg->error = MSG_APP_DB_NO_USER;
                    else
                        msgrsp_arg->error = MSG_APP_DB_FAIL;
                }
                msgrsp_arg->bytes = 0;
                msg_pack(CMD_SFID_DELETE_DB, NULL, 0);
                break;
            }
            default:
                msgrsp_arg->error = RSP_UKNOWN_CMD;
                msgrsp_arg->bytes = 0;
                msg_pack(msghdr->cmd, NULL, 0);
                break;
        }
    }

    /* rrr: temporally for db test */
    else if (msghdr->cmd >> 8 == 100) // 0x5A
    {
        struct fid_db_user_data app_user_data;

        int db_cmd = (msghdr->cmd & 0x0F);        /* db test command code */
        uint16_t para1 = (msgcmd_arg->addr) & 0xFFFF;        /* user id */
        uint16_t para2 = (msgcmd_arg->len) & 0x0F;    /* feature map index */
        switch (db_cmd) {
            case 0x1:
                dbg_msg("[INFO][host_com] db_cmd: %s, user_id: %04d\n", "add_user", para1);

                app_user_data.user_id_in = para1;
                ret = kdp_appmgr()->run_app(1, &app_user_data, 0);

                dbg_msg("[INFO][host_com] add user data to index: %d in FID database\n", app_user_data.user_idx);
                break;
            case 0x2:
                dbg_msg("[INFO][host_com] db_cmd: %s, user_id: %04d\n", "delete_user", para1);

                app_user_data.user_id_in = para1;
                ret = kdp_appmgr()->run_app(2, &app_user_data, 0);

                dbg_msg("[INFO][host_com] delete user data from index: %d in FID database\n", app_user_data.user_idx);
                break;
            case 0x3:
                dbg_msg("[INFO][host_com] db_cmd: %s, user_id: %04d, fm_idx: %d\n", "register_user", para1, para2);

                ret = kdp_app_db_register(0x70007000, para1, para2);
                break;
            case 0x4:
                dbg_msg("[INFO][host_com] db_cmd: %s, user_id: %04d, fm_idx: %d\n", "upload_user", para1, para2);

                app_user_data.user_id_in = para1;
                app_user_data.fm_idx = para2;
                ret = kdp_appmgr()->run_app(4, &app_user_data, 0);

                break;
            case 0x5:
                dbg_msg("[INFO][host_com] db_cmd: %s\n", "compare_user");

                ret = kdp_app_db_compare(0x70007000, &(app_user_data.user_id_out));

                dbg_msg("[INFO][host_com] user id: [%04d] detect!!!\n", app_user_data.user_id_out);
                break;
            case 0x6:
                dbg_msg("[INFO][host_com] db_cmd: %s\n", "list_user");

                ret = kdp_appmgr()->run_app(3, 0, 0);

                break;
            default:
                break;
        }
        msg_pack(CMD_ACK, NULL, 0);
    }
    else if (msghdr->cmd >> 8 == 0x20)
    {

#ifdef DBG_ALONE

      switch (msghdr->cmd) 
      {
         case CMD_DBG_USB_MEM_WR:
         {
            uint32_t mem_addr = (uint32_t)msgcmd_arg->addr;
            uint32_t buf_len = (uint32_t)msgcmd_arg->len;
            dbg_msg("cmd_parser: USB mem write to addr:0x%x, size:%d\n", mem_addr, buf_len);
            // get data from USB
            ret = usb_mem_read(mem_addr, buf_len); 
            if(ret == buf_len) {
              dbg_msg("cmd_parser: read USB data successfully\n");
    		  msgrsp_arg->error = NO_ERROR;
	    	  msgrsp_arg->bytes = 0; 
	    	  msg_pack(CMD_ACK, NULL, 0);
            }
            else {
              dbg_msg("Error! cmd_parser: read USB data failed\n");
    		  msgrsp_arg->error = USB_ERROR;
	    	  msgrsp_arg->bytes = 0; 
	    	  msg_pack(CMD_DBG_USB_MEM_WR, NULL, 0);
            }
            break;
         }

		 case CMD_DBG_SET_MODEL_TYPE:
		 {
            dbg_model_type = (uint32_t)msgcmd_arg->addr;
			msgrsp_arg->error = NO_ERROR;
			msgrsp_arg->bytes = 0; 
			msg_pack(CMD_ACK, NULL, 0);

			dbg_msg("cmd_parser: set model_type=%d\n", dbg_model_type);
			break;
		 }
         default:
            break;
      }
#endif

    }
    else
    {
        msgrsp_arg->error = RSP_UKNOWN_CMD;
        msgrsp_arg->bytes = 0;
        msg_pack(msghdr->cmd, NULL, 0);
    }
    return rstatus;
}
#else
int cmd_parser(u8 *buf, int len)
{    
    MsgHdr *msghdr;
    u8 *pdata = NULL;
    u32 *pwdata = NULL;
    u16 crc16;
    u8 rstatus = STS_OK;
    int wlen;
    kdp_app_mgr_t *mgr = kdp_appmgr();
    int ret;

    // set up call back event to continue after calling AppMgr
    //mgr->_tid_caller = tid_host_comm;
    //mgr->_threadNotifyFlag = FLAG_HOST_COMM_APP_DONE;    
	
    msghdr = (MsgHdr*) buf;
    pdata = (u8*) (buf + MSG_HDR_SIZE);
    pwdata = (u32*) (buf + MSG_HDR_SIZE);

    crc16 = gen_crc16((const u8*) (buf + 4), len - 4);  
    if (crc16 != msghdr->crc16) {
        /* CRC16 checksum error */
        msg_pack(CMD_CRC_ERR, NULL, 0);
        return STS_ERR_CRC;
    }

    dbg_msg("cmd_parser[%d]: cmd %d [addr, len]=[0x%x, %d]\n", len, msghdr->cmd, msghdr->addr, msghdr->len);

    wlen = (msghdr->len % 4) == 0? msghdr->len/4 :  msghdr->len/4 + 1;
    /* Command parsing */
    switch(msghdr->cmd) {
    case CMD_MEM_WRITE:
        #if MSG_MEM_BYTE_WRITE
        /* NPU memory(0x30000000) doest not support byte R/W */
        if ((msghdr->addr >= NPU_PA_BASE) && (msghdr->addr < 0x40000000)) {
        #else
        if (1) {
        #endif
            for (int i = 0; i < wlen; i++) {
                outw(msghdr->addr + i * 4, (u32) *(pwdata + i));
            }
        } else {
            for (int i = 0; i < msghdr->len; i++) {
                outb(msghdr->addr + i, *(pdata + i));
            }
        }
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_MEM_READ:
        #if MSG_MEM_BYTE_WRITE
        /* NPU memory(0x30000000) doest not support byte R/W */
        if ((msghdr->addr >= NPU_PA_BASE) && (msghdr->addr < 0x40000000)) {
        #else
        if (1) {
        #endif
            for (int i = 0; i < wlen; i++) {
                *(pwdata + i) = inw(msghdr->addr + 4 * i);
            }
        } else {
            for (int i = 0; i < msghdr->len; i++) {
                *(pdata + i) = inb(msghdr->addr + i);
            }
        }
        msg_pack(CMD_MEM_READ, (u8 *)pdata, msghdr->len);
        break;
    case CMD_TEST_ECHO:
        msg_pack(CMD_TEST_ECHO, (u8 *)pdata, msghdr->len);
        break;
    case CMD_MEM_CLR:
        memset((void *)msghdr->addr, 0, msghdr->len);
        msg_pack(CMD_ACK, NULL, 0);
        break;

    case CMD_DEMO_SET_MODEL:
        kdp_com_demo_set_model(pdata, msghdr->len);
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_SET_IMAGES:
        kdp_com_demo_set_images(pdata, msghdr->len);
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_RUN_ONCE:
        kdp_com_demo_run_once();
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_RUN:
        kdp_com_demo_run(1);
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_STOP:
        kdp_com_demo_run(0);
        msg_pack(CMD_ACK, NULL, 0);
        break;

	//vincent, test app_fdr(inference mode)
	case CMD_DEMO_TINY_YOLO:
    {
        struct yolo_result app_out_data;
        ret = mgr->run_app(1, NULL, &app_out_data);  // tiny yolo is 1 in obj_detc application 
        msg_pack(CMD_ACK, NULL, 0);
        break;
    }
    case CMD_DEMO_FD:
    {
        struct fd_out_data app_out_data;
        ret = mgr->run_app(2, NULL, &app_out_data); //fd in FID application

        if(ret == KDP_APP_OK){ 
            dbg_msg("    (x,y,w,h) = %d,%d,%d,%d\n", 
                                app_out_data.x, 
                                app_out_data.y,
                                app_out_data.w, 
                                app_out_data.h);
		}
        
        msg_pack(CMD_ACK, NULL, 0);
        break;
    }
    case CMD_DEMO_INFERENCE:
    {
        struct fdr_in_data app_data;
        app_data.mode = 0; //inference
        
        ret = mgr->run_app(1, &app_data ,NULL); //fdr-inference in FID application
       
        dbg_msg("[INFO] host_com: fdr(inference): return=%d\n", ret);

        if(ret == KDP_APP_OK){
            dbg_msg("      user_id = %d\n", app_data.user_id);
        }
        
        msg_pack(CMD_ACK, NULL, 0);
        break;
    }

    case CMD_DEMO_REG1:
    {
        struct fdr_in_data app_data;
        app_data.mode = 1; //register 1 fm
        app_data.user_id = 123; 
        
        ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application
        
        //get data from kdp_appmgr()->_buf
        dbg_msg("[INFO] host_com: fdr(register=1, user_id=123): return=%d\n", ret);

        msg_pack(CMD_ACK, NULL, 0);
        break;
    }

    case CMD_DEMO_REG2:
    {
        struct fdr_in_data app_data;
        app_data.mode = 2; //register 2nd fm
        app_data.user_id = 123; 
        
        ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application
        
        //get data from kdp_appmgr()->_buf
        dbg_msg("[INFO] host_com: fdr(register=2, user_id=123): return=%d\n", ret);

        msg_pack(CMD_ACK, NULL, 0);
        break;
    }
    case CMD_DEMO_REG3:
    {
        struct fdr_in_data app_data;
        app_data.mode = 3; //register 3rd fm
        app_data.user_id = 123; 
        
        ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application
        
        dbg_msg("[INFO] host_com: fdr(register=3, user_id=123): return=%d\n", ret);

        msg_pack(CMD_ACK, NULL, 0);
        break;
    }
    case CMD_DEMO_REG4:
    {
        struct fdr_in_data app_data;
        app_data.mode = 4; //register 3rd fm
        app_data.user_id = 123; 
        
        ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application

        dbg_msg("[INFO] host_com: fdr(register=4, user_id=123): return=%d\n", ret);

        msg_pack(CMD_ACK, NULL, 0);
        break;
    }
    case CMD_DEMO_REG5:
    {
        struct fdr_in_data app_data;
        app_data.mode = 5; //register 3rd fm
        app_data.user_id = 123; 
        
        ret = mgr->run_app(1, &app_data ,NULL); //fdr-register in FID application

        dbg_msg("[INFO] host_com: fdr(register=5, user_id=123): return=%d\n", ret);

        msg_pack(CMD_ACK, NULL, 0);
        break;
    }
    case CMD_DEMO_ADDUSER:
    {
        struct fid_db_user_data app_data;
        app_data.user_id_in = 123;
        
        ret = mgr->run_app(3, &app_data ,NULL); //db_add in FID application

        dbg_msg("[INFO] host_com: db_add(user_id=123): return=%d\n", ret);

        msg_pack(CMD_ACK, NULL, 0);
        break;
    }

    case CMD_DEMO_DELUSER:
    {
        struct fid_db_user_data app_data;
        app_data.user_id_in = 123;
        
        ret = mgr->run_app(4, &app_data ,NULL); //db_add in FID application

        dbg_msg("[INFO] host_com: db_del(user_id=123): return=%d\n", ret);

        msg_pack(CMD_ACK, NULL, 0);
        break;
    }
    case CMD_DEMO_ABORT_REG:
    {
        ret = mgr->run_app(7, NULL ,NULL); //abort_reg in FID application
        
        dbg_msg("[INFO] host_com: abort_reg: return=%d\n", ret);
    	 
        msg_pack(CMD_ACK, NULL, 0);
        break;
    }

    case CMD_STS_CLR:
    default:
        msg_pack(CMD_ACK, NULL, 0);
        break;
    }

    /* rrr: temporally for db test */
    if (msghdr->cmd >> 24 == 100) { // 0x5A
        struct fid_db_user_data app_user_data;

        int db_cmd = (msghdr->cmd >> 20) & 0x0F;        /* db test command code */
        uint16_t para1 = (msghdr->cmd) & 0xFFFF;        /* user id */
        uint16_t para2 = (msghdr->cmd >> 16) & 0x0F;    /* feature map index */
        switch (db_cmd) {
            case 0x1:
                dbg_msg("[INFO][host_com] db_cmd: %s, user_id: %04d\n", "add_user", para1);

                app_user_data.user_id_in = para1;
								ret = kdp_appmgr()->run_app(1, &app_user_data, 0);

                dbg_msg("[INFO][host_com] add user data to index: %d in FID database\n", app_user_data.user_idx);
                break;
            case 0x2:
                dbg_msg("[INFO][host_com] db_cmd: %s, user_id: %04d\n", "delete_user", para1);

                app_user_data.user_id_in = para1;
                ret = kdp_appmgr()->run_app(2, &app_user_data, 0);

                dbg_msg("[INFO][host_com] delete user data from index: %d in FID database\n", app_user_data.user_idx);
                break;
            case 0x3:
                dbg_msg("[INFO][host_com] db_cmd: %s, user_id: %04d, fm_idx: %d\n", "register_user", para1, para2);

                ret = kdp_app_db_register(0x70007000, para1, para2);
                break;
            case 0x4:
                dbg_msg("[INFO][host_com] db_cmd: %s, user_id: %04d, fm_idx: %d\n", "upload_user", para1, para2);

                app_user_data.user_id_in = para1;
                app_user_data.fm_idx = para2;
                ret = kdp_appmgr()->run_app(4, &app_user_data, 0);

                break;
            case 0x5:
                dbg_msg("[INFO][host_com] db_cmd: %s\n", "compare_user");

                ret = kdp_app_db_compare(0x70007000, &(app_user_data.user_id_out));

                dbg_msg("[INFO][host_com] user id: [%04d] detect!!!\n", app_user_data.user_id_out);
                break;
            case 0x6:
                dbg_msg("[INFO][host_com] db_cmd: %s\n", "list_user");

                ret = kdp_appmgr()->run_app(3, 0, 0);

                break;
            default:
                break;
        }
        msg_pack(CMD_ACK, NULL, 0);
    }

    return rstatus;
}
#endif

static void send_ack_to_uart()
{
    u8 ack_packet[8] = {0x35, 0x8A, 4, 0, 4, 0, 0, 0};

    for (int i = 0; i < 8; i++) {
        msg_putc(MSG_PORT, ack_packet[i]);
    }
}

u32 usb_mem_read(u32 address, u32 size)
{
    send_ack_to_uart();

    usb_com_read((u8*) address, size, FLAG_HOST_COMM_USB_DONE);
    return size;
}

static void host_comm_timer(void *arg)
{
    osThreadFlagsSet(tid_host_comm, FLAG_HOST_COMM_TIMER);
}

static void host_comm_isr(void)
{
    //fLib_printf("host_comm_isr\n");
    osThreadFlagsSet(tid_host_comm, FLAG_HOST_COMM_ISR);
    NVIC_DisableIRQ((IRQn_Type)COM_IRQ);
}

static void host_comm_thread(void *argument)
{
    int32_t flags;
    int len;
    uint8_t *rIdx = msg_rbuf;
    u8 val;
    
    fLib_SerialInit(MSG_PORT, COM_BAUD, PARITY_NONE, 0, 8, 1);
    fLib_SetSerialInt(MSG_PORT, SERIAL_IER_RLS | SERIAL_IER_DR ); // ~SERIAL_IER_TE
    fLib_SetSerialFifoCtrl(MSG_PORT, 9, 8, 1, 1);

    NVIC_SetVector((IRQn_Type)COM_IRQ, (uint32_t)host_comm_isr);
    NVIC_EnableIRQ((IRQn_Type)COM_IRQ);

    return;
    for (;;) {
        flags = osThreadFlagsWait(FLAG_HOST_COMM_TIMER | FLAG_HOST_COMM_ISR, osFlagsWaitAny, osWaitForever);
        osThreadFlagsClear(flags);

        //int idx = 0;

        if (flags & (FLAG_HOST_COMM_ISR | FLAG_HOST_COMM_TIMER)) {
            do {

                UINT32 intr = inw(UART_PORT[MSG_PORT]+SERIAL_IIR);
                UINT32 status2;

                if ((intr & 0xF) == 0x06) { // Receiver Line Status
                    //fLib_printf("Receiver Line Status SERIAL_IIR= %x\n", intr);

                    for (;;) {
                        status2 = inw(UART_PORT[MSG_PORT]+SERIAL_LSR);
                        if ((status2 & SERIAL_LSR_DR)==SERIAL_LSR_DR)
                        //if (inw(UART_PORT[MSG_PORT]+SERIAL_LSR) & SERIAL_LSR_DR)
                        {
                            //*rIdx++ = inw(UART_PORT[MSG_PORT] + SERIAL_RBR);
                            *rIdx++ = inw(UART_PORT[MSG_PORT] + SERIAL_RBR);
                            //rIdx++;
                            //fLib_printf("B fifo val= %x\n", val);
                        }
                        else
                        {
                            //fLib_printf("Receiver Line Status intr=%x status2= %x\n", intr, status2);
                            break;
                        }
                    }
                    //int lenn = rIdx - msg_rbuf;
                    //fLib_printf("Receiver Line Status rIdx - msg_rbuf= %d\n", lenn);

                    break;
                }
                else if ((intr & 0xF) == 0x04) { // Received Data Ready
                    while (msg_getc(MSG_PORT, &val, 500) == 0) {
                        *rIdx++ = val;
                    }

                    //intr = inw(UART_PORT[MSG_PORT]+SERIAL_IIR);
                    status2 = inw(UART_PORT[MSG_PORT]+SERIAL_LSR);
                    if (!(status2 & SERIAL_LSR_DR))
                    //fLib_printf("B intr= %x %d %x\n", intr, rIdx - msg_rbuf, status2);
                    //if ((intr & 0xF) == 0x01)
                    {
                        fLib_SetSerialFifoCtrl(MSG_PORT, 9, 8, 1, 1);
						msg_read(msg_rbuf, rIdx - msg_rbuf);
                        rIdx = msg_rbuf;
                    }

                    break;
                }
                else if ((intr & 0xF) == 0x0c) { // Character Reception Timeout
                    //fLib_printf("Character Reception Timeout SERIAL_IIR= %x\n", intr);

                    for (;;) {
                        if (inw(UART_PORT[MSG_PORT]+SERIAL_LSR) & SERIAL_LSR_DR)
                        {
                            *rIdx++ = inw(UART_PORT[MSG_PORT] + SERIAL_RBR);
                        }
                        else
                        {
                            //fLib_printf("Character Reception Timeout intr=%x status2= %x\n", intr, status2);
                            break;
                        }
                    }

                    int lenn = rIdx - msg_rbuf;
                    //fLib_printf("Character Reception Timeout rrIdx - msg_rbuf= %d\n", lenn);
                    fLib_SetSerialFifoCtrl(MSG_PORT, 9, 8, 1, 1);

					msg_read(msg_rbuf, lenn);
                    rIdx = msg_rbuf;

                    break;
                }
                else if ((intr & 0xF) == 0x02) { // Transmitter Holding Register Empty
                    fLib_printf("Transmitter Holding Register SERIAL_IIR= %x\n", intr);
                    len = inw(UART_PORT[MSG_PORT]+0x5C) & 0xF;
                    fLib_printf("Transmitter Holding Register A fifo bytes= %u\n", len);
                    for (;;) {
                        if (inw(UART_PORT[MSG_PORT]+SERIAL_LSR) & SERIAL_LSR_DR)
                            *rIdx++ = inw(UART_PORT[MSG_PORT] + SERIAL_RBR);
                        break;
                    }
                }
                else if ((intr & 0xF) == 0x01) { // None
//                    fLib_printf("None SERIAL_IIR= %x\n", intr);
//                    fLib_printf("None A fifo bytes= %u\n", inw(UART_PORT[MSG_PORT]+0x5C) & 0xF);
                    UINT32 fcr = inw(UART_PORT[MSG_PORT]+SERIAL_FCR);
                    fcr|=SERIAL_FCR_RXFR;
                    outw(UART_PORT[MSG_PORT]+SERIAL_FCR,fcr);
					msg_read(msg_rbuf, rIdx - msg_rbuf);
                    rIdx = msg_rbuf;

                    break;
                }
                else {
                    fLib_printf("finish= %x\n", intr);
                }
            } while (1);

            if (flags & FLAG_HOST_COMM_ISR) {
                NVIC_ClearPendingIRQ((IRQn_Type)COM_IRQ);
                NVIC_EnableIRQ((IRQn_Type)COM_IRQ);
            }
        }

        if (flags & FLAG_HOST_COMM_TIMER) {
			len = msg_read_final(msg_rbuf);
            if (len > 0) {
                fLib_printf("Get uart_len = %d\n", len);
                msg_read(msg_rbuf, len);
            }
        }
    }
}

void host_com_init(void)
{
    //osStatus_t status;

    tid_host_comm = osThreadNew(host_comm_thread, NULL, NULL);
#if 0
    status = osThreadSetPriority(tid_host_comm, osPriorityNormal);
    if (status != osOK) {
        fLib_printf("osThreadSetPriority (host comm) error: %d\n", status);
    }
#endif
}
