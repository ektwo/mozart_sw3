/*
 * Kneron Header for KDP on Mozart
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */

#ifndef KDP_COM_H
#define KDP_COM_H

#include "ipc.h"

/* KDP image structure */
struct kdp_image {
    uint32_t    image_mem_addr;
    int32_t     image_mem_len;
    int32_t     image_col;
    int32_t     image_row;
    int32_t     image_ch;
    uint32_t    image_format;
};

/* Command structure from Host */
struct demo_set_images_cmd_s {
    uint32_t    image_mem_addr;
    int32_t     image_mem_len;
    int32_t     image_col;
    int32_t     image_row;
    int32_t     image_ch;
    uint32_t    image_format;
} __attribute__((packed));

/* Response structure to Host: Unused in demo commands */
#if 0
struct demo_rsp_s {
    uint32_t    model_id;
    uint16_t    image_id;
    uint16_t    time_ms;
    uint32_t    input_count;  // # of input image
    uint32_t    output_count; // # of input images processed/output
} __attribute__((packed));
#endif

/* API */
void kdp_com_init(void);
void kdp_com_set_model(struct kdp_model *model_p, uint32_t p_model_info_idx, int32_t p_slot_idx);
void kdp_com_set_model_slot_index(uint32_t index);
int kdp_com_run_image(struct kdp_image *image_p);
void kdp_com_demo_set_model(uint8_t *pdata, int len);
void kdp_com_demo_set_images(uint8_t *pdata, int len);
void kdp_com_demo_run_once(void);
void kdp_com_demo_run(int run_demo);

struct ncpu_to_scpu* kdp_com_get_input_ptr(void);
struct scpu_to_ncpu* kdp_com_get_output_ptr(void);
#endif
