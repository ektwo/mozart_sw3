#ifndef __KDP_APP_OBJ_DET_H_
#define __KDP_APP_OBJ_DET_H_

#include <stdint.h>

int32_t kdpapp_obj_det(void* input, void* output);

int32_t kdpapp_obj_det_init(void* p_inptr, void* p_outptr);
#endif
