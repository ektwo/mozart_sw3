#include "kdpApp.h"
#include "kdpAppFID.h"
#include "kdp_app_db.h"
#include "kdpModelMgr.h"
#include "kdpAppMgr.h"
#include "kdp_com.h"
#include "scu_ipc.h"     /*for NCPU triggering */
#include "dbg.h"
#include "cmsis_os2.h"

#include "model_type.h"
#include "memxfer.h"
#include "memory.h"

enum KDP_APP_FID_DDR_ADDR {
    KDP_APP_FID_DDR_ADDR_IMAGE,
    KDP_APP_FID_DDR_ADDR_DEPTH,
    KDP_APP_FID_DDR_ADDR_FID_DB,
    KDP_APP_FID_DDR_ADDR_END,
    KDP_APP_FID_DDR_ADDR_TABLE_SIZE
};
uint32_t kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_TABLE_SIZE];

// dbg
//==============================================================================
//todo: temporally to place performance profiling code here
#ifdef LOG_ENABLE
//for profile
uint32_t t_start__;
uint32_t t_end__;  
#endif
//==============================================================================


struct kdp_app_image_info g_image_info;
//========================================================================================
//##  for test only
//========================================================================================
#if 1
//todo: debug only 
static void _test_load_image(uint32_t addr, uint32_t size);
//todo: debug only  : test image data in flash (date:0424)
static void _test_load_nir_no_need_rotation(void)      
{ 
    _test_load_image(0x3c7000, 424224);  
    g_image_info.format = 0x80000020;   //NIR raw8
    g_image_info.row = 491;
    g_image_info.col = 864;
    g_image_info.ch = 1;
    return;
}

static void _test_load_depth_no_need_rotation(void)    { _test_load_image(0x42cdf8, 424224);  return; }
static void _test_load_nir_need_rotation(void)         
{ 
    _test_load_image(0x497000, 424224);  
    g_image_info.format = 0xC0000020;   //NIR raw8
    g_image_info.row = 491;
    g_image_info.col = 864;
    g_image_info.ch = 1;
    return;
}

static void _test_load_depth_need_rotation(void)       { _test_load_image(0x4fc038, 424224);  return;}
static void _test_load_rgb_1(void)                     
{ 
    _test_load_image(0x567000, 614400);  
    g_image_info.format = 0x80000060;   //RGB 565
    g_image_info.row = 480;
    g_image_info.col = 640;
    g_image_info.ch = 3;
    return;
}

static void _test_load_rgb_2(void)                     
{ 
    _test_load_image(0x5fd000, 614400);  
    g_image_info.format = 0x80000060;   //RGB 565
    g_image_info.row = 480;
    g_image_info.col = 640;
    return;
}

/**
 * @brief temporal function: load presaved image
 * @prarm(in) addr: image source address
 * @param(in) size: size to load
 * @return N/A
 */
void _test_load_image(uint32_t addr, uint32_t size)
{
    uint32_t dest = (uint32_t)kdpapp_get_ddr_addr(kdp_app_fid_ddr_addr_table_s, KDP_APP_FID_DDR_ADDR_IMAGE);
    kdp_memxfer_module.flash_to_ddr(dest, addr, size);
    dbg_msg("[INFO] kdpAppFID: load image from flash[0x%x] to DDR[0x%x]\n", addr, dest);
    return;
}

static void _test_kdp_app_db_ddr_user_data_clear()
{
    uint32_t fid_db_addr = kdpapp_get_ddr_addr(kdp_app_fid_ddr_addr_table_s, KDP_APP_FID_DDR_ADDR_FID_DB);
    
    int i, j, k;
    
    for (i = 0; i < 15; i++) {
        outhw(fid_db_addr + 0x00 + i * 12288, 0x0);
        outhw(fid_db_addr + 0x02 + i * 12288, 0x0);
        outhw(fid_db_addr + 0x04 + i * 12288, 0x0);
        outhw(fid_db_addr + 0x06 + i * 12288, 0x0);
        outhw(fid_db_addr + 0x08 + i * 12288, 0x0);

        for (j = 0; j < 5; j++) {
            for (k = 0; k < 512; k++) {
                outw(fid_db_addr + 0x800 + i * 12288 + j * 2048 + k * 4, 0x00000000);
            }
        }
    }
    
    uint32_t total_size = FID_DB_SIZE;
    kdp_memxfer_module.ddr_to_flash(KDP_APP_FLASH_ADDR_DB, fid_db_addr, total_size);
}

static void _test_kdp_app_db_ddr_user_data_init()
{
    uint32_t fid_db_addr = kdpapp_get_ddr_addr(kdp_app_fid_ddr_addr_table_s, KDP_APP_FID_DDR_ADDR_FID_DB);
    
    int i, j, k;
    union IntFloat {int32_t i; float f;};
    union IntFloat val;

    for (i = 0; i < 10; i++) {
        outhw(fid_db_addr + 0x00 + i * 12288, 0x5aa5);        // header
        outhw(fid_db_addr + 0x02 + i * 12288, 0x0001);        // valid
        outhw(fid_db_addr + 0x04 + i * 12288, 0x1111);        // crc16
        outhw(fid_db_addr + 0x06 + i * 12288, 1000+i);        // user id
        outhw(fid_db_addr + 0x08 + i * 12288, 0x0005);        // valid fm

        /* simulate user feature map data in db */
        for (j = 0; j < 5; j++) {
            for (k = 0; k < 512; k++) {
                val.f = (float) (i * 5 * 512 + j * 512 + k) / (float)1.68;
                outw(fid_db_addr + 0x0800 + i * 12288 + j * 2048 + k * 4, val.i);
            }
        }
    }
    /* simulate user feature map data from fdr */
    uint32_t total_size = FID_DB_SIZE;
    kdp_memxfer_module.ddr_to_flash(KDP_APP_FLASH_ADDR_DB, fid_db_addr, total_size);
}
#endif
//========================================================================================



static int32_t _kdpapp_fd(void);
static int32_t _kdpapp_lm(void);
static int32_t _kdpapp_lv(void);
static int32_t _kdpapp_fr(void);

static struct facedet_result *p_fd_output_s;   //stucture defined in ipc.h
static struct landmark_result *p_lm_output_s;  //strcture defined in ipc.h
static struct fr_result *p_fr_output_s;        //strcture defined in ipc.h

/* ##########################
 * ##   private function   ##
 * ########################## */
/**
 * @brief do face dectection
 *        save FD BBox in fd_output_s
 * @return status: OK or Err code
 */
int32_t _kdpapp_fd(void)
{
    //preapre image
    //todo: suggest to pass user defined data structure to NCPU
    //struct FD_input{
    //  int row,
    //  int col,
    //  int format,
    //  int crop_top, crop_bottom, crop_left, crop_right;
    //  void* image_addr;
    //}
    int n_model_info_index = 0; // 0 is FD model
    int n_model_slot_index = 0;

    kdp_com_set_model(kdp_modelmgr()->p_model_info, n_model_info_index, n_model_slot_index);
    kdp_com_get_output_ptr()->models_type[n_model_slot_index] = KNERON_FDSMALLBOX;
    kdp_modelmgr()->load_model(n_model_info_index);

    int active_image_index = 0; //only 1 image is used
    kdp_com_get_output_ptr()->active_img_index = active_image_index;
    struct kdp_img_raw* p_raw_image = &(kdp_com_get_output_ptr()->raw_images[active_image_index]);  
    p_raw_image->state = IMAGE_STATE_ACTIVE;
    p_raw_image->seq_num = 1;
    p_raw_image->ref_idx = active_image_index;     //?? which one is correct?  active_image_index/n_model_slot_index
    p_raw_image->input_row = g_image_info.row;
    p_raw_image->input_col = g_image_info.col;
    p_raw_image->input_channel = g_image_info.ch;
    p_raw_image->format = g_image_info.format; //NIR , raw8 , sub128, no need rotate

    p_raw_image->crop_top = 0;
    p_raw_image->crop_bottom = 0;
    p_raw_image->crop_left= 0;
    p_raw_image->crop_right= 0;
	  
    p_raw_image->image_mem_addr = kdpapp_get_ddr_addr(kdp_app_fid_ddr_addr_table_s, KDP_APP_FID_DDR_ADDR_IMAGE);
    p_raw_image->image_mem_len = p_raw_image->input_row *p_raw_image->input_col;
    //for output
    p_raw_image->results[n_model_slot_index].result_mem_addr = (uint32_t)p_fd_output_s;
    p_raw_image->results[n_model_slot_index].result_mem_len = (int32_t)sizeof(struct facedet_result);

    struct kdp_img_result* p_raw_result = &(kdp_com_get_input_ptr()->img_results[active_image_index]);
    p_raw_result->seq_num = 1;
    //todo: this is duplicated information
    p_raw_result->result_mem_addr = (uint32_t)p_fd_output_s;
				
    kdp_com_set_model_slot_index(n_model_slot_index);

//    dbg_msg("[INFO] kdpApp_fd:\n");
//    dbg_msg(" active_image_index = %d\n", kdp_com_get_output_ptr()->active_img_index);
//    dbg_msg("       (row/col/ch) = %d/%d/%d\n", p_raw_image->input_row, p_raw_image->input_col, p_raw_image->input_channel);
//    dbg_msg("             format = 0x%x\n", p_raw_image->format);
//    dbg_msg("  crop(tp/bt/lf/rt) = %d/%d/%d/%d\n", p_raw_image->crop_top, p_raw_image->crop_bottom,
//                                                   p_raw_image->crop_left, p_raw_image->crop_right);
//    dbg_msg("            ref_idx = %d\n", p_raw_image->ref_idx);
//    dbg_msg("         model type = %d\n", kdp_com_get_output_ptr()->models_type[n_model_slot_index]);
//    dbg_msg("        image  addr = 0x%x\n", p_raw_image->image_mem_addr);
//    dbg_msg("        output addr = 0x%x\n", p_raw_image->results[n_model_slot_index].result_mem_addr);

    // Start time for ncpu/npu round trip
    p_raw_image->tick_start = osKernelGetTickCount();

    //trigger ncpu
    scu_ipc_trigger_to_ncpu_int();
    
    int status;
		
    //extract result 
    osThreadFlagsWait(FLAG_APPMGR_FROM_NCPU, osFlagsWaitAll, osWaitForever);

    status = kdp_com_get_input_ptr()->img_results[active_image_index].status;
    if(status != IMAGE_STATE_DONE)
        return KDP_APP_UNKNOWN_ERR;
            
    if(p_fd_output_s->xywh[2] == -1) {  //width or height == -1 means NO_FACE
        dbg_msg("[INFO] FD result: NO FACE\n");
        return KDP_APP_FDR_NO_FACE;
    }

    dbg_msg("[INFO] FD result: len=%d xywh= %d %d %d %d\n", 
                p_fd_output_s->len, 
                p_fd_output_s->xywh[0], p_fd_output_s->xywh[1], 
                p_fd_output_s->xywh[2], p_fd_output_s->xywh[3]);

    return KDP_APP_OK;
}

/**
 * @brief do land mark
 *        save 5 pts in lm_output_s
 * @return status: OK or Err code
 */
int32_t _kdpapp_lm(void)
{
    int n_model_info_index = 1; // 1 is LM model
    int n_model_slot_index = 1; //requsted by Willie

    kdp_com_set_model(kdp_modelmgr()->p_model_info, n_model_info_index, n_model_slot_index);
    kdp_com_get_output_ptr()->models_type[n_model_slot_index] = KNERON_LM_5PTS;
    kdp_modelmgr()->load_model(n_model_info_index);

    int active_image_index = 0;
    kdp_com_get_output_ptr()->active_img_index = active_image_index;
    struct kdp_img_raw* p_raw_image = &(kdp_com_get_output_ptr()->raw_images[active_image_index]);  
    p_raw_image->state = IMAGE_STATE_ACTIVE;
    //p_raw_image->seq_num = 1;
    p_raw_image->ref_idx = active_image_index;     //?? which one is correct?  active_image_index/n_model_slot_index
    //p_raw_image->input_row = 491;
    //p_raw_image->input_col = 864;
    //p_raw_image->input_channel = 1;
    //p_raw_image->format = 0x80000020; //NIR , raw8 , sub128, no need rotate
    p_raw_image->crop_top = p_fd_output_s->xywh[1];
    p_raw_image->crop_bottom = p_raw_image->input_row - p_fd_output_s->xywh[1] - p_fd_output_s->xywh[3];
    p_raw_image->crop_left= p_fd_output_s->xywh[0];
    p_raw_image->crop_right= p_raw_image->input_col - p_fd_output_s->xywh[0] - p_fd_output_s->xywh[2];
    //p_raw_image->image_mem_addr = kdpapp_get_ddr_addr(KDP_APP_FID_DDR_ADDR_IMAGE);
    //p_raw_image->image_mem_len = p_raw_image->input_row *p_raw_image->input_col;

    //for output
    p_raw_image->results[n_model_slot_index].result_mem_addr = (int32_t)p_lm_output_s;
    p_raw_image->results[n_model_slot_index].result_mem_len = (int32_t)sizeof(struct landmark_result);

    struct kdp_img_result* p_raw_result = &(kdp_com_get_input_ptr()->img_results[active_image_index]);
    p_raw_result->seq_num = 1;
    //todo: this is duplicated information
    p_raw_result->result_mem_addr = (uint32_t)p_lm_output_s;
		
    kdp_com_set_model_slot_index(n_model_slot_index);

//    dbg_msg("[INFO] kdpApp_lm:\n");
//    dbg_msg(" active_image_index = %d\n", kdp_com_get_output_ptr()->active_img_index);
//    dbg_msg("       (row/col/ch) = %d/%d/%d\n", p_raw_image->input_row, p_raw_image->input_col, p_raw_image->input_channel);
//    dbg_msg("             format = 0x%x\n", p_raw_image->format);
//    dbg_msg("  crop(tp/bt/lf/rt) = %d/%d/%d/%d\n", p_raw_image->crop_top, p_raw_image->crop_bottom,
//                                                   p_raw_image->crop_left, p_raw_image->crop_right);
//    dbg_msg("            ref_idx = %d\n", p_raw_image->ref_idx);
//    dbg_msg("         model type = %d\n", kdp_com_get_output_ptr()->models_type[n_model_slot_index]);
//    dbg_msg("        image  addr = 0x%x\n", p_raw_image->image_mem_addr);
//    dbg_msg("        output addr = 0x%x\n", p_raw_image->results[n_model_slot_index].result_mem_addr);

    // Start time for ncpu/npu round trip
    p_raw_image->tick_start = osKernelGetTickCount();  

    //trigger ncpu
    scu_ipc_trigger_to_ncpu_int();
    
    int status;
    //extract result 
    osThreadFlagsWait(FLAG_APPMGR_FROM_NCPU, osFlagsWaitAll, osWaitForever);

    status = kdp_com_get_input_ptr()->img_results[active_image_index].status;
    if(status != IMAGE_STATE_DONE)
        return KDP_APP_FDR_BAD_POSE;

    dbg_msg("[INFO] LM result:(%d,%d), (%d,%d), (%d,%d), (%d,%d), (%d,%d)\n", 
            p_lm_output_s->marks[0].x, p_lm_output_s->marks[0].y,
            p_lm_output_s->marks[1].x, p_lm_output_s->marks[1].y,
            p_lm_output_s->marks[2].x, p_lm_output_s->marks[2].y,
            p_lm_output_s->marks[3].x, p_lm_output_s->marks[3].y,
            p_lm_output_s->marks[4].x, p_lm_output_s->marks[4].y);

    return KDP_APP_OK;
}

int32_t _kdpapp_lv(void)
{
    return KDP_APP_OK;
}

/**
 * @brief do fr
 *        save feature map in fr_output_s
 * @return status: OK or Err code
 */
int32_t _kdpapp_fr(void)
{
    int n_model_info_index = 3; // 3 is FR model
    int n_model_slot_index = 2; //requsted by Willie

    kdp_com_set_model(kdp_modelmgr()->p_model_info, n_model_info_index, n_model_slot_index);
    kdp_com_get_output_ptr()->models_type[n_model_slot_index] = KNERON_FR_VGG10;
    kdp_modelmgr()->load_model(n_model_info_index);

    int active_image_index = 0;
    kdp_com_get_output_ptr()->active_img_index = active_image_index;
    struct kdp_img_raw* p_raw_image = &(kdp_com_get_output_ptr()->raw_images[active_image_index]);  
    p_raw_image->state = IMAGE_STATE_ACTIVE;
    //p_raw_image->seq_num = 1;
    //p_raw_image->ref_idx = n_model_slot_index;
    p_raw_image->ref_idx = active_image_index;     //?? which one is correct?  active_image_index/n_model_slot_index
    //p_raw_image->input_row = 491;
    //p_raw_image->input_col = 864;
    //p_raw_image->input_channel = 3;
    //p_raw_image->format = 0x80000020; //NIR , raw8 , sub128, no need rotate
	
  	//enlarge 12.5 %(by >>3) of FD BBOX
    int h_margin = p_fd_output_s->xywh[3] >> 3;
    int w_margin = p_fd_output_s->xywh[2] >> 3;
    int new_crop;
    new_crop = p_fd_output_s->xywh[1] - h_margin;
    p_raw_image->crop_top = (new_crop<0) ? 0: new_crop;

    new_crop = p_raw_image->input_row - p_fd_output_s->xywh[1] - p_fd_output_s->xywh[3] - h_margin;
    p_raw_image->crop_bottom = (new_crop<0) ? 0 : new_crop;

    new_crop = p_fd_output_s->xywh[0] - w_margin;
    p_raw_image->crop_left = (new_crop<0) ? 0 : new_crop;

    new_crop = p_raw_image->input_col - p_fd_output_s->xywh[0] - p_fd_output_s->xywh[2] - w_margin;
    p_raw_image->crop_right = (new_crop<0) ? 0 : new_crop;
	  
    //p_raw_image->image_mem_addr = kdpapp_get_ddr_addr(KDP_APP_FID_DDR_ADDR_IMAGE);
    //p_raw_image->image_mem_len = p_raw_image->input_row *p_raw_image->input_col;
    //for output
    p_raw_image->results[n_model_slot_index].result_mem_addr = (uint32_t)p_fr_output_s;
    p_raw_image->results[n_model_slot_index].result_mem_len = (int32_t)sizeof(struct fr_result);

    struct kdp_img_result* p_raw_result = &(kdp_com_get_input_ptr()->img_results[active_image_index]);
    //p_raw_result->seq_num = 1;
    //todo: this is duplicated information
    p_raw_result->result_mem_addr = (uint32_t)p_fr_output_s;

    kdp_com_set_model_slot_index(n_model_slot_index);

//    dbg_msg("[INFO] kdpApp_fr:\n");
//    dbg_msg(" active_image_index = %d\n", kdp_com_get_output_ptr()->active_img_index);
//    dbg_msg("       (row/col/ch) = %d/%d/%d\n", p_raw_image->input_row, p_raw_image->input_col, p_raw_image->input_channel);
//    dbg_msg("             format = 0x%x\n", p_raw_image->format);
//    dbg_msg("  crop(tp/bt/lf/rt) = %d/%d/%d/%d\n", p_raw_image->crop_top, p_raw_image->crop_bottom,
//                                                   p_raw_image->crop_left, p_raw_image->crop_right);
//    dbg_msg("            ref_idx = %d\n", p_raw_image->ref_idx);
//    dbg_msg("         model type = %d\n", kdp_com_get_output_ptr()->models_type[n_model_slot_index]);
//    dbg_msg("        image  addr = 0x%x\n", p_raw_image->image_mem_addr);
//    dbg_msg("        output addr = 0x%x\n", p_raw_image->results[n_model_slot_index].result_mem_addr);

    // Start time for ncpu/npu round trip
    p_raw_image->tick_start = osKernelGetTickCount();  

    //trigger ncpu
    scu_ipc_trigger_to_ncpu_int();
    
    int status;
    //extract result 
    osThreadFlagsWait(FLAG_APPMGR_FROM_NCPU, osFlagsWaitAll, osWaitForever);

    status = kdp_com_get_input_ptr()->img_results[active_image_index].status;
    if(status != IMAGE_STATE_DONE)
        return KDP_APP_FDR_FR_FAIL;
        
    dbg_msg("[INFO] FR result : first 5 data\n");
    dbg_msg("   %f,%f,%f,%f,%f\n", p_fr_output_s->feature_map[0],
                                   p_fr_output_s->feature_map[1], 
                                   p_fr_output_s->feature_map[2], 
                                   p_fr_output_s->feature_map[3], 
                                   p_fr_output_s->feature_map[4]);

    return KDP_APP_OK;
}

/* #########################
 * ##   public function   ##
 * ######################### */
/**
 * @brief do face detection
 * @param(in) n/a
 * @param(out) outputPtr:  struct fd_out_data{}
 * @return OK or Err code
 */
int32_t kdpapp_fd(void* inputPtr, void* outputPtr)
{
    struct fd_out_data *out = (struct fd_out_data*)outputPtr;

    //allocate ddr for ncpu output
    kdp_ddr_init(kdpapp_get_ddr_addr(kdp_app_fid_ddr_addr_table_s, KDP_APP_FID_DDR_ADDR_END));
    if(p_fd_output_s == 0)
        p_fd_output_s = (struct facedet_result*)kdp_ddr_malloc(sizeof(struct facedet_result));


    //todo: scpu_comm_thread in ncpu will check CMD_RUN_CMD 
    kdp_com_get_output_ptr()->cmd = CMD_RUN_NPU;    


    //run NN flow
    int32_t ret;
    dbg_profile_start();
    ret = _kdpapp_fd();
    if(ret != KDP_APP_OK){
        return ret;
    }
    dbg_profile_stop("kdpApp_fd");  

    out->x = p_fd_output_s->xywh[0];
    out->y = p_fd_output_s->xywh[1];
    out->w = p_fd_output_s->xywh[2];
    out->h = p_fd_output_s->xywh[3];

    return ret;
}

/**
 * @brief do face recognition for inference or register
 * @param inputPtr points 
 *          { mode;
 *            user_id; //as input (or ouput) }
 *        mode = 0   : capture + fd/fr + 1:n compare
 *        mode = 1-5 : capture + fd/fr + register in DDR DB
 *        outputPtr: N/A
 * @return OK or Err code
 */
int32_t kdpapp_fdr(void* inputPtr, void* outputPtr)
{
    struct fdr_in_data *inout = (struct fdr_in_data*)inputPtr;

    //prepare model information to kdp_com
    kdp_model_mgr_t *mgr = 0;
    mgr = kdp_modelmgr();
    if(mgr == 0)
    {
        dbg_msg("[FATAL] model manager allocation failed\n");
        return KDP_APP_UNKNOWN_ERR;
    }   

    //allocate ddr for ncpu output
    kdp_ddr_init(kdpapp_get_ddr_addr(kdp_app_fid_ddr_addr_table_s, KDP_APP_FID_DDR_ADDR_END));
    if(p_fd_output_s == 0)
        p_fd_output_s = (struct facedet_result*)kdp_ddr_malloc(sizeof(struct facedet_result));

    if(p_lm_output_s == 0)
        p_lm_output_s = (struct landmark_result*)kdp_ddr_malloc(sizeof(struct landmark_result));

    if(p_fr_output_s == 0)
        p_fr_output_s = (struct fr_result*)kdp_ddr_malloc(sizeof(struct fr_result));
    
    //todo: scpu_comm_thread in ncpu will check CMD_RUN_CMD 
    kdp_com_get_output_ptr()->cmd = CMD_RUN_NPU;    


    //run NN flow
    int32_t ret;
    dbg_profile_start();
    ret = _kdpapp_fd();
    if(ret != KDP_APP_OK){
        return ret;
    }
    dbg_profile_stop("kdpApp_fd");  

    dbg_profile_start();
    ret = _kdpapp_lm();
    if(ret != KDP_APP_OK){
        return ret;
    }
    dbg_profile_stop("kdpApp_lm");  

    ret = _kdpapp_lv();
    if(ret != KDP_APP_OK){
        return ret;
    }

    dbg_profile_start();
    ret = _kdpapp_fr();
    if(ret != KDP_APP_OK){
        return ret;
    }
    dbg_profile_stop("kdpApp_fr");  

    if(inout->mode == 0) {
        dbg_profile_start();
        ret = kdp_app_db_compare((unsigned long)p_fr_output_s, &(inout->user_id));
        dbg_profile_stop("kdp_app_db_compare");  
    } 
    else if (inout->mode > MAX_FID){
        dbg_msg("[ERR] fm_idx > 5\n");
        return KDP_APP_FDR_WRONG_USAGE;
    }
    else{ //mode == 1 ~ MAX_FID
        dbg_profile_start();
        ret = kdp_app_db_register((unsigned long)p_fr_output_s, inout->user_id, inout->mode);
        dbg_profile_stop("kdp_app_db_register");  
    }

    return ret;
}


/**
 * @brief get ddr address for reqeust saved data
 * @return address of DDR
 */
/**
 * @brief  init necessary information
 * @return ok/fail
 */
int32_t kdpapp_init(void* p_inptr, void* p_outptr)
{
    int32_t ret = 0;
    ret = kdp_memxfer_module.init(MEMXFER_OPS_DMA/*for flash src*/, MEMXFER_OPS_DMA/*for DDR src*/);
    if( 0 != ret)
        return 1; //error
    
    //load models
    dbg_profile_start();
    kdp_modelmgr()->load_model(-1); //load all models
    dbg_profile_stop("Load model");
    
    kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_IMAGE]    = kdp_modelmgr()->l_ddr_addr_model_end;
    kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_DEPTH]    = kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_IMAGE] + IMAGE_SIZE;
    kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_FID_DB]   = kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_DEPTH] + DEPTH_SIZE;
    kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_END]      = kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_FID_DB] + FID_DB_SIZE;

//    dbg_msg("[DBG] ddr_addr_FID_DB = 0x%x\n", kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_FID_DB]);
//    dbg_msg("[DBG] ddr_addr_IMAGE  = 0x%x\n", kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_IMAGE]);
//    dbg_msg("[DBG] ddr_addr_DEPTH  = 0x%x\n", kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_DEPTH]);
//    dbg_msg("[DBG] ddr_addr_END    = 0x%x\n", kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_END]);

    //pre-allocate DDR resource for NCPU
    kdp_ddr_init(kdpapp_get_ddr_addr(kdp_app_fid_ddr_addr_table_s, KDP_APP_FID_DDR_ADDR_END));
    uint32_t mem_len2 = 0x100000;
    uint8_t *mem_addr2 = (uint8_t*)kdp_ddr_malloc(sizeof(uint8_t)*mem_len2);
    kdp_com_get_output_ptr()->input_mem_addr2 = (uint32_t)mem_addr2;
    kdp_com_get_output_ptr()->input_mem_len2 = mem_len2;
    

    
    //load image
    //todo:load image from camera
    dbg_profile_start();
    _test_load_rgb_1();
    //_test_load_nir_no_need_rotation();
    dbg_profile_stop("Load image");
    
    //copy FID database data from flash to DDR
    kdp_memxfer_module.flash_to_ddr(kdp_app_fid_ddr_addr_table_s[KDP_APP_FID_DDR_ADDR_FID_DB], KDP_APP_FLASH_ADDR_DB, FID_DB_SIZE);
    
    //register application callback
    /* func id = 1 "inference or register */ 
    kdp_appmgr()->register_app(KDP_APPMGR_CB_TYPE_NPU, "inference",     &kdpapp_fdr);
    /* func id = 2 "face detection "*/
    kdp_appmgr()->register_app(KDP_APPMGR_CB_TYPE_NPU, "FD",            &kdpapp_fd);

    //------------------------------------------------
    
    /* func id = 3 "Store feature map data in DDR to flash", */
    kdp_appmgr()->register_app(KDP_APPMGR_CB_TYPE_CPU, "update db",     &kdp_app_db_add);

    /* func id = 4 "Delete user feature data from database in flash", */
    kdp_appmgr()->register_app(KDP_APPMGR_CB_TYPE_CPU, "del user",      &kdp_app_db_delete);

    /* func id = 5 "Generate user id list on the FID database", */
    kdp_appmgr()->register_app(KDP_APPMGR_CB_TYPE_CPU, "gen id list",   &kdp_app_db_list);

    /* func id = 6 "Return the feature map of a user from database", */
    kdp_appmgr()->register_app(KDP_APPMGR_CB_TYPE_CPU, "get fm",        &kdp_app_db_upload);

    /* func id = 7 "abort register flow", */
    kdp_appmgr()->register_app(KDP_APPMGR_CB_TYPE_CPU, "abort reg",     &kdp_app_db_abort_reg);
    
    return 0;
}

#if 0
int32_t kdpapp_test_img(void* p_inptr, void* p_outptr){
    int32_t ret = 0;
    ret = kdp_memxfer_module.init(MEMXFER_OPS_DMA, MEMXFER_OPS_DMA);
    dbg_msg("before init\n");
    //ret = kdp_memxfer_module.init(MEMXFER_OPS_DMA, MEMXFER_OPS_DMA);
    dbg_msg("after init: %d\n", ret);
    if( 0 != ret)
        return 0;
    dbg_msg("after init: %d\n", ret);
    _test_load_rgb_1();
    return 0;
}

int32_t kdpapp_test_dma_flash_to_ddr(uint32_t ddr_dest_addr, uint32_t flash_src_offset, uint32_t size){
    int32_t ret = 0;
    ret = kdp_memxfer_module.init(MEMXFER_OPS_DMA, MEMXFER_OPS_DMA);

    if( 0 != ret)
        return 0;
    kdp_memxfer_module.flash_to_ddr(ddr_dest_addr, flash_src_offset, size);
    fLib_printf("finish kdp_memxfer_module.flash_to_ddr\n");
    return 0;
}

int32_t kdpapp_test_dma_ddr_to_flash(uint32_t ddr_src_addr, uint32_t flash_dest_offset, uint32_t size){
    int32_t ret = 0;
    ret = kdp_memxfer_module.init(MEMXFER_OPS_DMA, MEMXFER_OPS_DMA);

    if( 0 != ret)
        return 0;
    
    kdp_memxfer_module.ddr_to_flash(flash_dest_offset, ddr_src_addr, size);
    fLib_printf("finish kdp_memxfer_module.ddr_to_flash\n");
    return 0;
}
#endif
