#ifndef __V2K_CAM_H__
#define __V2K_CAM_H__


#include <framework/v2k.h>

#define KDP_CAMERA_START_WITH_PREVIEW 0x00000001L

int v2k_camera_init(void);

/* 
 * Sample code that shows how to register a call back function that 
 * captures a v2k_cam image.                                        
 *
static void sample_on_newimage(
    struct v2k_buffer *buf, void *user_start, unsigned int user_length) 
{
    static int m_frame_number = 0;
    DSG("<%s> buffer[%d]: type=%x start=0x%x len=%u frames=%d ts=%llu", 
        __func__, 
        buf->index,
        buf->type, 
        (unsigned int)user_start, 
        user_length,
        ++m_frame_number,
        buf->timestamp);
}
register_v2k_cam_newimage(sample_on_newimage);
*/
typedef void (*fn_v2k_cam_newimage)(
    struct v2k_buffer *, void *user_start, unsigned int user_length);
void register_v2k_cam_newimage(fn_v2k_cam_newimage fn);

unsigned int kdp_camera_get_buffer_cnt(void);
int kdp_camera_capture_frame(unsigned int *addr, unsigned int *size);



/**
 * @brief Create a camera instance to access a hardware camera.
 * @return 0,       if kdp_camera_open was successfully, or
 *         others,  if an error occurred
 */
int kdp_camera_open(void);
/**
 * @brief Release the camera instance.
 * @return 0,       if kdp_camera_close was successfully, or
 *         others,  if an error occurred
 */
int kdp_camera_close(void);
/**
 * @brief Start capturing.
 * @param flags KDP_CAMERA_START_WITH_PREVIEW, capture and preview frames to lcdc 
 * @return 0,       if kdp_camera_start was successfully, or
 *         others,  if an error occurred
 */
int kdp_camera_start(unsigned int flags);
/**
 * @brief Stop capturing.
 * @return 0,       if kdp_camera_stop was successfully, or
 *         others,  if an error occurred
 */
int kdp_camera_stop(void);
/**
 * @brief Prepare a buffer from camera sdk.
 * @return 0,       if kdp_camera_buffer_prepare was successfully, or
 *         others,  if an error occurred
 */
int kdp_camera_buffer_prepare(void);
/**
 * @brief Capture a buffer to the caller or application.
 * @return 0,       if kdp_camera_buffer_capture was successfully, or
 *         others,  if an error occurred
 */
int kdp_camera_buffer_capture(unsigned int *addr, unsigned int *size);



#endif
