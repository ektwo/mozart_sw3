#include <stdint.h>
#include <stdio.h>
#include "../../common/include/base.h"
#include "../../common/include/dbg.h"

#define CRC16_CONSTANT 0x8005

u16 gen_crc16(u8 *data, u16 size)
{
	u16 out = 0;
	int bits_read = 0, bit_flag, i;

	/* Sanity check: */
	if (data == NULL)
		return 0;

	while (size > 0)
	{
		bit_flag = out >> 15;

		/* Get next bit: */
		out <<= 1;
		out |= (*data >> bits_read) & 1; // item a) work from the least significant bits

										 /* Increment bit counter: */
		bits_read++;
		if (bits_read > 7)
		{
			bits_read = 0;
			data++;
			size--;
		}

		/* Cycle check: */
		if (bit_flag)
			out ^= CRC16_CONSTANT;

	}

	// push out the last 16 bits
	for (i = 0; i < 16; ++i) {
		bit_flag = out >> 15;
		out <<= 1;
		if (bit_flag)
			out ^= CRC16_CONSTANT;
	}

	// reverse the bits
	u16 crc = 0;
	i = 0x8000;
	int j = 0x0001;
	for (; i != 0; i >>= 1, j <<= 1) {
		if (i & out) crc |= j;
	}

	return crc;
}

#if 0
int main()
{
    u16 crc16;
    u8 buf[]={11, 22, 33, 44, 55, 66, 77, 88, 99, 100, 111, 122, 133, 144, 155};
    crc16 = gen_crc16((const char*) buf, sizeof(buf));
    printf("CRC test code\n");
    printf("crc16 = %d\n", crc16);
}
#endif
