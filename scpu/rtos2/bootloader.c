#include "bootloader.h"
#include "kneron_mozart.h" 
#include "memory.h"

int bootloader_handler(void) {
#define WHOAMI_KEY 0x12345678    
#define WHOAMI_ADDR (S_D_RAM_ADDR + SdRAM_MEM_SIZE - 16)
    unsigned int flag = (*(volatile unsigned int *)(WHOAMI_ADDR));
    if (WHOAMI_KEY == flag)
        return 1;
    
    return 0;
}