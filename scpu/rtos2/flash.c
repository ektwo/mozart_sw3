#include "flash.h"
#include <string.h>
#include "cmsis_os2.h"
#include "DrvPWMTMR010.h"
#include "Driver_Flash.h"
#include "dbg.h"

#define TEST_FLASH_BLOCK_SIZE   256
#define COMPARE_FLASH_DATA      TRUE


#if 0
//#define FLASH_TIMER             DRVPWMTMR4
//#define FLASH_GET_TICK          fLib_CurrentT4Tick
//#define FLASH_TIMER             DRVPWMTMR3
//#define FLASH_GET_TICK          fLib_CurrentT3Tick
#define FLASH_TIMER             DRVPWMTMR2
#define FLASH_GET_TICK          fLib_CurrentT2Tick
#else
#define FLASH_TIMER             DRVPWMTMR1
#define FLASH_GET_TICK          fLib_CurrentT1Tick
#endif

ARM_Flash_SignalEvent_t m_event;
extern ARM_DRIVER_FLASH Driver_Flash;

osThreadId_t tid_flash;

static void flash_thread(void *argument)
{
    int loopCnt;
    int oldT, newT;    
    int32_t bResultComp = TRUE;
    ARM_FLASH_INFO *pInfo;
	uint8_t wbuf[TEST_FLASH_BLOCK_SIZE];
    uint8_t rbuf[TEST_FLASH_BLOCK_SIZE];
    
    ARM_DRIVER_FLASH *pDrvFlash = &Driver_Flash;

    fLib_Timer_Init(FLASH_TIMER, PWMTMR_1MSEC_PERIOD);
    //fLib_printf("0%x\n", inw(0xC2380018));
    //fLib_printf("0%x\n", inw(0xC2380024));
    DSG(1, "------ test_flash ------");

    pDrvFlash->Initialize(m_event);
    DSG(1, "------ test_flash 2------");
    pDrvFlash->GetVersion();
    
    pInfo = pDrvFlash->GetInfo();
    loopCnt = pInfo->flash_size / TEST_FLASH_BLOCK_SIZE;
    DSG(1, "0. flash size=%u loopCnt=%u\n", pInfo->flash_size, loopCnt);
    #if 1
    DSG(1, "1. erase flash -- start\n");
    oldT = osKernelGetTickCount();//m_flashTick;
    pDrvFlash->EraseChip();
    newT = osKernelGetTickCount();//m_flashTick;
    DSG(1, "1. erase flash -- done (took %u-%u=%u ms)\n", newT, oldT, newT - oldT);
        
    for(int i = 0; i < TEST_FLASH_BLOCK_SIZE; i++)
        wbuf[i] = i;

	DSG(1, "2. write flash -- start\n");
    oldT = FLASH_GET_TICK();
    for (int i = 0; i < loopCnt; ++i) {
        //for (int j = 0; j < TEST_FLASH_BLOCK_SIZE; ++j) 
        {
            pDrvFlash->ProgramData(TEST_FLASH_BLOCK_SIZE * i, wbuf, TEST_FLASH_BLOCK_SIZE);
        }
    }
    newT = osKernelGetTickCount();    
	DSG(1, "2. write flash -- done (took %u-%u=%u ms)\n", newT, oldT, newT - oldT);
	    #endif
	memset(rbuf, 0, 256);

    DSG(1, "3. read flash -- start");
    oldT = osKernelGetTickCount();
    for (int i = 0; i < loopCnt; ++i) {
        //for (int j = 0; j < TEST_FLASH_BLOCK_SIZE; ++j) 
        {
            pDrvFlash->ReadData(TEST_FLASH_BLOCK_SIZE * i, rbuf, TEST_FLASH_BLOCK_SIZE);	
        }
    #ifdef COMPARE_FLASH_DATA        
        for (int j = 0; j < TEST_FLASH_BLOCK_SIZE; ++j) {
            if (rbuf[j] != j)
                bResultComp = FALSE;
                break;
        }
    #endif        
    }

    newT = osKernelGetTickCount();//m_flashTick;      
#ifdef COMPARE_FLASH_DATA        
    if (bResultComp) {
        DSG(1, "3. read flash + compare -- PASSED"); 
    }
    else {
        DSG(1, "3. read flash + compare -- FAILED");
    }
#endif    
	DSG(1, "3. read flash -- done (took %u-%u=%u ms)", newT, oldT, newT - oldT);
    
    //fLib_Timer_Close(FLASH_TIMER);    
}

void test_flash(void)
{
    tid_flash = osThreadNew(flash_thread, NULL, NULL);
    
    osDelay(osWaitForever);
    /* Idle */
    while(1);
}
