#include "kdpFlashUtil.h"

char* kdp_flash_read(char* dest, FLADDR src, uint32_t numbytes)
{
    return 0;
}
    
void kdp_flash_write(FLADDR dest, char* src, uint32_t numbytes)
{
    return;
}

void kdp_flash_clear(FLADDR dest, uint32_t numbytes)
{
    return;
}

void kdp_flash_update(FLADDR dest, char* src, uint32_t numbytes)   //combine clear + write
{
    return;
}

void kdp_flash_fill(FLADDR dest, uint32_t numbytes, char fill)
{
    return;
}
