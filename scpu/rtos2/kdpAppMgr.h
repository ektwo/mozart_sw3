#ifndef __APP_MGR_H
#define __APP_MGR_H

#include <stdint.h>
#include <stddef.h>
#include "cmsis_os2.h"
#include "base.h"
#include "types.h"


#define KDP_APPMGR_MAX_CB_COUNT 10

/* thread flags */
//==============================================================================

#define FLAG_APPMGR_FROM_NCPU  BIT(0) 
#define FLAG_APPMGR_RUN_APP    BIT(1)
extern osThreadId_t tid_appmgr;

/* enum types definition */
//==============================================================================

//callback function mode
typedef enum _KDP_APPMGR_CB_TYPE_E {
    KDP_APPMGR_CB_TYPE_CPU,     //callback run on CPU
    KDP_APPMGR_CB_TYPE_NPU,     //callback run on NPU
} kdp_appmgr_cb_type_e;

//usage mode
typedef enum _KDP_APPMGR_CONFIG_MODE_E {
    KDP_APPMGR_CONFIG_STANDALONE,
    KDP_APPMGR_CONFIG_COMPANION
} kdp_appmgr_config_mode_e;

//callback function generic form
typedef int32_t (* kdp_appmgr_cb_fn_t)(void* input_data, void* output_data);

/* brief Application Manager Interface */
//==============================================================================

typedef struct _KDP_APP_MGR {
    int32_t         (*register_app)      (kdp_appmgr_cb_type_e, char*, 
                                          kdp_appmgr_cb_fn_t);          ///< Pointer to \ref kdp_appmgr_RegisterApp : load model(index)
    int32_t         (*run_app)           (uint32_t, void*, void*);      ///< Pointer to \ref kdp_appmgr_run_App : run callback function
    int32_t         (*get_app_count)     (void);                        ///< Pointer to \ref kdp_appmgr_get_app_count :
#if DEBUG
    void            (*dump_all_app)      (void);                        ///< Pointer to \ref kdp_appmgr_DumpApp : dump callback info
#endif
} kdp_app_mgr_t;

/**
 * @brief To get application controller
 * @return : kdp_app_mgr_t* : application manager controller
 */
kdp_app_mgr_t*  kdp_appmgr(void);                                       /// get singleton application manager

/**
 * @brief To init kdp application manager
 * @param (in) app_init_func: application init call back function pointer 
 * @param (in) tid_caller   : thread ID which communicate with application manager
 * @param (in) notify_flag  : flag used to notify caller thread 
 * @param (in) usage_mode   : standalone/companion mode
 * @return : NA
 */
void            kdp_appmgr_init(kdp_appmgr_cb_fn_t  app_init_func,                     /// init function
                                osThreadId_t        tid_caller, 
                                uint32_t            notify_flag, 
                                kdp_appmgr_config_mode_e usage_mode);

#endif

