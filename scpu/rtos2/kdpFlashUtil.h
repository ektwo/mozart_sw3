#ifndef __KDP_FLASH_UTIL_H
#define __KDP_FLASH_UTIL_H

#include <stdint.h>
#define FLADDR uint32_t

char* kdp_flash_read(char* dest, FLADDR src, uint32_t numbytes);
void kdp_flash_write(FLADDR dest, char* src, uint32_t numbytes);
void kdp_flash_clear(FLADDR dest, uint32_t numbytes);
void kdp_flash_update(FLADDR dest, char* src, uint32_t numbytes);   //combine clear + write
void kdp_flash_fill(FLADDR dest, uint32_t numbytes, char fill);


#endif
