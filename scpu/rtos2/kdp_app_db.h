#ifndef __KDP_APP_DB_H_
#define __KDP_APP_DB_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "DrvUART010.h"
#include "DrvDMAC020.h"
#include "cmsis_os2.h"
#include "Driver_Common.h"
#include "DrvPWMTMR010.h"
#include "kneron_mozart.h"
#include "base.h"
#include "crc.h"
#include "com.h"
#include "io.h"
#include "string.h"
#include "ddr.h"
#include "msg.h"
#include "dbg.h"
#include "kdpApp.h"


#define MAX_USER                  15
#define MAX_FID                   5
#define INFO_SIZE_BYTE            2048  /* including header, valid, user id, valid feature map ... info. */
#define FM_SIZE                   512
#define FM_SIZE_BYTE              (FM_SIZE * 4)
#define USER_DATA_SIZE            12288 /* (2KB + 2KB * 5) for one user */

#define FID_HEADER                0x5AA5

#define HEAD_OFFSET               0
#define VALID_OFFSET              2
#define CRC16_OFFSET              4
#define ID_OFFSET                 6
#define VALID_FM_OFFSET           8
#define FM_OFFSET                 2048 /* header/valid/id/valid fm/... */

/* TODO: confirm threshold = ? */
#define FID_THRESHOLD             1.264

#define DDR_USER_BASE_ADDR        (kdp_modelmgr()->l_ddr_addr_model_end + KDP_APP_IMG_SIZE*2)
#define FLASH_USER_BASE_ADDR      KDP_APP_FLASH_ADDR_DB

#define DDR_USER_HEAD_ADDR(i)     (DDR_USER_BASE_ADDR + (i) * USER_DATA_SIZE + HEAD_OFFSET)
#define DDR_USER_VALID_ADDR(i)    (DDR_USER_BASE_ADDR + (i) * USER_DATA_SIZE + VALID_OFFSET)
#define DDR_USER_ID_ADDR(i)       (DDR_USER_BASE_ADDR + (i) * USER_DATA_SIZE + ID_OFFSET)
#define DDR_USER_VALID_FM_ADDR(i) (DDR_USER_BASE_ADDR + (i) * USER_DATA_SIZE + VALID_FM_OFFSET)
#define DDR_USER_FM_ADDR(i, j)    (DDR_USER_BASE_ADDR + (i) * USER_DATA_SIZE + (j) * 4 * FM_SIZE + FM_OFFSET)

#define FLASH_USER_HEAD_ADDR(i)   (FLASH_USER_BASE_ADDR + (i) * USER_DATA_SIZE + HEAD_OFFSET)

typedef enum {
    TYPE_INVALID = 0,
    TYPE_VALID,
    TYPE_REGISTER,
} TYPE_VALID_FLAG;

//==============================================================================
//  release to kdpApp.h
//==============================================================================
enum _KDP_APP_OBJ_STATUS_CODE_E {
    KDP_APP_DB_NO_SPACE = 0x200,   //kdpApp.h
    KDP_APP_DB_ALREADY_SAVED,
    KDP_APP_DB_DEL_NOT_VALID,
    KDP_APP_DB_NO_MATCH,
    KDP_APP_DB_REG_FIRST,
    KDP_APP_DB_USER_NOT_REG,
    KDP_APP_DB_DEL_FAIL
};

struct fid_db_user_data {
    uint16_t user_id_in;
    uint16_t user_id_out;
    uint16_t user_idx;
    uint16_t fm_idx;
    uint16_t del_all;
};

int32_t kdp_app_db_compare(uint32_t fdr_addr, uint16_t *user_id/*output*/);
int32_t kdp_app_db_register(uint32_t fdr_addr, uint16_t user_id, uint16_t fm_idx);

int32_t kdp_app_db_add(void* input, void* output);
int32_t kdp_app_db_delete(void* input, void* output);
int32_t kdp_app_db_list(void* input, void* output);
int32_t kdp_app_db_upload(void* input, void* output);
int32_t kdp_app_db_abort_reg(void* input, void* output);

#endif /* __KDP_APP_DB_H_ */
