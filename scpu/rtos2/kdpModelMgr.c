#include "kdpModelMgr.h"
#include <stdlib.h>
#include <string.h>
#include "dbg.h"
#include "memory.h"
#include "memxfer.h"     /*for flash access */

#define DEBUG 0

extern const struct s_kdp_memxfer kdp_memxfer_module;

void kdp_modelmgr_set_model_from_host(void);
int32_t kdp_modelmgr_load_model(int8_t model_info_index_p);
#if DEBUG
void kdp_modelmgr_dump_model_info(void);
#endif

static int32_t _kdp_modelmgr_load_model_info(kdp_model_mgr_t* mgr);
static int32_t _kdp_modelmgr_load_model(kdp_model_mgr_t* mgr, uint8_t model_index_p/*starts from 0*/);



kdp_model_mgr_t* kdp_modelmgr(void)
{
    static kdp_model_mgr_t mgr_singleton;
    static kdp_model_mgr_t* p_mgr_singleton = 0;

    if(0 == p_mgr_singleton)
    {
        *(uint32_t*)(0x63900000) = 0; //trick: we will check the work to see if model_info is uploaded
       
        p_mgr_singleton = &mgr_singleton;

#if DEBUG
        p_mgr_singleton->dump_all_model     = kdp_modelmgr_dump_all_model;
#endif
        p_mgr_singleton->load_model         = kdp_modelmgr_load_model;
        p_mgr_singleton->set_model_from_host= kdp_modelmgr_set_model_from_host;


        p_mgr_singleton->p_model_info  = 0;
        p_mgr_singleton->n_model_count = 0;
        p_mgr_singleton->l_ddr_addr_model_end= 0;
    }

    return p_mgr_singleton;
}

//private functions
int32_t _kdp_modelmgr_load_model_info(kdp_model_mgr_t* mgr) 
{
	uint32_t offset;

    if(mgr->m_is_model_from_host) {
				//uint32_t base_addr = KDP_DDR_BASE_MOZART;
			  uint32_t base_addr = 0x63900000;
        if( *(uint32_t*)(base_addr) == 0) 
            return 0; //error, model_info is not ready

        //get model count
        mgr->n_model_count = *(uint32_t*)(base_addr);
        dbg_msg("[DBG] model info: model count:%d\n", mgr->n_model_count);
        
        //get model info
        mgr->p_model_info = (kdp_model_info_t*)malloc(mgr->n_model_count * sizeof(struct kdp_model));
        memcpy(mgr->p_model_info, (const void*)(base_addr+4), sizeof(kdp_model_info_t)*mgr->n_model_count);

        //get ddr model end addr
        offset = sizeof(kdp_model_info_t) * mgr->n_model_count;
        mgr->l_ddr_addr_model_end = *(uint32_t*)(base_addr + 4 + offset);
        dbg_msg("[DBG] modelInfo: DDR end address: 0x%x\n", mgr->l_ddr_addr_model_end);
    }
    else {
        //get model count
        kdp_memxfer_module.flash_to_ddr((uint32_t)&(mgr->n_model_count), MODEL_MGR_FLASH_ADDR_MODEL_COUNT, 4);
        dbg_msg("[DBG] model info: model count:%d\n", mgr->n_model_count);
        
        //get model info
        mgr->p_model_info = (kdp_model_info_t*)malloc(mgr->n_model_count * sizeof(struct kdp_model));
        kdp_memxfer_module.flash_to_ddr((uint32_t)(mgr->p_model_info), MODEL_MGR_FLASH_ADDR_MODEL_INFO , 
                       sizeof(kdp_model_info_t)*mgr->n_model_count);

        //get ddr model end addr
        offset = sizeof(kdp_model_info_t) * mgr->n_model_count;
        kdp_memxfer_module.flash_to_ddr((uint32_t)&(mgr->l_ddr_addr_model_end),
                        (uint32_t)(MODEL_MGR_FLASH_ADDR_MODEL_INFO + offset), 4);
        dbg_msg("[DBG] modelInfo: DDR end address: 0x%x\n", mgr->l_ddr_addr_model_end);
    }

    return mgr->n_model_count;
}

int32_t _kdp_modelmgr_load_model(kdp_model_mgr_t* mgr, uint8_t model_index_p/*starts from 0*/)
{
    uint32_t ddr_addr_model_pool; //start point = the 1st model cmd.bin
    uint32_t ddrAddr_offset;
    uint32_t flAddr;
    uint32_t nLenToLoad;
    struct kdp_model *p_model;

    // moved to outside
    //if(mgr->p_model_info == 0)
    //    return 0; //modelInfo is not inited

    
    if(mgr->pn_is_model_loaded_table[model_index_p] == 1 )
        return 0; //model has been loaded
    else
        mgr->pn_is_model_loaded_table[model_index_p] = 1;
		
    //load model with (index=model_index_p) from flash to DDR
    ddr_addr_model_pool = mgr->p_model_info[0].cmd_mem_addr; //start point = the 1st model cmd.bin
    p_model = &(mgr->p_model_info[model_index_p]);

    //load cmd + weight + setup together
    ddrAddr_offset = p_model->cmd_mem_addr - ddr_addr_model_pool;
    flAddr = MODEL_MGR_FLASH_ADDR_MODEL_POOL + ddrAddr_offset;
    nLenToLoad = p_model->cmd_mem_len + p_model->weight_mem_len + p_model->setup_mem_len;

    if(mgr->m_is_model_from_host)  {
        if(*(uint32_t*)p_model->cmd_mem_addr == 0 )
            dbg_msg("[ERR] modelmgr: model is not loaded\n");
        else
            dbg_msg("[INFO] modelmgr: model[%d] is verified at ddr[0x%x]\n", model_index_p, p_model->cmd_mem_addr);
    }
    else {
        kdp_memxfer_module.flash_to_ddr(p_model->cmd_mem_addr, flAddr, nLenToLoad);
        dbg_msg("[INFO] modelmgr: load model[%d] at ddr[0x%x]\n", model_index_p, p_model->cmd_mem_addr);
    }

    ////lod .bin files seperatedly
    ////load cmd.bin
    //ddrAddr_offset = p_model->cmd_mem_addr - ddr_addr_model_pool;
    //flAddr = MODEL_MGR_FLASH_ADDR_MODEL_POOL + ddrAddr_offset;
    //nLenToLoad = p_model->cmd_mem_len;
    //kdp_memxfer_module.flash_to_ddr(p_model->cmd_mem_addr, flAddr, nLenToLoad);

    ////load weight.bin
    //ddrAddr_offset = p_model->weight_mem_addr - ddr_addr_model_pool;
    //flAddr = MODEL_MGR_FLASH_ADDR_MODEL_POOL + ddrAddr_offset;
    //nLenToLoad = p_model->weight_mem_len;
    //kdp_memxfer_module.flash_to_ddr(p_model->weight_mem_addr, flAddr, nLenToLoad);

    ////load setup.bin
    //ddrAddr_offset = p_model->setup_mem_addr - ddr_addr_model_pool;
    //flAddr = MODEL_MGR_FLASH_ADDR_MODEL_POOL + ddrAddr_offset;
    //nLenToLoad = p_model->setup_mem_len;
    //kdp_memxfer_module.flash_to_ddr(p_model->setup_mem_addr, flAddr, nLenToLoad);
    
    return 0;
}

//public functions 
/**
 * @brief load model 
 * @param(in) -1: load all models
 *            0-n: info_index of model to load
 * @return status: 0: false
 *                 1: ok 
 */
int32_t kdp_modelmgr_load_model(int8_t model_info_index_p)
{
    int32_t ret;
	kdp_model_mgr_t *mgr = kdp_modelmgr();

    if(mgr->p_model_info == 0) {
        ret = _kdp_modelmgr_load_model_info(mgr);  //bin address and length
        if(ret == 0)
            return 1; //error   , no model_info data 
        
        mgr->pn_is_model_loaded_table = (uint8_t*)malloc(mgr->n_model_count * sizeof(uint8_t));
    }

    //load all models
    if (-1 == model_info_index_p) { 
        uint8_t i;
        for (i = 0 ;i < mgr->n_model_count ; i++)
        {
            ret = _kdp_modelmgr_load_model(mgr, i);
            if(ret == 0) {
                return 0;  //loaded
            }
        }
    }
    //load specific model
    else {
        ret = _kdp_modelmgr_load_model(mgr, model_info_index_p);
        return ret;
    }

    return 1; //error
}

void kdp_modelmgr_set_model_from_host(void)
{
	kdp_model_mgr_t *mgr = kdp_modelmgr();
    mgr->m_is_model_from_host = 1;
    return;
}

#if DEBUG
void kdp_modelmgr_dump_model_info(void)
{
	kdp_model_mgr_t *mgr = kdp_modelmgr();
    kdp_model_info_t* p_modelInfo = 0;
    uint8_t i;

    dbg_msg("Model info Count = %d\n", mgr->n_model_count);

    for (i = 0 ;i < mgr->n_model_count ; i++)
    {
        p_model = &(mgr->p_model_info[i]); 
        dbg_msg("Model(%2d):input[%x](sz:%d) -> cmd[%x](sz:%d),weight[%x](sz:%d),setup[%x](sz:%d),buf[%x](sz:%d) -> out[%x](sz:%d)\n", 
                (i+1),
                p_modelInfo->input_mem_addr, p_modelInfo->input_mem_len,
                p_modelInfo->cmd_mem_addr,   p_modelInfo->cmd_mem_len,
                p_modelInfo->weight_mem_addr,p_modelInfo->weight_mem_len,
                p_modelInfo->setup_mem_addr, p_modelInfo->setup_mem_len, 
                p_modelInfo->buf_addr,       p_modelInfo->buf_len,
                p_modelInfo->output_mem_addr,p_modelInfo->output_mem_len);
    }
     
    return;
}
#endif

