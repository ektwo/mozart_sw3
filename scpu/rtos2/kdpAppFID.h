#ifndef __KDP_APP_FID_H_
#define __KDP_APP_FID_H_

/* ==================
 *  FID application 
 *      kdpAppFID.c
 * ================== */

enum _KDP_APP_FID_STATUS_CODE_E {
    KDP_APP_FDR_WRONG_USAGE = 0x100,   //KDP_APP_FID_CODES
    KDP_APP_FDR_NO_FACE,
    KDP_APP_FDR_BAD_POSE, 
    KDP_APP_FDR_NO_ALIVE,
    KDP_APP_FDR_FR_FAIL,
};

//inferance/register
//---------------------------------
struct fdr_in_data {
    uint16_t mode;   //0: inference   1-5: register with faceId(1-5)
    uint16_t user_id;    //as input: user id got from Host
                     //as output: user id returned by Mozart
};
int32_t kdpapp_fdr(void* input, void* output);

//fd only
//---------------------------------
struct fd_out_data {
    int x;
    int y;
    int w;
    int h;
};
int32_t kdpapp_fd(void* input, void* output);

//application package init
//---------------------------------
int32_t kdpapp_init(void* p_inptr, void* p_outptr);
#endif
