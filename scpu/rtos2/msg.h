#ifndef __MSG_H__
#define __MSG_H__
#include "DrvUART010.h"
#include "base.h"
#include "com.h"

/* Modified MSG_DATA_BUF_MAX from 0x100 to 0x1400 to improve the speed of testcase verification */
extern u8 msg_rbuf[MSG_DATA_BUF_MAX + sizeof(MsgHdr)+ sizeof(RspPram) + 4];
extern u8 msg_tbuf[MSG_DATA_BUF_MAX + sizeof(MsgHdr)+ sizeof(RspPram) + 4];


#ifdef MOZART_CHIP
#define MSG_PORT            DRVUART_PORT4
#else
#define MSG_PORT            DRVUART_PORT1
#endif


/* message read */
int msg_read_final(u8 *buf);
int msg_read(u8 *buf, int size);
/* message write */
#if PACKET
int msg_write(u8 *buf, int len, int crc_flag);
#else
int msg_write(u8 *buf, int len);
#endif
/* message pack */
void msg_pack(u16 cmd, u8 *buf, int len);
/* put 1 character to uart */
void msg_putc(DRVUART_PORT port_no, char ch);
/* get 1 character from uart */
int msg_getc(DRVUART_PORT port_no, u8 *value, unsigned long timeout);

#endif
