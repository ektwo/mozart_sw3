#include "kdp_app_db.h"

#define FUNC_ADD 0
#define FUNC_DEL 1

/* ##########################
 * ##   Static Functions   ##
 * ########################## */

/**
 * @brief fast sqrt() implementation
 * @param x input
 * @return sqrt of x
 */
static float SQRT(float x)
{
    union {
        int i;
        float x;
    } u;
    u.x = x;
    u.i = (1<<29) + (u.i >> 1) - (1<<22);

    // Two Babylonian Steps (simplified from:)
    // u.x = 0.5f * (u.x + x/u.x);
    // u.x = 0.5f * (u.x + x/u.x);
    u.x =       u.x + x/u.x;
    u.x = 0.25f*u.x + x/u.x;

    return u.x;
}

/**
 * @brief Update user feature map data
 * @param fdr_addr address of user feature map data
 * @param update_idx update index in ddr
 * @param fm_idx feature map index
 */
static void UpdateFeature(uint32_t fdr_addr, uint16_t update_idx, uint16_t fm_idx)
{
    int size = FM_SIZE;
    int i = 0;

    while (size--) {
        outw(DDR_USER_FM_ADDR(update_idx, fm_idx - 1) + i * 4, inw(fdr_addr + i * 4)); /* save fm data to (fm_idx-1) */
        i++;
    }
}

/**
 * @brief Calculate distance of two feature points
 * @param user_idx user data index
 * @param fm_idx user feature map data index
 * @param fdr_addr address of user feature map data
 * @return L2 distance
 */
float FrCalDist(uint16_t user_idx, uint16_t fm_idx, uint32_t fdr_addr)
{
    int size = FM_SIZE;
    float sum = 0;
    union IntFloat {int32_t i; float f;};
    union IntFloat val;

    int i = 0;
    while (size--) {
        val.i = inw(fdr_addr + i * 4);
        float user_fm = val.f;

        val.i = inw(DDR_USER_FM_ADDR(user_idx, fm_idx - 1) + i * 4); /* read fm data from (fm_idx-1) */
        float db_fm = val.f;

        sum += (user_fm - db_fm) * (user_fm - db_fm);
        i++;
    }

    return SQRT(sum);
}

/* ###########################
 * ##   Private Functions   ##
 * ########################### */
/**
 * @brief check slot is in used(TYPE_VALID or TYPE_REGISTER) or not
 * @param i slot index
 * @return true(TYPE_VALID or TYPE_REGISTER) false(other value)
 */
bool _kdp_app_db_slot_is_used(uint16_t i)
{
    return ((inhw(DDR_USER_VALID_ADDR(i)) == TYPE_VALID) || (inhw(DDR_USER_VALID_ADDR(i)) == TYPE_REGISTER));
}

/**
 * @brief find index of existed user id in ddr
 * @param user_id user id
 * @return index of id in ddr
 */
uint16_t _kdp_app_db_find_exist_id(uint16_t user_id)
{
    uint16_t i;
    for (i = 0; i < MAX_USER; i++) {
        if ((inhw(DDR_USER_ID_ADDR(i)) == user_id) && _kdp_app_db_slot_is_used(i)) {
            break;
        }
    }

    return i;
}

/**
 * @brief find space in ddr for saving user data
 * @return idx of space where valid flag is not TYPE_VALID or TYPE_REGISTER
 */
uint16_t _kdp_app_db_find_space()
{
    uint16_t i;
    for (i = 0; i < MAX_USER; i++) {
        if (!_kdp_app_db_slot_is_used(i)) {
            break;
        }
    }

    return i;
}

/**
 * @brief save user data from model output to database in ddr
 * @param update_idx index in ddr to save
 * @param user_id user id
 * @param fm_idx feature map index
 * @param fdr_addr address of fdr feature map data
 * @return none
 */
void _kdp_app_db_update(uint16_t update_idx, uint16_t user_id, uint16_t fm_idx, uint32_t fdr_addr)
{
    /* update header, valid flag, user id, valid_fm_num */
    outhw(DDR_USER_HEAD_ADDR(update_idx), FID_HEADER);
    outhw(DDR_USER_VALID_ADDR(update_idx), TYPE_REGISTER);
    outhw(DDR_USER_ID_ADDR(update_idx), user_id);
    outhw(DDR_USER_VALID_FM_ADDR(update_idx), fm_idx);

    /* update user feature map data to ddr */
    UpdateFeature(fdr_addr, update_idx, fm_idx);
}

/**
 * @brief calculate threshold of detect user feature map data and user data in ddr
 * @param fdr_addr address of user feature map data
 * @param user_idx user index in database
 * @return threshold of user feature map data and exist data in ddr
 */
float _kdp_app_db_cal_feature_map_thresh(uint32_t fdr_addr, uint16_t user_idx)
{
    float min_thresh = 10000.0;

    uint16_t valid_fm_num = inhw(DDR_USER_VALID_FM_ADDR(user_idx));
    while (valid_fm_num) {
        float thresh_fdr = FrCalDist(user_idx, valid_fm_num, fdr_addr);
        /* find the minimum value */
        if (thresh_fdr < min_thresh) min_thresh = thresh_fdr;
        valid_fm_num--;
    }

    return min_thresh;
}

/**
 * @brief program user data in ddr to flash
 * @param update_idx user index in ddr and flash
 * @return none
 */
void _kdp_app_db_save_ddr_to_flash(uint16_t update_idx, uint16_t func)
{
    extern const struct s_kdp_memxfer kdp_memxfer_module;
    /*kdp_memxfer_module.init(MEMXFER_OPS_CPU, MEMXFER_OPS_DMA);*/

    if (func == FUNC_ADD) { /* add user data in ddr to flash */
        uint16_t total_size = INFO_SIZE_BYTE + MAX_FID * FM_SIZE_BYTE;
        kdp_memxfer_module.ddr_to_flash(FLASH_USER_HEAD_ADDR(update_idx), DDR_USER_HEAD_ADDR(update_idx), total_size);
    } else { /* del user data in flash */
        uint16_t total_size = INFO_SIZE_BYTE + MAX_FID * FM_SIZE_BYTE;
        kdp_memxfer_module.ddr_to_flash(FLASH_USER_HEAD_ADDR(update_idx), DDR_USER_HEAD_ADDR(update_idx), total_size);
    }

    /* TODO: check if ddr and flash data are match */

}

void _kdp_app_db_flush_register_data()
{
    int i;

    /* flush all register type user data */
    for (i = 0; i < MAX_USER; i++) {
        if (inhw(DDR_USER_VALID_ADDR(i)) == TYPE_REGISTER) {
            dbg_msg("[INFO][kdp_app_db] Clear user id: [%04d], fm count: [%d], unregister data in idx: [%d]\n",
                                              inhw(DDR_USER_ID_ADDR(i)), inhw(DDR_USER_VALID_FM_ADDR(i)), i);

            outhw(DDR_USER_VALID_ADDR(i), TYPE_INVALID);
            outhw(DDR_USER_ID_ADDR(i), 0xFFFE);
            outhw(DDR_USER_VALID_FM_ADDR(i), 0);
        }
    }
}

/* ##########################
 * ##   Public Functions   ##
 * ########################## */
/**
 * @brief compare user fd/fr result to user data in ddr
 * @param fdr_addr address of fd/fr feature map data
 * @return compare result and user id where find match feature map data
 */
int32_t kdp_app_db_compare(uint32_t fdr_addr, uint16_t *user_id)
{
    int i;
    float thresh_fid;
    float min_thresh = 10000.0;
    int min_idx = 0;

    /* flush all register type user data after inference */
    _kdp_app_db_flush_register_data();

    /* compare to all user data in ddr */
    for (i = 0; i < MAX_USER; i++) {
        /* only compare to valid user data in database */
        if (inhw(DDR_USER_VALID_ADDR(i)) == TYPE_VALID) {
            thresh_fid = _kdp_app_db_cal_feature_map_thresh(fdr_addr, i);
            dbg_msg("[INFO][kdp_app_db] idx: [%d], user id: [%04d], threshold: [%f]\n",
                                        i, inhw(DDR_USER_ID_ADDR(i)), thresh_fid);
            if (thresh_fid < min_thresh) {
                min_thresh = thresh_fid;
                min_idx = i;
            }
        }
    }

    if (min_thresh > (float)FID_THRESHOLD) {
        dbg_msg("[INFO][kdp_app_db] Can not find the user id !!!\n");
        return KDP_APP_DB_NO_MATCH;
    } else {
        dbg_msg("[INFO][kdp_app_db] Find user id: [%04d] in idx: [%d] of FID database!!!\n",
                                         inhw(DDR_USER_ID_ADDR(min_idx)), min_idx);
        *user_id = inhw(DDR_USER_ID_ADDR(min_idx));
        return KDP_APP_OK;
    }
}

/**
 * @brief register user fm data
 * @param fdr_addr address of fd/fr feature map data
 * @param user_id use id
 * @param fm_idx feature map data index
 * @return none
 */
int32_t kdp_app_db_register(uint32_t fdr_addr, uint16_t user_id, uint16_t fm_idx)
{
    uint16_t update_idx;

    /* check if id was existed */
    uint16_t find_id_idx = _kdp_app_db_find_exist_id(user_id);
    if (find_id_idx != MAX_USER) {
        if ((inhw(DDR_USER_VALID_ADDR(find_id_idx)) == TYPE_REGISTER) &&
            (fm_idx >= inhw(DDR_USER_VALID_FM_ADDR(find_id_idx)))) {
            update_idx = find_id_idx;
        } else {
            dbg_msg("[INFO][kdp_app_db] registered user id: [%04d] fm_idx: [%d] is in FID database idx: [%d], set new fm or delete user!!!\n",
                    user_id, inhw(DDR_USER_VALID_FM_ADDR(find_id_idx)), find_id_idx);
            return KDP_APP_DB_ALREADY_SAVED;
        }
    } else {
        /* check if have space for new user */
        uint16_t find_space_idx = _kdp_app_db_find_space();
        if (find_space_idx == MAX_USER) {
            dbg_msg("[INFO][kdp_app_db] no space for user id: [%04d]\n", user_id);
            return KDP_APP_DB_NO_SPACE;
        }
        update_idx = find_space_idx;
    }

    dbg_msg("[INFO][kdp_app_db] register user id: [%04d], fm index: [%d] to FID database idx: [%d] in DDR address: [0x%08x]\n",
                                         user_id, fm_idx, update_idx, DDR_USER_FM_ADDR(update_idx, fm_idx - 1));

    _kdp_app_db_update(update_idx, user_id, fm_idx, fdr_addr);

    return KDP_APP_OK;
}

/**
 * @brief register user data to database
 * @param user_id user id
 * @return user index in database
 */
int32_t kdp_app_db_add(void* input_ptr, void* output_ptr)
{
    struct fid_db_user_data *user_data = (struct fid_db_user_data *)input_ptr;
    uint16_t user_id = user_data->user_id_in;

    uint16_t i;
    for (i = 0; i < MAX_USER; i++) {
        /* save valid/user_id/valid_fm/fm data to flash */
        if (inhw(DDR_USER_VALID_ADDR(i)) == TYPE_REGISTER) {
            if (inhw(DDR_USER_ID_ADDR(i)) == user_id) {
                dbg_msg("[INFO][kdp_app_db] add user id: [%04d] fm count: [%d] data to flash addr: [0x%08x]\n",
                        user_id, inhw(DDR_USER_VALID_FM_ADDR(i)), FLASH_USER_HEAD_ADDR(i));

                /* set valid flag when save user data to flash */
                outhw(DDR_USER_VALID_ADDR(i), TYPE_VALID);

                _kdp_app_db_save_ddr_to_flash(i, FUNC_ADD);

                break;
            }
        }
    }

    user_data->user_idx = i;

    if (i == MAX_USER) {
        dbg_msg("[INFO][kdp_app_db] need to register user id: [%04d] data first\n", user_id);
        return KDP_APP_DB_REG_FIRST;
    } else {
        kdp_app_db_list(0, 0);
        return KDP_APP_OK;
    }
}

/**
 * @brief delete user data in ddr and flash
 * @param user_id user id
 * @param del_all del_all = 1 to delete all user data in flash
 * @return none
 */
int32_t kdp_app_db_delete(void* input_ptr, void* output_ptr)
{
    struct fid_db_user_data *user_data = (struct fid_db_user_data *)input_ptr;
    uint16_t user_id = user_data->user_id_in;
    uint16_t del_all = user_data->del_all;

    /* flush all register type user data after delete */
    _kdp_app_db_flush_register_data();

    if (del_all == 0x01) {
        int i;
        for (i = 0; i < MAX_USER; i++) {
            /* TODO: use mem_clr function */
            /* clear (set invalid flag) all user data in ddr */
            outhw(DDR_USER_VALID_ADDR(i), TYPE_INVALID);
            outhw(DDR_USER_ID_ADDR(i), 0xFFFE);
            outhw(DDR_USER_VALID_FM_ADDR(i), 0);

            /* TODO: erase user database area in flash by write invalid type */
            _kdp_app_db_save_ddr_to_flash(i, FUNC_DEL);

            dbg_msg("[INFO][kdp_app_db] delete user idx: [%d], user id: [%04d] data from flash addr: [0x%08x]\n",
                                                    i, inhw(DDR_USER_ID_ADDR(i)), FLASH_USER_HEAD_ADDR(i));
        }

        return KDP_APP_OK;

    } else {
        /* check id and valid flag data in flash */
        uint16_t find_id_idx = _kdp_app_db_find_exist_id(user_id);
        if (find_id_idx == MAX_USER) {
            dbg_msg("[INFO][kdp_app_db] can not find user id: [%04d] data\n", user_id);
            return KDP_APP_DB_DEL_FAIL;

        } else {
            dbg_msg("[INFO][kdp_app_db] find user id: [%04d] data in index: [%d]\n",
                                             user_id, find_id_idx);

            outhw(DDR_USER_VALID_ADDR(find_id_idx), TYPE_INVALID);
            outhw(DDR_USER_ID_ADDR(find_id_idx), 0xFFFE);
            outhw(DDR_USER_VALID_FM_ADDR(find_id_idx), 0);

            _kdp_app_db_save_ddr_to_flash(find_id_idx, FUNC_DEL);

            dbg_msg("[INFO][kdp_app_db] delete user id: [%04d] data from flash addr: [0x%08x]\n",
                                               user_id, FLASH_USER_HEAD_ADDR(find_id_idx));

            user_data->user_idx = find_id_idx;

            return KDP_APP_OK;
        }
    }
}

/**
 * @brief output user number and all user id data
 */
int32_t kdp_app_db_list(void* input_ptr, void* output_ptr)
{
    int i;
    for (i = 0; i < MAX_USER; i++) {
        if (_kdp_app_db_slot_is_used(i)) {
            uint16_t user_id = inhw(DDR_USER_ID_ADDR(i));
            uint16_t valid_fm = inhw(DDR_USER_VALID_FM_ADDR(i));
            dbg_msg("[INFO][kdp_app_db] idx: [%d], user id: [%04d], valid fm: [%d], type: [%d]\n",
                                        i, user_id, valid_fm, inhw(DDR_USER_VALID_ADDR(i)));
        }
    }
    return KDP_APP_OK;
}

/**
 * @brief output one feature map data of one user
 * @param user_id user id
 * @param image_idx image index
 */
int32_t kdp_app_db_upload(void* input_ptr, void* output_ptr)
{
    struct fid_db_user_data *user_data = (struct fid_db_user_data *)input_ptr;
    uint16_t user_id = user_data->user_id_in;
    uint16_t fm_idx = user_data->fm_idx;

    uint16_t find_id_idx = _kdp_app_db_find_exist_id(user_id);
    if (find_id_idx == MAX_USER) {
        return KDP_APP_DB_USER_NOT_REG;
    } else {
        union IntFloat {int32_t i; float f;};
        union IntFloat val;
        int i;

        dbg_msg("[INFO][kdp_app_db] user id: [%04d], feature map idx: [%d] feature map data in idx: [%d]:\n",
                                    user_id, fm_idx, find_id_idx);
        for (i = 0; i < FM_SIZE; i++) {
            val.i = inw(DDR_USER_FM_ADDR(find_id_idx, (fm_idx - 1)) + 4 * i); /* read fm data from (fm_idx-1) */
            dbg_msg("%03d: %3.8f\n", i, val.f);
        }
    }

    return KDP_APP_OK;
}

int32_t kdp_app_db_abort_reg(void* input_ptr, void* output_ptr)
{
    /* flush all register type user data */
    _kdp_app_db_flush_register_data();
	
    return KDP_APP_OK;
}
