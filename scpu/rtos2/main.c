/* -------------------------------------------------------------------------- 
 * Copyright (c) 2013-2016 ARM Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *      Name:    main.c
 *      Purpose: RTX for Kneron
 *
 *---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "DrvUART010.h"
#include "DrvDMAC020.h"
#include "kneron_mozart.h"                // Device header
#include "kneron_mozart_ext.h"
#include "cmsis_os2.h"                    // ARM::CMSIS:RTOS2:Keil RTX5
#include "Driver_Common.h"
#include "base.h"
#include "crc.h"
#include "com.h"
#include "io.h"
#include "string.h"
#include "ddr.h"
#include "msg.h"
#include "kdp_com.h"
#include "host_com.h"
#include "v2k_cam.h"
#include "kdpAppMgr.h"
#ifdef MOZART_CHIP
#include "scu_extreg.h"
#include "bootloader.h"
#include "system.h"
#include "clock.h"
#endif

#include "usb_com.h"
#include "version.h"
#include "dbg.h"

//#include "kdpApp.h"
#ifdef DBG_ALONE
#include "kdp_app_obj_det.h"

extern struct yolo_result *p_yolo_output_s;        //strcture defined in ipc.h
extern struct facedet_result *p_fd_output_s;        //strcture defined in ipc.h
extern struct landmark_result *p_lm_output_s;        //strcture defined in ipc.h
extern struct fr_result *p_fr_output_s;        //strcture defined in ipc.h

#else
#include "kdpAppFID.h"
#endif

#define MSG_DBG_LOG 
#define MSG_MEM_BYTE_WRITE     0

/**
 * @info(), Get kernel information
 */

int cpu_type = 0;
void info(int from_boot_loader) {
    char infobuf[100];
    osVersion_t osv;
    osStatus_t status;
    uint32_t rom_table;
	
    if (from_boot_loader) {
        DSG("\n=== From Bootloader ===");
    }
    else {
        DSG("\n=== From JTag debug ===");
    }
    
    rom_table = inw(CPU_ID_STS_ADDR);
    if (CPU_ID_SCPU == rom_table) {
        DSG("=== [I am SCPU] firmware version: %x ===", g_fw_misc.version);
        cpu_type = 0;
    }
    else if (CPU_ID_NCPU == rom_table) {
        DSG("=== [I am NCPU] firmware version: %x ===", g_fw_misc.version);
        cpu_type = 1;
    }  
    
    status = osKernelGetInfo(&osv, infobuf, sizeof(infobuf));
    if(status == osOK) {	
        DSG("+-------------------Keil RTX5----------------------+");
        DSG("Kernel Information: %s", infobuf);
        DSG("Kernel Version    : %d", osv.kernel);
        DSG("Kernel API Version: %d", osv.api);      
    }
}

/**
 * @brief main, main dispatch function
 */
extern u32 __sys_int_flag;
extern void small_d(int delay);

void enable_pll(void)
{
	fLib_printf("\n\nPLL init\n");
	int val,i;

//	//PLL2,PLL3,PLL5 en
//	val = inw(SCU_EXTREG_PA_BASE + 0x00C);
//	outw(SCU_EXTREG_PA_BASE + 0x00C, val | 0x00000001);

//#ifdef MIPI_TEST        //PLL2,PLL3,PLL5 en
//    val = inw(SCU_EXTREG_PA_BASE + 0x00C);        
//	//20190506 for mipi , set clkout1 to 360Mhz , clkout2 to 90Mhz
//	val &= (0xe0e008fe);
//	//val |= (0x02<<24)|(0x78<<12)|(0x01<<8); //ps = 0x2, ns =0x78, ms=0x1				
//	val |= (0x01<<24)|(0x11b<<12)|(0x02<<8); //ps = 0x1, ns = 0x11b, ms=0x02
//	outw(SCU_EXTREG_PA_BASE + 0x00C, val | 0x00000001);//enable
//	//divider
//	val = inw(SCU_EXTREG_PA_BASE + 0x028);
//	val &= 0xe0e0fff;
//	val |= (0x04<<28)|(0x06<<20)|(0x1d<<12);
//	outw(SCU_EXTREG_PA_BASE + 0x028, val);
//#endif
//    
//	val = inw(SCU_EXTREG_PA_BASE + 0x008);
//	outw(SCU_EXTREG_PA_BASE + 0x008, val | 0x00000001);
//	val = inw(SCU_EXTREG_PA_BASE + 0x03C);
//	outw(SCU_EXTREG_PA_BASE + 0x03C, val | 0x00000001);
//	small_d(10);

//	//PLL2,PLL3,PLL5 out_en
//	val = inw(SCU_EXTREG_PA_BASE + 0x014);
//	outw(SCU_EXTREG_PA_BASE + 0x014, val | 0x00000334);
//	



	// For fcs function to gating pll4(from pll0)
//	small_d(10);
//	//pll4 out_en setting
//	val = inw(SCU_EXTREG_PA_BASE + 0x014);
//	outw(SCU_EXTREG_PA_BASE + 0x014, val & 0xffffffbf);
//	small_d(30);
//	//pll4 en setting
//	val = inw(SCU_EXTREG_PA_BASE + 0x010);
//	outw(SCU_EXTREG_PA_BASE + 0x010, val & 0xfffffffe);
//	small_d(30);
//	//pll4 fref_en setting
//	val = inw(SCU_EXTREG_PA_BASE + 0x014);
//	outw(SCU_EXTREG_PA_BASE + 0x014, val & 0xffffe7ff);

	// Initial INTC
//		outw(0xE000E284,0x00000001 << 8);
//		outw(0xE000E104,0x00000001 << 8);
//		val = inw(0xE000E104);
//	// SCU Interrupt enable register, clear scu state
//		outw(SCU_FTSCU100_PA_BASE + 0x24 , 0xffffffff);
//	
//	// SCU PLL2 register setting
//	val = inw(SCU_FTSCU100_PA_BASE + 0x40);
//	outw(SCU_FTSCU100_PA_BASE + 0x40, val & 0xfffffffe | 0x00000001 );
//	// SCU DLL register setting
//	val = inw(SCU_FTSCU100_PA_BASE + 0x44);
//	outw(SCU_FTSCU100_PA_BASE + 0x44, val & 0xfffffffe | 0x00000001 );
//	// SCU PLLCTL, keep PLL NS value and frange
//	val = inw(SCU_FTSCU100_PA_BASE + 0x30);
//	outw(SCU_FTSCU100_PA_BASE + 0x30, val & 0xffffff0f | 0x00000020 );
//	// SCU PWR_MOD register setting
//	val = inw(SCU_FTSCU100_PA_BASE + 0x20);
//	outw(SCU_FTSCU100_PA_BASE + 0x20, val & 0x0f0fff0f | 0x90000040 );

//	__WFI();
//	do{
//       //idle(100);
//  }while((__sys_int_flag)!= 0x1);

//	//pll4 fref_en setting
//	val = inw(SCU_EXTREG_PA_BASE + 0x014);
//	outw(SCU_EXTREG_PA_BASE + 0x014, val | 0x00001800);
//	small_d(30);
//	//pll4 fref_en setting
//	val = inw(SCU_EXTREG_PA_BASE + 0x010); 
//	outw(SCU_EXTREG_PA_BASE + 0x010, val | 0x00000001);
//	small_d(30);
//	//pll4 fref_en setting
//	val = inw(SCU_EXTREG_PA_BASE + 0x014); 
//	outw(SCU_EXTREG_PA_BASE + 0x014, val | 0x00000040);

	//outw(0xC2380090,0x13000003); // MIPI0
	//outw(0xC2380094,readl(0xc2380094)|0x13000003); // MIPI1
  //outw(0xC238009C,readl(0xc238009c)|0x00000070); // MIPI1	
	//outw(0xc238009c, 0xff); //DPI2AHB		
	//trace_clk_pinmux(); // pinmux with LCD

#if 1
    //small_d(10);
	//outw(SCU_EXTREG_PA_BASE + 0x80, (inw(SCU_EXTREG_PA_BASE + 0x80) | 0x10000000 | 0x8000)); // wakeup DDRC 
	//small_d(10);
#else
	outw(SCU_EXTREG_PA_BASE + 0x14, (inw(SCU_EXTREG_PA_BASE + 0x14) | 0x00C00000)); // NCPU fclk src en setting; Trace clk
	outw(SCU_EXTREG_PA_BASE + 0x18, (inw(SCU_EXTREG_PA_BASE + 0x18) | 0x00800000)); // Enable npu clk_en
	//outw(SCU_EXTREG_PA_BASE + 0x4c, (inw(SCU_EXTREG_PA_BASE + 0x4c) | 0x00800002)); // Enable PD_NPU_reset_n
	outw(SCU_EXTREG_PA_BASE + 0x4c, (inw(SCU_EXTREG_PA_BASE + 0x4c) | 0x00800004)); // Enable NPU_reset_n
	
	//outw(SCU_EXTREG_PA_BASE + 0x18, (inw(SCU_EXTREG_PA_BASE + 0x18) | 0x00000003)); // Enable LCD clock
	//outw(SCU_EXTREG_PA_BASE + 0x14, (inw(SCU_EXTREG_PA_BASE + 0x14) | 0x00300800)); // Enable PLL5
	//outw(SCU_EXTREG_PA_BASE + 0x4c, (inw(SCU_EXTREG_PA_BASE + 0x4c) | 0x00800001)); // Enable LCD reset release

    small_d(10);
	outw(SCU_EXTREG_PA_BASE + 0x80, (inw(SCU_EXTREG_PA_BASE + 0x80) | 0x10000000 | 0x8000)); // wakeup DDRC 
	small_d(10);

	//fLib_printf("\nWake up NCPU\n");
	outw(SCU_EXTREG_PA_BASE + 0x68, (inw(SCU_EXTREG_PA_BASE + 0x68) | 0x00001000)); // wakeup NCPU
    
	outw(0xC2380044, 0x1F0200);
#endif    
#ifdef MIPI_TEST
    //enable mipi related clock
	val = inw(SCU_EXTREG_PA_BASE + 0x018);
	outw(SCU_EXTREG_PA_BASE + 0x018, val | 0x00000773);				
        				
    outw(0xC2380090,0x13000003); // MIPI0
    outw(0xC2380094,readl(0xc2380094)|0x13000003); // MIPI1
	//mipi phy settle count
	outb(0xc3700011, 3);
    outw(0xc238009c, 0xff); //DPI2AHB   
#endif		
}

#include "DrvPWMTMR010.h"
//extern void enable_pll(void);

extern void ip_test_main(void);
osThreadId_t tid_app;
static void test_thread(void *argument)
{
    int ret;
    unsigned int frame_addr;
    unsigned int frame_size;
    
     delay_ms(1000);
    
    kdp_camera_open();
    kdp_camera_start(KDP_CAMERA_START_WITH_PREVIEW);

    for (;;) {
        ret = kdp_camera_buffer_capture(&frame_addr, &frame_size);
        DSG("rrrrrrrrrrrrrrrret=%x", ret);
        if (0 != ret)
            continue;//break;

        DSG("frame_addr=%x frame_size=%d waiting", frame_addr, frame_size);
        delay_ms(1000);

        if (0 != kdp_camera_buffer_prepare())
            break;
    }

    kdp_camera_stop();
    kdp_camera_close();
}
void app(void)
{
    tid_app = osThreadNew(test_thread, NULL, NULL);
}
extern void delay_ms(int msec);
int main(void) 
{
	int from_boot_loader = 0; 
    fLib_SerialInit(DEBUG_CONSOLE, DEFAULT_CONSOLE_BAUD, PARITY_NONE, 0, 8, 0);    
#ifdef MOZART_CHIP
	//from_boot_loader = bootloader_handler();
    system_init();
    ddr_init_spl();
    uart4_enable();
#else
    ddr_init_v35_mipi(0);
#endif

    info(from_boot_loader);
    
    SystemCoreClockUpdate();            // System Initialization 
    osKernelInitialize();               // Initialize CMSIS-RTOS

    kdp_com_init();
    v2k_camera_init();
    usb_init();

    app();
#ifdef PRINT_CPU_USAGE
    // this does a cheap trick to print SCPU loading usage per 3 secs
    // put this in power.c, this code can be removed once get a better way
    power_mgr_cpu_usage_init();
#endif

    system_wakeup_ncpu(from_boot_loader);           

    host_com_init();

    ////dma flash <-> ddr test
    //char* dest = (char*)0x603c0f60;
    //memset(dest, 0, 2000);
    //kdpapp_test_dma_flash_to_ddr( (u32_t)0x603c0f60, (u32_t)0x0000, (u32_t)2000);
    ////kdpapp_test_dma_ddr_to_flash( (u32_t)0x603c0f60, (u32_t)0x0000, (u32_t)2000);
    //fLib_printf("finish dma flash <-> ddr test\n");

//#ifdef DBG_ALONE

//    p_yolo_output_s = NULL;
//    p_fd_output_s = NULL;
//    p_lm_output_s = NULL;
//    p_fr_output_s = NULL;

//    kdp_appmgr_init(&kdpapp_obj_det_init, tid_host_comm, 
//                FLAG_HOST_COMM_APP_DONE,
//                KDP_APPMGR_CONFIG_COMPANION);
//#else
//    kdp_appmgr_init(&kdpapp_init, tid_host_comm, 
//                FLAG_HOST_COMM_APP_DONE,
//                KDP_APPMGR_CONFIG_COMPANION);
//#endif

    //application is triggered in host_com.c
    if (osKernelGetState() == osKernelReady) {
        osKernelStart();
    }

    while(1) {
    }
}
