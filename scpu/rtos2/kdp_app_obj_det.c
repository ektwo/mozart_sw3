#include <string.h>  //for memcpy
#include "kdpAppMgr.h"
#include "kdpModelMgr.h"
#include "kdpApp.h"
#include "kdp_app_obj_det.h"

#include "kdp_com.h"
#include "scu_ipc.h"     /*for NCPU triggering */
#include "dbg.h"
#include "cmsis_os2.h"
#include "model_type.h"
#include "memxfer.h"
#include "memory.h"

//extern const struct s_kdp_memxfer kdp_memxfer_module;
//struct image_info g_image_info;

struct yolo_result *p_yolo_output_s;        //strcture defined in ipc.h
struct facedet_result *p_fd_output_s;        //strcture defined in ipc.h
struct landmark_result *p_lm_output_s;        //strcture defined in ipc.h
struct fr_result *p_fr_output_s;        //strcture defined in ipc.h

static void * pNpuOut = NULL;
static int32_t nNpuOutLen;

extern uint32_t dbg_model_type;
extern struct kdp_image    image_s;


#define INT_YOLO_RESULT_ADDR     0x63900110
#define TINY_YOLO_RESULT_LEN_ADDR 0x63900100

extern void scpu_get_result(void);
extern void scpu_convert_yolo_float_to_int(void);

struct int_bounding_box {
    int32_t x1;           // top-left corner:  x
    int32_t y1;           // top-left corner:  y
    int32_t x2;           // bottom-right corner:  x
    int32_t y2;           // bottom-right corner:  y

    int32_t score;        // probability score
    int class_num;      // class # (of many) with highest probability
};

struct int_yolo_result {
    uint32_t    class_count;            // total class count
    uint32_t    box_count;              /* boxes of all classes */
    struct int_bounding_box boxes[1];       /* [box_count] */
};

struct int_fr_result {
    int32_t feature_map[512];
};

enum KDP_APP_OBJ_DDR_ADDR {
    KDP_APP_OBJ_DDR_ADDR_IMAGE,
    KDP_APP_OBJ_DDR_ADDR_END,
    KDP_APP_OBJ_DDR_ADDR_TABLE_SIZE
};
uint32_t kdp_app_obj_ddr_addr_table_s[KDP_APP_OBJ_DDR_ADDR_TABLE_SIZE];   //moved to kdpApp.c

/* ##########################
 * ##   private function   ##
 * ########################## */
#ifdef DBG_ALONE

static void extract_yolo_result(void *p)
{
    struct yolo_result *ptr = (struct yolo_result *)p;
    dbg_msg("[INFO] OBJ_DET (classCount,BoxCount)= %d,%d\n", 
                ptr->class_count,
                ptr->box_count);

    if(ptr->box_count > 0)
    {
        for(int i=0; i<p_yolo_output_s->box_count; i++)
        {
          dbg_msg("box[%d]:class=%d, (x1,y1,x2,y2) = (%f, %f, %f, %f) @ score=%f\n", i,
                ptr->boxes[i].class_num,
                ptr->boxes[i].x1,
                ptr->boxes[i].y1,
                ptr->boxes[i].x2,
                ptr->boxes[i].y2,
                ptr->boxes[i].score);
        }
    }

    uint32_t len = 0;
    len += sizeof(struct yolo_result);         // 1 box included
    if (ptr->box_count >=1)
      len += (ptr->box_count - 1) * sizeof(struct bounding_box);   // the rest
    else
      len -= sizeof(struct bounding_box);     // -1 for 0 box_count

    if(ptr->box_count == 0)  
      len = 0;
    *((uint32_t *)TINY_YOLO_RESULT_LEN_ADDR) = len;

    struct int_yolo_result *pIntYolo = (struct int_yolo_result *)INT_YOLO_RESULT_ADDR;

    pIntYolo->box_count = ptr->box_count;
    pIntYolo->class_count = ptr->class_count;

    for(int i=0; i<ptr->box_count; i++)
    {
        pIntYolo->boxes[i].class_num = (int)ptr->boxes[i].class_num;
        pIntYolo->boxes[i].x1 = (int)ptr->boxes[i].x1;
        pIntYolo->boxes[i].y1 = (int)ptr->boxes[i].y1;
        pIntYolo->boxes[i].x2 = (int)ptr->boxes[i].x2;
        pIntYolo->boxes[i].y2 = (int)ptr->boxes[i].y2;
        pIntYolo->boxes[i].score = (int)100*ptr->boxes[i].score;
    }

}

static void extract_fd_result(void *p)
{
    uint32_t len;
    struct facedet_result *ptr = (struct facedet_result *)p;
    dbg_msg("[INFO] Face Detect bounding boxes: x=%d, y=%d, w=%d, h=%d\n", 
                ptr->xywh[0],
                ptr->xywh[1],
                ptr->xywh[2],
                ptr->xywh[3]);

    if(ptr->len == 0)  
      len = 0;
    *((uint32_t *)TINY_YOLO_RESULT_LEN_ADDR) = len;

    struct facedet_result *pHost = (struct facedet_result *)INT_YOLO_RESULT_ADDR;
    *pHost = *ptr;

}

static void extract_lm_result(void *p)
{
    struct landmark_result *ptr = (struct landmark_result *)p;

    dbg_msg("[INFO] LandMarks:\n");

    for(int i=0; i<5; i++)
    {
      dbg_msg("lm[%d]: x=%d  y=%d\n", i, 
                ptr->marks[2*i],
                ptr->marks[2*i+1]);
    }

    *((uint32_t *)TINY_YOLO_RESULT_LEN_ADDR) = sizeof(struct landmark_result);

    struct landmark_result *pHost = (struct landmark_result *)INT_YOLO_RESULT_ADDR;
    *pHost = *ptr;

}

static void extract_fr_result(void *p)
{
    struct fr_result *ptr = (struct fr_result *)p;

    dbg_msg("[INFO] Face Recog Results:\n");

    for(int i=0; i<512; i++)
    {
      dbg_msg("fm[%d]=%f\n", i, ptr->feature_map[i]);
    }

    *((uint32_t *)TINY_YOLO_RESULT_LEN_ADDR) = sizeof(struct int_fr_result);

    struct int_fr_result *pHost = (struct int_fr_result *)INT_YOLO_RESULT_ADDR;
    for(int i=0; i<512; i++)
    {
      pHost->feature_map[i] = (int32_t)(ptr->feature_map[i]);
    }

}

#endif

/**
 * @brief do obj dectection
 *        save obj result in yolo_output_s
 * @return status: OK or Err code
 */
static int32_t _kdpapp_obj_det(void)
{
    int n_model_info_index = 0; // 0 is yolo in app_obj_det
    int n_model_slot_index = 0;
    int status;

    kdp_com_set_model(kdp_modelmgr()->p_model_info, n_model_info_index, n_model_slot_index);
    kdp_com_get_output_ptr()->models_type[n_model_slot_index] = dbg_model_type;
    kdp_modelmgr()->load_model(n_model_info_index);

    int active_image_index = 0; //only 1 image is used
    kdp_com_get_output_ptr()->active_img_index = active_image_index;
    struct kdp_img_raw* p_raw_image = &(kdp_com_get_output_ptr()->raw_images[active_image_index]);  
    p_raw_image->state = IMAGE_STATE_ACTIVE;
    p_raw_image->seq_num = 1;
    p_raw_image->ref_idx = active_image_index;

    p_raw_image->format     = image_s.image_format; //RGBA8888 without sub-128
    p_raw_image->input_col  = image_s.image_col;
    p_raw_image->input_row  = image_s.image_row;
    p_raw_image->input_channel = image_s.image_ch;

    p_raw_image->crop_top = 0;
    p_raw_image->crop_bottom = 0;
    p_raw_image->crop_left= 0;
    p_raw_image->crop_right= 0;
	  
    p_raw_image->image_mem_addr = kdpapp_get_ddr_addr(kdp_app_obj_ddr_addr_table_s, KDP_APP_OBJ_DDR_ADDR_IMAGE);
    p_raw_image->image_mem_len = p_raw_image->input_row *p_raw_image->input_col;
    //for output

#ifdef DBG_ALONE
    p_raw_image->results[n_model_slot_index].result_mem_addr = (uint32_t)pNpuOut;
    p_raw_image->results[n_model_slot_index].result_mem_len = (int32_t)nNpuOutLen;
#else
    pNpuOut = (struct yolo_result*)kdp_ddr_malloc(sizeof(struct yolo_result));
    nNpuOutLen = sizeof(struct yolo_result);
#endif

    struct kdp_img_result* p_raw_result = &(kdp_com_get_input_ptr()->img_results[active_image_index]);
    p_raw_result->seq_num = 1;
				
    kdp_com_set_model_slot_index(n_model_slot_index);

    dbg_msg("[INFO] kdp_app_obj_dec:\n");
    dbg_msg(" active_image_index = %d\n", kdp_com_get_output_ptr()->active_img_index);
    dbg_msg("       (row/col/ch) = %d/%d/%d\n", p_raw_image->input_row, p_raw_image->input_col, p_raw_image->input_channel);
    dbg_msg("             format = 0x%x\n", p_raw_image->format);
    dbg_msg("  crop(tp/bt/lf/rt) = %d/%d/%d/%d\n", p_raw_image->crop_top, p_raw_image->crop_bottom,
                                                   p_raw_image->crop_left, p_raw_image->crop_right);
    dbg_msg("            ref_idx = %d\n", p_raw_image->ref_idx);
    dbg_msg("         model type = %d\n", kdp_com_get_output_ptr()->models_type[n_model_slot_index]);
    dbg_msg("        image  addr = 0x%x\n", p_raw_image->image_mem_addr);
    dbg_msg("        output addr = 0x%x\n", p_raw_image->results[n_model_slot_index].result_mem_addr);

    // Start time for ncpu/npu round trip
    p_raw_image->tick_start = osKernelGetTickCount();

    //trigger ncpu
    scu_ipc_trigger_to_ncpu_int();
    
    //extract result 
    osThreadFlagsWait(FLAG_APPMGR_FROM_NCPU, osFlagsWaitAll, osWaitForever);

    status = kdp_com_get_input_ptr()->img_results[active_image_index].status;
    if(status != IMAGE_STATE_DONE)
        return KDP_APP_UNKNOWN_ERR;

#ifdef DBG_ALONE

    switch(dbg_model_type)
    {
       case KNERON_TINY_YOLO_PERSON:
            extract_yolo_result(pNpuOut);
          break;

       case KNERON_FDSMALLBOX:
            extract_fd_result(pNpuOut);
          break;

       case KNERON_LM_5PTS:
            extract_lm_result(pNpuOut);
          break;

       case KNERON_FR_VGG10:
            extract_fr_result(pNpuOut);
          break;
       default:
            dbg_msg("%s: Error: model_type is not set\n", __func__);
    }
#endif

    return KDP_APP_OK;
}


/* ##########################
 * ##   public function   ##
 * ########################## */
/**
 * @brief do obj dectection
 *        save obj result in yolo_output_s
 * @return status: OK or Err code
 */
int32_t kdpapp_obj_det(void* p_input, void* p_output)
{   
    if(kdp_modelmgr()->n_model_count == 0)
        kdpapp_obj_det_init(0,0);  //do again to use model_info data
    
    //allocate ddr for ncpu output
    kdp_ddr_init(kdpapp_get_ddr_addr(kdp_app_obj_ddr_addr_table_s, KDP_APP_OBJ_DDR_ADDR_END));

    switch(dbg_model_type)
    {
       case KNERON_TINY_YOLO_PERSON:
          if(p_yolo_output_s == NULL)
        p_yolo_output_s = (struct yolo_result*)kdp_ddr_malloc(sizeof(struct yolo_result));
          pNpuOut = p_yolo_output_s;
          nNpuOutLen = sizeof(struct yolo_result);
          break;

       case KNERON_FDSMALLBOX:
          if(p_fd_output_s == NULL)
             p_fd_output_s = (struct facedet_result*)kdp_ddr_malloc(sizeof(struct facedet_result));
          pNpuOut = p_fd_output_s;
          nNpuOutLen = sizeof(struct facedet_result);
          break;

       case KNERON_LM_5PTS:
          if(p_lm_output_s == NULL)
             p_lm_output_s = (struct landmark_result*)kdp_ddr_malloc(sizeof(struct landmark_result));
          pNpuOut = p_lm_output_s;
          nNpuOutLen = sizeof(struct landmark_result);
          break;

       case KNERON_FR_VGG10:
          if(p_fr_output_s == NULL)
             p_fr_output_s = (struct fr_result*)kdp_ddr_malloc(sizeof(struct fr_result));
          pNpuOut = p_fr_output_s;
          nNpuOutLen = sizeof(struct fr_result);
          break;
       default:
            dbg_msg("%s: Error: model_type is not set\n", __func__);
    }

    //todo: scpu_comm_thread in ncpu will check CMD_RUN_CMD 
    kdp_com_get_output_ptr()->cmd = CMD_RUN_NPU;    

    //run NN flow
    int32_t ret;
    dbg_profile_start();
    ret = _kdpapp_obj_det();
    dbg_profile_stop("kdpapp_obj_det");  
    if(ret != KDP_APP_OK){
        return ret;
    }

    memcpy(p_output, p_yolo_output_s, sizeof(struct yolo_result));

    return ret;
}

int32_t kdpapp_obj_det_init(void* p_inptr, void* p_outptr)
{
    int32_t ret = 0;
    
    //register application callback
    /* func id = 1 "inference or register */ 
    if(kdp_appmgr()->get_app_count() == 1)
        kdp_appmgr()->register_app(KDP_APPMGR_CB_TYPE_NPU, "inference",     &kdpapp_obj_det);
    
    kdp_memxfer_module.init(MEMXFER_OPS_DMA/*for flash src*/, MEMXFER_OPS_DMA/*for DDR src*/);
    
    kdp_modelmgr()->set_model_from_host();  //model data from host via python command
    //load models
    //dbg_profile_start();
    ret = kdp_modelmgr()->load_model(-1); //load all models
    //dbg_profile_stop("Load model");
    
    if(kdp_modelmgr()->n_model_count == 0)
        return 0; 
    
    kdp_app_obj_ddr_addr_table_s[KDP_APP_OBJ_DDR_ADDR_IMAGE]= kdp_modelmgr()->l_ddr_addr_model_end;
    kdp_app_obj_ddr_addr_table_s[KDP_APP_OBJ_DDR_ADDR_END]  = kdp_app_obj_ddr_addr_table_s[KDP_APP_OBJ_DDR_ADDR_IMAGE] + 
                                                              KDP_APP_IMG_SIZE;
    
    dbg_msg("[DBG] ddr_addr_IMAGE  = 0x%x\n", kdp_app_obj_ddr_addr_table_s[KDP_APP_OBJ_DDR_ADDR_IMAGE]);
    dbg_msg("[DBG] ddr_addr_END    = 0x%x\n", kdp_app_obj_ddr_addr_table_s[KDP_APP_OBJ_DDR_ADDR_END]);

    //pre-allocate DDR resource for NCPU
    kdp_ddr_init(kdpapp_get_ddr_addr(kdp_app_obj_ddr_addr_table_s, KDP_APP_OBJ_DDR_ADDR_END));
    uint32_t mem_len2 = 0x100000;
    uint8_t *mem_addr2 = (uint8_t*)kdp_ddr_malloc(sizeof(uint8_t)*mem_len2);
    kdp_com_get_output_ptr()->input_mem_addr2 = (uint32_t)mem_addr2;
    kdp_com_get_output_ptr()->input_mem_len2 = mem_len2;
    
    //load image
    //todo:load image from camera
    //dbg_profile_start();
    //uint32_t src = MODEL_MGR_FLASH_ADDR_MODEL_END; //image flash address 
    //uint32_t dest = (uint32_t)kdpapp_get_ddr_addr(kdp_app_obj_ddr_addr_table_s, KDP_APP_OBJ_DDR_ADDR_IMAGE);
    //kdp_memxfer_module.flash_to_ddr(dest, src, KDP_APP_IMG_SIZE);
    //dbg_profile_stop("Load image");
    
    
    
    return 0;
}
