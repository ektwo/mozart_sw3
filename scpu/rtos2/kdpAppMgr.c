#include <stdlib.h>
#include <string.h>
#include "kdpAppMgr.h"
#include "kdp_com.h"
#include "host_com.h"
#include "dbg.h"

#define DEBUG 0

/* =============================
 *  private data
 * =============================
 */
#define MAX_APP_CB_DESCRIPTION 15
typedef struct _KDP_APPMGR_CB {
#if DEBUG
    char        description[MAX_APP_CB_DESCRIPTION+1];  // brief description of call back function
#endif
    kdp_appmgr_cb_fn_t runfunc;                         // callback function
    kdp_appmgr_cb_type_e cb_type;                          // callback is private or public
} kdp_appmgr_cb_t;

typedef struct _KDP_APPMGR_DATA {
    uint32_t            _cb_count;                      // application count
    kdp_appmgr_cb_t     _callbacks[KDP_APPMGR_MAX_CB_COUNT];   // callback function pool
    uint32_t            _config_mode;                   // (0)standalone mode/ (1)companion mode
    uint32_t            _app_id;                        // app_id to run 

    //interface to output
    osThreadId_t        _tid_caller;                    // thread calling appMgr
    uint32_t            _threadNotifyFlag;              // flag to notify "_tid_output"  
    void*               _in_buf;                        // input buf btw Host_com
    void*               _out_buf;                       // out buf btw Host_com
    int32_t             _ret_code;                      // return code
} kdp_appmgr_data_t;
static kdp_appmgr_data_t s_kdp_appmgr_data;

osThreadId_t tid_appmgr;

/* =============================
 *  public functions 
 *  ============================
 */
int32_t kdp_appmgr_register_app(kdp_appmgr_cb_type_e type, char* description, kdp_appmgr_cb_fn_t cbf);
int32_t kdp_appmgr_run_app(uint32_t nCB_ID, void* paramIn, void* paramOut);
int32_t kdp_appmgr_get_app_count(void);

#if DEBUG
void    kdp_appmgr_dump_all_app(void);
#endif

/* =============================
 *  private functions 
 *  ============================
 */
static void _kdp_appmgr_dummy(void* p_in, void* p_out);
static void _kdp_appmgr_thread(void* argument);

//##############################################################################
// implementation
//##############################################################################

void _kdp_appmgr_dummy(void* p_in, void* p_out) {return;}

int32_t kdp_appmgr_get_app_count(void)
{
    return s_kdp_appmgr_data._cb_count;
}


void _kdp_appmgr_thread(void* argument)
{
    kdp_app_mgr_t *mgr = kdp_appmgr();
    
	  //run the 1st registered func(reserved for user application init function)
    s_kdp_appmgr_data._ret_code = 
        s_kdp_appmgr_data._callbacks[0].runfunc( s_kdp_appmgr_data._in_buf, 
                                                   s_kdp_appmgr_data._out_buf);
    
    for(;;) {
        osThreadFlagsWait(FLAG_APPMGR_RUN_APP, osFlagsWaitAll, osWaitForever);
        s_kdp_appmgr_data._ret_code = 
            s_kdp_appmgr_data._callbacks[s_kdp_appmgr_data._app_id].runfunc(
                                                   s_kdp_appmgr_data._in_buf, 
                                                   s_kdp_appmgr_data._out_buf);
        osThreadFlagsSet(s_kdp_appmgr_data._tid_caller, 
                         s_kdp_appmgr_data._threadNotifyFlag);
    }
}

void kdp_appmgr_init(
    kdp_appmgr_cb_fn_t p_init_func, 
    osThreadId_t p_tid_caller, 
    uint32_t p_threadDoneFlag,
    kdp_appmgr_config_mode_e p_config_mode)
{
    kdp_app_mgr_t *mgr = kdp_appmgr();
    if(p_init_func != NULL) {
        mgr->register_app(KDP_APPMGR_CB_TYPE_CPU, "init", p_init_func);

        if(p_tid_caller == NULL ) {
            s_kdp_appmgr_data._ret_code = 
                s_kdp_appmgr_data._callbacks[0].runfunc(s_kdp_appmgr_data._in_buf, 
                                                          s_kdp_appmgr_data._out_buf);
            return;
        }
    }
		else
		    mgr->register_app(KDP_APPMGR_CB_TYPE_CPU, "dummy func", (kdp_appmgr_cb_fn_t)&_kdp_appmgr_dummy);

    if(p_tid_caller && p_threadDoneFlag )  {
        tid_appmgr = osThreadNew(_kdp_appmgr_thread, NULL, NULL);

        s_kdp_appmgr_data._tid_caller = p_tid_caller;
        s_kdp_appmgr_data._threadNotifyFlag = p_threadDoneFlag;
    }

    s_kdp_appmgr_data._config_mode = p_config_mode; 
    
    return;
}

kdp_app_mgr_t* kdp_appmgr(void)
{
    static kdp_app_mgr_t mgr_singleton;
    static kdp_app_mgr_t* p_mgr_singleton = 0;
    if(0 == p_mgr_singleton)
    {
        p_mgr_singleton = &mgr_singleton;
#if DEBUG
        p_mgr_singleton->dump_all_app  = kdp_appmgr_dump_all_app;
#endif
        p_mgr_singleton->register_app = kdp_appmgr_register_app;
        p_mgr_singleton->run_app      = kdp_appmgr_run_app;
        p_mgr_singleton->get_app_count= kdp_appmgr_get_app_count;
    }

    return p_mgr_singleton;
}

int32_t kdp_appmgr_register_app(kdp_appmgr_cb_type_e type, char* description, kdp_appmgr_cb_fn_t cbf)
{
    kdp_appmgr_cb_t *pCB = 0;

    if(s_kdp_appmgr_data._cb_count == KDP_APPMGR_MAX_CB_COUNT)
    {
        dbg_msg("[ERR] aleady hit MAX_APP_CB_COUNT = %d\n", KDP_APPMGR_MAX_CB_COUNT);
        return -1;
    }
    
    pCB = &(s_kdp_appmgr_data._callbacks[s_kdp_appmgr_data._cb_count]);
    pCB->runfunc = cbf;
    pCB->cb_type = type;
#if DEBUG
    strncpy(pCB->description, description, MAX_APP_CB_DESCRIPTION);
    pCB->description[MAX_APP_CB_DESCRIPTION] = '\0';

    dbg_msg("[INFO] registed callback function[%d]:%s\n", s_kdp_appmgr_data._cb_count, pCB->description);
#endif
    dbg_msg("[INFO] registed callback function[%d]\n", s_kdp_appmgr_data._cb_count);
    s_kdp_appmgr_data._cb_count++;

    return (s_kdp_appmgr_data._cb_count - 1);
}
    
int32_t kdp_appmgr_run_app(uint32_t nCB_ID, void* p_input, void* p_output)
{
    kdp_app_mgr_t *mgr = kdp_appmgr();

    s_kdp_appmgr_data._app_id = nCB_ID;
    s_kdp_appmgr_data._in_buf = p_input;
    s_kdp_appmgr_data._out_buf = p_output;

    if(nCB_ID > s_kdp_appmgr_data._cb_count)
        return 1; //error 
    else {
        if(s_kdp_appmgr_data._tid_caller == NULL)  //call without caller thread
            s_kdp_appmgr_data._ret_code = 
                s_kdp_appmgr_data._callbacks[nCB_ID].runfunc(s_kdp_appmgr_data._in_buf, 
                                                              s_kdp_appmgr_data._out_buf);
        else {
            osThreadFlagsSet(tid_appmgr, FLAG_APPMGR_RUN_APP);
            osThreadFlagsWait(FLAG_HOST_COMM_APP_DONE, osFlagsWaitAll, osWaitForever);
        }
    }
    return s_kdp_appmgr_data._ret_code;
}

#if DEBUG
void kdp_appmgr_dump_all_app(void)
{
    uint32_t i;
    kdp_app_mgr_t *mgr = kdp_appmgr();

    dbg_msg("CB_ID\t\t(type)\tCB_Description\n");
    dbg_msg("------------------------------------\n");
    for(i=0 ;i<s_kdp_appmgr_data._cb_count; i++)
    {
        dbg_msg("%d\t\t%d\t%s\n", i, 
                    s_kdp_appmgr_data._callbacks[i].cb_type, 
                    s_kdp_appmgr_data._callbacks[i].description);
    } 
    dbg_msg("------------------------------------\n");

    return;
}
#endif

