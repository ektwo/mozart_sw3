/*
 * @name : v2k_cam.c
 * @brief : V2K sample user application
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */
#ifdef V2K_CAM_ENABLE
#include "v2k_cam.h"
#include <string.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <cmsis_os2.h>
#include <config/board_kdp520.h>
#include <framework/framework.h>
#include <framework/errno.h>
//#include <framework/v2k.h>
#include <framework/v2k_ioctl.h>
#include <framework/mm.h>
#include <framework/kdp_dev.h> 
#include <framework/mutex.h>
//#include <framework/kdp_dev_api.h> 
#include <media/v2k_image_sizes.h> // will be removed in the future.
#include <media/video_renderer.h>
#include "dbg.h"


/* wait for the host command to end the loop */
#define FRAME_INFINITE              (0xFFFFFFFFUL)
#define FRAME_COUNT_TO_END          1000
#define BUFFER_COUNT                2

#define V2K_SUCCEEDED(__ret__)      (0 == __ret__)
#define V2K_SUCCEEDED_RET(__ret__)  { if (!V2K_SUCCEEDED(__ret__)) return __ret__; }

#define FLAG_V2K_CAM_START          0x0001

#if V2K_CAM_ENABLE == SENSOR_HMX2056
#define VIDEO_WIDTH     TFT43_WIDTH//VGA_WIDTH
#define VIDEO_HEIGHT    TFT43_HEIGHT//VGA_HEIGHT
#define VIDEO_FORMAT 	V2K_PIX_FMT_RGB565 //V2K_PIX_FMT_RAW8_1
#elif V2K_CAM_ENABLE == SENSOR_HMXRICA_RAW8
#define VIDEO_WIDTH     HMX_RICA_WIDTH
#define VIDEO_HEIGHT    HMX_RICA_HEIGHT
#define VIDEO_FORMAT 	V2K_PIX_FMT_GREY
#endif

//#define PERPETUAL_MOTION

struct mutex cam_lock;

osThreadId_t tid_v2k_cam;

struct camera_buffer_info {
    void* start;
    unsigned int length;    
};

static unsigned long m_frame_number;
static struct camera_buffer_info *m_appbufs;
static struct v2k_requestbuffers m_req;
static unsigned int m_old_addr = 0;
static unsigned int m_curr_addr = 0;
static int m_fd;
static struct v2k_buffer m_apibuf;

void delay_ms(int msec);

static void _default_process_image(struct v2k_buffer *buf, 
        void *user_start, u32 user_length)
{
    ++m_frame_number;
    m_curr_addr = buf->type;
//    DSG("<%s> buffer[%d]: type=%x start=0x%x len=%u frames=%d ts=%llu", 
//        __func__, 
//        buf->index,
//        buf->type, 
//        (unsigned int)user_start, 
//        user_length,
//        m_frame_number,
//        buf->timestamp);
}
static fn_v2k_cam_newimage _on_newimage = _default_process_image;

unsigned int kdp_camera_get_buffer_cnt(void) 
{
    return m_req.count;
}

int kdp_camera_capture_frame(unsigned int *addr, unsigned int *size)
{
    int ret = 0;
    
    while (m_old_addr == m_curr_addr)
        delay_ms(1);
        
    mutex_lock(&cam_lock);
    
    if ((addr) && (size)) {
        *addr = (unsigned int)m_appbufs[0].start;
        *size = m_appbufs[0].length;
    }
    
    mutex_unlock(&cam_lock);    
    
    m_old_addr = m_curr_addr;
    
    return ret;
}

void register_v2k_cam_newimage(fn_v2k_cam_newimage fn) {
    if (fn)
        _on_newimage = fn;
}

static int _read_frame(int fd)
{
    int ret;
    struct v2k_buffer buf;

    ret = kdp_dev_ioctl(fd, V2K_VIOC_DQBUF, (void*)&buf);

    if (V2K_SUCCEEDED(ret)) {
        {
            mutex_lock(&cam_lock);
            
            _on_newimage(&buf, 
                m_appbufs[buf.index].start, 
                m_appbufs[buf.index].length);
            
            mutex_unlock(&cam_lock);
        }
        ret = kdp_dev_ioctl(fd, V2K_VIOC_QBUF, (void*)&buf);
    }

    return ret;
}

static int open_device(const char *dev_name) {
    return kdp_dev_open(dev_name);
}

static void close_device(int fd) {
    if (fd)
        kdp_dev_close(fd);
}

static int get_device_info(int fd)
{
    int ret;
    struct v2k_capability cap;

    ret = kdp_dev_ioctl(fd, V2K_VIOC_QUERYCAP, (void*)&cap);
    if (V2K_SUCCEEDED(ret)) {
        DSG("===== Capability Informations ====="); 
        DSG("   driver: %s", cap.driver); 
        DSG("   desc: %s", cap.desc); 
        DSG("   version: %08X", cap.version);
    } else {
        DSG("v2k_cam: Query device capabilities ERROR(%d)", ret);
    } 

    return ret;
}

static int set_frame_format(int fd)
{
    int ret;
    struct v2k_format fmt;
    memset(&fmt, 0, sizeof(fmt));

    fmt.width = VIDEO_WIDTH;
    fmt.height = VIDEO_HEIGHT;
    fmt.pixelformat = VIDEO_FORMAT;
    fmt.field = V2K_FIELD_ANY; //V2K_FIELD_INTERLACED;	

    ret = kdp_dev_ioctl(fd, V2K_VIOC_S_FMT, (void*)&fmt);
    if (V2K_SUCCEEDED(ret)) {
        DSG("v2k_cam: set video format, OK");        
    } else {
        DSG("v2k_cam: set video format, ERROR(%d)", ret);
    }

    return ret;
}

static int get_frame_format(int fd)
{
    int ret;
    struct v2k_format fmt;
    memset(&fmt, 0, sizeof(fmt));

    ret = kdp_dev_ioctl(fd, V2K_VIOC_G_FMT, (void*)&fmt);
    if (V2K_SUCCEEDED(ret)) {    
        DSG("===== Stream Format Informations =====");         
        DSG("   width: %d", fmt.width); 
        DSG("   height: %d", fmt.height); 

        char fmtstr[8]; 
        memset(fmtstr, 0, 8); 
        memcpy(fmtstr, &fmt.pixelformat, 4); 
        DSG("   pixelformat: %s", fmtstr); 
        DSG("   field: %d", fmt.field); 
        DSG("   bytesperline: %d", fmt.bytesperline); 
        DSG("   sizeimage: %d", fmt.sizeimage); 
        DSG("   colorspace: %d", fmt.colorspace);         
    } else {
        DSG("v2k_cam: set video format, ERROR(%d)", ret);
    }

    return ret;
}

static int init_mem_mapping(int fd)
{
    int ret;
    int i;
    struct v2k_buffer buf;

    m_req.count = BUFFER_COUNT;
    ret = kdp_dev_ioctl(fd, V2K_VIOC_REQBUFS, (void*)&m_req);
    if (V2K_SUCCEEDED(ret)) {        
        m_appbufs = (struct camera_buffer_info*)calloc(m_req.count, 
            sizeof(struct camera_buffer_info)); 
        if (!m_appbufs) {
            return -ENOMEM;
        }

        for (i = 0; i < m_req.count; ++i) {
            buf.index = i;
            buf.length = 0;
            ret = kdp_dev_ioctl(fd, V2K_VIOC_QUERYBUF, (void*)&buf);
            if (!V2K_SUCCEEDED(ret)) {
                DSG("v2k_cam: Query the status of a buffer, ERROR(%d)", ret);
                break;
            }

            m_appbufs[i].length = buf.length;
            m_appbufs[i].start = (void*)kdp_mmap(buf.length, 0, fd, buf.offset);
            DSG("<%s> i=%d buf.offset=0x%x length=%d start=0x%x", __func__, i, buf.offset, m_appbufs[i].length, m_appbufs[i].start);
            if (MAP_FAILED == m_appbufs[i].start) {
                ret = -1;
                break;
            }
        }

    } else {
        DSG("v2k_cam: Initiate Memory Mapping, ERROR(%d)", ret);
    }

    return ret;  
}

static int start_capturing(int fd)
{
    int ret;
    unsigned int i;

    for (i = 0; i < BUFFER_COUNT; ++i)
    {
        m_apibuf.index = i;

        ret = kdp_dev_ioctl(fd, V2K_VIOC_QBUF, (void*)&m_apibuf);
        if (!V2K_SUCCEEDED(ret)) {   
            DSG("v2k_cam: Exchange a buffer with the driver, ERROR(%d)", ret);
            break;
        }
    }

    ret = kdp_dev_ioctl(fd, V2K_VIOC_STREAMON, 0);
    if (!V2K_SUCCEEDED(ret)) {  
        DSG("v2k_cam: Start streaming I/O, ERROR(%d)", ret);
    }

    return ret;
}

static int cam_loop(int fd)
{
    unsigned int count = FRAME_COUNT_TO_END;
    int ret;

    do {        
        for (;;) {
            
            ret = _read_frame(fd);
            if (V2K_SUCCEEDED(ret))
                break;

            switch (ret) {
                case EAGAIN:
                    //sleep
                    continue;
                default:
                    DSG("v2k_cam: failed to _read_frame");
                    return ret;
            }
        }
    
#if FRAME_COUNT_TO_END == FRAME_INFINITE
    } while (1);
#else
    } while (--count);
#endif
    
    return 0;
}

static int stop_capturing(int fd)
{
    int ret;

    ret = kdp_dev_ioctl(fd, V2K_VIOC_STREAMOFF, 0);
    if (ret) {
        DSG("v2k_cam: Stop streaming I/O, OK");   
    } else {
        DSG("v2k_cam: Stop streaming I/O, ERROR(%d)", ret);   
    }

    return ret;
}

int v2k_cam_run(void)
{
    const char dev_name[16] = "kdp_dev/video0";  
    m_fd = open_device(dev_name);
    if (m_fd <= 0) {
        DSG("v2k_cam : Failed to open device (%s)", dev_name);
        return -1;
    }

    V2K_SUCCEEDED_RET(get_device_info(m_fd));
    V2K_SUCCEEDED_RET(set_frame_format(m_fd)); 
    V2K_SUCCEEDED_RET(get_frame_format(m_fd));  
    V2K_SUCCEEDED_RET(init_mem_mapping(m_fd));
    V2K_SUCCEEDED_RET(start_capturing(m_fd)); 
    V2K_SUCCEEDED_RET(open_video_renderer(VIDEO_WIDTH, VIDEO_HEIGHT));
    V2K_SUCCEEDED_RET(cam_loop(m_fd));
    V2K_SUCCEEDED_RET(stop_capturing(m_fd));
    
    if (m_appbufs)
        free(m_appbufs);

    close_device(m_fd);

    return 0;
}


extern int FTLCDC210_Test_Main(void);
extern void MIPI_main(void);
extern void ip_test_main(void);


static void v2k_cam_thread(void *argument)
{
    int32_t flags;

    start_framework();

    for (;;) {
        flags = osThreadFlagsWait(FLAG_KERNEL_INIT_READY_EVT, osFlagsWaitAny, osWaitForever);
        if (flags & (FLAG_KERNEL_INIT_READY_EVT)) {
        #ifdef PERPETUAL_MOTION            
            v2k_cam_run();
        #endif
        }

        osThreadFlagsClear(flags);
    }
}
#endif

int v2k_camera_init(void)
{

#ifdef V2K_CAM_ENABLE
    tid_v2k_cam = osThreadNew(v2k_cam_thread, NULL, NULL);
    //if (tid_v2k_cam == NULL) DSG("ERR: @tid_v2k_cam thread create error()"); 
    //DSG(" v2k_camera_init()"); 
#endif
    return 0;
}

int kdp_camera_open(void)
{
    int ret = 0;
    const char dev_name[16] = "kdp_dev/video0";  
    m_fd = open_device(dev_name);
    if (m_fd <= 0) {
        DSG("v2k_cam : Failed to open device (%s)", dev_name);
        return -1;
    }

    V2K_SUCCEEDED_RET(get_device_info(m_fd));
    V2K_SUCCEEDED_RET(set_frame_format(m_fd)); 
    V2K_SUCCEEDED_RET(get_frame_format(m_fd));  
    V2K_SUCCEEDED_RET(init_mem_mapping(m_fd));    

    return ret;
}

int kdp_camera_close(void)
{
    if (m_appbufs)
        free(m_appbufs);

    close_device(m_fd);
    
    return 0;
}

int kdp_camera_start(unsigned int flags)
{
    int ret = start_capturing(m_fd);
    if (flags & KDP_CAMERA_START_WITH_PREVIEW)
        V2K_SUCCEEDED_RET(open_video_renderer(VIDEO_WIDTH, VIDEO_HEIGHT));
    return ret;
}

int kdp_camera_stop(void)
{
    return stop_capturing(m_fd); 
}

int kdp_camera_buffer_prepare(void)
{
    int ret = 0;

    ret = kdp_dev_ioctl(m_fd, V2K_VIOC_QBUF, (void*)&m_apibuf);
    return ret;
}

int kdp_camera_buffer_capture(unsigned int *addr, unsigned int *size)
{
    int ret = 0;

    if ((addr) && (size)) {
        ret = kdp_dev_ioctl(m_fd, V2K_VIOC_DQBUF, (void*)&m_apibuf);
        if (V2K_SUCCEEDED(ret)) {
            {
                *addr = (unsigned int)m_appbufs[0].start;
                *size = m_appbufs[0].length;
            }
        }
    }

    return ret;
}
