/* -------------------------------------------------------------------------- 
 * @file msg.c 
 *      pc command message threads (Uart/USB)
 *---------------------------------------------------------------------------*/
#include "base.h"
#include "msg.h"
#include "com.h"
#include "crc.h"
#include "io.h"
#include "string.h"
#include "dbg.h"
#include "host_com.h"


u8 msg_rbuf[MSG_DATA_BUF_MAX + sizeof(MsgHdr)+ sizeof(RspPram) + 4];  // allow u16 crc value
u8 msg_tbuf[MSG_DATA_BUF_MAX + sizeof(MsgHdr)+ sizeof(RspPram) + 4];

/*
 * @brief msg_read_final(), check pc command message
 * @param buf buffer for uart read
 */
int msg_read_final(u8 *buf)
{
    u8 val;
    int len = 0;
    while (msg_getc(MSG_PORT, &val, 1000) == 0) {
        *(buf++) = val;
        len++;
        if (len > MSG_DATA_BUF_MAX + (sizeof(MsgHdr)+sizeof(RspPram))) {
            fLib_printf("Error, Data buffer out of max(%d)\n", MSG_DATA_BUF_MAX);
            return (-1);
        }
    }
    return len; 
}

#if PACKET
/*
 * @brief msg_read(), process the incoming uart message and checks the packet format
 * @param buf buffer for uart read
 */
int msg_read(u8 *buf, int size)
{
    MsgHdr *msghdr = (MsgHdr*)msg_rbuf;
    RspPram *msgrsp_arg;
    u32 crc16;
	int length;
	
	if (msghdr->preamble != MSG_HDR_CMD)
	{
        dbg_msg("BAD Preamble %d", size);
        return -1;
	}
	if (msghdr->pSize & PKT_CRC_FLAG)
    {
	    length = msghdr->pSize & 0x0FFF;  // mask off packet flags
        crc16 = gen_crc16(buf, length);  // compute crc value
        if (crc16 != (buf[length+0]+(buf[length+1]<<8)))
	    {
	        // send CRC ERROR packet, format the message in msg_rbuf first
            dbg_msg("BAD CRC [%d] crc16=%x : %x\r\n", length, crc16, buf[length+3]+(buf[length+3]<<8));
            msgrsp_arg = (RspPram*) msg_rbuf + sizeof(MsgHdr);
            msgrsp_arg->error = CMD_CRC_ERR;
            msgrsp_arg->bytes = 0;
            msg_pack(msghdr->cmd, NULL, 0);  // send crc error packet
	        return -1;
	    }
	}
	cmd_parser(buf, size);  // if there are more than one command, we only process the first one
	return size;
}

/**
 * @brief msg_write(), write msg to PC by uart
 *        also format uart packet here
 * @param buf buffer data to write
 * @param len data length
 */
int msg_write(u8 *buf, int len, int crc_flag)
{
    MsgHdr *msghdr;
    u32 crc16, *crc;

    crc = (u32*)&buf[len];  // identify the crc location
    msghdr = (MsgHdr*) buf;
    msghdr->preamble = MSG_HDR_RSP;
    if (crc_flag)
    {
        msghdr->pSize = len | PKT_CRC_FLAG;  // packet + crc(4B) - pkt header (2B)
        crc16 = gen_crc16(buf, len);
        *crc = crc16;  // store crc
        len += 4;  // adjust len to include crc
    }
    else
        msghdr->pSize = len - 2;
	
    for (int i = 0; i < len; i++) {
        msg_putc(MSG_PORT, buf[i]);
    }           
    return 0;    
}

/**
 * @brief msg_pack(), pack message and send out, always use crc
 * @param cmd message command
 * @param buf data buffer
 * @param len length of data buffer, if len < 0, don't flip the cmd
 */
void msg_pack(u16 cmd, u8 *buf, int len)
{
    MsgHdr *msghdr;
	CmdPram *msgcmd_arg;
	RspPram *msgrsp_arg;
    if (len > 0) {  // copy stuff beyond len and address
        memcpy((msg_tbuf + (sizeof(MsgHdr)+sizeof(RspPram))), buf, len);
    };
        
    msghdr = (MsgHdr *) msg_tbuf;  // msghdr now points to tbuf
    msgcmd_arg = (CmdPram*) (msg_rbuf + sizeof(MsgHdr));
    msgrsp_arg = (RspPram*) (msg_tbuf + sizeof(MsgHdr));
     if (len >= 0) {
        msghdr->cmd = cmd | 0x8000;
        msghdr->mSize = len + sizeof(RspPram);  // minimum msg payload include RspPram
        msgrsp_arg->error = msgcmd_arg->addr;
        msgrsp_arg->bytes = msgcmd_arg->len;
        len += sizeof(MsgHdr)+sizeof(RspPram);
    }
    else {
        msghdr->cmd = cmd;
        msghdr->mSize = 0;  // no message payload
        len = sizeof(MsgHdr);  // just send msg header
    }
    msg_write(msg_tbuf, len, 1);  // use crc
    return;
}
#else
int msg_read(u8 *buf, int size)
{
	cmd_parser(buf, size);
	return size;
}
int msg_write(u8 *buf, int len)
{    
    for (int i = 0; i < len; i++) {
        msg_putc(MSG_PORT, buf[i]);
    }           
    return 0;    
}
void msg_pack(u16 cmd, u8 *buf, int len)
{
    MsgHdr *msghdr;
    if (len > 0) {
        memcpy((msg_tbuf + MSG_HDR_SIZE), buf, len);
    };
        
    msghdr = (MsgHdr *) msg_tbuf;
    msghdr->header = MSG_HDR_VAL;    
    msghdr->cmd = cmd;
    msghdr->addr = 0;
    msghdr->len = len;
    msghdr->crc16 = gen_crc16((const u8 *) (msg_tbuf + 4), MSG_HDR_SIZE + len - 4);
    //fLib_printf("Send uart_len = %d\n", MSG_HDR_SIZE + len);
    //buf_dbg_out(msg_tbuf, MSG_HDR_SIZE + len);
    msg_write(msg_tbuf, MSG_HDR_SIZE + len);
    return;
}
#endif
/**
 * @brief msg_putc, send 1 byte data to message com port 
 */
void msg_putc(DRVUART_PORT port_no, char ch)
{
    CheckTxStatus(port_no);
    outw(UART_PORT[port_no] + SERIAL_THR, ch);
}


/**
 * @brief msg_getc, get 1 byte data from message com port 
 */
int msg_getc(DRVUART_PORT port_no, u8 *value, unsigned long timeout)
{
    /* wait until rx status ready */
    while((GetUartStatus(port_no) & SERIAL_IER_DR) == 0x0) {
        if(timeout != 0) {/* 0 means never timeout */ 
            timeout --;/* count down timeout value */
            if(timeout == 0) {/* 0 means timeout */
                return (-1);/* return timeout value */
            }
        }
    }
    /* return rx character */
    *value = inw(UART_PORT[port_no]+SERIAL_RBR);
    return (0);
}

