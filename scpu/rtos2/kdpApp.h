#ifndef __KDP_APP_H_
#define __KDP_APP_H_

#include <stdint.h>
#include "kdpModelMgr.h"   //for FLASH address information 
#include "base.h"
#include "ipc.h"
#include "memxfer.h"

//todo: need to move to a shared place
#ifdef LOG_ENABLE
extern uint32_t t_start__;
extern uint32_t t_end__;
#define dbg_profile_start()       t_start__=osKernelGetTickCount(); 
#define dbg_profile_stop(tag)    do{                       \
        t_end__=osKernelGetTickCount();                    \
        dbg_msg("[TIME] <%s> elapses %d ms\n", tag,    \
            (t_end__-t_start__));                          \
    } while(0)
#else
#define dbg_profile_start() 
#define dbg_profile_stop(tag) 
#endif

extern const struct s_kdp_memxfer kdp_memxfer_module;
//==============================================================================
// define infromation of user data in flash
//==============================================================================
#ifdef DBG_ALONE
#define IMAGE_SIZE 692224    // 416x416x4
                                  
#define KDP_APP_IMG_ALIGNMENT 32
#define KDP_APP_IMG_BUF_COUNT 1 

#define KDP_APP_IMG_FORMAT BIT(19)      //RGBA888 without sub-128, bypass preprocess
#define KDP_APP_IMG_COL 416
#define KDP_APP_IMG_ROW 416
#define KDP_APP_IMG_CH 4
#define KDP_APP_IMG_SIZE 692224         // 416x416x4

//useless -------------
#define KDP_APP_FLASH_ADDR_DB 0

#define FID_DB_SIZE 0
#define DEPTH_SIZE 0
//#define IMAGE_SIZE 424224         // 864x491 NIR
//#define DEPTH_SIZE 424224         // 864x491 depth
//--------------------

//------------------------------------------------------------------------------
#else                                    //for FID/DB application
/* DDR memory table 
 *  --- controlled by model manager ---
 *  .bin files 
 *  --- controlled by kdpApp (User defined data)----
 *  FID database
 *  NIR image frame buffer
 *  Depth data frame buffer
 */

#define KDP_APP_FLASH_ADDR_DB     0x39a000 

#define FID_DB_SIZE 184320        // FID database size: 180 KB = 15ppl * 12kb/each
#define IMAGE_SIZE 614400         // 640x480
#define DEPTH_SIZE 614400         // 640x480
//#define IMAGE_SIZE 424224         // 864x491 NIR
//#define DEPTH_SIZE 424224         // 864x491 depth
                                  
#define KDP_APP_IMG_ALIGNMENT 32
#define KDP_APP_IMG_BUF_COUNT 2

#define KDP_APP_IMG_FORMAT 0x80000060    //rdb565
#define KDP_APP_IMG_COL 640
#define KDP_APP_IMG_ROW 480
#define KDP_APP_IMG_CH 3
#define KDP_APP_IMG_SIZE 614400   // 640x480x3 RGB565
#endif
//==============================================================================

enum kdp_app_status_code_e{
    KDP_APP_OK = 0,
    KDP_APP_UNKNOWN_ERR,

    KDP_APP_FID_CODES = 0x100,    //codes defined in kdpAppFID.h
    KDP_APP_DB_CODES  = 0x200,    
    KDP_APP_OBJ_CODES = 0x300 
};

struct kdp_app_image_info {
    uint32_t format;
    uint32_t col;
    uint32_t row;
    uint32_t ch;
};;

//kdpApp generic 
//---------------------------------
uint32_t kdpapp_get_ddr_addr(uint32_t* table, uint32_t type);
void kdpapp_query_img_transfer_setting(uint32_t* un_buf_count_p, 
                                       uint32_t* un_buf_head_addr_p, 
                                       uint32_t* img_size_p, 
                                       uint32_t* alignment_p);

//for DMA test
//int32_t kdpapp_test_img(void* p_inptr, void* p_outptr);
//int32_t kdpapp_test_dma_flash_to_ddr(uint32_t ddr_dest_addr, uint32_t flash_src_offset, uint32_t size);
//int32_t kdpapp_test_dma_ddr_to_flash(uint32_t ddr_src_addr, uint32_t flash_dest_offset, uint32_t size);

#endif
