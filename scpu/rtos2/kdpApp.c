#include "kdpApp.h"
#include <stdint.h>

void kdpapp_query_img_transfer_setting(
    uint32_t* pn_buf_count_p, 
    uint32_t* pn_buf_base_addr_p, 
    uint32_t* pn_img_size_p, 
    uint32_t* pn_alignment_in_bit_p)
{
    *pn_buf_count_p         = KDP_APP_IMG_BUF_COUNT; 
    *pn_buf_base_addr_p     = kdp_modelmgr()->l_ddr_addr_model_end;  
    *pn_img_size_p          = KDP_APP_IMG_SIZE; 
    *pn_alignment_in_bit_p  = KDP_APP_IMG_ALIGNMENT;
}

uint32_t kdpapp_get_ddr_addr(uint32_t* table, uint32_t type)
{
    return table[type];
}

