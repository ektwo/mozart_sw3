/*
 * Kneron Host Communication driver
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */

#ifndef HOST_COM_H
#define HOST_COM_H
#include "types.h"
#include "cmsis_os2.h"

#define FLAG_HOST_COMM_START			BIT(0) 
#define FLAG_HOST_COMM_ISR  			BIT(1)
#define FLAG_HOST_COMM_TIMER			BIT(2)
#define FLAG_HOST_COMM_APP_DONE         BIT(3)      //for tid_appmgr
#define FLAG_HOST_COMM_USB_DONE         BIT(4)

extern osThreadId_t tid_host_comm;

// error code for cmd parser
#define DEVICE_ID       0x520  // Kneron device id for Mozart
#define FW_VERSION      0x010000
#define NO_ERROR        0
#define NO_MATCH        1
#define RSP_UKNOWN_CMD  0x100
#define BAD_CONFIRMATION 2  // CMD_RESET RESPONSE
#define BAD_MODE         1
#define USB_ERROR       1  // USB datat transfer error

/* API */
void host_com_init(void);
int cmd_parser(u8 *buf, int len);
u32 usb_mem_read(u32 dst_address, u32 read_size);
#endif
