/*
 * Kneron NPU driver for KDP520
 *
 * Copyright (C) 2018 Kneron, Inc. All rights reserved.
 *
 */
 
#include <string.h>
#include "cmsis_os2.h"
#include "io.h"
#include "memory.h"
#include "ipc.h"
#include "scu_ipc.h"
#include "kdp_com.h"
#include "kdpAppMgr.h"
#include "msg.h"
#include "dbg.h"


#define FLAG_NCPU_COMM_START			0x0001
#define FLAG_NCPU_COMM_ISR				0x0002
#define FLAG_NCPU_COMM_TIMER			0x0004

#define DEMO_RESULT_BUF                 (DDR_MEM_BASE + 0x4000000)      // will use dynamic memory
#define DEMO_RESULT_BUF2                (DEMO_RESULT_BUF + 0x10000)
#define DEMO_RESULT_BUF3                (DEMO_RESULT_BUF2 + 0x10000)

#define INPUT_BUF_ADDR2                 0x70000000
#define INPUT_BUF_LEN2                  0x100000        // 1MB
#define OUTPUT_BUF2                     (INPUT_BUF_ADDR2 + INPUT_BUF_LEN2)
#define OUTPUT_BUF_LEN2                 0x100000        // 1MB

#define NCPU_IRQ					51


osThreadId_t tid_ncpu_comm;

struct scpu_to_ncpu *out_comm_p;
struct ncpu_to_scpu *in_comm_p;

int demo_mode;
int demo_run_once;

struct kdp_image    image_s;

uint32_t    tick_clock;

int32_t     demo_num_images;


#if 0
static int kdp_com_get_result(void **result_buf_p)
{
    struct yolo_result *yolo_p;
    struct facedet_result *fd_p;
    struct landmark_result *lm_p;
    struct fr_result *fr_p;
    struct imagenet_result *imagenet_p;
    int i, len, model_id, status, model_slot_index;
    uint32_t tick_elapsed;
    uint32_t    tick_start;
    uint32_t    tick_end;

    i = in_comm_p->img_index_done;
    tick_start = out_comm_p->raw_images[i].tick_start;
    tick_end = osKernelGetTickCount();
    
    if (tick_end < tick_start)
        tick_elapsed = ~(tick_end - tick_start) + 1;
    else
        tick_elapsed = tick_end - tick_start;

    model_slot_index = out_comm_p->model_slot_index;      // assume same model in and out
    model_id = out_comm_p->models_type[model_slot_index];
    status = in_comm_p->img_results[i].status;

    dbg_msg("[%d] status %d: tick[in/out]: %d, %d (elapsed %d) model[%d]=%d\n", i, status, 
        tick_start, tick_end, tick_elapsed, model_slot_index, model_id);

    if (status == IMAGE_STATE_NPU_DONE) {
        return 0;
    } else if (status != IMAGE_STATE_DONE) {
        dbg_msg("kdp_com_get_result: ERR: image [%d] status (%d) wrong\n", i, in_comm_p->img_results[i].status);
        return -1;
    }

    len = 0;
    switch (model_id) {
        case KNERON_FR_RES50:
        case KNERON_FR_RES34:
        case KNERON_FR_VGG10:
            len += sizeof(struct fr_result);
            fr_p = (struct fr_result *)out_comm_p->raw_images[i].results[model_slot_index].result_mem_addr;
            *result_buf_p = fr_p;
            dbg_msg("FR result: %d (0x%x)\n", len, fr_p);
            break;
        case KNERON_LM_5PTS:
            len += sizeof(struct landmark_result);
            lm_p = (struct landmark_result *)out_comm_p->raw_images[i].results[model_slot_index].result_mem_addr;
            *result_buf_p = lm_p;
            dbg_msg("LM result: [%d %d] [%d %d] [%d %d] [%d %d] [%d %d]\n",
                    (int)lm_p->marks[0].x, (int)lm_p->marks[0].y,
                    (int)lm_p->marks[1].x, (int)lm_p->marks[1].y,
                    (int)lm_p->marks[2].x, (int)lm_p->marks[2].y,
                    (int)lm_p->marks[3].x, (int)lm_p->marks[3].y,
                    (int)lm_p->marks[4].x, (int)lm_p->marks[4].y);
            break;
        case KNERON_FDSMALLBOX:
            len += sizeof(struct facedet_result);
            fd_p = (struct facedet_result *)out_comm_p->raw_images[i].results[model_slot_index].result_mem_addr;
            *result_buf_p = fd_p;
            dbg_msg("FD result[%d] xywh: %d %d %d %d\n", fd_p->len, fd_p->xywh[0], fd_p->xywh[1], fd_p->xywh[2], fd_p->xywh[3]);
            break;
        case TINY_YOLO_VOC:
        case KNERON_TINY_YOLO_PERSON:
            // Get All bounding boxes
            yolo_p = (struct yolo_result *)out_comm_p->raw_images[i].results[model_slot_index].result_mem_addr;
            len += sizeof(struct yolo_result);                              // 1 box included
            if (yolo_p->box_count >=1)
                len += (yolo_p->box_count - 1) * sizeof(struct bounding_box);   // the rest
            else
                len -= sizeof(struct bounding_box);     // -1 for 0 box_count
            *result_buf_p = yolo_p;
            dbg_msg("YOLO result: %d bounding boxes\n", yolo_p->box_count);
            break;
        case IMAGENET_CLASSIFICATION_RES50:
        case IMAGENET_CLASSIFICATION_MOBILENET_V2:
            // Get Top 1
            len += sizeof(struct imagenet_result);
            imagenet_p = (struct imagenet_result *)out_comm_p->raw_images[i].results[model_slot_index].result_mem_addr;
            *result_buf_p = imagenet_p;
            dbg_msg("Imagenet result: [%d] score %f\n", imagenet_p->index, imagenet_p->score);
            break;
    }

    in_comm_p->img_results[i].status = IMAGE_STATE_INACTIVE;
    out_comm_p->raw_images[i].state = IMAGE_STATE_INACTIVE;
    return len;
}
#endif

static void kdp_com_demo_run_image(void)
{
    if (demo_mode == 0)
        return;
    
    // Start to run saved images
    kdp_com_run_image(&image_s);
}

static int kdp_measure_time(void)
{
    int i, model_id, model_slot_index;
    int tick_elapsed, diff1, diff2, diff3;

    i = in_comm_p->img_index_done;
    model_slot_index = out_comm_p->model_slot_index;
    model_id = out_comm_p->models_type[model_slot_index];

    tick_elapsed = osKernelGetTickCount()
            - out_comm_p->raw_images[i].tick_start;
    diff1 = out_comm_p->raw_images[i].tick_end_pre
            - out_comm_p->raw_images[i].tick_start_pre;
    diff2 = out_comm_p->raw_images[i].tick_end_npu
            - out_comm_p->raw_images[i].tick_start_npu;
    diff3 = out_comm_p->raw_images[i].tick_end_post
            - out_comm_p->raw_images[i].tick_start_post;

    dbg_msg("[Model type %d] total %d: pre/npu/post: %d/%d/%d ms\n",
                model_id, tick_elapsed, diff1, diff2, diff3);
	  return 0;
}

static void ncpu_comm_thread(void *argument)
{
    int32_t flags;

    tick_clock = osKernelGetTickFreq();
 
    for (;;) {
        flags = osThreadFlagsWait(FLAG_NCPU_COMM_ISR, osFlagsWaitAny, osWaitForever);
        osThreadFlagsClear(flags);

        if (flags & (FLAG_NCPU_COMM_ISR)) {
            dbg_msg("scpu: got NCPU interrupt\n");

            kdp_measure_time();


            osThreadFlagsSet(tid_appmgr, FLAG_APPMGR_FROM_NCPU);

            if (flags & FLAG_NCPU_COMM_ISR) {
                scu_ipc_clear_from_ncpu_int();
                NVIC_ClearPendingIRQ((IRQn_Type)NCPU_IRQ);
                NVIC_EnableIRQ((IRQn_Type)NCPU_IRQ);
            }
        }
    }
}

static void NCPU_IRQHandler(void)
{
    osThreadFlagsSet(tid_ncpu_comm, FLAG_NCPU_COMM_ISR);
    NVIC_DisableIRQ((IRQn_Type)NCPU_IRQ);
}

void kdp_com_set_model_slot_index(uint32_t n_index)
{
    out_comm_p->model_slot_index = n_index;
    return;
}

/**
 * @brief set model information to IPC(scpu_to_ncpu)
 * @param(in) model_p : model bin address infomation
 * @param(in) p_target_index: target slot to save
 * @param(in) model_type:
 * @param(in) model_info_idx: the index of model bin address information array
 * @return N/A
 */
void kdp_com_set_model(struct kdp_model *model_p, uint32_t p_model_info_index, int32_t p_slot_index)
{
    if(p_slot_index >= MULTI_MODEL_MAX)
    {
        dbg_msg("[ERR] too many active model is set\n");
        return;
    }
    
    out_comm_p->models[p_slot_index] = *(model_p + p_model_info_index);
    //out_comm_p->models[i].input_mem_addr    = model_p->input_mem_addr;
    //out_comm_p->models[i].input_mem_len     = model_p->input_mem_len;
    //out_comm_p->models[i].output_mem_addr   = model_p->output_mem_addr;
    //out_comm_p->models[i].output_mem_len    = model_p->output_mem_len;
    //out_comm_p->models[i].buf_addr          = model_p->buf_addr;
    //out_comm_p->models[i].buf_len           = model_p->buf_len;
    //out_comm_p->models[i].cmd_mem_addr      = model_p->cmd_mem_addr;
    //out_comm_p->models[i].cmd_mem_len       = model_p->cmd_mem_len;
    //out_comm_p->models[i].weight_mem_addr   = model_p->weight_mem_addr;
    //out_comm_p->models[i].weight_mem_len    = model_p->weight_mem_len;
    //out_comm_p->models[i].setup_mem_addr    = model_p->setup_mem_addr;
    //out_comm_p->models[i].setup_mem_len     = model_p->setup_mem_len;

    dbg_msg("[INFO] kdp_com_set_model: modelinfo[%d] -> slot=%d\n", p_model_info_index, p_slot_index);
    dbg_msg(" .input_mem_addr = 0x%x\n", (model_p + p_model_info_index)->input_mem_addr);
    dbg_msg(" .input_mem_len  = 0x%x\n", (model_p + p_model_info_index)->input_mem_len);
    dbg_msg(" .output_mem_addr= 0x%x\n", (model_p + p_model_info_index)->output_mem_addr);
    dbg_msg(" .output_mem_len = 0x%x\n", (model_p + p_model_info_index)->output_mem_len);
    dbg_msg(" .buf_addr       = 0x%x\n", (model_p + p_model_info_index)->buf_addr);
    dbg_msg(" .buf_len        = 0x%x\n", (model_p + p_model_info_index)->buf_len);
    dbg_msg(" .cmd_mem_addr   = 0x%x\n", (model_p + p_model_info_index)->cmd_mem_addr);
    dbg_msg(" .cmd_mem_len    = 0x%x\n", (model_p + p_model_info_index)->cmd_mem_len);
    dbg_msg(" .weight_mem_addr= 0x%x\n", (model_p + p_model_info_index)->weight_mem_addr);
    dbg_msg(" .weight_mem_len = 0x%x\n", (model_p + p_model_info_index)->weight_mem_len);
    dbg_msg(" .setup_mem_addr = 0x%x\n", (model_p + p_model_info_index)->setup_mem_addr);
    dbg_msg(" .setup_mem_len  = 0x%x\n", (model_p + p_model_info_index)->setup_mem_len);

    if (p_slot_index+1 > out_comm_p->num_models)
        out_comm_p->num_models = p_slot_index+1;
}

int kdp_com_run_image(struct kdp_image *image_p)
{
    int i, j, w, in_window;
    static int model_in_test, inited;

    i = out_comm_p->active_img_index;
    if (out_comm_p->raw_images[i].state != IMAGE_STATE_INACTIVE) {
        j = (i + 1) % IPC_IMAGE_MAX;
        if (out_comm_p->raw_images[j].state != IMAGE_STATE_INACTIVE) {
            /* Two+ images forward are active: wrong! */
            dbg_msg("kdp_com_run_image: Wrong state: [%d]=%d & [%d]=%d\n", i, 
                out_comm_p->raw_images[i].state, j, out_comm_p->raw_images[j].state);
            return -1;
        }
        out_comm_p->active_img_index = i = j;
    }

    /* Check the window backward */
    j = (i - 1) % IPC_IMAGE_MAX;
    in_window = 0;
    for (w = 0; w < IPC_IMAGE_ACTIVE_MAX; w++) {
        if (j < 0)
            j += IPC_IMAGE_MAX;
        if (out_comm_p->raw_images[j].state == IMAGE_STATE_INACTIVE) {
            in_window = 1;
            break;
        }
        j = (j - 1) % IPC_IMAGE_MAX;
    }

    if (!in_window) {
        dbg_msg("kdp_com_run_image: window (%d) is full before [%d]\n", IPC_IMAGE_ACTIVE_MAX, i);
        return -1;
    }

    out_comm_p->input_count++;

    /* Pass raw image size parameters here */
    out_comm_p->raw_images[i].input_row = image_p->image_row;
    out_comm_p->raw_images[i].input_col = image_p->image_col;
    out_comm_p->raw_images[i].input_channel = image_p->image_ch;
    out_comm_p->raw_images[i].format = image_p->image_format;

    out_comm_p->raw_images[i].image_mem_addr = image_p->image_mem_addr;
    out_comm_p->raw_images[i].image_mem_len = image_p->image_mem_len;
    out_comm_p->raw_images[i].state = IMAGE_STATE_ACTIVE;
    out_comm_p->raw_images[i].seq_num = out_comm_p->input_count;
    out_comm_p->raw_images[i].ref_idx = i;
    out_comm_p->raw_images[i].tick_start = osKernelGetTickCount();

    if (!inited) {
        out_comm_p->raw_images[i].results[0].result_mem_addr = (uint32_t)DEMO_RESULT_BUF;
        out_comm_p->raw_images[i].results[1].result_mem_addr = (uint32_t)DEMO_RESULT_BUF2;
        out_comm_p->raw_images[i].results[2].result_mem_addr = (uint32_t)DEMO_RESULT_BUF3;

        /* Initialize 2nd input buffer for ncpu working buffer */
        out_comm_p->input_mem_addr2 = INPUT_BUF_ADDR2;
        out_comm_p->input_mem_len2 = INPUT_BUF_LEN2;
        inited = 1;
    }

    if (model_in_test < out_comm_p->num_models) {
        out_comm_p->model_slot_index = model_in_test;
        model_in_test = (model_in_test + 1) % out_comm_p->num_models;
    }

    out_comm_p->cmd = CMD_RUN_NPU;

    j = in_comm_p->img_index_done;
    w = (i - j) % IPC_IMAGE_MAX;
    if (w < 0)
        w += IPC_IMAGE_MAX;
    dbg_msg("kdp_com_run_image: [%d] = %d, win = %d (%d - %d) {%d}\n", i, out_comm_p->input_count, w, i, j, model_in_test);

    scu_ipc_trigger_to_ncpu_int();

    return 0;
}

void kdp_com_demo_set_model(uint8_t *pdata, int len)
{
    struct kdp_model *mp;
    int i, model_type;
    
    mp = (struct kdp_model *)pdata;
    
    /* Hack for bosch_testcase */
    switch (mp->cmd_mem_addr) {
        case 0x600cb5d0:    // FR
            i = 2;
            model_type = KNERON_FR_VGG10;
            break;
        case 0x60092f00:    // LM
            i = 1;
            model_type = KNERON_LM_5PTS;
            break;
        case 0x6002bc00:    // FD
        default:
            i = 0;
            model_type = KNERON_FDSMALLBOX;
            break;
    }

    out_comm_p->models_type[i] = model_type;
    out_comm_p->models[i].input_mem_addr    = mp->input_mem_addr;
    out_comm_p->models[i].input_mem_len     = mp->input_mem_len;
    out_comm_p->models[i].output_mem_addr   = mp->output_mem_addr;
    out_comm_p->models[i].output_mem_len    = mp->output_mem_len;
    out_comm_p->models[i].buf_addr          = mp->buf_addr;
    out_comm_p->models[i].buf_len           = mp->buf_len;
    out_comm_p->models[i].cmd_mem_addr      = mp->cmd_mem_addr;
    out_comm_p->models[i].cmd_mem_len       = mp->cmd_mem_len;
    out_comm_p->models[i].weight_mem_addr   = mp->weight_mem_addr;
    out_comm_p->models[i].weight_mem_len    = mp->weight_mem_len;
    out_comm_p->models[i].setup_mem_addr    = mp->setup_mem_addr;
    out_comm_p->models[i].setup_mem_len     = mp->setup_mem_len;

    dbg_msg(" .input_mem_addr=%x\n", mp->input_mem_addr);
    dbg_msg(" .input_mem_len=%x\n", mp->input_mem_len);
    dbg_msg(" .output_mem_addr=%x\n", mp->output_mem_addr);
    dbg_msg(" .output_mem_len=%x\n", mp->output_mem_len);
    dbg_msg(" .buf_addr=%x\n", mp->buf_addr);
    dbg_msg(" .buf_len=%x\n", mp->buf_len);
    dbg_msg(" .cmd_mem_addr=%x\n", mp->cmd_mem_addr);
    dbg_msg(" .cmd_mem_len=%x\n", mp->cmd_mem_len);
    dbg_msg(" .weight_mem_addr=%x\n", mp->weight_mem_addr);
    dbg_msg(" .weight_mem_len=%x\n", mp->weight_mem_len);
    dbg_msg(" .setup_mem_addr=%x\n", mp->setup_mem_addr);
    dbg_msg(" .setup_mem_len=%x\n", mp->setup_mem_len);

    if (i+1 > out_comm_p->num_models)
        out_comm_p->num_models = i+1;
}

void kdp_com_demo_set_images(uint8_t *pdata, int len)
{
    struct demo_set_images_cmd_s *ip;

    ip = (struct demo_set_images_cmd_s *)pdata;

    image_s.image_mem_addr = ip->image_mem_addr;
    image_s.image_mem_len = ip->image_mem_len;
    image_s.image_col = ip->image_col;
    image_s.image_row = ip->image_row;
    image_s.image_ch = ip->image_ch;
    image_s.image_format = ip->image_format;

    dbg_msg(" .image_mem_addr=0x%x\n", image_s.image_mem_addr);
    dbg_msg(" .image_mem_len=0x%x\n", image_s.image_mem_len);
    dbg_msg(" .image_col=%d\n", image_s.image_col);
    dbg_msg(" .image_row=%d\n", image_s.image_row);
    dbg_msg(" .image_ch=%d\n", image_s.image_ch);
    dbg_msg(" .image_format=0x%x\n", image_s.image_format);
}

void kdp_com_demo_run(int run_demo)
{
    demo_mode = run_demo;

    if (run_demo) {
        demo_run_once = 0;
        kdp_com_demo_run_image();
    }
}

void kdp_com_demo_run_once()
{
    demo_mode = 1;
    demo_run_once = 1;

    kdp_com_demo_run_image();
}

void kdp_com_init(void)
{
    //osStatus_t status;

    out_comm_p = (struct scpu_to_ncpu *)IPC_MEM_ADDR;
    in_comm_p = (struct ncpu_to_scpu *)IPC_MEM_ADDR2;

    memset(out_comm_p, 0, sizeof(struct scpu_to_ncpu));
    memset(in_comm_p, 0, sizeof(struct ncpu_to_scpu));

    out_comm_p->id = SCPU2NCPU_ID;
    out_comm_p->input_count = 0;

    // Runtime debug control
#ifdef DBG_ALONE
    log_set_level_scpu(LOG_DBG);
    log_set_level_ncpu(LOG_DBG);
#else
    log_set_level_scpu(LOG_DBG);
    log_set_level_ncpu(LOG_ERROR);
#endif

    tid_ncpu_comm = osThreadNew(ncpu_comm_thread, NULL, NULL);
#if 0
    status = osThreadSetPriority(tid_ncpu_comm, osPriorityRealtime);
    if (status != osOK) {
        dbg_msg("osThreadSetPriority (ncpu comm) error: %d\n", status);
    }
#endif
    NVIC_SetVector((IRQn_Type)NCPU_IRQ, (uint32_t)NCPU_IRQHandler);
    NVIC_EnableIRQ((IRQn_Type)NCPU_IRQ);

    scu_ipc_enable_to_ncpu_int();
}

struct ncpu_to_scpu* kdp_com_get_input_ptr(void)
{
    return in_comm_p;
}

struct scpu_to_ncpu* kdp_com_get_output_ptr(void)
{
    return out_comm_p;
}
