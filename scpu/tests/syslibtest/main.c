/*
 * Kneron system library example code
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#include "cmsis_os2.h"
#include "test_peripheral.h"

#ifdef SYS_INIT_WORKAROUND
#include "pll_enable.h"
#include "ddr.h"
#include "DrvUART010.h"
#endif



extern void SystemCoreClockUpdate (void);

enum{

    TEST_GPIO = 0,

}TEST_CASE;


/**
 * @brief main, main dispatch function
 */
int main(void) 
{

    /* put kneron devices dependent init calls here */

#ifdef SYS_INIT_WORKAROUND

    pll_enable();
    ddr_init_chip_0429();
    fLib_SerialInit(DEBUG_CONSOLE, DEFAULT_CONSOLE_BAUD, PARITY_NONE, 0, 8, 0);

#endif

    SystemCoreClockUpdate();

    osKernelInitialize();

    int test_case = TEST_GPIO;

    switch(test_case)
    {
        case TEST_GPIO:
            printf("\nRunning gpio_main()\n");
            gpio_main();
            break;
        default:
            break;
    }

    if (osKernelGetState() == osKernelReady) {
        osKernelStart();
    }

    while(1) ;
}
