/*
 * Kneron system library example code
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */

#ifndef __TEST_PERIPHERAL_H__
#define __TEST_PERIPHERAL_H__

#define SYS_INIT_WORKAROUND // FIXME: it will be unnecessary after implementing system API

#ifdef SYS_INIT_WORKAROUND
#include "DrvUART010.h" // for printf
#define printf(...) fLib_printf( __VA_ARGS__ )
#else
#define printf(...) do{ } while(0);
#endif

extern void gpio_main(void);

#endif
