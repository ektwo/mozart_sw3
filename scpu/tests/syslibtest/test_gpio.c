/*
 * Kneron system library test main code
 *
 * Copyright (C) 2019 Kneron, Inc. All rights reserved.
 *
 */


#include "cmsis_os2.h"
#include "test_peripheral.h"

// including kdp-system.lib API
#include "kdp_peripheral.h"

static osThreadId_t tid_user; 
static void gpio_test_thread(void *argument);
static void gpio_test_callback(void *data);

void gpio_main(void)
{
    printf("Creating a user thread for interrupt notification\n");

    // create a thread to wait for interrupt notifications
    tid_user = osThreadNew(&gpio_test_thread, NULL, NULL);

    printf("Configuring pins to GPIO function, GPIO22(SD_CLK), GPIO23(SD_CMD), GPIO24(SD_DAT0), GPIO25(SD_DAT1)\n");

    /* 
       configure selected pins as GPIO function (pinmux)
       users must check pinmux table to configure specified pins
       as desired peripheral functions properly
    */
    kdp_pinconfig_set_pin_mode(KDP_PIN_SD_CLK, PIN_MODE_3);    // as GPIO 22
    kdp_pinconfig_set_pin_mode(KDP_PIN_SD_CMD, PIN_MODE_3);    // as GPIO 23
    kdp_pinconfig_set_pin_mode(KDP_PIN_SD_DAT_0, PIN_MODE_3);  // as GPIO 24
    kdp_pinconfig_set_pin_mode(KDP_PIN_SD_DAT_1, PIN_MODE_3);  // as GPIO 25

    printf("init gpio controller\n");

    // init GPIO controller
    kdp_gpio_init(KDP_GPIO_CTRL_0);

    printf("setting GPIO22 : Output Low\n");

    // set GPIO 22 as output low
    kdp_gpio_set_pin_attribute(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_22, KDP_GPIO_DIR_OUTPUT);
    kdp_gpio_set_pin_value(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_22, KDP_BOOL_FALSE);

    printf("setting GPIO23 : Input with pull-high\n");

    // set GPIO 23 as input with pad pull-high
    kdp_gpio_set_pin_attribute(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_23,
        KDP_GPIO_DIR_INPUT |
        KDP_GPIO_PAD_PULL_HIGH);

    printf("setting GPIO24 : Interrupt source, pull-high, debounce, user callback\n");

    /* set GPIO 24 as interrupt input, pull-high, debounce and register a user callback */
    {
        // first disable interrupt in case of wrong condition
        kdp_gpio_set_interrupt_enable(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_24, KDP_BOOL_FALSE);

        // set pin attributes as desired
        kdp_gpio_set_pin_attribute(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_24,
            KDP_GPIO_DIR_INPUT |
            KDP_GPIO_INT_EDGE_FALLLING |
            KDP_GPIO_PAD_PULL_HIGH);

        // enable internal hardware debounce with clock rate
        kdp_gpio_set_debounce_enable(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_24, KDP_BOOL_TRUE /* 1 for enable */, 1000);

        // register user callback function
        kdp_gpio_register_callback(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_24, &gpio_test_callback, NULL);

        // at last enable interrupt after all settings done
        kdp_gpio_set_interrupt_enable(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_24, KDP_BOOL_TRUE);
    }

    printf("setting GPIO25 : Interrupt source, pull-high, debounce, thread notification\n");

    /* set GPIO 25 as interrupt input, pull-high, debounce and register a user thread notification */
    {
        // first disable interrupt in case of wrong condition
        kdp_gpio_set_interrupt_enable(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_25, KDP_BOOL_FALSE);

        // set pin attributes as desired
        kdp_gpio_set_pin_attribute(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_25,
            KDP_GPIO_DIR_INPUT |
            KDP_GPIO_INT_EDGE_FALLLING |
            KDP_GPIO_PAD_PULL_HIGH);

        // enable internal hardware debounce with clock rate
        kdp_gpio_set_debounce_enable(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_25, KDP_BOOL_TRUE /* 1 for enable */, 1000);

        // register a thread flag notification
        kdp_gpio_register_threadflag(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_25, tid_user, 0x0001U);

        // at last enable interrupt after all settings done
        kdp_gpio_set_interrupt_enable(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_25, KDP_BOOL_TRUE);
    }

    /* exit here then let kernel get started */
}

static void gpio_test_thread(void *argument)
{
    for(;;)
    {
        uint32_t flags = osThreadFlagsWait(0x0001U, osFlagsWaitAny, 2000); // 2 secs timeout

        if(flags == osFlagsErrorTimeout)
        {
            // do following per 2 secs timeout

            // read GPIO 23
            kdp_bool_e value;
            kdp_gpio_get_pin_value(KDP_GPIO_CTRL_0, KDP_GPIO_PIN_23, &value);

            /* print GPIO 23 value */
            printf("GPIO23(SD_CMD) input : %s\n", value ? "High" : "Low");

        }else if(flags & 0x0001U)
        {
            /* print something */
            printf("GPIO25(SD_DAT1) -> thread notification\n");
        }
    }
}

static void gpio_test_callback(void *data)
{
    /* print something */
    printf("GPIO24(SD_DAT0) -> user callback\n");
}
