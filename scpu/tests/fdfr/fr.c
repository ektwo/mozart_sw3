/**
 * @file      fr.c
 * @brief     face recognition
 * @version $Id$
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include <string.h>
#include <assert.h>
#include "fr.h"
#include "base.h"
#include "sys.h"
#include "image.h"
#include "matrix.h"
#include "hwctrl.h"
#include "cnn.h"


/* ############################
 * ##    Static Functions    ##
 * ############################ */

/**
 * @brief fast sqrt() implementation
 * @param x input
 * @return sqrt of x
 */
static float SQRT(float x)
{
    union {
        int i;
        float x;
    } u;
    u.x = x;
    u.i = (1<<29) + (u.i >> 1) - (1<<22);

    // Two Babylonian Steps (simplified from:)
    // u.x = 0.5f * (u.x + x/u.x);
    // u.x = 0.5f * (u.x + x/u.x);
    u.x =       u.x + x/u.x;
    u.x = 0.25f*u.x + x/u.x;

    return u.x;
}

/**
 * @brief Calculate L2 norm
 * @param pMat input matrix
 */
static float L2Norm(Matrix *pMat)
{
    assert(pMat->type == MT_FLOAT);

    int size = pMat->width * pMat->height;
    int i = 0;
    float *pF = pMat->data.f;
    float norm = 0;

    /* Calculate demoniator */
    while (size--) {
        norm += pF[i]*pF[i];
        i++;
    }

    return SQRT(norm);
}

/**
 * @brief Normalize pMat by dividing L2 norm
 * @param pMat target matrix
 */
void NormalizeFeature(Matrix *pMat)
{
    assert(pMat->type == MT_FLOAT);

    int size = pMat->width * pMat->height;
    int i = 0;
    float *pF = pMat->data.f;
    float denominator = L2Norm(pMat);

    /* Calculate demoniator */
    while (size--) {
        pF[i++] /= denominator;
    }
}

/* ##############################
 * ##    Exported Functions    ##
 * ############################## */

/**
 * @brief Get NIR readin img scale param
 * @param start starting row
 * @param height number of rows
 * @return param
 */
ImgScaleParam GetNirScaleParam(int start, int height)
{
    ImgScaleParam param = {0};

    param.isFrame0 = 1;
    param.moveMode = 1;
    param.srcW = FR_IMG_WIDTH;
    param.srcH = FR_IMG_HEIGHT;
    param.subVal = 128;
    param.divide = 256;
    param.cropXstr = 0;
    param.cropXend = FR_IMG_WIDTH;
    param.cropYstr = start;
    param.cropYend = MIN(FR_IMG_HEIGHT, start + height);
    param.scaleImgOff = ALIGNED_IMG_ADDR - FRAME_0_ADDR;
    param.scaleW = FR_IMG_WIDTH;
    param.scaleH = param.cropYend - param.cropYstr;
    param.padX = 0;
    param.padY = 0;

    return param;
}

/**
 * @brief Get feature vector of image
 * @param pWarped face-aligned image
 * @return feature vector
 */
Matrix* GetFrFeature(const Image *pWarped)
{
    Matrix *pFeature = NULL;
	#ifdef DYFP
    RunCnnNirFrVggDyFp(pWarped, &pFeature);
	#else
    RunCnnNirFr(pWarped, &pFeature);
	#endif
    /* RunCnnNirFrVgg(pWarped, &pFeature); */
    NormalizeFeature(pFeature);

    return pFeature;
}


/**
 * @brief Calculate distance of two feature points
 * @param pFeat1 feature point 1
 * @param pFeat2 feature point 2
 * @return L2 distance
 */
float FrCalDist(const Matrix *pFeat1, const Matrix *pFeat2)
{
    assert(pFeat1->width * pFeat1->height == pFeat2->width * pFeat2->height);
    assert(pFeat1->type == MT_FLOAT && pFeat1->type == MT_FLOAT);

    int size = pFeat1->width * pFeat1->height;
    float *pF1 = pFeat1->data.f;
    float *pF2 = pFeat2->data.f;
    float sum = 0;

    while (size--) {
        sum += (*pF1-*pF2)*(*pF1-*pF2);
        pF1++;
        pF2++;
    }

    return SQRT(sum);
}

