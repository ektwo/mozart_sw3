/**
 * @file      fwmain.h
 * @brief     firmware main header
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __FWMAINX_H__
#define __FWMAINX_H__


int FwInit(void);
void DoDemoProcessImg(void);
void DoVerifyFaceDet(void);
void DoVerifyLandmark(void);
void DoVerifyLiveness(void);
void DoVerifyFaceRecog(void);
//void testAlignment(void);
//void testFRPost(void);
//void testLVPost(void);
//void test_softmax(void);

#endif


