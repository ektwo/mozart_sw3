/**
 * @file      image.c
 * @brief     3D image related functions
 * @version $Id: image.c 555 2018-08-14 03:13:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef SYS_PC
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif
#include "memory.h"
#include "image.h"
#include "matrix.h"

#define IMAGE_CONV_SCRIPT    "utils/conv_img.py"

/* ##############################
 * ##    Exported Functions    ##
 * ############################## */

/**
 * @return allocated Image* (caller should free the result)
 */
Image* MakeImage(int width, int height)
{
    return MakeMatrix(width, height, IMG_DATA_TYPE);
}

Image* MakeImageWithData(int width, int height, void *pData)
{
    return MakeMatrixWithData(width, height, IMG_DATA_TYPE, pData);
}

/**
 * Free image
 */
void FreeImage(Image *pImg)
{
    FreeMatrix(pImg);
}

/**
 * Create image whoose content is read from file
 */
Image* ReadImage(const char* filename)
{
    return ReadMatrix(filename, IMG_DATA_TYPE);
}

/**
 * Print image content to stdout
 */
void PrintImage(const Image *pImg)
{
    PrintMatrix(pImg);
}

/**
 * Print image content to stdout then free
 */
void PrintImageThenFree(Image *pImg)
{
    PrintMatrixThenFree(pImg);
}



/**
 * Write image to disk
 * @param pImg input image
 * @param rawfile raw data in text format
 * @param imgfile png file
 */
void WriteImage(const Image *pImg, const char *rawFile, const char *imgFile)
{
		#ifdef SYS_PC
    rawFile = (rawFile == NULL) ? imgFile : rawFile;
    if (rawFile) {
        WriteMat(rawFile, pImg);
    }

    if (imgFile) {
        char cmd[512] = {0};
        snprintf(cmd, sizeof(cmd), "python3 %s -t %s %s", IMAGE_CONV_SCRIPT, rawFile, imgFile);

        if (0 != system(cmd)) {
            printf("[error] Failed to write image [%s] [%s]\n", rawFile, imgFile);
        }
    }
		#endif
}

/**
 * @return Src[hStr:hEnd, wStr:wEnd]
 */
Image* SubImage(const Image *pSrc, int hStr, int hEnd, int wStr, int wEnd)
{
    Image *pImg = MakeImage(wEnd - wStr, hEnd - hStr);
    imdata *pValDst = IM_DATA(pImg);
    int wSrc = pSrc->width;
    int i, j;

    assert((pSrc->height >= (hEnd - hStr) &&
            pSrc->width >= (wEnd - wStr)) || !"dimention mismatch");

    for (i = hStr; i < hEnd; ++i) {
        imdata *pValSrc = IM_DATA_AT(pSrc, wSrc, i, wStr);
        for (j = wStr; j < wEnd; ++j) {
            *pValDst++ = *pValSrc++;
        }
    }

    return pImg;
}

/**
 * @brief Generate padded image to specified size
 * @param pSrc source image
 * @param pSize size of padded image
 * @param info padded info returned
 * @return padded image
 */
Image *GenPadImg(const Image *pSrc, const Size *pSize)
{
    int width = pSize->width;
    int height = pSize->height;
    int wSrc = pSrc->width;
    int hSrc = pSrc->height;
    int wStr = (width - wSrc) / 2;
    int hStr = (height - hSrc) / 2;

    assert((width >= wSrc && height >= hSrc) || !"Invalid params");
    Image *pImg = MakeImage(width, height);
    imdata *pValSrc = IM_DATA(pSrc);
    int i, j;

    for (i = hStr; i < hStr + hSrc; ++i) {
        imdata *pValDst = IM_DATA_AT(pImg, width, i, wStr);
        for (j = wStr; j < wStr + wSrc; ++j) {
            *pValDst++ = *pValSrc++;
        }
    }

    return pImg;
}


#ifdef SYS_PC
/**
 * Write image to binary file
 */
void ImgToBinFile(const Image *pImg, const char *filename)
{
    int fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, 0644);
    int size;

    assert(fd >= 0 || !"Failed to read file");

    size = sizeof(int);
    if ((size != write(fd, &(pImg->width), size)) ||
        (size != write(fd, &(pImg->height), size))) {
        printf("Failed to write file size\n");
    }

    size = pImg->width*pImg->height*sizeof(imdata);
    if (size != write(fd, IM_DATA(pImg), size)) {
        printf("Failed to write file data\n");
    }

    close(fd);
}


/**
 * Read image from binary file
 */
Image* ImgFromBinFile(const char *filename, bool isDepthImg)
{
    Image *pImg = NULL;
    int fd = open(filename, O_RDONLY);
    int width, height;
    int size;

    assert(fd >= 0 || !"Failed to read file");

    size = sizeof(int);
    if ((read(fd, &width, size) != size) ||
        (read(fd, &height, size) != size)) {
        printf("Failed to read image size from bin [%s]\n", filename);
    }

    if (isDepthImg) {
        pImg = MakeMatrix(width, height, MT_INT);
        size = width*height*sizeof(s16);
    } else {
        pImg = MakeImage(width, height);
        size = width*height*sizeof(imdata);
    }

    if (size != read(fd, IM_DATA(pImg), size)) {
        printf("Failed to read image size from bin [%s]\n", filename);
    }

    close(fd);

    return pImg;
}
#endif
