/**
 * @file      matrix.c
 * @brief     2D matrix related functions
 * @version $Id: matrix.c 555 2018-08-14 03:13:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "memory.h"
#include "matrix.h"
#include "base.h"
#include "sys.h"

/* ############################
 * ##    Static Functions    ##
 * ############################ */

static int GetElementSize(const Matrix *pMat)
{
    if (pMat->type == MT_FLOAT) {
        return sizeof(float);
    } else if (pMat->type == MT_INT) {
        return sizeof(int);
    } else {
        return sizeof(u8);
    }
}

static Matrix* _sCopyMatrix(const Matrix *pSrc, MAT_TYPE type)
{
    Matrix *pMat = MakeMatrix(pSrc->width, pSrc->height, type);
    int size = pSrc->width * pSrc->height;

    if (pSrc->type == type) {
        size *= GetElementSize(pMat);
        memcpy(pMat->data.c, pSrc->data.c, size);
    } else {
        MatData dSrc = pSrc->data;
        MatData dDst = pMat->data;
        MAT_TYPE tSrc = pSrc->type;
        MAT_TYPE tDst = pMat->type;
        int k = 0;

        while (size--) {
            M_SET_VAL_1D(dDst, tDst, k, M_GET_VAL_1D(dSrc, tSrc, k));
            k++;
        }
    }

    return pMat;
}

static void _PrintMatrix(const Matrix* pMat, FILE *fp)
{
    int width = pMat->width;
    int height = pMat->height;
    int i, j, k = 0;
    MAT_TYPE type = pMat->type;
    MatData data = pMat->data;
    const char *fmt = (type == MT_FLOAT) ? " %9.6f" : " %4d";

    fprintf(fp, "%d %d\n", width, height);
    for (i = 0; i < height; ++i) {
        for (j = 0; j < width; ++j) {
            if (type == MT_FLOAT) {
                fprintf(fp, fmt, (float) M_GET_VAL_1D(data, type, k));
            } else {
                fprintf(fp, fmt, (int) M_GET_VAL_1D(data, type, k));
            }
            k++;
        }
        fprintf(fp, "\n");
    }
}

/**
 * Init Matrix data
 */
static void InitMatrix(Matrix *pMat, int width, int height, int type, void *pData)
{
    int size = width * height;

    pMat->width = width;
    pMat->height = height;
    pMat->type = (MAT_TYPE)type;

    if (pData) {
        pMat->needFreeData = 0;
        pMat->data.c = (void*) pData;
    } else {
        pMat->needFreeData = 1;
        if (type == MT_UCHAR) {
            pMat->data.c = (u8*) MemCalloc(size, sizeof(u8));
            assert(pMat->data.c || !"Out of memory.");
        } else if (type == MT_INT) {
            pMat->data.i = (s16*) MemCalloc(size, sizeof(int));
            assert(pMat->data.i || !"Out of memory.");
        } else if (type == MT_FLOAT) {
            pMat->data.f = (float*) MemCalloc(size, sizeof(float));
            assert(pMat->data.f || !"Out of memory.");
        }
    }
}

/* ############################
 * ##    Public Functions    ##
 * ############################ */

/**
 * @return Allocated matrix (caller has to free it)
 */
Matrix* MakeMatrix(int width, int height, MAT_TYPE type)
{
    return MakeMatrixWithData(width, height, type, NULL);
}

/**
 * @return Allocated matrix with specified pData (caller has to free it)
 */
Matrix* MakeMatrixWithData(int width, int height, MAT_TYPE type, void *pData)
{
    Matrix* pMat;

    assert((width > 0 && height > 0) || !"New matrix must be at least a 1 by 1");
    pMat = (Matrix*) MemAlloc(sizeof(Matrix));
    assert(pMat || !"Out of memory.");

    InitMatrix(pMat, width, height, type, pData);

    return pMat;
}

/**
 * Free matrix
 */
void FreeMatrix(Matrix *pMat)
{
    if (!pMat) {
        return;
    }

    if (pMat->needFreeData) {
        FreeMatData(pMat);
    }

    MemFree(pMat);
}

/**
 * Free Matrix data
 */
void FreeMatData(Matrix *pMat)
{
    if (!pMat) {
        return;
    }

    if (pMat->type == MT_UCHAR) {
        MemFree(pMat->data.c);
        pMat->data.c = NULL;
    } else if (pMat->type == MT_INT) {
        MemFree(pMat->data.i);
        pMat->data.i = NULL;
    } else {
        MemFree(pMat->data.f);
        pMat->data.f = NULL;
    }
}

/**
 * @return np.ones(height, width)
 */
Matrix* OnesMatrix(int width, int height, MAT_TYPE type)
{
    Matrix *pMat = MakeMatrix(width, height, type);
    MatData data = pMat->data;
    int size = width*height;
    int k = 0;

    while (size--) {
        M_SET_VAL_1D(data, type, k, 1);
        k++;
    }

    return pMat;
}

/**
 * @return Copy of matrix
 */
Matrix* CopyMatrix(const Matrix *pSrc, MAT_TYPE type)
{
    return _sCopyMatrix(pSrc, type);
}

/**
 * @return np.ndarray.astype(M)
 */
Matrix* MatAsType(const Matrix *pSrc, MAT_TYPE type)
{
    return _sCopyMatrix(pSrc, type);
}

/**
 * Print matrix content to stdout
 */
void PrintMatrix(const Matrix* pMat)
{
    _PrintMatrix(pMat, stdout);
}

/**
 * Print matrix content to stdout then free matrix
 */
void PrintMatrixThenFree(Matrix *pMat)
{
    PrintMatrix(pMat);
    FreeMatrix(pMat);
}

/**
 * Write matrix content to file
 */
void WriteMat(const char *filename, const Matrix *pSrc)
{
    #ifdef SYS_PC
    FILE *fp = fopen(filename, "w");

    if (!fp) {
        fprintf(stderr, "Error: Cannot open [%s] with errno [%d]\n", filename, errno);
        exit(1);
    }

    _PrintMatrix(pSrc, fp);
    fclose(fp);
    #endif
}

/**
 * @return matrix whoose content is read from file
 */
Matrix* ReadMatrix(const char *filename, MAT_TYPE type)
{
    Matrix* pMat;
    #ifdef SYS_PC
    FILE* fh;
    float val;
    int width, height;
    int k, size;
    int ret;
    MatData data;

    if ((fh = fopen(filename, "r")) == NULL) {
        fprintf(stderr, "Error: Cannot open %s\n", filename);
        exit(1);
    }

    ret = fscanf(fh, "%d", &width);
    assert(ret != EOF || !"Failed to read from file.");

    ret = fscanf(fh, "%d", &height);
    assert(ret != EOF || !"Failed to read from file.");

    pMat = MakeMatrix(width, height, type);
    data = pMat->data;
    size = width * height;
    k = 0;

    while (size--) {
        ret = fscanf(fh, "%f", &val);
        assert(ret != EOF || !"Failed to read from file. File is incomplete.");
        M_SET_VAL_1D(data, type, k, val);
        k++;
    }

    fclose(fh);
    #endif
    return pMat;
}

/**
 * @return whether m is vector
 */
int IsVector(const Matrix *pMat)
{
    return (pMat->width == 1 || pMat->height == 1);
}

/**
 * @return np.max(m)
 */
float MaxVal(const Matrix *pMat)
{
    assert(pMat->type == MT_FLOAT);

    int size = pMat->width * pMat->height;
    float *pF = pMat->data.f;
    float max = pF[0];

    while (size--) {
        max = MAX(max, *pF);
        pF++;
    }

    return max;
}

/**
 * @return np.sum(m)
 */
float SumMat(const Matrix *pMat)
{
    MatData data = pMat->data;
    MAT_TYPE type = pMat->type;
    int size = pMat->width * pMat->height;
    int k = 0;
    float sum = 0;

    while (size--) {
        sum += M_GET_VAL_1D(data, type, k);
        k++;
    }

    return sum;
}

/**
 * @return src[hStr:hEnd, wStr:wEnd
 * caller has to free it
 */
Matrix* SubMat(const Matrix *pSrc, int hStr, int hEnd, int wStr, int wEnd)
{
    Matrix *pDst = MakeMatrix(wEnd - wStr, hEnd - hStr, pSrc->type);
    SubMatAssign(pDst, pSrc, hStr, hEnd, wStr, wEnd);

    return pDst;
}

/**
 * @return dst = src[hStr:hEnd, wStr:wEnd]
 * caller has to free it
 */
void SubMatAssign(Matrix *pDst, const Matrix *pSrc, int hStr, int hEnd, int wStr, int wEnd)
{
    MatData dSrc = pSrc->data;
    MatData dDst = pDst->data;
    MAT_TYPE tSrc = pSrc->type;
    MAT_TYPE tDst = pDst->type;
    int wSrc = pSrc->width;
    int i, j, k;

    /* In case of vector */
    assert((pSrc->height >= (hEnd - hStr) &&
            pSrc->width >= (wEnd - wStr)) || !"dimention mismatch");

    k = 0;
    for (i = hStr; i < hEnd; ++i) {
        for (j = wStr; j < wEnd; ++j) {
            M_SET_VAL_1D(dDst, tDst, k, M_GET_VAL(dSrc, tSrc, wSrc, i, j));
            k++;
        }
    }
}

/**
 * @brief m.reshape(width, height)
 */
void MatReshape(Matrix *pMat, int width, int height)
{
    if (width*height != pMat->width*pMat->height) {
        assert(!"Mat size miamatch!");
        return;
    }

    pMat->width = width;
    pMat->height = height;
}

/**
 * @return m[:, col]
 */
Matrix* MatGetCol(const Matrix *pSrc, int col)
{
    return SubMat(pSrc, 0, pSrc->height, col, col+1);
}

/**
 * @return m[row, :]
 */
Matrix* MatGetRow(const Matrix *pSrc, int row)
{
    return SubMat(pSrc, row, row+1, 0, pSrc->width);
}

/**
 * dst[hStr:hEnd, wStr:wEnd] = src
 */
void AssignMat(Matrix *pDst, const Matrix *pSrc, int hStr, int hEnd, int wStr, int wEnd)
{
    MatData dSrc = pSrc->data;
    MatData dDst = pDst->data;
    MAT_TYPE tSrc = pSrc->type;
    MAT_TYPE tDst = pDst->type;
    int wDst = pDst->width;
    int i, j, k;

    if (pSrc->width == 1 || pSrc->height == 1) {
        assert(pSrc->width*pSrc->height == (hEnd - hStr)*(wEnd - wStr)
               || !"dimension mismatch");
    } else {
        assert((pSrc->height >= (hEnd - hStr) &&
                pSrc->width >= (wEnd - wStr)) || !"dimention mismatch");
    }

    k = 0;
    for (i = hStr; i < hEnd; ++i) {
        for (j = wStr; j < wEnd; ++j) {
            M_SET_VAL(dDst, tDst, wDst, i, j,
                      M_GET_VAL_1D(dSrc, tSrc, k));
            k++;
        }
    }
}

/**
 * @return np.eye(n)
 */
Matrix* EyeMatrix(int n, MAT_TYPE type)
{
    assert(n > 0 || !"Identity matrix must have value greater than zero.");

    Matrix *pMat = MakeMatrix(n, n, type);
    MatData data = pMat->data;
    int i;

    for (i = 0; i < n; ++i) {
        M_SET_VAL(data, type, n, i, i, 1);
    }

    return pMat;
}

/**
 * @return np.nan()
 */
Matrix* NanMatrix(int width, int height)
{
    Matrix *pMat = MakeMatrix(width, height, MT_FLOAT);
    MatData data = pMat->data;
    MAT_TYPE type = pMat->type;
    int size = pMat->width * pMat->height;
    int k = 0;

    while (size--) {
        M_SET_VAL_1D(data, type, k, 0);
        k++;
    }

    return pMat;
}

/**
 * @return np.diag()
 */
Matrix* DiagMatrix(const Matrix* pVec)
{
    assert(pVec->height == 1 || pVec->width == 1 || !"dimension mismatch");

    int i;
    int k = 0;
    int dim = MAX(pVec->height, pVec->width);
    Matrix *pMat = MakeMatrix(dim, dim, pVec->type);
    MatData dVec = pVec->data;
    MatData dDst = pMat->data;
    MAT_TYPE tVec = pVec->type;
    MAT_TYPE tDst = pMat->type;

    for (i = 0; i < dim; i++) {
        M_SET_VAL(dDst, tDst, dim, i, i,
                  M_GET_VAL_1D(dVec, tVec, k));
        k++;
    }

    return pMat;
}

/**
 * @return np.ndarray.mean(axis=0)
 */
Matrix* MeanMat(const Matrix *pSrc)
{
    Matrix *pMat = MakeMatrix(pSrc->width, 1, MT_FLOAT);
    MatData dSrc = pSrc->data;
    MatData dDst = pMat->data;
    MAT_TYPE tSrc = pSrc->type;
    MAT_TYPE tDst = pMat->type;
    int wSrc = pSrc->width;
    int wDst = pMat->width;
    int i, j;

    assert(pSrc->height > 0 || !"Height of matrix cannot be zero.");

    for (i = 0; i < pSrc->width; i++) {
        float val = 0;
        for (j = 0; j < pSrc->height; j++) {
            val += M_GET_VAL(dSrc, tSrc, wSrc, j, i);
        }
        M_SET_VAL(dDst, tDst, wDst, 0, i, val / pSrc->height);
    }

    return pMat;
}

/**
 * @return np.ndarray.var(axis=0)
 */
Matrix* VarMat(const Matrix* pSrc)
{
    Matrix *pMat = MeanMat(pSrc);
    MatData dSrc = pSrc->data;
    MatData dDst = pMat->data;
    MAT_TYPE tSrc = pSrc->type;
    MAT_TYPE tDst = pMat->type;
    int wSrc = pSrc->width;
    int wDst = pMat->width;
    int i, j;

    for (i = 0; i < pSrc->width; i++) {
        float mean = M_GET_VAL(dDst, tDst, wDst, 0, i);
        float tmp = 0;

        for (j = 0; j < pSrc->height; j++) {
            float val = M_GET_VAL(dSrc, tSrc, wSrc, j, i);
            tmp += (val - mean)*(val - mean);
        }

        M_SET_VAL(dDst, tDst, wDst, 0, i, tmp / pSrc->height);
    }

    return pMat;
}

/**
 * @param a (MxN) matrix
 * @param vector (1xN) vector
 * @return a - vector (axis=0)
 */
Matrix* SubtractVec(const Matrix *pSrc, const Matrix *pVec)
{
    assert(pSrc->width == MAX(pVec->width, pVec->height) || !"width mismatch");
    assert(pVec->height == 1 || pVec->width == 1 || !"incorrect height");

    MAT_TYPE type = M_MAX_TYPE(pSrc->type, pVec->type);
    Matrix *pMat = CopyMatrix(pSrc, type);
    MatData dVec = pVec->data;
    MatData dDst = pMat->data;
    MAT_TYPE tVec = pVec->type;
    MAT_TYPE tDst = pMat->type;
    int i, j;
    int k1 = 0;

    for (i = 0; i < pMat->height; ++i) {
        int k2 = 0;
        for (j = 0; j < pMat->width; ++j) {
            M_SUB_VAL_1D(dDst, tDst, k1, M_GET_VAL_1D(dVec, tVec, k2));
            k1++;
            k2++;
        }
    }

    return pMat;
}

/**
 * @return m.T
 */
Matrix* TransposeMat(const Matrix* pSrc)
{
    Matrix* pMat = MakeMatrix(pSrc->height, pSrc->width, pSrc->type);
    MatData dSrc = pSrc->data;
    MatData dDst = pMat->data;
    MAT_TYPE tSrc = pSrc->type;
    MAT_TYPE tDst = pMat->type;
    int wSrc = pSrc->width;
    int i, j, k = 0;

    for (i = 0; i < pMat->height; i++) {
        for (j = 0; j < pMat->width; j++) {
            M_SET_VAL_1D(dDst, tDst, k, M_GET_VAL(dSrc, tSrc, wSrc, j, i));
            k++;
        }
    }

    return pMat;
}

/**
 * m = (m*value) if blScaleUp else (m/value)
 */
void ScaleMat(Matrix* pMat, float value, bool blScaleUp)
{
    if (value == 1) {
        return;
    }

    MatData dDst = pMat->data;
    MAT_TYPE tDst = pMat->type;
    int size = pMat->width * pMat->height;
    int k = 0;

    while (size--) {
        if (blScaleUp) {
            M_MUL_VAL_1D(dDst, tDst, k, value);
        } else {
            M_DIV_VAL_1D(dDst, tDst, k, value);
        }
        k++;
    }
}

/**
 * sub = m[hStr:hEnd, wStr, wEnd]
 * sub = (sub*value) if blScaleUp else (sub/value)
 */
void ScaleSubMat(Matrix *pMat, float value, bool blScaleUp, int hStr, int hEnd, int wStr, int wEnd)
{
    if (value == 1) {
        return;
    }

    MatData dDst = pMat->data;
    MAT_TYPE tDst = (MAT_TYPE)pMat->type;
    MAT_TYPE wDst = pMat->width;
    int i, j;

    for (i = hStr; i < hEnd; ++i) {
        for (j = wStr; j < wEnd; ++j) {
            if (blScaleUp) {
                M_MUL_VAL(dDst, tDst, wDst, i, j, value);
            } else {
                M_DIV_VAL(dDst, tDst, wDst, i, j, value);
            }
        }
    }
}

/**
 * @param pVec1 vector
 * @param pVec2 vector
 * @return np.dot(pVec1, pVec2)
 */
float InnerProduct(const Matrix *pVec1, const Matrix *pVec2)
{
    assert((IsVector(pVec1) && IsVector(pVec2)) || !"input should be both vectors");
    assert(pVec1->width*pVec1->height == pVec2->width*pVec2->height || !"input size should be the same");

    MatData d1 = pVec1->data;
    MatData d2 = pVec2->data;
    MAT_TYPE t1 = pVec1->type;
    MAT_TYPE t2 = pVec2->type;
    int size = pVec1->width * pVec1->height;
    int k = 0;
    float res = 0;

    while (size--) {
        res += M_GET_VAL_1D(d1, t1, k) * M_GET_VAL_1D(d2, t2, k);
        k++;
    }

    return res;
}

/**
 * @return np.dot(a,b)
 */
Matrix* DotProduct(const Matrix* pMat1, const Matrix* pMat2)
{
    assert(pMat1->width == pMat2->height || !"a->width must equal to b->height");

    MAT_TYPE t1 = pMat1->type;
    MAT_TYPE t2 = pMat2->type;
    MAT_TYPE tOut = M_MAX_TYPE(pMat1->type, pMat2->type);
    Matrix* pOut = MakeMatrix(pMat2->width, pMat1->height, tOut);
    MatData d1 = pMat1->data;
    MatData d2 = pMat2->data;
    MatData dOut = pOut->data;
    int w1 = pMat1->width;
    int w2 = pMat2->width;
    int i, j, k;
    int h = 0;
    float val;

    for (i = 0; i < pMat1->height; i++) {
        for (j = 0; j < pMat2->width; j++) {
            val = 0;
            for (k = 0; k < pMat2->height; k++) {
                val += M_GET_VAL(d1, t1, w1, i, k) * M_GET_VAL(d2, t2, w2, k, j);
            }
            M_SET_VAL_1D(dOut, tOut, h, val);
            h++;
        }
    }

    return pOut;
}

/**
 * @return np.linalg.det(A)
 */
float DetMatrix(const Matrix *pMat)
{
    assert(pMat->height == pMat->width || !"matrix should be NxN");

    MatData data = pMat->data;
    MAT_TYPE type = pMat->type;
    int width = pMat->width;
    int n = pMat->height;
    int i, j, j1, j2;
    float det = 0;

    if(n == 1) {
        det = M_GET_VAL_1D(data, type, 0);
    } else if (n == 2) {
        det = M_GET_VAL_1D(data, type, 0) * M_GET_VAL_1D(data, type, 3) -
              M_GET_VAL_1D(data, type, 2) * M_GET_VAL_1D(data, type, 1);
    } else {
        for(j1 = 0; j1 < n; j1++) {
            Matrix *pM = MakeMatrix(n-1, n-1, pMat->type);
            MatData dM = pM->data;

            for (i = 1; i < n; i++) {
                j2 = 0;
                for(j = 0; j < n; j++) {
                    if(j == j1) {
                        continue;
                    }
                    M_SET_VAL(dM, type, n-1, i-1, j2,
                              M_GET_VAL(data, type, width, i, j));
                    j2++;
                }
            }

            if (j1 & 0x01) {
                det -= M_GET_VAL(data, type, width, 0, j1) * DetMatrix(pM);
            } else {
                det += M_GET_VAL(data, type, width, 0, j1) * DetMatrix(pM);
            }

            FreeMatrix(pM);
        }
    }

    return det;
}

/**
 * Truncate decimal part
 * Prints a matrix. Great for debugging.
 */
Matrix* TruncMatDecimalVal(const Matrix* pSrc)
{
    Matrix *pMat = CopyMatrix(pSrc, pSrc->type);
    MatData dSrc = pSrc->data;
    MatData dDst = pMat->data;
    MAT_TYPE tSrc = pSrc->type;
    MAT_TYPE tDst = pMat->type;
    int size = pMat->width * pMat->height;
    int k = 0;

    while (size--) {
        M_SET_VAL_1D(dDst, tDst, k, M_GET_VAL_1D(dSrc, tSrc, k));
        k++;
    }

    return pMat;
}

/**
 * Merge matrix column
 * @param m1 (M x N1) matrix
 * @param m2 (M x N2) matrix
 * @return (M x (N1+N2)) matrix
 */
Matrix* MergeMatrixCol(const Matrix *m1, const Matrix *m2)
{
    if (m1->height != m2->height){
        printf("can not col merge two matrix: col mismatch!!!!\n");
    }

    int width = m1->width + m2->width;
    int height = m1->height;
    MAT_TYPE t1 = m1->type;
    MAT_TYPE t2 = m2->type;
    MAT_TYPE tOut = M_MAX_TYPE(m1->type, m2->type);
    Matrix* pOut = MakeMatrix(width, height, tOut);
    MatData d1 = m1->data;
    MatData d2 = m2->data;
    MatData dOut = pOut->data;
    int i, j, k, h;

    h = 0;
    for (i = 0; i < height; i++) {
        k = i*width;
        for (j = 0; j < m1->width; j++) {
            M_SET_VAL_1D(dOut, tOut, k, M_GET_VAL_1D(d1, t1, h));
            k++;
            h++;
        }
    }

    h = 0;
    for (i = 0; i < height; i++) {
        k = i*width + m1->width;
        for (j = 0; j < m2->width; j++) {
            M_SET_VAL_1D(dOut, tOut, k, M_GET_VAL_1D(d2, t2, h));
            k++;
            h++;
        }
    }

    return pOut;
}

/**
 * Perform matrix-elemenwise operation (+, -, *)
 */
Matrix* MatElementOp(const Matrix *m1, const Matrix *m2, int op)
{
    MAT_TYPE t1 = m1->type;
    MAT_TYPE t2 = m2->type;
    MAT_TYPE tOut = M_MAX_TYPE(m1->type, m2->type);
    Matrix* pOut = MakeMatrix(m1->width, m1->height, tOut);
    MatData d1 = m1->data;
    MatData d2 = m2->data;
    MatData dOut = pOut->data;
    int size = m1->width * m1->height;
    int k = 0;

    assert(m1->height == m2->height || m1->width == m2->width);

    if (op == MATRIX_ELEMENT_ADD){
        while (size--) {
            M_SET_VAL_1D(dOut, tOut, k, M_GET_VAL_1D(d1, t1, k) + M_GET_VAL_1D(d2, t2, k));
            k++;
        }
    } else if (op == MATRIX_ELEMENT_MINUS) {
        while (size--) {
            M_SET_VAL_1D(dOut, tOut, k, M_GET_VAL_1D(d1, t1, k) - M_GET_VAL_1D(d2, t2, k));
            k++;
        }
    } else if (op == MATRIX_ELEMENT_MULTIPLY) {
        while (size--) {
            M_SET_VAL_1D(dOut, tOut, k, M_GET_VAL_1D(d1, t1, k) * M_GET_VAL_1D(d2, t2, k));
            k++;
        }
    } else {
        printf("error operation!!!!\n");
    }

    return pOut;
}
