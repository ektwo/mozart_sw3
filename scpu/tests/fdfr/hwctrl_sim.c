/**
 * @file      hwctrl_sim.c
 * @brief     hw ctrl simulation functions
 * @version $Id: hwctrl_sim.c 555 2018-08-14 03:13:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifdef SYS_PC

#include "hwctrl.h"
#include "imagecv.h"

const char *FACE_IMG_BIN_L = "input/img/nir1.bin";
const char *DEPTH_IMG_BIN = "input/img/depth_172x224.bin";

Image* GetNirImg()
{
    return ImgFromBinFile(FACE_IMG_BIN_L, false);
}

Image* GetDepthImg()
{
    return ImgFromBinFile(DEPTH_IMG_BIN, true);
}

void StartCnn(int cmdOffset, bool blResetExtWeightPtr)
{
}

void WaitCnn()
{
}

void StartFetchNirImg()
{
}

void StartFetchDepthImg()
{
}

#endif
