/* -------------------------------------------------------------------------- 
 * Copyright (c) 2013-2016 ARM Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *      Name:    main.c
 *      Purpose: RTX for Kneron
 *
 *---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "DrvUART010.h"
#include "DrvDMAC020.h"
#include "DrvPWMTMR010.h"
//#include "ARMCM4.h"                     // Device header
#include "cmsis_os2.h"                    // ARM::CMSIS:RTOS2:Keil RTX5
#include "Driver_Common.h"
#include "kneron_mozart.h"                // Device header
#include "base.h"
#include "npu.h"
#include "crc.h"
#include "com.h"
#include "io.h"
#include "string.h"
#include "ddr.h"
#include "msg.h"
#include "fwmain.h"
#include "cnn.h"
#include "fd.h"
#include "fdebug.h"
#include "sys.h"



#define MSG_DBG_LOG 
#define MSG_MEM_BYTE_WRITE      0

int cmd_parser(u8 *buf, int len);
void sys_init(void);
void mem_clr(u32 addr, u32 len);

#if 1 
void __stack_chk_fail()
{
    //print_uart0("__stack_chk_fail()\n");
    while(1);
}

void *__stack_chk_guard = (void *)0;
#endif

/**
 * @info(), Get kernel information
 */
void info(void){
    char infobuf[100];
    osVersion_t osv;
    osStatus_t status;
 
    status = osKernelGetInfo(&osv, infobuf, sizeof(infobuf));
    if(status == osOK) {
        fLib_printf("+-------------------Keil RTX5----------------------+\n");
        fLib_printf("Kernel Information: %s\r\n", infobuf);
        fLib_printf("Kernel Version    : %d\r\n", osv.kernel);
        fLib_printf("Kernel API Version: %d\r\n", osv.api);
    }
}




/**
 * @brief buf_dbg_out, print out data by uart debug port
 * @param buf buffer data to write
 * @param len data length
 */
int buf_dbg_out(u8 *buf, int len)
{
    char buf1[64];
    for (int i = 0; i < len; i++) {
        sprintf(buf1 + ((i%16) * 3), "%02X ", buf[i]);
        if ((i + 1) % 16 == 0) {
            fLib_printf("%s\n", buf1);
            memset(buf1, 0, sizeof(buf1));
        }
    }
    fLib_printf("%s\n", buf1);
    return 0;
}

/**
 * @brief npu_run(), run npu
 */
void npu_run()
{
    u32 st_tick, sp_tick, total_time;
    outw(ADDR_NPU_INT, NPU_INT_int(0xff));    
    st_tick = SYS_TMR_GET_TICK();
    outw(ADDR_NPU_RUN, NPU_RUN_go(1));     
    while(!(inw(ADDR_NPU_INT) & 0x80)) {
        //st_tick = osKernelGetTickCount();;
        sp_tick = SYS_TMR_GET_TICK();
        total_time = (sp_tick >= st_tick)? sp_tick - st_tick : st_tick - sp_tick;
        if ( total_time > 2000) {
            fLib_printf("ERR: NPU calculate timeout \n");
            break;
        }
    }
    sp_tick = SYS_TMR_GET_TICK();
    total_time = (sp_tick >= st_tick)? sp_tick - st_tick : st_tick - sp_tick;
    fLib_printf("NPU calculate time (st=%d, sp=%d) %d \n", sp_tick, st_tick, total_time);
    return;   
}

/**
 * @brief cmd_parser(), uart/usb message parser
 */
    int cmd_parser(u8 *buf, int len)
{    
    MsgHdr *msghdr;
    u8 *pdata = NULL;
    u32 *pwdata = NULL;
    u16 crc16;
    u8 rstatus = 0;
    int wlen;
    msghdr = (MsgHdr*) buf;
    pdata = (u8*) (buf + MSG_HDR_SIZE);
    pwdata = (u32*) (buf + MSG_HDR_SIZE);

    crc16 = gen_crc16((const u8*) (buf + 4), len - 4);  
    if (crc16 != msghdr->crc16) {
        /* CRC16 checksum error */
        msg_pack(CMD_CRC_ERR, NULL, 0);
        return (-1);
    }

    wlen = (msghdr->len % 4) == 0? msghdr->len/4 :  msghdr->len/4 + 1;
    /* Command parsing */
    switch(msghdr->cmd) {
    case CMD_MEM_WRITE:
        #if MSG_MEM_BYTE_WRITE
        if ((msghdr->addr >= 0x30000000) && (msghdr->addr < 0x40000000)) {
        #else
        if (1) {
        #endif
            for (int i = 0; i < wlen; i++) {
                outw(msghdr->addr + i * 4, (u32) *(pwdata + i));
            }
        } else {
            for (int i = 0; i < msghdr->len; i++) {
                outb(msghdr->addr + i, *(pdata + i));
            }
        }
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_MEM_READ:
        #if MSG_MEM_BYTE_WRITE
        if ((msghdr->addr >= 0x30000000) && (msghdr->addr < 0x40000000)) {
        #else
        if (1) {
        #endif
            for (int i = 0; i < wlen; i++) {
                *(pwdata + i) = inw(msghdr->addr + 4 * i);
            }
        } else {
            for (int i = 0; i < msghdr->len; i++) {
                *(pdata + i) = inb(msghdr->addr + i);
            }
        }
        msg_pack(CMD_MEM_READ, (u8 *)pdata, msghdr->len);
        break;
    case CMD_TEST_ECHO:
        msg_pack(CMD_TEST_ECHO, (u8 *)pdata, msghdr->len);
        break;
    case CMD_MEM_CLR:
        mem_clr(msghdr->addr, msghdr->len);
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_RUN_ONCE:
        msg_pack(CMD_ACK, NULL, 0);
        npu_run();
        break;
    case CMD_DEMO_RUN:    
        msg_pack(CMD_ACK, NULL, 0);
        fLib_printf("DBG: CMD_DEMO_RUN.\n");
        if (0 != CnnInit()) {
            fLib_printf("Failed to init cnn.\n");
        }
        FwInit();
        DoDemoProcessImg();
        //compareBench(NULL, NULL);
        //rotate(NULL, NULL, 0, 0);
        //testAlignment();
        //testFRPost();
        //testLVPost();
        //test_softmax();
        //testRotate();
        break;        
    case CMD_DEMO_FD:   // for development
        fLib_printf("DBG: CMD_DEMO_FD.\n");
        DoVerifyFaceDet(); //TODO: can create standalone FD function
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_LM:   // for development
        fLib_printf("DBG: CMD_DEMO_LM.\n");
        //FwInit();
        DoVerifyLandmark();
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_LD:   // for development
        fLib_printf("DBG: CMD_DEMO_LD.\n");
        DoVerifyLiveness();
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_FR:   // for development
        fLib_printf("DBG: CMD_DEMO_FR.\n");
        DoVerifyFaceRecog();
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_STOP:
    case CMD_STS_CLR:
    default:
        msg_pack(CMD_ACK, NULL, 0);
        break;
    }
    return rstatus;
}



/**
 * @brief mem_clr(), DDR/NPU SRAM memory clear
 */
void mem_clr(u32 addr, u32 len)
{
    memset((void *) addr, 0, len);
}

/**
 * @brief sys_init(), system initial
 */
void sys_init()
{
    mem_clr(0x30000000, 512 * 1024);
    /* Enable Extenal debug, NPU deassert */
    // Disable this function if we need use only one SCPU 
    //outw(SCU_EXTREG_PA_BASE + 0x0068, 0x1001);
    /* outw(SCU_EXTREG_PA_BASE + 0x0014, inw(SCU_EXTREG_PA_BASE + 0x14) | BIT(22));   */
}

/**
 * @brief main, main dispatch function
 */
int main(void) 
{
    int len;
    ddr_init_1228_kingston_v35(0);
    sys_init();
    fLib_SerialInit(DEBUG_CONSOLE, DEFAULT_CONSOLE_BAUD, PARITY_NONE, 0, 8, 0);
    fLib_SerialInit(MSG_PORT, BAUD_921600, PARITY_NONE, 0, 8, 0);
    info();         
    SystemCoreClockUpdate();            // System Initialization 
    osKernelInitialize();               // Initialize CMSIS-RTOS
    fLib_Timer_Init(SYS_TMR, PWMTMR_1MSEC_PERIOD);
    while(1) {
        len = msg_read(msg_rbuf);
        if (len > 0) {
            //fLib_printf("Get uart_len = %d\n", len);
            cmd_parser(msg_rbuf, len);
        }
    }    
}
