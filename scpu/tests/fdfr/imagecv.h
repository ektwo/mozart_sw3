/**
 * @file      imagecv.h
 * @brief     Opencv functions
 * @version $Id: imagecv.h 498 2018-07-16 05:43:35Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __IMAGECV_H__
#define __IMAGECV_H__

#include "base.h"
#include "matrix.h"
#include "image.h"

void cvWarpAffine(Image *pImg, const Image *pSrc, const Matrix *pM);
void cvResize(Image *pImg, const Image *pSrc);
Image* letterbox_image(Image *im, int w, int h);
#endif
