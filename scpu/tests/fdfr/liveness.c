#include "liveness.h"
#include "matrix.h"
#include "math.h"
#include "image.h"
#include "image_base.h"
#include "stdlib.h"


int constrain_int(int a, int min, int max)
{
    if (a < min) return min;
    if (a > max) return max;
    return a;
}

int abs_float_compare (const void * a, const void * b)
{
    //printf("float_abs_compare\n");
    float fa = *(const float*) a;
    if(fa < 0) fa = -fa;
    float fb = *(const float*) b;
    if(fb < 0) fb = -fb;
    //printf("float_abs_compare1\n");
    return (fa > fb) - (fa < fb);
}


Image crop_image(Image im, int dx, int dy, int w, int h)
{
    //Image cropped = MakeImage(w, h, im.c);
	Image *cropped = MakeImage(w, h);
    imdata *pValSrc = IM_DATA(&im);
    imdata *pValDst = IM_DATA(cropped);    
    int i, j, k;
    //for(k = 0; k < im.c; ++k){
        for(j = 0; j < h; ++j){
            for(i = 0; i < w; ++i){
                int r = j + dy;
                int c = i + dx;
                float val;
                r = constrain_int(r, 0, im.height - 1);
                c = constrain_int(c, 0, im.width - 1);
                //val = get_pixel(im, c, r, k);
                //set_pixel(cropped, i, j, k, val);
                val = IM_GET_VAL(pValSrc, w, i, j);
                IM_SET_VAL(pValDst, w, i+dy, j+dx, val);
            }
        }
    //}
    return *cropped;
}


/** @brief Crop depth image
 *  @param[in]  im         input depth image
 *  @param[in]  rc         face rectangle (x,y,w,h)
 *  @return image
 */
Image crop_depth_image(Image im, rect rc) {
    Image cimg;
    if (rc.w > 0 && rc.h >0) {
        if (rc.x < 0) {
            rc.w += rc.x;
            rc.x = 0;
        }
        if (rc.y < 0) {
            rc.h += rc.y;
            rc.y = 0;
        }
        rc.w = rc.x + rc.w >= im.width ? im.width - rc.x : rc.w;
        rc.h = rc.y + rc.h >= im.height ? im.height - rc.y : rc.h;       
        cimg = crop_image(im, rc.x, rc.y, rc.w, rc.h);
    } else {
        cimg = im;
    }
    return cimg;
}


float find_max(float* input, int size)
{
    float max_val = input[0];
    for (int i = 1; i < size; i++)
        max_val = MAX(max_val, input[i]);
    return max_val;
}

/** @brief Find min value from the data which are greater than zero.
 *  @param[in]  input             input data pointer
 *  @param[in]  size              input size
 *  @return float
 */
float find_min_positive(float* input, int size)
{
    float min_val = 0;
    int k = 0;
    for (int i = 0; i < size; i++) {
        if (input[i] > 0) {
            min_val = input[i];
            k = i;
            break;
        }
    }

    for (int i = k + 1; i < size; i++) {
        if (input[i] > 0) {
            min_val = MIN(min_val, input[i]);
        }
    }
    return min_val;
}


/** @brief Find min value from the data which are greater than zero.
 *  @param[in]  input             input data pointer
 *  @param[in]  size              input size
 *  @return float
 */
float find_median_positive(float* input, int size)
{
    float median = 0.;
    int k = 0;

    int total_positive = 0;
    for (int i = 0; i < size; i++) {
        if (input[i] > 0) {
            total_positive++;
        }
    }
    //printf("Total positive:%d %d\n", size, total_positive);
    float *positive_input = calloc(total_positive, sizeof(float));

    int p_num = 0;
    for (int i = 0; i < size; i++) {
        if (input[i] > 0) {
            positive_input[p_num] = input[i];
            p_num++;
            //printf("%f ", positive_input[i]);
        }
    }
    //printf("\nfind_median_positive1 %d\n",sizeof(positive_input)/sizeof(positive_input[0]));
    //printf("\nfind_median_positive2 %f %d\n",positive_input[0],total_positive);
    qsort(positive_input,total_positive,sizeof(float),abs_float_compare);

    if (0 == total_positive % 2) {
        median = (positive_input[total_positive/2 - 1] + positive_input[total_positive/2]) / 2.0;
    } else {
        median = positive_input[total_positive/2];
    }
    free(positive_input);
    //printf("Median:%f\n",median);
    return median;
}



/** @brief Check liveness with depth image, face rectangle, and landmarks. Return the value of depth code.
 *  @param[in]  depth_im          input depth image 
 *  @param[in]  rc                face rectangle (x,y,w,h)
 *  @param[in]  landmarks         five points landmarks (left eye, right eye, nose, mouth left, mouth right)
 *  @param[in]  scale             set to 0 by default
 *  @return image
 */
Image preprocess_depth_image(Image depth_im, rect rc, float *landmarks, int scale) {
    //Crop the face area
    Image dcropimg = crop_depth_image(depth_im, rc);
    imdata *pdCropImgVal = IM_DATA(&dcropimg);
    //printf("preprocess_depth_image:  %d %d %f %f\n",rc.x, rc.y,landmarks[4],landmarks[5]);
    int nose_x = (int)(landmarks[4]) + rc.x;//rc.x + rc.w/2;
    int nose_y = (int)(landmarks[5]) + rc.y;//rc.y + rc.h/2;
    rect nose_rc;
    nose_rc.x = (nose_x - 30) > 0 ? (nose_x - 30) : 0;
    nose_rc.y = (nose_y - 30) > 0 ? (nose_y - 30) : 0;
    nose_rc.w = (nose_x - 30) > 0 ? 60 : 60 + (nose_x - 30);
    nose_rc.h = (nose_y - 30) > 0 ? 60 : 60 + (nose_y - 30);
    //printf("preprocess_depth_image value1 %d %d %d %d\n", nose_rc.x, nose_rc.y, nose_rc.w, nose_rc.h);
    //Crop the nose area
    Image noseimg = crop_depth_image(depth_im, nose_rc);

    float nose_depth = find_median_positive(noseimg.data.f, noseimg.width * noseimg.height);
    float low_thresh = nose_depth - 200.;
    float high_thresh = nose_depth + 80.;
    //printf("preprocess_depth_image value21");
    for (int n = 0; n < dcropimg.width * dcropimg.height; n++) {
        //if (*(pdCropImgVal+n) > high_thresh || (dcropimg.data[n] < low_thresh) || (dcropimg.data[n] < 0)) {
        imdata value = *(pdCropImgVal + n);    
        if ((value > high_thresh) || (value < low_thresh) || (value < 0)) {            
            *(pdCropImgVal + n) = 0;
        }
    }
    //printf("preprocess_depth_image value2");
    float dmin = find_min_positive(dcropimg.data.f, dcropimg.width * dcropimg.height);
    for (int n = 0; n < dcropimg.width * dcropimg.height; n ++) {
    	imdata value = *(pdCropImgVal + n);
        if(value > 0) {
            *(pdCropImgVal + n) = value - dmin;
        }
    }

    if (0 != scale) {
        //printf("preprocess_depth_image value3");
        float dmax = find_max(dcropimg.data.f, dcropimg.width * dcropimg.height);
        for (int n = 0; n < dcropimg.width * dcropimg.height; n++) {            
            *(pdCropImgVal + n) = ((*(pdCropImgVal + n))/dmax)*255.;
        }
    }
    //printf("preprocess_depth_image value11");
    float eye_center_x = (landmarks[0] + landmarks[2]) / 2.;
    float eye_center_y = (landmarks[1] + landmarks[3]) / 2.;
    float mouth_center_x = (landmarks[6] + landmarks[8]) / 2.;
    float mouth_center_y = (landmarks[7] + landmarks[9]) / 2.;
    //printf("preprocess_depth_image value12");
    float length = sqrt(pow(eye_center_x - mouth_center_x, 2) + pow(eye_center_y - mouth_center_y, 2));
    rect cnose_rc;
    cnose_rc.x = (int)(landmarks[4] - length / 2.) > 0 ? (int)(landmarks[4] - length / 2.) : 0;
    cnose_rc.y = (int)(landmarks[5] - length / 2.) > 0 ? (int)(landmarks[5] - length / 2.) : 0;
    cnose_rc.w = (int)(landmarks[4] - length / 2.) > 0 ? (int)length : (int)length + (int)(landmarks[4] - length / 2.);
    cnose_rc.h = (int)(landmarks[5] - length / 2.) > 0 ? (int)length : (int)length + (int)(landmarks[5] - length / 2.);
    //printf("\npreprocess_depth_image value2 %f %d %d %d %d\n", length, cnose_rc.x, cnose_rc.y, cnose_rc.w, cnose_rc.h);
    //Crop the nose area
    Image cnoseimg = crop_depth_image(dcropimg, cnose_rc);
    //Liveness detection
    /*float nose = dcropimg.data[(int)landmarks[4] + (int)landmarks[5]*dcropimg.width];
    float leye = dcropimg.data[(int)landmarks[0] + (int)landmarks[1]*dcropimg.width];
    float reye = dcropimg.data[(int)landmarks[2] + (int)landmarks[3]*dcropimg.width];
    float live_val_left = (nose - leye) > 0 ? (nose - leye) : -(nose - leye);
    float live_val_right = (nose - reye) > 0? (nose - reye) : -(nose - reye);
    float live_val = (live_val_left + live_val_right) / 2.;*/

    FreeImage(&dcropimg);
    FreeImage(&noseimg);
    return cnoseimg;
}

//vincent
void fd_postprocess(void)
{
    return;
}
