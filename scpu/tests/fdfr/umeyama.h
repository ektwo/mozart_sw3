/**
 * @file      umeyama.h
 * @brief     umeyama header
 * @version $Id: umeyama.h 498 2018-07-16 05:43:35Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __UMEYAMA_H__
#define __UMEYAMA_H__

#include <stdbool.h>
#include "matrix.h"

Matrix* umeyama(const Matrix *src, const Matrix *dst, bool blEstimateScale);

#endif
