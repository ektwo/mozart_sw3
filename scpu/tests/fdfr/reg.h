/**
 * @file      reg.h
 * @brief     Register header
 * @version $Id: reg.h 517 2018-07-18 10:10:18Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __REG_H__
#define __REG_H__


#include "base.h"

#define RegBitOn(reg, bitOff)                  ((reg) |= 0x01u << (bitOff))
#define RegBitOff(reg, bitOff)                 ((reg) &= ~(0x01u << (bitOff)))
#define RegSetBits(reg, val, bitMask, bitOff)  ((reg) &= ~((bitMask) << (bitOff))); \
                                               ((reg) |= (((val) & (bitMask)) << (bitOff)))

#define REG_FLAG_ON         0x01
#define REG_FLAG_OFF        0x00

#define CNN_CTRL            (CTRL_REG_BASE_ADDR + 0x0000)
#define CNN_CTRL1           (CTRL_REG_BASE_ADDR + 0x0004)
#define CNN_STATUS          (CTRL_REG_BASE_ADDR + 0x0008)
#define CNN_STATUS1         (CTRL_REG_BASE_ADDR + 0x000C)
#define RDIN_CTRL_REG       (CTRL_REG_BASE_ADDR + 0x0010)
#define RDIN_PAD_NO         (CTRL_REG_BASE_ADDR + 0x0014)
#define SCALE_IMG_S         (CTRL_REG_BASE_ADDR + 0x0018)
#define SCALE_DST_S         (CTRL_REG_BASE_ADDR + 0x001C)
#define SCALE_IMG_ADR       (CTRL_REG_BASE_ADDR + 0x0020)
#define SCALE_CROP_ADR      (CTRL_REG_BASE_ADDR + 0x0024)
#define SCALE_CROP_WH       (CTRL_REG_BASE_ADDR + 0x0028)
#define SCALE_RATIO         (CTRL_REG_BASE_ADDR + 0x002C)
#define SCALE_CTRL          (CTRL_REG_BASE_ADDR + 0x0030)
#define SNRSD_IMG_S         (CTRL_REG_BASE_ADDR + 0x0034)
#define SNRSD_DST_S         (CTRL_REG_BASE_ADDR + 0x0038)
#define SNRSD_CROP_Y        (CTRL_REG_BASE_ADDR + 0x003C)
#define SNRSD_CROP_X        (CTRL_REG_BASE_ADDR + 0x0040)
#define SNRSD_RATIO         (CTRL_REG_BASE_ADDR + 0x0044)
#define SNRSD_CTRL          (CTRL_REG_BASE_ADDR + 0x0048)
#define DPHSD_IMG_S         (CTRL_REG_BASE_ADDR + 0x004C)
#define DPHSD_DST_S         (CTRL_REG_BASE_ADDR + 0x0050)
#define DPHSD_CROP_Y        (CTRL_REG_BASE_ADDR + 0x0054)
#define DPHSD_CROP_X        (CTRL_REG_BASE_ADDR + 0x0058)
#define DPHSD_RATIO         (CTRL_REG_BASE_ADDR + 0x005C)
#define DPHSD_CTRL          (CTRL_REG_BASE_ADDR + 0x0060)
#define INT_CTRL_REG        (CTRL_REG_BASE_ADDR + 0x0064)
#define GAMMA_LUT00_REG     (CTRL_REG_BASE_ADDR + 0x0068)
#define GAMMA_LUT04_REG     (CTRL_REG_BASE_ADDR + 0x006C)
#define GAMMA_LUT08_REG     (CTRL_REG_BASE_ADDR + 0x0070)
#define GAMMA_LUT0C_REG     (CTRL_REG_BASE_ADDR + 0x0074)
#define GAMMA_LUT10_REG     (CTRL_REG_BASE_ADDR + 0x0078)
#define GAMMA_BLKLVL_REG    (CTRL_REG_BASE_ADDR + 0x007C)
#define RESHAPE_REG         (CTRL_REG_BASE_ADDR + 0x0080)
#define ADDER_CTRL          (CTRL_REG_BASE_ADDR + 0x0084)
#define ADDER_PARAM0        (CTRL_REG_BASE_ADDR + 0x0088)
#define ADDER_PARAM1        (CTRL_REG_BASE_ADDR + 0x008C)
#define ADDER_PARAM2        (CTRL_REG_BASE_ADDR + 0x0090)

#define MASK_BIT_3          0x0007
#define MASK_BIT_4          0x000F
#define MASK_BIT_7          0x007F
#define MASK_BIT_8          0x00FF
#define MASK_BIT_9          0x01FF
#define MASK_BIT_10         0x03FF
#define MASK_BIT_11         0x07FF
#define MASK_BIT_12         0x0FFF
#define MASK_BIT_13         0x1FFF
#define MASK_BIT_14         0x3FFF
#define MASK_BIT_15         0x7FFF
#define MASK_BIT_16         0x0000FFFF
#define MASK_BIT_17         0x0001FFFF
#define MASK_BIT_18         0x0003FFFF
#define MASK_BIT_19         0x0007FFFF
#define MASK_BIT_20         0x000FFFFF

#define CNN_CTRL_OFF_ENABLE             0
#define CNN_CTRL_OFF_BOOT_ENABLE        1
#define CNN_CTRL_OFF_BOOT_STATUS        2
#define CNN_CTRL_OFF_FD_WEIGHT_OFF      13
#define CNN_CTRL_OFF_EXT_WEIGHT_RESET   30
#define CNN_CTRL_OFF_WEIGHT_TYPE        31

#define CNN_CTRL1_OFF_CMD_OFFSET        0

#define CNN_STATUS_OFF_CNN_FINISH       0
#define CNN_STATUS_OFF_RDIN_FINISH      1
#define CNN_STATUS_OFF_SNRSD_FINISH     2
#define CNN_STATUS_OFF_DPHSD_FINISH     3
#define CNN_STATUS_OFF_INT_CNN          4
#define CNN_STATUS_OFF_INT_RDIN         5
#define CNN_STATUS_OFF_INT_SNRSD        6
#define CNN_STATUS_OFF_INT_DPHSD        7
#define CNN_STATUS_OFF_CRAM_WRCNT_OV    30
#define CNN_STATUS_OFF_WRAM_WRCNT_OV    31

#define CNN_STATUS1_OFF_CRAM_WRCNT      0
#define CNN_STATUS1_OFF_WRAM_WRCNT      13

#define RDIN_PADNO_OFF_LEFT_RIGHT       0
#define RDIN_PADNO_OFF_TOP_DOWN         8
#define RDIN_CTRL_REG_OFF_ENABLE        0
#define RDIN_CTRL_REG_OFF_FRAME_SEL     1
#define RDIN_CTRL_REG_OFF_FINISH        4
#define RDIN_CTRL_REG_OFF_DIVIDE        8
#define RDIN_CTRL_REG_OFF_MOVE_MODE     15
#define RDIN_CTRL_REG_OFF_SUBT          16
#define RDIN_CTRL_DIVIDE_BY_128         1
#define RDIN_CTRL_DIVIDE_BY_256         0
#define RDIN_CTRL_NO_DIVIDE             8

#define SCALE_IMG_OFF_WIDTH             0
#define SCALE_IMG_OFF_HEIGHT            16
#define SCALE_IMG_OFF_ADDR              0
#define SCALE_DST_OFF_WIDTH             0
#define SCALE_DST_OFF_HEIGHT            16
#define SCALE_CROP_ADR_OFF_START        0
#define SCALE_CROP_WH_OFF_W             0
#define SCALE_CROP_WH_OFF_H             16
#define SCALE_RATIO_OFF_H               0
#define SCALE_RATIO_OFF_V               16
#define SCALE_CTRL_OFF_ENABLE           0
#define SCALE_CTRL_OFF_RESET            8

#define SNRSD_IMG_OFF_WIDTH             0
#define SNRSD_IMG_OFF_HEIGHT            16
#define SNRSD_DST_OFF_WIDTH             0
#define SNRSD_DST_OFF_HEIGHT            16
#define SNRSD_RATIO_OFF_H               0
#define SNRSD_RATIO_OFF_V               16
#define SNRSD_CROP_OFF_START            0
#define SNRSD_CROP_OFF_END              16
#define SNRSD_CTRL_OFF_EN               0
#define SNRSD_CTRL_OFF_RESET            8

#define DPHSD_IMG_OFF_WIDTH             0
#define DPHSD_IMG_OFF_HEIGHT            16
#define DPHSD_DST_OFF_WIDTH             0
#define DPHSD_DST_OFF_HEIGHT            16
#define DPHSD_RATIO_OFF_H               0
#define DPHSD_RATIO_OFF_V               16
#define DPHSD_CROP_OFF_START            0
#define DPHSD_CROP_OFF_END              16
#define DPHSD_CTRL_OFF_EN               0
#define DPHSD_CTRL_OFF_RESET            8

#define GAMMA_LUT_OFF_0                 0
#define GAMMA_LUT_OFF_1                 8
#define GAMMA_LUT_OFF_2                 16
#define GAMMA_LUT_OFF_3                 24
#define GAMMA_BLKLVL_OFF                0

#define RESHAPE_OFF_WIDTH               0
#define RESHAPE_OFF_VWIDTH              4
#define RESHAPE_OFF_HEIGHT              8
#define RESHAPE_OFF_VHEIGHT             12
#define RESHAPE_OFF_CHANNEL             16
#define RESHAPE_OFF_DIR                 30
#define RESHAPE_OFF_RUN                 31

#define ADDER_OFF_ENABLE                0
#define ADDER_OFF_BIAS                  0
#define ADDER_OFF_SCALE                 0
#define ADDER_OFF_LEAKY                 16
#define ADDER_OFF_RADDR                 0

#endif
