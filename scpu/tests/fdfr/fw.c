/**
 * @file      fr.c
 * @brief     Kneron facial recognition function.
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include <assert.h>
#include <unistd.h>
#include <stdio.h>
#include "memory.h"
#include "umeyama.h"
#include "imagecv.h"
#include "matrix.h"
#include "facealign.h"
#include "fw.h"
#include "fr.h"
#include "fd.h"
#include "base.h"
#include "reg.h"
#include "sys.h"
#include "cnn.h"
#include "hwctrl.h"
#include "profile.h"

/* ############################
 * ##    Static Functions    ##
 * ############################ */

/**
 * @brief Process NIR & depth images
 *        Run face detection & face recognition
 * @return true if the face is recognized
 */
#ifdef DYFP
bool ProcessImg(void)
#else
static bool ProcessImg(void)
#endif
{
    StartFetchNirImg();
    StartFetchDepthImg();
    //TODO: Modified for image self test
    #ifdef DYFP
    Image *pNir = MakeImageWithData(ORG_IMG_WIDTH, ORG_IMG_HEIGHT, (void*) DDR_IMG_BASE_ADDR);
    #else
    Image *pNir = GetNirImg();
    #endif
    Image *pDepth = GetDepthImg();
    Image *pWarped = NULL;
    Matrix *pLandmark = NULL;
    Matrix* pFeat = NULL;
    bool isValidFaceDet = false;
    Box bbox = {0};
    bool blDet = FaceDet(pNir, pDepth, &bbox, &pLandmark, isValidFaceDet);

    if (true == blDet) {
        pWarped = FaceAlign(pNir, &bbox, pLandmark);
        pFeat = GetFrFeature(pWarped);
        for(int i =0; i < 512; ++i) {
            printf("%f\n\r",pFeat->data.f[i]);
        }

        /* TODO: calculate distance */
    }

    FreeMatrix(pFeat);
    FreeMatrix(pLandmark);
    FreeImage(pWarped);
    FreeImage(pDepth);
    FreeImage(pNir);

    return false;
}

/* ##############################
 * ##    Exported Functions    ##
 * ############################## */

int FwMain(void)
{
    void *pMemCur = MemGetCur();
    int cnt = 1;

    while (cnt--) {
        ProcessImg();
        MemReset(pMemCur);
    }

    return 0;
}

#ifdef SYS_PC

extern const char *FACE_IMG_BIN_L;

static int Init(void)
{
    if (0 != CnnInit()) {
        printf("Failed to init cnn.\n");
        return -1;
    }

    FdInit();

    return 0;
}

int main(int argc, char **argv)
{
    if (0 != Init()) {
        printf("Failed to init.\n");
        return -1;
    }

    if (argc == 2) {
        FACE_IMG_BIN_L = argv[1];
    }

    return FwMain();
}

#endif
