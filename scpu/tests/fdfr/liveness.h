#ifndef __LIVENESS_H__
#define __LIVENESS_H__

#include "matrix.h"
#include "image_base.h"

typedef struct{
    int x, y, w, h;
    float score;
} rect;

Image preprocess_depth_image(Image depth_im, rect rc, float *landmarks, int scale);



#endif
