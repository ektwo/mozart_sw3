#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <math.h>
#include "dma.h"
#include "image.h"
#include "reg.h"
#include "hwctrl.h"
#include "cnn.h"
#include "profile.h"
#include "fd.h"
#include "landmark.h"
#include "fr.h"
#include "facealign.h"
#include "memory.h"
#include "fw.h"
#include "fwmain.h"
#include "DrvUART010.h"
#include "string.h"

// for benchmark by Vincent
void testRotate(void)
{ 
    //init test input
    int row=640;//864;
    int col=360;//491;
    //short *pS= (void*)DDR_IMG_BASE_ADDR;
    //short *pD= (void*)(DDR_IMG_BASE_ADDR + (row*col));
    unsigned char* pS= (unsigned char*)DDR_IMG_BASE_ADDR;
    unsigned char* pD= (unsigned char*)(DDR_IMG_BASE_ADDR + (row*col));


    //timer 1
    UINT32 timer_1 = fLib_CurrentT1Tick();

    //body: only simulate one ch
    unsigned int r, c; 
    //char *pw, *pr;
    for (r = 0; r < row; r++) 
    { 
        //pw = (char*)(pD + row - r - 1);
        //pr = (char*)(pS + r * col);
        for (c = 0; c < col; c++) 
        { 
            //4 here is for RGBA format
            *(pD + (c * row + (row - r - 1))) =  
                             *(pS + (r * col + c)); 
            //*(pLD + c*row ) = *(pLS + c);
            
            //*pw = *pr++;
            //pw = pw + row;
        } 
    } 

    //timer2
    UINT32 timer_2 = fLib_CurrentT1Tick();
    fLib_printf("time rotate(640x360) : %d\n", timer_2 - timer_1);

    return;
} 

/*
#include "NN_CPU.h"
void test_softmax(void)
{
    float* aaa = (float*)DDR_IMG_BASE_ADDR;
    DoProfStart();
    softmax(aaa, 1024);
    DoProfClick("softmax time:");
}
*/

/*
// for benchmark by Vincent
void testLVPost(void)
{
    //timer 1
    UINT32 timer_1 = fLib_CurrentT1Tick();

    //prepare curv_map
    short *temp_curv = (void*)DDR_IMG_BASE_ADDR;
    float* curv_map = (float*)MemAlloc(sizeof(float) * 2312);
    //float* curv_map = (float*)malloc(sizeof(float)*2312);

    int i;
    for(i=0; i<2312;i++){
        curv_map[i] = ((float)temp_curv[i]/(float)(1<<14));
    }

    float curv_accum = 0.;
    for (i = 0; i < 2312; i++) {
        if (curv_map[i] < 0) {
            curv_accum += (-curv_map[i]);
        } else {
            curv_accum += curv_map[i];
        }
    }

     //timer2
    UINT32 timer_2 = fLib_CurrentT1Tick();
    fLib_printf("time1 : %d\n", timer_2 - timer_1);

    if ((curv_accum / 3136.0) < 0.1 ) {
      return;
    }

    free(curv_map);
}
*/

/*
// for benchmark by Vincent
void testFRPost(void)
{
//Normalize embedding
//void NormalizeEmbedding(float *x, int emb_size)
    float* x = (void*)DDR_IMG_BASE_ADDR;

    //timer1
    UINT32 timer_1 = fLib_CurrentT1Tick();

    float sum_x = 0.;
    int i = 0;

    for(i = 0; i < 512 ; i++) {
        sum_x += powf(x[i], 2);
    }

    sum_x = sqrtf(sum_x);

    for(i = 0; i < 512; i++) {
        x[i] /= sum_x;
    }

    //timer2
    UINT32 timer_2 = fLib_CurrentT1Tick();
    fLib_printf("time1 : %d\n", timer_2 - timer_1);
}
*/
/*
// for benchmark by Vincent
// usage: need to comment below three line on FaceAlign of facealign.c
        Image *pTmp = SubImage(pImg, bb.y1, bb.y2, bb.x1, bb.x2);
        cvResize(pRet, pTmp);
        FreeImage(pTmp);

Image* LoadImageIn(int height, int width, int addr);
void testAlignment(void)
{
    //prepare pImg
    Image *pImg = MakeImageWithData(640, 360, (void*)DDR_IMG_BASE_ADDR);
    //LoadImageIn(ORG_IMG_HEIGHT, ORG_IMG_WIDTH, DDR_IMG_BASE_ADDR);

    //prepare bbox
    Box bbox = {0};
    bbox.x1 = 204;
    bbox.y1 = 105;
    bbox.x2 = 393;
    bbox.y2 = 339;

    //prepare pLandMark
    float fLandMark[10] = {257,180, 340,172, 302,243, 272,288, 337,281};
    Matrix* pLandmark = NULL;
    pLandmark = MakeMatrixWithData(2, 5, MT_FLOAT, (void*)fLandMark);

    //timer1
    UINT32 timer_1 = fLib_CurrentT1Tick();
    Image *pWarped = NULL;

    pWarped = FaceAlign(pImg, &bbox, pLandmark);
    UINT32 timer_2 = fLib_CurrentT1Tick();
    fLib_printf("time1 : %d\n", timer_2 - timer_1);
}
*/
/* 
// for benchmark by Vincent
void rotate(unsigned char *pS, 
            unsigned char *pD,
            unsigned short row, 
            unsigned short col)
{
    pS = (unsigned char*)DDR_IMG_BASE_ADDR;
    pD = (unsigned char*)(DDR_IMG_BASE_ADDR + 640 * 480);
    row = 640;
    col = 480; 
    u16 r, c;

    UINT32 timer_1 = fLib_CurrentT1Tick();
 
    for(int i=0;i<3;i++) {
        for (r = 0; r < row; r++)
        {
            for (c = 0; c < col; c++)
            {   
                *(pD + c * row + (row - r - 1)) = 
                            *(pS + r * col + c);
            }   
        }
    }

    UINT32 timer_2 = fLib_CurrentT1Tick();
    fLib_printf("time1 : %d\n", timer_2 - timer_1);
}
*/


/*
// for benchmark by Vincent
float FrCalDist_u8(const Matrix *pFeat1, const Matrix *pFeat2)
{
    assert(pFeat1->width * pFeat1->height == pFeat2->width * pFeat2->height);
    assert(pFeat1->type == MT_UCHAR && pFeat1->type == MT_UCHAR);

    int size = pFeat1->width * pFeat1->height;
    char *pC1 = pFeat1->data.c;
    char *pC2 = pFeat2->data.c;
    float sum = 0;

    while (size--) {
        sum += ((float)(*pC1-*pC2))*((float)(*pC1-*pC2));
        pC1++;
        pC2++;
    }

    return sqrtf(sum);//SQRT(sum);
}

void compareBench(Matrix *pFeature, float *pFeatureDist)
{
    //prepare test data
    //to test matrix:
    pFeature =   MakeMatrixWithData(1, 512, MT_UCHAR, (void*)DDR_IMG_BASE_ADDR);
    //db matrix;
    Matrix* pDBFeature = MakeMatrixWithData(1, 512, MT_UCHAR, (void*)(DDR_IMG_BASE_ADDR+512));


    float min = 100;
    char str[15];

    UINT32 timer_1 = fLib_CurrentT1Tick();
    //DoProfStart();
    int RegCnt = 1;
    //sprintf(str, "%d times=\0", RegCnt);

    for (int i = 0; i < RegCnt; ++i) {
        float dist = FrCalDist_u8(pFeature, pDBFeature);
        fLib_printf("=> [%d]: dist: [%.2f]\n", i, dist);

        if ((dist < 1000) && (dist < min)) {
            ; //do nothing
        }

        min = MIN(min, dist);
    }
    
    UINT32 timer_2 = fLib_CurrentT1Tick();
    fLib_printf("time1 : %d\n", timer_2 - timer_1);
    
    for (int i = 0; i < 10; ++i) {
        float dist = FrCalDist_u8(pFeature, pDBFeature);
        //fLib_printf("=> [%d]: dist: [%.2f]\n", i, dist);

        if ((dist < 1000) && (dist < min)) {
            ; //do nothing
        }

        min = MIN(min, dist);
    }

    UINT32 timer_3 = fLib_CurrentT1Tick();
    fLib_printf("time10 : %d\n", timer_3 - timer_2);
    
    for (int i = 0; i < 100; ++i) {
        float dist = FrCalDist_u8(pFeature, pDBFeature);
        //fLib_printf("=> [%d]: dist: [%.2f]\n", i, dist);

        if ((dist < 1000) && (dist < min)) {
            ; //do nothing
        }

        min = MIN(min, dist);
    }

        UINT32 timer_4 = fLib_CurrentT1Tick();
    fLib_printf("time100 : %d\n", timer_4 - timer_3);

    for (int i = 0; i < 1000; ++i) {
        float dist = FrCalDist_u8(pFeature, pDBFeature);
        //fLib_printf("=> [%d]: dist: [%.2f]\n", i, dist);

        if ((dist < 1000) && (dist < min)) {
            ; //do nothing
        }

        min = MIN(min, dist);
    }
    
    *pFeatureDist = min;

    FreeMatrix(pFeature);

    //DoProfClick(str);
    UINT32 timer_5 = fLib_CurrentT1Tick();
    fLib_printf("time1000 : %d\n", timer_5 - timer_4);
}
*/

/* ##################
 * ##    Define    ##
 * ################## */

/* #define Log(format, arg...)    xil_printf(format, ##arg) */
#define Log(...)

//#define MAX_BUF_SIZE            (640 << 10)
// ASTA: TODO check size
#define MAX_BUF_SIZE            (2 << 10)
#define DEF_FR_DIST_THRESH      0.97
#define MAX_FR_REGISTER_SIZE    30
#define ACK_MSG                 "UsbAck"

#ifdef __ASTA__
#else
static int sRegCnt = 0;
static Matrix *sFeatures[MAX_FR_REGISTER_SIZE] = {0};
#endif

typedef struct {
    int cmd;
    u32 dataLen;
} __attribute__((packed)) MsgHdr;

typedef struct {
    u32 addr;
    u32 dataLen;
} __attribute__((packed)) MemCtrlData;

typedef int (*FuncPtr)(const MsgHdr *pHdr, const u8* pData, u8 *pBuf);


typedef enum {
    CMD_NONE = 0,
    CMD_MEM_READ,
    CMD_MEM_WRITE,
    CMD_TEST_ECHO,
    CMD_TEST_IMG_SCALE,
    CMD_TEST_CNN,
    CMD_TEST_ADDER,
    CMD_TEST_RESHAPE,
    CMD_DEMO_RESET,
    CMD_DEMO_PROCESS_IMG,
} Cmd;

typedef enum {
    CNN_TYPE_NONE,
    CNN_TYPE_FD,
    CNN_TYPE_LANDMARK,
    CNN_TYPE_NIR_FR,
} CNN_TYPE;

typedef enum {
    DEMO_FD_ONLY,           // FD detect only
    DEMO_REGISTER_2D,       // Register 2D     
    DEMO_INFERENCE_2D,      // Inference 2D     
    DEMO_REGISTER_2D3D,     // Register 2D/3D     
    DEMO_INFERENCE_2D3D,    // Inference 2D/3D 
} DEMO_TYPE;

typedef struct {
    int width;
    int height;
    int channel;
    int validW;
    int validH;
    int direction;
} __attribute__((packed)) ReshapeParam;

typedef struct {
    int bias;
    int scale;
    int leaky;
    int raddrAddr;
    int dstAddr;
    int len;
} __attribute__((packed)) AdderParam;


static int DemoProcessImg(const MsgHdr *pHdr, const u8 *pData, u8 *pBuf);

/* ############################
 * ##    Static Functions    ##
 * ############################ */



/**
 * @brief Init platform
 */
int FwInit(void)
{
    if (0 != CnnInit()) {
        fLib_printf("Failed to init cnn.\n");
    }

    FdInit();

    return 0;
}

//static int SendMsg(const MsgHdr *pHdr, const u8 *pData)
int SendMsg(const MsgHdr *pHdr, const u8 *pData)
{
#if 0    
    if (SendResponeToUIfromUSB((const char*) pHdr, sizeof(MsgHdr)) != 0) {
        fLib_printf("Failed to send msg\n");
        return -1;
    }

    if (SendResponeToUIfromUSB((const char*) pData, pHdr->dataLen) != 0) {
        fLib_printf("Failed to send msg\n");
        return -1;
    }
#endif
    return 0;
}

/**
 * @brief Load image from bin data
 *        the first two int are width & height
 *        then the followings are image data
 * @param pData binary data
 * @return image
 */



Image* DoLoadImageIn(int height, int width, int addr, bool isDepth)
{
    #ifdef __ASTA__    
    #else
    return isDepth ?
        MakeMatrixWithData(height, width, MT_INT, (void*)addr) :
        MakeImageWithData(height, width, (void*)addr);
    #endif
}

Image* LoadImageIn(int height, int width, int addr)
{
    return DoLoadImageIn(height, width, addr, false);
}

Image* LoadDepthImageIn(int height, int width, int addr)
{
    return DoLoadImageIn(height, width, addr, true);
}


int PrepareCnnResp(u8 *pBuf, const Matrix *pOut)
{
    int size = 0;

    if (pOut) {
        size = (pOut->width*pOut->height)*sizeof(float);
        memcpy(pBuf, pOut->data.f, size);
    }

    return size;
}

#ifndef __MOZART__
int PrepareCnnResp2(u8 *pBuf, const Matrix *pOut1, const Matrix *pOut2)
{
    int size = PrepareCnnResp(pBuf, pOut1);
    size += PrepareCnnResp(pBuf + size, pOut2);
    return size;
}
#endif






void InitSram(int baseAddr, int off1, int off2, int str, int end)
{
#ifdef __ASTA__
#else
    u32 *pVal = (u32*) baseAddr;
    int diff = end - str + 1;

    for (int i = str; i <= end; ++i) {
        u16 x0 = (off1 + i) & 0xFFFF;
        u16 x1 = (off1 + i + diff) & 0xFFFF;
        *pVal++ = (x1 << 16) + x0;
    }

    for (int i = str; i <= end; ++i) {
        u16 x0 = (off2 + i) & 0xFFFF;
        u16 x1 = (off2 + i + diff) & 0xFFFF;
        *pVal++ = (x1 << 16) + x0;
    }
#endif
}



u32 PrepareProcImgResp(u8 *pBuf, bool blDet, const Box *pBox,
                              const Matrix *pLandmark, const Image *pImg,
                              int personIdx, float featureDist)
{
    #ifdef __ASTA__
    #else
    /* blDet */
    u8 *pCur = pBuf;
    int *pInt = (int*) pCur;
    *pInt++ = (int) blDet;
    pCur = (u8*) pInt;

    /* face box */
    *pInt++ = pBox->x1;
    *pInt++ = pBox->y1;
    *pInt++ = pBox->x2;
    *pInt++ = pBox->y2;
    pCur = (u8*) pInt;

    /* face landmark */
    if (pLandmark) {
        float *pF = pLandmark->data.f;
        int size = 10*sizeof(float);
        memcpy(pCur, pF, size);
        pCur += size;
    }

    /* aligned face img */
    if (pImg) {
        pInt = (int*) pCur;
        *pInt++ = pImg->width;
        *pInt++ = pImg->height;
        pCur = (u8*) pInt;

        imdata *pSrc = IM_DATA(pImg);
        int size = pImg->width*pImg->height;
        memcpy(pCur, pSrc, size*sizeof(imdata));
        pCur += size;
    }

    /* FR result */
    *((int*) pCur) = personIdx;
    pCur += sizeof(int);
    *((float*) pCur) = featureDist;
    pCur += sizeof(float);

    return pCur - pBuf;
    #endif    
    return 0;
}

void FrRegister(Matrix *pFeature)
{
    #if __ASTA__
    #else
    assert(sRegCnt < MAX_FR_REGISTER_SIZE || !"Exceeds max register limit!");

    /* Use malloc here since pFeature is allocated through MemAlloc(),
     * the MemAlloced space will be recycled after the message processed */
    Matrix *pMat = (Matrix*) malloc(sizeof(Matrix));
    *pMat = *pFeature;

    int size = sizeof(float)*pMat->width*pMat->height;
    pMat->data.f = (float*) malloc(size);
    memcpy(pMat->data.f, pFeature->data.f, size);

    sFeatures[sRegCnt++] = pMat;
    #endif
}

void FrInference(Matrix *pFeature, int *pPersonIdx, float *pFeatureDist)
{
    #if __ASTA__
    #else
    assert(sRegCnt > 0);

    float min = 100;

    for (int i = 0; i < sRegCnt; ++i) {
        float dist = FrCalDist(pFeature, sFeatures[i]);
        fLib_printf("=> [%d]: dist: [%.2f]\n", i, dist);

        if ((dist < DEF_FR_DIST_THRESH) && (dist < min)) {
            *pPersonIdx = i;
        }

        min = MIN(min, dist);
    }
    *pFeatureDist = min;

    FreeMatrix(pFeature);
    #endif
}

static void FrReset()
{
    #if __ASTA__
    #else
    for (int i = 0; i < sRegCnt; ++i) {
        Matrix *pFeature = sFeatures[i];

        /* Use free() rather than FreeMatrix(), since it's allocated through malloc() */
        if (pFeature) {
            free(pFeature->data.f);
            free(pFeature);
            pFeature = NULL;
        }
    }

    sRegCnt = 0;
    #endif
}

static int DemoProcessImg(const MsgHdr *pHdr, const u8 *pData, u8 *pBuf)
{
    DEMO_TYPE type = (DEMO_TYPE) *(int*) pData;
    Image *pImg = LoadImageIn(ORG_IMG_HEIGHT, ORG_IMG_WIDTH, 0x68300000);
    Image *pDepthImg = NULL;
    //Image *pWarped = NULL;
    Matrix *pLandmark = NULL;
    //Matrix *pFeat = NULL;
    Box bbox = {0};
    bool blDet = false;
    bool isValidFaceDet = false;
    //int personIdx = -1;
    //float featureDist = -1;

    ProfStart();
    isValidFaceDet = 
            ((type == DEMO_REGISTER_2D) || (type == DEMO_INFERENCE_2D))? false : true;
    blDet = FaceDet(pImg, pDepthImg, &bbox, &pLandmark, isValidFaceDet);

#if 0
    if (blDet) {
        pWarped = FaceAlign(pImg, &bbox, pLandmark);
        if ((type == DEMO_REGISTER_2D) || (type == DEMO_REGISTER_2D3D)) {
            pFeat = GetFrFeature(pWarped);
            FrRegister(pFeat);
        } else if ((type == DEMO_INFERENCE_2D) || (type == DEMO_INFERENCE_2D3D)) {
            pFeat = GetFrFeature(pWarped);
            FrInference(pFeat, &personIdx, &featureDist);
        }
    }
    ProfClick(__func__);

    u32 respSize = PrepareProcImgResp(pBuf, blDet, &bbox, pLandmark,
                                      pWarped, personIdx, featureDist);
    MsgHdr respHdr = { pHdr->cmd, respSize };

    int ret = SendMsg(&respHdr, (const u8*) pBuf);

    FreeImage(pImg);
    FreeImage(pDepthImg);
    FreeImage(pWarped);
    FreeMatrix(pLandmark);

    return ret;
    #else
    return 0;
    #endif
}

void DoDemoProcessImg(void)
{
    const MsgHdr *pHdr;
    const u8 *pData;
    static u8 pRespBuf[MAX_BUF_SIZE];
    pData = (const u8 *)PC_IMG_IN;
    DemoProcessImg(pHdr, pData, pRespBuf);
}

void DoVerifyFaceDet(void)
{
    Matrix *pFeature = NULL;
    Matrix *pFeaturePool = NULL;
    RunCnnFdDyFp(NULL, &pFeature, &pFeaturePool);
}

void DoVerifyLandmark(void)
{
    RunLandmarkBd(NULL, NULL, NULL);
}

void DoVerifyLiveness(void)
{
    RunCnnLivenessDyFp(NULL, NULL, NULL);
}

void DoVerifyFaceRecog(void)
{
    RunCnnNirFrVggDyFp(NULL, NULL);
}

/* ################
 * ##    Main    ##
 * ################ */

int fwmain(void)
{
    #if 0
    static u8 pData[MAX_BUF_SIZE];
    MsgHdr hdr;

    if (0 != Init()) {
        fLib_printf("Failed to init\n");
        return -1;
    }

    void *pMemCur = MemGetCur();
    for (;;) {        
        ReadMsg(&hdr, pData);
        ProcessMsg(&hdr, pData);
        MemReset(pMemCur);
    }

    return 0;
    #else
    return 0;
    #endif
}
