/**
 * @file      landmark.h
 * @brief     Landmark functions
 * @version $Id: landmark.h 555 2018-08-14 03:13:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */



#ifndef __LANDMARK_H__
#define __LANDMARK_H__


#include "base.h"
#include "matrix.h"
//#include "fd.h"
#include "sys.h"
#include "hwctrl.h"
#include "box.h"

// include box.h to replace the following code
/* left top right bottom */
/*
#ifndef DYFP
typedef struct _Box {
    int x1;
    int y1;
    int x2;
    int y2;
} Box;
#endif
*/

ImgScaleParam GetLandmarkScaleParam(const Box *pBox);
void RunLandmarkBd(const Image *pImg, const Box *pBox, Matrix **ppLandmark);
#ifdef DYFP
void ScaleLandmarkResult(Matrix *pMat, const Box *pBox);
#endif


#endif
