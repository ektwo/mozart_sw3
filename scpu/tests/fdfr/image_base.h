/**
 * @file      image_base.h
 * @brief     image base definitions
 * @version $Id: image_base.h 498 2018-07-16 05:43:35Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __IMAGE_BASE_H__
#define __IMAGE_BASE_H__



/* Our image has only one channel */
typedef Matrix  Image;
typedef u8      imdata;

#define IMG_DATA_TYPE  MT_UCHAR

/* If change image data type => change IM_DATA macro as well */

#endif
