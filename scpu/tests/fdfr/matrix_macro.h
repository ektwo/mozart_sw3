/**
 * @file      matrix_macro.h
 * @brief     Matrix macros
 * @version $Id: matrix_macro.h 498 2018-07-16 05:43:35Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __MATRIX_MACRO_H__
#define __MATRIX_MACRO_H__


#define M_MAX_TYPE(type1, type2)              (MAX(type1, type2))

/* ###########################
 * ##    Exported Macros    ##
 * ########################### */

#define M_GET_VAL_UCHAR(dataC, width, row, col)  _M_GET_VAL(dataC, (width), (row), (col))
#define M_GET_VAL_INT(dataI, width, row, col)    _M_GET_VAL(dataI, (width), (row), (col))
#define M_GET_VAL_FLOAT(dataF, width, row, col)  _M_GET_VAL(dataF, (width), (row), (col))

#define M_GET_VAL(data, type, width, row, col)                     \
    ((type == MT_INT)   ? (_M_GET_VAL(data.i, width, row, col)) :  \
     (type == MT_FLOAT) ? (_M_GET_VAL(data.f, width, row, col)) :  \
                          (_M_GET_VAL(data.c, width, row, col)))
#define M_SET_VAL(data, type, width, row, col, val)                   \
    ((type == MT_INT)   ? _M_SET_VAL(data.i, width, row, col, val) :  \
     (type == MT_FLOAT) ? _M_SET_VAL(data.f, width, row, col, val) :  \
                          _M_SET_VAL(data.c, width, row, col, val))
#define M_ADD_VAL(data, type, width, row, col, val)                   \
    ((type == MT_INT)   ? _M_ADD_VAL(data.i, width, row, col, val) :  \
     (type == MT_FLOAT) ? _M_ADD_VAL(data.f, width, row, col, val) :  \
                          _M_ADD_VAL(data.c, width, row, col, val))
#define M_SUB_VAL(data, type, width, row, col, val)                   \
    ((type == MT_INT)   ? _M_SUB_VAL(data.i, width, row, col, val) :  \
     (type == MT_FLOAT) ? _M_SUB_VAL(data.f, width, row, col, val) :  \
                          _M_SUB_VAL(data.c, width, row, col, val))
#define M_MUL_VAL(data, type, width, row, col, val)                   \
    ((type == MT_INT)   ? _M_MUL_VAL(data.i, width, row, col, val) :  \
     (type == MT_FLOAT) ? _M_MUL_VAL(data.f, width, row, col, val) :  \
                          _M_MUL_VAL(data.c, width, row, col, val))
#define M_DIV_VAL(data, type, width, row, col, val)                   \
    ((type == MT_INT)   ? _M_DIV_VAL(data.i, width, row, col, val) :  \
     (type == MT_FLOAT) ? _M_DIV_VAL(data.f, width, row, col, val) :  \
                          _M_DIV_VAL(data.c, width, row, col, val))

#define M_GET_VAL_1D(data, type, offset)                   \
    ((type == MT_INT)   ? _M_GET_VAL_1D(data.i, offset) :  \
     (type == MT_FLOAT) ? _M_GET_VAL_1D(data.f, offset) :  \
                          _M_GET_VAL_1D(data.c, offset))
#define M_SET_VAL_1D(data, type, offset, val)                   \
    ((type == MT_INT)   ? _M_SET_VAL_1D(data.i, offset, val) :  \
     (type == MT_FLOAT) ? _M_SET_VAL_1D(data.f, offset, val) :  \
                          _M_SET_VAL_1D(data.c, offset, (u8) val))
#define M_ADD_VAL_1D(data, type, offset, val)                   \
    ((type == MT_INT)   ? _M_ADD_VAL_1D(data.i, offset, val) :  \
     (type == MT_FLOAT) ? _M_ADD_VAL_1D(data.f, offset, val) :  \
                          _M_ADD_VAL_1D(data.c, offset, val))
#define M_SUB_VAL_1D(data, type, offset, val)                   \
    ((type == MT_INT)   ? _M_SUB_VAL_1D(data.i, offset, val) :  \
     (type == MT_FLOAT) ? _M_SUB_VAL_1D(data.f, offset, val) :  \
                          _M_SUB_VAL_1D(data.c, offset, val))
#define M_MUL_VAL_1D(data, type, offset, val)                   \
    ((type == MT_INT)   ? _M_MUL_VAL_1D(data.i, offset, val) :  \
     (type == MT_FLOAT) ? _M_MUL_VAL_1D(data.f, offset, val) :  \
                          _M_MUL_VAL_1D(data.c, offset, val))
#define M_DIV_VAL_1D(data, type, offset, val)                   \
    ((type == MT_INT)   ? _M_DIV_VAL_1D(data.i, offset, val) :  \
     (type == MT_FLOAT) ? _M_DIV_VAL_1D(data.f, offset, val) :  \
                          _M_DIV_VAL_1D(data.c, offset, val))

/* #################################
 * ##    Implementation Macros    ##
 * ################################# */

#define _M_GET_VAL(pData, width, i, j)        (_M_GET_VAL_1D(pData, (i)*(width)+(j)))
#define _M_SET_VAL(pData, width, i, j, val)   (_M_SET_VAL_1D(pData, (i)*(width)+(j), (val)))
#define _M_ADD_VAL(pData, width, i, j, val)   (_M_ADD_VAL_1D(pData, (i)*(width)+(j), (val)))
#define _M_SUB_VAL(pData, width, i, j, val)   (_M_SUB_VAL_1D(pData, (i)*(width)+(j), (val)))
#define _M_MUL_VAL(pData, width, i, j, val)   (_M_MUL_VAL_1D(pData, (i)*(width)+(j), (val)))
#define _M_DIV_VAL(pData, width, i, j, val)   (_M_DIV_VAL_1D(pData, (i)*(width)+(j), (val)))

#define _M_GET_VAL_1D(pData, offset)          ((pData)[offset])
#define _M_SET_VAL_1D(pData, offset, val)     ((pData)[offset] = (val))
#define _M_ADD_VAL_1D(pData, offset, val)     ((pData)[offset] += (val))
#define _M_SUB_VAL_1D(pData, offset, val)     ((pData)[offset] -= (val))
#define _M_MUL_VAL_1D(pData, offset, val)     ((pData)[offset] *= (val))
#define _M_DIV_VAL_1D(pData, offset, val)     ((pData)[offset] /= (val))

#endif
