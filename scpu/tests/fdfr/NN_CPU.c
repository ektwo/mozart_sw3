/**
 * @file      math.c
 * @brief     math functions
 */

#include "cmsis_os2.h"
#include "base.h"
#include <math.h>


void softmax(float* pScores, int pLen)
{
    int i;
    float fMAX;
    float tmp;
    
    //find MAX
    float *pfIndex = pScores;
    fMAX = *(pfIndex++);
    for (i = 1; i < pLen; i++) {
        tmp = *(pfIndex++); 
        if (tmp > fMAX) {
            fMAX = tmp;
        }
    }

    float sum = 0.0;
    pfIndex = pScores;
    for (i = 0; i < pLen; i++) {
        tmp = *(pfIndex++); 
        sum += expf(tmp - fMAX);
    }

    for (i = 0; i < pLen; i++) {
        *pScores = expf(*pScores - fMAX - logf(sum));
        pScores++;
    }    
}
