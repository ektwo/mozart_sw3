/**
 * @file      memory.h
 * @brief     Memory functions
 * @version $Id: memory.c 555 2018-08-14 03:13:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include "base.h"
#include "sys.h"
#include "memory.h"

#if USE_C_MALLOC

#include <stdlib.h>

void MemReset(void *ptr)
{
}

void* MemGetCur(void)
{
    return NULL;
}

void* MemAlloc(size_t size)
{
    return malloc(size);
}

void* MemCalloc(size_t nmemb, size_t size)
{
    return calloc(nmemb, size);
}

void MemFree(void *ptr)
{
    free(ptr);
}

#else

#include <string.h>
#include "base.h"

#if DBG_MEM
#include <stdio.h>
#define DbgLog(fmt, args...)  printf("[mem] " fmt "\n", ##args)
#else
#define DbgLog(fmt, args...)
#endif

#ifdef SYS_PC
#define MEM_BULK_SIZE          (1 << 20)
#else
/* TODO: this can be set smaller? */
    #ifdef __ASTA__
        #define MEM_BULK_SIZE          (20 << 10)
    #else
        #define MEM_BULK_SIZE          (360 << 10)
    #endif
//#define MEM_BULK_SIZE          (50 << 10)
#endif

#define MEM_ADDR_ALIGN_MASK    (0x1f)


#ifdef __ASTA__
static char _sMemBulk[MEM_BULK_SIZE] = {0};
#else
static char _sMemBulk[MEM_BULK_SIZE] __attribute__((at(0x88000000)));
#endif
static char *_sCur = NULL;
static char *_sPeak = NULL;

static void MemInit()
{
    _sCur = _sMemBulk;
    _sPeak = _sMemBulk + MEM_BULK_SIZE;
    DbgLog("Mem range: [%p] - [%p]", (void*) _sCur, _sPeak);
}

void MemReset(void *ptr)
{
    _sCur = (char*) ptr;
    DbgLog("reset ptr, current usage: [%td]", _sCur-_sMemBulk);
}

void* MemGetCur(void)
{
    return _sCur;
}

void* MemAlloc(size_t size)
{
    if (_sCur == NULL) {
        MemInit();
    }

    addr_t addr = (addr_t)(void*)_sCur;
    if (addr & MEM_ADDR_ALIGN_MASK) {
        addr = (addr + MEM_ADDR_ALIGN_MASK) & ~MEM_ADDR_ALIGN_MASK;
    }

    if (addr + size < (addr_t) _sPeak) {
        _sCur = (void*) (addr + size);
        DbgLog("addr: [%p], req size: [%zu], current usage: [%td]",
               (void*) addr, size, _sCur-_sMemBulk);
        return (void*) addr;
    } else {
        return NULL;
    }
}

void* MemCalloc(size_t nmemb, size_t size)
{
    size_t totalSize = nmemb*size;
    void *pData = MemAlloc(totalSize);

    if (pData) {
        memset(pData, 0, totalSize);
    }

    return pData;
}

void MemFree(void *ptr)
{
}

#endif
