/******************************************************************************
 * @brief Kneron facial recognition function.
 *
 * @coryright Kneron 2015~2018
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "fd.h"
#include "cnn.h"
#include "landmark.h"
#include "facealign.h"
#include "fdebug.h"
#include "profile.h"
#include "NN_CPU.h"


typedef struct {
    int min;
    int max;
    int threshL;
    int threshH;
} DepthNormParam;



bool Feature2Rectangle(Box *pBox, const Matrix *pFeature, const Matrix *pFeaturePool)
{
    Box box1, box2;
    Box *pSrcBox;
    int actPtCnt = GetFdBox1(pFeature, &box1);
    if (actPtCnt == 0) {
        return false;
    }

    float max;
    if (actPtCnt > FD_ACT_PT_CNT_THRESH) {
        max = MaxVal(pFeaturePool);
        pSrcBox = &box1;
    } else {
        max = GetFdBox2(pFeature, pFeaturePool, &box2);
        pSrcBox = &box2;
    }

    if (max < FD_FEATURE_POOL_THRESH) {
        return false;
    }

    GetFaceBox(pBox, pSrcBox);

    return IsValidBox(pBox);
}


/**
 * @brief Get face box from feature
 * @param pFeature matrix
 * @param pBox face box
 * @return number of activation points
 */
int GetFdBox1(const Matrix *pFeature, Box *pBox)
{
    float *pF = pFeature->data.f;
    int left = -1, top = -1, right = -1, bottom = -1;
    int r, c;
    int cnt = 0;

    //assert(pFeature->width == FD_CNN_OUT_SIZE);
    //assert(pFeature->height == FD_CNN_OUT_SIZE);

    for (r = 0; r < FD_CNN_OUT_SIZE; ++r) {
        for (c = 0; c < FD_CNN_OUT_SIZE; ++c) {
            if (*pF++ > FD_SCORE_THRESH) {
                left = (left < 0) ? c : left;
                top = (top < 0) ? r : top;
                right = c;
                bottom = r;
                cnt++;
            }
        }
    }

    pBox->x1 = left;
    pBox->y1 = top;
    pBox->x2 = right;
    pBox->y2 = bottom;
    return cnt;
}


/**
 * @brief Get face box from feature pool
 * @param pFeaturePool matrix
 * @param pBox face box
 * @return max score value in feature pool
 */
float GetFdBox2(const Matrix *pFeature, const Matrix *pFeaturePool, Box *pBox)
{
    float *pF = pFeaturePool->data.f;
    int left = -1, top = -1, right = -1, bottom = -1;
    float max = 0;
    int r, c;

    //assert(pFeaturePool->width == FD_CNN_OUT_SIZE);
    //assert(pFeaturePool->height == FD_CNN_OUT_SIZE);

    for (r = 0; r < FD_CNN_OUT_SIZE; ++r) {
        for (c = 0; c < FD_CNN_OUT_SIZE; ++c) {
            if (*pF > max) {
                max = *pF;
                left = right = c;
                top = bottom = r;
            }
            pF++;
        }
    }


    c = left;
    r = top;
    pF = pFeature->data.f;


    //// Deling: 4 while loops to search 4 directions
    while (left > 0) {
        // (r-1, left-1) -> upper_left
        // (r+1, left-1) -> lower_left
        float upper_left_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, r-1, left-1);
        float left_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, r, left-1);
        float lower_left_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, r+1, left-1);

        if (left_value > 0) {
            left--;
        } else if (upper_left_value > 0 && r > 0) {
            r--;
            left--;
        } else if (lower_left_value > 0 && r < FD_CNN_OUT_SIZE-1){
            r++;
            left--;
        } else {
            break;
        }
    }

    while (right < FD_CNN_OUT_SIZE-1) {
        float upper_right_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, r-1, right+1);
        float right_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, r, right+1);
        float lower_right_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, r+1, right+1);

        if (right_value > 0) {
            right++;
        } else if (lower_right_value > 0 && r < FD_CNN_OUT_SIZE-1){
            r++;
            right++;
        } else if (upper_right_value > 0 && r > 0) {
            r--;
            right++;
        } else {
            break;
        }
    }

    while (top > 0) {
        // (top-1, c-1) -> minus_top
        // (top-1, c+1) -> plus_top
        float minus_top_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, top-1, c-1);
        float top_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, top-1, c);
        float plus_top_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, top-1, c+1);

        if (top_value > 0) {
            top--;
        } else if (minus_top_value > 0 && c > 0) {
            c--;
            top--;
        } else if (plus_top_value > 0 && c < FD_CNN_OUT_SIZE-1){
            c++;
            top--;
        } else {
            break;
        }
    }

    while (bottom < FD_CNN_OUT_SIZE-1) {
        float minus_bot_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, bottom+1, c-1);
        float bot_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, bottom+1, c);
        float plus_bot_value = M_GET_VAL_FLOAT(pF, FD_CNN_OUT_SIZE, bottom+1, c+1);

        if (bot_value > 0) {
            bottom++;
        } else if (plus_bot_value > 0 && c < FD_CNN_OUT_SIZE-1){
            c++;
            bottom++;
        } else if (minus_bot_value > 0 && c > 0) {
            c--;
            bottom++;
        } else {
            break;
        }
    }
    pBox->x1 = left;
    pBox->y1 = top;
    pBox->x2 = right;
    pBox->y2 = bottom;
    return max;
}

/**
 * @brief Get final FD box from small FD box
 * @param pFace final FD box
 * @param pSrc small FD box
 */
void GetFaceBox(Box *pFace, const Box *pSrc)
{
    float scaleInit = (float) FD_IMG_SCALE_SIZE / MAX(ORG_IMG_WIDTH, ORG_IMG_HEIGHT);
    float scale = FD_IMG_SCALE_HEIGHT/FD_CNN_OUT_SIZE;
    float cX = (pSrc->x1 + pSrc->x2)/2.0 + 0.5;
    float cY = (pSrc->y1 + pSrc->y2)/2.0 + 0.5;
    cX = cX*scale - FD_PAD_SIZE_X;
    cY = cY*scale - FD_PAD_SIZE_Y;


    float length = pSrc->y2 - pSrc->y1 + 1;

    //// Deling: FD_RECT_MIN_SIZE = 3.5
    // length = MAX(length, FD_RECT_MIN_SIZE);
    length = MAX(length, 3.5);


    float width = (length*0.8)*scale;
    float height = (length)*scale;

    pFace->x1 = MAX(0, (cX-width/2)/scaleInit);
    pFace->y1 = MAX(0, (cY-height/2)/scaleInit);

    //// Deling: fixed
    // pFace->x2 = MIN(ORG_IMG_WIDTH, pFace->x1 + width/scaleInit);
    // pFace->y2 = MIN(ORG_IMG_HEIGHT, pFace->y1 + height/scaleInit);
    pFace->x2 = MIN(ORG_IMG_WIDTH,  (cX+width/2)/scaleInit);
    pFace->y2 = MIN(ORG_IMG_HEIGHT, (cY+height/2)/scaleInit);
}


/**
 * @brief Whether FD box is valid
 * @return valid
 */
bool IsValidBox(const Box *pBox)
{
    fLib_printf("[FD] det face box: (%d,%d,%d,%d)\n",
           pBox->x1, pBox->y1, pBox->x2, pBox->y2);

    if ((pBox->x2 > pBox->x1) && (pBox->y2 > pBox->y1) &&
        (pBox->x1 >= 0) && (pBox->y1 >= 0) &&
        (pBox->x2 <= ORG_IMG_WIDTH) && (pBox->y2 <= ORG_IMG_HEIGHT)) {
        return true;
    } else {
        fLib_printf("invalid face box!\n");
        return false;
    }
}


ImgScaleParam GetFdScaleParam(const Box *pBox)
{
    ImgScaleParam param = {0};
		#ifndef __MOZART__
    int scale = ORG_IMG_HEIGHT / FD_IMG_SCALE_SIZE;
		#endif
    param.isFrame0 = 1;
    param.moveMode = 0;
    param.srcW = ORG_IMG_WIDTH;
    param.srcH = ORG_IMG_HEIGHT;
    param.subVal = 0;
    param.divide = 256;

    param.cropXstr = pBox->x1;
    param.cropXend = pBox->x2;
    param.cropYstr = pBox->y1;
    param.cropYend = pBox->y2;

    param.scaleImgOff = 0;
    param.scaleW = 48;
    param.scaleH = 48;
    int spaceX = (FD_IMG_SCALE_SIZE - param.scaleW);
    param.padX = spaceX / 2;
    param.padY = 0;



    return param;
}


/**
 * @brief Get FD readin img scale param
 * @param start starting row
 * @param height number of rows
 * @return param
 */
ImgScaleParam GetFdScaleParamX(int start, int height)
{
    ImgScaleParam param = {0};
    int scale = ORG_IMG_HEIGHT / FD_IMG_SCALE_SIZE;

    param.isFrame0 = 1;
    param.moveMode = 0;
    param.srcW = ORG_IMG_WIDTH;
    param.srcH = ORG_IMG_HEIGHT;
    param.subVal = 0;
    param.divide = 256;

    param.cropXstr = 0;
    param.cropXend = ORG_IMG_WIDTH;
    param.cropYstr = start * scale;
    int yEnd = param.cropYstr + height * scale;
    param.cropYend = MIN(ORG_IMG_HEIGHT, yEnd);

    param.scaleImgOff = 0;
    param.scaleW = FD_IMG_SCALE_WIDTH;
    param.scaleH = (param.cropYend - param.cropYstr) / scale;
    int spaceX = (FD_IMG_SCALE_SIZE - param.scaleW);
    param.padX = spaceX / 2;
    param.padY = 0;

    //assert(spaceX % 2 == 0);
    //assert(ORG_IMG_HEIGHT % FD_IMG_SCALE_SIZE == 0);
    //assert((param.cropYend - param.cropYstr) % scale == 0);

    return param;
}



/**
 * @brief Get depth value normalize param
 * @param pDepth depth image
 * @param pBox face box
 * @param pLandmark face landmark
 * @param pParam depth norm param
 * @return 0 on success
 */
static int GetDepthNormParam(const Image *pDepth, const Box *pBox,
                             DepthNormParam *pParam)
{
    s16 *pData = pDepth->data.i;
    int width = pDepth->width;

    int x = (pBox->x1 + pBox->x2) / 2;
    int y = (pBox->y1 + pBox->y2) / 2;
    int x1 = MAX(x - DEPTH_DET_BOX_SIZE, 0);
    int y1 = MAX(y - DEPTH_DET_BOX_SIZE, 0);
    int x2 = MIN(x + DEPTH_DET_BOX_SIZE, pDepth->width);
    int y2 = MIN(y + DEPTH_DET_BOX_SIZE, pDepth->height);
    int sum = 0;
    int cnt = 0;

    /* Get mean around nose */
    for (int i = y1; i < y2; ++i) {
        s16 *pI = pData + i*width;
        for (int j = x1; j < x2; ++j) {
            s16 val = pI[j];
            if (val > 0) {
                sum += val;
                cnt++;
            }
        }
    }

    if (cnt == 0) {
        return -1;
    }

    int mean = ROUND((float) sum/cnt);
    int threshL = mean - 100;
    int threshH = mean + 200;
    int min = threshH;
    int max = 0;

    /* Get min/max within face box */

    for (int i = pBox->y1; i < pBox->y2; ++i) {
        s16 *pI = pData + i*width;
        for (int j = pBox->x1; j < pBox->x2; ++j) {
            s16 val = pI[j];
            if (val >= threshL && val <= threshH) {
                min = MIN(val, min);
                max = MAX(val, max);
            }
        }
    }
    if (min >= max) {
        return -1;
    }

    pParam->min = min;
    pParam->max = max;
    pParam->threshL = threshL;
    pParam->threshH = threshH;

    return 0;
}




static float GetDepthNormVal(const Image *pDepth, int x, int y,
                             const DepthNormParam *pParam)
{
    s16 val = pDepth->data.i[pDepth->width*y + x];
    return (val < pParam->threshL || val > pParam->threshH) ?
           0 : (float) (val - pParam->min) / (pParam->max - pParam->min) * 255;
}



/**
 * @brief Check whether landmark regions are large enough
 * @param pLandmark face landmark
 * @return true if OK
 */
static bool IsPoseOK(const Matrix *pLandmark)
{
    float *pF = pLandmark->data.f;
    float minX = pF[0];
    float maxX = pF[0];
    float minY = pF[1];
    float maxY = pF[1];

    for (int i = 0; i < 10; i+=2) {
        minX = pF[i] < minX ? pF[i] : minX;
        maxX = pF[i] > maxX ? pF[i] : maxX;
        minY = pF[i+1] < minY ? pF[i+1] : minY;
        maxY = pF[i+1] > maxY ? pF[i+1] : maxY;
    }

    // Bad pose detection with different threshold.
    if ((ABS(pF[4] - minX) < 4.5) ||
        (ABS(pF[4] - maxX) < 4.5)) {
        fLib_printf("Err: Pose detect fail\n");
        return false;
    }

    if ((ABS(pF[5] - minY) < 4.0) ||
        (ABS(pF[5] - maxY) < 6.5)) {
        fLib_printf("Err: Pose detect fail\n");
        return false;
    }

    return true;

}


/**
 * @brief Use depth info to check whether the face is 3D
 * @param pDepth depth image
 * @param pBox face box
 * @param pLandmark face landmark
 * @return whether face is 3D
 */
#ifdef DYFPx
static bool Is3DFace(const Image *pDepth, const Box *pBox, const Matrix *pLandmark)
{
	int value;
	value = RunCnnLivenessDyFp(pDepth, pBox, pLandmark);
	return value;

}
#else
static bool Is3DFace(const Image *pDepth, const Box *pBox, const Matrix *pLandmark)
{
    float *pF = pLandmark->data.f;
    DepthNormParam param = {0};
    if (0 != GetDepthNormParam(pDepth, pBox, &param)) {
        fLib_printf("ERR:GetDepthNormParam() fail\n");
        return false;
    }
    //normalize value near landmarks
    float nose = GetDepthNormVal(pDepth, pF[4], pF[5], &param);
    float eyeL = GetDepthNormVal(pDepth, pF[0], pF[1], &param);
    float eyeR = GetDepthNormVal(pDepth, pF[2], pF[3], &param);
    float val = (ABS(nose-eyeL) + ABS(nose-eyeR)) / 2;

	float mouthL = GetDepthNormVal(pDepth, pF[6], pF[7], &param);
	float mouthR = GetDepthNormVal(pDepth, pF[8], pF[9], &param);
	float val2 = (ABS(nose-mouthL) + ABS(nose-mouthR)) / 2;

	fLib_printf("3D face dist: %f, %f\n", val, val2);

    return (val > DEPTH_DIFF_THRESH) || (val2 > DEPTH_DIFF_THRESH);
}
#endif

/**
 * @brief Whether face is valid
 * @param pDepth depth image
 * @param pBox face box
 * @param pLandmark face landmarks
 * @return true if valid
 */
bool IsValidFace(const Image *pDepth, const Box *pBox, const Matrix *pLandmark)
{
    /* 
     *  Because the zcu706 FPGA memory is not enough!, we resize the depth data 
     *  from 640x360 to 320x180
     */
    #ifdef ZC706_DEPTH_QUATER
    Box depBox;
    depBox.x1 = pBox->x1 / 2;
    depBox.y1 = pBox->y1 / 2;
    depBox.x2 = pBox->x2 / 2;
    depBox.y2 = pBox->y2 / 2;
    Matrix *depLandMark = MakeMatrix(2, 5, MT_FLOAT);
    for (int i = 0; i < 10; i++) {
        depLandMark->data.f[i] = pLandmark->data.f[i] / 2;
    }    
    return  IsPoseOK(pLandmark) && Is3DFace(pDepth, &depBox, depLandMark);
    #else
    return  IsPoseOK(pLandmark) && Is3DFace(pDepth, pBox, pLandmark);
    #endif
}

/**
 * @brief FdInit(), Face detection initial.
 */
void FdInit(void)
{
    FaceAlignInit();
}

/**
 * @brief FaceDet(), Face detection main function.
 * @param pNir original NIR image
 * @param ppLandmark face landmakr (5x2 matrix)
 * @return whether face detected
 */
bool FaceDet(const Image *pNir, const Image *pDepth, Box *pBox, Matrix **ppLandmark, bool isValidFaceDet)
{
    bool blDet;
    Matrix *pFeature = NULL;
    Matrix *pFeaturePool = NULL;
    RunCnnFdDyFp(pNir, &pFeature, &pFeaturePool);
    DoProfClick("FD: post process");
    blDet = Feature2Rectangle(pBox, pFeature, pFeaturePool);
    fLib_printf("fd_result: %d\n\r", blDet);

    FreeMatrix(pFeature);
    FreeMatrix(pFeaturePool);

#if 0

    if (blDet) {
        RunLandmarkBd(pNir, pBox, ppLandmark);
        /* kai2do build with ENABLE_3D_CHECK */
#if defined(HIMAX_INTEGRATION) || defined(ENABLE_3D_CHECK)
        if (isValidFaceDet) {
            blDet = IsValidFace(pDepth, pBox, *ppLandmark);
            if (!blDet) fLib_printf("ERR: Not valid face !!\n");
        }
#endif
    }
#endif
    return blDet;
}

void feat2rect_nir(Box* outputBox/*out*/, float* feature, float* feature_pooled, float scale_init)
{
    float feature_poll_thresh = 45.;

    //get max(feature_pooled)
    int size = 400; //20*20
    float max_pooled = 0.0; 
    float *pf=feature_pooled;
    while(size--) {
        max_pooled = MAX(max_pooled, *pf);
        pf++;
    }
        
    //init box
    outputBox->x1 = -1;
    outputBox->y1 = -1;
    outputBox->x2 = -1;
    outputBox->y2 = -1;

    if (max_pooled < feature_poll_thresh) return;
    
    // In case of many activated pixels on feature map, skip searching for face edge
    int i;
    for(i = 0; i < 400; i++) {  //20 * 20
        //feature[i] = feature_pooled[i] > feature_poll_thresh ? 1 : 0;
        if( *(feature+i) > feature_poll_thresh) 
            *(feature+i) = 1;
        else
            *(feature+i) = 0;
    }

    int left = 20, right = 0;
    int top = 20, bot = 0;
    int tmp0 = 0, tmp1 = 0;
    int num = 0;
    for (int j = 0; j < 20; j++) {
        for (int i = 0; i < 20; i++) {
            if (*(feature + j * 20 + i) > 0.0) {
                num++;
                left  = MIN(left, i);    
                top   = MIN(top, j);
                right = MAX(right, i);
                bot   = MAX(bot, j);
            } 
            if (*(feature_pooled+j * 20 + i) == max_pooled) {
                tmp0 = i;
                tmp1 = j;
            }
        }
    }

    if (num <= 400 / 3) { //20x20
        left = tmp0;
        top = tmp1;
        right = tmp0;
        bot = tmp1;

        int c_y = (bot + top) << 1;    // div by 2
        int c_x = (right + left) << 1; //div by 2

        //// note: change to C style
        while (left > 0) {
            if (feature[c_y * 20 + left-1] > 0) {
                left--;
                continue;
            }
            if (feature[(c_y-1) * 20 + left-1] > 0 && c_y > 0) {
                c_y--;
                left--;
                continue;
            }
            if (feature[(c_y+1) * 20 + left-1] > 0 && c_y < 20-1) {
                c_y++;
                left--;
                continue;
            }
            break;
        }

        while(right < 20-1) {
            if(feature[c_y * 20 + right+1] > 0) {
                right++;
                continue;
            }
            if(feature[(c_y+1) * 20 + right+1] > 0 && c_y < 20-1) {
                right++; 
                c_y++;
                continue;
            }
            if(feature[(c_y-1) * 20 + right+1] > 0 && c_y > 0) {
                right++;
                c_y--;
                continue;
            }
            break;
        }

        while(top > 0){
            if(feature[(top-1) * 20 + c_x] > 0){
                top--;
                continue;
            }

            if(feature[(top-1) * 20 + c_x-1] > 0 && c_x > 0){
                top--; 
                c_x--;
                continue;
            }

            if(feature[(top-1) * 20 + c_x+1] > 0 && c_x < 20-1){
                top--;
                c_x++;
                continue;
            }
            break;
        }

        while(bot < 20-1){
            if(feature[(bot+1) * 20+ c_x] > 0){
                bot++;
                continue;
            }

            if(feature[(bot+1) * 20+ c_x+1] > 0 && c_x>20-1){
                c_x++;
                bot++;
                continue;
            }

            if(feature[(bot+1) * 20+ c_x-1] > 0 && c_x>0){
                c_x--;
                bot++;
                continue;
            }
            break;
        }
    }
   
    // center of face on feature map;
    float c_y = (float)((bot + top) << 1) + 0.5;
    float c_x = (float)((right + left) << 1)+ 0.5;

    // center of face on input map;
    c_y = c_y * 8;
    c_x = c_x * 8;
    
    // adjust rectangle size
    float length = (float)(bot - top + 1);
    float flength = (float)MAX(length, 3.5);
    float fwidth = (float)flength * 0.8 * (float)8;
    float fheight = (float)flength * (float)8;

    outputBox->x1 = (int) (round((c_x - fwidth / 2.) / scale_init));
    outputBox->y1 = (int) (round((c_y - fheight / 2.) / scale_init));
    outputBox->x2 = (int) (round((c_x + fwidth / 2.) / scale_init)) - outputBox->x1;
    outputBox->y2 = (int) (round((c_y + fheight / 2.) / scale_init)) - outputBox->y1;
    
    return;
}

#if 0
int fd_postprocess(void)
{
    //input: cnn result in nmem format (fixed point)
    float scale = 1.0;  //todo: should get from setup.bin
	  int fraction = 3;    //todo: should get from setup.bin
    s8 *pCnn_output = (s8*)FD_OUT_ADDR;
    
    int i;
    //NMEM format -> seq format
    nmem2seq(pCnn_output,
             FD_OUT_ROW/*row*/, FD_OUT_COL/*col*/, FD_OUT_CH/*ch*/,
             32/*pNMEMCol*/, 8/*nSeq:8 or 16*/);

    //convert to float
    int nTotalElements = FD_OUT_ROW * FD_OUT_COL;
    float *model_out_f = new_float_block_from_s8(pCnn_output, fraction, scale, nTotalElements);

    //debug
    for(i = 0; i < 10; ++i){
        fLib_printf("[dbg] model_out_f[%d]: %f\n\r", i, model_out_f[i]);
    }
   
    //pre_average_pooling
    //todo: should flatten new_flat_block_from_s8()
    for(i = 0; i < nTotalElements; i++) {
        if( model_out_f[i] > 0)
            model_out_f[i] = 1;
        else
            model_out_f[i] = 0;
    }
    //model_out_f --> feature

    //do averagePool   
    float *model_out_f_pooled = (float *)calloc(nTotalElements, sizeof(float));
    averagePool(model_out_f, model_out_f_pooled, 
                FD_OUT_ROW, FD_OUT_COL, FD_OUT_CH, 3/*kernal*/, 1/*stride*/, 1/*padding*/);
     
    //post_average_pooling
    //Get rectangle 
    float scale_init = (float)(FD_OUT_COL)/(float)640;   //MAX(640,480)
    Box fdbox;
    feat2rect_nir(&fdbox, model_out_f, model_out_f_pooled, scale_init);
    if(fdbox.x2 < 0  || fdbox.x2 == 0 ||
       fdbox.y2 < 0  || fdbox.y2 == 0 ) 
        return 1; //NO FACE
    
    //debug
    fLib_printf("[dbg] fd output: %d, %d, %d, %d\n", fdbox.x1, fdbox.y1, fdbox.x2, fdbox.y2);
    
    //todo
    //should set fdbox to shared DDR

    free(model_out_f);
    free(model_out_f_pooled);

    return 1;
}
#endif

