/**
 * @file      fr.h
 * @brief     face recognition
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __FR_H__
#define __FR_H__


#include "base.h"
#include "image.h"
#include "matrix.h"
#include "hwctrl.h"

ImgScaleParam GetNirScaleParam(int start, int height);
Matrix* GetFrFeature(const Image *pWarped);
float FrCalDist(const Matrix *pFeat1, const Matrix *pFeat2);
void NormalizeFeature(Matrix *pMat);
#endif


