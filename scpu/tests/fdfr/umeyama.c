/**
 * @file      umeyama.c
 * @brief     umeyama
 * @version $Id: umeyama.c 386 2018-06-08 03:42:29Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include <stdio.h>
#include <stdbool.h>
#include "matrix.h"
#include "svd.h"

#if DBG_DUMP
#define Log(msg)    puts(msg)
#else
#define Log(msg)
#endif

/**
 * Estimate N-D similarity transformation with or without scaling.
 * @param src (M, N) array: Source coordinates.
 * @param dst (M, N) array: Destination coordinates.
 * @param blEstimateScale whether to estimate scaling factor.
 * @return T : (N + 1, N + 1)
 *         The homogeneous similarity transformation matrix. The matrix contains
 *         NaN values only if the problem is not well-conditioned.
 */
Matrix* umeyama(const Matrix *src, const Matrix *dst, bool blEstimateScale)
{
    MAT_TYPE type = MT_FLOAT;
    int num = src->height;
    int dim = src->width;
    float scale;

    /* Compute mean of src and dst. */
    Matrix *src_mean = MeanMat(src);
    Matrix *dst_mean = MeanMat(dst);

    /* Subtract mean from src and dst. */
    Matrix *src_demean = SubtractVec(src, src_mean);
    Matrix *dst_demean = SubtractVec(dst, dst_mean);

    /* Eq. (38). */
    Matrix *dst_demean_T = TransposeMat(dst_demean);
    Matrix *A = DotProduct(dst_demean_T, src_demean);
    ScaleMat(A, num, 0);

    /* Eq. (39). */
    Matrix *d = OnesMatrix(dim, 1, type);
    if (DetMatrix(A) < 0) {
        d->data.f[dim - 1] = -1;
    }

    Matrix *T = EyeMatrix(dim + 1, type);
    Matrix *U = NULL;
    Matrix *S = NULL;
    Matrix *V = NULL;
    Matrix *tmp1 = NULL;
    Matrix *tmp2 = NULL;
    Matrix *tmp3 = NULL;
    Matrix *tmp4 = NULL;
    int rank = 0;

    SVD(A, &U, &S, &V, &rank);

    /* Eq. (40) and (43). */
    if (rank == 0) {
        Log("[umeyama]: case 1");
        int width = T->width;
        int height = T->height;
        FreeMatrix(T);
        T = NanMatrix(width, height);
        goto FUNC_OUT;
    } else if (rank == dim - 1) {
        if (DetMatrix(U) * DetMatrix(V) > 0) {
            Log("[umeyama]: case 2-1");
            Matrix *tmp = DotProduct(U, V);
            AssignMat(T, tmp, 0, dim, 0, dim);
            FreeMatrix(tmp);
        } else {
            Log("[umeyama]: case 2-2");
            float s = d->data.f[dim - 1];
            d->data.f[dim - 1] = -1;
            Matrix *tmp1 = DiagMatrix(d);
            Matrix *tmp2 = DotProduct(tmp1, V);
            Matrix *tmp3 = DotProduct(U, tmp2);
            AssignMat(T, tmp3, 0, dim, 0, dim);
            d->data.f[dim - 1] = s;
            FreeMatrix(tmp1);
            FreeMatrix(tmp2);
            FreeMatrix(tmp3);
        }
    } else {
        Log("[umeyama]: case 3");
        Matrix *tmp1 = DiagMatrix(d);
        Matrix *tmp2 = DotProduct(tmp1, V);
        Matrix *tmp3 = DotProduct(U, tmp2);
        AssignMat(T, tmp3, 0, dim, 0, dim);
        FreeMatrix(tmp1);
        FreeMatrix(tmp2);
        FreeMatrix(tmp3);
    }

    if (blEstimateScale) {
        /* Eq. (41) and (42). */
        Matrix *tmp = VarMat(src_demean);
        scale = 1.0 / SumMat(tmp) * InnerProduct(S, d);
        FreeMatrix(tmp);
    } else {
        scale = 1.0;
    }

    tmp1 = SubMat(T, 0, dim, 0, dim);
    tmp2 = TransposeMat(src_mean);
    tmp3 = DotProduct(tmp1, tmp2);
    ScaleMat(tmp3, scale, 1);
    tmp4 = SubtractVec(dst_mean, tmp3);
    AssignMat(T, tmp4, 0, dim, dim, dim+1);
    ScaleSubMat(T, scale, 1, 0, dim, 0, dim);

    FreeMatrix(tmp1);
    FreeMatrix(tmp2);
    FreeMatrix(tmp3);
    FreeMatrix(tmp4);

FUNC_OUT:

    FreeMatrix(src_mean);
    FreeMatrix(src_demean);
    FreeMatrix(dst_mean);
    FreeMatrix(dst_demean);
    FreeMatrix(dst_demean_T);
    FreeMatrix(d);
    FreeMatrix(A);
    FreeMatrix(U);
    FreeMatrix(S);
    FreeMatrix(V);

    return T;
}
