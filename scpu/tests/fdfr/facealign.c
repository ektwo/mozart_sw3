/**
 * @file      facealign.c
 * @brief     Face alignment
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "image.h"
#include "imagecv.h"
#include "base.h"
#include "sys.h"
#include "reg.h"
#include "umeyama.h"
#include "landmark.h"
#include "matrix.h"
#include "facealign.h"

#define FACE_ALIGN_MARGIN    44
#define FD_VERIGY_SCRIPT    "utils/fd_verify.py"

static Matrix *_sSrcTransformMat = NULL;

/* ############################
 * ##    Static Functions    ##
 * ############################ */

#if DBG_DUMP
static void DumpFaceImg(const Image *pImg, const Image *pWarped,
                        const Box *pBox, const Matrix *pLandmark)
{
    mkdir("output", 0755);
    WriteImage(pImg, NULL, "output/original.png");
    WriteImage(pWarped, NULL, "output/preprocessed.png");

    /* FD output image (face box & landmark) */
    char cmd[512] = {0};
    float *pVal = pLandmark->data.f;

    snprintf(cmd, sizeof(cmd), "%s \"(%d,%d,%d,%d)\" \"(%d,%d,%d,%d,%d,%d,%d,%d,%d,%d)\"",
             FD_VERIGY_SCRIPT, pBox->x1, pBox->y1, pBox->x2, pBox->y2,
             (int) pVal[0], (int) pVal[1], (int) pVal[2], (int) pVal[3], (int) pVal[4],
             (int) pVal[5], (int) pVal[6], (int) pVal[7], (int) pVal[8], (int) pVal[9]);
    /* printf("=> cmd: %s\n", cmd); */

    if (0 != system(cmd)) {
        puts("Failed to dump face image result.");
    }
}
#endif

static Matrix* SimilarityTransform(const Matrix *pSrc, const Matrix *pDst)
{
    /* https://github.com/scikit-image/scikit-image/blob/master/skimage/transform/_geometric.py#L1041 */
    Matrix *pTmp = umeyama(pSrc, pDst, true);
    Matrix *pTform = SubMat(pTmp, 0, 2, 0, pTmp->width);
    FreeMatrix(pTmp);

    return pTform;
}

/**
 * @brief Perform face alignment:
 *            1. find face landmarks
 *            2. do warp affine based on face landmarks
 * @param pImg original image
 * @param pBox face bounding box
 * @return face-aligned image
 */
Image* FaceAlign(const Image *pImg, const Box *pBox, Matrix *pLandmark)
{
    #ifdef DYFP
    FaceAlignInit();
    int width = FR_IMG_WIDTH;
    int height = FR_IMG_HEIGHT;

    #else
    int width = FR_IMG_WIDTH;
    int height = FR_IMG_HEIGHT;

    assert(pImg->width >= width && pImg->height >= height);
    #endif

#ifdef SYS_PC
    Image *pRet = MakeImage(width, height);
#else
    #ifdef DYFP
    Image *pRet = MakeImageWithData(width, height, 0);
    #else
    Image *pRet = MakeImageWithData(width, height, (void*) ALIGNED_IMG_ADDR);
    #endif
#endif

    Matrix *pM = SimilarityTransform(pLandmark, _sSrcTransformMat);
    if (pM) {
        cvWarpAffine(pRet, pImg, pM);
        FreeMatrix(pM);
    } else {
        Box det;
        Box bb;

        det.x1 = pBox->x1;
        det.y1 = pBox->y1;
        det.x2 = pBox->x2;
        det.y2 = pBox->y2;

        bb.x1 = MAX(det.x1 - FACE_ALIGN_MARGIN/2, 0);
        bb.y1 = MAX(det.y1 - FACE_ALIGN_MARGIN/2, 0);
        bb.x2 = MAX(det.x2 + FACE_ALIGN_MARGIN/2, pImg->width);
        bb.y2 = MAX(det.y2 + FACE_ALIGN_MARGIN/2, pImg->height);

        Image *pTmp = SubImage(pImg, bb.y1, bb.y2, bb.x1, bb.x2);
        cvResize(pRet, pTmp);
        FreeImage(pTmp);
    }

#if DBG_DUMP
    DumpFaceImg(pImg, pRet, pBox, pLandmark);
#endif

    return pRet;
}

/* ############################
 * ##    Public Functions    ##
 * ############################ */

void FaceAlignInit(void)
{
    int i;
    Matrix *pMat = MakeMatrix(2, 5, MT_FLOAT);
    float *pVal = pMat->data.f;
    float vals[] = { 30.2946 + 8, 51.6963,
                     65.5318 + 8, 51.5014,
                     48.0252 + 8, 71.7366,
                     33.5493 + 8, 92.3655,
                     62.7299 + 8, 92.2041 };

    for (i = 0; i < 10; ++i) {
        pVal[i] = vals[i];
    }

    _sSrcTransformMat = pMat;
}
