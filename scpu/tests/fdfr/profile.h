/**
 * @file      profile.h
 * @brief     Profiling Utils
 * @version $Id: profile.h 566 2018-08-28 18:30:50Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __PROFILE_H__
#define __PROFILE_H__


#ifdef SYS_PC

#include <sys/time.h>
#define DoProfStart()      struct timeval tm1; gettimeofday(&tm1, NULL);
#define DoProfClick(tag)   do {                                              \
    struct timeval tm2;                                                      \
    gettimeofday(&tm2, NULL);                                                \
    double diffMs = ((double) tm2.tv_sec*1000 + (double) tm2.tv_usec/1000) - \
                    ((double) tm1.tv_sec*1000 + (double) tm1.tv_usec/1000);  \
    printf("=> [%s] elapses %.6f (ms)\n", tag, diffMs);                      \
    tm1 = tm2;                                                               \
    } while(0)

#elif defined(SYS_XILINX)

#include <stdio.h>
#include "xtime_l.h"
#define DoProfStart()      XTime profTm1; XTime_GetTime(&profTm1);
#define DoProfClick(tag)   do {                                   \
        XTime profTm2;                                            \
        XTime_GetTime(&profTm2);                                  \
        printf("=> [%s] elapses %.6f (ms)\n", tag,                \
               (profTm2-profTm1)*1000.0/COUNTS_PER_SECOND);       \
    } while(0)

#elif defined(SYS_ARM)
/* ARM CM4 Env */
#include "io.h"
#include "DrvPWMTMR010.h"
#include "fdebug.h"
extern unsigned int t1_tick;
extern unsigned int t2_tick;
#define DoProfStart()      t1_tick = SYS_TMR_GET_TICK();
#define DoProfClick(tag)   do {                                              \
        t2_tick = SYS_TMR_GET_TICK();                                         \
        fLib_printf("=> [%s] elapse %d (ms)\n", tag, (t2_tick-t1_tick));     \
        } while(0)       
#endif

#if defined(SYS_PC) || (defined(SYS_XILINX) && defined(CNN_PROFILING))
#define ProfStart()        DoProfStart()
#define ProfClick(tag)     DoProfClick(tag)

#else
#define ProfStart()
#define ProfClick(tag)
#endif

#endif
