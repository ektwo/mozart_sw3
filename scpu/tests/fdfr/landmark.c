/**
 * @file      landmark.c
 * @brief     find face landmark
 * @version $Id: landmark.c 555 2018-08-14 03:13:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include <assert.h>
#include <stdio.h>
#include "landmark.h"
#include "reg.h"
#include "image.h"
#include "imagecv.h"
#include "fd.h"
#include "cnn.h"
#include "matrix.h"
#include "base.h"
#include "sys.h"
#include "hwctrl.h"
#include "box.h"

/* ############################
 * ##    Static Functions    ##
 * ############################ */
#ifdef DYFP


/* ############################
 * ##    Static Functions    ##
 * ############################ */
float find_min(const Matrix *pMat){
    int size = pMat->width * pMat->height;
    float *pF = pMat->data.f;
    float min_val = pF[0];

    while (size--) {
        min_val = MIN(min_val, *pF);
        pF++;
    }
    return min_val;
}

void ScaleLandmarkResult(Matrix *pMat, const Box *pBox)//pBox comes from FD rectangle
{
    float scaleW = 0, scaleH = 0, offset_x = 0, offset_y = 9;
    if ((pBox->x2 - pBox->x1)==(pBox->y2 - pBox->y1)){ //FD cropped width == FD cropped height
        scaleW = (float) (pBox->x2 - pBox->x1) / LANDMARK_IMG_SIZE;
        scaleH = (float) (pBox->y2 - pBox->y1) / LANDMARK_IMG_SIZE;
    }
    else{
        scaleW = (float)MAX(pBox->x2 - pBox->x1, pBox->y2 - pBox->y1)/LANDMARK_IMG_SIZE;
        scaleH = (float)MAX(pBox->x2 - pBox->x1, pBox->y2 - pBox->y1)/LANDMARK_IMG_SIZE;
    }
    float *pVal = pMat->data.f;
    int i;


    //Need to handle landmark value <0
    int cnt=0;
    if (find_min(pMat)<0){
        for (int i=0; i<10; i++){
            *pVal = *pVal + (float) LANDMARK_IMG_SIZE/2;
            pVal++;
            cnt++;
        }
    }
    if (cnt==10) pVal-=10;
    //*****************
    /*
    if ((pBox->x2 - pBox->x1)==(pBox->y2 - pBox->y1)){ //FD cropped width == FD cropped height
        offset_x = 0;
        offset_y = 0;
    }
    else{//resize method: left and right add pads to get square size
        offset_x = (pBox->y2 - pBox->y1)>= (pBox->x2 - pBox->x1) ? (float) ((pBox->y2- pBox->y1)-(pBox->x2 - pBox->x1))/2.0 : 0;
        offset_y = (pBox->y2 - pBox->y1)>= (pBox->x2 - pBox->x1) ? 0: (float) ((pBox->x2- pBox->x1)-(pBox->y2 - pBox->y1))/2.0;
    }*/

    for (i = 0; i < 10; i+=2) {
        *pVal = ROUND(*pVal*scaleW  + pBox->x1);
        pVal++;
        *pVal = ROUND(*pVal*scaleH  + pBox->y1);
        pVal++;
    }

    MatReshape(pMat, 2, 5);
}




#else
static void ScaleLandmarkResult(Matrix *pMat, const Box *pBox)
{
    float scaleW = (float) (pBox->x2 - pBox->x1) / LANDMARK_IMG_SIZE;
    float scaleH = (float) (pBox->y2 - pBox->y1) / LANDMARK_IMG_SIZE;
    float *pVal = pMat->data.f;
    int i;

    assert(pMat->width*pMat->height == 10 || !"result should to be 1x10 mat");

    for (i = 0; i < 10; i+=2) {
        *pVal = ROUND(*pVal*scaleW + pBox->x1);
        pVal++;
        *pVal = ROUND(*pVal*scaleH + pBox->y1);
        pVal++;
    }

    MatReshape(pMat, 2, 5);
}

#endif
/* ############################
 * ##    Public Functions    ##
 * ############################ */

ImgScaleParam GetLandmarkScaleParam(const Box *pBox)
{
    ImgScaleParam param = {0};

    param.isFrame0 = 1;
    param.moveMode = 0;
    param.srcW = ORG_IMG_WIDTH;
    param.srcH = ORG_IMG_HEIGHT;
    param.subVal = 128;
    param.divide = 256;
    param.cropXstr = pBox->x1;
    param.cropXend = pBox->x2;
    param.cropYstr = pBox->y1;
    param.cropYend = pBox->y2;
    param.scaleW = LANDMARK_IMG_SIZE;
    param.scaleH = LANDMARK_IMG_SIZE;
    param.scaleImgOff = 0;
    param.padX = 0;
    param.padY = 0;

    return param;
}

/**
 * @brief Given face region, find landmark and refined face region
 * @param pImg original image
 * @param pBox refined face bounding box
 * @param ppLandmark face landmark
 */
void RunLandmarkBd(const Image *pImg, const Box *pBox, Matrix **ppLandmark)
{
    Matrix *pRes = NULL;
    *ppLandmark = NULL;
#ifdef DYFP
    if (0 == RunCnnLandmarkDyFp(pImg, pBox, &pRes)) {
#else
    if (0 == RunCnnLandmark(pImg, pBox, &pRes)) {
#endif
        //ScaleLandmarkResult(pRes, pBox);
        //*ppLandmark = pRes;
    }

    //printf("Landmark result: ");
    //PrintMatrix(*ppLandmark);
}


