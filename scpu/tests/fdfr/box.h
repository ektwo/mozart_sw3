#ifndef BOX_H
#define BOX_H
typedef struct _Box {
    int x1;
    int y1;
    int x2;
    int y2;
} Box;

#endif
