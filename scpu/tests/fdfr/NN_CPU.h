/**
 * @file      math.h
 * @brief     math functions
 * @version $Id: math.h 498 2018-07-16 05:43:35Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __MATH_H__
#define __MATH_H__
#include "average_pool.h"

void softmax(float* pScores, int pLen);
//void averagePool(float *dst, int row, int col, int ch, int kernel, int stride, int need_padding);

#endif
