#include "average_pool.h"
#include "stdlib.h"
#include "string.h"

int get_idx(int row, int col, int ch, int total_col, int total_ch){
	return row*total_col*total_ch + col*total_ch + ch;
}

float average(float *src, int s_i, int s_j, int s_k, int kernel, int col, int ch){
	int i, j;
	float total = 0;
	for(i = 0; i < kernel; i++){
		for(j = 0; j < kernel; j++){
			total += src[get_idx(s_i+i, s_j+j, s_k, col, ch)];
		}
	}
	return total / (kernel * kernel);
}

void averagePool(float *src, float *dst, int row, int col, int ch, int kernel, int stride, int need_padding)
{
	if(need_padding){
		int pad_h, pad_w;
		if(row%stride==0){
			pad_h = max(kernel-stride, 0);
		}else{
			pad_w = max(kernel-(row%stride), 0);
		}
		if(col%stride==0){
			pad_w = max(kernel-stride, 0);
		}else{
			pad_h = max(kernel-(col%stride), 0);
		}
		float* buffer = (float*)malloc((row+pad_h) * (col+pad_w) * ch * sizeof(float));
		memset(buffer, 0, (row+pad_h)*(col+pad_w)*ch*sizeof(float));
		for(int i = 0; i < row; i++) {
			for(int j = 0; j < col; j++) {
				for(int k = 0; k < ch; k++) {
					buffer[get_idx(pad_h/2+i, pad_w/2+j, k, col+pad_w, ch)] = src[get_idx(i, j, k, col, ch)];
				}
			}
		}
		src = buffer;
		row = row + pad_h;
		col = col + pad_w;
	}
	int i, j, k, ii, jj;
	// Calculate result column
	int res_col = 0;
	for(i = 0; i < col - kernel + 1; i += stride) res_col++;

	// Do average pooling
	for (k = 0; k < ch; k++) {
		for (i = 0, ii = 0; i < row - kernel + 1; i += stride, ii++) {
			for (j = 0, jj = 0; j < col - kernel + 1; j += stride, jj++) {
				dst[get_idx(ii, jj, k, res_col, ch)] = (float)average(src, i, j, k, kernel, col, ch);
			}
		}
	}
	if (need_padding == 1)
		free(src);
}
