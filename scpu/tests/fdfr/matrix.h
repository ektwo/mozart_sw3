/**
 * @file      matrix.h
 * @brief     2D matrix related functions
 * @version $Id: matrix.h 555 2018-08-14 03:13:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __MATRIX_H__
#define __MATRIX_H__


#include "base.h"
#include "sys.h"
#include "matrix_macro.h"

#define MATRIX_ELEMENT_ADD         0
#define MATRIX_ELEMENT_MINUS       1
#define MATRIX_ELEMENT_MULTIPLY    2

typedef enum {
    MT_UNKNOWN,
    MT_UCHAR,
    MT_INT,
    MT_FLOAT
} MAT_TYPE;

typedef union {
    u8 *c;
    s16 *i;
    float *f;
} MatData;

typedef struct {
    int height;
    int width;
    int needFreeData;
    MAT_TYPE type;
    MatData data;
} Matrix;

Matrix* MakeMatrix(int width, int height, MAT_TYPE type);
Matrix* MakeMatrixWithData(int width, int height, MAT_TYPE type, void *pData);
void FreeMatrix(Matrix *pMat);
void FreeMatData(Matrix *pMat);
Matrix* OnesMatrix(int width, int height, MAT_TYPE type);
Matrix* EyeMatrix(int n, MAT_TYPE type);
Matrix* NanMatrix(int width, int height);
Matrix* DiagMatrix(const Matrix *pVec);
Matrix* CopyMatrix(const Matrix *pSrc, MAT_TYPE type);
Matrix* MatAsType(const Matrix *pSrc, MAT_TYPE type);
void AssignMat(Matrix *pDst, const Matrix *pSrc, int hStr, int hEnd, int wStr, int wEnd);

void PrintMatrix(const Matrix* pMat);
void PrintMatrixThenFree(Matrix *pMat);
Matrix* ReadMatrix(const char *filename, MAT_TYPE type);
void WriteMat(const char *filename, const Matrix *pSrc);

int IsVector(const Matrix *pMat);
float MaxVal(const Matrix *pMat);
float SumMat(const Matrix *pMat);
Matrix* SubMat(const Matrix *pSrc, int hStr, int hEnd, int wStr, int wEnd);
void SubMatAssign(Matrix *pDst, const Matrix *pSrc, int hStr, int hEnd, int wStr, int wEnd);
void MatReshape(Matrix *pMat, int width, int height);
Matrix* MatGetCol(const Matrix *pSrc, int col);
Matrix* MatGetRow(const Matrix *pSrc, int row);
Matrix* TransposeMat(const Matrix* pSrc);
Matrix* MeanMat(const Matrix *pSrc);
Matrix* VarMat(const Matrix *pSrc);
Matrix* SubtractVec(const Matrix *pSrc, const Matrix *pVec);
void ScaleMat(Matrix* pMat, float value, bool blScaleUp);
void ScaleSubMat(Matrix *pMat, float value, bool blScaleUp, int hStr, int hEnd, int wStr, int wEnd);
float InnerProduct(const Matrix* pVec1, const Matrix* pVec2);
Matrix* DotProduct(const Matrix* pMat1, const Matrix* pMat2);
float DetMatrix(const Matrix *pMat);

Matrix* TruncMatDecimalVal(const Matrix *pSrc);
Matrix* MergeMatrixCol(const Matrix* m1, const Matrix* m2);
Matrix* MatElementOp(const Matrix* m1, const Matrix* m2,int op);

#endif
