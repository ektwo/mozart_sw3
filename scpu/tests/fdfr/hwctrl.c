#if __MOZART__
#else

/**
 * @file      hwctrl.c
 * @brief     hw ctrl functions
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef SYS_PC

#include <assert.h>
#include "hwctrl.h"
#include "reg.h"
#ifndef DYFP
#include "profile.h"
#endif
#include "image.h"

#define SNRSD_SCALE_RATIO_H        CAL_SCALE_RATIO(SNRSD_IMG_WIDTH - 2*SNRSD_IMG_CROP_EDGE, ORG_IMG_WIDTH)
#define SNRSD_SCALE_RATIO_V        CAL_SCALE_RATIO(SNRSD_IMG_HEIGHT - 2*SNRSD_IMG_CROP_EDGE, ORG_IMG_HEIGHT)
#define DPHSD_SCALE_RATIO_H        CAL_SCALE_RATIO(DPHSD_IMG_WIDTH - 2*DPHSD_IMG_CROP_EDGE, ORG_IMG_WIDTH)
#define DPHSD_SCALE_RATIO_V        CAL_SCALE_RATIO(DPHSD_IMG_HEIGHT - 2*DPHSD_IMG_CROP_EDGE, ORG_IMG_HEIGHT)

/* TODO: need to measure this reg wait timeout (~3 sec) */
#define REG_WAIT_TIMEOUT_CNT       0xFFFFFF

/* ############################
 * ##    Publix Functions    ##
 * ############################ */

/**
 * @brief Wait until bit flag is on/off
 * @param addr register address
 * @param bitPos bit position of flag
 * @param flagOn 1/0: wait until flag on/off
 */
int RegWaitFlag(u32 addr, int bitOff, int flagOn)
{
    u32 mask = 0x01 << bitOff;
    u32 cnt = 0;

    for (;;) {
        u32 val = ReadWord(addr) & mask;
        if ((val && flagOn) || (!val && !flagOn)) {
            return 0;
        } else if (cnt++ == REG_WAIT_TIMEOUT_CNT) {
            printf("RegWaitFlag timeout, addr: [%p], bitOff: [%d], flagOn: [%d]",
                   (void*) addr, bitOff, flagOn);
            return -1;
        }
    }
}

/**
 * @brief Reset register bit
 * @param addr address of register
 * @param bitOff bit offset
 */
void RegReset(addr_t addr, int bitOff)
{
    u32 reg = ReadWord(addr);
    RegBitOn(reg, SNRSD_CTRL_OFF_RESET);
    WriteWord(addr, reg);
}

/**
 * @brief Wait until CNN boot complete
 */
void WaitCnnBootDone(void)
{
    for (;;) {
        if (0 == RegWaitFlag(CNN_CTRL, CNN_CTRL_OFF_BOOT_STATUS, REG_FLAG_OFF)) {
            break;
        }
    }
}

/**
 * @brief Init params of gamma correction
 */
void InitGammaCorrection(void)
{
    u32 reg;

    reg = ReadWord(GAMMA_LUT00_REG);
    RegSetBits(reg, 0, MASK_BIT_8, GAMMA_LUT_OFF_0);
    RegSetBits(reg, 84, MASK_BIT_8, GAMMA_LUT_OFF_1);
    RegSetBits(reg, 111, MASK_BIT_8, GAMMA_LUT_OFF_2);
    RegSetBits(reg, 131, MASK_BIT_8, GAMMA_LUT_OFF_3);
    WriteWord(GAMMA_LUT00_REG, reg);

    reg = ReadWord(GAMMA_LUT04_REG);
    RegSetBits(reg, 147, MASK_BIT_8, GAMMA_LUT_OFF_0);
    RegSetBits(reg, 160, MASK_BIT_8, GAMMA_LUT_OFF_1);
    RegSetBits(reg, 172, MASK_BIT_8, GAMMA_LUT_OFF_2);
    RegSetBits(reg, 183, MASK_BIT_8, GAMMA_LUT_OFF_3);
    WriteWord(GAMMA_LUT04_REG, reg);

    reg = ReadWord(GAMMA_LUT08_REG);
    RegSetBits(reg, 193, MASK_BIT_8, GAMMA_LUT_OFF_0);
    RegSetBits(reg, 203, MASK_BIT_8, GAMMA_LUT_OFF_1);
    RegSetBits(reg, 211, MASK_BIT_8, GAMMA_LUT_OFF_2);
    RegSetBits(reg, 220, MASK_BIT_8, GAMMA_LUT_OFF_3);
    WriteWord(GAMMA_LUT08_REG, reg);

    reg = ReadWord(GAMMA_LUT0C_REG);
    RegSetBits(reg, 227, MASK_BIT_8, GAMMA_LUT_OFF_0);
    RegSetBits(reg, 235, MASK_BIT_8, GAMMA_LUT_OFF_1);
    RegSetBits(reg, 242, MASK_BIT_8, GAMMA_LUT_OFF_2);
    RegSetBits(reg, 249, MASK_BIT_8, GAMMA_LUT_OFF_3);
    WriteWord(GAMMA_LUT0C_REG, reg);

    reg = ReadWord(GAMMA_LUT10_REG);
    RegSetBits(reg, 255, MASK_BIT_9, GAMMA_LUT_OFF_0);
    WriteWord(GAMMA_LUT10_REG, reg);

    reg = ReadWord(GAMMA_BLKLVL_REG);
    RegSetBits(reg, 0, MASK_BIT_10, GAMMA_BLKLVL_OFF);
    WriteWord(GAMMA_BLKLVL_REG, reg);
}

/**
 * @brief Start fetching NIR image
 */
void StartFetchNirImg()
{
#ifndef HIMAX_INTEGRATION
    return;
#endif

    u32 reg;

    /* Crop */
    reg = ReadWord(SNRSD_CROP_X);
    RegSetBits(reg, SNRSD_IMG_CROP_EDGE, MASK_BIT_11, SNRSD_CROP_OFF_START);
    RegSetBits(reg, SNRSD_IMG_WIDTH-SNRSD_IMG_CROP_EDGE-1, MASK_BIT_11, SNRSD_CROP_OFF_END);
    WriteWord(SNRSD_CROP_X, reg);

    reg = ReadWord(SNRSD_CROP_Y);
    RegSetBits(reg, SNRSD_IMG_CROP_EDGE, MASK_BIT_11, SNRSD_CROP_OFF_START);
    RegSetBits(reg, SNRSD_IMG_HEIGHT-SNRSD_IMG_CROP_EDGE-1, MASK_BIT_11, SNRSD_CROP_OFF_END);
    WriteWord(SNRSD_CROP_Y, reg);

    /* Scale */
    reg = ReadWord(SNRSD_IMG_S);
    RegSetBits(reg, SNRSD_IMG_WIDTH, MASK_BIT_11, SNRSD_IMG_OFF_WIDTH);
    RegSetBits(reg, SNRSD_IMG_HEIGHT, MASK_BIT_11, SNRSD_IMG_OFF_HEIGHT);
    WriteWord(SNRSD_IMG_S, reg);

    reg = ReadWord(SNRSD_DST_S);
    RegSetBits(reg, ORG_IMG_WIDTH, MASK_BIT_10, SNRSD_DST_OFF_WIDTH);
    RegSetBits(reg, ORG_IMG_HEIGHT, MASK_BIT_10, SNRSD_DST_OFF_HEIGHT);
    WriteWord(SNRSD_DST_S, reg);

    reg = ReadWord(SNRSD_RATIO);
    RegSetBits(reg, SNRSD_SCALE_RATIO_H, MASK_BIT_16, SNRSD_RATIO_OFF_H);
    RegSetBits(reg, SNRSD_SCALE_RATIO_V, MASK_BIT_16, SNRSD_RATIO_OFF_V);
    WriteWord(SNRSD_RATIO, reg);

    /* Start */
    reg = ReadWord(SNRSD_CTRL);
    RegBitOn(reg, SNRSD_CTRL_OFF_EN);
    WriteWord(SNRSD_CTRL, reg);
}

/**
 * @brief Start fetching depth image
 */
void StartFetchDepthImg()
{
#ifndef HIMAX_INTEGRATION
    return;
#endif

    u32 reg;

    /* Crop */
    reg = ReadWord(DPHSD_CROP_X);
    RegSetBits(reg, DPHSD_IMG_CROP_EDGE, MASK_BIT_11, DPHSD_CROP_OFF_START);
    RegSetBits(reg, DPHSD_IMG_WIDTH-DPHSD_IMG_CROP_EDGE-1, MASK_BIT_11, DPHSD_CROP_OFF_END);
    WriteWord(DPHSD_CROP_X, reg);

    reg = ReadWord(DPHSD_CROP_Y);
    RegSetBits(reg, DPHSD_IMG_CROP_EDGE, MASK_BIT_11, DPHSD_CROP_OFF_START);
    RegSetBits(reg, DPHSD_IMG_HEIGHT-DPHSD_IMG_CROP_EDGE-1, MASK_BIT_11, DPHSD_CROP_OFF_END);
    WriteWord(DPHSD_CROP_Y, reg);

    /* Scale */
    reg = ReadWord(DPHSD_IMG_S);
    RegSetBits(reg, DPHSD_IMG_WIDTH, MASK_BIT_11, DPHSD_IMG_OFF_WIDTH);
    RegSetBits(reg, DPHSD_IMG_HEIGHT, MASK_BIT_11, DPHSD_IMG_OFF_HEIGHT);
    WriteWord(DPHSD_IMG_S, reg);

    reg = ReadWord(DPHSD_DST_S);
    RegSetBits(reg, ORG_IMG_WIDTH, MASK_BIT_10, DPHSD_DST_OFF_WIDTH);
    RegSetBits(reg, ORG_IMG_HEIGHT, MASK_BIT_10, DPHSD_DST_OFF_HEIGHT);
    WriteWord(DPHSD_DST_S, reg);

    reg = ReadWord(DPHSD_RATIO);
    RegSetBits(reg, DPHSD_SCALE_RATIO_H, MASK_BIT_16, DPHSD_RATIO_OFF_H);
    RegSetBits(reg, DPHSD_SCALE_RATIO_V, MASK_BIT_16, DPHSD_RATIO_OFF_V);
    WriteWord(DPHSD_RATIO, reg);

    /* Start */
    reg = ReadWord(DPHSD_CTRL);
    RegBitOn(reg, DPHSD_CTRL_OFF_EN);
    WriteWord(DPHSD_CTRL, reg);
}

/**
 * @brief Fetch NIR image
 */
Image* GetNirImg(void)
{
#ifdef HIMAX_INTEGRATION
    if (0 != RegWaitFlag(CNN_STATUS, CNN_STATUS_OFF_SNRSD_FINISH, REG_FLAG_ON)) {
        RegReset(SNRSD_CTRL, SNRSD_CTRL_OFF_RESET);
    }
#endif

    return MakeImageWithData(ORG_IMG_WIDTH, ORG_IMG_HEIGHT, (void*) FRAME_0_ADDR);
}

/**
 * @brief fetch depth image
 */
Image* GetDepthImg(void)
{
#ifdef HIMAX_INTEGRATION
    if (0 != RegWaitFlag(CNN_STATUS, CNN_STATUS_OFF_DPHSD_FINISH, REG_FLAG_ON)) {
        RegReset(DPHSD_CTRL, DPHSD_CTRL_OFF_RESET);
    }
#endif

    return MakeMatrixWithData(ORG_IMG_WIDTH, ORG_IMG_HEIGHT, MT_INT, (void*) FRAME_1_ADDR);
}

/**
 * @brief Adder(), HW DSP Adder function.
 *      y = ((src + raddr) * scale) + bias
 * @param pSrcData source data
 * @param pRaddrData raddr data
 * @param len data length
 * @param bias bias data, 32bits (16bits integer, 16bits fraction)
 * @param scale scale data 16bits (8bits integer, 8bits fraction)
 * @param leaky leaky value 16bits (8bits integer, 8bits fraction)
 * @param raddrAddr raddr address
 * @param dstAddr destination address for adder copy.
 */
void Adder(u16 *pSrcData, u16 *pRaddrData, int len, u32 bias,
           u16 scale, u16 leaky, u32 raddrAddr, u32 dstAddr)
{
    u32 value;
    /* Copy test data to raddr */
    memcpy((void*) raddrAddr, (void*) pRaddrData, len * 2);
    WriteWord(ADDER_PARAM0, bias);
    WriteWord(ADDER_PARAM1, (leaky << 16) + scale);
    WriteWord(ADDER_PARAM2, raddrAddr / 4);
    AdderCtrl(1);
    for (int i = 0; i < len/2; i++) {
        value = (pSrcData[2 * i + 1] << 16) + pSrcData[2 * i];
        WriteWord(dstAddr + i * 4,  value);
    }
    AdderCtrl(0);
}

/**
 * @brief Control add layer behavior
 * @param blOn enable or disable add layer
 */
void AdderCtrl(bool blOn)
{
    u32 reg = ReadWord(ADDER_CTRL);

    if (blOn) {
        RegBitOn(reg, ADDER_OFF_ENABLE);
    } else {
        RegBitOff(reg, ADDER_OFF_ENABLE);
    }
    WriteWord(ADDER_CTRL, reg);
}

/**
 * @brief Reshape SRAM data into 6x6
 * @param width data width
 * @param height data height
 * @param channel data channel (equals to valid data channel)
 * @param validW valid data width
 * @param validH valid data height
 * @param direction 0: AB->CD, 1: CD->AB
 */
void Reshape(int width, int height, int channel, int validW, int validH, int direction)
{
    u32 reg = ReadWord(RESHAPE_REG);

    RegSetBits(reg, width - 1, MASK_BIT_3, RESHAPE_OFF_WIDTH);
    RegSetBits(reg, height - 1, MASK_BIT_3, RESHAPE_OFF_HEIGHT);
    RegSetBits(reg, channel - 1, MASK_BIT_10, RESHAPE_OFF_CHANNEL);
    RegSetBits(reg, validW - 1, MASK_BIT_3, RESHAPE_OFF_VWIDTH);
    RegSetBits(reg, validH - 1, MASK_BIT_3, RESHAPE_OFF_VHEIGHT);

    if (direction == 0) {
        RegBitOff(reg, RESHAPE_OFF_DIR);
    } else {
        RegBitOn(reg, RESHAPE_OFF_DIR);
    }

    RegBitOn(reg, RESHAPE_OFF_RUN);
    WriteWord(RESHAPE_REG, reg);
    RegWaitFlag(RESHAPE_REG, RESHAPE_OFF_RUN, REG_FLAG_OFF);
}

/**
 * @brief Start running CNN
 * @param cmdOffset cmd addr offset
 * @param blResetExtWeightPtr reset external weight addr pointer
 */
void StartCnn(int cmdOffset, bool blResetExtWeightPtr)
{
    /* TODO: use interrupt? */
    u32 reg = ReadWord(CNN_CTRL1);
    RegSetBits(reg, cmdOffset, MASK_BIT_13, CNN_CTRL1_OFF_CMD_OFFSET);
    WriteWord(CNN_CTRL1, reg);

    reg = ReadWord(CNN_CTRL);
    RegBitOn(reg, CNN_CTRL_OFF_ENABLE);
    RegBitOn(reg, CNN_CTRL_OFF_WEIGHT_TYPE);

    if (blResetExtWeightPtr) {
        RegBitOn(reg, CNN_CTRL_OFF_EXT_WEIGHT_RESET);
    } else {
        RegBitOff(reg, CNN_CTRL_OFF_EXT_WEIGHT_RESET);
    }

    WriteWord(CNN_CTRL, reg);
}

/**
 * @brief Start FD CNN
 * @param cmdOffset command count offset (1 command has 2 bytes)
 * @param weightOffset weight word count offset (1 word has 4 bytes)
 */
void StartCnnFd(int cmdOffset, int weightOffset)
{
    u32 reg = ReadWord(CNN_CTRL1);
    RegSetBits(reg, cmdOffset, MASK_BIT_13, CNN_CTRL1_OFF_CMD_OFFSET);
    WriteWord(CNN_CTRL1, reg);

    reg = ReadWord(CNN_CTRL);
    RegBitOn(reg, CNN_CTRL_OFF_ENABLE);
    RegBitOff(reg, CNN_CTRL_OFF_WEIGHT_TYPE);
    RegBitOff(reg, CNN_CTRL_OFF_EXT_WEIGHT_RESET);

    RegSetBits(reg, weightOffset, MASK_BIT_17, CNN_CTRL_OFF_FD_WEIGHT_OFF);
    WriteWord(CNN_CTRL, reg);
}

/**
 * @brief Wait until CNN complete
 */
void WaitCnn(void)
{
    RegWaitFlag(CNN_STATUS, CNN_STATUS_OFF_CNN_FINISH, REG_FLAG_ON);
}

#endif


#endif // end of, #ifdef __MOZART__
