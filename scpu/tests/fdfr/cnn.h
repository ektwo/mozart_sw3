/**
 * @file      cnn.h
 * @brief     Cnn functions
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __CNN_H__
#define __CNN_H__

#include "matrix.h"
#include "fd.h"

/* All command files are squeezed into one chunk */


#define DDR_IMG_BASE_ADDR           0x60800000
#define DDR_DEPTH_BASE_ADDR         0x60900000
/* Model base address */
#define FD_BASE_ADDR                0x60000000      // face detect
#define LM_BASE_ADDR                0x70000000      // landmark
#define FR_BASE_ADDR                0x80000000      // face recognition
#define LD_BASE_ADDR                0x90000000      // liveness detect

/* bin file offset address */
#define CMD_BIN_OFFSET              0x00000000
#define INP_BIN_OFFSET              0x00100000
#define SET_BIN_OFFSET              0x00200000
#define WEI_BIN_OFFSET              0x00300000
#define NPU_IN_OFFSET               0x02000000
#define NPU_OUT_OFFSET              0x03000000


/**
 * Model address mapping
 */
/* FD 
input: rgba8 160 160 1 0 140 0 0 102400 (w=4)
       110 160 1 0 109 1611661312 1611763712 (0x60100000 - 0x60119000)
output: seq8 20 20 1 0 2 0 0 640 (w=2, col=32)
        20 20 1 0 19 1627389952 1627390592 (0x61000000 - 0x61000280)
*/
#define FD_INP_CMD_ADDR     (FD_BASE_ADDR + INP_BIN_OFFSET)
#define FD_INP_CMD_SIZE     0xB0
#define FD_INP_IN_ADDR      DDR_IMG_BASE_ADDR
#define FD_INP_IN_ROW       491 //640       
#define FD_INP_IN_COL       864 //360       
#define FD_INP_IN_SIZE      (FD_INP_IN_ROW * FD_INP_IN_COL)
#define FD_INP_OUT_ADDR     (FD_BASE_ADDR + NPU_IN_OFFSET)
#define FD_INP_OUT_ROW      160
#define FD_INP_OUT_COL      160
#define FD_INP_OUT_SIZE     (FD_INP_OUT_ROW * FD_INP_OUT_COL * 4)

#define FD_IN_ADDR			(FD_BASE_ADDR + NPU_IN_OFFSET)
#define FD_IN_SIZE          FD_INP_OUT_SIZE
#define FD_CMD_ADDR         (FD_BASE_ADDR + CMD_BIN_OFFSET)
#define FD_CMD_SIZE         0x2B60
#define FD_WEI_ADDR			(FD_BASE_ADDR + WEI_BIN_OFFSET)
#define FD_WEI_SIZE         0x64910  
#define FD_SET_ADDR			(FD_BASE_ADDR + SET_BIN_OFFSET)
#define FD_SET_SIZE         0x00
#define FD_OUT_ADDR			(FD_BASE_ADDR + NPU_OUT_OFFSET)
#define FD_OUT_SIZE         0x280                      // 20 * 32 * 1
#define FD_OUT_ROW          20
#define FD_OUT_COL          20
#define FD_OUT_CH           1
#define FD_OUT_COL_NMEM     32

/* Landmark
input: rgba8 48 48 1 0 20 0 0 9216 (w=4)
       48 48 1 0 47 1880096768 1880105984 (0x70100000 - 0x70102400)
output: seq8 1 1 10 0 1 0 0 160 (w=1, col=16)
        1 1 10 0 0 1895825408 1895825568 (0x71000000 - 0x710000a0)
*/
#define LM_INP_CMD_ADDR     (LM_BASE_ADDR + INP_BIN_OFFSET)
#define LM_INP_CMD_SIZE     0xB0
#define LM_INP_IN_ADDR      DDR_IMG_BASE_ADDR
#define LM_INP_IN_ROW       491 //640       
#define LM_INP_IN_COL       864 //360
#define LM_INP_IN_SIZE      (LM_INP_IN_ROW * LM_INP_IN_COL)
#define LM_INP_OUT_ADDR     (LM_BASE_ADDR + NPU_IN_OFFSET)
#define LM_INP_OUT_ROW      48
#define LM_INP_OUT_COL      48
#define LM_INP_OUT_SIZE     (LM_INP_OUT_ROW * LM_INP_OUT_COL * 4)

#define LM_IN_ADDR			(LM_BASE_ADDR + NPU_IN_OFFSET)
#define LM_IN_SIZE          LM_INP_OUT_SIZE
#define LM_CMD_ADDR         (LM_BASE_ADDR + CMD_BIN_OFFSET)
#define LM_CMD_SIZE         0x5CC
#define LM_WEI_ADDR			(LM_BASE_ADDR + WEI_BIN_OFFSET)
#define LM_WEI_SIZE         0x374A0  
#define LM_SET_ADDR			(LM_BASE_ADDR + SET_BIN_OFFSET)
#define LM_SET_SIZE         0x00
#define LM_OUT_ADDR			(LM_BASE_ADDR + NPU_OUT_OFFSET)
#define LM_OUT_SIZE         0xA0                      // 1 * 16 * 10
#define LM_OUT_ROW          1
#define LM_OUT_COL          1
#define LM_OUT_CH           10
#define LM_OUT_COL_NMEM     16


/* FR
input: rgba8 112 112 3 0 52 0 0 50176 (w=4)
       58 112 3 0 57 -2146435072 -2146384896 (0x80100000 - 0x8010c400)
output: seq8 1 1 512 32 16 0 0 8192 (w=1, col=16)
        1 1 512 0 0 -2130706432 -2130698240 (0x81000000 - 0x81002000)
*/
#define FR_INP_CMD_ADDR     (FR_BASE_ADDR + INP_BIN_OFFSET)
#define FR_INP_CMD_SIZE     0xB0
#define FR_INP_IN_ADDR      DDR_IMG_BASE_ADDR
#define FR_INP_IN_ROW       491 //640
#define FR_INP_IN_COL       864 //360
#define FR_INP_IN_SIZE      (FR_INP_IN_ROW * FR_INP_IN_COL)
#define FR_INP_OUT_ADDR     (FR_BASE_ADDR + NPU_IN_OFFSET)
#define FR_INP_OUT_ROW      112
#define FR_INP_OUT_COL      112
#define FR_INP_OUT_SIZE     (FR_INP_OUT_ROW * FR_INP_OUT_COL * 4)
 
#define FR_IN_ADDR			(FR_BASE_ADDR + NPU_IN_OFFSET)
#define FR_IN_SIZE          FR_INP_OUT_SIZE
#define FR_CMD_ADDR         (FR_BASE_ADDR + CMD_BIN_OFFSET)
#define FR_CMD_SIZE         0xF7C
#define FR_WEI_ADDR			(FR_BASE_ADDR + WEI_BIN_OFFSET)
#define FR_WEI_SIZE         0x2C7A00  
#define FR_SET_ADDR			(FR_BASE_ADDR + SET_BIN_OFFSET)
#define FR_SET_SIZE         0x00
#define FR_OUT_ADDR			(FR_BASE_ADDR + NPU_OUT_OFFSET)
#define FR_OUT_SIZE         0x2000                      // 1 * 16 * 512
#define FR_OUT_ROW          1
#define FR_OUT_COL          1
#define FR_OUT_CH           512
#define FR_OUT_COL_NMEM     16



/* Liveness detection
input: rgba8 56 56 1 0 28 0 0 14336 (w=4)
       56 56 1 0 55 -1877999616 -1877985280 (0x90100000 - 0x90103800)
output: seq16 34 34 2 0 15 0 0 5440 (w=5, col=40)
        34 34 2 0 33 -1862270976 -1862265536 (0x91000000 - 0x91001540)
*/
#define LD_INP_CMD_ADDR     (LD_BASE_ADDR + INP_BIN_OFFSET)
#define LD_INP_CMD_SIZE     0xB0
#define LD_INP_IN_ADDR      DDR_DEPTH_BASE_ADDR
//#define LD_INP_IN_ADDR      DDR_IMG_BASE_ADDR
#define LD_INP_IN_ROW       491 //640
#define LD_INP_IN_COL       864 //360
#define LD_INP_IN_SIZE      (LD_INP_IN_ROW * LD_INP_IN_COL * 2)
//#define LD_INP_IN_SIZE      (LD_INP_IN_ROW * LD_INP_IN_COL)
#define LD_INP_OUT_ADDR     (LD_BASE_ADDR + NPU_IN_OFFSET)
#define LD_INP_OUT_ROW      56
#define LD_INP_OUT_COL      56
#define LD_INP_OUT_COL_CEIL 64      // by the format of NMEM (base=16)
#define LD_INP_OUT_SIZE     (LD_INP_OUT_ROW * LD_INP_OUT_COL_CEIL * 4)
        
#define LD_IN_ADDR			(LD_BASE_ADDR + NPU_IN_OFFSET)
#define LD_IN_SIZE          LD_INP_OUT_SIZE
#define LD_CMD_ADDR         (LD_BASE_ADDR + CMD_BIN_OFFSET)
#define LD_CMD_SIZE         0x5B4
#define LD_WEI_ADDR			(LD_BASE_ADDR + WEI_BIN_OFFSET)
#define LD_WEI_SIZE         0x7C0  
#define LD_SET_ADDR			(LD_BASE_ADDR + SET_BIN_OFFSET)
#define LD_SET_SIZE         0x00
#define LD_OUT_ADDR			(LD_BASE_ADDR + NPU_OUT_OFFSET)
#define LD_OUT_SIZE         0x1540                          // 34 * 40 * 2 * 2
#define LD_OUT_ROW          34
#define LD_OUT_COL          34
#define LD_OUT_CH           2
#define LD_OUT_COL_NMEM     40


typedef struct inNode{
    u32 start;
    u32 length;
    u32 NumCol;
    u32 NumChannel;
    u32 NumRow;
    u32 lengthCol;
    u32 isAB;
    u32 dram_data;
    u32 cmd_data;
    u32 cmd_length;
    u32 weight_data;
    u32 weight_length;
    u32 move_input;
    u32 reserved_word_1;
    u32 reserved_word_2;
} inNode;

typedef struct outNode{
    u32 start;
    u32 length;
    u32 NumCol;
    u32 NumChannel;
    u32 NumRow;
    u32 isAB;
    u32 dram_data;
    u32 lengthSRAM;
    u32 row_offset;
    u32 lenCol;
    u32 lenColSRAM;
    u32 move_output;
    u32 output_frac_bit;
    u32 reserved_word_2;
    u32 reserved_word_3;
} outNode;

typedef struct cpuNode{
    u32 cpu_op_id;
    u32 NumCol;
    u32 NumChannel;
    u32 NumRow;
    u32 dram_data;
    u32 dram_data2;
    u32 isAB;
    u32 rowSRAM;
    u32 colSRAM;
    u32 dstrowSRAM;
    u32 dram_data_dst;
    u32 move_output;
    u32 dstNumChannel;
    u32 output_frac_bit;
    u32 dynamic_shift;
} cpuNode;


typedef struct operation{
    u32 op_id;
    inNode * inN;
    outNode * outN;
    cpuNode * cpuN;
    struct operation * next;
} operation;

typedef struct CNN_header{
    u32 operationNum;
    u32 convWeightSize;
    u32 dramSize;
    u32 inputC;
    u32 inputCh;
    u32 inputR;
    u32 cmdLength;
    u32 input_frac_bit;
    u32 model_scale;
} CNN_header;


typedef enum {
    E_NMEM_SEQ8 = 0,
    E_NMEM_SEQ16,
} NMEM_DATA_FORMAT;


/* Output is fixed-point value:
 *   The first 8 bit is signed int
 *   The last 8 bit is fractional part */
#define CNN_GET_OUT_VAL(val)    ((s16) (val) / 256.0)

#if  (defined(SYS_PC) | defined(DYFP))
Matrix* ImageMultistep(const Image *pSrc, const ImgScaleParam *pParam);
#else
void ImageMultistep(const ImgScaleParam *pParam);
#endif

void GetImage(u16* pOut, int numCol, int numRow, int numChannel, int div);
int CnnInit(void);
#ifdef DYFP_SELF_TEST
int RunCnnSelfTest(void);
#endif
void npu_reset(void);
int RunCnnFdDyFp(const Image *pImg, Matrix **ppOut1, Matrix **ppOut2);
int RunCnnLandmarkDyFp(const Image *pImg, const Box *pBox, Matrix **ppOut);
int RunCnnNirFrVggDyFp(const Image *pImg, Matrix **ppOut);
void cnn_model_parse(void);
static void parse_set_up(operation *cur_op, CNN_header **cur_cnn, int set_up_length, u32 *setup_buffer);
void CnnModelParse(void);
void nmem_data_alignment(NMEM_DATA_FORMAT format, u32 src_col, u32 src_row,
                         const u8 *src, u8 *dst, u32 dst_col);
int wait_cnn(void);
void start_cnn(u32 cmd_addr, u32 len);
static void parse_set_up(operation *cur_op, CNN_header **cur_cnn, int set_up_length, u32 *setup_buffer);
void operation_init(operation *cur_op, u32 op_id);
void pre_process_image(Image *img, CNN_header *cnn, int need_pre_process, bool copy_to_three_ch);
Matrix* ImageMultistepX(const Image *pSrc, const ImgScaleParam *pParam);



#endif // end of, #ifndef __CNN_H__
