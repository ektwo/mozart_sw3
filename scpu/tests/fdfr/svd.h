/**
 * @file      svd.h
 * @brief     Single Value Decomposition
 * @version $Id: svd.h 498 2018-07-16 05:43:35Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __SVD_H__
#define __SVD_H__

#include "matrix.h"

void SVD(Matrix *pA, Matrix **ppU, Matrix **ppS, Matrix **ppV, int *pRank);

#endif
