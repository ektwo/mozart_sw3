/**
 * @file      dma.h
 * @brief     dma related files
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */
#ifndef __DMA_H__
#define __DMA_H__


#include "base.h"

int InitDma(void);
int DmaSend(u32 pkt_length, u32 base_address);


#endif
