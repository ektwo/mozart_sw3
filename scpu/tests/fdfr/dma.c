#ifndef __SYS_XILINX__

#include "base.h"
#include "sys.h"
#include "dma.h"

int InitDma(void)
{   //dummy function
    return 1;
}
    
int DmaSend(u32 pkt_length, u32 base_address)
{   //dummy function
    return 1;
}

#else
#include <xaxidma.h>
#include "xparameters.h"
#include "xdebug.h"

#include "xplatform_info.h"
#include "xuartps.h"
#include "xscugic.h"
#include "xil_exception.h"
#include "xil_printf.h"

#ifdef __aarch64__
#include "xil_mmu.h"
#endif

#if defined(XPAR_UARTNS550_0_BASEADDR)
#include "xuartns550_l.h"       /* to use uartns550 */
#endif

#if (!defined(DEBUG))
extern void xil_printf(const char *format, ...);
#endif

#define UART_DEVICE_ID      XPAR_XUARTPS_0_DEVICE_ID
#define INTC_DEVICE_ID      XPAR_SCUGIC_SINGLE_DEVICE_ID
#define UART_INT_IRQ_ID     XPAR_XUARTPS_1_INTR

#define DMA_DEV_ID          XPAR_AXIDMA_0_DEVICE_ID
#ifdef XPAR_AXI_7SDDR_0_S_AXI_BASEADDR
#define DDR_BASE_ADDR       XPAR_AXI_7SDDR_0_S_AXI_BASEADDR
#elif XPAR_MIG7SERIES_0_BASEADDR
#define DDR_BASE_ADDR       XPAR_MIG7SERIES_0_BASEADDR
#elif XPAR_MIG_0_BASEADDR
#define DDR_BASE_ADDR       XPAR_MIG_0_BASEADDR
#elif XPAR_PSU_DDR_0_S_AXI_BASEADDR
#define DDR_BASE_ADDR       XPAR_PSU_DDR_0_S_AXI_BASEADDR
#endif

#ifndef DDR_BASE_ADDR
#warning CHECK FOR THE VALID DDR ADDRESS IN XPARAMETERS.H, DEFAULT SET TO 0x01000000
#define MEM_BASE_ADDR       0x01000000
#else
#define MEM_BASE_ADDR       (DDR_BASE_ADDR + 0x1000000)
#endif

#define TX_BD_SPACE_BASE    (MEM_BASE_ADDR)
#define TX_BD_SPACE_HIGH    (MEM_BASE_ADDR + 0x00000FFF)
#define RX_BD_SPACE_BASE    (MEM_BASE_ADDR + 0x00001000)
#define RX_BD_SPACE_HIGH    (MEM_BASE_ADDR + 0x00001FFF)

#define MARK_UNCACHEABLE    0x701

static XAxiDma AxiDma;

/* #################################
 * ##    KD Compiler Functions    ##
 * ################################# */

/**
 * This function sets up the TX channel of a DMA engine to be ready for packet
 * transmission
 * @param    AxiDmaInstPtr is the instance pointer to the DMA engine.
 * @return    XST_SUCCESS if the setup is successful, XST_FAILURE otherwise.
 */
static int TxSetup(XAxiDma *AxiDmaInstPtr)
{
    XAxiDma_BdRing *TxRingPtr;
    XAxiDma_Bd BdTemplate;
    int Delay = 0;
    int Coalesce = 1;
    int Status;
    u32 BdCount;

    TxRingPtr = XAxiDma_GetTxRing(&AxiDma);

    /* Disable all TX interrupts before TxBD space setup */

    XAxiDma_BdRingIntDisable(TxRingPtr, XAXIDMA_IRQ_ALL_MASK);

    /* Set TX delay and coalesce */
    XAxiDma_BdRingSetCoalesce(TxRingPtr, Coalesce, Delay);

    /* Setup TxBD space  */
    BdCount = XAxiDma_BdRingCntCalc(XAXIDMA_BD_MINIMUM_ALIGNMENT,
                                    TX_BD_SPACE_HIGH - TX_BD_SPACE_BASE + 1);
    xil_printf("TX BdCount is %d\n",BdCount);
    Status = XAxiDma_BdRingCreate(TxRingPtr, TX_BD_SPACE_BASE,
                                  TX_BD_SPACE_BASE,
                                  XAXIDMA_BD_MINIMUM_ALIGNMENT, BdCount);
    if (Status != XST_SUCCESS) {
        xil_printf("failed create BD ring in txsetup\n");

        return XST_FAILURE;
    }

    /*
     * We create an all-zero BD as the template.
     */
    XAxiDma_BdClear(&BdTemplate);

    Status = XAxiDma_BdRingClone(TxRingPtr, &BdTemplate);
    if (Status != XST_SUCCESS) {
        xil_printf("failed bdring clone in txsetup %d\n", Status);

        return XST_FAILURE;
    }

    /* Start the TX channel */
    Status = XAxiDma_BdRingStart(TxRingPtr);
    if (Status != XST_SUCCESS) {
        xil_printf("failed start bdring txsetup %d\n", Status);

        return XST_FAILURE;
    }

    return XST_SUCCESS;
}

/**
 * This function transmits one packet non-blockingly through the DMA engine.
 * @param    AxiDmaInstPtr points to the DMA engine instance, packet length and
 *           base address to send.
 * @return   - XST_SUCCESS if the DMA accepts the packet successfully,
 *           - XST_FAILURE otherwise.
 */
static int NewBdRing(XAxiDma *AxiDmaInstPtr, u32 pkt_length, u32 base_address)
{
    XAxiDma_BdRing *TxRingPtr;
    u8 *TxPacket;
    XAxiDma_Bd *BdPtr;
    int Status;

    TxRingPtr = XAxiDma_GetTxRing(AxiDmaInstPtr);
    u32 *Packet = (u32 *) base_address;
    /* Create pattern in the packet to transmit */
    TxPacket = (u8 *) Packet;

    /* Flush the SrcBuffer before the DMA transfer, in case the Data Cache
     * is enabled
     */
    Xil_DCacheFlushRange((UINTPTR)TxPacket, pkt_length);
#ifdef __aarch64__
    Xil_DCacheFlushRange((UINTPTR)RX_BUFFER_BASE, pkt_length);
#endif

    /* Capture the BD from hardware */
    Status = XAxiDma_BdRingFromHw(TxRingPtr, 1, &BdPtr);
    if(Status != 0){
        XAxiDma_BdRingFree(TxRingPtr, 1, BdPtr);
    }

    /* Allocate a BD */
    Status = XAxiDma_BdRingAlloc(TxRingPtr, 1, &BdPtr);
    if (Status != XST_SUCCESS) {
        xil_printf("SendPacket cannot allocate BD Ring %d\n", Status);
        return XST_FAILURE;
    }

    /* Set up the BD using the information of the packet to transmit */
    Status = XAxiDma_BdSetBufAddr(BdPtr, (UINTPTR) Packet);
    if (Status != XST_SUCCESS) {
        xil_printf("Tx set buffer addr %x on BD %x failed %d\n",
            (UINTPTR)Packet, (UINTPTR)BdPtr, Status);
        return XST_FAILURE;
    }

    Status = XAxiDma_BdSetLength(BdPtr, pkt_length,
                                 TxRingPtr->MaxTransferLen);
    if (Status != XST_SUCCESS) {
        xil_printf("Tx set length %d on BD %x failed %d\n",
                pkt_length, (UINTPTR)BdPtr, Status);

        return XST_FAILURE;
    }

#if (XPAR_AXIDMA_0_SG_INCLUDE_STSCNTRL_STRM == 1)
    Status = XAxiDma_BdSetAppWord(BdPtr,
        XAXIDMA_LAST_APPWORD, pkt_length);

    /* If Set app length failed, it is not fatal
     */
    if (Status != XST_SUCCESS) {
        xil_printf("Set app word failed with %d\n", Status);
    }
#endif

    /* For single packet, both SOF and EOF are to be set
     */
    XAxiDma_BdSetCtrl(BdPtr, XAXIDMA_BD_CTRL_TXEOF_MASK |
                        XAXIDMA_BD_CTRL_TXSOF_MASK);

    XAxiDma_BdSetId(BdPtr, (UINTPTR)Packet);

    /* Give the BD to DMA to kick off the transmission. */
    Status = XAxiDma_BdRingToHw(TxRingPtr, 1, BdPtr);
    if (Status != XST_SUCCESS) {
        xil_printf("to hw failed %d\n", Status);
        return XST_FAILURE;
    }

    return XST_SUCCESS;
}

/* ############################
 * ##    Public Functions    ##
 * ############################ */


/**
 * This function initilize everything
 * @return   XST_SUCCESS if the setup is successful, XST_FAILURE otherwise.
 * @note     None.
 */
int InitDma(void)
{
    xil_printf("\n--- Entering main() ---\n");

    int Status;
    XAxiDma_Config *Config = NULL;

#ifdef __aarch64__
    Xil_SetTlbAttributes(TX_BD_SPACE_BASE, MARK_UNCACHEABLE);
    Xil_SetTlbAttributes(RX_BD_SPACE_BASE, MARK_UNCACHEABLE);
#endif

    Config = XAxiDma_LookupConfig(DMA_DEV_ID);
    if (!Config) {
        xil_printf("No config found for %d\n", DMA_DEV_ID);
        return XST_FAILURE;
    }

    /* Initialize DMA engine */
    Status = XAxiDma_CfgInitialize(&AxiDma, Config);
    if (Status != XST_SUCCESS) {
        xil_printf("Initialization failed %d\n", Status);
        return XST_FAILURE;
    }

    if(!XAxiDma_HasSg(&AxiDma)) {
        xil_printf("Device configured as Simple mode\n");
        return XST_FAILURE;
    }

    Status = TxSetup(&AxiDma);
    if (Status != XST_SUCCESS) {
        return XST_FAILURE;
    }

    xil_printf("exiting ini\n");

    return XST_SUCCESS;
}


/**
 * @param pkt_length length of data to be send
 * @param base_address source data address
 * @return XST_SUCCESS on success
 */
int DmaSend(u32 pkt_length, u32 base_address)
{
    int Status;
    u32 left_length = pkt_length;
    u32 updated_base = base_address;
    while(left_length > 0xFFFF0){
        Status = NewBdRing(&AxiDma, 0xFFFF0, updated_base);
        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }
        left_length -= 0xFFFF0;
        updated_base += 0xFFFF0;
    }
    if(left_length > 0){
        Status = NewBdRing(&AxiDma, left_length, updated_base);
        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }
    }
    return XST_SUCCESS;
}
#endif // end of, #ifndef SYS_XILINX
