/**
 * @file      cnn_sim.c
 * @brief     Cnn simulation functions
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifdef SYS_PC

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "matrix.h"
#include "image.h"
#include "imagecv.h"
#include "cnn.h"
#include "fd.h"
#include "fr.h"
#include "hwctrl.h"

#define CNN_REQ_FIFO           "/tmp/cnn_req_fifo"
#define CNN_RESP_FIFO          "/tmp/cnn_resp_fifo"
#define CNN_EXE                "cnn_worker.py"

#define CNN_MODEL_FD           0
#define CNN_MODEL_FD_POOL      1
#define CNN_MODEL_LANDMARK     2
#define CNN_MODEL_FR           3

#if DBG_CNN
#define DbgLog(fmt, args...)  printf(fmt"\n", ##args)
#else
#define DbgLog(...)
#endif

static int _sReqFifo;
static int _sRespFifo;

typedef struct {
    int model;
    int imgWidth;
    int imgHeight;
    int msgSize;
} __attribute__((packed)) CnnMsgHdr;

/* ############################
 * ##    Static Functions    ##
 * ############################ */

static int FileExist(const char *file)
{
    struct stat buf;
    return stat(file, &buf);
}

static int IsWorkerAlive()
{
    char cmd[256] = {0};

    if (NULL == getcwd(cmd, sizeof(cmd))) {
        printf("Failed to call getcwd() with errno [%d]\n", errno);
        return -1;
    }

    const char *pPat = "projects/himax/";
    char *pStr = strstr(cmd, pPat);

    if (!pStr) {
        printf("Failed to find CNN exe path\n");
        return -1;
    }

    pStr += strlen(pPat);
    strcpy(pStr, "src/core/utils/" CNN_EXE " -p");

    return system(cmd);
}

/**
 * @brief RunCnn
 * @param model model number
 * @param pIn input matrix
 * @param ppOut output matrix
 * @return 0 on success
 */
static int RunCnn(int model, const Matrix *pIn, Matrix **ppOut)
{
    int ret = -1;
    int size = pIn->width * pIn->height * sizeof(float);
    Matrix *pMat = MatAsType(pIn, MT_FLOAT);
    CnnMsgHdr hdr = { model, pMat->width, pMat->height, size };
    CnnMsgHdr respHdr = {0};

    *ppOut = NULL;
    DbgLog("Send cnn request hdr: model [%d], mat [%dx%d], data size [%d]",
           hdr.model, hdr.imgWidth, hdr.imgHeight, hdr.msgSize);

    if ((ret = write(_sReqFifo, &hdr, sizeof(hdr))) != sizeof(hdr)) {
        printf("Failed to write cnn msg hdr with ret [%d] and errno [%d]\n", ret, errno);
        goto FUNC_OUT;
    }

    if ((ret = write(_sReqFifo, pMat->data.c, size)) != size) {
        printf("Failed to write img data with ret [%d] and errno [%d]\n", ret, errno);
        goto FUNC_OUT;
    }

    /* Read response */
    if ((ret = read(_sRespFifo, &respHdr, sizeof(respHdr))) != sizeof(respHdr)) {
        printf("Failed to read cnn msg hdr with ret [%d] and errno [%d]\n", ret, errno);
        goto FUNC_OUT;
    }

    DbgLog("Receive response hdr: model [%d], mat [%dx%d], data size [%d]",
           respHdr.model, respHdr.imgWidth, respHdr.imgHeight, respHdr.msgSize);

    *ppOut = MakeMatrix(respHdr.imgWidth, respHdr.imgHeight, MT_FLOAT);

    if ((ret = read(_sRespFifo, (*ppOut)->data.c, respHdr.msgSize)) != respHdr.msgSize) {
        printf("Failed to read cnn result with ret [%d] and errno [%d]\n", ret, errno);
        FreeMatrix(*ppOut);
        goto FUNC_OUT;
    }

    ret = 0;

FUNC_OUT:

    FreeMatrix(pMat);

    return ret;
}

/* ############################
 * ##    Public Functions    ##
 * ############################ */

/**
 * @brief Mimic HW read-in control
 * @param pSrc source image
 * @param pParam image scale param
 * @return Output float matrix
 */
Matrix* ImageMultistep(const Image *pSrc, const ImgScaleParam *pParam)
{
    /* Crop */
    Image *pCrop = SubImage(pSrc, pParam->cropYstr, pParam->cropYend,
                            pParam->cropXstr, pParam->cropXend);

    /* Scale */
    Image *pResize = MakeImage(pParam->scaleW, pParam->scaleH);
    cvResize(pResize, pCrop);

    /* Padding */
    Size size = { pResize->width + pParam->padX*2, pResize->height + pParam->padY*2 };
    Image *pPadding = GenPadImg(pResize, &size);;

    /* Normalization */
    Matrix* pRes = MatAsType(pPadding, MT_FLOAT);
    int cnt = pRes->width*pRes->height;
    int i;

    if (pParam->subVal != 0) {
        float *pVal = pRes->data.f;
        for (i = 0; i < cnt; ++i) {
            *pVal -= pParam->subVal;
            pVal++;
        }
    }

    if (pParam->divide != 1) {
        float *pVal = pRes->data.f;
        for (i = 0; i < cnt; ++i) {
            *pVal /= pParam->divide;
            pVal++;
        }
    }

    FreeImage(pCrop);
    FreeImage(pResize);
    FreeImage(pPadding);

    return pRes;
}

/**
 * @brief Init FIFO to communicate with CNN worker
 * @return 0 on success
 */
int CnnInit(void)
{
    if (0 != IsWorkerAlive()) {
        puts("CNN worker not alive.");
        return -1;
    }

    if ((0 != FileExist(CNN_REQ_FIFO)) ||
        (0 != FileExist(CNN_RESP_FIFO))) {
        puts("CNN fifo not ready.");
    }

    if ((_sReqFifo = open(CNN_REQ_FIFO, O_WRONLY)) == -1) {
        printf("Failed to open fifo with errno [%d]\n", errno);
        return -1;
    }

    if ((_sRespFifo = open(CNN_RESP_FIFO, O_RDONLY)) == -1) {
        printf("Failed to open fifo with errno [%d]\n", errno);
        return -1;
    }

    return 0;
}

int RunCnnFd(const Image *pImg, Matrix **ppOut1, Matrix **ppOut2)
{
    ImgScaleParam param = GetFdScaleParam(0, FD_IMG_SCALE_HEIGHT);
    Matrix *pIn = ImageMultistep(pImg, &param);
    int ret = RunCnn(CNN_MODEL_FD, pIn, ppOut1);

    if (0 == ret) {
        ret = RunCnn(CNN_MODEL_FD_POOL, *ppOut1, ppOut2);
    }

    FreeImage(pIn);

    return ret;
}

int RunCnnLandmark(const Image *pImg, const Box *pBox, Matrix **ppOut)
{
    ImgScaleParam param = GetLandmarkScaleParam(pBox);
    Matrix *pIn = ImageMultistep(pImg, &param);
    int ret = RunCnn(CNN_MODEL_LANDMARK, pIn, ppOut);
    FreeMatrix(pIn);

    return ret;
}

int RunCnnNirFr(const Image *pImg, Matrix **ppOut)
{
    ImgScaleParam param = GetNirScaleParam(0, FR_IMG_HEIGHT);
    Matrix *pIn = ImageMultistep(pImg, &param);
    int ret = RunCnn(CNN_MODEL_FR, pIn, ppOut);
    FreeMatrix(pIn);

    return ret;
}

int RunCnnNirFrVgg(const Image *pImg, Matrix **ppOut)
{
    ImgScaleParam param = GetNirScaleParam(0, FR_IMG_HEIGHT);
    Matrix *pIn = ImageMultistep(pImg, &param);
    int ret = RunCnn(CNN_MODEL_FR, pIn, ppOut);
    FreeMatrix(pIn);

    return ret;
}

#endif
