#include "base.h"
#include "memory.h"
#include <string.h>


//not all of our embedded systems utilize the standard library, and perhaps round() is not supplied. 
//inline fixed_point_t float_to_fixed(double input)
//{
//    return (fixed_point_t)(round(input * (1 << FIXED_POINT_FRACTIONAL_BITS)));
//}
//  ==> 
//inline fixed_point_t float_to_fixed(double input, uint8_t fractional_bits)
//{
//    return (fixed_point_t)(input * (1 << fractional_bits));
//}

//double fixed16_to_double(uint16_t input, uint8_t fractional_bits)
//{
//    return ((double)input / (double)(1 << fractional_bits));
//}


inline float fixed16_to_float(fixed16 input, uint8_t fraction)
{
    return ((float)input / (float)(1 << fraction));
}

inline float fixed8_to_float(fixed8 input, uint8_t fraction)
{
    return ((float)input / (float)(1 << fraction));
}

inline fixed16 float_to_fixed16(float input, uint8_t fraction)
{
    return (fixed16)(input * (1 << fraction));
}

inline fixed8 float_to_fixed8(float input, uint8_t fraction)
{
    return (fixed8)(input * (1 << fraction));
}

float* new_float_block_from_s8(s8* addr, int fraction,float scale, int len)
{
    float *ret = (float*)MemCalloc(len, sizeof(float));
    int i;
    for(i = 0; i < len; i++) 
    {
        *(ret+i) = (float)(*addr+i)/(float)(1 << fraction)/scale;
    }
    return ret;
}

void nmem2seq(void* pAddr,
              int row, int col, int ch,
              int pNMEMCol, int nSeq/*8 or 16*/)
{
    int i;

    unsigned char *dest = pAddr;
    unsigned char *src = pAddr;

    unsigned char nBitWidth = 1;
    if(nSeq == 16)
    {
        nBitWidth = 2;
    }

    unsigned short nBlockSize = nBitWidth * col;
    unsigned char nEntrySize = nBitWidth* pNMEMCol;

    dest = dest + nBlockSize;
    src  = src  + nEntrySize;

    int nTotalRows = ch * row;
    for(i=1;i<nTotalRows;i++) {
        memcpy(dest, src, nBlockSize);
        dest = dest + nBlockSize;
        src  = src  + nEntrySize;
    }

    return;
}
