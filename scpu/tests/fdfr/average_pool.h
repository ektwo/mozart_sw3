
#ifndef __AVERAGE_POOL_H__
#define __AVERAGE_POOL_H__

#include "math.h"
#define max(x,y) ((x)>(y)?(x):(y))

int get_idx(int row, int col, int ch, int total_col, int total_ch);
float average(float *src, int s_i, int s_j, int s_k, int kernel, int col, int ch);
void averagePool(float *src, float *dst, int row, int col, int ch, int kernel, int stride, int need_padding);

#endif
