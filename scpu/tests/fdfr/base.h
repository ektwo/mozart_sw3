/**
 * @file      base.h
 * @brief     Basic  utils & struct
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __BASE_CNN_H__
#define __BASE_CNN_H__

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "io.h"

typedef uint8_t    u8;
typedef uint16_t   u16;
typedef uint32_t   u32;
typedef uint64_t   u64;

typedef int8_t     s8;
typedef int16_t    s16;
typedef int32_t    s32;
typedef int64_t    s64;

typedef uint16_t   fixed16;    // fixed-point 16-bit
typedef uint8_t    fixed8;     // fixed-point 8-bit


#define BIT(x)      (0x01 << (x))


float fixed16_to_float(fixed16 input, uint8_t fraction);
float fixed8_to_float(fixed8 input, uint8_t fraction);
fixed16 float_to_fixed16(float input, uint8_t fraction);
fixed8 float_to_fixed8(float input, uint8_t fraction);

/**
 * new a float array of converted signed char value array
 * @param addr  : signed char array(seq. data)
 * @param len   : elements count
 * @return      : an allocated array of float type elements
 */
float* new_float_block_from_s8(s8* addr, int fraction, float scale, int len);

/**
 * convert NMEM format to seq format in DDR
 * @param pAddr : DDR address of the data
 * @param row   : row attribute
 * @param col   : column attribute
 * @param ch    : channel attribute
 * @param pNMEMCol: NMEM column number of an entry
 * @param nSeq  : seq8 or seq16 mode
 * @return : data@pAddr is overwritten
 */
void nmem2seq(void* pAddr,
              int row, int col, int ch,
              int pNMEMCol, int nSeq/*8 or 16*/);

#endif
