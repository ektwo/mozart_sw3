/**
 * @file      facealign.h
 * @brief     Face alignment
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __FACEALIGN_H__
#define __FACEALIGN_H__


#include "base.h"
#include "fd.h"
#include "image.h"

void FaceAlignInit(void);
Image* FaceAlign(const Image *pImg, const Box *pBox, Matrix *pLandmark);

#endif
