/**
 * @file      imagecv.c
 * @brief     Opencv functions
 * @version $Id: imagecv.c 494 2018-07-13 09:48:36Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#include <assert.h>
#include <stdio.h>
#include "image.h"
#include "imagecv.h"
#include "matrix.h"

#define ROUND_DELTA    512

/* ############################
 * ##    Static Functions    ##
 * ############################ */

/**
 * Perform image affine transform (nearest neighbor)
 * Modified from https://blog.csdn.net/fengbingchun/article/details/17713429
 * @param pImg destination image
 * @param pSrc source image
 * @param pM [2x3] transform matrix
 */
void cvWarpAffine(Image *pImg, const Image *pSrc, const Matrix *pM)
{
    assert(((pM->height == 2) && (pM->width == 3)) || !"M should be 2x3");
    assert((pSrc->height >= pImg->height) && (pSrc->width >= pImg->width));
    assert(pM->type == MT_FLOAT);

    int newWidth = pImg->width;
    int newHeight = pImg->height;
    float *m = pM->data.f;

    float D = m[0]*m[4] - m[1]*m[3];
    D = D != 0 ? 1./D : 0;
    float A11 = m[4]*D;
    float A22 = m[0]*D;
    m[0] = A11; m[1] *= -D;
    m[3] *= -D; m[4] = A22;
    float b1 = -m[0]*m[2] - m[1]*m[5];
    float b2 = -m[3]*m[2] - m[4]*m[5];
    m[2] = b1;
    m[5] = b2;

    /* To prevent from floating point operations,
     * convert to int by scaling up then scaling down (may overflow?) */
    int M[6];
    M[0] = m[0] * 1024;
    M[1] = m[1] * 1024;
    M[2] = m[2] * 1024;
    M[3] = m[3] * 1024;
    M[4] = m[4] * 1024;
    M[5] = m[5] * 1024;

#ifndef NDEBUG
    /* Check overflow */
    int i;
    int bitCnt = 31 - 7 - 1;
    for (i = 0; i < 6; ++i) {
        int val = (M[i] < 0) ? -M[i] : M[i];
        /* printf("%d\n", val); */
        assert(val < (1 << bitCnt));
    }
#endif

    int adelta, bdelta, X0, Y0;
    int x, y;
    unsigned width = pSrc->width;
    unsigned height = pSrc->height;
    unsigned X, Y;
    imdata *pValSrc = IM_DATA(pSrc);
    imdata *pValDst = IM_DATA(pImg);

    for (y = 0; y < newHeight; ++y) {
        for (x = 0; x < newWidth; ++x) {
            adelta = ((s64) M[0]*(x << 10)) >> 10;
            X0 = M[1]*y + M[2] + ROUND_DELTA;
            X = ((X0 + adelta) >> 10);

            imdata val = 0;
            if (X < width) {
                bdelta = ((s64) M[3]*(x << 10)) >> 10;
                Y0 = M[4]*y + M[5] + ROUND_DELTA;
                Y = (Y0 + bdelta) >> 10;
                if (Y < height) {
                    val = IM_GET_VAL(pValSrc, width, Y, X);
                }
            }
            IM_SET_VAL(pValDst, newWidth, y, x, val);
        }
    }
}

/**
 * Resize image (nearest neighbor)
 * Modified from https://blog.csdn.net/fengbingchun/article/details/17335477
 * @param pImg destination image
 * @param pSrc source image
 */
void cvResize(Image *pImg, const Image *pSrc)
{
    int width = pImg->width;
    int height = pImg->height;
    int widthSrc = pSrc->width;
    int heightSrc = pSrc->height;
    int scaleX = (widthSrc << 10) / width;
    int scaleY = (heightSrc << 10) / height;
    int i, j;
    imdata *pValSrc = IM_DATA(pSrc);
    imdata *pValDst = IM_DATA(pImg);

    /* To prevent from floating point operations,
     * convert to int by scaling up then scaling down (may overflow?) */
    for (i = 0; i < height; ++i) {
        int sy = FLOOR((i*scaleY) >> 10);
        sy = MIN(sy, heightSrc - 1);
        for (j = 0; j < width; ++j) {
            int sx = FLOOR((j*scaleX) >> 10);
            sx = MIN(sx, widthSrc - 1);
            *pValDst++ = IM_GET_VAL(pValSrc, widthSrc, sy, sx);
        }
    }
}

#ifdef DYFP

/**
 * embed_image from source image to destination image
 * assume channel = 1
*/
void embed_image(Image *source, Image *dest, int dx, int dy)
{
    //int height = dest->height;
    int width = dest->width;
    int heightsrc = source->height;
    int widthsrc = source->width;

    imdata *pValSrc = IM_DATA(source);
    imdata *pValDst = IM_DATA(dest);
    for (int i = 0; i < heightsrc; ++i) {
        for (int j = 0; j < widthsrc; ++j) {
            imdata val = IM_GET_VAL(pValSrc, widthsrc, i, j);
            IM_SET_VAL(pValDst, width, i+dy, j+dx, val);
        }
    }
}

/**
 * fill image with given values
 * assume channel = 1
 *
*/
static void fill_image(Image *m, u8 s)
{
    int i,j;
    imdata *pVal = IM_DATA(m);
    int width = m->width;
    int height = m->height;

    for(i = 0; i < height; ++i){
        for (j=0; j< width; ++j){
            IM_SET_VAL(pVal, width, i, j, s);
        }
    }
}

/**
 * convert rectangle image to squre image and add padding on left and right
 * assume channel = 1
*/
Image* letterbox_image(Image *im, int w, int h)
{
    int new_w = im->width;
    int new_h = im->height;
    if (((float)w/im->width) < ((float)h/im->height)) {
        new_w = w;
        new_h = (im->height * w)/im->width;
    } else {
        new_h = h;
        new_w = (im->width * h)/im->height;
    }

    Image *resized = MakeImage(new_w, new_h);
    cvResize(resized, im);

    Image *boxed = MakeImage(w, h);
    fill_image(boxed, 0);
    //int h_off = (boxed->width - resized->width) / 2;
    //int v_off = (boxed->height - resized->height) / 2;
    //int l = MAX(0, h_off);
    //int t = MAX(0, v_off);
    //int r = MIN(pad->width, pad->width - h_off);
    //int b = MIN(pad->height, pad->height - v_off);

    //Image *boxed = SubImage(pad, t, b, l, r);

    embed_image(resized, boxed, 0, 0);
    FreeImage(resized);
    return boxed;
}

#endif
