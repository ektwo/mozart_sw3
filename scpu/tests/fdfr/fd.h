/**
 * @file      fd.h
 * @brief     face detection header
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __FD_H__
#define __FD_H__

#include "base.h"
#include "image.h"
#include "matrix.h"
#include "hwctrl.h"

#include "box.h"
void FdInit(void);
bool FaceDet(const Image *pImg, const Image *pDepth, Box *pBox, Matrix **ppLandmark, bool isValidFaceDet);
bool IsValidFace(const Image *pDepth, const Box *pBox, const Matrix *pLandmark);

//vincent
int fd_postprocess(void);
void feat2rect_nir(Box* box/*out*/, float* feature, float* feature_pooled, float scale_init);

#ifdef DYFP


bool Feature2Rectangle(Box *pBox,
                              const Matrix *pFeature, const Matrix *pFeaturePool);

int GetFdBox1(const Matrix *pFeature, Box *pBox);
float GetFdBox2(const Matrix *pFeature, const Matrix *pFeaturePool, Box *pBox);
void GetFaceBox(Box *pFace, const Box *pSrc);
bool IsValidBox(const Box *pBox);
ImgScaleParam GetFdScaleParam(const Box *pBox);
ImgScaleParam GetFdScaleParamX(int start, int height);



#else
    

#include "landmark.h"


ImgScaleParam GetFdScaleParam(int start, int height);




#if DYFP
bool Feature2Rectangle(Box *pBox, const Matrix *pFeature, const Matrix *pFeaturePool);
#endif
#endif

#endif
