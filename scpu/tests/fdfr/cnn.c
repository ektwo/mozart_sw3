/**
 * @file      cnn.c
 * @brief     Cnn function implementation
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef SYS_PC

#include <assert.h>
#include "cnn.h"
#include "hwctrl.h"
#include "npu.h"
#include "fd.h"
#include "base.h"
#include "memory.h"
#include "profile.h"
#include "fr.h"
#include "dma.h"
#include "string.h"
#include "stdlib.h"
#include "average_pool.h"
#include "imagecv.h"
#include "liveness.h"
#include "io.h"  //CM4 ARM
#include "DrvPWMTMR010.h"
#include "fdebug.h"
#include "sys.h"

static int cpu_operation_offset;
static u16 *dram;

/* Test code for INPROC run on DDR/SDRAM/NDRAM */


#define INPROC_BIN_RUN_DDR      0
#define INPROC_BIN_RUN_SDRAM    1
#define INPROC_BIN_RUN_NDRAM    2
#define INPROC_RUN_MODE         INPROC_BIN_RUN_NDRAM


enum {
    SDRAM_IDX = 0,
    NDRAM_IDX
};

#define INPROC_ADDR_OFFSET          0x5000
int MEM_SCPU_ADDR[2] = {0x10200000, 0x2FFF0000};
int MEM_NPU_ADDR[2] = {0x20200000, 0x0FFF0000};


#if (INPROC_RUN_MODE == INPROC_BIN_RUN_SDRAM)
u8 inproc_bin_sram[0xB0] __attribute__((at(0x10200000 + INPROC_ADDR_OFFSET)));
#elif (INPROC_RUN_MODE == INPROC_BIN_RUN_NDRAM)
u8 inproc_bin_sram[0xB0] __attribute__((at(0x2FFF0000 + INPROC_ADDR_OFFSET)));
#endif



#define LOG_CNN_INFO
#define ROT_CW  0
#define ROT_CCW 1

/* ############################
 * ##    Static Functions    ##
 * ############################ */


/**
 * @brief Whether scale flag needs to be enable
 * @param pParam image scale param
 * @return 1 if scale flag needs to be enabled
 */
bool NeedScale(const ImgScaleParam *pParam)
{
    int cropW = pParam->cropXend - pParam->cropXstr;
    int cropH = pParam->cropYend - pParam->cropYstr;

    return ((cropW == pParam->scaleW) && (cropH == pParam->scaleH)) ? false : true;
}

static Matrix* GetCnnResult(const float *pData, int row, int col)
{
    int size = row*col;
    Matrix *pMat = MakeMatrix(col, row, MT_FLOAT);
    float *pf = pMat->data.f;
    #ifdef DYFP
    const float *pVal = pData;
    #else
    const u16 *pVal = pData;
    #endif

    while (size--) {
        *pf++ = CNN_GET_OUT_VAL(*pVal);
        pVal++;
    }

    return pMat;
}

/**
 * @brief calculate padding from
 * @param in_row input image row
 * @param in_col input image column
 * @param out_row output image row
 * @param out_col output image column
 * @param pad_mode 0 : pad on both side, 1 : pad on right and bottom only
 * @param pad_top
 * @param pad_bottom
 * @param pad_left
 * @param pad_right
 * @return N/A
 */
static void calculate_padding(int in_row, int in_col, int out_row, int out_col, int pad_mode,
                              int* pad_top, int* pad_bottom, int* pad_left, int* pad_right)
{
    //pad on both side
    if (pad_mode == 0) {
        if (in_row >= in_col) {
            *pad_top = 0;
            *pad_bottom = 0;
            *pad_left = (out_row - ((int)(((float)out_row / in_row) * in_col) / 2) * 2) / 2;
            *pad_right = *pad_left;
        } else {
            *pad_top = (out_col - ((int)(((float)out_col / in_col) * in_row) / 2) * 2) / 2;
            *pad_bottom = *pad_top;
            *pad_left = 0;
            *pad_right = 0;
        }
    }
    //only pad right and bottom
    else if (pad_mode == 1) {
        if (in_row >= in_col) {
            *pad_top = 0;
            *pad_bottom = 0;
            *pad_left = 0;
            *pad_right = (int)(out_row -  (((float)out_row / in_row)* in_col));
        } else {
            *pad_top = 0;
            *pad_bottom = (int)(out_col - (((float)out_col / in_col) * in_row));
            *pad_left = 0;
            *pad_right = 0;
        }
    }
}

/**
 * @brief alter inprocess data but keep the op code
 * @param addr DDR address of inprocess data to alter
 * @value value to alter
 * @return N/A
 */
static void alter_inproc_data(int addr, int value)
{
    int data = ReadWord(addr);
    data &= 0xFFFF0000;
    data |= (value & 0x0000FFFF);
    WriteWord(addr, data);
}

/**
 * @brief align the specified data to 4 
 * @param
 * @return
 */
static inline int pad_to_4(int value)
{
    return (value % 4) ? (((value / 4) + 1) * 4): value;
}

//TODO: reduce the calculation of each variable if it is fixed on the following models

/**
 * @brief update the content of inprocess data
 * @param inproc_base_addr DDR address of inprocess data
 * @param in_addr DDR address of input data
 * @param out_addr DDR address of output data
 * @param in_row input image row
 * @param in_col input image column
 * @param out_row output image row
 * @param out_col output image column
 * @param crop_top the distance between the top of the original image & crop rectangle   
 * @param crop_bottom the distance between the bottom of the original image & crop rectangle
 * @param crop_left the distance between the left of the original image & crop rectangle
 * @param crop_right the distance between the right of the original image & crop rectangle
 * @param sub image subtration value
 * @param start_row for large-size image cut, if no image cut, set to 0
 * @param src_img_mode source image format,
 *                         NIR 8-bit (default in inproc.bin) = 0x20
 *                         RGB565 = 0x60
 *                         Raw8(1 channel) = NIR 8-bit = 0x20
 *                         Raw16(1 channel) = NIR 16-bit = 0x21
 *                         RGBA8888 = 0x80
 *                         ... please refer to Mozart NPU spec for more formats
 * @return N/A
 * @note it needs to modify the fixed addr on bin file, if bin file is changed, 
 *       it should update these codes.
 */
static void update_inproc(int inproc_base_addr, int in_addr, int out_addr, 
                          int in_row, int in_col, int out_row, int out_col, 
                          int crop_top, int crop_bottom, int crop_left, int crop_right,
                          int sub, int start_row, int src_img_mode)
{
    // calculate pixel_size
    //TODO: modify to macro defined
    int pixel_size;
    switch(src_img_mode){
    case 0x60:      pixel_size = 2;     break;      // RGB565
    case 0x21:      pixel_size = 2;     break;      // NIR 16-bit
    case 0x00:      pixel_size = 4;     break;      // RGBA8888
    case 0x20:                                      // NIR 8-bit
    default:        pixel_size = 1;     break;      // others
    }

    // TODO: pad_mode from?
    int pad_mode = 0;

    // calculate padding
    int pad_top, pad_bottom, pad_left, pad_right;
    int valid_in_row = in_row - (crop_top + crop_bottom);
    int valid_in_col = in_col - (crop_left + crop_right);
    calculate_padding(valid_in_row, valid_in_col, out_row, out_col, pad_mode,
                      &pad_top, &pad_bottom, &pad_left, &pad_right);
    int valid_out_row = out_row - (pad_top + pad_bottom);
    int valid_out_col = out_col - (pad_left + pad_right);

    // set NPU_RDMA0_SRC0_sa (start address of handle cropped image)
    int img_str_row = (start_row - pad_top) > 0 ? (start_row - pad_top) : 0;
    img_str_row = valid_in_row * img_str_row / valid_out_row;
    int offset = ((img_str_row + crop_top) * in_col + crop_left) * pixel_size;
    int sa = in_addr + offset;
    alter_inproc_data(inproc_base_addr + 0x00, sa);
    alter_inproc_data(inproc_base_addr + 0x04, (sa >> 16));

    // set NPU_RDMA0_SRC1_pitch
    int pitch = in_col * pixel_size;
    alter_inproc_data(inproc_base_addr + 0x08, pitch);

    // set NPU_RDMA0_SRC2_len
    int len = valid_in_col * pixel_size;
    alter_inproc_data(inproc_base_addr + 0x10, len);
    
    // set NPU_RDMA0_BLK_line
    int line = valid_in_row - 1;
    alter_inproc_data(inproc_base_addr + 0x18, line);

    // set NPU_NMEM_RDMA1_h / NPU_NMEM_RDMA1_w
    int rdma_hw = ((out_row << 16) & 0xFFFF0000) + 4;  // h=b31:16, w=b11:0
    alter_inproc_data(inproc_base_addr + 0x24, rdma_hw);
    alter_inproc_data(inproc_base_addr + 0x28, (rdma_hw >> 16));

    // set NPU_NMEM_RDMA2_l
    int ceil_l = out_col / (4 * 4);
    if (out_col % (4 * 4))
        ceil_l++;
    alter_inproc_data(inproc_base_addr + 0x2C, ceil_l);

    // set NPU_FORMAT_mode
    alter_inproc_data(inproc_base_addr + 0x30, src_img_mode);

    // set NPU_PREP0_sub (sub=b31:16)
    alter_inproc_data(inproc_base_addr + 0x3C, sub);

    // set NPU_PREP1_pad_r / NPU_PREP1_pad_l / NPU_PREP1_pad_b / NPU_PREP1_pad_t
    int pad = ((pad_right << 24) & 0xFF000000) + ((pad_left << 16) & 0x00FF0000) +
              ((pad_bottom << 8) & 0x0000FF00) + (pad_top & 0x000000FF); // r=b31:24, l=b23:16, b=b15:8, t=b7:0
    alter_inproc_data(inproc_base_addr + 0x40, pad);
    alter_inproc_data(inproc_base_addr + 0x44, (pad >> 16));

    // set NPU_RESIZE_DST_h / NPU_RESIZE_DST_w
    int resize_hw = ((valid_out_row << 16) & 0x01FF0000) + (valid_out_col & 0x000001FF);  // h=b24:16, w=b8:0
    alter_inproc_data(inproc_base_addr + 0x58, resize_hw);
    alter_inproc_data(inproc_base_addr + 0x5C, (resize_hw >> 16));

    // set NPU_RESIZE_RATIO0_w / NPU_RESIZE_RATIO0_h (scaling ratio)
    int ratio_w = 4096 * (pad_to_4(valid_in_col) - 1) / (valid_out_col - 1);
    int ratio_h = 4096 * (valid_in_row - 1) / (valid_out_row - 1);
    alter_inproc_data(inproc_base_addr + 0x60, ratio_w);
    alter_inproc_data(inproc_base_addr + 0x64, (ratio_w >> 16));
    alter_inproc_data(inproc_base_addr + 0x68, ratio_h);
    alter_inproc_data(inproc_base_addr + 0x6C, (ratio_h >> 16));

    // set NPU_WDMA0_DST0_da
    int da = out_addr;
    alter_inproc_data(inproc_base_addr + 0x7C, da);
    alter_inproc_data(inproc_base_addr + 0x80, (da >> 16));

    // set NPU_WDMA0_DST1_pitch
    int wpitch = ceil_l * 4 * 16;
    alter_inproc_data(inproc_base_addr + 0x84, wpitch);
    alter_inproc_data(inproc_base_addr + 0x88, (wpitch >> 16));

    // set NPU_WDMA0_DST2_len
    int wlen = wpitch;
    alter_inproc_data(inproc_base_addr + 0x8C, wlen);

    // set NPU_WDMA0_BLK_line
    int wline = out_row - 1;
    alter_inproc_data(inproc_base_addr + 0x90, wline);

    // set NPU_NMEM_WDMA1_h / NPU_NMEM_WDMA1_w
    int wdma_hw = ((out_row << 16) & 0x0FFF0000) + 4;   // h=b27:16, w=b11:0
    alter_inproc_data(inproc_base_addr + 0x9C, wdma_hw);
    alter_inproc_data(inproc_base_addr + 0xA0, (wdma_hw >> 16));

    // set NPU_NMEM_WDMA2_l
    alter_inproc_data(inproc_base_addr + 0xA4, ceil_l);
}


/* #################################
 * ##    KD Compiler Functions    ##
 * ################################# */

/* ##############################
 * ##    Exported Functions    ##
 * ############################## */

/**
 * @brief Image read-in control (Frame buffer => CNN BRAM):
 *        Steps: crop => resize => padding => normalization.
 *        The output will be put into bank ram after format converter;
 *        thus the output will not be in continuous order.
 * @param pParam image scale param
 */
/**
 * @brief Mimic HW read-in control
 * @param pSrc source image
 * @param pParam image scale param
 * @return Output float matrix
 */
Matrix* ImageMultistep(const Image *pSrc, const ImgScaleParam *pParam)
{
    /* Crop */
    Image *pCrop = SubImage(pSrc, pParam->cropYstr, pParam->cropYend,
                            pParam->cropXstr, pParam->cropXend);
    //Modify by Jenna
    Image *limage = letterbox_image((Image *)pCrop, MAX(pCrop->width, pCrop->height), MAX(pCrop->width, pCrop->height) );

    /* Scale */
    Image *pResize = MakeImage(pParam->scaleW, pParam->scaleH);
    cvResize(pResize, limage);

    FreeImage(pCrop);
    FreeImage(limage);

    return pResize;
}


/**
 * @brief Mimic HW read-in control
 * @param pSrc source image
 * @param pParam image scale param
 * @return Output float matrix
 */
Matrix* ImageMultistepX(const Image *pSrc, const ImgScaleParam *pParam)
{
    /* Crop */
    Image *pCrop = SubImage(pSrc, pParam->cropYstr, pParam->cropYend,
                            pParam->cropXstr, pParam->cropXend);

    /* Scale */
    Image *pResize = MakeImage(pParam->scaleW, pParam->scaleH);
    cvResize(pResize, pCrop);

    /* Padding */
    Size size = { pResize->width + pParam->padX*2, pResize->height + pParam->padY*2 };
    Image *pPadding = GenPadImg(pResize, &size);

    /* Normalization */
    Matrix* pRes = MatAsType(pPadding, MT_FLOAT);
    int cnt = pRes->width*pRes->height;
    int i;

    if (pParam->subVal != 0) {
        float *pVal = pRes->data.f;
        for (i = 0; i < cnt; ++i) {
            *pVal -= pParam->subVal;
            pVal++;
        }
    }

    if (pParam->divide != 1) {
        float *pVal = pRes->data.f;
        for (i = 0; i < cnt; ++i) {
            *pVal /= pParam->divide;
            pVal++;
        }
    }

    FreeImage(pCrop);
    FreeImage(pResize);
    FreeImage(pPadding);

    return pRes;
}



/**
 * @brief Get image data from SRAM to destination
 * @param pOut serialized output image data
 * @param numCol image width
 * @param numRow image height
 * @param numChannel image channel
 */
void GetImage(u16* pOut, int numCol, int numRow, int numChannel, int div)
{
#ifdef __ASTA__
#else
    /* multiple of 8 */
    int paddedLength = (numRow + (8-1)) / 8 * 8;

    GetData(0, paddedLength, numRow,
            numCol, numChannel, numRow,
            1, 0, numChannel,
            numCol, numCol, pOut);

    /* Remove padding byte (e.g. 7200 7800 => 72 78)*/
    assert(sizeof(imdata) < sizeof(u16) || !"no need to remove padding");
    imdata *pData = (imdata*) pOut;
    int size = numCol*numRow*numChannel;
    int shift = (div == 256) ? 0 : (div == 128) ? 1 : 8;

    while (size--) {
        *pData++ = (*pOut++ >> shift);
    }
#endif
}

int CnnInit(void)
{
    //CnnModelParse();
    return 0;
}



CNN_header *pCnnFd;
operation *pHeadFd;
CNN_header *pCnnLm;
operation *pHeadLm;
CNN_header *pCnnLd;
operation *pHeadLd;
CNN_header *pCnnFr;
operation *pHeadFr;


void CnnModelParse(void)
{
    //u32 *set_up_buffer = (u32 *) Set_up_RecvBuffer;
    //u32 *set_up_buffer = (u32 *) DDR_SETUP_BASE_ADDR;
    pCnnFd = MemCalloc(sizeof(CNN_header), sizeof(u8));
    pHeadFd = MemCalloc(sizeof(operation), sizeof(u8));
    pCnnLm = MemCalloc(sizeof(CNN_header), sizeof(u8));
    pHeadLm = MemCalloc(sizeof(operation), sizeof(u8));
    pCnnLd = MemCalloc(sizeof(CNN_header), sizeof(u8));
    pHeadLd = MemCalloc(sizeof(operation), sizeof(u8));
    pCnnFr = MemCalloc(sizeof(CNN_header), sizeof(u8));
    pHeadFr = MemCalloc(sizeof(operation), sizeof(u8));
    parse_set_up(pHeadFd, &pCnnFd, FD_SET_SIZE, (u32 *)FD_SET_ADDR);
    fLib_printf("== FD CNN Start ==\n");
    fLib_printf("cmd length = %d\n\r",(int)pCnnFd->cmdLength);
    fLib_printf("number of operation: %d\n\r",(int)pCnnFd->operationNum);

    parse_set_up(pHeadLm, &pCnnLm, LM_SET_SIZE, (u32 *)LM_SET_ADDR);
    fLib_printf("== LM CNN Start ==\n");
    fLib_printf("cmd length = %d\n\r",(int)pCnnLm->cmdLength);
    fLib_printf("number of operation: %d\n\r",(int)pCnnLm->operationNum);
      
    parse_set_up(pHeadFr, &pCnnFr, FR_SET_SIZE, (u32 *)FR_SET_ADDR);
    fLib_printf("== FR CNN Start ==\n");
    fLib_printf("cmd length = %d\n\r",(int)pCnnFr->cmdLength);
    fLib_printf("number of operation: %d\n\r",(int)pCnnFr->operationNum);
    
    parse_set_up(pHeadLd, &pCnnLd, LD_SET_SIZE, (u32 *)LD_SET_ADDR);
    fLib_printf("== LD CNN Start ==\n");
    fLib_printf("cmd length = %d\n\r",(int)pCnnLd->cmdLength);
    fLib_printf("number of operation: %d\n\r",(int)pCnnLd->operationNum);

}

void test_rotate(int src_addr_in_ddr, int row, int col, int rot_dir)
{
    fLib_printf("rotate input addr: %x, row=%d, col=%d\n", src_addr_in_ddr, row, col);

    #if 0   // write byte
    int unit = 4;
    unsigned char *pS = (unsigned char*)(src_addr_in_ddr);
    unsigned char *pD = (unsigned char*)(src_addr_in_ddr + (row * col * 4));
    #else   // write 4bytes
    int unit = 1;
    unsigned int *pS = (unsigned int*)(src_addr_in_ddr);
    unsigned int *pD = (unsigned int*)(src_addr_in_ddr + (row * col * 4));
    #endif

    //timer 1
    UINT32 timer_1 = fLib_CurrentT1Tick();

    //body: only simulate one ch
    unsigned int r, c; 
    for (r = 0; r < row; r++) { 
        for (c = 0; c < col; c++) { 
            #if 1
            // clockwise
            if (rot_dir == ROT_CW) {
                *(pD + unit * (c * row + (row - r - 1))) =
                                 *(pS + unit * (r * col + c));
            // counter-clockwise
            } else {
                *(pD + unit * (row * (col - c - 1) + r)) =
                        *(pS + unit * (r * col + c));
            }
            #else // no rotate
            *(pD + unit * (r * col + c)) = *(pS + unit * (r * col + c));
            #endif
        } 
    } 

    //timer2
    UINT32 timer_2 = fLib_CurrentT1Tick();
    fLib_printf("time rotate : %d\n", timer_2 - timer_1);

    return;
} 

/**
 * @Brife CnnMemFree(), Free CNN global memory
 */
void CnnMemFree()
{
    MemFree(pCnnFd);
    MemFree(pHeadFd);
    MemFree(pCnnLm);
    MemFree(pHeadLm);
    MemFree(pCnnLd);
    MemFree(pHeadLd);
    MemFree(pCnnFr);
    MemFree(pHeadFr);    
}

bool do_fd_postprocess()
{
    //TODO: move post process here
    return true;
}

bool do_lm_postprocess()
{
    /*
    //Matrix* out = MakeMatrixWithData(1, 10, MT_FLOAT, 0);

    // do memory alignment & read data
    u8 *model_out = (u8 *)calloc(LM_OUT_ROW * LM_OUT_COL, sizeof(u8));
    float *model_out_f = (float *)calloc(FD_OUT_ROW * FD_OUT_COL, sizeof(float));
    nmem_data_alignment(E_NMEM_SEQ8, LM_OUT_COL_NMEM, LM_OUT_ROW,
                        (u8*)LM_OUT_ADDR, (u8 *)model_out, LM_OUT_COL);

    for (int i = 0; i < FD_OUT_ROW * FD_OUT_COL; i++) {  
        model_out_f[i] = ((float)((s8)model_out[i])) / 8;    
    }
    for(int i = 0; i < 10; ++i){
        fLib_printf("%f\n\r", model_out_f[i]);
    }
 
    // copy LM raw data from NMEM to SRAM & convert to float number
    for (int i = 0; i < 10; i++) {
        int rdata = ReadWord(LM_OUT_ADDR + i);
        float data = fixed16_to_float(, 5);
        out->data.f[i] = data;
    }

    // scaled landmark
    ScaleLandmarkResult(out, pBox);

    // print 
    for (int i = 0; i < 10; ++i)
        fLib_printf("%f\n", out->data.f[i]);
    */
    return true;
}

bool do_fr_postprocess()
{
    return true;
}

bool do_ld_postprocess()
{
    return true;
}

bool do_inprocess(int model_base_addr)
{
    int row = 0;
    int col = 0;

    // update parameters on inproc.bin
    //TODO: refactor to improve code
    switch(model_base_addr) {
    case FD_BASE_ADDR:
        fLib_printf("FD inproc update\n");
        // golden sample: testcase\model_app\inprocess\inproc_bin_golden\inproc_resize_fd\inproc.bin
        #if ((INPROC_RUN_MODE == INPROC_BIN_RUN_SDRAM) || (INPROC_RUN_MODE == INPROC_BIN_RUN_NDRAM))
        memcpy((void*)&inproc_bin_sram, (void*)FD_INP_CMD_ADDR, sizeof(inproc_bin_sram));
        update_inproc((int)&inproc_bin_sram, FD_INP_IN_ADDR, FD_INP_OUT_ADDR, 
                      FD_INP_IN_ROW, FD_INP_IN_COL, FD_INP_OUT_ROW, FD_INP_OUT_COL, 
                      0, 0, 0, 0,       // crop
                      128, 0, 0x20); // sub, start_row, src_img_mode (0x20 is nir)      
        #else
        update_inproc(FD_INP_CMD_ADDR, FD_INP_IN_ADDR, FD_INP_OUT_ADDR, 
                      FD_INP_IN_ROW, FD_INP_IN_COL, FD_INP_OUT_ROW, FD_INP_OUT_COL, 
                      0, 0, 0, 0,       // crop
                      128, 0, 0x20); // sub, start_row, src_img_mode (0x20 is nir)
        #endif
        row = FD_INP_OUT_ROW;
        col = FD_INP_OUT_COL;
        break;
    case LM_BASE_ADDR:
        fLib_printf("LM inproc update\n");
        // golden sample: testcase\model_app\inprocess\inproc_bin_golden\inproc_crop_resize_lm\inproc.bin
        update_inproc(LM_INP_CMD_ADDR, LM_INP_IN_ADDR, LM_INP_OUT_ADDR,
                      LM_INP_IN_ROW, LM_INP_IN_COL, LM_INP_OUT_ROW, LM_INP_OUT_COL, 
                      100, 120, 80, 60, 
                      128, 0, 0x20);
        row = LM_INP_OUT_ROW;
        col = LM_INP_OUT_COL;
        break; 
    case LD_BASE_ADDR:
        fLib_printf("LD inproc update\n");
        // golden sample: testcase\model_app\inprocess\inproc_bin_golden\inproc_crop_resize_ld\inproc.bin
        update_inproc(LD_INP_CMD_ADDR, LD_INP_IN_ADDR, LD_INP_OUT_ADDR, 
                      LD_INP_IN_ROW, LD_INP_IN_COL, LD_INP_OUT_ROW, LD_INP_OUT_COL, 
                      100, 120, 80, 60, 
                      128, 0, 0x21); 
        row = LD_INP_OUT_ROW;
        col = LD_INP_OUT_COL_CEIL;
        break;
    case FR_BASE_ADDR:
        fLib_printf("FR inproc update\n");
        // golden sample: testcase\model_app\inprocess\inproc_bin_golden\inproc_crop_resize_fr\inproc.bin
        update_inproc(FR_INP_CMD_ADDR, FR_INP_IN_ADDR, FR_INP_OUT_ADDR, 
                      FR_INP_IN_ROW, FR_INP_IN_COL, FR_INP_OUT_ROW, FR_INP_OUT_COL, 
                      200, 50, 200, 200,
                      //100, 120, 80, 60,  
                      128, 0, 0x20);  
        row = FR_INP_OUT_ROW;
        col = FR_INP_OUT_COL;
       break;
    default:
        return false;
    }

    // run inprocess
    #if (INPROC_RUN_MODE == INPROC_BIN_RUN_SDRAM)
    start_cnn(MEM_NPU_ADDR[SDRAM_IDX] + INPROC_ADDR_OFFSET, FD_INP_CMD_SIZE);   // inp are the same sizes
    #elif (INPROC_RUN_MODE == INPROC_BIN_RUN_NDRAM)
    start_cnn(MEM_NPU_ADDR[NDRAM_IDX] + INPROC_ADDR_OFFSET, FD_INP_CMD_SIZE);   // inp are the same sizes
    #else
    start_cnn(model_base_addr + INP_BIN_OFFSET, FD_INP_CMD_SIZE);   // inp are the same sizes
    #endif
    if (wait_cnn() != 0) {
        fLib_printf("ERR: NPU inproc timeout \n");
        return false;
    }

    // do rotate
    test_rotate(model_base_addr + NPU_IN_OFFSET, row, col, ROT_CW);

    return true;
}

int run_model(int model_base_addr)
{
    int cmd_size = 0;

    //TODO: refactor it
    switch(model_base_addr) {
    case FD_BASE_ADDR:  cmd_size = FD_CMD_SIZE;     break;
    case LM_BASE_ADDR:  cmd_size = LM_CMD_SIZE;     break;
    case FR_BASE_ADDR:  cmd_size = FR_CMD_SIZE;     break;
    case LD_BASE_ADDR:  cmd_size = LD_CMD_SIZE;     break;
    default:            return false;
    }
 
    start_cnn(model_base_addr, cmd_size);
    if (wait_cnn() != 0) {
        fLib_printf("ERR: Run model timeout \n");
        return false;
    }
    return true;
}

bool do_postprocess(int model_base_addr)
{
    //TODO: refactor it
    switch(model_base_addr) {
    case FD_BASE_ADDR:  
        do_fd_postprocess();
        break;
    case LM_BASE_ADDR:
        do_lm_postprocess();
        break;
    case FR_BASE_ADDR:
        do_fr_postprocess();
        break;
    case LD_BASE_ADDR:
        do_ld_postprocess();
        break;
    default:
        return false;
    }

    return true;
}

/**
 * @Brife RunCnnFdDyFp(), Run CNN FD detection.
 * @param ppOut1 pFeature 
 * @param ppOut2 pFeaturePool
 */
int RunCnnFdDyFp(const Image *pImg, Matrix **ppOut1, Matrix **ppOut2)
{
    //CNN_header *cnn = pCnnFd;
    
    #ifdef LOG_CNN_INFO
    // TODO: Wait setup.bin parser
    fLib_printf("== FD CNN Start ==\n");
    //fLib_printf("cmd length = %d\n\r",(int)cnn->cmdLength);
    //fLib_printf("number of operation: %d\n\r",(int)cnn->operationNum);
    #endif
        
    #ifdef LOG_CNN_INFO  
    DoProfStart();
    #endif

    if (!do_inprocess(FD_BASE_ADDR))
        return -1;

    /* // replace by do_inprocess(), remove it later
    start_cnn(FD_INP_CMD_ADDR, FD_INP_CMD_SIZE);
    if (wait_cnn() != 0) {
        fLib_printf("ERR: NPU INproc calculate timeout \n");
        return(-1);
    }
    */

    #ifdef LOG_CNN_INFO
    DoProfClick("FD: Inproc time");    
    DoProfStart();
    #endif

    start_cnn(FD_CMD_ADDR, FD_CMD_SIZE);
    if (wait_cnn() != 0) {
        fLib_printf("ERR: NPU calculate timeout \n");
        return(-1);
    }

    #ifdef LOG_CNN_INFO  
    DoProfClick("FD: Run model time");
    DoProfStart();
    #endif

    //Anto: 03/15: test FD will crash, open it later if we needs postprocess
    #if 0

    u8 *model_out = (u8 *)calloc(FD_OUT_ROW * FD_OUT_COL, sizeof(u8));
    float *model_out_f = (float *)calloc(FD_OUT_ROW * FD_OUT_COL, sizeof(float));
    nmem_data_alignment(E_NMEM_SEQ8, FD_OUT_COL_NMEM, FD_OUT_ROW,
                            (u8 *)FD_OUT_ADDR, (u8 *)model_out, FD_OUT_COL);
    for (int i = 0; i < FD_OUT_ROW * FD_OUT_COL; i++) {  
        model_out_f[i] = ((float)((s8)model_out[i])) / 8;    
    }
    for(int i = 0; i < 10; ++i){
        fLib_printf("%f\n\r", model_out_f[i]);
    }

    // FD post process here
    float *model_out_pool1 = (float *)calloc(sizeof(float),20*20*1);
    float *model_out_pool2 = (float *)calloc(sizeof(float),20*20*1);

    averagePool(model_out_f, model_out_pool1, 20, 20, 1, 3, 1, 1);
    averagePool(model_out_pool1, model_out_pool2, 20, 20, 1, 3, 1, 1);

    *ppOut1 = GetCnnResult(model_out_f, FD_OUT_ROW, FD_OUT_COL);
    *ppOut2 = GetCnnResult(model_out_pool2, FD_OUT_ROW, FD_OUT_COL);   

    #ifdef LOG_CNN_INFO
    DoProfClick("FD: Postporcess time");
    #endif
    
    free(model_out);
    free(model_out_f);
    free(model_out_pool1);
    free(model_out_pool2);

    #endif
    
    return 0;
}    

/**
 * @Brife RunCnnFdDyFp2(), Run CNN FD detection.
 *
 * @param ppOut1 pFeature 
 * @param ppOut2 pFeaturePool
 */
int RunCnnFdDyFp2(const Image *pImg, Matrix **ppOut1, Matrix **ppOut2)
{
    #ifdef LOG_CNN_INFO
    // TODO: Wait setup.bin parser
    fLib_printf("== FD CNN Start ==\n");
    //fLib_printf("cmd length = %d\n\r",(int)cnn->cmdLength);
    //fLib_printf("number of operation: %d\n\r",(int)cnn->operationNum);
    #endif
        
    #ifdef LOG_CNN_INFO  
    DoProfStart();
    #endif

    if (!do_inprocess(FD_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO
    DoProfClick("FD: Inproc time:");    
    DoProfStart();
    #endif

    if (!run_model(FD_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO  
    DoProfClick("FD: Run model time:");
    DoProfStart();
    #endif

    if (!do_postprocess(FD_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO
    DoProfClick("FD: Postprocess time");
    #endif

    return 0;
}    

int RunCnnLandmarkDyFp(const Image *pImg, const Box *pBox, Matrix **ppOut)
{
    //CNN_header *cnn = pCnnLm;
    
    #ifdef LOG_CNN_INFO
    fLib_printf("== LM CNN Start ==\n");
    //fLib_printf("cmd length = %d\n\r",(int)cnn->cmdLength);
    //fLib_printf("number of operation: %d\n\r",(int)cnn->operationNum);
    #endif
        
    #ifdef LOG_CNN_INFO  
    DoProfStart();
    #endif

    if (!do_inprocess(LM_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO
    DoProfClick("LM: Inproc time");    
    DoProfStart();
    #endif

    if (!run_model(LM_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO  
    DoProfClick("LM: Run model time");
    DoProfStart();
    #endif

    if (!do_postprocess(LM_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO
    DoProfClick("LM: Postprocess time");
    #endif

    return 0;
}

/**
 * @RunCnnLivenessDyFp liveness detection
 * @param pImg depth image
 *
 */
//int RunCnnLivenessDyFp(const Image *pImg, const Box *pBox, Matrix *pLandmark)
//static void pre_process_image_f(Image *img, CNN_header *cnn, int need_pre_process, bool copy_to_three_ch);
//void RunCnnLivenessDyFp()
int RunCnnLivenessDyFp(const Image *pImg, const Box *pBox, Matrix *pLandmark)
{    
    #ifdef LOG_CNN_INFO
    fLib_printf("== LD CNN Start ==\n");
    #endif
        
    #ifdef LOG_CNN_INFO  
    DoProfStart();
    #endif

    if (!do_inprocess(LD_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO
    DoProfClick("LD: Inproc time");    
    DoProfStart();
    #endif

    // only test LD inprocess
    return 0;

    if (!run_model(LD_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO  
    DoProfClick("LD: Run model time");
    DoProfStart();
    #endif

    if (!do_postprocess(LD_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO
    DoProfClick("LD: Postprocess time");
    #endif

    return 0;

#if 0
    u32 tStart, tEnd;
    Image *cnoseimg;
    Image *rnoseimg;
    float model_factor;
    float *model_output;    
    CNN_header *cnn = pCnnLd;
    operation *head = pHeadLd;
    #ifdef LOG_CNN_INFO  
    fLib_printf("cmd length = %d\n\r",(int)cnn->cmdLength);
    fLib_printf("number of operation: %d\n\r",(int)cnn->operationNum);
    #endif
    dram = malloc((cnn->dramSize)*sizeof(u16));
    operation *curOp = head;
    operation *lastOp;


    rect rectx;
    rectx.x = pBox->x1;
    rectx.y = pBox->y1;
    rectx.w = pBox->x2 - pBox->x1;
    rectx.h = pBox->y2 - pBox->y1;

    *cnoseimg = preprocess_depth_image(*pImg, rectx, pLandmark->data.f, 0);
    //rnoseimg = resize_image(cnoseimg, detector->ptr_kdetector[3]->net->w, detector->ptr_kdetector[3]->net->h);    
    rnoseimg = MakeImage(cnn->inputC, cnn->inputR);
    cvResize(rnoseimg, cnoseimg);
    
    //Checked
    //float *X = rnoseimg->data.f;

    //float *curv_map = network_predict(detector->ptr_kdetector[3]->net, X);


    //Run CNN
    //Image* rnoseimg = MakeImageWithData(56, 56, (void*) DDR_IMG_BASE_ADDR);
	#if 0 //add for test
    rnoseimg = MakeMatrixWithData(56, 56, MT_FLOAT, (void*) DDR_DEPTH_BASE_ADDR);
    pre_process_image_f(rnoseimg, cnn, 1, false);
	#else
    pre_process_image(rnoseimg, cnn, 1, false);
	#endif
    start_cnn(0, 0);
    wait_cnn();
    model_factor = *(float *)&cnn->model_scale;
    #ifdef LOG_CNN_INFO 
    fLib_printf("model_factor: %f\n\r",model_factor);
    #endif
    multiply_model_factor(lastOp, model_factor, &model_output);
    #ifdef LOG_CNN_INFO 
    for(int i = 0; i < 34*34*2; ++i){
        fLib_printf("%f\n\r", model_output[i]);
    }
    #endif
    //curvature_det
    float curv_accum = 0.;

    int input_size = cnn->inputC * cnn->inputCh * cnn->inputR;
    int size_curv_map = 34 * 34 * 2;

    fLib_printf("liveness outputs: input:%d  output:%d\n", input_size, size_curv_map);
    for (int i = 0; i < size_curv_map; i++) {
        if (model_output[i] < 0) {
            curv_accum += (-model_output[i]);
        } else {
            curv_accum += model_output[i];
        }
    }

    if (curv_accum / ((float)input_size) < 0.1) {
        FreeImage(cnoseimg);
        FreeImage(rnoseimg);
        return 0;
    }
#endif
    return 1;

}
    
    
int RunCnnNirFrVggDyFp(const Image *pImg, Matrix **ppOut)
{
    #ifdef LOG_CNN_INFO
    fLib_printf("== FR CNN Start ==\n");
    #endif
        
    #ifdef LOG_CNN_INFO  
    DoProfStart();
    #endif

    if (!do_inprocess(FR_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO
    DoProfClick("FR: Inproc time");    
    DoProfStart();
    #endif

    if (!run_model(FR_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO  
    DoProfClick("FR : Run model time");
    DoProfStart();
    #endif

    if (!do_postprocess(FR_BASE_ADDR))
        return -1;

    #ifdef LOG_CNN_INFO
    DoProfClick("FR: Postprocess time");
    #endif

    return 0;


#if 0
    /******************************** vgg ********************************/
    u32 tStart, tEnd;
	float totalTime;
    float model_factor;
    float *model_output;
    CNN_header *cnn = pCnnFr;
    operation *head = pHeadFr;
    #ifdef LOG_CNN_INFO    
    fLib_printf("cmd length = %d\n\r",cnn->cmdLength);
    fLib_printf("number of operation: %d\n\r",cnn->operationNum);
    #endif
    dram = malloc((cnn->dramSize)*sizeof(u16));
    operation *curOp = head;
    operation *lastOp;

    pre_process_image((Image *)pImg, cnn, 1, true);

    // Operation Node Analysis
    start_cnn(0, 0);
    wait_cnn();
    model_factor = *(float *)&cnn->model_scale;
    //fLib_printf("model_factor: %f\n\r",model_factor);
    // multiply model factor
    //multiply_model_factor(lastOp, model_factor, &model_output);
    cpu_operation_offset = 0;
    free(dram);
    // vgg post process here
    // TO DO normalize
    *ppOut = MakeMatrixWithData(1, 512, MT_FLOAT, 0);
    memcpy((*ppOut)->data.f, model_output, sizeof(float)*1*512);
#if 0
    Matrix* fr_out = MakeMatrixWithData(1, 512, MT_FLOAT, 0);
    memcpy(fr_out->data.f, model_output, sizeof(float)*1*512);
NormalizeF:eature(fr_out);

    for(int i =0; i < 512; ++i) {
        fLib_printf("%f\n\r",fr_out->data.f[i]);
    }
    FreeMatrix(fr_out);
#endif
    free(model_output);
    fLib_printf("FR done!\n\r");
#endif
    return 0;
}

void pre_process_image_f(Image *img, CNN_header *cnn, int need_pre_process, bool copy_to_three_ch)
{
    int index = 0;
    int data;
    int channel_actual = 0;
    for(int channel = 0; channel < cnn-> inputCh + 1; ++channel){
        for(int r = 0; r < cnn -> inputR; ++r){
            for(int c = 0; c < cnn -> inputC; ++c){
                if(channel != cnn-> inputCh){
                    if(copy_to_three_ch == true)
                        data = img->data.f[channel_actual * cnn->inputR * cnn->inputC + r * cnn->inputC + c];
                    else
                        data = img->data.f[channel * cnn->inputR * cnn->inputC + r * cnn->inputC + c];
                    //decide pre_process
                    //inputImage[index++] = data << (cnn->input_frac_bit - 8);
                    if(need_pre_process == 1)
                        dram[index++] = (data-128) << (cnn->input_frac_bit - 8);
                    else
                        dram[index++] = data << (cnn->input_frac_bit - 8);
                }else{
                    dram[index++] = 0;
                }
            }
        }
    }
}

void pre_process_image(Image *img, CNN_header *cnn, int need_pre_process, bool copy_to_three_ch)
{
    int index = 0;
    int data;
    int channel_actual = 0;
    for(int channel = 0; channel < cnn-> inputCh + 1; ++channel){
        for(int r = 0; r < cnn -> inputR; ++r){
            for(int c = 0; c < cnn -> inputC; ++c){
                if(channel != cnn-> inputCh){
                    if(copy_to_three_ch == true)
                        data = img->data.c[channel_actual * cnn->inputR * cnn->inputC + r * cnn->inputC + c];
                    else
                        data = img->data.c[channel * cnn->inputR * cnn->inputC + r * cnn->inputC + c];
                    //decide pre_process
                    //inputImage[index++] = data << (cnn->input_frac_bit - 8);
                    if(need_pre_process == 1)
                        dram[index++] = (data-128) << (cnn->input_frac_bit - 8);
                    else
                        dram[index++] = data << (cnn->input_frac_bit - 8);
                }else{
                    dram[index++] = 0;
                }
            }
        }
    }
}


static void parse_set_up(operation *cur_op, CNN_header **cur_cnn, int set_up_length, u32 *setup_buffer){
    // first one is clc value
    // second one is a version number
    int i = 11;
    operation *prevOp, *newOp;
    newOp = cur_op;
    prevOp = cur_op;
    *cur_cnn = (CNN_header *) &setup_buffer[2];
    while(i < set_up_length / 4){
        if(setup_buffer[i] == 0){
            // current is a input Node
            if(i != 11){
                newOp = (operation*)malloc(sizeof(operation));
                prevOp->next = newOp;
            }
            operation_init(newOp,0);
            newOp -> inN = (inNode*)malloc(sizeof(inNode));
            newOp -> inN = (inNode*) &setup_buffer[i+1];
            prevOp = newOp;
            i += 16;
        }else if(setup_buffer[i] == 1){
            // current is a output Node
            if(i != 11){
                newOp = (operation*)malloc(sizeof(operation));
                prevOp->next = newOp;
            }
            operation_init(newOp,1);
            newOp -> outN = (outNode*)malloc(sizeof(outNode));
            newOp -> outN = (outNode*) &setup_buffer[i+1];
            prevOp = newOp;
            i += 16;
        }else if(setup_buffer[i] == 2){
            // current is a cpu Node
            if(i != 11){
                newOp = (operation*)malloc(sizeof(operation));
                prevOp->next = newOp;
            }
            operation_init(newOp,2);
            newOp -> cpuN = (cpuNode*)malloc(sizeof(cpuNode));
            newOp -> cpuN = (cpuNode*) &setup_buffer[i+1];
            prevOp = newOp;
            i += 16;
        }
    }
}

void operation_init(operation *cur_op, u32 op_id){
    cur_op->op_id = op_id;
}


/**
 * @brief npu_reset(), NPU reset
 */
void npu_reset(void)
{
    u32 val;
    /* 0xC238_004C[2], NPU reset control register. set 0 : reset, 1: release */
    val = ReadWord(0xC238004C);
    WriteWord(0xC238004C, val & ~BIT(2));
    WriteWord(0xC238004C, val | BIT(2));
}

/**
 * @brief start_cnn(), NPU start
 * @param cmd_addr command address
 * @param len command length
 */
void start_cnn(u32 cmd_addr, u32 len) 
{
    npu_reset();
    WriteWord(ADDR_NPU_CODE, cmd_addr);
    WriteWord(ADDR_NPU_CLEN, len);
    WriteWord(ADDR_NPU_INT, NPU_INT_int(0xFFF));    
    WriteWord(ADDR_NPU_INTEN, NPU_INT_int(0xFFF));
    WriteWord(ADDR_NPU_RUN, 0x00000001);
    return;    
}

/**
 * @brief wait_cnn(), Wait for NPU running 
 * @return 0: Success, -1:Fail
 */
int wait_cnn(void) 
{
    u32 st_tick_s, sp_tick_s, total_time;
    st_tick_s = SYS_TMR_GET_TICK();
    while(!(ReadWord(ADDR_NPU_INT) & 0x80)) {
        sp_tick_s = SYS_TMR_GET_TICK();
        total_time = (sp_tick_s >= st_tick_s)? sp_tick_s - st_tick_s : st_tick_s - sp_tick_s;
        //Anto: modify for 491*864 NIR8 image
        //if ( total_time > 2000) {
        if ( total_time > 10000) {
            return (-1);
        }
    }
    return 0;
}





void CopyRow_C(const u8 *src, u8 *dst, int size) {
    memcpy(dst, src, size);
}


/**
 * @brife nmem_data_alignment(), NMEM data alignment.
 * @param src_col
 * @param src_row
 * @param src source buffer
 * @param dst destination buffer
 * @param dst_col destination stride
 *     nmem       model output
 *   ex: 20 30 1 -> 20 20 1 
 */
void nmem_data_alignment(NMEM_DATA_FORMAT format, u32 src_col, u32 src_row,
                         const u8 *src, u8 *dst, u32 dst_col)
{
    void (*CopyRow)(const u8* src, u8* dst, int size) = CopyRow_C;
    if (format == E_NMEM_SEQ16) {
            src_col *= 2;
            src_row *= 2;
            dst_col *= 2;
    }
       
    while (src_row-- > 0) {
        CopyRow(src, dst, dst_col);       
        src += src_col;
        dst += dst_col;
    }
}



#endif   // end of #ifndef SYS_PC
