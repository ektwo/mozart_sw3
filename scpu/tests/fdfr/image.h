/**
 * @file      image.h
 * @brief     3D image related functions header
 * @version $Id: image.h 555 2018-08-14 03:13:37Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __IMAGE_H__
#define __IMAGE_H__


#include "matrix.h"
#include "image_base.h"

/* ##################
 * ##    Define    ##
 * ################## */

#define IM_DATA(pImg)                          ((pImg)->data.c)

#define IM_DATA_AT_ROW(pImg, width, i)         (IM_DATA(pImg) + ((i)*(width)))
#define IM_DATA_AT(pImg, width, i, j)          (IM_DATA_AT_ROW(pImg,(i),(width)) + (j))

#define IM_GET_VAL(pData, width, i, j)         (_M_GET_VAL(pData, width, i, j))
#define IM_SET_VAL(pData, width, i, j, val)    (_M_SET_VAL(pData, width, i, j, val))

/* ##############################
 * ##    Exported Functions    ##
 * ############################## */

Image* MakeImage(int width, int height);
Image* MakeImageWithData(int width, int height, void *pData);
void FreeImage(Image *pImg);
Image* SubImage(const Image *pSrc, int hStr, int hEnd, int wStr, int wEnd);
Image *GenPadImg(const Image *pSrc, const Size *pSize);

/* For testing (should be removed in the future) */
#ifdef SYS_PC
Image* ImgFromBinFile(const char *filename, bool isDepthImg);
void ImgToBinFile(const Image *pImg, const char *filename);
#endif

Image* ReadImage(const char* pFilename);
void PrintImage(const Image *pImg);
void PrintImageThenFree(Image *pImage);
void WriteImage(const Image *pImg, const char *rawFile, const char *imgFile);

#endif
