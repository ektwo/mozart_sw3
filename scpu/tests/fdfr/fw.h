/**
 * @file      fd.h
 * @brief     face detection header
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __FW_H__
#define __FW_H__


#include "image.h"

int FwMain(void);
#ifdef DYFP
#else
bool ProcessImg(void);
#endif

#endif
