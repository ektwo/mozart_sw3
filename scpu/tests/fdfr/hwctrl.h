/**
 * @file      hwctrl.h
 * @brief     hwctrl functions
 * @version $Id: matrix.h 163 2018-04-23 02:54:08Z kai.wang $
 * @copyright (c) 2018 Kneron Inc. All right reserved.
 */

#ifndef __HWCTRL_H__
#define __HWCTRL_H__

#include <stdbool.h>
#include "image.h"

/**
 * @brief Calculate scale ratio in fixed point (100% is 4096)
 * @param src source width/height
 * @param dst destination width/height
 * @return scaling ratio
 */
#define CAL_SCALE_RATIO(src, dst)  (u32) (((src-1) << 12) / (dst-1))


 /**
  * @brief Image scaling param
  * @param subVal subtract value (for normalization, 128 for example)
  * @param cropXstr crop x start (inclusive)
  * @param cropXend crop x end (exclusive)
  * @param cropYstr crop y start (inclusive)
  * @param cropYend crop y end (exclusive)
  * @param scaleW width of scaled image
  * @param scaleH height of scaled image
  * @param padX padding of left and right
  * @param padY padding of top and bottom
  */
typedef struct {
    int isFrame0;
    int moveMode;  /* 0: copy to channel 0 */
                   /* 1: copy to channel 0/1/2 */
    int srcW;
    int srcH;
    int subVal;
    int divide;
    int cropXstr;  /* crop start point (inclusive) */
    int cropXend;  /* crop end point (exclusive) */
    int cropYstr;
    int cropYend;
    int scaleImgOff;
    int scaleW;
    int scaleH;
    int padX;
    int padY;
} __attribute__((packed)) ImgScaleParam;

int RegWaitFlag(u32 addr, int bitOff, int flagOn);
void RegReset(addr_t addr, int bitOff);

void WaitCnnBootDone(void);
void InitGammaCorrection(void);

void StartFetchNirImg(void);
void StartFetchDepthImg(void);
Image* GetNirImg(void);
Image* GetDepthImg(void);
void AdderCtrl(bool blOn);
void Adder(u16 *pSrcData, u16 *pRaddrData, int len, u32 bias, u16 scale, u16 leaky, u32 raddrAddr, u32 dstAddr);
void Reshape(int width, int height, int channel, int validW, int validH, int direction);

void StartCnn(int cmdOffset, bool blResetExtWeightPtr);
void StartCnnFd(int cmdOffset, int weightOffset);
void WaitCnn(void);

#endif


