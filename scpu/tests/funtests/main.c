/* -------------------------------------------------------------------------- 
 * Copyright (c) 2013-2016 ARM Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *      Name:    main.c
 *      Purpose: RTX for Kneron
 *
 *---------------------------------------------------------------------------*/

#include <stdio.h>
#include <string.h>
#include "DrvUART010.h"
#include "DrvDMAC020.h"
//#include "ARMCM4.h"                     // Device header
#include "cmsis_os2.h"                    // ARM::CMSIS:RTOS2:Keil RTX5
#include "Driver_Common.h"
#include "DrvPWMTMR010.h"
#include "Driver_Flash.h"
#include "kneron_mozart.h"                // Device header
#include "base.h"
#include "npu.h"
#include "crc.h"
#include "com.h"
#include "io.h"
#include "string.h"
#include "ddr.h"
#include "msg.h"
#include "sys.h"
#include "dbg.h"
#include "pll_enable.h"

#define TEST_NPU_WDMA_RDMA      1

#define MSG_DBG_LOG 
#define MSG_MEM_BYTE_WRITE      0

extern void DMA_Mem_Mem_Test(void); 

int cmd_parser(u8 *buf, int len);
void sys_init(void);
void mem_clr(u32 addr, u32 len);


/**
 * Flash control 
 */
ARM_Flash_SignalEvent_t m_event;
extern ARM_DRIVER_FLASH Driver_Flash;
osThreadId_t tid_flash;
ARM_DRIVER_FLASH *pdrv_flash = &Driver_Flash;

/**
 * @info(), Get kernel information
 */
void info(void){
    char infobuf[100];
    osVersion_t osv;
    osStatus_t status;
 
    status = osKernelGetInfo(&osv, infobuf, sizeof(infobuf));
    if(status == osOK) {
        fLib_printf("+-------------------Keil RTX5----------------------+\n");
        fLib_printf("Kernel Information: %s\r\n", infobuf);
        fLib_printf("Kernel Version    : %d\r\n", osv.kernel);
        fLib_printf("Kernel API Version: %d\r\n", osv.api);
    }
}


int static ddr_sram_rw_verify(void)
{
    #if 1 // use DRAM
    #include "inst_ddr_8k.h"
    #define MEM_BASE (DDR_MEM_BASE)
    #define MEM_SIZE (0x80000)
    #define MEM_BASE4NPU (MEM_BASE)
    #define PATTERN_LENGTH (17*16)
    #else // use SdRam
    #include "inst_sram_8k.h"
    #define MEM_BASE (SdRAM_MEM_BASE)
    #define MEM_SIZE (SdRAM_MEM_SIZE)
    #define MEM_BASE4NPU (0x20200000)
    #define PATTERN_LENGTH (8*64*16)

    #endif
    #define PATTERN1(i) ((i^0x55aa)+((i^0xaa55)<<16))


    #if TEST_NPU_WDMA_RDMA
    int int_ok_cnt=0;
    int int_fail_cnt=0;    
    int cmp_ok_cnt=0;
    int cmp_fail_cnt=0;
    int TestLength=0;    

    #endif
    
    int i, j, k, len, test_loop;
    k=0;
    int test1_err_cnt = 0;
    int test2_err_cnt = 0;
    int test3_err_cnt = 0;
    test_loop = 10;
    fLib_printf("Test mem 1\n");
    for (k = 0; k < test_loop; k++) {
        for (i = 0; i < MEM_SIZE / 4; i++) {
            outw(MEM_BASE+4 * i, PATTERN1(i));
        }
        len = 0;
        for (i=0; i < MEM_SIZE / 4; i++) {
            //for( k=0; k<10; k++ );
            j = inw(MEM_BASE+4*i);
            if (j != PATTERN1(i))
            {
                test1_err_cnt++;
                fLib_printf("%d> Test mem 1 failed ev=%08x, av=%08x\n", i, PATTERN1(i), j);
                len++;
                if(len > 10) {
                    fLib_printf("%d> exit\n");
                    return (-1);
                }
            }
        }
        fLib_printf("Test loop %d/%d\n", k, test_loop);
    }
    
    fLib_printf("Test mem 2\n");
    for (k = 0; k < test_loop; k++) {
        for (i = 0; i < MEM_SIZE / 4; i++) {
            outw(MEM_BASE + 4 * i, (i << 16) + ((i^0xffff)));
        }
        len = 0;
        for(i = 0; i < MEM_SIZE / 4; i++) {
            j = inw(MEM_BASE + 4 * i);
            if ( j != ((i << 16) + (i^0xffff))) {
                test2_err_cnt++;
                fLib_printf("Test mem 2 failed ev=%08x, av=%08x\n", (i<<16)+((i^0xffff)), j);
                len++;
                if(len>10) {
                    fLib_printf("%d> exit\n");
                    return (-1);
                }
            }
        }
        fLib_printf("Test loop %d/%d\n", k, test_loop);
    }
    
    fLib_printf("Test mem 3\n");
    for (k = 0; k < test_loop; k++) {
        for (i = 0; i < MEM_SIZE / 4; i++) { 
            outw(MEM_BASE + 4 * i, 0);
        }
        len=0;
        for(i = 0; i < MEM_SIZE / 4; i++) {
            j = inw(MEM_BASE + 4 * i);
            if ( j != (0)) {
                test3_err_cnt++;
                fLib_printf("Test mem 3 failed ev=%08x, av=%08x\n", 0, j);
                len++;
                if (len > 10) {
                    fLib_printf("%d> exit\n");
                    return (-1);
                }
            }
        }
        fLib_printf("Test loop %d/%d\n", k, test_loop);
    }
    #if TEST_NPU_WDMA_RDMA
    for(k=0; k<10;k++) {
        fLib_printf("run %d: start\n", k );
        fLib_printf("load pattern\n");
        for( i=0; i<PATTERN_LENGTH/4; i++ ) {
            // fLib_printf("load pattern[%d]=%08x\n", 4*i, i+((i^0xffff)<<16));
            outw(MEM_BASE+0x100+4*i, i+((i^0xffff)<<16));
        }
    
        fLib_printf("clear nram\n");
        for( i=0; i<PATTERN_LENGTH/4; i++ ) {
            outw(0x30000000+4*i, 0x0);
        }
        fLib_printf("load inst, size=%d\n", INST_LENGTH*4);
        for( i=0; i<INST_LENGTH; i++ ) {       
            // fLib_printf("inst[%d]=%08x\n", 4*i, inst_na[i]);
            outw(MEM_BASE+4*i, inst_na[i]);
        }
        for( i=0; i<INST_LENGTH; i++ ) {       
            j=inw(MEM_BASE+4*i);
            if( j!=inst_na[i] ) {
                fLib_printf("read back error inst[%d]=%08x, ev=%08x\n", 4*i, j, inst_na[i]);
            }
        }

        outw(ADDR_NPU_CODE, NPU_CODE_a(MEM_BASE4NPU));
        outw(ADDR_NPU_CLEN, NPU_CLEN_l(INST_LENGTH*4));
        outw(ADDR_NPU_INTEN, NPU_INTEN_int(0x00)) ;
        outw(ADDR_NPU_INTEN, NPU_INTEN_int(0xff)) ;
        outw(ADDR_NPU_INT, NPU_INT_int(0xff));
        outw(ADDR_NPU_RUN, NPU_RUN_go(1));
        fLib_printf("waiting interrupt\n");
        
        for( i=0; i<100000; i++) {
            j=inw(ADDR_NPU_INT);
            if(j&0xf0) break;
        }
        TestLength=inw(ADDR_NPU_RDMA0_SRC2);
        if( !(j&0xf0) ) {
            int_fail_cnt++;
            fLib_printf("Run %d: Wait int 0x80 timeout, int=0x%8x\n", k, j);
            i=inw(ADDR_NPU_RDMA0_SRC0); fLib_printf("       sa=0x%08x(%d)\n", i, i );
            i=inw(ADDR_NPU_RDMA0_SRC1); fLib_printf("       pitch=0x%08x(%d)\n", i, i );
            i=inw(ADDR_NPU_RDMA0_SRC2); fLib_printf("       len=0x%08x(%d)\n", i, i );
        } else {   
            int HasError=0;
            unsigned int epv,av;
            int_ok_cnt++;
            fLib_printf("Run %d: ok, int=0x%8x\n", k, j);
            len=0;
            for( i=0; i<TestLength/4; i++ ) {
                epv=inw(MEM_BASE+0x100+4*i);
                av=inw(0x30000000+4*i);
                if( epv!=av) {
                    fLib_printf("@%d, ev(%08x), av(%08x)\n", 4*i, epv, av);
                    HasError=1;
                    len++;
                    if(len>10) break;
                }
            }
            if( HasError) 
                cmp_fail_cnt++;
            else 
                cmp_ok_cnt++;
        }
        fLib_printf
        ( "run %d: int_ok(%d) int_fail(%d) cmp_ok(%d) cmp_fail(%d), test length=%d\n"
        , k
        , int_ok_cnt
        , int_fail_cnt
        , cmp_ok_cnt
        , cmp_fail_cnt
        , TestLength
        );
    }
    #endif
    fLib_printf("End of test\n");    
    fLib_printf("Test result mem1_test %d/%d, mem2_test=%d/%d, mem3_test=%d/%d\n", 
            test_loop - test1_err_cnt, test_loop, 
            test_loop - test2_err_cnt, test_loop, 
            test_loop - test3_err_cnt, test_loop);        
    return 0;
}


/**
 * @brief buf_dbg_out, print out data by uart debug port
 * @param buf buffer data to write
 * @param len data length
 */
int buf_dbg_out(u8 *buf, int len)
{
    char buf1[64];
    for (int i = 0; i < len; i++) {
        sprintf(buf1 + ((i%16) * 3), "%02X ", buf[i]);
        if ((i + 1) % 16 == 0) {
            fLib_printf("%s\n", buf1);
            memset(buf1, 0, sizeof(buf1));
        }
    }
    fLib_printf("%s\n", buf1);
    return 0;
}

#define FLASH_TIMER             DRVPWMTMR1
#define FLASH_GET_TICK          fLib_CurrentT1Tick

void npu_run()
{
    u32 st_tick, sp_tick, total_time;
    outw(ADDR_NPU_INT, NPU_INT_int(0xff));    
    st_tick = SYS_TMR_GET_TICK();
    outw(ADDR_NPU_RUN, NPU_RUN_go(1));     
    while(!(inw(ADDR_NPU_INT) & 0x80)) {
        //st_tick = osKernelGetTickCount();;
        sp_tick = FLASH_GET_TICK();
        total_time = (sp_tick >= st_tick)? sp_tick - st_tick : st_tick - sp_tick;
        if ( total_time > 2000) {
            fLib_printf("ERR: NPU calculate timeout \n");
            break;
        }
    }
    sp_tick = SYS_TMR_GET_TICK();
    fLib_printf("NPU calculate time (st=%d, sp=%d) %d \n", sp_tick, st_tick, sp_tick - st_tick);
    return;    
}

/**
 * @brief cmd_parser(), uart/usb message parser
 */
int cmd_parser(u8 *buf, int len)
{    
    MsgHdr *msghdr;
    u8 *pdata = NULL;
    u32 *pwdata = NULL;
    u16 crc16;
    u8 rstatus = STS_OK;
    int wlen;
    u32 st_tick, sp_tick;
    msghdr = (MsgHdr*) buf;
    pdata = (u8*) (buf + MSG_HDR_SIZE);
    pwdata = (u32*) (buf + MSG_HDR_SIZE);

    crc16 = gen_crc16((const u8*) (buf + 4), len - 4);  
    if (crc16 != msghdr->crc16) {
        /* CRC16 checksum error */
        msg_pack(CMD_CRC_ERR, NULL, 0);
        return STS_ERR_CRC;
    }

    wlen = (msghdr->len % 4) == 0? msghdr->len/4 :  msghdr->len/4 + 1;
    /* Command parsing */
    switch(msghdr->cmd) {
    case CMD_MEM_WRITE:
        #if MSG_MEM_BYTE_WRITE
        if ((msghdr->addr >= 0x30000000) && (msghdr->addr < 0x40000000)) {
        #else
        if (1) {
        #endif
            for (int i = 0; i < wlen; i++) {
                outw(msghdr->addr + i * 4, (u32) *(pwdata + i));
            }
        } else {
            for (int i = 0; i < msghdr->len; i++) {
                outb(msghdr->addr + i, *(pdata + i));
            }
        }
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_MEM_READ:
        #if MSG_MEM_BYTE_WRITE
        if ((msghdr->addr >= 0x30000000) && (msghdr->addr < 0x40000000)) {
        #else
        if (1) {
        #endif
            for (int i = 0; i < wlen; i++) {
                *(pwdata + i) = inw(msghdr->addr + 4 * i);
            }
        } else {
            for (int i = 0; i < msghdr->len; i++) {
                *(pdata + i) = inb(msghdr->addr + i);
            }
        }
        msg_pack(CMD_MEM_READ, (u8 *)pdata, msghdr->len);
        break;
    case CMD_TEST_ECHO:
        msg_pack(CMD_TEST_ECHO, (u8 *)pdata, msghdr->len);
        break;
    case CMD_MEM_CLR:
        mem_clr(msghdr->addr, msghdr->len);
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_RUN_ONCE:
        msg_pack(CMD_ACK, NULL, 0);
        npu_run();
        break;
    case CMD_FLASH_INFO:
    case CMD_FLASH_CHIP_ERASE:
        fLib_printf("Flash erase start ..... \n");
        msg_pack(CMD_ACK, NULL, 0);
        st_tick = SYS_TMR_GET_TICK();
        pdrv_flash->EraseChip();
        sp_tick = SYS_TMR_GET_TICK();
        fLib_printf("Flash erase time(ms) (st=%d, sp=%d) %d \n", sp_tick, st_tick, sp_tick - st_tick);
        break;
    case CMD_FLASH_SECTOR_ERASE:
        msg_pack(CMD_ACK, NULL, 0);    
        fLib_printf("Flash erase start ..... \n");
        st_tick = SYS_TMR_GET_TICK();
        pdrv_flash->EraseSector(msghdr->addr);
        sp_tick = SYS_TMR_GET_TICK();
        fLib_printf("Flash erase time(ms) (st=%d, sp=%d) %d \n", sp_tick, st_tick, sp_tick - st_tick);
        break;
    case CMD_FLASH_READ:
        pdrv_flash->ReadData(msghdr->addr, pdata, msghdr->len);	
        msg_pack(CMD_MEM_READ, (u8 *)pdata, msghdr->len);
        break;
    case CMD_FLASH_WRITE:
        pdrv_flash->ProgramData(msghdr->addr, pdata, msghdr->len);
        msg_pack(CMD_ACK, NULL, 0);
        break;
    case CMD_DEMO_RUN:    
    case CMD_DEMO_STOP:
    case CMD_STS_CLR:
    default:
        msg_pack(CMD_ACK, NULL, 0);
        break;
    }
    return rstatus;
}



/**
 * @brief mem_clr(), DDR/NPU SRAM memory clear
 */
void mem_clr(u32 addr, u32 len)
{
    memset((void *) addr, 0, len);
}

/**
 * @brief sys_init(), system initial
 */
void sys_init()
{
    mem_clr(0x30000000, 512 * 1024);
    /* Enable Extenal debug, NPU deassert */
    // Disable this function if we need use only one SCPU 
    //outw(SCU_EXTREG_PA_BASE + 0x0068, 0x1001);
    /* outw(SCU_EXTREG_PA_BASE + 0x0014, inw(SCU_EXTREG_PA_BASE + 0x14) | BIT(22));   */
}





void flash_init()
{
   ARM_FLASH_INFO *pinfo;
    _DSG(1, "Driver initialize");
    pdrv_flash->Initialize(m_event);
    pdrv_flash->GetVersion();
    pinfo = pdrv_flash->GetInfo();
    _DSG(1, "0. flash size=%u \n", pinfo->flash_size);
}
/**
 * @brief main, main dispatch function
 */
int main(void) 
{
    int len;
    len =5;
#ifdef MOZART_CHIP
    pll_enable();
    ddr_init_chip_0429();
    uart4_enable();
#else
    ddr_init_1228_kingston_v35(0);
#endif    
    sys_init();
    fLib_SerialInit(DEBUG_CONSOLE, DEFAULT_CONSOLE_BAUD, PARITY_NONE, 0, 8, 0);
    fLib_SerialInit(MSG_PORT, BAUD_921600, PARITY_NONE, 0, 8, 0);
    info();         
    //ddr_sram_rw_verify(); 
    //DMA_Mem_Mem_Test();    
    SystemCoreClockUpdate();            // System Initialization 
    flash_init();
    osKernelInitialize();               // Initialize CMSIS-RTOS
    fLib_Timer_Init(DRVPWMTMR1, PWMTMR_1MSEC_PERIOD);
    while(1) {
        len = msg_read_final(msg_rbuf);
        if (len > 0) {
            //fLib_printf("Get uart_len = %d\n", len);
            //buf_dbg_out(msg_rbuf, len);
            cmd_parser(msg_rbuf, len);
        }
    }    
}
