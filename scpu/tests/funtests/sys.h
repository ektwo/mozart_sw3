#ifndef __SYS_H__
#define __SYS_H__

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "io.h"
#include "DrvDMAC020.h"

#define SYS_TMR             DRVPWMTMR1
#define SYS_TMR_GET_TICK    fLib_CurrentT1Tick

#define ReadWord(addr)              inw(addr)
#define WriteWord(addr, val)        outw(addr, val)


/* System control definition:
 *   SYS_PC
 *   SYS_XILINX */

#ifdef SYS_PC
typedef u64 addr_t;
#define DBG_DUMP      1
#define USE_C_MALLOC  1
#else
typedef u32 addr_t;
#endif

typedef struct {
    int width;
    int height;
} Size;

#define MIN(a,b)                (((a)<(b))?(a):(b))
#define MAX(a,b)                (((a)>(b))?(a):(b))
#define ABS(a)                  (((a)>=0)?(a):(-(a)))
#define FLOOR(val)              ((int)(val) - ((int)(val) > val))
#define ROUND(x)                ((x)>=0?(int)((x)+0.5):(int)((x)-0.5))

/* Image control and size definition */
#define SNRSD_IMG_WIDTH         736
#define SNRSD_IMG_HEIGHT        1296
#define SNRSD_IMG_CROP_EDGE     8

#define DPHSD_IMG_WIDTH         736
#define DPHSD_IMG_HEIGHT        1296
#define DPHSD_IMG_CROP_EDGE     8

#define ORG_IMG_WIDTH           360
#define ORG_IMG_HEIGHT          640
#define ORG_IMG_SIZE            (ORG_IMG_WIDTH * ORG_IMG_HEIGHT)

/* Face detection */
#define FD_IMG_SCALE_WIDTH      90
#define FD_IMG_SCALE_HEIGHT     160 /* 160x160 */
#define FD_IMG_SCALE_SIZE       MAX(FD_IMG_SCALE_WIDTH, FD_IMG_SCALE_HEIGHT)
#define FD_CNN_OUT_SIZE         20  /* 20x20 */
#define FD_SCORE_THRESH         0
#define FD_FEATURE_POOL_THRESH  45 / 81
#define FD_ACT_PT_CNT_THRESH    (FD_CNN_OUT_SIZE*FD_CNN_OUT_SIZE/3)
#define FD_RECT_MIN_SIZE        3
#define FD_PAD_SIZE_X           0
#define FD_PAD_SIZE_Y           0


#define LANDMARK_IMG_SIZE       48

#define DEPTH_DET_BOX_SIZE      30
#define DEPTH_DIFF_THRESH       10  // TODO: Need to double check the depth pixel definition

#define FR_IMG_WIDTH            112
#define FR_IMG_HEIGHT           112


/* ######################
 * ##    Memory Map    ##
 * ###################### */

#define BASE_ADDR           0x68000000
#define OP_MEM_ADDR         0x13000000

#define PC_IMG_IN           (BASE_ADDR + 0x00300000)
#define FRAME_0_ADDR        (BASE_ADDR + 0x00500000)
#define FRAME_0_SIZE        (320 << 10)
#define FRAME_1_ADDR        (BASE_ADDR + 0x00600000)
#define ALIGNED_IMG_ADDR    (FRAME_0_ADDR + ORG_IMG_WIDTH * ORG_IMG_HEIGHT)


#endif
